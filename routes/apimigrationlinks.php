<?php
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::get('/user', function (Request $request) {
    $user = $request->user();
    // Even with
    $user = Auth::user();

    return $user;
});
// CODE ADDED BY DKV

Route::post('ledgerImport1', 'API\Migration\CustomerSeedingController@ledgerImport1');
// bakerycustomer
Route::post('bakerycustomer', 'API\Migration\CustomerSeedingController@bakerycustomer');

// products with some category
Route::post('import/productofCategory', 'API\Migration\ProductSeedingController@migrateProductsWithCategory');

// update stock quantity
Route::post('import/stockQuantity', 'API\Migration\RateSeedingController@updatestockQuantity');

// Nano
Route::post('migrateCustomersNano', 'API\Migration\CustomerSeedingController@migrateCustomersNano');
// jeddah
Route::post('migrateCustomersdb2', 'API\Migration\CustomerSeedingController@migrateCustomersdb2');
Route::post('migrateSuppliersdb2', 'API\Migration\CustomerSeedingController@migrateSuppliersdb2');

// Riyadh
Route::post('migrateCustomers', 'API\Migration\CustomerSeedingController@migrateCustomers');
Route::post('migrateSuppliers', 'API\Migration\CustomerSeedingController@migrateSuppliers');

Route::post('product_import', 'API\Migration\ExcelImportController@productImport');
Route::post('rateUpdate', 'API\Migration\ExcelImportController@rateUpdate');
Route::post('customer_import', 'API\Migration\ExcelImportController@customerImport');
Route::post('stock_import', 'API\Migration\ExcelImportController@stockImport');


Route::get('rate_transfer', 'API\Migration\RateSeedingController@getActiveEanWithUnitRates');




/*
CODE ADDED BY DKV
MIGRATING DATA FROM OLD DATABASE WITHOUT STOCK (units ,category , subcategory , products ,stock rates)

STEP1 - DATABSE SETUP

a).setup MYSQL DB2 configuration (db from data to be exported)-  in env file
b).setup MYSQL DB1 configuration (db to  data to be imported)-  in env file
c).changes in migration file

make bigIncrements as bigInteger for the below files

I).2019_06_12_055131_create_categories_table
II).2019_06_12_055625_create_subcategories_table
III).2019_06_12_055325_create_units_table

for above file make
D).run below commands in cmd
i).php artisan migrate:fresh --seed
ii).php artisan passport:install

STEP2 */
Route::post('generalSeeding', 'API\Migration\SeedingController@generalSeeding');
// STEP3
Route::post('generateLinks', 'API\Migration\SeedingController@generateLinks');

// STEP4 ::run all links generated in step 3

Route::post('new_db_install', 'API\Migration\SeedingController@install_db');
Route::post('export_old_db', 'API\Migration\SeedingController@export_old_db');
Route::post('transfer/get_total_steps', 'API\Migration\SeedingController@get_steps');
Route::post('product_transfer/{type}', 'Api\Migration\SeedingController@startTransfer');

Route::post('create_default_stock', 'Api\Migration\SeedingController@createDefaultStock');
// END OF MIGRATION AND SETTINGS






Route::post('login', 'API\UserController@login');
Route::post('usr_register', 'API\UserController@usrRegister');

Route::group(['middleware' => 'auth:api'], function () {
    Route::post('details', 'API\UserController@details');

});

Route::post('add_product', 'Api\Product\ProductController@addProduct');
Route::get('logout', 'API\UserController@logout');

Route::post('edit_user_details', 'API\UserController@editUser');
Route::get('get_user_types', 'Api\UserController@getUsertypes');
Route::get('get_sub_users', 'Api\UserController@getSubUsertypes');
/*Products Module*/
Route::get('get_units', 'Api\Product\UnitsController@getUnits');
Route::get('get_base_units/{id}', 'Api\Product\UnitsController@getBaseUnits');
Route::post('get_base_unit_details', 'Api\Product\UnitsController@getBaseUnitDetails');
Route::get('get_drvd_units/{id}', 'Api\Product\UnitsController@getDerivedUnits');
Route::get('has_drvd_units/{id}', 'Api\Product\UnitsController@hasDerivedUnits');
Route::post('add_unit', 'Api\Product\UnitsController@addUnits');
Route::post('edit_unit', 'Api\Product\UnitsController@editUnits');

Route::get('get_categories', 'Api\Product\CategoryController@getCategories');
Route::post('add_category', 'Api\Product\CategoryController@addCategory');
Route::post('edit_category', 'Api\Product\CategoryController@editCategory');

Route::get('get_subcategories/{id}', 'Api\Product\CategoryController@getSubcategories');
Route::get('get_allsubcategories/{id}', 'Api\Product\CategoryController@getAllSubcategories');
//  api for retrieving all sub catagories for a parent cat id
Route::post('add_subcategory', 'Api\Product\CategoryController@addSubcategory');
Route::post('edit_subcategory', 'Api\Product\CategoryController@editSubCategory');

Route::post('edit_product', 'Api\Product\ProductController@editProduct');
Route::get('get_products', 'Api\Product\ProductController@getProducts');
Route::get('get_prod_detail/{id}', 'Api\Product\ProductController@getProductdetail');
Route::post('seed/products', 'Api\Product\ProductController@seed_products');
Route::get('test_prod/{id}', 'Api\Product\ProductController@testProd');

Route::post('add_feature', 'Api\Product\ProductController@addFeature');
Route::post('edit_feature', 'Api\Product\ProductController@editFeature');
Route::get('get_features', 'Api\Product\ProductController@getFeatures');

Route::post('add_prod_units', 'Api\Product\ProductController@addProdUnits');
Route::post('edit_prod_units', 'Api\Product\ProductController@editProdUnits');
Route::post('get_prod_units', 'Api\Product\ProductController@getProdUnits');

Route::post('add_prod_features', 'Api\Product\ProductController@addProdFeatures');
Route::post('edit_prod_features', 'Api\Product\NewProductController@editProdFeatures');
Route::get('get_prod_features', 'Api\Product\ProductController@getProdFeatures');
// Route::get('get_search/{prod_name?}', 'Api\Product\ProductController@getSearch');

// Manufacturer
Route::post('add_manufacturer', 'Api\Product\ManufacturerController@addManufacturer');
Route::post('edit_manufacturer', 'Api\Product\ManufacturerController@editManufacturer');
Route::get('get_manufacturers', 'Api\Product\ManufacturerController@getManufacturer');

// Damaged Product

Route::post('add_damaged_product', 'Api\Product\DamageController@addDamagedProduct');
Route::post('view_damaged_product', 'Api\Product\DamageController@viewDamagedProduct');
Route::post('delete_damaged_product', 'Api\Product\DamageController@deleteDamegedProduct');
Route::get('get_damaged_product', 'Api\Product\DamageController@getDamagedProduct');

/*Products Module end*/

// Ledger Group
Route::post('add_ledgroup', 'Api\Accounts\LedgerGroupController@addLedgroup');
Route::post('edit_ledgroup', 'Api\Accounts\LedgerGroupController@editLedgroup');
Route::get('get_ledgroup', 'Api\Accounts\LedgerGroupController@getLedgergroup');
Route::get('get_primary_ledgroup', 'Api\Accounts\LedgerGroupController@getPrimaryLedgergroup');

// Ledger
Route::post('add_ledger', 'Api\Accounts\LedgerController@addLedger');
Route::post('edit_ledger', 'Api\Accounts\LedgerController@editLedger');
Route::get('get_ledgerdetails', 'Api\Accounts\LedgerController@getLedgerDetails');
Route::post('set_opening_balance', 'Api\Accounts\LedgerController@setOpeningBalance');
Route::post('set_ledger_balance_privacy', 'Api\Accounts\LedgerController@setLedgerBalancePrivacy');
Route::post('get_cashinhand_bank_ledger', 'Api\Accounts\LedgerController@getCashInHandAndBanks');
Route::post('get_cashinhand_bank_ledger', 'Api\Accounts\LedgerController@getCashInHandAndBanks');
Route::post('get_non_acc_ledger', 'Api\Accounts\LedgerController@getNonAccLedger');
Route::post('get_acc_ledger_by_grp', 'Api\Accounts\LedgerController@searchLedgerByAccGroup');



// Ledger - Customer
Route::post('add_customer', 'Api\Accounts\LedgerController@addCustomer');
Route::post('edit_customer', 'Api\Accounts\LedgerController@editCustomer');
Route::get('get_customer', 'Api\Accounts\LedgerController@getCustomer');

// Ledger Supplier
Route::post('add_supplier', 'Api\Accounts\LedgerController@addSupplier');

// Ledger Staff
Route::post('search_ledger_detail', 'Api\Accounts\LedgerController@searchLedgerDetail');
Route::post('staff/{type}', 'Api\Accounts\staffController@satffOperations');

// Branch Mobule
Route::post('add_branch', 'Api\Company\BranchController@addBranch');
Route::post('edit_branch', 'Api\Company\BranchController@editBranch');
Route::get('get_branches', 'Api\Company\BranchController@getBranches');
Route::post('branch_data', 'Api\Company\BranchController@branchData');

// Company  Mobule
Route::post('company_data', 'Api\Company\BranchController@companyData');
Route::post('update_company', 'Api\Company\BranchController@updateCompany');

//Stock Module
Route::post('stock/{type}', 'Api\Stock\StockController@stock');
Route::post('stock_search/batch', 'Api\Stock\StockController@batchSearch');

// Route::post('stock_search/product', 'Api\Stock\StocksController@productSearch');

Route::post('stock_search/product', 'Api\Stock\StockController@productSearch');
Route::post('stock_search/product1', 'Api\Stock\StocksController@productSearch');

Route::post('stock_search/purchase', 'Api\Stock\StockController@productPurchaseSearch');
Route::post('stock_search/purchasebcode', 'Api\Stock\StockController@productPurchaseSearchBarcode');
Route::post('stock_search/purchaseuean', 'Api\Stock\StockController@productPurchaseSearchUean');

// Godown Module
Route::post('godown/{type}', 'Api\Godown\GodownController@Godown');
Route::post('godown_stock/{type}', 'Api\Godown\GodownStockController@GodownStock');
Route::post('godown_transfer/{type}', 'Api\Godown\GodownTransferController@GodownTransfer');
Route::get('get_godowns', 'Api\Godown\GodownController@getGodownByBranch');
Route::post('godowns_list', 'Api\Godown\GodownController@getAllGodowns');

Route::post('search_branch', 'Api\Company\BranchController@searchBranch');
Route::post('search_units', 'Api\Product\UnitsController@searchUnits');
Route::post('get_search', 'Api\Product\ProductController@getSearch');
Route::post('search_ledger', 'Api\Accounts\LedgerController@searchLedger');
Route::post('search_users', 'Api\Company\BranchController@searchUsers');

Route::post('search_manufacturers', 'Api\Product\ManufacturerController@searchManufacture');
Route::post('get_all_ledgroup', 'Api\Accounts\LedgerGroupController@getAllLedgergroup');
Route::post('search_categories', 'Api\Product\CategoryController@searchCategories');
Route::post('search_godown', 'Api\Godown\GodownController@searchGodown');

Route::get('users', 'API\UserController@allUsers');
Route::post('edit_user_details', 'API\UserController@editUser');
Route::get('get_base_units', 'Api\Product\UnitsController@getBaseUnitsAll');
Route::post('search_subcat', 'Api\Product\CategoryController@searchSubCat');
Route::post('search_features', 'Api\Product\ProductController@searchFeatures');

// Route::post('search_stocks', 'Api\Stock\StockController@searchStock');

//Product  Report
Route::post('product_report/{type}', 'Api\Reports\ProductReportController@Report');

//Stock  Report
Route::post('stock_report/{type}', 'Api\Reports\StockReportController@Report');

//Godown  Report
Route::post('godown_report/{type}', 'Api\Reports\GodownReportController@Report');

// product add
Route::post('new_product', 'Api\Product\NewProductController@addNewproduct');
Route::post('add_derived', 'Api\Product\NewProductController@addDerivedUnits');
Route::post('edit_derived', 'Api\Product\NewProductController@editDerivedUnits');
Route::post('add_features', 'Api\Product\NewProductController@addFeatures');
Route::post('add_stock_rates', 'Api\Product\NewProductController@updateStockRates');
Route::post('add_list_product', 'Api\Product\NewProductController@addListNewproduct');
Route::post('add_list_feature', 'Api\Product\NewProductController@addListFeature');

//Supplier
Route::post('add_supplier', 'Api\Purchase\SupplierController@addNewSupplier');
Route::get('get_suppliers', 'Api\Purchase\SupplierController@getSuppliers');
Route::post('search_supplier', 'Api\Purchase\SupplierController@searchSupplier');
Route::post('edit_supplier', 'Api\Purchase\SupplierController@editSupplier');
Route::post('list_suppliers', 'Api\Purchase\SupplierController@searchSupplier');

//Add Opening Stock
Route::post('open_stock', 'Api\Stock\OpeningStockController@openStocks');

//Remoove From Opening Stock
Route::post('list_opening_stock', 'Api\Stock\OpeningStockController@listOpeningStock');
Route::post('opening_stock_remoove', 'Api\Stock\OpeningStockController@revertOpeningStock');

//Purchase
Route::post('purchase', 'Api\Purchase\PurchaseController@Purchase');
Route::post('purchase/return', 'Api\Purchase\PurchaseController@PurchaseReturn');
Route::post('list_purchase_return', 'Api\Purchase\PurchaseController@ListPurchaseReturn');
Route::post('void_purchase_return', 'Api\Purchase\PurchaseController@voidTranReturn');
Route::post('search_stock_products', 'Api\Stock\StockController@searchStocksProduct');
Route::post('search_stock_barcode', 'Api\Stock\StockController@searchStocksProductBarcode');
Route::post('list_all_purchase_returns', 'Api\Purchase\PurchaseController@ListAllPurchaseReturn');

//Production Report
Route::post('production/production_report/{type}', 'Api\Reports\ProductionReportController@Report');

//Purchase details report and  purchase preiview
Route::post('purchase_preview', 'Api\Purchase\PurchaseController@purchasePreview');

Route::post('purchase_void', 'Api\Purchase\PurchaseController@purchaseVoid');
Route::post('purchase_report/{type}', 'Api\Reports\PurchaseReportController@Report');
Route::post('purchase_summary_report', 'Api\Reports\PurchaseController@purchaseSummary');
Route::post('update_purch_note', 'Api\Purchase\PurchaseController@updatePurchNote');
Route::post('purchase/item_sum', 'Api\Purchase\PurchaseController@item_sum');
Route::post('get_purch_num', 'Api\Purchase\PurchaseController@get_purch_num');
Route::post('get_purch_ret_num', 'Api\Purchase\PurchaseController@get_purch_ret_num');
Route::post('purchase/temp_purchase', 'Api\Purchase\PurchaseController@TempPurchase');
Route::post('purchase/list_tpurchases', 'Api\Purchase\PurchaseController@ListTempPurchase');
Route::post('open_temp_purchase', 'Api\Purchase\PurchaseController@TempPurchasePreview');
Route::post('removeDraft', 'Api\Purchase\PurchaseController@removeTempPurch');

//Van
Route::post('getNxtVanTranId', 'Api\Van\VanTransferController@getNxtVanTranId');
Route::post('vanline/{type}', 'Api\Van\VanlineController@vanOperations');
Route::post('van/{type}', 'Api\Van\VanController@vanOperations');
Route::post('van_transfer/search', 'Api\Van\VanTransferController@serachProduct');
Route::post('van_transfer', 'Api\Van\VanTransferController@addStocktoVan');
Route::post('van_transfer/return', 'Api\Van\VanTransferController@vanTreturn');
Route::post('van_transfer/void', 'Api\Van\VanTransferController@vanTvoid');
Route::post('van_transfer/report', 'Api\Reports\VanReportController@vanTransferReport');
Route::post('tran_preview', 'Api\Reports\VanReportController@vanTransferPreiview');
Route::post('tran_return_preview', 'Api\Reports\VanReportController@vanTransferReturnPreiview');
Route::post('van_transfer/invoice_report', 'Api\Reports\VanReportController@invoiceBasedReport');
Route::post('van_transfer/item_based_report', 'Api\Reports\VanReportController@vanTransferItemReport');
Route::post('van_transfer/item_report', 'Api\Reports\VanReportController@vanItemReport');

Route::post('vantran_void', 'Api\Van\VanTransferController@vanTranVoid');
Route::post('vantranreturn_void', 'Api\Van\VanTransferController@vanTranReturnVoid');
Route::post('van_transfer/by_period', 'Api\Reports\VanReportController@vanTransReportByPeriod');

Route::post('transfer_return', 'Api\Van\VanTransferController@returnvanTransfer');
Route::post('van_transfer/search_items', 'Api\Van\VanTransferController@listTransferInfo');
Route::post('getNxtVanRetTranId', 'Api\Van\VanTransferController@getNxtVanRetTranId');
Route::get('van/list_products', 'Api\Van\VanController@listAllVanProducts');
Route::post('van_transfer_return/report', 'Api\Reports\VanReportController@vanTransferReturnReport');

//Production
Route::post('production_formula/{type}', 'Api\Production\ProductionFormulaController@productionFormulaOperations');
Route::post('production/{type}', 'Api\Production\ProductionController@productionOperations');

// Sales
Route::post('sales_report/{type}', 'Api\Reports\SalesReportController@Report');
Route::post('list_leadgers', 'Api\Accounts\LedgerController@listLedger');
Route::post('show_suppliers', 'Api\Purchase\SupplierController@listSupplier');


// });

Route::post('customer/{type}', 'Api\Accounts\CustomerController@customerOperations');
Route::post('price_group/{type}', 'Api\Accounts\PriceGroupController@priceGroupOperations');

Route::post('clear_stock', 'Api\Stock\StocksController@clear_stock');
Route::post('next_production_id', 'Api\Production\ProductionController@geNextProdctnId');

Route::post('users_list', 'Api\Company\BranchController@usersList');
Route::post('user/activate', 'API\UserController@activateUser');

Route::post('branch_list', 'API\UserController@branchList');

Route::post('stock/{type}', 'Api\Stock\StockController@stock');

Route::post('get_branches_unit', 'Api\Company\BranchController@getBranchesUnit');

Route::get('get_stock_branches', 'Api\Company\BranchController@getStockBranches');
