<?php
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

// Route::get('/user', function (Request $request) {
//     $user = $request->user();
//     // Even with
//     $user = Auth::user();

//     return $user;
// });

Route::middleware(['dbroute'])->group(function () {
  
  Route::any('api/{type}', 'Van\Register\RegisterController@operations');

  Route::middleware(['vanlogin'])->group(function () {
    
    Route::post('user/{type}', 'Van\User\UserController@operations');

    Route::post('customer/{type}', 'Van\Customer\CustomerController@operations');
    Route::post('product/{type}', 'Van\Product\ProductController@operations');
    Route::post('unit/{type}', 'Van\Product\UnitController@operations');
    Route::post('product_unit/{type}', 'Van\Product\ProductUnitController@operations');
    Route::post('category/{type}', 'Van\Product\CategoryController@operations');
    Route::post('sub_category/{type}', 'Van\Product\SubCategoryController@operations');
    Route::post('manufacture/{type}', 'Van\Product\ManufactureController@operations');
    Route::post('sales_master/{type}', 'Van\Sales\SalesMasterController@operations');
    Route::post('sales_sub/{type}', 'Van\Sales\SalesSubController@operations');
    Route::post('sales_due/{type}', 'Van\Sales\SalesDueController@operations');
    Route::post('sales_due_sub/{type}', 'Van\Sales\SalesDueSubController@operations');
    Route::post('sales_return/{type}', 'Van\Sales\SalesReturnController@operations');
    Route::post('sales_return_sub/{type}', 'Van\Sales\SalesReturnSubController@operations');
    Route::post('common/{type}', 'Van\Common\CommonController@operations');

    Route::post('stock_batch/{type}', 'Van\Stock\StockBatchController@operations');
    Route::post('stock_unit_rate/{type}', 'Van\Stock\StockUnitRateController@operations');
    Route::post('van_daily_stock/{type}', 'Van\Stock\VanDailyStockController@operations');
    Route::post('van_stock/{type}', 'Van\Stock\VanStockController@operations');

    Route::post('van_transfer/{type}', 'Van\Transfer\VanTransferController@operations');
    Route::post('van_transfer_sub/{type}', 'Van\Transfer\VanTransferSubController@operations');
    Route::post('van_transfer_return/{type}', 'Van\Transfer\VanTransferReturnController@operations');
    Route::post('van_transfer_return_sub/{type}', 'Van\Transfer\VanTransferReturnSubController@operations');

    Route::any('receipt/{type}', 'Van\Accounts\ReceiptController@operations');
  });
});

// Route::group(['middleware' => 'vanlogin'], function(){
//   Route::post('user/login', 'Van\UserController@login');
// });
// Route::middleware(['vanlogin'])->group(function () {
//   Route::post('user/login', 'Van\UserController@login');
// });
// Route::middleware(['vanlogin', 'userLogin'])->group(function () {

// Route::post('api/{type}', 'Van\Register\RegisterController@operations');
// Route::post('price_group/{type}', 'Api\Accounts\PriceGroupController@priceGroupOperations');
