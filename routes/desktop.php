<?php
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

// Route::get('/user', function (Request $request) {
//     $user = $request->user();
//     // Even with
//     $user = Auth::user();

//     return $user;
// });

Route::middleware(['dbroute'])->group(function () {
  Route::any('api/{type}', 'Desktop\Register\RegisterController@operations');
  Route::any('test/{type}', 'Desktop\Common\CommonController@operations');

  Route::middleware(['desktopLogin'])->group(function () {
    
    Route::any('upload/{type}', 'Desktop\Common\CommonController@operations');
    Route::any('branch/{type}', 'Desktop\Common\BranchController@operations');
    Route::any('user/{type}', 'Desktop\User\UserController@operations');

    Route::any('customer/{type}', 'Desktop\Customer\CustomerController@operations');
    Route::any('product/{type}', 'Desktop\Product\ProductController@operations');
    Route::any('unit/{type}', 'Desktop\Product\UnitController@operations');
    Route::any('product_unit/{type}', 'Desktop\Product\ProductUnitController@operations');
    Route::any('category/{type}', 'Desktop\Product\CategoryController@operations');
    Route::any('sub_category/{type}', 'Desktop\Product\SubCategoryController@operations');
    Route::any('manufacture/{type}', 'Desktop\Product\ManufactureController@operations');
    Route::any('sales_master/{type}', 'Desktop\Sales\SalesMasterController@operations');
    Route::any('sales_sub/{type}', 'Desktop\Sales\SalesSubController@operations');
    Route::any('sales_due/{type}', 'Desktop\Sales\SalesDueController@operations');
    Route::any('sales_due_sub/{type}', 'Desktop\Sales\SalesDueSubController@operations');
    Route::any('sales_return/{type}', 'Desktop\Sales\SalesReturnController@operations');
    Route::any('sales_return_sub/{type}', 'Desktop\Sales\SalesReturnSubController@operations');
    Route::any('common/{type}', 'Desktop\Common\CommonController@operations');

    Route::any('company_stock/{type}', 'Desktop\Stock\CompanyStockController@operations');
    Route::any('branch_stock/{type}', 'Desktop\Stock\BranchStockController@operations');
    Route::any('branch_daily_stock/{type}', 'Desktop\Stock\BranchDailyStockController@operations');
    Route::any('branch_stock_log/{type}', 'Desktop\Stock\BranchStockLogController@operations');
    Route::any('branch_stock_unit_rates/{type}', 'Desktop\Stock\StockUnitRateController@operations');
    Route::any('stock_batch/{type}', 'Desktop\Stock\StockBatchController@operations');

    Route::any('ledger_group/{type}', 'Desktop\Accounts\LedgerGroupController@operations');
    Route::any('ledger/{type}', 'Desktop\Accounts\LedgerController@operations');
    Route::any('barcode_print/{type}', 'Desktop\Settings\BarcodePrintController@operations');
    Route::any('rate_codes/{type}', 'Desktop\Settings\RateCodesController@operations');
    Route::any('tax_category/{type}', 'Desktop\Tax\TaxCategoryController@operations');

    Route::any('godown_master/{type}', 'Desktop\Godown\GodownMasterController@operations');
    Route::any('godown_stock/{type}', 'Desktop\Godown\GodownStocksController@operations');
  });
});

// Route::group(['middleware' => 'vanlogin'], function(){
//   Route::post('user/login', 'Van\UserController@login');
// });
// Route::middleware(['vanlogin'])->group(function () {
//   Route::post('user/login', 'Van\UserController@login');
// });
// Route::middleware(['vanlogin', 'userLogin'])->group(function () {

// Route::post('api/{type}', 'Van\Register\RegisterController@operations');
// Route::post('price_group/{type}', 'Api\Accounts\PriceGroupController@priceGroupOperations');
