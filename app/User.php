<?php
namespace App;


use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{

    public $primaryKey = 'usr_id';

    public function getAuthPassword()
    {
        return $this->usr_password;
    }

    public function AauthAcessToken()
    {
        return $this->hasMany('\App\OauthAccessToken');
    }

    use HasApiTokens, Notifiable;
/**
 * The attributes that are mass assignable.
 *
 * @var array
 */
    protected $fillable = [
        'usr_type','usr_name', 'usr_username', 'usr_email','usr_nickname', 'pass_md5', 'usr_password','usr_mobile', 'usr_phone', 'usr_address','company_id','branch_id',
        'server_sync_flag',
        'server_sync_time',
        'local_sync_time',
        'usr_cash_ledger_id',
        'usr_default_godown_id'
    ];
/**
 * The attributes that should be hidden for arrays.
 *
 * @var array
 */

    protected $hidden = [
        'usr_password', 'remember_token','email_verified_at','created_at','updated_at'
    ];
    
    // protected $hidden = [
    //     'usr_password', 'remember_token','pass_md5','email_verified_at','server_sync_flag'
    //     ,'server_sync_time','local_sync_time','created_at','updated_at'
    // ];


     /**
     * Get the user type associated with the user.
     */
    public function usertype()
    {
        return $this->hasOne('App\Model\Userdetails\UserType','usr_type');
       // return $this->belongsTo('App\Model\Userdetails\UserType','usr_type','type_id');
    }

    public function base_unit()
    {
        return $this->belongsTo('App\Models\Product\Units','prd_base_unit_id','unit_id');
    }

    // public function sales()
    //   {
    //         return $this->hasMany('App\Models\Sales\SalesMaster','sales_added_by','usr_id');
    //   }
}
