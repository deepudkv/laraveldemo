<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{

	protected $primaryKey = 'feat_id';

	protected $fillable = [
		'feat_name', 'feat_description','company_id','branch_id',  'server_sync_flag',
        'server_sync_time',
        'local_sync_time'
	];

	protected $hidden = [
		'created_at', 'updated_at',
	];
	public function getFeatNameAttribute($value)
    {
        return strtoupper($value);
    }

}
