<?php

namespace App\Models\Product;


use Illuminate\Database\Eloquent\Model;

class Damage extends Model
{
    //

    protected $fillable = ['damage_stock_id','damage_prod_id','damage_qty','damage_rem_qty','damage_purch_rate','damage_notes','damage_serail','damage_date','damage_added_by','server_sync_time','local_sync_time','server_sync_flag','branch_id'];
}
