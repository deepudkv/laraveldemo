<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class ProdFeatures extends Model
{
	protected $table = 'feature_products';
	protected $primaryKey = 'fetprod_id';
	protected $fillable = [
		'fetprod_prod_id', 'fetprod_fet_id','fetprod_fet_val', 'company_id', 'branch_id',   'server_sync_flag',
        'server_sync_time',
        'local_sync_time'
	];

	protected $hidden = [
		'created_at', 'updated_at',
	];


	public function product()
    {
        return $this->belongsTo('App\Models\Product\product');
	}
	
}
