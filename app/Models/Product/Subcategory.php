<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
    protected $primaryKey = 'subcat_id';
    protected $fillable = [
        'subcat_id',
        'subcat_parent_category', 'subcat_name', 'subcat_remarks', 'subcat_pos', 'company_id', 'branch_id',
        'server_sync_time',
        'server_sync_flag',
        'local_sync_time'
    ];

//     public function category()
    // {
    //     return $this->belongsTo('App\Models\Product\Category');
    // }

    public function getSubcatNameAttribute($value)
    {
        return strtoupper($value);
    }

}
