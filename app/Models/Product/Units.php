<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class Units extends Model
{
    protected $primaryKey = 'unit_id';

    protected $fillable = [
        'unit_name', 'unit_code', 'unit_type', 'unit_base_id', 'unit_base_qty', 'unit_remarks', 'unit_display', 'company_id', 'branch_id',
        'server_sync_flag',
        'server_sync_time',
        'local_sync_time'
    ];
    protected $visible = ['unit_id', 'unit_name', 'unit_code', 'unit_type', 'unit_base_id', 'unit_base_qty', 'unit_remarks', 'unit_display', 'company_id', 'branch_id',
    'server_sync_flag',
    'server_sync_time',
    'local_sync_time'];

    public function getUnitNameAttribute($value)
    {
        return strtoupper($value);
    }

    // public function getUnitCodeAttribute($value)
    // {
    //     return strtoupper($value);
    // }

    // public function getUnitDisplayAttribute($value)
    // {
    //     return strtoupper($value);
    // }

    public function products()
    {
        return $this->hasMany('App\Models\Product\product');
    }

    public function children()
    {
        return $this->hasMany('App\Models\Product\Units', 'unit_base_id');
    }

}
