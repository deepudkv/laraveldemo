<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class ProdUnit extends Model
{

    protected $primaryKey = 'produnit_id';
    protected $fillable = [
        'produnit_prod_id', 
        'produnit_unit_id',
        'produnit_ean_barcode',
        'company_id',
        'branch_id',
        'server_sync_flag',
        'server_sync_time',
        'local_sync_time'
	];

	protected $hidden = [
		'created_at', 'updated_at',
	];


	public function product()
    {
        return $this->belongsTo('App\Models\Product\product');
	}

	public function unit_master()
    {
        
        return $this->hasOne('App\Models\Product\Units', 'unit_id', 'produnit_prod_id', 'produnit_unit_id');
    }

    
    public function units()
    {
        return $this->hasMany('App\Models\Product\Units','unit_id','produnit_unit_id');
	}
	
}
