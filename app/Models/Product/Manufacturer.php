<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class Manufacturer extends Model
{
    public $table = "manufacturer";
    //
    protected $fillable = ['manftr_comp_name', 'manftr_comp_code', 'manftr_comp_email',
        'manftr_comp_address', 'manftr_comp_phone', 'manftr_comp_mobile', 'manftr_comp_fax'
        , 'manftr_comp_contact_person', 'manftr_comp_website', 'manftr_comp_notes',
        'manftr_comp_add_date', 'manftr_comp_addedby', 'manftr_created_at', 'company_id', 'branch_id','server_sync_flag',
        'server_sync_time',
        'local_sync_time'];

    public function getManftrCompNameAttribute($value)
    {
        return strtoupper($value);
    }


     public function product()
    {
        return $this->hasMany('App\Models\Product\product','prd_supplier','id');
    }
}
