<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;
class product extends Model
{
    protected $primaryKey = 'prd_id';
    protected $fillable = [
        'prd_name', 'prd_cat_id', 'prd_sub_cat_id', 'prd_supplier', 'prd_alias', 'prd_short_name', 'prd_min_stock', 'prd_max_stock', 'prd_base_unit_id', 'prd_default_unit_id', 'prd_barcode', 'prd_added_by', 'company_id', 'branch_id',
        'prd_tax',
        'prd_tax_cat_id',
        'prd_type','prd_stock_status', 'prd_code', 'prd_ean','prd_tax_code', 'prd_exp_dur', 'prd_loyalty_rate', 'server_sync_time', 'prd_remarks', 'prd_desc', 'prd_minstock_alert', 'prd_maxstock_alert',
        'server_sync_flag',
        'server_sync_time',
        'local_sync_time',
    ];

    protected $hidden = [
        'prd_added_by','company_id','branch_id','updated_at','prd_flag'];
    // public function getPrdNameAttribute($value)
    // {
    //     return strtoupper($value);
    // }
    // public function getPrdAliasAttribute($value)
    // {
    //     return strtoupper($value);
    // }
    // public function getPrdShortNameAttribute($value)
    // {
    //     return strtoupper($value);
    // }

    public function getPrdCodeAttribute($value)
    {
        return strtoupper($value);
    }

    public function getPrdTaxCodeAttribute($value)
    {
        return strtoupper($value);
    }

    public function manufacturer()
    {
        return $this->belongsTo('App\Models\Product\Manufacturer','prd_supplier','id');
    }
    public function category()
    {
        return $this->belongsTo('App\Models\Product\Category','prd_cat_id','cat_id');
    }

    public function unit()
    {
        return $this->belongsTo('App\Models\Product\Units','prd_base_unit_id','unit_id');
    }
    
    public function sub_category()
    {
        return $this->belongsTo('App\Models\Product\Subcategory','prd_sub_cat_id','subcat_id');
	}
    public function units()
    {
        return $this->hasManyThrough(
            'App\Models\Product\Units',
            'App\Models\Product\ProdUnit',
            'produnit_prod_id',
            'unit_id',
            'prd_id',
            'produnit_unit_id'
        );
    }

    public function features()
    {
        return $this->hasManyThrough(
            'App\Models\Product\Feature',
            'App\Models\Product\ProdFeatures',
            'fetprod_prod_id',
            'feat_id',
            'prd_id',
            'fetprod_fet_id'
        );
    }

    public function branch_stocks()
    {
        return $this->hasManyThrough(
            'App\Models\Company\Acc_branch',
            'App\Models\Stocks\BranchStocks',
            'bs_prd_id',
            'branch_id',
            'prd_id',
            'bs_branch_id'
        );
    }


    public function batches()
    {
        return $this->hasMany('App\Models\Stocks\StockBatches','sb_prd_id',4);
	}
    public function base_unit()
    {
        return $this->belongsTo('App\Models\Product\Units','prd_base_unit_id','unit_id');
    }


}
