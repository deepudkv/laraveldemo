<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $primaryKey = 'cat_id';

    protected $fillable = [
        'cat_id',
        'cat_name','cat_remarks', 'cat_pos',
        'cat_tax_cat_id',
        'company_id','branch_id',
        'server_sync_flag',
        'server_sync_time',
        'local_sync_time'
        
    ];

	public function getCatNameAttribute($value)
    {
        return strtoupper($value);
    }

    public function sub_cat()
    {
       return $this->hasMany('App\Models\Product\Subcategory','subcat_parent_category');
    }
}
