<?php

namespace App\Models\Userdetails;

use Illuminate\Database\Eloquent\Model;

class UserType extends Model
{

	protected $primaryKey = 'type_id';

	protected $fillable = [
		'user_type'
	];

	protected $hidden = [
		'created_at', 'updated_at',
	];


}
