<?php

namespace App\Models\Damage;

use Illuminate\Database\Eloquent\Model;

class DaMage extends Model
{
    public $table = "damages";
    protected $primaryKey = 'damage_id';
 	 protected $guarded = [];
}