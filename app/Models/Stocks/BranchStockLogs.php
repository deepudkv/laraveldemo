<?php

namespace App\Models\Stocks;

use Illuminate\Database\Eloquent\Model;

class BranchStockLogs extends Model
{
    protected $primaryKey = 'bsl_log_id';

    protected $fillable = [
        'bsl_branch_stock_id','bsl_stock_id','bsl_prd_id','bsl_stock_quantity',
        'bsl_prate',
        'bsl_branch_id',
        'bsl_avg_prate','bsl_shop_quantity','bsl_gd_quantity',
        'bsl_srate',
        'bsl_revert',
        'server_sync_flag',
        'server_sync_time',
        'local_sync_time'
    ];


}
