<?php

namespace App\Models\Stocks;

use Illuminate\Database\Eloquent\Model;

class OpeningStockLog extends Model
{
    public $table = "opening_stock_log";

    protected $primaryKey = 'opstklog_id';

    protected $fillable = [
        'opstklog_branch_stock_id','opstklog_prd_id','opstklog_stock_id',
        'opstklog_date','opstklog_prate','opstklog_srate',
        'opstklog_stock_quantity_add','opstklog_stock_quantity_rem'
        ,'opstklog_type','opstklog_unit_id', 'server_sync_flag',
        'branch_id',
        'server_sync_time',
        'local_sync_time',
        'opstklog_gd_id'

    ];

}
