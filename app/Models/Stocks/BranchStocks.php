<?php

namespace App\Models\Stocks;

use Illuminate\Database\Eloquent\Model;

class BranchStocks extends Model
{
    protected $primaryKey = 'branch_stock_id';
    protected $fillable = [
        'branch_stock_id','bs_stock_id', 'bs_prd_id', 'bs_stock_quantity',
         'bs_stock_quantity_shop', 'bs_stock_quantity_gd', 'bs_prate','bs_avg_prate',
        'bs_srate','bs_branch_id','bs_batch_id','bs_expiry','bs_stk_status',
        'server_sync_flag',
        'server_sync_time',
        'local_sync_time'
    ];

    public function sales_sub()
    {
        return $this->hasMany('App\Models\Sales\SalesSub','salesub_branch_stock_id','branch_stock_id');
    }

    public function sales_return_sub()
    {
        return $this->hasMany('App\Models\Sales\SalesReturnSub','salesretsub_branch_stock_id','branch_stock_id');
    }




}

