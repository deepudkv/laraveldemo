<?php

namespace App\Models\Stocks;

use Illuminate\Database\Eloquent\Model;

class StockUnitRates extends Model
{
    protected $primaryKey = 'sur_id';
    protected $fillable = [
        'sur_id','branch_stock_id', 'sur_prd_id', 'sur_stock_id', 
        'sur_unit_id',
         'sur_unit_rate',
         'sur_branch_id'
        ,'server_sync_flag',
        'server_sync_time',
        'local_sync_time'
    ];

    public function getUnits()
    {
        return $this->belongsTo('App\Models\Product\Units','sur_unit_id','unit_id');
	}

   
}


