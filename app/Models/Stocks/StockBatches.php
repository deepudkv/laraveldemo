<?php

namespace App\Models\Stocks;

use Illuminate\Database\Eloquent\Model;

class StockBatches extends Model
{
    protected $primaryKey = 'sb_id';

    protected $fillable = [
        'sb_batch_code','sb_branch_stock_id','sb_prd_id','sb_stock_id',
        'sb_manufacture_date','sb_expiry_date',
        'sb_quantity',
        'branch_id',
        'server_sync_flag',
        'server_sync_time',
        'local_sync_time'
    ];

}