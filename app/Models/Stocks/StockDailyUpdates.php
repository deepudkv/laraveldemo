<?php

namespace App\Models\Stocks;

use Illuminate\Database\Eloquent\Model;

class StockDailyUpdates extends Model
{
    protected $primaryKey = 'sdu_id';

    protected $fillable = [
        'sdu_branch_stock_id', 'sdu_prd_id', 'sdu_stock_id', 'sdu_date',
         'sdu_stock_quantity',
         'sdu_godown_id',
         'branch_id',
        'server_sync_flag',
        'server_sync_time',
        'local_sync_time'

    ];

    public function prdData()
    {
        return $this->belongsTo('App\Models\Product\Product', 'sdu_prd_id', 'prd_id');
    }

    public function rateInfo()
    {
        return $this->belongsTo('App\Models\Stocks\BranchStocks', 'sdu_prd_id', 'bs_stock_id');
    }

}
