<?php

namespace App\Models\Stocks;

use Illuminate\Database\Eloquent\Model;

class   CompanyStocks extends Model
{
    protected $primaryKey = 'cmp_stock_id';
    protected $fillable = [
        'cmp_stock_id', 'cmp_prd_id', 'cmp_stock_quantity',
         'cmp_avg_prate', 
        'cmp_srate','server_sync_flag',
		'server_sync_time',	'local_sync_time'
    ];
    protected $guarded = [];
}
