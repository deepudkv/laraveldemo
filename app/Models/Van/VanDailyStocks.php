<?php

namespace App\Models\Van;

use Illuminate\Database\Eloquent\Model;

class VanDailyStocks extends Model
{
    public $table = "van_daily_stocks";

    protected $primaryKey = 'vds_id';

    protected $fillable = [
        'vds_branch_stock_id', 'vds_prd_id','vds_stock_id','vds_van_id',
        'vds_date','vds_stock_quantity','branch_id','server_sync_flag',
        'server_sync_time',
        'local_sync_time',];

	// protected $hidden = [
	// 	'created_at', 'updated_at',
	// ];
}
