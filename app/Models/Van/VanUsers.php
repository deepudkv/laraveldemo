<?php

namespace App\Models\Van;

use Illuminate\Database\Eloquent\Model;

class VanUsers extends Model
{
    public $table = "van_users";

    protected $fillable = [
        'van_id',
        'user_id',
        'server_sync_flag',
        'server_sync_time',
        'local_sync_time',
    ];

    protected $guarded = [];

    protected $hidden = [
        'created_at', 'updated_at'
    ];
    
}
