<?php

namespace App\Models\Van;

use Illuminate\Database\Eloquent\Model;

class VanTransferReturn extends Model
{
    public $table = "van_transfer_return";

    protected $primaryKey = 'vantranret_id';

	protected $fillable = ['vantranret_vantran_id','vantranret_van_id','vantranret_godown_id',
        'vantranret_price',
		'vantranret_purch_price', 'vantranret_date','vantranret_notes','vantranret_van_ledger_id',
		'vantranret_added_by','vantranret_type','vantranret_flag','server_sync_flag',
		'server_sync_time',
		'local_sync_time',
		'branch_id'
	];

	protected $hidden = [
		'created_at', 'updated_at',
	];

	
	public function van()
    {
        return $this->hasMany('App\Models\Van\Van','van_id','vantranret_van_id');
	}
	
	public function items()
	{
	   return $this->hasMany('App\Models\Van\VanTransferReturnSub','vantranretsub_vantranret_id','vantranret_id');

	}

	public function gd()
	{
	   return $this->hasMany('App\Models\Godown\GodownMaster','gd_id','vantranret_godown_id');

	}


}
