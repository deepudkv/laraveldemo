<?php

namespace App\Models\Van;

use Illuminate\Database\Eloquent\Model;

class VanTransfer extends Model
{
    public $table = "van_transfer";

    protected $primaryKey = 'vantran_id';

    protected $fillable = ['vantran_van_id','vantran_price','vantran_purch_price','vantran_date',
		'vantran_notes', 'vantran_van_ledger_id','vantran_added_by','vantran_type',
		'vantran_flags','branch_id','server_sync_flag','vantran_godown_id',
		'server_sync_time',
		'local_sync_time',
	];

		
	public function van()
    {
        return $this->hasMany('App\Models\Van\Van','van_id','vantran_van_id');
	}
	
	public function items()
	{
	   return $this->hasMany('App\Models\Van\VanTransferSub','vantransub_vantran_id','vantran_id');

	}

	public function gd()
	{
	   return $this->hasMany('App\Models\Godown\GodownMaster','gd_id','vantran_godown_id');

	}

	public function branch()
	{
	   return $this->hasOne('App\Models\Company\Acc_branch','branch_id','branch_id');

	}


}
