<?php

namespace App\Models\Van;

use Illuminate\Database\Eloquent\Model;

class VanStock extends Model
{
    public $table = "van_stock";

    protected $primaryKey = 'vanstock_id';

    protected $fillable = ['vanstock_prod_id', 'vanstock_stock_id', 'vanstock_branch_stock_id', 'vanstock_van_id',
        'vantran_godown_id', 'vanstock_qty', 'vanstock_last_tran_rate', 'vanstock_avg_tran_rate',
        'vanstock_flags', 'branch_id','server_sync_flag',
        'server_sync_time',
        'local_sync_time',
    ];
    protected $hidden = [
        'created_at', 'updated_at',
    ];
}
