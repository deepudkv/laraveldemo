<?php

namespace App\Models\Van;

use Illuminate\Database\Eloquent\Model;

class VanCategories extends Model
{
    public $table = "van_categories";

    protected $primaryKey = 'vc_id';

    protected $fillable = [
        'vc_van_id', 'vc_cat_id','vc_cat_flag','vc_added_by',
        'branch_id','server_sync_flag',
        'server_sync_time',
        'local_sync_time'];

	protected $hidden = [
		'created_at', 'updated_at',
	];
}
