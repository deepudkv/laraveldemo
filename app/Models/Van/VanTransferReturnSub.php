<?php

namespace App\Models\Van;

use Illuminate\Database\Eloquent\Model;

class VanTransferReturnSub extends Model
{
    public $table = "van_transfer_return_sub";

    protected $primaryKey = 'vantranretsub_id';

	protected $fillable = ['vantranretsub_id','vantransub_vantran_id','vantranretsub_vantran_id',
	    'vantranretsub_vantranret_id',
		'vantranretsub_van_id', 'vantranretsub_godown_id','vantranretsub_rate','vantranretsub_purch_rate',
		'vantranretsub_qty','vantranretsub_prod_id','vantranretsub_stock_id',
		'vantransub_branch_stock_id','vantranretsub_unit_id','vantranretsub_date','vantranretsub_added_by',
		'vantranretsub_flags','server_sync_flag',
		'server_sync_time',
		'local_sync_time',
		'branch_id'
	];

	protected $hidden = [
		'created_at', 'updated_at',
	];
   	
	public function product()
	{
	   return $this->hasMany('App\Models\Product\Product','prd_id','vantransub_prod_id');

	}
	public function van()
    {
        return $this->belongsTo('App\Models\Van\Van','van_id','vantransub_van_id');
	}
	
}
