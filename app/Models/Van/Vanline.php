<?php

namespace App\Models\Van;

use Illuminate\Database\Eloquent\Model;

class Vanline extends Model
{
    public $table = "vanline";

    protected $primaryKey = 'vanline_id';

    protected $fillable = [
		'vanline_name', 'vanline_desc','branch_id','server_sync_time','server_sync_flag',
		'local_sync_time',];

	protected $hidden = [
		'created_at', 'updated_at',
	];

}
