<?php

namespace App\Models\Van;

use Illuminate\Database\Eloquent\Model;

class VanTransferSub extends Model
{
    public $table = "van_transfer_sub";

    protected $primaryKey = 'vantransub_id';

    protected $fillable = ['vantransub_id','vantransub_vantran_id','vantransub_van_id','vantransub_godown_id',
		'vantransub_rate', 'vantransub_purch_rate','vantransub_qty','vantransub_prod_id',
		'vantransub_stock_id','vantransub_branch_stock_id','vantransub_unit_id','vantransub_date',
		'vantransub_added_by','vantransub_flags','branch_id','server_sync_flag',
		'server_sync_time',
		'local_sync_time',
	];

	// protected $hidden = [
	// 	'created_at', 'updated_at',
	// ];
   	
	public function product()
	{
	   return $this->hasMany('App\Models\Product\Product','prd_id','vantransub_prod_id');

	}
	public function van()
    {
        return $this->hasMany('App\Models\Van\Van','van_id','vantransub_van_id');
	}
	
}