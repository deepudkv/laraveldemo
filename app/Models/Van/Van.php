<?php

namespace App\Models\Van;

use Illuminate\Database\Eloquent\Model;

class Van extends Model
{
    public $table = "van";

    protected $primaryKey = 'van_id';

    protected $fillable = ['van_reg_no','van_vanline_id','van_ledger_id','van_type',
		'van_code', 'van_contact_address','van_contact_email','van_contact_mob',
        'van_contact_person','van_contact_phone','van_desc','van_name','van_password',
        'server_sync_flag',
        'server_sync_time',
        'local_sync_time',
        'branch_id','all_cat',
        'van_cash_ledger_id',
        'van_op_bal'
	];

	protected $hidden = [
		'created_at', 'updated_at',
	];
    public function ledger()
    {
        return $this->belongsTo('App\Models\Accounts\Acc_ledger','van_ledger_id','ledger_id');
	}
	public function vanlines()
    {
        return $this->belongsTo('App\Models\Van\Vanline','van_vanline_id','vanline_id');
    }
    public function vanusers()
    {
        return $this->hasManyThrough(
            'App\User',
            'App\Models\Van\VanUsers',
            'van_id',
            'usr_id',
            'van_id',
            'user_id'
        );
    }

    public function vanCatgory()
    {
        return $this->hasMany('App\Models\Van\VanCategories','vc_van_id','van_id');
    }
}
