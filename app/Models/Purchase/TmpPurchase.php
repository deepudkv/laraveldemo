<?php


namespace App\Models\Purchase;

use Illuminate\Database\Eloquent\Model;

class TmpPurchase extends Model
{

	protected $primaryKey = 'purch_id';
	protected $fillable = ['purch_no',
	'purch_branch_id',
	'purch_id2',
	'purch_inv_no',
	'purch_supp_id',
	'purch_inv_date',
	'purch_date',
	'purch_type',
	'purch_pay_type',
	'purch_note',
	'purch_amount',
	'purch_tax',
	'purch_tax2','purch_tax3','purch_agent','branch_id',
	'purch_discount',
	'purch_frieght',	
	'purch_tax_ledger_id',
	'purch_ord_no',
	 'purch_flag',
	'temp_purch_json'];


	public function supplier()
    {
        return $this->belongsTo('App\Models\Purchase\Supplier','purch_supp_id','supp_id');
	}
	
	public function items()
	{
	   return $this->hasMany('App\Models\Purchase\PurchaseSub','purchsub_purch_id','purch_id');

	}
}
