<?php

namespace App\Models\Purchase;

use Illuminate\Database\Eloquent\Model;

class TmpPurchaseSub extends Model
{
    protected $primaryKey = 'purchsub_id';
    protected $fillable = [
    'purchsub_purch_id',
    'purchsub_prd_id',
    'purchsub_stock_id','purchsub_branch_id','purchsub_batch_id','purchsub_expiry',
    'purchsub_qty','purchsub_tax','purchsub_tax_per','purchsub_tax2','purchsub_tax2_per',
    'purchsub_gd_id','purchsub_tax3','purchsub_tax3_per','purchsub_gd_qty',
    'purchsub_frieght','purchsub_date','purchsub_flag','purchsub_flag',
    'purchsub_rate',
    'purchsub_unit_id','purchsub_branch_id'];
}


