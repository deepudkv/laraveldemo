<?php


namespace App\Models\Purchase;

use Illuminate\Database\Eloquent\Model;

class PurchaseReturn extends Model
{
	public $table = "purchase_return";

	protected $primaryKey = 'purchret_id';
	
	protected $guarded = [];
	
	public function supplier()
    {
        return $this->belongsTo('App\Models\Purchase\Supplier','purchret_supp_id','supp_id');
	}
	
	public function ledger()
    {

		return $this->belongsTo('App\Models\Purchase\Supplier','purchret_supp_id','supp_id');
		//return $this->belongsTo('App\Models\Accounts\Acc_ledger','purchret_supp_id','ledger_id');
	}


	public function items()
	{
	   return $this->hasMany('App\Models\Purchase\PurchaseReturnSub','purchretsub_purch_id','purchret_id')
	   ->join('products','products.prd_id','=','purchase_return_sub.purchretsub_prd_id')
	   ->join('units','units.unit_id','=','products.prd_base_unit_id');

	}
}
