<?php

namespace App\Models\Purchase;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{

    protected $primaryKey = 'supp_id';

    protected $fillable = ['supp_name', 'supp_alias', 'supp_code', 'supp_address1', 'supp_address2',
        'supp_zip', 'supp_city', 'supp_state', 'supp_country', 'supp_phone',
        'supp_mob', 'supp_email', 'supp_fax', 'supp_notes', 'supp_contact',
        'supp_tin', 'supp_due', 'company_id', 'branch_id', 'supp_ledger_id',
         'supp_branch_id', 'server_sync_time','opening_balance'
    ];

    protected $guarded = [];

    protected $hidden = [
        'created_at', 'updated_at',
    ];

}
