<?php

namespace App\Models\Purchase;

use Illuminate\Database\Eloquent\Model;

class PurchaseReturnSub extends Model
{
    public $table = "purchase_return_sub";

    protected $primaryKey = 'purchretsub_id';

    protected $guarded = [];

    public function product()
    {
        return $this->belongsTo('App\Models\Product\Product', 'purchretsub_purch_id', 'prd_id');
    }

}
