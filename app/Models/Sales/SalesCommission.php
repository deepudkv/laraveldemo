<?php

namespace App\Models\Sales;

use Illuminate\Database\Eloquent\Model;

class SalesCommission extends Model
{
    public $table = "sales_commission";

    protected $fillable = [
        'salescomm_sales_inv_no',
        'salescomm_ledger_id',
        'salescomm_date',
        'salescomm_amount',
        'salescomm_sales_amount',
        'salescomm_added_by',
        'salescomm_flags',
        'branch_id',
        'is_web',
        'godown_id',
        'van_id',
        'server_sync_flag',
        'server_sync_time',
        'local_sync_time',
    ];
    
    protected $guarded = [];
    
    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
