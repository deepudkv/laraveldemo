<?php

namespace App\Models\Sales;

use Illuminate\Database\Eloquent\Model;

class SalesMaster extends Model
{
    public $table = "sales_master";

    protected $primaryKey = 'sales_id';

    protected $fillable = [
        'sales_id',
        'sales_inv_no',
        'sales_cust_type',
        'sales_cust_id',
        'sales_cust_name',
        'sales_cust_ph',
        'sales_cust_address',
        'sales_cust_tin',
        'sales_timestamp',
        'sales_date',
        'sales_time',
        'sales_datetime',
        'sales_total',
        'sales_discount',
        'sales_disc_promo',
        'sales_disc_loyalty',
        'sales_paid',
        'sales_balance',
        'sales_profit',
        'sales_rec_amount',
        'sales_tax',
        'sales_added_by',
        'sales_flags',
        'sales_notes',
        'sales_pay_type',
        'sales_branch_inv',
        'sales_branch_inv2',
        'branch_id',
        'is_web',
        'godown_id',
        'van_id',
        'sales_agent_ledger_id',
        'sales_acc_ledger_id',
        'sales_order_no',
        'sales_order_type',
        'sales_godownsale_id',
        'server_sync_flag',
        'server_sync_time',
        'local_sync_time',
        'sync_completed',
        'sync_code',
        'sales_van_inv',
        'sales_tax_type',
        'sales_cust_ledger_id',
        'sales_due_date'
    ];

    protected $guarded = [];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function customer()
    {
        return $this->belongsTo('App\Models\Accounts\Acc_customer','sales_cust_id','cust_id');
    }
    
    public function sales_sub()
    {
        return $this->hasMany('App\Models\Sales\SalesSub','salesub_sales_inv_no','sales_inv_no');
    }

    public function item_disc()
    {
        return $this->hasOne('App\Models\Sales\SalesSub','salesub_sales_inv_no','sales_inv_no');
    }

    public function sales_sub_and_prod()
    {
        return $this->hasMany('App\Models\Sales\SalesSub','salesub_sales_inv_no','sales_inv_no')
        ->join('products', 'products.prd_id', '=', 'sale_sub.salesub_prod_id');
    }
    public function user()
    {
        return $this->hasOne('App\User','usr_id','sales_added_by');
    }

}
