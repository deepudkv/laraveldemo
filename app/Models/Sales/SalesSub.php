<?php

namespace App\Models\Sales;

use Illuminate\Database\Eloquent\Model;

class SalesSub extends Model
{
    public $table = "sale_sub";

    protected $primaryKey = 'salesub_id';

    protected $fillable = [
      'salesub_id',
      'salesub_sales_inv_no',
      'branch_inv_no',
      'branch_inv_no2',
      'salesub_stock_id',
      'salesub_branch_stock_id',
      'salesub_prod_id',
      'salesub_rate',
      'salesub_qty',
      'salesub_rem_qty',
      'salesub_discount',
      'salesub_timestamp',
      'salesub_date',
      'salesub_serial',
      'salesub_flags',
      'salesub_profit',
      'salesub_unit_id',
      'salesub_prate',
      'salesub_promo_id',
      'salesub_promo_per',
      'salesub_promo_rate',
      'salesub_tax_per',
      'salesub_taxcat_id',
      'salesub_tax_rate',
      'branch_id',
      'is_web',
      'godown_id',
      'van_id',
      'server_sync_flag',
      'server_sync_time',
      'local_sync_time',
      'sync_completed',
      'salesub_notes',
      'sync_code',
      'salesub_added_by',
      'salesub_id2',
      'van_inv_no'
];

protected $guarded = [];

protected $hidden = [
      'created_at', 'updated_at'
];

      public function sales()
      {
            return $this->belongsTo('App\Models\Sales\SalesMaster','sales_inv_no','salesub_sales_inv_no');
      }
}
