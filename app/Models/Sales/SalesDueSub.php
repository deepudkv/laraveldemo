<?php

namespace App\Models\Sales;

use Illuminate\Database\Eloquent\Model;

class SalesDueSub extends Model
{
    public $table = "sales_due_sub";

    protected $primaryKey = 'salesduesub_id';

    protected $fillable = [
          'salesduesub_id',
          'salesduesub_salesdue_id',
          'salesduesub_inv_no',
          'salesduesub_ledger_id',
          'salesduesub_date',
          'salesduesub_type',
          'salesduesub_in',
          'salesduesub_out',
          'salesduesub_inv_amount',
          'salesduesub_inv_balance',
          'salesduesub_added_by',
          'salesduesub_flags',
          'salesduesub_agent_ledger_id',
          'branch_id',
          'is_web',
          'godown_id',
          'van_id',
          'server_sync_flag',
          'server_sync_time',
          'local_sync_time',
          'sync_completed',
          'sync_code',
          'van_inv_no',
          'salesduesub_id2',
          'branch_inv_no',
          'branch_inv_no2',
          'rec_no',
          'van_rec_no',
          'branch_rec_no',
          'salesduesub_ret_no',
          'salesduesub_van_ret_no'
    ];
    
    protected $guarded = [];
    
    protected $hidden = [
          'created_at', 'updated_at'
    ];
    public function user()
    {
        return $this->hasOne('App\User','usr_id','salesduesub_added_by');
    }
    public function agent()
    {
       // return $this->hasOne('App\Models\Accounts\Acc_ledger','salesduesub_agent_ledger_id','ledger_id');
        return $this->hasOne('App\Models\Accounts\Acc_ledger','ledger_id','salesduesub_agent_ledger_id');
    }
    
    public function invoice()
    {
        return $this->hasOne('App\Models\Sales\SalesMaster','sales_inv_no','salesduesub_inv_no');
    }

    public function ledger()
    {
      return $this->hasOne('App\Models\Accounts\Acc_ledger','ledger_id','salesduesub_ledger_id');
    }
    
}
