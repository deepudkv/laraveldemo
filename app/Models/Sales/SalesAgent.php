<?php

namespace App\Models\Sales;

use Illuminate\Database\Eloquent\Model;

class SalesAgent extends Model
{
    public $table = "sales_agent";

    protected $fillable = [
        'sagent_ledger_id',
        'sagent_notes',
        'sagent_added_by',
        'sagent_flags',
        'branch_id',
        'is_web',
        'godown_id',
        'van_id',
        'server_sync_flag',
        'server_sync_time',
        'local_sync_time',
    ];

    protected $guarded = [];

    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
