<?php

namespace App\Models\Sales;

use Illuminate\Database\Eloquent\Model;

class SalesReturnMaster extends Model
{
    public $table = "sales_return_master";

    protected $primaryKey = 'salesret_id';

    protected $fillable = [
        'salesret_id',
        'salesret_no',
        'salesret_sales_inv_no',
        'salesret_date',
        'salesret_time',
        'salesdue_timestamp',
        'salesdue_ledger_id',
        'salesdue_amount',
        'salesret_cust_type',
        'salesret_cust_id',
        'salesret_cust_name',
        'salesret_cust_address',
        'salesret_cust_ph',
        'salesret_amount',
        'salesret_discount',
        'salesret_pay_type',
        'salesret_added_by',
        'salesret_flags',
        'salesret_vch_no',
        'salesret_notes',
        'salesret_sales_amount',
        'salesret_sales_discount',
        'salesret_service_amount',
        'salesret_service_discount',
        'salesret_servoid_id',
        'salesret_misc_amount',
        'salesret_cust_tin',
        'salesret_tax',
        'salesret_service_tax',
        'salesret_tax_vch_no',
        'salesret_godown_id',
        'salesret_agent_ledger_id',
        'salesret_pay_type2',
        'salesret_acc_ledger_id',
        'salesret_datetime',
        'salesret_godown_ret',
        'branch_id',
        'is_web',
        'godown_id',
        'van_id',
        'server_sync_flag',
        'server_sync_time',
        'local_sync_time',
        'sync_completed',
        'sync_code',
        'salesret_van_ret_no',
        'salesret_sales_van_inv_no',
        'salesret_sales_branch_inv_no',
        'salesret_cust_ledger_id',
        'salesret_branch_ret_no'
    ];

    protected $guarded = [];

    protected $hidden = [
        'created_at', 'updated_at'
    ];



    public function customer()
    {
        return $this->belongsTo('App\Models\Accounts\Acc_customer','salesret_cust_id','cust_id');
    }
    

    public function items()
    {
        return $this->hasMany('App\Models\Sales\SalesReturnSub','salesretsub_salesret_no','salesret_no');
    }

    public function sales_sub()
    {
        return $this->hasMany('App\Models\Sales\SalesReturnSub','salesretsub_salesret_no','salesret_no');
    }
}
