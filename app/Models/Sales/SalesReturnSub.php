<?php

namespace App\Models\Sales;

use Illuminate\Database\Eloquent\Model;

class SalesReturnSub extends Model
{
    public $table = "sales_return_sub";
    
    protected $primaryKey = 'salesretsub_id';

    protected $fillable = [
        'salesretsub_id',
        'salesretsub_branch_stock_id',
        'salesretsub_salesret_no',
        'salesretsub_sales_inv_no',
        'salesretsub_salesub_id',
        'salesretsub_stock_id',
        'salesretsub_prod_id',
        'salesretsub_rate',
        'salesretsub_qty',
        'salesretsub_date',
        'salesretsub_flags',
        'salesretsub_unit_id',
        'salesretsub_tax_per',
        'salesretsub_tax_rate',
        'salesretsub_godown_id',
        'salesretsub_server_sync',
        'salesretsub_sync_timestamp',
        'salesretsub_server_timestamp',
        'branch_id',
        'is_web',
        'godown_id',
        'van_id',
        'server_sync_flag',
        'server_sync_time',
        'local_sync_time',
        'sync_completed',
        'sync_code',
        'salesretsub_van_ret_no',
        'salesretsub_taxcat_id',
        'salesretsub_id2',
        'salesretsub_sales_van_inv_no'
    ];

    protected $guarded = [];

    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
