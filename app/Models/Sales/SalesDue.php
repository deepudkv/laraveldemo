<?php

namespace App\Models\Sales;

use Illuminate\Database\Eloquent\Model;

class SalesDue extends Model
{
    public $table = "sales_due";

    protected $primaryKey = 'salesub_id';

    protected $fillable = [
        'salesub_id',
        'sales_recp_no',
        'salesdue_vch_no',
        'salesdue_date',
        'salesdue_timestamp',
        'salesdue_ledger_id',
        'salesdue_amount',
        'salesdue_multi',
        'salesdue_added_by',
        'salesdue_flags',
        'salesdue_agent_ledger_id',
        'sales_pay_type',
        'sales_pay_txn_id',
        'branch_id',
        'is_web',
        'godown_id',
        'van_id',
        'server_sync_flag',
        'server_sync_time',
        'local_sync_time',
        'sync_completed'
    ];

    protected $guarded = [];

    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
