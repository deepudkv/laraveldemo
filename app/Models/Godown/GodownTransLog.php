<?php

namespace App\Models\Godown;

use Illuminate\Database\Eloquent\Model;

class GodownTransLog extends Model
{
       protected $primaryKey = 'gt_id';
   
    protected $fillable = [
		'gtl_gdtrans_id', 'gtl_prod_id','gtl_branch_stock_id','gtl_unit_id','gtl_trans_qty',
		'gtl_rate','gtl_trans_date','gtl_added_by','gtl_flag','branch_id','server_sync_flag',
		'server_sync_time','local_sync_time'
	];

	protected $hidden = [
		'created_at', 'updated_at',
	];
}
