<?php

namespace App\Models\Godown;

use Illuminate\Database\Eloquent\Model;

class GodownMaster extends Model
{
    public $table = "godown_master";

    protected $primaryKey = 'gd_id';

    protected $fillable = [
		'gd_name', 'gd_code','gd_cntct_num','gd_cntct_person','gd_address','gd_desc',
		'gd_branch_id','branch_id',
		'server_sync_flag',
		'server_sync_time',
		'local_sync_time'
	];

	protected $hidden = [
		'created_at', 'updated_at',
	];

}
