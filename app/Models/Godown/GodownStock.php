<?php

namespace App\Models\Godown;

use Illuminate\Database\Eloquent\Model;

class GodownStock extends Model
{
     protected $primaryKey = 'gs_id';

    protected $fillable = [
		'gs_godown_id', 'gs_branch_stock_id','gs_stock_id','gs_prd_id','gs_qty','gs_date','branch_id',
		'server_sync_flag',
		'server_sync_time',
		'local_sync_time'
	];

	protected $hidden = [
		'created_at', 'updated_at',
	];
}
