<?php

namespace App\Models\Godown;

use Illuminate\Database\Eloquent\Model;

class GodownTransfer extends Model
{
     protected $primaryKey = 'gdtrn_id';
    protected $table = "godown_transfer_master";

    protected $fillable = [
		'gdtrn_inv_id', 'gdtrn_from','gdtrn_to','gdtrn_date','gdtrn_time',
		'gdtrn_datetime','gdtrn_notes','gdtrn_added_by','gdtrn_flags','branch_id','server_sync_time',
		'server_sync_flag','local_sync_time'
	];

	protected $hidden = [
		'created_at', 'updated_at',
	];


	// public function transfers()
    // {
    //     return $this->hasMany('App\Models\Godown\GodownStockLog','gsl_tran_inv_id','gdtrn_inv_id');
	// }
	
	public function transfers()
    {
        return $this->hasMany('App\Models\Godown\GodownStockLog','gsl_tran_inv_id','gdtrn_inv_id');
    }

}
