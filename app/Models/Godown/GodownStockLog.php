<?php

namespace App\Models\Godown;

use Illuminate\Database\Eloquent\Model;

class GodownStockLog extends Model
{
    protected $primaryKey = 'gsl_id';
    protected $table = "godown_stock_log";

    protected $fillable = [
		'gsl_godown_id', 'gsl_tran_vanid','gsl_branch_stock_id','gsl_gdwn_stock_id',
		'gsl_stock_id','gsl_prd_id','gsl_prod_unit','gsl_qty','gsl_from','gsl_to',
		'gsl_vchr_type','gsl_date','gsl_added_by','branch_id','gsl_tran_inv_id',
		'server_sync_flag',
		'server_sync_time',
		'local_sync_time'
	];

	protected $hidden = [
		'created_at', 'updated_at',
	];

	// public function godownMaster()
    //   {
    //         return $this->belongsTo('App\Models\Godown\GodownTransfer','gdtrn_inv_id','gsl_tran_inv_id');
	//   }
	  
	  public function master()
      {
            return $this->belongsTo('App\Models\Godown\GodownTransfer','gdtrn_inv_id','gsl_tran_inv_id');
      }
}
