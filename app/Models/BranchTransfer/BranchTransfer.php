<?php

namespace App\Models\BranchTransfer;

use Illuminate\Database\Eloquent\Model;

class BranchTransfer extends Model
{
    public $table = "stock_transfer";

    protected $primaryKey = 'stocktr_id';

	protected $guarded = [];

	protected $hidden = [
		'created_at', 'updated_at',
	];


	
	public function items()
	{
	   return $this->hasMany('App\Models\BranchTransfer\BranchTransferSub','stocktrsub_stocktr_id','stocktr_id');

	}
	public function frombranch()
	{
	   return $this->hasOne('App\Models\Company\Acc_branch','branch_id','branch_id');

	}


	public function branch()
	{
	   return $this->hasOne('App\Models\Company\Acc_branch','branch_id','stocktr_to');

	}


}
