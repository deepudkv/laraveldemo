<?php

namespace App\Models\BranchTransfer;

use Illuminate\Database\Eloquent\Model;

class BranchTransferSub extends Model
{
    public $table = "stock_transfer_sub";

    protected $primaryKey = 'stocktrsub_id';

	protected $guarded = [];

	// protected $hidden = [
	// 	'created_at', 'updated_at',
	// ];
   	
	public function product()
	{
	   return $this->hasMany('App\Models\Product\Product','prd_id','vantransub_prod_id');

	}
	public function van()
    {
        return $this->hasMany('App\Models\Van\Van','van_id','vantransub_van_id');
	}
	
}