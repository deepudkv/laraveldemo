<?php

namespace App\Models\Accounts;

use Illuminate\Database\Eloquent\Model;

class Acc_ledger extends Model
{

protected $primaryKey = 'ledger_id';

protected $guarded = [];

protected $hidden = [
		'created_at', 'updated_at',
	];

	public function balance()
	{
			return $this->hasMany('App\Models\Accounts\Acc_voucher', 'vch_ledger_to', 'ledger_id');
	}
}
