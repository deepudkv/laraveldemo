<?php

namespace App\Models\Accounts;

use Illuminate\Database\Eloquent\Model;

class AccPaymentSub extends Model
{

      public $table = "acc_payment_sub";

      protected $fillable = [
            'paysub_id',
            'paysub_pay_no',
            'paysub_date',
            'paysub_time',
            'paysub_datetime',
            'paysub_timestamp',
            'paysub_acc_ledger_id',
            'paysub_ledger_id',
            'paysub_amount',
            'paysub_narration',
            'paysub_vat_per',
            'paysub_vat_amt',
            'branch_id',
            'van_id',
            'server_sync_flag',
            'server_sync_time',
            'local_sync_time',
            'paysub_flags',
            'van_pay_no',
            'branch_pay_no',
            'vat_catgeory_id',
            'paysub_ttl_amount',
            'paysub_vat_inc',
            'paysub_no'
      ];

      protected $guarded = [];

      protected $hidden = [
            'created_at', 'updated_at'
      ];
    
}
