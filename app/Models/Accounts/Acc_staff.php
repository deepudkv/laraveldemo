<?php

namespace App\Models\Accounts;

use Illuminate\Database\Eloquent\Model;

class Acc_staff extends Model
{
public $table = "staff";
protected $primaryKey = 'staff_id';

protected $guarded = [];

protected $hidden = [
		'created_at', 'updated_at',
	];

}
