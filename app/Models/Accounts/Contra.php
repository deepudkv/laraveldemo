<?php

namespace App\Models\Accounts;

use Illuminate\Database\Eloquent\Model;

class Contra extends Model
{
    public $table = "contra";
    
    protected $primaryKey = 'cont_id';

    protected $fillable = [
          'cont_id',
          'cont_ref_no',
          'cont_type',
          'cont_date',
          'cont_time',
          'cont_datetime',
          'cont_timestamp',
          'cont_from_ledger_id',
          'cont_to_ledger_id',
          'cont_amount',
          'cont_note',
          'branch_cont_no',
          'branch_id',
          'branch_cont_no',
          'branch_cont_no2',
          'server_sync_flag',
          'server_sync_time',
          'local_sync_time',
          
          'is_web',
          'cont_flags',
          'sync_code',
          'sync_completed'
    ];
    
    protected $guarded = [];
    
    protected $hidden = [
          'created_at', 'updated_at'
    ];
}
