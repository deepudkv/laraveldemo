<?php

namespace App\Models\Accounts;

use Illuminate\Database\Eloquent\Model;

class AccPayment extends Model
{

      public $table = "acc_payment";

      protected $fillable = [
            'pay_id',
            'pay_no',
            'pay_date',
            'pay_time',
            'pay_datetime',
            'pay_timestamp',
            'pay_acc_ledger_id',
            'pay_ttl_amount',
            'pay_note',
            'branch_id',
            'van_id',
            'server_sync_flag',
            'server_sync_time',
            'local_sync_time',
            'pay_flags',
            'is_web',
            'van_pay_no',
            'branch_pay_no',
            'branch_pay_no2',
            'pay_vat_amount',
            'pay_gttl_amount',
            'pay_vchtype_id',
            'pay_interbranch_status',
            'pay_inter_branch_id',
            'is_inter_branch',
            'added_by'

      ];

      protected $guarded = [];

      protected $hidden = [
            'created_at', 'updated_at'
      ];


      public function particulars()
    {
        return $this->hasMany('App\Models\Accounts\AccPaymentSub','paysub_pay_no','pay_no');
    }

}
