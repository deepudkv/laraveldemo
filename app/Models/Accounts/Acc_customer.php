<?php

namespace App\Models\Accounts;

use Illuminate\Database\Eloquent\Model;

class Acc_customer extends Model
{

		public $table = "acc_customers";

		protected $primaryKey = 'cust_id';

		protected $fillable = [
			        'cust_id',
		        	'cust_code',
					'ledger_id',
					'name',
					'alias',
					'cust_home_addr',
					'branch_id',
					'zip',
					'city',
					'state',
					'state_code',
					'country',
					'fax',
					'dflt_delvry_addr',
					'dflt_delvry_zip',
					'dflt_delvry_city',
					'dflt_delvry_state',
					'dflt_delvry_state_code',
					'dflt_delvry_country',
					'dflt_delvry_fax',
					'dflt_delvry_mobile',
					'cust_category',
					'email',
					'mobile',
					'note',
					'due_days',
					'vat_no',
					'van_line_id',
					'price_group_id',
					'server_sync_flag',
					'server_sync_time',
					'local_sync_time','opening_balance'
		];
		protected $hidden = [
					'created_at', 'updated_at'
		];

		public function customer()
		{
			return $this->hasMany('App\Models\Sales\SalesMaster','cust_id','sales_cust_id');
		}

}
