<?php

namespace App\Models\Accounts;

use Illuminate\Database\Eloquent\Model;

class voucherType extends Model
{
    public $table = "voucher_type";
   
    protected $guarded = [];
    
    protected $hidden = [
            'created_at', 'updated_at',
        ];
    
}
