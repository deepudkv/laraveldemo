<?php

namespace App\Models\Accounts;

use Illuminate\Database\Eloquent\Model;

class Acc_journal_sub extends Model
{
    public $table = "journal_sub";

    protected $fillable = [
          'jnsub_id',
          'jnsub_ref_no',
          'jnsub_date',
          'jnsub_time',
          'jnsub_datetime',
          'jnsub_timestamp',
          'jnsub_acc_ledger_id',
          'jnsub_ledger_id',
          'jnsub_amount',
          'jnsub_narration',
          'jnsub_ttl_amount',
          'jnsub_no',
          'van_ref_no',
          'branch_jn_no',
          'server_sync_flag',
          'server_sync_time',
          'local_sync_time',
          'jn_flags',
          'branch_id',
          'van_id'
          
    ];

    protected $guarded = [];

    protected $hidden = [
          'created_at', 'updated_at'
    ];
}
