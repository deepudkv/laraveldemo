<?php

namespace App\Models\Accounts;

use Illuminate\Database\Eloquent\Model;

class Acc_group extends Model
{

    protected $primaryKey = 'accgrp_id';

    protected $guarded = [];

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function subGroups()
    {
        return $this->hasMany('App\Models\Accounts\Acc_group', 'accgrp_parent', 'accgrp_id');
    }

    public function ledgers()
    {
        return $this->hasMany('App\Models\Accounts\Acc_ledger', 'ledger_accgrp_id', 'accgrp_id');
    }

    public function ledgerBalance()
    {
        return $this->hasMany('App\Models\Accounts\Acc_ledger', 'ledger_accgrp_id', 'accgrp_id')
            ->join('acc_voucher', 'acc_voucher.vch_ledger_to', '=', 'acc_ledgers.ledger_id')
		  ;
		//  ->where('acc_voucher.vch_id2', 0)
    }

}
