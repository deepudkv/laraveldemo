<?php

namespace App\Models\Accounts;

use Illuminate\Database\Eloquent\Model;

class Acc_supplier extends Model
{

protected $primaryKey = 'supp_id';

protected $guarded = [];

protected $hidden = [
		'created_at', 'updated_at',
	];

}
