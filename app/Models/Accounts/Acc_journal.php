<?php

namespace App\Models\Accounts;

use Illuminate\Database\Eloquent\Model;

class Acc_journal extends Model
{
    public $table = "journal";
    
    protected $primaryKey = 'jn_id';

    protected $fillable = [
          'jn_id',
          'jn_no',
          'jn_date',
          'jn_time',
          'jn_datetime',
          'jn_timestamp',
          'jn_acc_ledger_id',
          'jn_ttl_amount',
          'jn_note',
          'branch_id',
          'van_id',
          'server_sync_flag',
          'server_sync_time',
          'local_sync_time',
          'van_jn_no',
          'branch_jn_no',
          'branch_rec_no2',
          'is_web',
          'jn_flags',
          'server_sync_flag',
          'server_sync_time',
          'local_sync_time',
          'sync_code',
          'sync_completed'
    ];
    
    protected $guarded = [];
    
    protected $hidden = [
          'created_at', 'updated_at'
    ];


    public function particulars()
    {
        return $this->hasMany('App\Models\Accounts\Acc_journal_sub','jnsub_ref_no','jn_no');
    }


}
 