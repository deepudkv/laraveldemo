<?php

namespace App\Models\Accounts;

use Illuminate\Database\Eloquent\Model;

class price_group extends Model
{
    public $table = "price_group";

    protected $fillable = [
        'group_name',
        'group_descp',
        'branch_id',
        'godown_id',
        'van_id',
        'server_sync_flag',
        'server_sync_time',
        'local_sync_time'
    ];

    protected $guarded = [];

    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
