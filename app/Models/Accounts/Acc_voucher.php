<?php

namespace App\Models\Accounts;

use Illuminate\Database\Eloquent\Model;

class Acc_voucher extends Model
{
    public $table = "acc_voucher";
    protected $primaryKey = 'vch_id';
    protected $fillable = [
        'vch_id',
        'vch_no',
        'vch_ledger_from',
        'vch_ledger_to',
        'vch_date',
        'vch_in',
        'vch_out',
        'vch_vchtype_id',
        'vch_notes',
        'vch_added_by',
        'vch_id2',
        'vch_from_group',
        'vch_flags',
        'ref_no',
        'branch_id',
        'is_web',
        'godown_id',
        'van_id',
        'branch_ref_no',
        'sub_ref_no'
    ];

    protected $guarded = [];

    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
