<?php

namespace App\Models\Accounts;

use Illuminate\Database\Eloquent\Model;

class AccReceiptSub extends Model
{

      public $table = "acc_receipt_sub";

      protected $fillable = [
            'recsub_id',
            'recsub_rec_no',
            'recsub_date',
            'recsub_time',
            'recsub_datetime',
            'recsub_timestamp',
            'recsub_acc_ledger_id',
            'recsub_ledger_id',
            'recsub_amount',
            'recsub_narration',
            'recsub_vat_per',
            'recsub_vat_amt',
            'branch_id',
            'van_id',
            'server_sync_flag',
            'server_sync_time',
            'local_sync_time',
            'recsub_flags',
            'van_rec_no',
            'branch_rec_no',
            'vat_catgeory_id',
            'recsub_ttl_amount',
            'recsub_vat_inc',
            'recsub_no'
            
            
      ];

      protected $guarded = [];

      protected $hidden = [
            'created_at', 'updated_at'
      ];
}
