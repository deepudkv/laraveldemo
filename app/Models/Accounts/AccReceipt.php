<?php

namespace App\Models\Accounts;

use Illuminate\Database\Eloquent\Model;

class AccReceipt extends Model
{
    public $table = "acc_receipt";
    
    protected $primaryKey = 'rec_id';

    protected $fillable = [
          'rec_id',
          'rec_no',
          'rec_date',
          'rec_time',
          'rec_datetime',
          'rec_timestamp',
          'rec_acc_ledger_id',
          'rec_ttl_amount',
          'rec_note',
          'branch_id',
          'van_id',
          'server_sync_flag',
          'server_sync_time',
          'local_sync_time',
          'van_rec_no',
          'branch_rec_no',
          'branch_rec_no2',
          'is_web',
          'rec_flags',
          'rec_vat_amount',
          'rec_gttl_amount',
          'rec_vchtype_id',
          'sync_code',
          'sync_completed',
          'inter_payment_no',
          'added_by'
    ];
    
    protected $guarded = [];
    
    protected $hidden = [
          'created_at', 'updated_at'
    ];


    public function particulars()
    {
        return $this->hasMany('App\Models\Accounts\AccReceiptSub','recsub_rec_no','rec_no');
    }

    public function subLedger()
    {
        return $this->hasOne('App\Models\Accounts\AccReceiptSub','recsub_rec_no','rec_no');
    }


    public function ledger()
    {
        return $this->hasMany('App\Models\Accounts\Acc_ledger','ledger_id','rec_acc_ledger_id');
    }
}
