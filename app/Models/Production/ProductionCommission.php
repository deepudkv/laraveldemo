<?php

namespace App\Models\Production;

use Illuminate\Database\Eloquent\Model;

class ProductionCommission extends Model
{
    public $table = "production_comm";

    protected $fillable = ['prdncomm_prodform_id','prdncomm_prdn_id','prdncomm_date'
    ,'prdncomm_flag'
    ,'prdncomm_in','prdncomm_out','prdncomm_laccount_no',
    'server_sync_flag',
    'server_sync_time',
    'local_sync_time','branch_id'];

    protected $guarded = [];

	protected $hidden = [
		'created_at', 'updated_at',
    ];

    
   
}
