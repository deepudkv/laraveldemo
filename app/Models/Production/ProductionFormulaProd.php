<?php

namespace App\Models\Production;

use Illuminate\Database\Eloquent\Model;

class ProductionFormulaProd extends Model
{
    public $table = "production_formula_product";

    protected $primaryKey = 'prodformprod_id';

		protected $fillable = ['prodformprod_prodform_id', 'prodformprod_prod_id', 
		'prodformprod_qty', 'prodformprod_perc', 'prodformprod_unit_id',
		 'prodformprod_flag','server_sync_flag',
		 'server_sync_time',
		 'local_sync_time','branch_id'];

	protected $hidden = [
		'created_at', 'updated_at',
	];

}
