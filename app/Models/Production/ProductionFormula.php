<?php

namespace App\Models\Production;

use Illuminate\Database\Eloquent\Model;

class ProductionFormula extends Model
{
    public $table = "production_formula";

    protected $primaryKey = 'prodform_id';

    protected $fillable = ['prodform_id', 'prodform_name', 'prodform_ingred',
        'prodform_prod_count', 'prodform_commission', 'prodform_comm_amount',
        'prodform_misc', 'prodform_insp_laccount_no', 'prodfrom_date',
        'prodform_added_by', 'prodform_flag','server_sync_flag',
        'server_sync_time',
        'local_sync_time','branch_id'];

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function commision()
    {
        return $this->hasManyThrough(
            'App\Models\Accounts\Acc_ledger',
            'App\Models\Production\ProductionFormulaComm',
            'prodformcomm_prodform_id',
            'ledger_id',
            'prodform_id',
            'prodformcomm_laccount_no'
        );
    }
    // public function commision() {
    //    return $this->hasMany('App\Models\Production\ProductionFormulaComm','prodformcomm_prodform_id','prodform_id');
    // }
    public function prod1()
    {
        return $this->hasManyThrough(
            'App\Models\Product\Product',
            'App\Models\Production\ProductionFormulaProd',
            'prodformprod_prodform_id',
            'prd_id',
            'prodform_id',
            'prodformprod_prod_id'
        )->join('units', 'units.unit_id', '=', 'products.prd_base_unit_id')
            ->join('branch_stocks', 'branch_stocks.bs_prd_id', '=', 'products.prd_id')
            ->groupBy('production_formula_product.prodformprod_id')
            ->orderBy('production_formula_product.prodformprod_id');
    }

    public function prod() {
        return $this->hasManyThrough(
            'App\Models\Product\Product',
            'App\Models\Production\ProductionFormulaProd',
            'prodformprod_prodform_id',
            'prd_id',
            'prodform_id',
            'prodformprod_prod_id'
        )->join('units','units.unit_id','=','products.prd_base_unit_id');
    }


    public function sub1()
    {
        return $this->hasManyThrough(
            'App\Models\Product\Product',
            'App\Models\Production\ProductionFormulaSub',
            'prodformsub_prodform_id',
            'prd_id',
            'prodform_id',
            'prodformsub_prod_id'
        )->join('units', 'units.unit_id', '=', 'products.prd_base_unit_id')
            ->join('branch_stocks', 'branch_stocks.bs_prd_id', '=', 'products.prd_id')
            ->groupBy('production_formula_sub.prodformsub_prodform_id');
    }

    public function sub() {
        return $this->hasManyThrough(
            'App\Models\Product\Product',
            'App\Models\Production\ProductionFormulaSub',
            'prodformsub_prodform_id',
            'prd_id',
            'prodform_id',
            'prodformsub_prod_id'
        )->join('units','units.unit_id','=','products.prd_base_unit_id');
    }

    public function staff()
    {
        return $this->hasOne('App\Models\Accounts\Acc_ledger', 'ledger_id', 'prodform_insp_laccount_no');
    }

}
