<?php

namespace App\Models\Production;

use Illuminate\Database\Eloquent\Model;

class ProductionSub extends Model
{
    public $table = "production_sub";

    protected $primaryKey = 'prdnsub_id';
  
    protected $fillable = ['prdnsub_prdn_id','prdnsub_prd_id','prdnsub_prodform_id','prdnsub_prodformsub_id',
    'prdnprd_prd_id' ,'prdnsub_stock_id','prdnsub_branch_stock_id','prdnsub_qty','prdnsub_unit_id',
    'prdnsub_rate','prdnsub_date','prdnsub_added_by','prdnsub_flag','server_sync_flag',
    'server_sync_time',
    'local_sync_time','branch_id'];


	protected $hidden = [
		'created_at', 'updated_at',
    ];

    
   
}
