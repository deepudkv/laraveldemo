<?php

namespace App\Models\Production;

use Illuminate\Database\Eloquent\Model;

class ProductionProducts extends Model
{
    public $table = "production_products";


    protected $fillable = ['prdnprd_btach_code','prdnprd_exp_date','prdnprd_mfg_date','prdnprd_gd_id','prdnprd_prdn_id','prdnprd_prodform_id','prdnprd_prd_id',
    'prdnprd_stock_id' ,'prdnprd_branch_stock_id','prdnprd_qty','prdnprd_required_qty',
    'prdnprd_unit_id',
    'prdnprd_rate','prdnprd_rate_expected','prdnprd_date','prdnprd_added_by','prdnprd_flag','server_sync_flag',
    'server_sync_time',
    'local_sync_time','branch_id'];


	protected $hidden = [
		'created_at', 'updated_at',
    ];

  
 public function production()
{
    return $this->belongsTo('App\Models\Production\Production',
    	'prdnprd_prdn_id');
}   
   
}
