<?php

namespace App\Models\Production;

use Illuminate\Database\Eloquent\Model;

class ProductionFormulaComm extends Model
{
    public $table = "production_formula_comm";

    protected $primaryKey = 'prodformcomm_id';

        protected $fillable = ['prodformcomm_prodform_id', 'prodformcomm_prodform_id',
         'prodformcomm_laccount_no', 'prodformcomm_amount',
         'server_sync_flag',
         'server_sync_time',
         'local_sync_time','branch_id'];

	protected $hidden = [
		'created_at', 'updated_at',
	];
}
