<?php

namespace App\Models\Production;

use Illuminate\Database\Eloquent\Model;

class ProductionFormulaSub extends Model
{
    public $table = "production_formula_sub";

    protected $primaryKey = 'prodformsub_id';

		protected $fillable = ['prodformsub_prodform_id', 'prodformsub_prod_id', 
		'prodformsub_qty','prodformsub_unit_id', 'prodformsub_flag','server_sync_flag',
        'server_sync_time',
        'local_sync_time','branch_id'];

	protected $hidden = [
		'created_at', 'updated_at',
	];

}
