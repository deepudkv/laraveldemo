<?php

namespace App\Models\Production;

use Illuminate\Database\Eloquent\Model;

class Production extends Model
{
    public $table = "production";

    protected $primaryKey = 'prdn_id';

    protected $fillable = ['prdn_prodform_id','prdn_purch_amount','prdn_misc_exp','prdn_comm_amount'
    ,'prdn_cost','prdn_date','prdn_added_by','prdn_inspection_staff','prdn_commission',	'prdn_flag','server_sync_flag',
    'server_sync_time',
    'local_sync_time','branch_id'];

	protected $hidden = [
		'created_at', 'updated_at',
    ];
    
    
    public function commision() {
        return $this->hasManyThrough(
            'App\Models\Accounts\Acc_ledger',
            'App\Models\Production\ProductionCommission',
            'prdncomm_prodform_id',
            'ledger_id',
            'prdn_id',
            'prdncomm_laccount_no'
        );
    }

    public function prod() {


        return $this->hasManyThrough(
            'App\Models\Product\Product',
            'App\Models\Production\ProductionProducts',
            'prdnprd_prdn_id',
            'prd_id',
            'prdn_id',
            'prdnprd_prd_id'
        )->join('units','units.unit_id','=','products.prd_base_unit_id')
        ->leftjoin('godown_master','godown_master.gd_id','=','production_products.prdnprd_gd_id');
    }

    public function sub() {
        return $this->hasManyThrough(
            'App\Models\Product\Product',
            'App\Models\Production\ProductionSub',
            'prdnsub_prdn_id',
            'prd_id',
            'prdn_id',
            'prdnsub_prd_id'
        )->join('units','units.unit_id','=','products.prd_base_unit_id');
    }
    
    public function formula()
    {
        return $this->belongsTo('App\Models\Production\ProductionFormula','prdn_prodform_id','prodform_id');
    }

}
