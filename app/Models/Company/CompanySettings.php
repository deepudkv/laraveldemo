<?php

namespace App\Models\Company;

use Illuminate\Database\Eloquent\Model;

class CompanySettings extends Model
{
    public $table = "company_settings";

    protected $fillable = ['*'];
    
    protected $guarded = [];
    
    protected $hidden = [
          'created_at', 'updated_at'
    ];
}
