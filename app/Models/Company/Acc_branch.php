<?php

namespace App\Models\Company;

use Illuminate\Database\Eloquent\Model;

class Acc_branch extends Model
{

protected $primaryKey = 'branch_id';

protected $guarded = [];

protected $hidden = [
		'created_at', 'updated_at',
	];


}
