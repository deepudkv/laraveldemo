<?php

namespace App\Models\Company;

use Illuminate\Database\Eloquent\Model;

class Companies extends Model
{
    public $table = "companies";

    protected $fillable = [
          'cmp_name',
          'cmp_addr',
          'cpm_code',
          'cmp_sub_domain'
    ];
    
    protected $guarded = [];
    
    protected $hidden = [
          'created_at', 'updated_at'
    ];
}
