<?php

namespace App\Models\Sync;

use Illuminate\Database\Eloquent\Model;

class SyncSalesReturnSub extends Model
{
    public $table = "sync_sales_return_sub";

    protected $fillable = [
        'salesretsub_salesret_id',
        'salesretsub_sales_inv_no',
        'salesretsub_salesub_id',
        'salesretsub_stock_id',
        'salesretsub_prod_id',
        'salesretsub_rate',
        'salesretsub_qty',
        'salesretsub_date',
        'salesretsub_flags',
        'salesretsub_unit_id',
        'salesretsub_tax_per',
        'salesretsub_tax_rate',
        'salesretsub_godown_id',
        'salesretsub_server_sync',
        'salesretsub_sync_timestamp',
        'salesretsub_server_timestamp',
        'branch_id',
        'godown_id',
        'van_id'
    ];

    protected $guarded = [];

    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
