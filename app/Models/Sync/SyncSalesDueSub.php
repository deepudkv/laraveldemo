<?php

namespace App\Models\Sync;

use Illuminate\Database\Eloquent\Model;

class SyncSalesDueSub extends Model
{
    public $table = "sync_sales_due_sub";

    protected $fillable = [
        'salesduesub_salesdue_id',
        'salesduesub_inv_no',
        'salesduesub_ledger_id',
        'salesduesub_date',
        'salesduesub_type',
        'salesduesub_in',
        'salesduesub_out',
        'salesduesub_inv_amount',
        'salesduesub_inv_balance',
        'salesduesub_added_by',
        'salesduesub_flags',
        'salesduesub_agent_ledger_id',
        'branch_id',
        'godown_id',
        'van_id'
    ];

    protected $guarded = [];

    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
