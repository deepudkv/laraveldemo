<?php

namespace App\Models\Sync;

use Illuminate\Database\Eloquent\Model;

class SyncSalesMaster extends Model
{
    public $table = "sync_sales_master";

    protected $fillable = [
        'sales_inv_no',
        'sales_cust_type',
        'sales_cust_id',
        'sales_cust_name',
        'sales_cust_ph',
        'sales_cust_address',
        'sales_cust_tin',
        'sales_timestamp',
        'sales_date',
        'sales_time',
        'sales_datetime',
        'sales_total',
        'sales_discount',
        'sales_disc_promo',
        'sales_disc_loyalty',
        'sales_paid',
        'sales_balance',
        'sales_profit',
        'sales_rec_amount',
        'sales_tax',
        'sales_added_by',
        'sales_flags',
        'sales_notes',
        'sales_pay_type',
        'branch_id',
        'godown_id',
        'van_id',
        'sales_agent_ledger_id',
        'sales_acc_ledger_id',
        'sales_order_no',
        'sales_order_type',
        'sales_godownsale_id'
    ];

    protected $guarded = [];

    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
