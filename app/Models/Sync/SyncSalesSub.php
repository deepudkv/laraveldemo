<?php

namespace App\Models\Sync;

use Illuminate\Database\Eloquent\Model;

class SyncSalesSub extends Model
{
    public $table = "sync_sales_sub";

    protected $fillable = [
        'salesub_sales_inv_no',
        'salesub_stock_id',
        'salesub_branch_stock_id',
        'salesub_prod_id',
        'salesub_rate',
        'salesub_qty',
        'salesub_rem_qty',
        'salesub_discount',
        'salesub_timestamp',
        'salesub_date',
        'salesub_serial',
        'salesub_flags',
        'salesub_profit',
        'salesub_unit_id',
        'salesub_prate',
        'salesub_promo_id',
        'salesub_promo_per',
        'salesub_promo_rate',
        'salesub_tax_per',
        'salesub_tax_rate',
        'branch_id',
        'godown_id',
        'van_id'
    ];

    protected $guarded = [];

    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
