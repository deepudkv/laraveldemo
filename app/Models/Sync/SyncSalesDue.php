<?php

namespace App\Models\Sync;

use Illuminate\Database\Eloquent\Model;

class SyncSalesDue extends Model
{
    public $table = "sync_sales_due";

    protected $fillable = [
        'sales_recp_no',
        'salesdue_vch_no',
        'salesdue_date',
        'salesdue_timestamp',
        'salesdue_ledger_id',
        'salesdue_amount',
        'salesdue_multi',
        'salesdue_added_by',
        'salesdue_flags',
        'salesdue_agent_ledger_id',
        'sales_pay_type',
        'sales_pay_txn_id',
        'branch_id',
        'godown_id',
        'van_id'
    ];

    protected $guarded = [];

    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
