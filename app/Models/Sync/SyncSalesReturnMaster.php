<?php

namespace App\Models\Sync;

use Illuminate\Database\Eloquent\Model;

class SyncSalesReturnMaster extends Model
{
    public $table = "sync_sales_return_master";

    protected $fillable = [
        'salesret_sales_inv_no',
        'salesret_date',
        'salesret_time',
        'salesdue_timestamp',
        'salesdue_ledger_id',
        'salesdue_amount',
        'salesret_cust_type',
        'salesret_cust_id',
        'salesret_cust_name',
        'salesret_cust_address',
        'salesret_cust_ph',
        'salesret_amount',
        'salesret_discount',
        'salesret_pay_type',
        'salesret_added_by',
        'salesret_flags',
        'salesret_vch_no',
        'salesret_notes',
        'salesret_sales_amount',
        'salesret_sales_discount',
        'salesret_service_amount',
        'salesret_service_discount',
        'salesret_servoid_id',
        'salesret_misc_amount',
        'salesret_cust_tin',
        'salesret_tax',
        'salesret_service_tax',
        'salesret_tax_vch_no',
        'salesret_godown_id',
        'salesret_agent_ledger_id',
        'salesret_pay_type2',
        'salesret_acc_ledger_id',
        'salesret_datetime',
        'salesret_godown_ret',
        'branch_id',
        'godown_id',
        'van_id'
    ];

    protected $guarded = [];

    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
