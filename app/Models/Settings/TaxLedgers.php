<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Model;

class TaxLedgers extends Model
{
    public $table = 'tax_ledgers';
    
    public $primaryKey = 'taxled_auto_id';  

    protected $fillable = [
        'taxled_id',
        'taxled_taxcat_id',
        'taxled_branch_id',
        'taxled_ledger_id',
        'taxled_taxcatsub_id',
        'taxled_vchtype_id'
    ];

    protected $guarded = [];

    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
