<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Model;

class userPrivilages extends Model
{
    protected $fillable = [
        'up_id',
        'up_menu_id',
        'up_usr_id',
        'up_show_menu',
        'up_add',
        'up_edit',
        'up_void','up_flags','branch_id','server_sync_flag','server_sync_time','local_sync_time'
    ];

    protected $guarded = [];

    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
