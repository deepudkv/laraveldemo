<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Model;

class TaxCategorySub extends Model
{
    public $table = 'tax_category_sub';
    public $primaryKey = 'taxcatsub_auto_id';    

    protected $fillable = [
        'taxcatsub_id',
        'taxcatsub_name',
        'taxcatsub_code',
        'taxcatsub_vchtype_id'
    ];

    protected $guarded = [];

    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
