<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Model;

class MenuMaster extends Model
{
    public $table = "menu_master";
    public $primaryKey = 'menu_id';    

    protected $fillable = [
        'menu_id',
        'menu_name',
        'menu_grp_id',
        'menu_use_id',
        'menu_prnt_id',
        'menu_depth',
        'menu_flag',
        'branch_id',
        'server_sync_flag',
        'server_sync_time',
        'local_sync_time'
        
    ];

    protected $guarded = [];

    protected $hidden = [
        'created_at', 'updated_at'
    ];


    public function subMenus()
    {
        return $this->hasMany('App\Models\Settings\MenuMaster','menu_prnt_id','menu_id');
	}


}
