<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Model;

class BarCode extends Model
{
    public $table = "settings_barcode_print";
    protected $primaryKey = 'barset_id';
 	 protected $guarded = [];
}