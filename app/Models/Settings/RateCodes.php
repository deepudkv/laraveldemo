<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Model;

class RateCodes extends Model
{
    public $table = "rate_codes";
    public $primaryKey = 'rcd_id';    

    protected $fillable = [
        'rcd_id',
        'rcd_code',
        'rcd_added_by',
        'server_sync_flag',
        'server_sync_time',
        'local_sync_time'
        
    ];

    protected $guarded = [];

    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
