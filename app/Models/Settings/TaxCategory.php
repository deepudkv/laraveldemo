<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Model;

class TaxCategory extends Model
{
    public $table = 'tax_category';
    public $primaryKey = 'taxcat_auto_id';    

    protected $fillable = [
        'taxcat_id',
        'taxcat_name',
        'taxcat_code',
        'taxcat_tax_per',
        'display_name',
        'is_sales_ledger',
        'is_purchase_ledger',
        'is_servcie_ledger'
    ];

    protected $guarded = [];

    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
