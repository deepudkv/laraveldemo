<?php

namespace App\Common;
use App\Models\Accounts\Acc_ledger;
use App\Models\Accounts\Acc_customer;
use App\Models\Accounts\Acc_supplier;
use Illuminate\Http\Request;
use DB;

class Corelib
{
    public static function createCust(Request $request)
    {
    $res= 1;   
    DB::beginTransaction();
	try {
		$led_insert = Acc_ledger::create(['ledger_name' => $request->ledger_name,
		'ledger_alias' => ($request->ledger_alias != "" ? $request->ledger_alias : 'Nill'),
		'ledger_code' => ($request->ledger_code != "" ? $request->ledger_code : 'Nill'),
		'ledger_accgrp_id' => ($request->ledger_accgrp_id != "" ? $request->ledger_accgrp_id : 0 ),
		'ledger_branch_id' => ($request->ledger_branch_id != "" ?  $request->ledger_branch_id : 0 ),

		'ledger_email' => ($request->ledger_email != "" ? $request->ledger_email : "Nill"),
		'ledger_mobile' => ($request->ledger_mobile != "" ? $request->ledger_mobile : "Nill" ),
		'ledger_notes' => ($request->ledger_notes != "" ? $request->ledger_notes : "Nill"),
		'ledger_flags' => ($request->ledger_flags != "" ? $request->ledger_flags : 0),
		'ledger_due' => ($request->ledger_due != "" ? $request->ledger_due : 0),
		'ledger_tin' => ($request->ledger_tin != "" ? $request->ledger_tin : "Nill"),
		'company_id' => ($request->company_id != "" ? $request->company_id : 0),
		'branch_id' => ($request->branch_id != "" ? $request->branch_id : 0),
		'server_sync_time' => ($request->server_sync_time != "" ? $request->server_sync_time : "Nill")
		]);
			
		Acc_customer::create(['ledger_id' => $led_insert->ledger_id,
		'cust_home_addr' => ($request->cust_home_addr != "" ? $request->cust_home_addr : "Nill"),
		'cust_image' => ($request->cust_image != "" ? $request->cust_image : "Nill"),	
		'company_id' => ($request->company_id != "" ? $request->company_id : 0),
		'branch_id' => ($request->branch_id != "" ? $request->branch_id : 0),
		'server_sync_time' => ($request->server_sync_time != "" ? $request->server_sync_time : 0)
		]);

		DB::commit();
	} catch (\Exception $e) {
	DB::rollback();
	throw $e;	
	$res=0;	
	}
	return ($res);
    }


    public static function editCust(Request $request,$id)
    {
    $res= 1;  
    DB::beginTransaction();
	try {
		$data = Acc_ledger::find($id);   
		$res =$data->fill(['ledger_name' => $request->ledger_name,
		'ledger_alias' => ($request->ledger_alias != "" ? $request->ledger_alias : "Nill"),
		'ledger_code' => ($request->ledger_code != "" ? $request->ledger_code : "Nill"),
		'ledger_accgrp_id' => ($request->ledger_accgrp_id != "" ? $request->ledger_accgrp_id : 0),
		'ledger_branch_id' => ($request->ledger_branch_id != "" ? $request->ledger_branch_id : 0),
		'ledger_address' => ($request->ledger_address != "" ? $request->ledger_address : "Nill"),
		'ledger_email' => ($request->ledger_email != "" ? $request->ledger_email : "Nill"),
		'ledger_mobile' => ($request->ledger_mobile != "" ? $request->ledger_mobile : "Nill"),
		'ledger_notes' => ($request->ledger_notes != "" ? $request->ledger_notes : "Nill"),
		'ledger_flags' => ($request->ledger_flags != "" ? $request->ledger_flags : 0),
		'ledger_due' => ($request->ledger_due != "" ? $request->ledger_due : 0),
		'ledger_tin' => ($request->ledger_tin != "" ? $request->ledger_tin : 0),
		'company_id' => ($request->company_id != "" ? $request->company_id : 0),
		'branch_id' => ($request->branch_id != "" ? $request->branch_id : 0),
		'server_sync_time' => ($request->server_sync_time != "" ? $request->server_sync_time : 0)
		])->save();  

		$customer = Acc_customer::where('ledger_id',$id)->first();
		$res = $customer->fill(['cust_home_addr' =>  ($request->cust_home_addr != "" ? $request->cust_home_addr : "Nill"),
		'cust_image' =>  ($request->cust_image != "" ? $request->cust_image : 0),	
		'company_id' =>  ($request->company_id != "" ? $request->company_id : 0),
		'branch_id' =>  ($request->branch_id != "" ? $request->branch_id : 0),
		'server_sync_time' =>  ($request->server_sync_time != "" ? $request->server_sync_time : "Nill")
		])->save();

		DB::commit();
	} catch (\Exception $e) {
	DB::rollback();
	// throw $e;	
	$res=0;	
	}
	return ($res);
    }



    public static function createSup(Request $request)
    {
    $res= 1;   
    DB::beginTransaction();
	try {

		if($request->op_bal_type){
            $request->op_bal = 0 - $request->op_bal;
            }else{
                $request->op_bal = 0 + $request->op_bal;
			}
			
		$led_insert = Acc_ledger::create(['ledger_name' => $request->ledger_name,
		'ledger_alias' =>  ($request->ledger_alias != "" ? $request->ledger_alias : "Nill"),
		'ledger_code' =>  ($request->ledger_code != "" ? $request->ledger_code : 0),
		'ledger_accgrp_id' =>  ($request->ledger_accgrp_id != "" ? $request->ledger_accgrp_id : 0),
		'ledger_branch_id' =>  ($request->ledger_branch_id != "" ? $request->ledger_branch_id : 0),
		'ledger_address' =>  ($request->ledger_address != "" ? $request->ledger_address : "Nill"),
		'ledger_email' =>  ($request->ledger_email != "" ? $request->ledger_email : "Nill"),
		'ledger_mobile' =>  ($request->ledger_mobile != "" ? $request->ledger_mobile : "Nill"),
		'ledger_notes' =>  ($request->ledger_notes != "" ? $request->ledger_notes : "Nill"),
		'ledger_flags' =>  ($request->ledger_flags != "" ? $request->ledger_flags : 1),
		'ledger_due' =>  ($request->ledger_due != "" ? $request->ledger_due : 0),
		'ledger_tin' =>  ($request->ledger_tin != "" ? $request->ledger_tin : 0),
		'opening_balance' => $request->op_bal,
		'company_id' =>  ($request->company_id != "" ? $request->company_id : 0),
		'branch_id' =>  ($request->branch_id != "" ? $request->branch_id : 0),
		'server_sync_time' =>  ($request->server_sync_time != "" ? $request->server_sync_time : 0)
		]);

		Acc_supplier::create(['ledger_id' => $led_insert->ledger_id,
		'supp_perm_addr' =>  ($request->supp_perm_addr != "" ? $request->supp_perm_addr : "Nill"),
		'supp_image' =>  ($request->supp_image != "" ? $request->supp_image : "Nill"),
		'opening_balance' => $request->op_bal,	
		'company_id' =>  ($request->company_id != "" ? $request->company_id : 0),
		'branch_id' =>  ($request->branch_id != "" ? $request->branch_id : 0),
		'server_sync_time' =>  ($request->server_sync_time != "" ? $request->server_sync_time : 0)
		]);

		DB::commit();
	} catch (\Exception $e) {
	DB::rollback();
	//throw $e;	
	$res=0;	
	}
	return ($res);
    }


     public static function editSup(Request $request,$id)
    {
    $res= 1;  
    DB::beginTransaction();
	try {
		if($request->op_bal_type){
            $data['opening_balance'] = 0 - $request->op_bal;
            }else{
                $data['opening_balance'] = 0 + $request->op_bal;
            }
		$data = Acc_ledger::find($id);   
		$res =$data->fill(['ledger_name' => $request->ledger_name,
		'ledger_alias' =>  ($request->ledger_alias != "" ? $request->ledger_alias : "Nill"),
		'ledger_code' =>  ($request->ledger_code != "" ? $request->ledger_code : 0),
		'ledger_accgrp_id' =>  ($request->ledger_accgrp_id != "" ? $request->ledger_accgrp_id : 0),
		'ledger_branch_id' =>  ($request->ledger_branch_id != "" ? $request->ledger_branch_id : 0),
		'ledger_address' =>  ($request->ledger_address != "" ? $request->ledger_address : "Nill"),
		'ledger_email' =>  ($request->ledger_email != "" ? $request->ledger_email : "Nill"),
		'ledger_mobile' =>  ($request->ledger_mobile != "" ? $request->ledger_mobile : "Nill"),
		'ledger_notes' =>  ($request->ledger_notes != "" ? $request->ledger_notes : "Nill"),
		'ledger_flags' =>  ($request->ledger_flags != "" ? $request->ledger_flags : 1),
		'ledger_due' =>  ($request->ledger_due != "" ? $request->ledger_due : 0),
		'ledger_tin' =>  ($request->ledger_tin != "" ? $request->ledger_tin : 0),
		'opening_balance' => $data['opening_balance'],
		'company_id' =>  ($request->company_id != "" ? $request->company_id : 0),
		'branch_id' =>  ($request->branch_id != "" ? $request->branch_id : 0),
		'server_sync_time' =>  ($request->server_sync_time != "" ? $request->server_sync_time : 0)
		])->save();  

		$supplier = Acc_supplier::where('ledger_id',$id)->first();
		$res = $supplier->fill(['supp_perm_addr' =>  ($request->supp_perm_addr != "" ? $request->supp_perm_addr : "Nill"),
		'supp_image' =>  ($request->supp_image != "" ? $request->supp_image : "Nill"),
		'opening_balance' => $data['opening_balance'],	
		'company_id' =>  ($request->company_id != "" ? $request->company_id : 0),
		'branch_id' =>  ($request->branch_id != "" ? $request->branch_id : 0),
		'server_sync_time' =>  ($request->server_sync_time != "" ? $request->server_sync_time : 0)
		])->save();

		DB::commit();
	} catch (\Exception $e) {
	DB::rollback();
	//throw $e;	
	$res=0;	
	}
	return ($res);
    }


}