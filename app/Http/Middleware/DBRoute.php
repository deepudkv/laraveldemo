<?php

namespace App\Http\Middleware;

use Closure;
// use Config;
use Illuminate\Contracts\Encryption\DecryptException;
class DBRoute
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if($request->header('cmptoken') != '' || $request->header('vanToken') != '' || $request->header('desktoken') != ''){
            if($request->header('cmptoken') != ''){
                $token = $request->header('cmptoken');
            }
            if($request->header('vanToken') != ''){
                $token = $request->header('vanToken');
            }
            if($request->header('desktoken') != ''){
                $token = $request->header('desktoken');
            }
            try {
                $decrypted = decrypt($token);
                $cmpCode = $decrypted['company_code'];
            } catch (DecryptException $e) {
                return response()->json(['error' => 'Invalid CMP Token', 'status' => 400]);
            }
        } else if(isset($request->company_code)) {
            $cmpCode = $request->company_code;
        } else{
            return response()->json(['error' => 'Invalid Company code', 'status' => 400]);
        }
        if(env('ERP_DATABASE_' . $cmpCode)){
            \Config::set('database.connections.mysql.database', env('ERP_DATABASE_' . $cmpCode));
            return $next($request);
        } else{
             return response()->json(['error' => 'Invalid Company code', 'status' => 400]);
        }
    }
}
