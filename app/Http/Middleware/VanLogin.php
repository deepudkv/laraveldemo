<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Encryption\DecryptException;
class VanLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        
        if($request->header('vanToken') != ''){
            try {
                $decrypted = decrypt($request->header('vanToken'));
                $request->merge($decrypted);
            } catch (DecryptException $e) {
                return response()->json(['error' => 'Invalid Van Token', 'status' => 400]);
            }
        } else {
            return response()->json(['error' => 'Invalid Token', 'status' => 400]);
        }
        return $next($request);
    }
}
