<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Encryption\DecryptException;
class DesktopLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       if($request->header('desktoken') != '') {
            try {
                // function getUserIpAddr(){
                //     if(!empty($_SERVER['HTTP_CLIENT_IP'])){
                //         //ip from share internet
                //         $ip = $_SERVER['HTTP_CLIENT_IP'];
                //     }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
                //         //ip pass from proxy
                //         $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                //     }else{
                //         $ip = $_SERVER['REMOTE_ADDR'];
                //     }
                //     return $ip;
                // }
                
                // echo 'User Real IP - '.getUserIpAddr();
                // echo '<br>' .$_SERVER['HTTP_USER_AGENT'];
               
                // echo '<br>' .$_SERVER['LOCAL_ADDR'];
                // echo '<br>' .$_SERVER['LOCAL_PORT'];
                // echo '<br>' .$_SERVER['REMOTE_ADDR'];
                // exit;
                // echo '<br>' .$_SERVER['HTTP_USER_AGENT'].$_SERVER['LOCAL_ADDR'].$_SERVER['LOCAL_PORT'].$_SERVER['REMOTE_ADDR'];
                // $request->add([
                //     'server_sync_time' => date('Ymdhis'),
                // ]);
                $decrypted = decrypt($request->header('desktoken'));
                $decrypted['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                $request->merge($decrypted);
            } catch (DecryptException $e) {
                return response()->json(['error' => 'Invalid Desktop Token', 'status' => 400]);
            }
        } else {
            return response()->json(['error' => 'Invalid Token', 'status' => 400]);
        }
        return $next($request);
    }
}
