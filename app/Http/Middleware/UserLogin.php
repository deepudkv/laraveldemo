<?php

namespace App\Http\Middleware;

use Closure;
use App\User;

class UserLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $lgd_usr = auth('api')->user();
        if(!empty($lgd_usr)){
            $this->usr_id = $lgd_usr->usr_id;
            $this->usr_type = $lgd_usr->usr_type;
            $this->branch_id = $lgd_usr->branch_id;
            $this->per_page = ($request->per_page) ? $request->per_page : 10;
            $request->request->add(['company_id' => $lgd_usr->company_id,
                'branch_id' => $lgd_usr->branch_id,
                'server_sync_time' => date('ymdHis') . substr(microtime(),2,6),
            ]);
            $this->server_sync_time = date('ymdHis') . substr(microtime(),2,6);
            return $next($request);
        } else{
            return response()->json(['error' => 'Invalid Token', 'status' => 400]);
        }
    }
}
