<?php

namespace App\Http\Controllers\Desktop\Stock;

use App\Http\Controllers\Desktop\DesktopParent;
use App\Models\Stocks\StockBatches;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class stockbatchcontroller extends DesktopParent {

    public $badrequest_stat = 400;
    public $where = array();
    public $request;

    public function operations(Request $request, $type){

        if($request->sync_time){
            $this->where[] = ['server_sync_time','>', $request->sync_time];
        }
        if($request->branch_id){
            $this->where[] = ['branch_id','=', $request->branch_id];
        }
        $this->request = $request;

        switch ($type) {
            case 'get_count':
                return $this->getCount();
                break;
            case 'download':
                return $this->download();
                break;
            case 'upload':
                return $this->download();
                break;
            default:
                return response()->json(['error' => 'Invalid Endpoint', 'status' => $this->badrequest_stat]);
                break;
        }
    }

    public function getCount() {
        $ttlCount = StockBatches::where($this->where)->count();
        return parent::displayData($ttlCount);
    }

    public function download() {
        $query = StockBatches::query();
        $query->where($this->where);
        if($this->request->limit) {
            $perPage = ($this->request->part_no) ? $this->request->part_no : 0;
            $query->skip(($perPage * $this->request->limit));
            $query->take($this->request->limit);
        }
        $info = $query->get();
        return parent::displayData($info);
    }

    public function upload() {
        $data = json_decode($this->request->data, true);
        $ret = array();
        foreach($data as $val) {
            $info = array();
            $info['salesub_id'] = $val['salesub_id'];
            $info['salesub_sales_inv_no'] = $val['salesub_sales_inv_no'];
            $info['salesub_stock_id'] = $val['salesub_stock_id'];
            if(isset($val['salesub_branch_stock_id'])){
                $info['salesub_branch_stock_id'] = $val['salesub_branch_stock_id'];
            }
            $info['salesub_prod_id'] = $val['salesub_prod_id'];
            $info['salesub_rate'] = $val['salesub_rate'];
            $info['salesub_qty'] = $val['salesub_qty'];
            $info['salesub_rem_qty'] = $val['salesub_rem_qty'];
            $info['salesub_discount'] = $val['salesub_discount'];

            if(isset($val['salesub_timestamp'])){
                $info['salesub_timestamp'] = $val['salesub_timestamp'];
            }
            
            $info['salesub_date'] = $val['salesub_date'];
            $info['salesub_serial'] = $val['salesub_serial'];
            $info['salesub_flags'] = $val['salesub_flags'];
            $info['salesub_profit'] = $val['salesub_profit'];
            $info['salesub_unit_id'] = $val['salesub_unit_id'];
            $info['salesub_prate'] = $val['salesub_prate'];
            $info['salesub_promo_id'] = $val['salesub_promo_id'];
            $info['salesub_promo_per'] = $val['salesub_promo_per'];
            $info['salesub_promo_rate'] = $val['salesub_promo_rate'];
            $info['salesub_tax_per'] = $val['salesub_tax_per'];
            $info['salesub_tax_rate'] = $val['salesub_tax_rate'];
            $info['branch_id'] = $this->request->branch_id;
            if(isset($val['is_web'])){
                $info['is_web'] = $val['is_web'];
            }
            if(isset($val['godown_id'])){
                $info['godown_id'] = $val['godown_id'];
            }
            if(isset($val['server_sync_flag'])){
                $info['server_sync_flag'] = $val['server_sync_flag'];
                $info['local_sync_time'] = $val['local_sync_time'];
            }
           
            // $info['created_at'] = $val['created_at'];
            // $info['updated_at'] = $val['updated_at'];
            $info['van_id'] = 0;
            // if($this->request->van_id){
            //     $info['van_id'] = $this->request->van_id;
            // }
            $info['sync_completed'] = 0;
            $info['server_sync_time'] = $this->request->server_sync_time;
            $sales = SalesSub::updateOrCreate(
                [
                    'salesub_id' => $val['salesub_id'], 
                    'branch_id' => $this->request->branch_id, 
                    'van_id' => 0 
                ],
                $info
            );

            $ret[] = array(
                'salesub_id' => $sales->salesub_id,
                'server_sync_time' => $sales->server_sync_time,
                'salesub_sales_inv_no' => $sales->salesub_sales_inv_no,
            );
            
        }
        return parent::displayData($ret);
    }
}
