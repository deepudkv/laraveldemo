<?php

namespace App\Http\Controllers\Desktop\Stock;

use App\Http\Controllers\Desktop\DesktopParent;
use App\Models\Stocks\BranchStocks;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class branchstockcontroller extends DesktopParent {

    public $badrequest_stat = 400;
    public $where = array();
    public $request;

    public function operations(Request $request, $type){

        if($request->sync_time){
            $this->where[] = ['server_sync_time','>', $request->sync_time];
        }
        if($request->branch_id) {
            $this->where[] = ['bs_branch_id','=', $request->branch_id];
        }
        
        $this->request = $request;

        switch ($type) {
            case 'get_count':
                return $this->getCount();
                break;
            case 'download':
                return $this->download();
                break;
            default:
                return response()->json(['error' => 'Invalid Endpoint', 'status' => $this->badrequest_stat]);
                break;
        }
    }

    public function getCount() {
        $ttlCount = BranchStocks::where($this->where)->count();
        return parent::displayData($ttlCount);
    }

    public function download() {
        $query = BranchStocks::query();
        $query->where($this->where);
        if($this->request->limit) {
            $perPage = ($this->request->part_no) ? $this->request->part_no : 0;
            $query->skip(($perPage * $this->request->limit));
            $query->take($this->request->limit);
        }
        $query->orderBy('server_sync_time', 'ASC');
        $info = $query->get();
        if(!$info->isEmpty()){
            if((int)$info[0]['bs_prate'] == $info[0]['bs_prate']){
                $info[0]['bs_prate'] = number_format($info[0]['bs_prate'],2,".","");
            }
            if((int)$info[0]['bs_avg_prate'] == $info[0]['bs_avg_prate']){
                $info[0]['bs_avg_prate'] = number_format($info[0]['bs_avg_prate'],2,".","");
            }
            if((int)$info[0]['bs_srate'] == $info[0]['bs_srate']){
                $info[0]['bs_srate'] = number_format($info[0]['bs_srate'],2,".","");
            }
        }
        return parent::displayData($info);
    }

}
