<?php

namespace App\Http\Controllers\Desktop\Sales;

use App\Http\Controllers\Desktop\DesktopParent;
use App\Models\Sales\SalesMaster;
use App\Models\Accounts\Acc_customer;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class salesmastercontroller extends DesktopParent {

    public $badrequest_stat = 400;
    public $where = array();
    public $request;

    public function operations(Request $request, $type){

        if($request->sync_time){
            $this->where[] = ['server_sync_time','>', $request->sync_time];
        }
        if($request->van_id) {
            $this->where[] = ['van_id','=', $request->van_id];
        } else if($request->branch_id) {
            $this->where[] = ['branch_id','=', $request->branch_id];
        }
        $this->where[] = ['sync_completed','=', 1];
        
        $this->request = $request;

        switch ($type) {
            case 'get_count':
                return $this->getCount();
                break;
            case 'download':
                return $this->download();
                break;
            case 'upload':
                return $this->upload();
                break;
            default:
                return response()->json(['error' => 'Invalid Endpoint', 'status' => $this->badrequest_stat]);
                break;
        }
    }

    public function getCount() {
        $ttlCount = SalesMaster::where($this->where)->count();
        return parent::displayData($ttlCount);
    }

    public function download() {
        $query = SalesMaster::query();
        $query->where($this->where);
        if($this->request->limit) {
            $perPage = ($this->request->part_no) ? $this->request->part_no : 0;
            $query->skip(($perPage * $this->request->limit));
            $query->take($this->request->limit);
        }
        $info = $query->get();
        if(!$info->isEmpty()){
            if((int)$info[0]['sales_total'] == $info[0]['sales_total']){
                $info[0]['sales_total'] = number_format($info[0]['sales_total'],2,".","");
            }
            if((int)$info[0]['sales_discount'] == $info[0]['sales_discount']){
                $info[0]['sales_discount'] = number_format($info[0]['sales_discount'],2,".","");
            }
            if((int)$info[0]['sales_disc_promo'] == $info[0]['sales_disc_promo']){
                $info[0]['sales_disc_promo'] = number_format($info[0]['sales_disc_promo'],2,".","");
            }
            if((int)$info[0]['sales_disc_loyalty'] == $info[0]['sales_disc_loyalty']){
                $info[0]['sales_disc_loyalty'] = number_format($info[0]['sales_disc_loyalty'],2,".","");
            }
            if((int)$info[0]['sales_paid'] == $info[0]['sales_paid']){
                $info[0]['sales_paid'] = number_format($info[0]['sales_paid'],2,".","");
            }
            if((int)$info[0]['sales_balance'] == $info[0]['sales_balance']){
                $info[0]['sales_balance'] = number_format($info[0]['sales_balance'],2,".","");
            }
            if((int)$info[0]['sales_profit'] == $info[0]['sales_profit']){
                $info[0]['sales_profit'] = number_format($info[0]['sales_profit'],2,".","");
            }
            if((int)$info[0]['sales_rec_amount'] == $info[0]['sales_rec_amount']){
                $info[0]['sales_rec_amount'] = number_format($info[0]['sales_rec_amount'],2,".","");
            }
            if((int)$info[0]['sales_tax'] == $info[0]['sales_tax']){
                $info[0]['sales_tax'] = number_format($info[0]['sales_tax'],2,".","");
            }
        }
        return parent::displayData($info);
    }

    public function upload() {

        
        $data = $this->request->json()->all();
        $ret = array();
        foreach($data as $key => $val) {
            if(is_numeric($key)){
                $info = array();
                $max = SalesMaster::max('sales_inv_no');
                $info['sales_inv_no'] = ($val['sales_inv_no'] ==0 ) ? $max+1 : $val['sales_inv_no'];
                $info['sales_cust_type'] = $val['sales_cust_type'];
                $info['sales_cust_id'] = $val['sales_cust_id'];
                if($val['sales_cust_id'] > 0){ 
                    if(!isset($val['sales_cust_ledger_id'])){
                        $cust = Acc_customer::where('cust_id', $val['sales_cust_id'])->first('ledger_id');
                        $info['sales_cust_ledger_id'] = $cust['ledger_id'];
                    } else{
                        $info['sales_cust_ledger_id'] = $val['sales_cust_ledger_id'];
                    }
                } else {
                     $info['sales_cust_ledger_id'] = 0;
                }
                $info['sales_cust_name'] = $val['sales_cust_name'];
                $info['sales_cust_ph'] = "{$val['sales_cust_ph']}";
                $info['sales_cust_address'] = "{$val['sales_cust_address']}";
                $info['sales_cust_tin'] = "{$val['sales_cust_tin']}";
                $info['sales_timestamp'] = strtotime($val['sales_datetime']);
                $info['sales_date'] = $val['sales_date'];
                $info['sales_time'] = $val['sales_time'];
                $info['sales_datetime'] = $val['sales_date'] . ' ' . $val['sales_time'];
                $info['sales_total'] = $val['sales_total'];
                $info['sales_discount'] = $val['sales_discount'];
                $info['sales_disc_promo'] = $val['sales_disc_promo'];
                $info['sales_disc_loyalty'] = $val['sales_disc_loyalty'];
                $info['sales_paid'] = $val['sales_paid'];
                $info['sales_balance'] = $val['sales_balance'];
                $info['sales_profit'] = (int)$val['sales_profit'];
                $info['sales_rec_amount'] = $val['sales_rec_amount'];
                $info['sales_tax'] = $val['sales_tax'];
                $info['sales_flags'] = $val['sales_flags'];
                $info['sales_notes'] = "{$val['sales_notes']}";
                $info['sales_pay_type'] = $val['sales_pay_type'];
                $info['sales_agent_ledger_id'] = $val['sales_agent_ledger_id'];
                $info['sales_acc_ledger_id'] = $val['sales_acc_ledger_id'];
                $info['sales_order_no'] = $val['sales_order_no'];
                $info['sales_order_type'] = $val['sales_order_type'];
                $info['sales_branch_inv'] = $val['sales_branch_inv'];
                $info['sales_branch_inv2'] = $val['sales_branch_inv2'];
                $info['sales_tax_type'] = $val['sales_tax_type'];
                $info['sales_due_date'] = (isset($val['sales_due_date']) && $val['sales_due_date'] != '0000-00-00') ? $val['sales_due_date'] : $val['sales_date'];

                $info['branch_id'] = $this->request->branch_id;
                $info['van_id'] = 0;
                $info['godown_id'] = $val['godown_id'];
                if(isset($val['sales_added_by'])) {
                    $info['sales_added_by'] = $val['sales_added_by'];
                }
                $info['sync_completed'] = 0;
                $info['sync_code'] = $this->request->sync_code;
                $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                $sales = SalesMaster::updateOrCreate(
                    [
                        'sales_branch_inv' => $val['sales_branch_inv'], 
                        'branch_id' => $this->request->branch_id, 
                        'van_id' => 0 
                    ],
                    $info
                );

                $ret[] = array(
                    'sales_id' => $sales->sales_id,
                    'server_sync_time' => $sales->server_sync_time,
                    'sales_inv_no' => $sales->sales_inv_no,
                    'sales_branch_inv2' => $sales->sales_branch_inv2,
                );
            }
        }
        return parent::displayData($ret);
    }

}
