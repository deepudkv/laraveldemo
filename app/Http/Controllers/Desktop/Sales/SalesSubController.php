<?php

namespace App\Http\Controllers\Desktop\Sales;

use App\Http\Controllers\Desktop\DesktopParent;
use App\Models\Sales\SalesSub;
use App\Models\Sales\SalesMaster;
use App\Models\Settings\TaxCategory;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class salessubcontroller extends DesktopParent {

    public $badrequest_stat = 400;
    public $where = array();
    public $request;

    public function operations(Request $request, $type){

        if($request->sync_time){
            $this->where[] = ['server_sync_time','>', $request->sync_time];
        }
        if($request->van_id) {
            $this->where[] = ['van_id','=', $request->van_id];
        } else if($request->branch_id) {
            $this->where[] = ['branch_id','=', $request->branch_id];
        }
        $this->where[] = ['sync_completed','=', 1];
        $this->request = $request;

        switch ($type) {
            case 'get_count':
                return $this->getCount();
                break;
            case 'download':
                return $this->download();
                break;
            case 'upload':
                return $this->upload();
                break;
            default:
                return response()->json(['error' => 'Invalid Endpoint', 'status' => $this->badrequest_stat]);
                break;
        }
    }

    public function getCount() {
        $ttlCount = SalesSub::where($this->where)->count();
        return parent::displayData($ttlCount);
    }

    public function download() {
        $query = SalesSub::query();
        $query->where($this->where);
        if($this->request->limit) {
            $perPage = ($this->request->part_no) ? $this->request->part_no : 0;
            $query->skip(($perPage * $this->request->limit));
            $query->take($this->request->limit);
        }
        $info = $query->get();
        if(!$info->isEmpty()){
            if((int)$info[0]['salesub_rate'] == $info[0]['salesub_rate']){
                $info[0]['salesub_rate'] = number_format($info[0]['salesub_rate'],2,".","");
            }
            if((int)$info[0]['salesub_qty'] == $info[0]['salesub_qty']){
                $info[0]['salesub_qty'] = number_format($info[0]['salesub_qty'],2,".","");
            }
            if((int)$info[0]['salesub_rem_qty'] == $info[0]['salesub_rem_qty']){
                $info[0]['salesub_rem_qty'] = number_format($info[0]['salesub_rem_qty'],2,".","");
            }
            if((int)$info[0]['salesub_discount'] == $info[0]['salesub_discount']){
                $info[0]['salesub_discount'] = number_format($info[0]['salesub_discount'],2,".","");
            }
            if((int)$info[0]['salesub_profit'] == $info[0]['salesub_profit']){
                $info[0]['salesub_profit'] = number_format($info[0]['salesub_profit'],2,".","");
            }
            if((int)$info[0]['salesub_prate'] == $info[0]['salesub_prate']){
                $info[0]['salesub_prate'] = number_format($info[0]['salesub_prate'],2,".","");
            }
            if((int)$info[0]['salesub_promo_per'] == $info[0]['salesub_promo_per']){
                $info[0]['salesub_promo_per'] = number_format($info[0]['salesub_promo_per'],2,".","");
            }
            if((int)$info[0]['salesub_promo_rate'] == $info[0]['salesub_promo_rate']){
                $info[0]['salesub_promo_rate'] = number_format($info[0]['salesub_promo_rate'],2,".","");
            }
            if((int)$info[0]['salesub_tax_per'] == $info[0]['salesub_tax_per']){
                $info[0]['salesub_tax_per'] = number_format($info[0]['salesub_tax_per'],2,".","");
            }
            if((int)$info[0]['salesub_tax_rate'] == $info[0]['salesub_tax_rate']){
                $info[0]['salesub_tax_rate'] = number_format($info[0]['salesub_tax_rate'],2,".","");
            }
        }
        return parent::displayData($info);
    }

    public function upload() {
        // $data = $this->request->json()->all();
  
        $data = $this->request->json()->all();

        $taxCats = TaxCategory::get()->toArray();
        $taxCatIds = [];
        foreach($taxCats as $taxCat) {
            if($taxCat['taxcat_id'] != 4){
                $taxCatIds[$taxCat['taxcat_tax_per']] = $taxCat;
            }
        }
     
        // $data = json_decode($this->request->data, true);
        $ret = array();
        $barchInvNo = '';
        foreach($data as $key => $val) {
            if(is_numeric($key)){

                if( $barchInvNo != $val['branch_inv_no']){
                    $barchInvNo = $val['branch_inv_no'];
                    $where = array();
                    $where[] = ['sales_branch_inv','=', $val['branch_inv_no']];
                    $where[] = ['branch_id','=', $this->request->branch_id];
                    $query = SalesMaster::query();
                    $query->where($where);
                    $saleMaster = $query->first();
                } 
                // echo '<pre>';
                // print_r($saleMaster['sales_inv_no']);
                // exit;
                $info = array();
                // $info['salesub_id'] = $val['salesub_id'];
                $info['salesub_sales_inv_no'] = (!empty($saleMaster) && $val['salesub_sales_inv_no'] == 0 )  ?  $saleMaster['sales_inv_no'] : $val['salesub_sales_inv_no'];
                $info['salesub_stock_id'] = $val['salesub_stock_id'];
                if(isset($val['salesub_branch_stock_id'])){
                    $info['salesub_branch_stock_id'] = $val['salesub_branch_stock_id'];
                }
                $info['salesub_prod_id'] = $val['salesub_prod_id'];
                $info['salesub_rate'] = $val['salesub_rate'];
                $info['salesub_qty'] = $val['salesub_qty'];
                $info['salesub_rem_qty'] = (!isset($val['salesub_rem_qty'])) ? $val['salesub_qty'] : $val['salesub_rem_qty'];
                $info['salesub_discount'] = $val['salesub_discount'];

                $info['salesub_timestamp'] = strtotime($val['salesub_date']);
                // if(isset($val['salesub_timestamp'])){
                //     $info['salesub_timestamp'] = strtotime($val['salesub_date']);
                // }
                
                $info['salesub_date'] = $val['salesub_date'];
                $info['salesub_serial'] = "{$val['salesub_serial']}";
                $info['salesub_flags'] = $val['salesub_flags'];
                $info['salesub_profit'] = $val['salesub_profit'];
                $info['salesub_unit_id'] = $val['salesub_unit_id'];
                $info['salesub_prate'] = $val['salesub_prate'];
                $info['salesub_promo_id'] = (isset($val['salesub_promo_id'])) ? $val['salesub_promo_id'] : 0;
                $info['salesub_promo_per'] = $val['salesub_promo_per'];
                $info['salesub_promo_rate'] = $val['salesub_promo_rate'];
                $info['salesub_tax_per'] = $val['salesub_tax_per'];
                $info['salesub_taxcat_id'] = (isset($val['salesub_taxcat_id']) && $val['salesub_taxcat_id'] != 0) ? $val['salesub_taxcat_id'] : ((isset($taxCatIds[$val['salesub_tax_per']])) ? $taxCatIds[$val['salesub_tax_per']]['taxcat_id'] : 1);
                $info['salesub_tax_rate'] = $val['salesub_tax_rate'];

                $info['salesub_notes'] = "{$val['salesub_notes']}";
                $info['branch_inv_no2'] = $val['branch_inv_no2'];
                $info['branch_inv_no2'] = $val['branch_inv_no2'];
                $info['salesub_added_by'] = $val['salesub_added_by'];
                $info['salesub_id2'] = $val['salesub_id2'];

                $info['branch_id'] = $this->request->branch_id;
                $info['is_web'] = $val['is_web'];
                $info['godown_id'] = $val['godown_id'];
                
                if(isset($val['server_sync_flag'])){
                    $info['server_sync_flag'] = $val['server_sync_flag'];
                    $info['local_sync_time'] = $val['local_sync_time'];
                }
            
                // $info['created_at'] = $val['created_at'];
                // $info['updated_at'] = $val['updated_at'];
                $info['van_id'] = 0;
                // if($this->request->van_id){
                //     $info['van_id'] = $this->request->van_id;
                // }
                $info['sync_completed'] = 0;
                $info['sync_code'] = $this->request->sync_code;
                $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
          
                $sales = SalesSub::updateOrCreate(
                    [
                        'salesub_id2' => $val['salesub_id2'], 
                        'branch_inv_no' => $val['branch_inv_no'], 
                        'branch_id' => $this->request->branch_id, 
                        'van_id' => 0 
                    ],
                    $info
                );

                $ret[] = array(
                    'salesub_id' => $sales->salesub_id,
                    'server_sync_time' => $sales->server_sync_time,
                    'salesub_sales_inv_no' => $sales->salesub_sales_inv_no,
                    'salesub_id2' => $sales->salesub_id2,
                );
            }
            
        }
        return parent::displayData($ret);
    }

}
