<?php

namespace App\Http\Controllers\Desktop\Sales;

use App\Http\Controllers\Desktop\DesktopParent;
use App\Models\Sales\SalesReturnMaster;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class salesreturncontroller extends DesktopParent {

    public $badrequest_stat = 400;
    public $where = array();
    public $request;

    public function operations(Request $request, $type){

        if($request->sync_time){
            $this->where[] = ['server_sync_time','>', $request->sync_time];
        }
        if($request->van_id) {
            $this->where[] = ['van_id','=', $request->van_id];
        } else if($request->branch_id) {
            $this->where[] = ['branch_id','=', $request->branch_id];
        }
        
        $this->request = $request;

        switch ($type) {
            case 'get_count':
                return $this->getCount();
                break;
            case 'download':
                return $this->download();
                break;
            case 'upload':
                return $this->upload();
                break;    
            default:
                return response()->json(['error' => 'Invalid Endpoint', 'status' => $this->badrequest_stat]);
                break;
        }
    }

    public function getCount() {
        $ttlCount = SalesReturnMaster::where($this->where)->count();
        return parent::displayData($ttlCount);
    }

    public function download() {
        $query = SalesReturnMaster::query();
        $query->where($this->where);
        if($this->request->limit) {
            $perPage = ($this->request->part_no) ? $this->request->part_no : 0;
            $query->skip(($perPage * $this->request->limit));
            $query->take($this->request->limit);
        }
        $info = $query->get();
        if(!$info->isEmpty()){
            if((int)$info[0]['salesdue_amount'] == $info[0]['salesdue_amount']){
                $info[0]['salesdue_amount'] = number_format($info[0]['salesdue_amount'],2,".","");
            }
            if((int)$info[0]['salesret_amount'] == $info[0]['salesret_amount']){
                $info[0]['salesret_amount'] = number_format($info[0]['salesret_amount'],2,".","");
            }
            if((int)$info[0]['salesret_discount'] == $info[0]['salesret_discount']){
                $info[0]['salesret_discount'] = number_format($info[0]['salesret_discount'],2,".","");
            }
            if((int)$info[0]['salesret_sales_amount'] == $info[0]['salesret_sales_amount']){
                $info[0]['salesret_sales_amount'] = number_format($info[0]['salesret_sales_amount'],2,".","");
            }
            if((int)$info[0]['salesret_sales_discount'] == $info[0]['salesret_sales_discount']){
                $info[0]['salesret_sales_discount'] = number_format($info[0]['salesret_sales_discount'],2,".","");
            }
            if((int)$info[0]['salesret_service_amount'] == $info[0]['salesret_service_amount']){
                $info[0]['salesret_service_amount'] = number_format($info[0]['salesret_service_amount'],2,".","");
            }
            if((int)$info[0]['salesret_service_discount'] == $info[0]['salesret_service_discount']){
                $info[0]['salesret_service_discount'] = number_format($info[0]['salesret_service_discount'],2,".","");
            }
            if((int)$info[0]['salesret_misc_amount'] == $info[0]['salesret_misc_amount']){
                $info[0]['salesret_misc_amount'] = number_format($info[0]['salesret_misc_amount'],2,".","");
            }
            if((int)$info[0]['salesret_tax'] == $info[0]['salesret_tax']){
                $info[0]['salesret_tax'] = number_format($info[0]['salesret_tax'],2,".","");
            }
            if((int)$info[0]['salesret_service_tax'] == $info[0]['salesret_service_tax']){
                $info[0]['salesret_service_tax'] = number_format($info[0]['salesret_service_tax'],2,".","");
            }
            
        }
        return parent::displayData($info);
    }

    public function upload() {
        // $data = json_decode($this->request->data, true);
        $data = $this->request->json()->all();
        $ret = array();
        foreach($data as $key => $val) {
            if(is_numeric($key)){
                $info = array();
                $info['salesret_id'] = $val['salesret_id'];
                $info['salesret_sales_inv_no'] = $val['salesret_sales_inv_no'];
                $info['salesret_date'] = $val['salesret_date'];
                $info['salesret_time'] = $val['salesret_time'];
                $info['salesdue_timestamp'] =  strtotime($val['salesret_date'] . ' ' . $val['salesret_time']);

                $info['salesdue_ledger_id'] = $val['salesdue_ledger_id'];
                $info['salesdue_amount'] = $val['salesdue_amount'];
                $info['salesret_cust_type'] = $val['salesret_cust_type'];
                $info['salesret_cust_id'] = $val['salesret_cust_id'];
                $info['salesret_cust_name'] = $val['salesret_cust_name'];
                $info['salesret_cust_address'] = $val['salesret_cust_address'];
                $info['salesret_cust_ph'] = $val['salesret_cust_ph'];
                $info['salesret_amount'] = $val['salesret_amount'];
                $info['salesret_discount'] = $val['salesret_discount'];
                $info['salesret_pay_type'] = $val['salesret_pay_type'];
                if(isset($val['salesret_added_by'])) {
                    $info['salesret_added_by'] = $val['salesret_added_by'];
                }
                $info['salesret_flags'] = $val['salesret_flags'];
                $info['salesret_vch_no'] = $val['salesret_vch_no'];
                $info['salesret_notes'] = $val['salesret_notes'];
                $info['salesret_sales_amount'] = $val['salesret_sales_amount'];
                $info['salesret_sales_discount'] = $val['salesret_sales_discount'];
                $info['salesret_service_amount'] = $val['salesret_service_amount'];
                $info['salesret_service_discount'] = $val['salesret_service_discount'];
                $info['salesret_servoid_id'] = $val['salesret_servoid_id'];
                $info['salesret_misc_amount'] = $val['salesret_misc_amount'];
                $info['salesret_cust_tin'] = $val['salesret_cust_tin'];
                $info['salesret_tax'] = $val['salesret_tax'];
                $info['salesret_service_tax'] = $val['salesret_service_tax'];
                $info['salesret_tax_vch_no'] = $val['salesret_tax_vch_no'];
                $info['salesret_godown_id'] = $val['salesret_godown_id'];
                $info['salesret_agent_ledger_id'] = $val['salesret_agent_ledger_id'];
                $info['salesret_pay_type2'] = $val['salesret_pay_type2'];
                $info['salesret_acc_ledger_id'] = $val['salesret_acc_ledger_id'];
                $info['salesret_datetime'] = $val['salesret_datetime'];
                $info['salesret_godown_ret'] = $val['salesret_godown_ret'];
                $info['branch_id'] = $this->request->branch_id;
                $info['is_web'] = 0;
                $info['godown_id'] = $val['godown_id'];
                $info['van_id'] = 0;
                $info['server_sync_flag'] = $val['server_sync_flag'];
                $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                $info['local_sync_time'] = $val['local_sync_time'];
                $info['created_at'] = $val['created_at'];
                $info['updated_at'] = $val['updated_at'];
                $info['sync_completed'] = 0;
            
                $sales = SalesDueSub::updateOrCreate(
                    [
                        'salesret_id' => $val['salesret_id'], 
                        'salesret_sales_inv_no' => $val['salesret_sales_inv_no'],
                        'branch_id' => $this->request->branch_id, 
                        'van_id' => 0 
                    ],
                    $info
                );

                $ret[] = array(
                    'salesret_id' => $sales->salesret_id,
                    'server_sync_time' => $sales->server_sync_time,
                    'salesret_sales_inv_no' => $sales->salesret_sales_inv_no,
                );
            }    
        }
        return parent::displayData($ret);
    }

}
