<?php

namespace App\Http\Controllers\Desktop\Sales;

use App\Http\Controllers\Desktop\DesktopParent;
use App\Models\Sales\SalesDue;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class salesduecontroller extends DesktopParent {

    public $badrequest_stat = 400;
    public $where = array();
    public $request;

    public function operations(Request $request, $type){

        if($request->sync_time){
            $this->where[] = ['server_sync_time','>', $request->sync_time];
        }
        if($request->van_id) {
            $this->where[] = ['van_id','=', $request->van_id];
        } else if($request->branch_id) {
            $this->where[] = ['branch_id','=', $request->branch_id];
        }
        
        $this->request = $request;

        switch ($type) {
            case 'get_count':
                return $this->getCount();
                break;
            case 'download':
                return $this->download();
                break;
            case 'upload':
                return $this->upload();
                break;
            default:
                return response()->json(['error' => 'Invalid Endpoint', 'status' => $this->badrequest_stat]);
                break;
        }
    }

    public function getCount() {
        $ttlCount = SalesDue::where($this->where)->count();
        return parent::displayData($ttlCount);
    }

    public function download() {
        $query = SalesDue::query();
        $query->where($this->where);
        if($this->request->limit) {
            $perPage = ($this->request->part_no) ? $this->request->part_no : 0;
            $query->skip(($perPage * $this->request->limit));
            $query->take($this->request->limit);
        }
        $info = $query->get();
        return parent::displayData($info);
    }

    public function upload() {
        // $data = json_decode($this->request->data, true);
        $data = $this->request->json()->all();
        $ret = array();
        foreach($data as $key => $val) {
            if(is_numeric($key)){
                $info = array();
                $info['sales_pay_id'] = $val['sales_pay_id'];
                $info['sales_recp_no'] = $val['sales_recp_no'];
                $info['salesdue_vch_no'] = $val['salesdue_vch_no'];
                $info['salesdue_date'] = $val['salesdue_date'];
                $info['salesdue_timestamp'] = $val['salesdue_timestamp'];
                $info['salesdue_ledger_id'] = $val['salesdue_ledger_id'];
                $info['salesdue_amount'] = $val['salesdue_amount'];
                // $info['sales_timestamp'] = strtotime($val['sales_date'] . $val['sales_time']);;
                $info['salesdue_multi'] = $val['salesdue_multi'];
            
                if(isset($val['salesdue_added_by'])) {
                    $info['salesdue_added_by'] = $val['salesdue_added_by'];
                }
                // $info['sales_datetime'] = $val['sales_date'] . ' ' . $val['sales_time'];
                $info['salesdue_flags'] = $val['salesdue_flags'];
                $info['salesdue_agent_ledger_id'] = $val['salesdue_agent_ledger_id'];
                $info['sales_pay_type'] = $val['sales_pay_type'];
                $info['sales_pay_txn_id'] = $val['sales_pay_txn_id'];
                $info['branch_id'] = $this->request->branch_id;
                $info['is_web'] = 0;
                $info['godown_id'] = $val['godown_id'];
                $info['van_id'] = 0;
                $info['server_sync_flag'] = $val['server_sync_flag'];
                $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                $info['local_sync_time'] = $val['local_sync_time'];
                $info['created_at'] = $val['created_at'];
                $info['updated_at'] = $val['updated_at'];
                $info['sync_completed'] = 0;
                // $info['sales_order_no'] = $val['sales_order_no'];
                // $info['sales_order_type'] = $val['sales_order_type'];
                // $info['sales_godownsale_id'] = $val['sales_godownsale_id'];
                $sales = SalesDue::updateOrCreate(
                    [
                        'sales_pay_id' => $val['sales_pay_id'], 
                        'sales_recp_no' => $val['sales_recp_no'],
                        'branch_id' => $this->request->branch_id, 
                        'van_id' => 0 
                    ],
                    $info
                );

                $ret[] = array(
                    'sales_pay_id' => $sales->sales_pay_id,
                    'server_sync_time' => $sales->server_sync_time,
                    'sales_recp_no' => $sales->sales_recp_no,
                );
            }
            
        }
        return parent::displayData($ret);
    }
}
