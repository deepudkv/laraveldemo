<?php

namespace App\Http\Controllers\Desktop\Sales;

use App\Http\Controllers\Desktop\DesktopParent;
use App\Models\Sales\SalesReturnSub;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class salesreturnsubcontroller extends DesktopParent {

    public $badrequest_stat = 400;
    public $where = array();
    public $request;

    public function operations(Request $request, $type){

        if($request->sync_time){
            $this->where[] = ['server_sync_time','>', $request->sync_time];
        }
        if($request->van_id) {
            $this->where[] = ['van_id','=', $request->van_id];
        } else if($request->branch_id) {
            $this->where[] = ['branch_id','=', $request->branch_id];
        }
        
        $this->request = $request;

        switch ($type) {
            case 'get_count':
                return $this->getCount();
                break;
            case 'download':
                return $this->download();
                break;
            case 'upload':
                return $this->upload();
                break;   
            default:
                return response()->json(['error' => 'Invalid Endpoint', 'status' => $this->badrequest_stat]);
                break;
        }
    }

    public function getCount() {
        $ttlCount = SalesReturnSub::where($this->where)->count();
        return parent::displayData($ttlCount);
    }

    public function download() {
        $query = SalesReturnSub::query();
        $query->where($this->where);
        if($this->request->limit) {
            $perPage = ($this->request->part_no) ? $this->request->part_no : 0;
            $query->skip(($perPage * $this->request->limit));
            $query->take($this->request->limit);
        }
        $info = $query->get();
        if(!$info->isEmpty()){
            if((int)$info[0]['salesretsub_rate'] == $info[0]['salesretsub_rate']){
                $info[0]['salesretsub_rate'] = number_format($info[0]['salesretsub_rate'],2,".","");
            }
            if((int)$info[0]['salesretsub_qty'] == $info[0]['salesretsub_qty']){
                $info[0]['salesretsub_qty'] = number_format($info[0]['salesretsub_qty'],2,".","");
            }
            if((int)$info[0]['salesretsub_tax_per'] == $info[0]['salesretsub_tax_per']){
                $info[0]['salesretsub_tax_per'] = number_format($info[0]['salesretsub_tax_per'],2,".","");
            }
            if((int)$info[0]['salesretsub_tax_rate'] == $info[0]['salesretsub_tax_rate']){
                $info[0]['salesretsub_tax_rate'] = number_format($info[0]['salesretsub_tax_rate'],2,".","");
            }
        }
        return parent::displayData($info);
    }

    public function upload() {
        // $data = json_decode($this->request->data, true);
        $data = $this->request->json()->all();
        $ret = array();
        foreach($data as $key => $val) {
            if(is_numeric($key)){
                $info = array();
                $info['salesretsub_id'] = $val['salesretsub_id'];
                $info['salesretsub_salesret_id'] = $val['salesretsub_salesret_id'];
                $info['salesretsub_sales_inv_no'] = $val['salesretsub_sales_inv_no'];
                $info['salesretsub_salesub_id'] = $val['salesretsub_salesub_id'];
                $info['salesretsub_stock_id'] = $val['salesretsub_stock_id'];
                $info['salesretsub_prod_id'] = $val['salesretsub_prod_id'];
                $info['salesretsub_rate'] = $val['salesretsub_rate'];
                $info['salesretsub_qty'] = $val['salesretsub_qty'];
                $info['salesretsub_date'] = $val['salesretsub_date'];
                $info['salesretsub_flags'] = $val['salesretsub_flags'];
                $info['salesretsub_unit_id'] = $val['salesretsub_unit_id'];
                $info['salesretsub_tax_per'] = $val['salesretsub_tax_per'];
                $info['salesretsub_tax_rate'] = $val['salesretsub_tax_rate'];
                $info['salesretsub_godown_id'] = $val['salesretsub_godown_id'];
                $info['salesretsub_server_sync'] = $val['salesretsub_server_sync'];
                $info['salesretsub_sync_timestamp'] = $val['salesretsub_sync_timestamp'];
                $info['salesretsub_server_timestamp'] = $val['salesretsub_server_timestamp'];
                $info['branch_id'] = $val['branch_id'];
                $info['is_web'] = $val['is_web'];
                $info['godown_id'] = $val['godown_id'];
                $info['van_id'] = $val['van_id'];
                $info['server_sync_flag'] = $val['server_sync_flag'];
                $info['server_sync_time'] = $val['server_sync_time'];
                $info['local_sync_time'] = $val['created_at'];
                $info['updated_at'] = $val['updated_at'];
                $info['salesret_service_tax'] = $val['salesret_service_tax'];
                $info['salesret_tax_vch_no'] = $val['salesret_tax_vch_no'];
                $info['salesret_godown_id'] = $val['salesret_godown_id'];
                $info['salesret_agent_ledger_id'] = $val['salesret_agent_ledger_id'];
                $info['salesret_pay_type2'] = $val['salesret_pay_type2'];
                $info['salesret_acc_ledger_id'] = $val['salesret_acc_ledger_id'];
                $info['salesret_datetime'] = $val['salesret_datetime'];
                $info['salesret_godown_ret'] = $val['salesret_godown_ret'];
                $info['branch_id'] = $this->request->branch_id;
                $info['is_web'] = 0;
                $info['godown_id'] = $val['godown_id'];
                $info['van_id'] = 0;
                $info['server_sync_flag'] = $val['server_sync_flag'];
                $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                $info['local_sync_time'] = $val['local_sync_time'];
                $info['created_at'] = $val['created_at'];
                $info['updated_at'] = $val['updated_at'];
                $info['sync_completed'] = 0;
            
                $sales = SalesReturnSub::updateOrCreate(
                    [
                        'salesretsub_id' => $val['salesretsub_id'], 
                        'salesretsub_sales_inv_no' => $val['salesretsub_sales_inv_no'],
                        'branch_id' => $this->request->branch_id, 
                        'van_id' => 0 
                    ],
                    $info
                );

                $ret[] = array(
                    'salesretsub_id' => $sales->salesretsub_id,
                    'server_sync_time' => $sales->server_sync_time,
                    'salesretsub_sales_inv_no' => $sales->salesretsub_sales_inv_no,
                );
            }
        }
        return parent::displayData($ret);
    }

}
