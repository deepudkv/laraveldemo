<?php

namespace App\Http\Controllers\Desktop\User;

use App\Http\Controllers\Desktop\DesktopParent;
use App\User;
use App\Models\Van\VanUsers;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class usercontroller extends DesktopParent {

    public $badrequest_stat = 400;
    public $where = array();
    public $request;


    public function operations(Request $request, $type){

        if($request->sync_time){
            $this->where[] = ['server_sync_time','>', $request->sync_time];
        }
        // if($request->van_id) {
        //     $this->where[] = ['van_id','=', $request->van_id];
        // } else
        if($request->branch_id) {
            $this->where[] = ['branch_id','=', $request->branch_id];
        }
        
        $this->request = $request;

        switch ($type) {
            case 'get_count':
                return $this->getCount();
                break;
            case 'download':
                return $this->download();
                break;
            default:
                return response()->json(['error' => 'Invalid Endpoint', 'status' => $this->badrequest_stat]);
                break;
        }
    }

    public function getCount() {
        $ttlCount = User::where($this->where)->count();
        return parent::displayData($ttlCount);
    }
    
    public function download() {

        $query = User::query();
        $query->where($this->where);
        if($this->request->limit) {
            $perPage = ($this->request->part_no) ? $this->request->part_no : 0;
            $query->skip(($perPage * $this->request->limit));
            $query->take($this->request->limit);
        }

        $info = $query->get([
            'users.usr_name', 
            'users.usr_username',
            'users.pass_md5',
            'users.usr_id',
            'server_sync_flag',
            'server_sync_time',
            'local_sync_time',
            'users.usr_type',
            'users.usr_phone',
            'users.usr_email',
            'users.usr_address',
            'users.usr_active',
            'users.usr_cash_ledger_id',
            'users.usr_default_godown_id'
        ]);
        return parent::displayData($info);
    }

}
