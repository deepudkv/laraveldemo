<?php

namespace App\Http\Controllers\Desktop;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;


class DesktopParent extends BaseController
{
    public function __construct()
    {
        
        // $this->middleware(function ($request, $next) {
        //     $lgd_usr = auth('api')->user();
        //     $lgd_usr = User::where('usr_id', 1)->first();
        //     $this->usr_id = $lgd_usr->usr_id;
        //     $this->usr_type = $lgd_usr->usr_type;
        //     $this->branch_id = $lgd_usr->branch_id;
        //     $this->per_page = ($request->per_page) ? $request->per_page : 10;
        //     $request->request->add(['company_id' => $lgd_usr->company_id,
        //         'branch_id' => $lgd_usr->branch_id,
        //         'server_sync_time' => date('Ymdhis'),
        //     ]);
        //     return $next($request);
        // });
    }

    public function displayError($error, $status_code=400)
    {
        // echo '<pre>';
        // print_r($error->errors()->first());
        // exit;
        return response()->json(['error' => $error->errors()->first(), 'status' => $status_code]);
    }

    public function displayMessage($msg, $status_code=200)
    {
        return response()->json(['message' => $msg, 'status' => $status_code]);
    }

    public function displayData($data, $status_code=200)
    {
        return response()->json($data);
    }

    public function displayJson($data, $status_code=200)
    {
        //return Response::json(array('data' => $data));
        echo  $data =  stripslashes($data);
  

     //echo response()->(['data' => $data, 'status' => $status_code]);
     
    }

    // Success response codes
    public $success_stat = 200;
    public $created_stat = 201;
    // Error response codes
    public $badrequest_stat = 400;
    public $unauthorized_stat = 401;


    
    // public $forbidden_stat = 403;
    // public $notfound_stat = 404;
    // public $methodnotallowed_stat = 405;
    // public $conflict_stat = 409;
    // public $lenghthrequired_stat = 411;
    // public $preconditionfailed_stat = 412;
    // public $toomanyrequests_stat = 429;
    // public $internalservererror_stat = 500;
    // public $serviceunavailable_stat = 503;

    // public function translate($msg)
    // {
      
    //     return isset($lang[$msg]) ? $lang[$msg] : ucfirst(str_replace("_", " ", $msg));

    // }

    // public $lang = array(
    //     'invalid_request' => 'Invalid Request',
    // );




}
