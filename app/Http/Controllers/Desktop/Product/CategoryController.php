<?php

namespace App\Http\Controllers\Desktop\Product;

use App\Http\Controllers\Desktop\DesktopParent;
use App\Models\Product\Category;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class categorycontroller extends DesktopParent {

    public $badrequest_stat = 400;
    public $where = array();
    public $request;
    
    public function operations(Request $request, $type){

        if($request->sync_time){
            $this->where[] = ['server_sync_time','>', $request->sync_time];
        }
        $this->request = $request;

        switch ($type) {
            case 'get_count':
                return $this->getCount();
                break;
            case 'download':
                return $this->download();
                break;
            case 'upload':
                return $this->upload();
                break;
            default:
                return response()->json(['error' => 'Invalid Endpoint', 'status' => $this->badrequest_stat]);
                break;
        }
    }

    public function getCount() {
        $ttlCount = Category::where($this->where)->count();
        return parent::displayData($ttlCount);
    }

    public function download() {
        $query = Category::query();
        $query->where($this->where);
        if($this->request->limit) {
            $perPage = ($this->request->part_no) ? $this->request->part_no : 0;
            $query->skip(($perPage * $this->request->limit));
            $query->take($this->request->limit);
        }
        $info = $query->get([
            'cat_id', 
            'cat_name', 
            'cat_flag', 
            'cat_pos',
            'cat_tax_cat_id',
            'server_sync_flag',
            'server_sync_time',
            'local_sync_time'
        ]);
        return parent::displayData($info);
    }

    public function upload() {
        // print_r($this->request->data);
        $data = json_decode($this->request->data, true);
        foreach($data as $val) {
            // $where[] = ['server_sync_time','>', $this->request->sync_time];
            $id = $val['cat_id'];
            $info = Category::find($id);
            if($info === null) {
                Category::create($val);
            } else {
                $input = $this->request->all();
                $info->fill($val)->save();
            }
        }
    }
    
}
