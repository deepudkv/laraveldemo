<?php

namespace App\Http\Controllers\Desktop\Common;

use App\Http\Controllers\Desktop\DesktopParent;
use Illuminate\Http\Request;
use App\Models\Company\Acc_branch;
use App\Models\Company\CompanySettings;
use Illuminate\Validation\Rule;

use validator;

class BranchController extends DesktopParent
{

    public function operations(Request $request, $type){
        if($request->branch_id){
            $this->where[] = ['branch_id','=', $request->branch_id];
        }
        
        $this->request = $request;

        switch ($type) {
            case 'get_details':
                return $this->getBranches();
                break;
            default:
                return response()->json(['error' => 'Invalid Endpoint', 'status' => $this->badrequest_stat]);
                break;
        }
    }

    public function getBranches() {
        $info = Acc_branch::where($this->where)->first();
        return parent::displayData($info);
    }

    public function companyData()
    {
        $info = CompanySettings::first();
        return parent::displayData($info);
    }

    
    public function updateCompany ()
    {
        $info = CompanySettings::first();
        return parent::displayData($info);
    }
}
