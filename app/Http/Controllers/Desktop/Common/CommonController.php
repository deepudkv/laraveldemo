<?php

namespace App\Http\Controllers\Desktop\Common;

use App\Http\Controllers\Desktop\DesktopParent;

use App\User;
use App\Models\Van\VanUsers;
use App\Models\Accounts\Acc_customer;
use App\Models\Product\Category;
use App\Models\Product\Manufacturer;
use App\Models\Product\Product;
use App\Models\Product\ProdUnit;
use App\Models\Product\Subcategory;
use App\Models\Product\Units;
use App\Models\Sales\SalesDue;
use App\Models\Sales\SalesDueSub;
use App\Models\Sales\SalesMaster;
use App\Models\Sales\SalesSub;
use App\Models\Sales\SalesReturnMaster;
use App\Models\Sales\SalesReturnSub;
use App\Models\Stock\StockBatches;
use App\Models\Sales\StockUnitRates;
use App\Models\Stocks\VanDailyStocks;
use App\Models\Sales\VanStock;
use App\Models\Van\VanTransfer;
use App\Models\Van\VanTransferReturn;
use App\Models\Van\VanTransferReturnSub;
use App\Models\Van\VanTransferSub;
use App\Models\Accounts\Acc_voucher;

use App\Models\Stocks\BranchStockLogs;
use App\Models\Stocks\BranchStocks;
use App\Models\Stocks\CompanyStocks;
use App\Models\Stocks\StockDailyUpdates;

use App\Models\Godown\GodownStock;
use App\Models\Godown\GodownStockLog;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;
use DB;

class commoncontroller extends DesktopParent {

    public $badrequest_stat = 400;
    public $where = array();
    public $request;
    public $table= array(
        'user' => 'User' ,
        'customer' => 'Acc_customer' ,
    );


    public function operations(Request $request, $type){

        // if($request->sync_time){
        //     $this->where[] = ['server_sync_time','>', $request->sync_time];
        // }
        // if($request->van_id) {
        //     $this->where[] = ['van_id','=', $request->van_id];
        // } 
        // if($request->branch_id) {
        //     $this->where[] = ['branch_id','=', $request->branch_id];
        // }
        
        $this->request = $request;
        // $this->table = $request;

        switch ($type) {
            case 'test':
                return $this->test();
                break;
            case 'get_count':
                return $this->getCount();
                break;
            case 'salemaster':
                return $this->upload_salemaster();
                break;
            case 'sync_start':
                return $this->sync_start();
                break;
            // case 'download':
            //     return $this->download();
            //     break;
            default:
                return response()->json(['error' => 'Invalid Endpoint', 'status' => $this->badrequest_stat]);
                break;
        }
    }

    public function getCount() {
        echo '<pre>';
        print_r($this->request->name);
        foreach($this->request->name as $val) {
            // $ttlCount = {$table[$val]}::where($this->where)->count();
        }
        return parent::displayData($ttlCount);
        // if()
        // $ttlCount = VanUsers::where($this->where)->count();
        // return parent::displayData($ttlCount);
    }
    public function upload_salemaster() {
        
            // $where[] = ['sales_inv_no','=', $val['sales_inv_no']];
            // // $where[] = ['branch_id','=', $val['branch_id']];
            // $where[] = ['branch_id','=', $this->request->branch_id];
            // $where[] = ['van_id','=', 0];
            // // $id = $val['sales_inv_no'];
            // $extinfo = SyncSalesMaster::where($where)->get();
            // print_r($extinfo);
            // if(!$extinfo) {
            //     echo 'insert';
            //     SyncSalesMaster::create($info);
            // } else {
            //     // print_r($info);
            //     echo 'update';
            //     SyncSalesMaster::where($where)->update($info);
            // }
            // SyncSalesMaster::create($info);
    }

    public function sync_start() {

        $this->where[] = ['sync_completed','=', 0];
        $this->where[] = ['sync_code','=', $this->request->sync_code];
        $query = SalesMaster::query();
        $query->where($this->where);
        $info1 = $query->get();
      
        foreach($info1 as $key => $val){
            $where = $this->where;
            $where[] = ['branch_inv_no','=', $val['sales_branch_inv']];
            SalesSub::where($where)->update([
                'salesub_sales_inv_no' => $val['sales_inv_no'],
                'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)
                ]);
            SalesMaster::where($this->where)->update(['sync_completed' => 1, 'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)]);
        } 

        $salesSub = SalesSub::where($this->where)->get();
        $voucherEntryInv = [];
        foreach($salesSub as $key => $val){
            // salesub_added_by
            // $where = array();
            // $where[] = ['sales_branch_inv','=', $val['branch_inv_no']];
            // $salesMaster = SalesMaster::where($where)->first();
            if($val['salesub_flags'] == 1) {
                $where = array();
                $where[] = ['cmp_prd_id','=', $val['salesub_prod_id']];
                $cmpStock = CompanyStocks::where($where)->first();
                $fnlQty = $cmpStock->cmp_stock_quantity - $val['salesub_qty'];
                CompanyStocks::where($where)->update([
                    'cmp_stock_quantity' => $fnlQty,
                    'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)
                ]);

                $where = array();
                $where[] = ['sdu_branch_stock_id','=', $val['salesub_branch_stock_id']];
                $where[] = ['sdu_prd_id','=', $val['salesub_prod_id']];
                $where[] = ['sdu_date','=', $val['salesub_date']];
                // $where[] = ['sdu_date','=', Date('Y-m-d', $val['salesub_date'])];
                $where[] = ['branch_id','=', $this->request->branch_id];
                $stock = StockDailyUpdates::where($where)->first();
                if(!empty($stock)){
                    $fnlQty = $stock->sdu_stock_quantity - $val['salesub_qty'];
                    StockDailyUpdates::where($where)->update([
                        'sdu_stock_quantity' => $fnlQty,
                        'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)
                    ]);
                } else {
                                
                    $info = array();
                    $info['sdu_branch_stock_id'] = $val['salesub_branch_stock_id'];
                    $info['sdu_prd_id'] = $val['salesub_prod_id'];
                    $info['sdu_stock_id'] = $val['salesub_stock_id'];
                    // $info['sdu_date'] = Date('Y-m-d', $val['salesub_date']);
                    $info['sdu_date'] = $val['salesub_date'];
                    $info['sdu_stock_quantity'] = $val['salesub_qty'] * -1;
                    $info['branch_id'] = $this->request->branch_id;
                    $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                    $stock = StockDailyUpdates::create($info);

                }

                // $where = array();
                // // $where[] = ['branch_stock_id','=', $val['salesub_branch_stock_id']];
                // $where[] = ['bs_prd_id','=', $val['salesub_prod_id']];
                // $where[] = ['bs_branch_id','=', 0];
                // // print_r($where);
                // $stock = BranchStocks::where($where)->first();
                // // print_r($stock);
                // $fnlQty = $stock->bs_stock_quantity - $val['salesub_qty'];
                // $qty = array(
                //     'bs_stock_quantity' => $fnlQty,
                //     'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)
                // );

                // if($val['godown_id'] == 0){
                //     $qty['bs_stock_quantity_shop'] = $stock->bs_stock_quantity_shop - $val['salesub_qty'];
                // } else {
                //     $qty['bs_stock_quantity_gd'] = $stock->bs_stock_quantity_gd - $val['salesub_qty'];
                // }
                // BranchStocks::where($where)->update($qty);


                $where = array();
                // $where[] = ['branch_stock_id','=', $val['salesub_branch_stock_id']];
                $where[] = ['bs_prd_id','=', $val['salesub_prod_id']];
                $where[] = ['bs_branch_id','=', $this->request->branch_id];
                $stock = BranchStocks::where($where)->first();
                if(!empty($stock)){
                    $fnlQty = $stock->bs_stock_quantity - $val['salesub_qty'];
                    $qty = array(
                        'bs_stock_quantity' => $fnlQty,
                        'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)
                    );

                    if($val['godown_id'] == 0){
                        $qty['bs_stock_quantity_shop'] = $stock->bs_stock_quantity_shop - $val['salesub_qty'];
                    } else {
                        $qty['bs_stock_quantity_gd'] = $stock->bs_stock_quantity_gd - $val['salesub_qty'];
                    }
                    BranchStocks::where($where)->update($qty);
                } else {
                    $info = array();
                    $info['bs_stock_id'] = $val['salesub_stock_id'];
                    $info['bs_prd_id'] = $val['salesub_prod_id'];
                    $info['bs_stock_quantity'] = $val['salesub_qty'] * -1;
                    $info['bs_stock_quantity_shop'] = ($val['godown_id'] == 0) ? $val['salesub_qty'] * -1 : 0;
                    $info['bs_stock_quantity_gd'] = ($val['godown_id'] != 0) ? $val['salesub_qty'] * -1 : 0;
                    $info['bs_stock_quantity_van'] = 0;
                    $info['bs_prate'] = 0;
                    $info['bs_avg_prate'] = 0;
                    $info['bs_srate'] = 0;
                    $info['bs_batch_id'] = 0;
                    $info['bs_expiry'] = 0;
                    $info['bs_branch_id'] = $this->request->branch_id;
                    $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                    $stock = BranchStocks::create($info);
                }


                if($val['godown_id'] > 0){
                    $where1 = array();
                    $where1[] = ['gs_godown_id','=', $val['godown_id']];
                    $where1[] = ['gs_branch_stock_id','=', $val['salesub_branch_stock_id']];
                    $where1[] = ['gs_stock_id','=', $val['salesub_stock_id']];
                    $where1[] = ['gs_prd_id','=', $val['salesub_prod_id']];
                    $stock = GodownStock::where($where1)->first();
                    if(!empty($stock)){
                        $fnlQty = $stock->gs_qty - $val['salesub_qty'];
                        GodownStock::where($where1)->update([
                            'gs_qty' => $fnlQty,
                            'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)
                        ]);
                    } else {
                        $info = array();
                        $info['gs_godown_id'] = $val['godown_id'];
                        $info['gs_branch_stock_id'] = $val['salesub_branch_stock_id'];
                        $info['gs_stock_id'] = $val['salesub_stock_id'];
                        $info['gs_prd_id'] = $val['salesub_prod_id'];
                        $info['gs_qty'] = $val['salesub_qty'] * -1;
                        $info['gs_date'] = $val['salesub_date'];
                        $info['branch_id'] = $this->request->branch_id;
                        $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                        $stock = GodownStock::create($info);
                    }

                    $info = array();
                    $info['gsl_gdwn_stock_id'] = $stock->gs_id;
                    $info['gsl_branch_stock_id'] = $val['salesub_branch_stock_id'];
                    $info['gsl_stock_id'] = $val['salesub_stock_id'];
                    $info['gsl_prd_id'] = $val['salesub_prod_id'];
                    $info['gsl_prod_unit'] = $val['salesub_unit_id'];
                    $info['gsl_qty'] = $val['salesub_qty'] * -1;
                    $info['gsl_from'] = $val['godown_id'];
                    $info['gsl_to'] = 0;
                    $info['gsl_vchr_type'] = 5;
                    $info['gsl_date'] = $val['salesub_date'];
                    $info['gsl_added_by'] = $val['salesub_added_by'];
                    $info['branch_id'] = $this->request->branch_id;
                    $info['server_sync_flag'] = date('ymdHis') . substr(microtime(), 2, 6);
                    $data = GodownStockLog::create($info);
                }

                if(!in_array($val['salesub_sales_inv_no'], $voucherEntryInv)) {
                    $voucherEntryInv[] = $val['salesub_sales_inv_no'];
                    app('App\Http\Controllers\Api\Migration\ledgerOpeingbalanceController')->setsalessubtovoucher($val['salesub_sales_inv_no']);
                }
            }
            $subWhere = $this->where;
            $subWhere[] = ['salesub_id','=', $val['salesub_id']];
            SalesSub::where($subWhere)->update(['sync_completed' => 1, 'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)]);

        } 

        $salesDueSub = SalesDueSub::where($this->where)->get();
        foreach($salesDueSub as $key => $val){
            // sales due account (voucher) code comes here
            $dueSubWhere = $this->where;
            $dueSubWhere[] = ['salesduesub_id','=', $val['salesduesub_id']];
            SalesDueSub::where($dueSubWhere)->update(['sync_completed' => 1, 'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)]);

        } 

        if($salesSub->isEmpty() && $info1->isEmpty()){
            return parent::displayData(array('status' =>'success'));
        } else {
            return parent::displayData(array('status' =>'success'));
        }
    }

    function updateVoucher() {
        
    }
    // public function test() {
    //     // $result1 = VanTransferSub::get();
    //     // $result = VanTransferSub::groupBy('vantransub_prod_id')
    //     // ->selectRaw('*, sum(vantransub_qty) as sum')
    //     // ->get();
        
    //     $result = DB::select("SELECT *, sum(vantransub_qty) AS 'trans_ttlqty'
    //     FROM erp_van_transfer_sub A 
    //     LEFT JOIN
    //     (
    //     SELECT *, sum(vantranretsub_qty) AS 'trans_ret_ttlqty' FROM erp_van_transfer_return_sub
    //     GROUP BY vantranretsub_prod_id
    //     ) B
    //     ON A.vantransub_prod_id = B.vantranretsub_prod_id
    //     GROUP BY A.vantransub_prod_id");

    //     return parent::displayData($result);
    //     exit;
    //     // print_r($this->request->name);
    //     // foreach($this->request->name as $val) {
    //     //     // $ttlCount = {$table[$val]}::where($this->where)->count();
    //     // }
    //     // return parent::displayData($ttlCount);
    //     // if()
    //     // $ttlCount = VanUsers::where($this->where)->count();
    //     // return parent::displayData($ttlCount);
    // }
   

}
