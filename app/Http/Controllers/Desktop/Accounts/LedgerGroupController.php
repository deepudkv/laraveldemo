<?php

namespace App\Http\Controllers\Desktop\Accounts;

use App\Http\Controllers\Desktop\DesktopParent;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Models\Accounts\Acc_group;
use DB;

class LedgerGroupController extends DesktopParent
{

    public $badrequest_stat = 400;
    public $where = array();
    public $request;

    public function operations(Request $request, $type){

        if($request->sync_time){
            $this->where[] = ['server_sync_time','>', $request->sync_time];
        }
        $this->request = $request;

        switch ($type) {
            case 'get_count':
                return $this->getCount();
                break;
            case 'download':
                return $this->download();
                break;
            default:
                return response()->json(['error' => 'Invalid Endpoint', 'status' => $this->badrequest_stat]);
                break;
        }
    }

    public function getCount() {
        $ttlCount = Acc_group::where($this->where)->count();
        return parent::displayData($ttlCount);
    }

    public function download() {
        $query = Acc_group::query();
        $query->where($this->where);
        if($this->request->limit) {
            $perPage = ($this->request->part_no) ? $this->request->part_no : 0;
            $query->skip(($perPage * $this->request->limit));
            $query->take($this->request->limit);
        }
        $info = $query->get();
        return parent::displayData($info);
    }

}
