<?php

namespace App\Http\Controllers\Desktop\Register;

// use App\Http\Controllers\Van\VanParent;
use App\Http\Controllers\Controller;
use App\Models\Company\Companies;
use App\Models\Company\CompanySettings;
use App\Models\Company\Acc_branch;
use App\Models\Van\Van;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class registercontroller extends Controller {

    public $badrequest_stat = 400;

    public function operations(Request $request, $type){

        switch ($type) {
            case 'desk_register':
                return $this->desk_register($request);
                break;
            case 'desk_register_server':
                return $this->desk_register_server($request);
                break;
            default:
                return response()->json(['error' => 'Invalid Endpoint', 'status' => $this->badrequest_stat]);
                break;
        }
    }


    public function desk_register($request) {
        $validator = Validator::make(
            $request->all(),
            [
                'company_code' => 'required',
                'branch_code' => 'required',
                'branch_password' => 'required',
            ],
            [
                'company_code.required' => 'Company Code Required',
                'branch_code.required' => 'Branch Code Required',
                'branch_password.required' => 'Branch Password Required',
            ]
        );
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first(), 'status' => $this->badrequest_stat]);
        } 

        $company = Companies::where('cpm_code', $request->company_code)->first();
        if(empty($company)){
            return response()->json(['error' => 'Invalid company code', 'status' => $this->badrequest_stat]);
        }
        $branch = Acc_branch::where('branch_code', $request->branch_code)->first();
        if(empty($branch)){
            return response()->json(['error' => 'Invalid Branch code', 'status' => $this->badrequest_stat]);
        } else {
            if($branch['branch_password'] != md5($request->branch_password)){
                return response()->json(['error' => 'Invalid Branch Password', 'status' => $this->badrequest_stat]);
            }
        }
        $out = array();
        $out['token'] = encrypt(array(
            'comapny_id' => $company['id'], 
            'branch_id' => $branch['branch_id'],
            'company_code' => $request->company_code
            ));
        if (strpos(strtolower($company['cmp_sub_domain']), 'http') !== false) {
            $out['endpoint'] = rtrim($company['cmp_sub_domain'], '/'). '/';
        } else{
            $out['endpoint'] = url('/') . '/' . (($company['cmp_sub_domain']) ?  $company['cmp_sub_domain']. '/' : '');
        }
        $out['up_limit'] = $company['up_limit'];
        return response()->json(array($out));
        
    }

    public function desk_register_server($request) {
        $validator = Validator::make(
            $request->all(),
            [
                'company_code' => 'required',
                'branch_code' => 'required',
                'branch_password' => 'required',
            ],
            [
                'company_code.required' => 'Company Code Required',
                'branch_code.required' => 'Branch Code Required',
                'branch_password.required' => 'Branch Password Required',
            ]
        );
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first(), 'status' => $this->badrequest_stat]);
        } 

        $company = CompanySettings::where('cmp_code', $request->company_code)->first();
        if(empty($company)){
            return response()->json(['error' => 'Invalid company code in client', 'status' => $this->badrequest_stat]);
        }
        $branch = Acc_branch::where('branch_code', $request->branch_code)->first();
        if(empty($branch)){
            return response()->json(['error' => 'Invalid Branch code', 'status' => $this->badrequest_stat]);
        } else {
            if($branch['branch_password'] != md5($request->branch_password)){
                return response()->json(['error' => 'Invalid Branch Password', 'status' => $this->badrequest_stat]);
            }
        }
        $out = array();
        $out['token'] = encrypt(array(
            'comapny_id' => $company['id'], 
            'branch_id' => $branch['branch_id'],
            'company_code' => $request->company_code
            ));
        $out['endpoint'] = $request->end_point;
        // if (strpos(strtolower($company['cmp_sub_domain']), 'http') !== false) {
        //     $out['endpoint'] = rtrim($company['cmp_sub_domain'], '/'). '/';
        // } else{
        //     $out['endpoint'] = url('/') . '/' . (($company['cmp_sub_domain']) ?  $company['cmp_sub_domain']. '/' : '');
        // }
        $out['up_limit'] = $company['cmp_up_limit'];
        return response()->json(array($out));
        
    }

}
