<?php

namespace App\Http\Controllers\Van\Product;

use App\Http\Controllers\Van\VanParent;
use App\Models\Product\Category;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Models\Van\Van;
use App\Models\Van\VanCategories;
use Validator;

class categorycontroller extends VanParent {

    public $badrequest_stat = 400;
    public $where = array();
    public $request;
    
    public function operations(Request $request, $type){

            // if($request->sync_time){
            //     $this->where[] = ['categories.server_sync_time','>', $request->sync_time];
            // }
        $this->request = $request;

        switch ($type) {
            case 'get_count':
                return $this->getCount();
                break;
            case 'download':
                return $this->download();
                break;
            default:
                return response()->json(['error' => 'Invalid Endpoint', 'status' => $this->badrequest_stat]);
                break;
        }
    }

    public function getCount() {
        $van = Van::where(['van_id' => $this->request->van_id])->first();
        $query = Category::query();
        if($van['all_cat'] == 0){
            $query->join('van_categories','van_categories.vc_cat_id','categories.cat_id');
            $this->where[] = ['vc_van_id','=', $this->request->van_id];
            $this->where[] = ['vc_cat_flag','=', 1];
        } else{
            if($this->request->sync_time){
                $this->where[] = ['server_sync_time','>', $this->request->sync_time];
            }
        }
        $query->where($this->where);
        $ttlCount = $query->count();
        // $ttlCount = Category::where($this->where)->count();
        return parent::displayData($ttlCount);
    }

    public function download() {
        $van = Van::where(['van_id' => $this->request->van_id])->first();
        $query = Category::query();
        $where = array();
        if($van['all_cat'] == 0){
            $query->join('van_categories','van_categories.vc_cat_id','categories.cat_id');
            $where[] = ['vc_van_id','=', $this->request->van_id];
            $where[] = ['vc_cat_flag','=', 1];
        } else {
            if($this->request->sync_time){
                $where[] = ['server_sync_time','>', $this->request->sync_time];
            }
        }
        $query->where($where);
        if($this->request->limit) {
            $perPage = ($this->request->part_no) ? $this->request->part_no : 0;
            $query->skip(($perPage * $this->request->limit));
            $query->take($this->request->limit);
        }
        $info = $query->get(['cat_id', 'cat_name', 'cat_flag', 'cat_pos']);
        if(!$info->isEmpty()) {
            return parent::displayData($info);
        } else{
            // unset($this->where[0]);
            // $syncTime = Category::where($this->where)->max('server_sync_time');
            $syncTime = ($van['all_cat'] == 0) ? $this->request->sync_time : Category::max('server_sync_time');
            return parent::sendResponseSyncTime($syncTime);
        }
    }
    
}
