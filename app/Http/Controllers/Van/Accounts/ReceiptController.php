<?php

namespace App\Http\Controllers\Van\Accounts;

use App\Http\Controllers\Van\VanParent;
use App\Models\Accounts\AccReceipt;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Models\Van\Van;
use Validator;

class receiptcontroller extends VanParent {

    public $badrequest_stat = 400;
    public $where = array();
    public $request;
    
    public function operations(Request $request, $type){

        if($request->sync_time){
            $this->where[] = ['server_sync_time','>', $request->sync_time];
        }
        if($request->van_id) {
            $this->where[] = ['van_id','=', $request->van_id];
        }
       
        $this->request = $request;

        switch ($type) {
            case 'get_count':
                return $this->getCount();
                break;
            case 'download':
                return $this->download();
                break;
            case 'upload':
                return $this->upload();
                break;
            default:
                return response()->json(['error' => 'Invalid Endpoint', 'status' => $this->badrequest_stat]);
                break;
        }
    }

    public function getCount() {
        $ttlCount = AccReceipt::where($this->where)->count();
        return parent::displayData($ttlCount);
    }

    public function download() {
        $query = AccReceipt::query();
        $query->where($this->where);
        if($this->request->limit) {
            $perPage = ($this->request->part_no) ? $this->request->part_no : 0;
            $query->skip(($perPage * $this->request->limit));
            $query->take($this->request->limit);
        }
        $info = $query->get();
        if(!$info->isEmpty()) {
            return parent::displayData($info);
        } else{
            unset($this->where[0]);
            $syncTime = AccReceipt::where($this->where)->max('server_sync_time');
            return parent::sendResponseSyncTime($syncTime);
        }
    }

    public function upload() {
        $where = array();
        $where[] = ['van_id','=', $this->request->van_id];
        $van = Van::where($where)->first();
        // $data = $this->request->json()->all();
        $data = $this->request->data;
        $ret = array();
        foreach($data as $key => $val) {

            

            if(isset($val['rec_no'])){
                $info['rec_no'] = $val['rec_no'];
            } else {
                $where = array();
		        $where[] = ['van_rec_no','=', $val['van_rec_no']];
                $voucher = AccReceipt::where($where)->first();
                if(empty($voucher)){
                    $max = AccReceipt::max('rec_no');
                    $info['rec_no'] = (empty($max)) ? 1 : $max+1;
                }
            }
            $info['branch_rec_no'] = $val['van_rec_no'];
            $info['van_rec_no'] = $val['van_rec_no'];
            $info['rec_date'] = $val['rec_date'];
            $info['rec_time'] = $val['rec_time'];
            $info['rec_datetime'] = $val['rec_datetime'];
            $info['rec_timestamp'] = strtotime($val['rec_datetime']);
            $info['rec_acc_ledger_id'] = $van['van_cash_ledger_id'];
            $info['rec_ttl_amount'] = $val['rec_ttl_amount'];
            $info['rec_note'] = ($val['rec_note']) ? $val['rec_note'] : '';
            $info['branch_id'] = $this->request->branch_id;
            $info['van_id'] = $this->request->van_id;
            $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            $info['is_web'] = 0;
            $info['rec_vchtype_id'] = (isset($val['rec_vchtype_id']) && $val['rec_vchtype_id'] != 0) ? $val['rec_vchtype_id']: 22;
            $info['sync_completed'] = 0;
            $info['sync_code'] = $this->request->sync_code;
            $recp = AccReceipt::updateOrCreate(
                [
                    'van_rec_no' => $val['van_rec_no'], 
                    'branch_id' => $this->request->branch_id, 
                ],
                $info
            );

            $ret[] = array(
                'rec_id' => $recp->rec_id,
                'rec_no' => $recp->rec_no,
                'server_sync_time' => $recp->server_sync_time,
                'auto_inc_id' => $val['auto_inc_id'],
            );
            
        }
        return parent::displayData($ret);
    }
    
}
