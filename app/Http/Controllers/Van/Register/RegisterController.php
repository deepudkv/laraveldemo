<?php

namespace App\Http\Controllers\Van\Register;

// use App\Http\Controllers\Van\VanParent;
use App\Http\Controllers\Controller;
use App\Models\Company\Companies;
use App\Models\Company\Acc_branch;
use App\Models\Company\CompanySettings;
use App\Models\Van\Van;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class registercontroller extends Controller {

    public $badrequest_stat = 400;

    public function operations(Request $request, $type){

        switch ($type) {
            case 'app_register':
                return $this->register($request);
                break;
      
            case 'app_register_server':
                return $this->app_register_server($request);
                break;
      
            default:
                return response()->json(['error' => 'Invalid Endpoint', 'status' => $this->badrequest_stat]);
                break;
        }
    }

    public function register($request) {
        $validator = Validator::make(
            $request->all(),
            [
                'company_code' => 'required',
                'branch_code' => 'required',
                'van_code' => 'required',
                'van_pass' => 'required',
            ],
            [
                'company_code.required' => 'Company Code Required',
                'branch_code.required' => 'Branch Code Required',
                'van_code.required' => 'Van Code Required',
                'van_pass.required' => 'Van Password Required',
            ]
        );
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first(), 'status' => $this->badrequest_stat]);
        } 

        $company = Companies::where('cpm_code', $request->company_code)->first();
        if(empty($company)){
            return response()->json(['error' => 'Invalid company code', 'status' => $this->badrequest_stat]);
        }
        $branch = Acc_branch::where('branch_code', $request->branch_code)->first();
        if(empty($branch)){
            return response()->json(['error' => 'Invalid Branch code', 'status' => $this->badrequest_stat]);
        }
        // check branch id also 
        $van = Van::where(['van_code' => $request->van_code])->first();
        if(empty($van)){
            return response()->json(['error' => 'Invalid Van Code', 'status' => $this->badrequest_stat]);
        } else {
            if($van['branch_id'] != $branch['branch_id']){
                return response()->json(['error' => 'No Van Found Under This Branch', 'status' => $this->badrequest_stat]);
            }
            if($van['van_password'] != $request->van_pass){
                return response()->json(['error' => 'Invalid Password', 'status' => $this->badrequest_stat]);
            }
        }
        $out = array(
            'branch_name' => $branch['branch_name'],
            'branch_address' => $branch['branch_address'],
            'branch_address2' => $branch['branch_address2'],
            'branch_phone' => $branch['branch_phone'],
            'branch_mob' => $branch['branch_mob'],
            'branch_email' => $branch['branch_email'],
            'branch_tin' => $branch['branch_tin'],
            'branch_reg_no' => $branch['branch_reg_no'],
            'van_id' => $van['van_id'], 
            'branch_id' => $branch['branch_id'],
            'company_code' => $request->company_code,
            );
        $out['van_name'] = $van['van_name'];
        $out['van_token'] = encrypt(array(
            'comapny_id' => $company['id'], 
            'van_id' => $van['van_id'], 
            'branch_id' => $branch['branch_id'],
            'van_line_id' => $van['van_vanline_id'],
            'company_code' => $request->company_code,
            ));
        $out['van_reg_no'] = $van['van_reg_no'];
        $out['photo'] = url('/') . '/images/van/' . $van['photo'];
        // $out['endpoint'] = url('/') . '/' . (($company['cmp_sub_domain']) ?  $company['cmp_sub_domain']. '/' : '');

        if (strpos(strtolower($company['cmp_sub_domain']), 'http') !== false) {
            $out['endpoint'] = rtrim($company['cmp_sub_domain'], '/'). '/';
        } else{
            $out['endpoint'] = url('/') . '/' . (($company['cmp_sub_domain']) ?  $company['cmp_sub_domain']. '/van/' : '/van/');
        }
        return response()->json(['data' => $out, 'status' => 200]);
    }

    public function app_register_server($request) {
        $validator = Validator::make(
            $request->all(),
            [
                'company_code' => 'required',
                'branch_code' => 'required',
                'van_code' => 'required',
                'van_pass' => 'required',
            ],
            [
                'company_code.required' => 'Company Code Required',
                'branch_code.required' => 'Branch Code Required',
                'van_code.required' => 'Van Code Required',
                'van_pass.required' => 'Van Password Required',
            ]
        );
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first(), 'status' => $this->badrequest_stat]);
        } 

        $company = CompanySettings::where('cmp_code', $request->company_code)->first();
        if(empty($company)){
            return response()->json(['error' => 'Invalid company code in client', 'status' => $this->badrequest_stat]);
        }

        $branch = Acc_branch::where('branch_code', $request->branch_code)->first();
        if(empty($branch)){
            return response()->json(['error' => 'Invalid Branch code', 'status' => $this->badrequest_stat]);
        }
        // check branch id also 
        $van = Van::where(['van_code' => $request->van_code])->first();
        if(empty($van)){
            return response()->json(['error' => 'Invalid Van Code', 'status' => $this->badrequest_stat]);
        } else {
            if($van['branch_id'] != $branch['branch_id']){
                return response()->json(['error' => 'No Van Found Under This Branch', 'status' => $this->badrequest_stat]);
            }
            if($van['van_password'] != $request->van_pass){
                return response()->json(['error' => 'Invalid Password', 'status' => $this->badrequest_stat]);
            }
        }
        $out = array(
            'branch_name' => $branch['branch_name'],
            'branch_address' => $branch['branch_address'],
            'branch_address2' => $branch['branch_address2'],
            'branch_phone' => $branch['branch_phone'],
            'branch_mob' => $branch['branch_mob'],
            'branch_email' => $branch['branch_email'],
            'branch_tin' => $branch['branch_tin'],
            'branch_reg_no' => $branch['branch_reg_no'],
            'van_id' => $van['van_id'], 
            'branch_id' => $branch['branch_id'],
            'company_code' => $request->company_code,
            );
        $out['van_name'] = $van['van_name'];
        $out['van_token'] = encrypt(array(
            'comapny_id' => $company['id'], 
            'van_id' => $van['van_id'], 
            'branch_id' => $branch['branch_id'],
            'van_line_id' => $van['van_vanline_id'],
            'company_code' => $request->company_code,
            ));
        $out['van_reg_no'] = $van['van_reg_no'];
        $out['photo'] = $request->end_point . '/images/van/' . $van['photo'];
        // $out['endpoint'] = $request->end_point;
        $out['endpoint'] = $request->end_point . 'van/';
        $out['up_limit'] = $company['cmp_up_limit'];
        return response()->json(['data' => $out, 'status' => 200]);
        // return response()->json(array($out));
        
    }
    

}
