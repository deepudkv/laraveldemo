<?php

namespace App\Http\Controllers\Van\Common;

use App\Http\Controllers\Van\VanParent;

use App\User;
use App\Models\Van\VanUsers;
use App\Models\Accounts\Acc_customer;
use App\Models\Product\Category;
use App\Models\Product\Manufacturer;
use App\Models\Product\Product;
use App\Models\Product\ProdUnit;
use App\Models\Product\Subcategory;
use App\Models\Product\Units;
use App\Models\Sales\SalesDue;
use App\Models\Sales\SalesDueSub;
use App\Models\Sales\SalesMaster;
use App\Models\Sales\SalesSub;
use App\Models\Sales\SalesReturnMaster;
use App\Models\Sales\SalesReturnSub;
use App\Models\Stocks\StockBatches;
use App\Models\Stocks\StockUnitRates;
use App\Models\Van\VanDailyStocks;
use App\Models\Van\VanStock;
use App\Models\Van\VanTransfer;
use App\Models\Van\VanTransferReturn;
use App\Models\Van\VanTransferReturnSub;
use App\Models\Van\VanTransferSub;
use App\Models\Accounts\Acc_voucher;

use App\Models\Stocks\BranchStockLogs;
use App\Models\Stocks\BranchStocks;
use App\Models\Stocks\CompanyStocks;
use App\Models\Stocks\StockDailyUpdates;

use App\Models\Godown\GodownStock;
use App\Models\Godown\GodownStockLog;

use App\Models\Accounts\AccReceipt;
use App\Models\Accounts\Acc_ledger;
use App\Models\Accounts\AccReceiptSub;

use App\Models\Company\Acc_branch;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class commoncontroller extends VanParent {

    public $badrequest_stat = 400;
    public $where = array();
    public $request;
    public $table = array(
        'user' => 'App\User' ,
        'customer' => 'App\Models\Accounts\Acc_customer',
        'van_users' => 'App\Models\Van\VanUsers',
        'category' => 'App\Models\Product\Category',
        'manufacture' => 'App\Models\Product\Manufacturer',
        'products' => 'App\Models\Product\Product',
        'product_units' => 'App\Models\Product\ProdUnit',
        'sub_category' => 'App\Models\Product\Subcategory',
        'units' => 'App\Models\Product\Units',
        'sales_due' => 'App\Models\Sales\SalesDue',
        'sales_due_sub' => 'App\Models\Sales\SalesDueSub',
        'sales_master' => 'App\Models\Sales\SalesMaster',
        'sales_sub' => 'App\Models\Sales\SalesSub',
        'sales_return' => 'App\Models\Sales\SalesReturnMaster',
        'sales_return_sub' => 'App\Models\Sales\SalesReturnSub',
        'stock_batches' => 'App\Models\Stocks\StockBatches',
        'stock_unit_rates' => 'App\Models\Stocks\StockUnitRates',
        'van_daily_stock' => 'App\Models\Van\VanDailyStocks',
        'van_stock' => 'App\Models\Van\VanStock',
        'van_transfer' => 'App\Models\Van\VanTransfer',
        'van_transfer_sub' => 'App\Models\Van\VanTransferSub',
        'van_transfer_return' => 'App\Models\Van\VanTransferReturn',
        'van_transfer_return_sub' => 'App\Models\Van\VanTransferReturnSub',
        'acc_receipt' => 'App\Models\Accounts\AccReceipt'
    );
    public $vanCheckExcl = array(
        'user', 
        'customer', 
        'category', 
        'manufacture', 
        'products',
        'product_units',
        'sub_category',
        'units',
        'stock_batches',
        'stock_unit_rates',
        'van_daily_stock'
    );


    public function operations(Request $request, $type){

        $this->request = $request;
        // $this->table = $request;

        switch ($type) {
            case 'get_count':
                return $this->getCount();
                break;
            case 'sync_start':
                return $this->sync_start();
                break;
            default:
                return response()->json(['error' => 'Invalid Endpoint', 'status' => $this->badrequest_stat]);
                break;
        }
    }

    public function getCount() {

        $ret = $this->request->data;
        // print_r($ret);
        // exit;
        foreach($ret as $key => $val) {
            if(isset($this->table[$val['table_name']])){
                $where = array();
                if(!in_array($val['table_name'], $this->vanCheckExcl)){
                    $vanId = 'van_id';
                    if($val['table_name'] == 'van_daily_stock') { 
                        $vanId = 'vds_van_id';
                    }
                    if($val['table_name'] == 'van_stock') { 
                        $vanId = 'vanstock_van_id';
                    }
                    if($val['table_name'] == 'van_transfer') { 
                        $vanId = 'vantran_van_id';
                    }
                    if($val['table_name'] == 'van_transfer_sub') { 
                        $vanId = 'vantransub_van_id';
                    }
                    if($val['table_name'] == 'van_transfer_return') { 
                        $vanId = 'vantranret_van_id';
                    }
                    if($val['table_name'] == 'van_transfer_return_sub') { 
                        $vanId = 'vantranretsub_van_id';
                    }
                    $where[] = [$vanId,'=', $this->request->van_id];
                }
                if(isset($val['sync_time'])){
                    $where[] = ['server_sync_time','>', $val['sync_time']];
                }
                $ret[$key]['count'] = $this->table[$val['table_name']]::where($where)->count();
                unset($ret[$key]['sync_time']);
            }
        }
        $branch = Acc_branch::where('branch_id', $this->request->branch_id)->first();
        return response()->json(['data' => $ret, 'info'=>$branch, 'status' => 200]);
    }

    public function sync_start() {

        $this->where[] = ['sync_completed','=', 0];
        $this->where[] = ['sync_code','=', $this->request->sync_code];
        $query = SalesMaster::query();
        $query->where($this->where);
        $info1 = $query->get();
      
        foreach($info1 as $key => $val){
            $where = $this->where;
            $where[] = ['van_inv_no','=', $val['sales_van_inv']];
            SalesSub::where($where)->update([
                'salesub_sales_inv_no' => $val['sales_inv_no'],
                'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)
                ]);
            SalesMaster::where($this->where)->update(['sync_completed' => 1, 'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)]);
        } 

        $salesSub = SalesSub::where($this->where)->get();
        $voucherEntryInv = [];
        foreach($salesSub as $key => $val){

            if($val['salesub_flags'] == 1) {
                $where = array();
                $where[] = ['cmp_prd_id','=', $val['salesub_prod_id']];
                $cmpStock = CompanyStocks::where($where)->first();
                $fnlQty = $cmpStock->cmp_stock_quantity - $val['salesub_qty'];
                CompanyStocks::where($where)->update([
                    'cmp_stock_quantity' => $fnlQty,
                    'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)
                ]);

                $where = array();
                $where[] = ['sdu_branch_stock_id','=', $val['salesub_branch_stock_id']];
                $where[] = ['sdu_prd_id','=', $val['salesub_prod_id']];
                $where[] = ['sdu_date','=', $val['salesub_date']];
                // $where[] = ['sdu_date','=', Date('Y-m-d', $val['salesub_date'])];
                $where[] = ['branch_id','=', $this->request->branch_id];
                $stock = StockDailyUpdates::where($where)->first();
                if(!empty($stock)){
                    $fnlQty = $stock->sdu_stock_quantity - $val['salesub_qty'];
                    StockDailyUpdates::where($where)->update([
                        'sdu_stock_quantity' => $fnlQty,
                        'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)
                    ]);
                } else {    
                                
                    $info = array();
                    $info['sdu_branch_stock_id'] = $val['salesub_branch_stock_id'];
                    $info['sdu_prd_id'] = $val['salesub_prod_id'];
                    $info['sdu_stock_id'] = $val['salesub_stock_id'];
                    // $info['sdu_date'] = Date('Y-m-d', $val['salesub_date']);
                    $info['sdu_date'] = $val['salesub_date'];
                    $info['sdu_stock_quantity'] = $val['salesub_qty'] * -1;
                    $info['branch_id'] = $this->request->branch_id;
                    $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                    $stock = StockDailyUpdates::create($info);

                }

                // $where = array();
                // // $where[] = ['branch_stock_id','=', $val['salesub_branch_stock_id']];
                // $where[] = ['bs_prd_id','=', $val['salesub_prod_id']];
                // $where[] = ['bs_branch_id','=', 0];
                // // print_r($where);
                // $stock = BranchStocks::where($where)->first();
                // // print_r($stock);
                // $fnlQty = $stock->bs_stock_quantity - $val['salesub_qty'];
                // $qty = array(
                //     'bs_stock_quantity' => $fnlQty,
                //     'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6),
                //     'bs_stock_quantity_van' => $stock->bs_stock_quantity_van - $val['salesub_qty']
                // );

                // BranchStocks::where($where)->update($qty);


                $where = array();
                // $where[] = ['branch_stock_id','=', $val['salesub_branch_stock_id']];
                $where[] = ['bs_prd_id','=', $val['salesub_prod_id']];
                $where[] = ['bs_branch_id','=', $this->request->branch_id];
                $stock = BranchStocks::where($where)->first();
                if(!empty($stock)){
                    $fnlQty = $stock->bs_stock_quantity - $val['salesub_qty'];
                    $qty = array(
                        'bs_stock_quantity' => $fnlQty,
                        'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6),
                        'bs_stock_quantity_van' => $stock->bs_stock_quantity_van - $val['salesub_qty']
                    );
                    BranchStocks::where($where)->update($qty);
                } else {
                    $info = array();
                    $info['bs_stock_id'] = $val['salesub_stock_id'];
                    $info['bs_prd_id'] = $val['salesub_prod_id'];
                    $info['bs_stock_quantity'] = $val['salesub_qty'] * -1;
                    $info['bs_stock_quantity_shop'] = 0;
                    $info['bs_stock_quantity_gd'] = 0;
                    $info['bs_stock_quantity_van'] = $val['salesub_qty'] * -1;
                    $info['bs_prate'] = 0;
                    $info['bs_avg_prate'] = 0;
                    $info['bs_srate'] = 0;
                    $info['bs_batch_id'] = 0;
                    $info['bs_expiry'] = 0;
                    $info['bs_branch_id'] = $this->request->branch_id;
                    $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                    $stock = BranchStocks::create($info);
                }


                $where = array();
                $where[] = ['vanstock_prod_id','=', $val['salesub_prod_id']];
                $where[] = ['vanstock_branch_stock_id','=', $val['salesub_branch_stock_id']];
                $where[] = ['vanstock_van_id','=', $this->request->van_id];
                $stock = VanStock::where($where)->first();
                if(!empty($stock)){
                    $fnlQty = $stock->vanstock_qty - $val['salesub_qty'];
                    $qty = array(
                        'vanstock_qty' => $fnlQty,
                        'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6),
                    );
                    VanStock::where($where)->update($qty);
                } else {
                    $info = array();
                    $info['vanstock_prod_id'] = $val['salesub_prod_id'];
                    $info['vanstock_stock_id'] = $val['salesub_stock_id'];
                    $info['vanstock_branch_stock_id'] = $val['salesub_branch_stock_id'];
                    $info['vanstock_van_id'] = $this->request->van_id;
                    $info['vanstock_qty'] = $val['salesub_qty'] * -1;
                    $info['vanstock_last_tran_rate'] = 0;
                    $info['vanstock_avg_tran_rate'] = 0;
                    $info['branch_id'] = $this->request->branch_id;
                    $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                    $stock = VanStock::create($info);
                }


                $where = array();
                $where[] = ['vds_branch_stock_id','=', $val['salesub_branch_stock_id']];
                $where[] = ['vds_prd_id','=', $val['salesub_prod_id']];
                $where[] = ['vds_date','=', $val['salesub_date']];
                $where[] = ['vds_van_id','=', $this->request->van_id];
                $stock = VanDailyStocks::where($where)->first();
                if(!empty($stock)){
                    $fnlQty = $stock->vds_stock_quantity - $val['salesub_qty'];
                    VanDailyStocks::where($where)->update([
                        'vds_stock_quantity' => $fnlQty,
                        'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)
                    ]);
                } else {
                                
                    $info = array();
                    $info['vds_branch_stock_id'] = $val['salesub_branch_stock_id'];
                    $info['vds_prd_id'] = $val['salesub_prod_id'];
                    $info['vds_stock_id'] = $val['salesub_stock_id'];
                    $info['vds_van_id'] =  $this->request->van_id;
                    $info['vds_date'] = $val['salesub_date'];
                    $info['vds_stock_quantity'] = $val['salesub_qty'] * -1;
                    $info['branch_id'] = $this->request->branch_id;
                    $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                    $stock = VanDailyStocks::create($info);

                }


                if(!in_array($val['salesub_sales_inv_no'], $voucherEntryInv)){
                    $voucherEntryInv[] = $val['salesub_sales_inv_no'];
                    app('App\Http\Controllers\Api\Migration\ledgerOpeingbalanceController')->setsalessubtovoucher($val['salesub_sales_inv_no']);
                }
                
            }
            $subWhere = $this->where;
            $subWhere[] = ['salesub_id','=', $val['salesub_id']];
            SalesSub::where($subWhere)->update(['sync_completed' => 1, 'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)]);

        }

        $salesDueSub = SalesDueSub::where($this->where)->get();
        foreach($salesDueSub as $key => $val){
            // sales due account (voucher) code comes here
            if ($val['salesduesub_flags'] == 1) {
                //check sales due receipt
                if ($val['salesduesub_type'] == 2) {
                    // check sales_due_sub type 0 (main) entry exists with the sync, then do, nothing, else update salesduesub_inv_balance
                    $msterEntry = array_filter($salesDueSub->toArray(), function ($var) use ($val) {
                        return (
                            $var['salesduesub_type'] == (($val['salesduesub_inv_no'] == 0) ? 3 : 0) && 
                            $var['branch_inv_no'] == $val['branch_inv_no'] &&
                            $var['salesduesub_ledger_id'] == $val['salesduesub_ledger_id'] 
                        );
                    });
                    if(empty($msterEntry)){

                        $where[] = ['salesduesub_type','=', (($val['salesduesub_inv_no'] == 0) ? 3 : 0)];
                        $where[] = ['branch_inv_no','=', $val['branch_inv_no']];
                        $where[] = ['salesduesub_ledger_id','=', $val['salesduesub_ledger_id']];
                        $salesDueSubMain = SalesDueSub::where($where)->first();
                        $invBal = $salesDueSubMain['salesduesub_inv_balance'] - $val['salesduesub_in'];
                        SalesDueSub::where($where)->update(['salesduesub_inv_balance' => $invBal, 'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)]);
                    }
                }

                //check sales return
                if ($val['salesduesub_type'] == 1 && $val['salesduesub_inv_no'] != 0) {
                    // check sales_due_sub type 0 (main) entry exists with the sync, then do, nothing, else update salesduesub_inv_balance
                    $msterEntry = array_filter($salesDueSub->toArray(), function ($var) use ($val) {
                        return (
                            $var['salesduesub_type'] == 0 && 
                            $var['branch_inv_no'] == $val['branch_inv_no'] &&
                            $var['salesduesub_ledger_id'] == $val['salesduesub_ledger_id'] 
                        );
                    });
                    if(empty($msterEntry)){

                        $where[] = ['salesduesub_type','=', 0];
                        $where[] = ['branch_inv_no','=', $val['branch_inv_no']];
                        $where[] = ['salesduesub_ledger_id','=', $val['salesduesub_ledger_id']];
                        $salesDueSubMain = SalesDueSub::where($where)->first();
                        $invBal = $salesDueSubMain['salesduesub_inv_balance'] - $val['salesduesub_in'];
                        SalesDueSub::where($where)->update(['salesduesub_inv_balance' => $invBal, 'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)]);
                    }
                }
            }

            $dueSubWhere = $this->where;
            $dueSubWhere[] = ['salesduesub_id','=', $val['salesduesub_id']];
            SalesDueSub::where($dueSubWhere)->update(['sync_completed' => 1, 'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)]);

        }
        $where1[] = ['acc_receipt.sync_completed','=', 0];
        $where1[] = ['acc_receipt.sync_code','=', $this->request->sync_code];    
        $accRecp = AccReceipt::select('*', 'acc_receipt.rec_no as rec_no')
                ->where($where1)
                ->join('sales_due_sub','sales_due_sub.van_rec_no','acc_receipt.van_rec_no')
                ->groupBy('sales_due_sub.van_rec_no')
                ->get();
        foreach($accRecp as $key => $val){
            // echo $val['salesduesub_id'];
            $info = array();
            $max = AccReceiptSub::max('recsub_no');
            $info['recsub_no'] = (empty($max)) ? 1 : $max+1;
            $info['recsub_rec_no'] = $val['rec_no'];
            $info['recsub_date'] = $val['rec_date'];
            $info['recsub_time'] = $val['rec_time'];
            $info['recsub_datetime'] = $val['rec_datetime'];
            $info['recsub_timestamp'] = $val['rec_timestamp'];
            $info['recsub_acc_ledger_id'] = $val['rec_acc_ledger_id'];
            $info['recsub_ledger_id'] = $val['salesduesub_ledger_id'];
            $info['recsub_amount'] = $val['rec_ttl_amount'];
            // $info['recsub_narration'] =  $data['rec_date'];
            // $info['recsub_vat_per'] =  $data['rec_date'];
            // $info['recsub_vat_amt'] =  $data['rec_date'];
            $info['branch_id'] = $this->request->branch_id;
            $info['van_id'] = $this->request->van_id;
            $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            $info['van_rec_no'] =  $val['branch_rec_no'];
            $info['branch_rec_no'] =  $val['branch_rec_no'];
            $res1 = AccReceiptSub::create($info);

            $info = array();
            $max = Acc_voucher::max('vch_no');
            $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max+1;

            $ledgDetails = Acc_ledger::find($val['rec_acc_ledger_id']);
            
            $info['vch_ledger_from'] = $val['rec_acc_ledger_id'];
            $info['vch_ledger_to'] = $val['salesduesub_ledger_id'];;
            $info['vch_date'] = $val['rec_date'];
            $info['vch_in'] = 0;
            $info['vch_out'] = $val['rec_ttl_amount'];
            $info['vch_vchtype_id'] = 22;
            $info['vch_notes'] = 'Sales Due Receipt';
            $info['vch_added_by'] = $val['salesduesub_added_by'];
            $info['vch_id2'] = 0;
            $info['vch_from_group'] = $ledgDetails->ledger_accgrp_id;
            $info['vch_flags'] = 1;
            $info['ref_no'] = $val['rec_no'];
            $info['branch_id'] = $this->request->branch_id;
            $info['is_web'] = 0;
            $info['godown_id'] = 0;
            $info['van_id'] = $this->request->van_id;
            $info['branch_ref_no'] = $val['branch_rec_no'];
            $info['sub_ref_no'] = 0;
            $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            $res2 = Acc_voucher::create($info);

            $info = array();
            $info['vch_no'] = $vouchNo;
            $info['vch_ledger_from'] = $val['salesduesub_ledger_id'];
            $info['vch_ledger_to'] = $val['rec_acc_ledger_id'];
            $info['vch_date'] = $val['rec_date'];
            $info['vch_in'] = $val['rec_ttl_amount'];
            $info['vch_out'] = 0;
            $info['vch_vchtype_id'] = 22;
            $info['vch_notes'] = 'Sales Due Receipt';
            $info['vch_added_by'] = $val['salesduesub_added_by'];
            $info['vch_id2'] = $res2->vch_id;
            $info['vch_from_group'] = 21;
            $info['vch_flags'] = 1;
            $info['ref_no'] = $val['rec_no'];
            $info['branch_id'] = $this->request->branch_id;
            $info['is_web'] = 0;
            $info['godown_id'] = 0;
            $info['van_id'] = $this->request->van_id;
            $info['branch_ref_no'] = $val['branch_rec_no'];
            $info['sub_ref_no'] = 0;
            $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            $res2 = Acc_voucher::create($info);

            $dueSubWhere = $this->where;
            $dueSubWhere[] = ['rec_id','=', $val['rec_id']];
            AccReceipt::where($dueSubWhere)->update(['sync_completed' => 1, 'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)]);
            $where = array();
            $where[] = ['salesduesub_id','=', $val['salesduesub_id']];
            SalesDueSub::where($where)->update(['rec_no' => $val['rec_no'], 'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)]);

        } 

        $where = array();
        $where[] = ['sync_completed','=', 0];
        $where[] = ['sync_code','=', $this->request->sync_code];
        $query = SalesReturnMaster::query();
        $query->where($where);
        $salesRetMaster = $query->get()->toArray();
       
      
        foreach($salesRetMaster as $key => $val){
            $where1 = $where;
            $where1[] = ['salesretsub_van_ret_no','=', $val['salesret_van_ret_no']];
            
            $salesRet = SalesReturnSub::where($where1)->get()->toArray();
            foreach($salesRet as $subVal){
                if($subVal['salesretsub_flags'] == 1){

                    if($subVal['salesretsub_sales_inv_no'] != 0){
                        $wer = array();
                        $wer[] = ['salesub_id','=', $subVal['salesretsub_salesub_id']];
                        $salesSubtemp = SalesSub::where($wer)->first();
                        SalesSub::where($wer)->update([
                            'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6),
                            'salesub_rem_qty' => $salesSubtemp['salesub_rem_qty'] - $subVal['salesretsub_qty']
                        ]);
                    }
                    
                    $where = array();
                    $where[] = ['cmp_prd_id','=', $subVal['salesretsub_prod_id']];
                    $cmpStock = CompanyStocks::where($where)->first();
                    $fnlQty = $cmpStock->cmp_stock_quantity + $subVal['salesretsub_qty'];
                    CompanyStocks::where($where)->update([
                        'cmp_stock_quantity' => $fnlQty,
                        'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)
                    ]);

                    $where = array();
                    $where[] = ['sdu_branch_stock_id','=', $subVal['salesretsub_branch_stock_id']];
                    $where[] = ['sdu_prd_id','=', $subVal['salesretsub_prod_id']];
                    $where[] = ['sdu_date','=', $subVal['salesretsub_date']];
                    // $where[] = ['sdu_date','=', Date('Y-m-d', $val['salesub_date'])];
                    $where[] = ['branch_id','=', $this->request->branch_id];
                    $stock = StockDailyUpdates::where($where)->first();
                    if(!empty($stock)){
                        $fnlQty = $stock->sdu_stock_quantity + $subVal['salesretsub_qty'];
                        StockDailyUpdates::where($where)->update([
                            'sdu_stock_quantity' => $fnlQty,
                            'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)
                        ]);
                    } else {
                                    
                        $info = array();
                        $info['sdu_branch_stock_id'] = $subVal['salesretsub_branch_stock_id'];
                        $info['sdu_prd_id'] = $subVal['salesretsub_prod_id'];
                        $info['sdu_stock_id'] = $subVal['salesretsub_stock_id'];
                        // $info['sdu_date'] = Date('Y-m-d', $val['salesub_date']);
                        $info['sdu_date'] = $subVal['salesretsub_date'];
                        $info['sdu_stock_quantity'] = $subVal['salesretsub_qty'];
                        $info['branch_id'] = $this->request->branch_id;
                        $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                        $stock = StockDailyUpdates::create($info);

                    }

                    // $where = array();
                    // // $where[] = ['branch_stock_id','=', $val['salesub_branch_stock_id']];
                    // $where[] = ['bs_prd_id','=', $val['salesub_prod_id']];
                    // $where[] = ['bs_branch_id','=', 0];
                    // // print_r($where);
                    // $stock = BranchStocks::where($where)->first();
                    // // print_r($stock);
                    // $fnlQty = $stock->bs_stock_quantity - $val['salesub_qty'];
                    // $qty = array(
                    //     'bs_stock_quantity' => $fnlQty,
                    //     'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6),
                    //     'bs_stock_quantity_van' => $stock->bs_stock_quantity_van - $val['salesub_qty']
                    // );

                    // BranchStocks::where($where)->update($qty);


                    $where = array();
                    // $where[] = ['branch_stock_id','=', $val['salesub_branch_stock_id']];
                    $where[] = ['bs_prd_id','=', $subVal['salesretsub_prod_id']];
                    $where[] = ['bs_branch_id','=', $this->request->branch_id];
                    $stock = BranchStocks::where($where)->first();
                    if(!empty($stock)){
                        $fnlQty = $stock->bs_stock_quantity + $subVal['salesretsub_qty'];
                        $qty = array(
                            'bs_stock_quantity' => $fnlQty,
                            'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6),
                            'bs_stock_quantity_van' => $stock->bs_stock_quantity_van + $subVal['salesretsub_qty']
                        );
                        BranchStocks::where($where)->update($qty);
                    } else {
                        $info = array();
                        $info['bs_stock_id'] = $subVal['salesretsub_stock_id'];
                        $info['bs_prd_id'] = $subVal['salesretsub_prod_id'];
                        $info['bs_stock_quantity'] = $subVal['salesretsub_qty'];
                        $info['bs_stock_quantity_shop'] = 0;
                        $info['bs_stock_quantity_gd'] = 0;
                        $info['bs_stock_quantity_van'] = $subVal['salesretsub_qty'];
                        $info['bs_prate'] = 0;
                        $info['bs_avg_prate'] = 0;
                        $info['bs_srate'] = 0;
                        $info['bs_batch_id'] = 0;
                        $info['bs_expiry'] = 0;
                        $info['bs_branch_id'] = $this->request->branch_id;
                        $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                        $stock = BranchStocks::create($info);
                    }


                    $where = array();
                    $where[] = ['vanstock_prod_id','=', $subVal['salesretsub_prod_id']];
                    $where[] = ['vanstock_branch_stock_id','=', $subVal['salesretsub_branch_stock_id']];
                    $where[] = ['vanstock_van_id','=', $this->request->van_id];
                    $stock = VanStock::where($where)->first();
                    if(!empty($stock)){
                        $fnlQty = $stock->vanstock_qty + $subVal['salesretsub_qty'];
                        $qty = array(
                            'vanstock_qty' => $fnlQty,
                            'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6),
                        );
                        VanStock::where($where)->update($qty);
                    } else {
                        $info = array();
                        $info['vanstock_prod_id'] = $subVal['salesretsub_prod_id'];
                        $info['vanstock_stock_id'] = $subVal['salesretsub_stock_id'];
                        $info['vanstock_branch_stock_id'] = $subVal['salesretsub_branch_stock_id'];
                        $info['vanstock_van_id'] = $this->request->van_id;
                        $info['vanstock_qty'] = $subVal['salesretsub_qty'];
                        $info['vanstock_last_tran_rate'] = 0;
                        $info['vanstock_avg_tran_rate'] = 0;
                        $info['branch_id'] = $this->request->branch_id;
                        $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                        $stock = VanStock::create($info);
                    }


                    $where = array();
                    $where[] = ['vds_branch_stock_id','=', $subVal['salesretsub_stock_id']];
                    $where[] = ['vds_prd_id','=', $subVal['salesretsub_prod_id']];
                    $where[] = ['vds_date','=', $subVal['salesretsub_date']];
                    $where[] = ['vds_van_id','=', $this->request->van_id];
                    $stock = VanDailyStocks::where($where)->first();
                    if(!empty($stock)){
                        $fnlQty = $stock->vds_stock_quantity + $subVal['salesretsub_qty'];
                        VanDailyStocks::where($where)->update([
                            'vds_stock_quantity' => $fnlQty,
                            'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)
                        ]);
                    } else {
                                    
                        $info = array();
                        $info['vds_branch_stock_id'] = $subVal['salesretsub_branch_stock_id'];
                        $info['vds_prd_id'] = $subVal['salesretsub_prod_id'];
                        $info['vds_stock_id'] = $subVal['salesretsub_stock_id'];
                        $info['vds_van_id'] =  $this->request->van_id;
                        $info['vds_date'] = $subVal['salesretsub_date'];
                        $info['vds_stock_quantity'] = $subVal['salesretsub_qty'];
                        $info['branch_id'] = $this->request->branch_id;
                        $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                        $stock = VanDailyStocks::create($info);

                    }

                }

            }
            app('App\Http\Controllers\Api\Migration\ledgerOpeingbalanceController')->setsalesreturnsubtovoucher($val['salesret_id']);
            SalesReturnSub::where($where1)->update([
                'sync_completed' => 1, 
                'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)
            ]);
            $where = array();
            $where[] = ['sync_completed','=', 0];
            $where[] = ['sync_code','=', $this->request->sync_code];
            SalesReturnMaster::where($where)->update(['sync_completed' => 1, 'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)]);
        } 
     
        if($salesSub->isEmpty() && $info1->isEmpty() && $salesDueSub->isEmpty() && $accRecp->isEmpty() && empty($salesRetMaster)){
            return parent::displayData(array('message' => 'Nothing to sync', 'status' => $this->created_stat));
        } else {
            return parent::displayData(array('message' => 'sync completed Successfully', 'status' => $this->created_stat));
        }
    }

}
