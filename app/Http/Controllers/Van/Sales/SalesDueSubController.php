<?php
namespace App\Http\Controllers\Van\Sales;

use App\Http\Controllers\Van\VanParent;
use App\Models\Sales\SalesDueSub;
use App\Models\Sales\SalesMaster;
use App\Models\Accounts\Acc_customer;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class salesduesubcontroller extends VanParent {

    public $badrequest_stat = 400;
    public $where = array();
    public $request;

    public function operations(Request $request, $type){

        if($request->sync_time){
            $this->where[] = ['sales_due_sub.server_sync_time','>', $request->sync_time];
        }
        if($request->van_line_id) {
            $this->where[] = ['acc_customers.van_line_id','=', $request->van_line_id];
        }
        // if($request->van_id) {
        //     $this->where[] = ['van_id','=', $request->van_id];
        // }
        //  else if($request->branch_id) {
        //     $this->where[] = ['branch_id','=', $request->branch_id];
        // }
        
        $this->request = $request;

        switch ($type) {
            case 'get_count':
                return $this->getCount();
                break;
            case 'download':
                return $this->download();
                break;
            case 'upload':
                return $this->upload();
                break;
            default:
                return response()->json(['error' => 'Invalid Endpoint', 'status' => $this->badrequest_stat]);
                break;
        }
    }

    public function getCount() {
        $this->where[] = ['sales_due_sub.sync_completed','=', 1];
        $ttlCount = SalesDueSub::where($this->where)
        ->join('acc_customers', 'acc_customers.ledger_id', '=', 'sales_due_sub.salesduesub_ledger_id')
        ->count();
        return parent::displayData($ttlCount);
    }

    public function download() {
        $query = SalesDueSub::query();
        $query->select('sales_due_sub.*');
        $query->join('acc_customers', 'acc_customers.ledger_id', '=', 'sales_due_sub.salesduesub_ledger_id');
        $this->where[] = ['sales_due_sub.sync_completed','=', 1];
        $query->where($this->where);
        if($this->request->limit) {
            $perPage = ($this->request->part_no) ? $this->request->part_no : 0;
            $query->skip(($perPage * $this->request->limit));
            $query->take($this->request->limit);
        }
        $info = $query->get();
        if(!$info->isEmpty()) {
            return parent::displayData($info);
        } else{
            unset($this->where[0]);
            $syncTime = SalesDueSub::where($this->where)
            ->join('acc_customers', 'acc_customers.ledger_id', '=', 'sales_due_sub.salesduesub_ledger_id')
            ->max('sales_due_sub.server_sync_time');
            return parent::sendResponseSyncTime($syncTime);
        }
    }

    public function upload() {

        // $data = $this->request->json()->all();
        $data = $this->request->data;
        $ret = array();
        $barchInvNo = '';
        foreach($data as $key => $val) {

            if(is_numeric($key)){

                if( $barchInvNo != $val['van_inv_no']){
                    $barchInvNo = $val['van_inv_no'];
                    $where = array();
                    $where[] = ['sales_van_inv','=', $val['van_inv_no']];
                    $where[] = ['branch_id','=', $this->request->branch_id];
                    $query = SalesMaster::query();
                    $query->where($where);
                    $saleMaster = $query->first();
                } 
                
                $info = array();
                $info['salesduesub_inv_no'] = (!empty($saleMaster) && $val['salesduesub_inv_no'] == 0 )  ?  $saleMaster['sales_inv_no'] : $val['salesduesub_inv_no'];
                $info['salesduesub_ledger_id'] = $val['salesduesub_ledger_id'];
                $info['salesduesub_date'] = $val['salesduesub_date'];
                // salesduesub_type =>  0- sales, 1 sales return , 2 payment
                $info['salesduesub_type'] = $val['salesduesub_type'];
                $info['salesduesub_in'] = $val['salesduesub_in'];
                $info['salesduesub_out'] = $val['salesduesub_out'];
                $info['salesduesub_inv_amount'] = $val['salesduesub_inv_amount'];
                $info['salesduesub_inv_balance'] = $val['salesduesub_inv_balance'];
                $info['salesduesub_added_by'] = $val['salesduesub_added_by'];
                $info['salesduesub_flags'] = $val['salesduesub_flags'];
                $info['salesduesub_agent_ledger_id'] = $val['salesduesub_agent_ledger_id'];
                $info['is_web'] = 0;
                $info['godown_id'] = 0;
                $info['branch_inv_no'] = ($val['van_inv_no']) ? $val['van_inv_no'] : '';
                 
                if(isset($val['server_sync_flag'])){
                    $info['server_sync_flag'] = $val['server_sync_flag'];
                    $info['local_sync_time'] = $val['local_sync_time'];
                }
                $info['sync_completed'] = 0;
                $info['sync_code'] = $this->request->sync_code;
                $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                $info['rec_no'] = (isset($val['rec_no'])) ? $val['rec_no'] : 0;
                $info['van_rec_no'] = (isset($val['van_rec_no'])) ? $val['van_rec_no'] : 0;
                $info['branch_rec_no'] = (isset($val['branch_rec_no'])) ? $val['branch_rec_no'] : 0;

                $info['salesduesub_ret_no'] = (isset($val['salesduesub_ret_no'])) ? $val['salesduesub_ret_no'] : 0;
                $info['salesduesub_van_ret_no'] = (isset($val['salesduesub_van_ret_no'])) ? $val['salesduesub_van_ret_no'] : 0;
                
                $sales = SalesDueSub::updateOrCreate(
                    [
                        'salesduesub_id2' => $val['salesduesub_id2'], 
                        'van_inv_no' => ($val['van_inv_no']) ? $val['van_inv_no'] : '',
                        'branch_id' => $this->request->branch_id, 
                        'van_id' => $this->request->van_id,
                    ],
                    $info
                );

                $ret[] = array(
                    'salesduesub_id' => $sales->salesduesub_id,
                    'server_sync_time' => $sales->server_sync_time,
                    'salesduesub_inv_no' => $sales->salesduesub_inv_no,
                    'salesduesub_id2' => $sales->salesduesub_id2,
                );
            }
            
        }
        return parent::displayData($ret);
    }

}
