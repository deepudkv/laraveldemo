<?php

namespace App\Http\Controllers\Van\Sales;

use App\Http\Controllers\Van\VanParent;
use App\Models\Sales\SalesDue;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class salesduecontroller extends VanParent {

    public $badrequest_stat = 400;
    public $where = array();
    public $request;

    public function operations(Request $request, $type){

        if($request->sync_time){
            $this->where[] = ['server_sync_time','>', $request->sync_time];
        }
        if($request->van_id) {
            $this->where[] = ['van_id','=', $request->van_id];
        } 
        if($request->branch_id) {
            $this->where[] = ['branch_id','=', $request->branch_id];
        }
        
        $this->request = $request;

        switch ($type) {
            case 'get_count':
                return $this->getCount();
                break;
            case 'download':
                return $this->download();
                break;
            default:
                return response()->json(['error' => 'Invalid Endpoint', 'status' => $this->badrequest_stat]);
                break;
        }
    }

    public function getCount() {
        $ttlCount = SalesDue::where($this->where)->count();
        return parent::displayData($ttlCount);
    }

    public function download() {
        $query = SalesDue::query();
        $query->where($this->where);
        if($this->request->limit) {
            $perPage = ($this->request->part_no) ? $this->request->part_no : 0;
            $query->skip(($perPage * $this->request->limit));
            $query->take($this->request->limit);
        }
        $info = $query->get();
        if(!$info->isEmpty()) {
            return parent::displayData($info);
        } else{
            unset($this->where[0]);
            $syncTime = SalesDue::where($this->where)->max('server_sync_time');
            return parent::sendResponseSyncTime($syncTime);
        }
    }

}
