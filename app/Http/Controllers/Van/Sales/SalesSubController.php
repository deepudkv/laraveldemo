<?php

namespace App\Http\Controllers\Van\Sales;

use App\Http\Controllers\Van\VanParent;
use App\Models\Sales\SalesSub;
use App\Models\Sales\SalesMaster;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Models\Product\Units;
use App\Models\Settings\TaxCategory;
use Validator;

class salessubcontroller extends VanParent {

    public $badrequest_stat = 400;
    public $where = array();
    public $request;

    public function operations(Request $request, $type){

        if($request->sync_time){
            $this->where[] = ['server_sync_time','>', $request->sync_time];
        }
        if($request->van_id) {
            $this->where[] = ['van_id','=', $request->van_id];
        } 
        if($request->branch_id) {
            $this->where[] = ['branch_id','=', $request->branch_id];
        }
        $this->where[] = ['sync_completed','=', 1];
        // else if($request->branch_id) {
        //     $this->where[] = ['branch_id','=', $request->branch_id];
        // }
        
        $this->request = $request;

        switch ($type) {
            case 'get_count':
                return $this->getCount();
                break;
            case 'download':
                return $this->download();
                break;
            case 'upload':
                return $this->upload();
                break;
            default:
                return response()->json(['error' => 'Invalid Endpoint', 'status' => $this->badrequest_stat]);
                break;
        }
    }

    public function getCount() {
        $ttlCount = SalesSub::where($this->where)->count();
        return parent::displayData($ttlCount);
    }

    public function download() {
        $query = SalesSub::query();
        $query->where($this->where);
        if($this->request->limit) {
            $perPage = ($this->request->part_no) ? $this->request->part_no : 0;
            $query->skip(($perPage * $this->request->limit));
            $query->take($this->request->limit);
        }
        $info = $query->get()->toArray();
        if(!empty($info)) {
            if(!$this->request->header('version') || $this->request->header('version') < 3) {
                foreach($info as $key => $val){
                    $info[$key]['salesub_tax_rate'] = $val['salesub_tax_rate'] * $val['salesub_qty'];
                }
            }
            return parent::displayData($info);
        } else{
            unset($this->where[0]);
            $syncTime = SalesSub::where($this->where)->max('server_sync_time');
            return parent::sendResponseSyncTime($syncTime);
        }
        
    }

    public function upload() {

        // $data = $this->request->json()->all();
        $data = $this->request->data;

        $taxCats = TaxCategory::get()->toArray();
        $taxCatIds = [];
        foreach($taxCats as $taxCat) {
            if($taxCat['taxcat_id'] != 4){
                $taxCatIds[$taxCat['taxcat_tax_per']] = $taxCat;
            }
        }

        $ret = array();
        $barchInvNo = '';
        foreach($data as $key => $val) {
            if(is_numeric($key)){

                if( $barchInvNo != $val['sales_van_inv_no']){
                    $barchInvNo = $val['sales_van_inv_no'];
                    $where = array();
                    $where[] = ['sales_van_inv','=', $val['sales_van_inv_no']];
                    $where[] = ['branch_id','=', $this->request->branch_id];
                    $query = SalesMaster::query();
                    $query->where($where);
                    $saleMaster = $query->first();
                } 
                // echo '<pre>';
                // print_r($saleMaster['sales_inv_no']);
                // exit;
                $info = array();
                // $info['salesub_id'] = $val['salesub_id'];
                $info['salesub_sales_inv_no'] = (!empty($saleMaster) && $val['salesub_sales_inv_no'] == 0 )  ?  $saleMaster['sales_inv_no'] : $val['salesub_sales_inv_no'];
                $info['salesub_stock_id'] = $val['salesub_stock_id'];
                if(isset($val['salesub_branch_stock_id'])){
                    $info['salesub_branch_stock_id'] = $val['salesub_branch_stock_id'];
                }

                // $whr=['bs_branch_id'=>$this->request->branch_id,'bs_prd_id'=> $val['salesub_prod_id']];
                // $out = BranchStocks::where($whr)->first();
                // $info['salesub_branch_stock_id'] = $out['branch_stock_id'];

                $info['salesub_prod_id'] = $val['salesub_prod_id'];
                $info['salesub_qty'] = $val['salesub_qty'];
                $info['salesub_rem_qty'] = $val['salesub_qty'];
                // $info['salesub_rem_qty'] = (!isset($val['salesub_rem_qty'])) ? $val['salesub_qty'] : $val['salesub_rem_qty'];
                // $info['salesub_discount'] = $val['salesub_discount'];
                
                // Old api discount not per item , so no version in header (old api) update discount as (discount/qty) to get per item discount
                $info['salesub_discount'] = ($this->request->header('version')) ? $val['salesub_discount'] : ($val['salesub_discount']/$val['salesub_qty']);
                // $info['salesub_rate'] = $val['salesub_rate'];
                if($this->request->header('version')) {
                    $info['salesub_rate'] = $val['salesub_rate'];
                } else {
                    $where = array();
                    $where[] = ['unit_id','=', $val['salesub_unit_id']];
                    $units = Units::where($where)->first();
                    $units['unit_base_qty'] = ($units['unit_base_qty'] != 0) ? $units['unit_base_qty'] : 1;
                    $info['salesub_rate'] = ($val['salesub_rate']/$units['unit_base_qty']) - $info['salesub_discount'];
                }
                if($this->request->header('version') && $this->request->header('version') >= 3) {
                    $info['salesub_tax_rate'] = $val['salesub_tax_rate'];
                } else {
                    $info['salesub_tax_rate'] = ($val['salesub_tax_rate']/$val['salesub_qty']);
                }

                $info['salesub_timestamp'] = strtotime($val['salesub_date']);
                // if(isset($val['salesub_timestamp'])){
                //     $info['salesub_timestamp'] = $val['salesub_timestamp'];
                // }
                
                $info['salesub_date'] = $val['salesub_date'];
                $info['salesub_serial'] = "{$val['salesub_serial']}";
                $info['salesub_flags'] = $val['salesub_flags'];
                $info['salesub_profit'] = $val['salesub_profit'];
                $info['salesub_unit_id'] = $val['salesub_unit_id'];
                $info['salesub_prate'] = $val['salesub_prate'];
                $info['salesub_promo_id'] = (isset($val['salesub_promo_id'])) ? $val['salesub_promo_id'] : 0;
                $info['salesub_promo_per'] = $val['salesub_promo_per'];
                $info['salesub_promo_rate'] = $val['salesub_promo_rate'];
                $info['salesub_tax_per'] = $val['salesub_tax_per'];
                $info['salesub_taxcat_id'] = (isset($val['salesub_taxcat_id']) && $val['salesub_taxcat_id'] != 0) ? $val['salesub_taxcat_id'] : $taxCatIds[$val['salesub_tax_per']]['taxcat_id'];
                // $info['salesub_tax_rate'] = $val['salesub_tax_rate'];

                $info['salesub_notes'] = "{$val['salesub_notes']}";
                $info['salesub_added_by'] = $val['salesub_added_by'];
                $info['salesub_id2'] = $val['salesub_id2'];

                $info['branch_id'] = $this->request->branch_id;
                $info['is_web'] = 0;
                $info['godown_id'] = 0;
                $info['branch_inv_no'] = $val['sales_van_inv_no'];
                
                if(isset($val['server_sync_flag'])){
                    $info['server_sync_flag'] = $val['server_sync_flag'];
                    $info['local_sync_time'] = $val['local_sync_time'];
                }
                $info['van_id'] = $this->request->van_id;
                $info['sync_completed'] = 0;
                $info['sync_code'] = $this->request->sync_code;
                $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
          
                $sales = SalesSub::updateOrCreate(
                    [
                        'salesub_id2' => $val['salesub_id2'], 
                        'van_inv_no' => $val['sales_van_inv_no'], 
                        'branch_id' => $this->request->branch_id, 
                        'van_id' => $this->request->van_id,
                    ],
                    $info
                );

                $ret[] = array(
                    'salesub_id' => $sales->salesub_id,
                    'server_sync_time' => $sales->server_sync_time,
                    'salesub_sales_inv_no' => $sales->salesub_sales_inv_no,
                    'salesub_id2' => $sales->salesub_id2,
                );
            }
            
        }
        return parent::displayData($ret);
    }

}
