<?php

namespace App\Http\Controllers\Van\Sales;

use App\Http\Controllers\Van\VanParent;
use App\Models\Sales\SalesMaster;
use App\Models\Sales\SalesReturnMaster;
use App\Models\Van\Van;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class salesreturncontroller extends VanParent {

    public $badrequest_stat = 400;
    public $where = array();
    public $request;

    public function operations(Request $request, $type){

        if($request->sync_time){
            $this->where[] = ['server_sync_time','>', $request->sync_time];
        }
        if($request->van_id) {
            $this->where[] = ['van_id','=', $request->van_id];
        } 
        // else if($request->branch_id) {
        //     $this->where[] = ['branch_id','=', $request->branch_id];
        // }
        
        $this->request = $request;

        switch ($type) {
            case 'get_count':
                return $this->getCount();
                break;
            case 'download':
                return $this->download();
                break;
            case 'upload':
                return $this->upload();
                break;
            default:
                return response()->json(['error' => 'Invalid Endpoint', 'status' => $this->badrequest_stat]);
                break;
        }
    }

    public function getCount() {
        $ttlCount = SalesReturnMaster::where($this->where)->count();
        return parent::displayData($ttlCount);
    }

    public function download() {
        $query = SalesReturnMaster::query();
        $query->where($this->where);
        if($this->request->limit) {
            $perPage = ($this->request->part_no) ? $this->request->part_no : 0;
            $query->skip(($perPage * $this->request->limit));
            $query->take($this->request->limit);
        }
        $info = $query->get();
        if(!$info->isEmpty()) {
            return parent::displayData($info);
        } else{
            unset($this->where[0]);
            $syncTime = SalesReturnMaster::where($this->where)->max('server_sync_time');
            return parent::sendResponseSyncTime($syncTime);
        }
    }

    public function upload() {
        // $data = $this->request->json()->all();
        $data = $this->request->data;
        // echo '<pre>';
        // print_r($data);

        // exit;
        $ret = array();
        foreach($data as $key => $val) {
            if(is_numeric($key)){
                $info = array();

                $max = SalesReturnMaster::max('salesret_no');
                // if sales_return = 0 then it is new return from van, else update with existing number
                $info['salesret_no'] = (isset($val['salesret_no']) &&  $val['salesret_no'] != 0 ) ? $val['salesret_no'] : $max+1;
                //$info['salesret_sales_inv_no'] = $val['salesret_sales_inv_no'];
                $info['salesret_date'] = $val['salesret_date'];
                $info['salesret_time'] = $val['salesret_time'];
                // `salesdue_timestamp` int(11) NOT NULL,
                // $info['salesdue_ledger_id'] = $val['salesdue_ledger_id'];
                // $info['salesdue_amount'] = $val['salesdue_amount'];
                $info['salesret_cust_type'] = $val['salesret_cust_type'];
                $info['salesret_cust_id'] = $val['salesret_cust_id']; 
                $info['salesret_cust_name'] = $val['salesret_cust_name'];
                $info['salesret_cust_address'] = ($val['salesret_cust_address']) ? $val['salesret_cust_address'] : '';
                $info['salesret_cust_ph'] = ($val['salesret_cust_ph']) ? $val['salesret_cust_ph'] : '';

                $info['salesret_amount'] = $val['salesret_amount'];
                $info['salesret_discount'] = $val['salesret_discount'];
                // salesret_pay_type = 0 Cash, salesret_pay_type =1  Creadit
                $info['salesret_pay_type'] = $val['salesret_pay_type'];
                
                $info['salesret_added_by'] = $val['salesret_added_by'];
                $info['salesret_flags'] = $val['salesret_flags'];
                // $info['salesret_vch_no'] = 1;
                $info['salesret_notes'] = ($val['salesret_notes']) ? $val['salesret_notes'] : '';

                $info['salesret_sales_amount'] = $val['salesret_sales_amount'];
                $info['salesret_sales_discount'] = $val['salesret_sales_discount'];
                $info['salesret_service_amount'] = $val['salesret_service_amount'];
                $info['salesret_service_discount'] = $val['salesret_service_discount'];
                $info['salesret_servoid_id'] = ($val['salesret_servoid_id']) ? $val['salesret_servoid_id'] : 0;

                $info['salesret_misc_amount'] = $val['salesret_misc_amount'];
                $info['salesret_tax'] = $val['salesret_tax'];
                $info['salesret_service_tax'] = $val['salesret_service_tax'];
                $info['salesret_tax_vch_no'] = ($val['salesret_tax_vch_no']) ? $val['salesret_tax_vch_no'] : 0;

                $info['salesret_godown_id'] = ($val['salesret_godown_id'] ? $val['salesret_godown_id'] : 0);
                $info['salesret_agent_ledger_id'] = ($val['salesret_agent_ledger_id']) ? $val['salesret_agent_ledger_id'] : 0;
                $info['salesret_pay_type2'] = ($val['salesret_pay_type2']) ? $val['salesret_pay_type2'] : 0;

                if ($info['salesret_pay_type'] == 1) {
                    $info['salesret_acc_ledger_id'] = $val['salesret_cust_ledger_id'];
                } else {
                    $where = array();
                    $where[] = ['van_id','=', $this->request->van_id];
                    $van = Van::where($where)->first();
                    $info['salesret_acc_ledger_id'] = $van['van_cash_ledger_id'];
                }

                $info['salesret_datetime'] = $val['salesret_datetime'];
                $info['salesret_godown_ret'] = ($val['salesret_godown_ret']) ? $val['salesret_godown_ret'] : 0;;
                // `salesret_godown_ret` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
                $info['branch_id'] = $this->request->branch_id;
                $info['is_web'] = 0;
                // `godown_id` int(11) NOT NULL DEFAULT 0,
                $info['van_id'] = $this->request->van_id;
                // `server_sync_flag` tinyint(4) NOT NULL DEFAULT 0,
                $info["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);
       
                $info['sync_completed'] = 0;
                $info['sync_code'] = $this->request->sync_code;
                $info['salesret_van_ret_no'] = $val['salesret_van_ret_no'];
                $info['salesret_branch_ret_no'] = $val['salesret_van_ret_no'];
                $info['salesret_sales_van_inv_no'] = ($val['salesret_sales_van_inv_no']) ? $val['salesret_sales_van_inv_no'] : '';
                $info['salesret_sales_branch_inv_no'] = ($val['salesret_sales_van_inv_no']) ? $val['salesret_sales_van_inv_no'] : '';
                $info['salesret_cust_ledger_id'] = $val['salesret_cust_ledger_id'];

                $sales = SalesMaster::where('sales_van_inv', $val['salesret_sales_van_inv_no'])->first();
                $info['salesret_sales_inv_no'] = isset($sales['sales_inv_no']) ? $sales['sales_inv_no'] : 0;
              
                $salesReturn = SalesReturnMaster::updateOrCreate(
                    [
                        'salesret_van_ret_no' => $val['salesret_van_ret_no'], 
                        'branch_id' => $this->request->branch_id, 
                        'van_id' => $this->request->van_id,
                    ],
                    $info
                );

                $ret[] = array(
                    'salesret_id' => $salesReturn->salesret_id,
                    'server_sync_time' => $salesReturn->server_sync_time,
                    'salesret_no' => $salesReturn->salesret_no,
                    'salesret_van_ret_no' => $salesReturn->salesret_van_ret_no,
                    'sales_inv_no' =>  $sales['sales_inv_no'],
                );
            }
        }
        return parent::displayData($ret);
    }

}
