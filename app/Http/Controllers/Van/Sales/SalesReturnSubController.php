<?php

namespace App\Http\Controllers\Van\Sales;

use App\Http\Controllers\Van\VanParent;
use App\Models\Sales\SalesReturnSub;
use App\Models\Sales\SalesReturnMaster;
use App\Models\Stocks\BranchStocks;
use App\Models\Sales\SalesSub;
use App\Models\Settings\TaxCategory;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class salesreturnsubcontroller extends VanParent {

    public $badrequest_stat = 400;
    public $where = array();
    public $request;

    public function operations(Request $request, $type){

        if($request->sync_time){
            $this->where[] = ['server_sync_time','>', $request->sync_time];
        }
        if($request->van_id) {
            $this->where[] = ['van_id','=', $request->van_id];
        } 
        // else if($request->branch_id) {
        //     $this->where[] = ['branch_id','=', $request->branch_id];
        // }
        
        $this->request = $request;

        switch ($type) {
            case 'get_count':
                return $this->getCount();
                break;
            case 'download':
                return $this->download();
                break;
            case 'upload':
                return $this->upload();
                break;
            default:
                return response()->json(['error' => 'Invalid Endpoint', 'status' => $this->badrequest_stat]);
                break;
        }
    }

    public function getCount() {
        $ttlCount = SalesReturnSub::where($this->where)->count();
        return parent::displayData($ttlCount);
    }

    public function download() {
        $query = SalesReturnSub::query();
        $query->where($this->where);
        if($this->request->limit) {
            $perPage = ($this->request->part_no) ? $this->request->part_no : 0;
            $query->skip(($perPage * $this->request->limit));
            $query->take($this->request->limit);
        }
        $info = $query->get();
        if(!$info->isEmpty()) {
            return parent::displayData($info);
        } else{
            unset($this->where[0]);
            $syncTime = SalesReturnSub::where($this->where)->max('server_sync_time');
            return parent::sendResponseSyncTime($syncTime);
        }
    }

    public function upload() {

        $data = $this->request->data;

        $taxCats = TaxCategory::get()->toArray();
        $taxCatIds = [];
        foreach($taxCats as $taxCat) {
            if($taxCat['taxcat_id'] != 4){
                $taxCatIds[$taxCat['taxcat_tax_per']] = $taxCat;
            }
        }

        $ret = array();
        $brachRetNo  = '';
        foreach($data as $key => $val) {

            if(is_numeric($key)){

                if( $brachRetNo != $val['salesretsub_van_ret_no']) {
                    $brachRetNo = $val['salesretsub_van_ret_no'];
                    $where = array();
                    $where[] = ['salesret_van_ret_no','=', $val['salesretsub_van_ret_no']];
                    $where[] = ['branch_id','=', $this->request->branch_id];
                    $query = SalesReturnMaster::query();
                    $query->where($where);
                    $saleReturn = $query->first();
                } 

                $info = array();
                $info['salesretsub_salesret_no'] = (!empty($saleReturn))  ?  $saleReturn['salesret_no'] : $val['salesretsub_salesret_no'];
                $info['salesretsub_sales_inv_no'] = (!empty($saleReturn) && $val['salesretsub_sales_inv_no'] == 0 )  ?  $saleReturn['salesret_sales_inv_no'] : $val['salesretsub_sales_inv_no'];
                // $info['salesretsub_sales_inv_no'] = $val['salesretsub_sales_inv_no'];
                
                

                $info['salesretsub_stock_id'] = $val['salesretsub_stock_id'];
                $where = array();
                $where[] = ['bs_prd_id','=', $val['salesretsub_prod_id']];
                $where[] = ['bs_branch_id','=', $this->request->branch_id];
                $brachStock = BranchStocks::where($where)->first();

                $info['salesretsub_branch_stock_id'] = (isset($brachStock['branch_stock_id'])) ? $brachStock['branch_stock_id'] : 0;

                $info['salesretsub_prod_id'] = $val['salesretsub_prod_id'];
                $info['salesretsub_rate'] = $val['salesretsub_rate'];
                $info['salesretsub_qty'] = $val['salesretsub_qty'];

                // $where = array();
                // $where[] = ['salesub_prod_id','=', $val['salesretsub_prod_id']];
                // $where[] = ['salesub_unit_id','=', $val['salesretsub_unit_id']];
                // // $where[] = ['van_inv_no','=', $val['salesretsub_sales_van_inv_no']];
                // $salesSub = SalesSub::where($where)->first();
                // $info['salesretsub_salesub_id'] = (isset($salesSub['salesub_id'])) ? $salesSub['salesub_id'] : 0;

                $info['salesretsub_salesub_id'] = ($val['salesretsub_salesub_id']) ? $val['salesretsub_salesub_id'] : 0;
                
                $info['salesretsub_date'] = $val['salesretsub_date'];

                $info['salesretsub_flags'] = $val['salesretsub_flags'];
                $info['salesretsub_unit_id'] = $val['salesretsub_unit_id'];
                $info['salesretsub_tax_per'] = $val['salesretsub_tax_per'];
                $info['salesretsub_tax_rate'] = $val['salesretsub_tax_rate'];

                $info['salesretsub_godown_id'] = (isset($val['salesretsub_godown_id'])) ? $val['salesretsub_godown_id'] : 0;
                $info['salesretsub_server_sync'] = (isset($val['salesretsub_server_sync']))? $val['salesretsub_server_sync'] : 0;
                $info['salesretsub_sync_timestamp'] = (isset($val['salesretsub_sync_timestamp'])) ? $val['salesretsub_sync_timestamp'] : 0;
                $info['salesretsub_server_timestamp']= (isset($val['salesretsub_server_timestamp'])) ? $val['salesretsub_server_timestamp'] : 0;
                $info['branch_id'] = $this->request->branch_id;
                $info['is_web'] = 0;
                $info['godown_id'] = 0;
                $info['van_id'] = $this->request->van_id;
                $info['server_sync_flag'] = (isset($val['server_sync_flag'])) ? $val['server_sync_flag'] : 0;
                $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                $info['sync_completed'] = 0;
                $info['sync_code'] = $this->request->sync_code;
                $info['salesretsub_van_ret_no'] = $val['salesretsub_van_ret_no'];
                $info['salesretsub_taxcat_id'] = (isset($val['salesretsub_taxcat_id']) && $val['salesretsub_taxcat_id'] != 0) ? $val['salesretsub_taxcat_id'] : $taxCatIds[$val['salesretsub_tax_per']]['taxcat_id'];
                $val['salesretsub_sales_van_inv_no'] = (isset($val['salesretsub_sales_van_inv_no'])) ? $val['salesretsub_sales_van_inv_no'] : 0;;
                $saleRetsSub = SalesReturnSub::updateOrCreate(
                    [
                        'salesretsub_id2' => $val['salesretsub_id2'], 
                        'salesretsub_van_ret_no' => $val['salesretsub_van_ret_no'], 
                        'branch_id' => $this->request->branch_id, 
                        'van_id' => $this->request->van_id,
                    ],
                    $info
                );

                $ret[] = array(
                    'salesretsub_id' => $saleRetsSub->salesretsub_id, 
                    'salesretsub_id2' => $val['salesretsub_id2'], 
                    'salesretsub_van_ret_no' => $val['salesretsub_van_ret_no'], 
                    'salesretsub_sales_inv_no' => $val['salesretsub_sales_inv_no'], 
                    'salesretsub_salesret_no' => $saleRetsSub->salesretsub_salesret_no, 
                    'branch_id' => $this->request->branch_id, 
                    'van_id' => $this->request->van_id,
                );
            }
        }
        return parent::displayData($ret);
    }
    // echo '<pre>';
        // print_r($data);

        // exit;

}
