<?php

namespace App\Http\Controllers\Van\User;

use App\Http\Controllers\Van\VanParent;
use App\User;
use App\Models\Van\VanUsers;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;
use DB;

class usercontroller extends VanParent {

    public $badrequest_stat = 400;
    public $where = array();
    public $request;


    public function operations(Request $request, $type){

        if($request->sync_time){
            $this->where[] = ['users.server_sync_time','>', $request->sync_time];
        }
        if($request->van_id) {
            $this->where[] = ['van_users.van_id','=', $request->van_id];
        } else if($request->branch_id) {
            $this->where[] = ['van_users.branch_id','=', $request->branch_id];
        }
        
        $this->request = $request;

        switch ($type) {
            case 'get_count':
                return $this->getCount();
                break;
            case 'download':
                return $this->download();
                break;
            default:
                return response()->json(['error' => 'Invalid Endpoint', 'status' => $this->badrequest_stat]);
                break;
        }
    }

    public function getCount() {
        $ttlCount = VanUsers::where($this->where)->count();
        return parent::displayData($ttlCount);
    }
    
    public function download() {

        $query = VanUsers::query();
        $query->join('users', 'users.usr_id', 'van_users.user_id');
        $query->where($this->where);
        if($this->request->limit) {
            $perPage = ($this->request->part_no) ? $this->request->part_no : 0;
            $query->skip(($perPage * $this->request->limit));
            $query->take($this->request->limit);
        }

        $info = $query->get([
            'users.usr_name', 
            'users.usr_username',
            'users.pass_md5',
            'users.usr_id',
            'users.server_sync_flag',
            'users.server_sync_time',
            'users.local_sync_time',
            'users.usr_type',
            'users.usr_phone',
            'users.usr_email',
            'users.usr_address',
            'users.usr_active',
            'users.usr_cash_ledger_id',
            'users.usr_default_godown_id'
            ]);
        if(!$info->isEmpty()) {
            return parent::displayData($info);
        } else{
            unset($this->where[0]);
            $syncTime = VanUsers::where($this->where)
                ->join('users', 'users.usr_id', 'van_users.user_id')
                ->max('users.server_sync_time');
            return parent::sendResponseSyncTime($syncTime);
        }
    }

}
