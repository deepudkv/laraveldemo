<?php

namespace App\Http\Controllers\Api\SalesReturn;

use App\Http\Controllers\Api\ApiParent;
use App\Models\Accounts\Acc_customer;
use App\Models\Company\Acc_branch;
use App\Models\Godown\GodownStock;
use App\Models\Sales\SalesDueSub;
use App\Models\Sales\SalesReturnMaster;
use App\Models\Sales\SalesReturnSub;
use App\Models\Sales\SalesSub;
use App\Models\Stocks\BranchStocks;
use App\Models\Stocks\CompanyStocks;
use App\Models\Stocks\StockDailyUpdates;
use DB;
use Illuminate\Http\Request;

class salesreturncontroller extends ApiParent
{

    public function nextReturnId(Request $request)
    {
        return parent::displayData(['ret_id' => SalesReturnMaster::max('salesret_no') + 1]);
    }

    public function SalesReturn(Request $request)
    {

        $info = array();
        $where = array();
        $where[] = ['branch_id', '=', $this->branch_id];
        $branch = Acc_branch::where($where)->first();
        $where[] = ['is_web', '=', 1];

        $max = SalesReturnMaster::where($where)->max('salesret_branch_ret_no');

        if (empty($max)) {
            $info['branch_rec_no'] = 'W#RT' . $branch['branch_code'] . '0001';
        } else {
            $no = ltrim($max, 'W#RT' . $branch['branch_code']);
            $newNo = +$no + 1;
            $info['branch_rec_no'] = 'W#RT' . $branch['branch_code'] . str_pad($newNo, 4, "0", STR_PAD_LEFT);
        }
        $sales_return['salesret_branch_ret_no'] = $info['branch_rec_no'];

        $sales_return['salesret_no'] = SalesReturnMaster::max('salesret_no') + 1;
        $sales_return['salesret_sales_inv_no'] = ($request['transf_id']) ? $request['transf_id'] : 0;
        $sales_return['salesret_date'] = date('Y-m-d', strtotime($request['ret_date']));
        $sales_return['salesret_flags'] = 1;
        $sales_return['salesret_time'] = date("H:i:s");
        $sales_return["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);
        $sales_return['salesret_cust_id'] = ($request['customer_type'] == 1 ? 0 : $this->getCustomerId($request['ledger']['ledger_id']));
        $sales_return['salesret_cust_name'] = ($request['customer_type'] == 1 ? $request['ledger'] : $request['ledger']['ledger_name']);
        $sales_return['salesret_amount'] = $request['net_amount'];
        $sales_return['salesret_cust_type'] = $request['customer_type'];
        $sales_return['salesret_pay_type'] = $request['salesret_pay_type'];
        $sales_return['salesret_added_by'] = $this->usr_id;

        $sales_return['salesret_notes'] = (isset($request['purch_note']) ? $request['purch_note'] : "");
        $sales_return['salesret_sales_amount'] = $request['total_amount'];
        $sales_return['salesret_cust_tin'] = ($request['customer_type'] == 1 ? 0 : ($request['ledger']['ledger_tin'] ? $request['ledger']['ledger_tin'] : 0));
        $sales_return['salesret_tax'] = $request['total_vat'];
        $sales_return['salesret_godown_id'] = ($request['ret_godown_id'] ? $request['ret_godown_id'] : 0);

        $sales_return['salesret_acc_ledger_id'] = ($request['cash_in_account']) ? $request['cash_in_account'] : 0;
        $sales_return['salesret_cust_ledger_id'] = ($request['customer_type'] == 1 ? 0 : $request['ledger']['ledger_id']);

        $sales_return['branch_id'] = $this->branch_id;
        $sales_return['is_web'] = 1;
        $sales_return['sync_completed'] = 1;

        if ($request['salesret_pay_type'] == 2) { // if Credit

            $sales_due_sub['salesduesub_salesdue_id'] = 0;
            $sales_due_sub['salesduesub_inv_no'] = 0;
            $sales_due_sub['salesduesub_ledger_id'] = $request['ledger']['ledger_id'];
            $sales_due_sub['salesduesub_date'] = date('Y-m-d', strtotime($request['ret_date']));
            $sales_due_sub['salesduesub_type'] = 1;
            $sales_due_sub['salesduesub_in'] = $sales_return['salesret_sales_amount'] + $sales_return['salesret_tax'];
            $sales_due_sub['salesduesub_out'] = 0;
            $sales_due_sub['salesduesub_inv_amount'] = 0;
            $sales_due_sub['salesduesub_inv_balance'] = 0;
            $sales_due_sub['salesduesub_added_by'] = $this->usr_id;
            $sales_due_sub['salesduesub_flags'] = 1;
            $sales_due_sub['salesduesub_agent_ledger_id'] = 0;
            $sales_due_sub['branch_id'] = $this->branch_id;
            $sales_due_sub['is_web'] = 1;
            $sales_due_sub["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);
            // $sales_due_sub['salesduesub_ret_no'] = SalesDueSub::max('salesduesub_ret_no') + 1;
            $sales_due_sub['salesduesub_ret_no'] = $sales_return['salesret_no'];

            SalesDueSub::create($sales_due_sub);
        }

        $last_insert = SalesReturnMaster::create($sales_return);
        foreach ($request['items'] as $key => $item) {

            $return_sub['salesretsub_salesret_no'] = $sales_return['salesret_no'];

            $return_sub['salesretsub_stock_id'] = $item['vantransub_stock_id'];
            $return_sub['salesretsub_prod_id'] = $item['vantransub_prod_id'];
            $return_sub['salesretsub_branch_stock_id'] = $item['vantransub_branch_stock_id'];

            $return_sub['salesretsub_unit_id'] = $item['vantransub_unit_id'];
            $return_sub['salesretsub_rate'] = $item['rate'];
            $return_sub['salesretsub_qty'] = $item['return_qty'];

            $return_sub['salesretsub_tax_per'] = $item['vat'];
            $return_sub['salesretsub_tax_rate'] = $item['rate'] * ($item['vat'] / 100);
            $return_sub['salesretsub_godown_id'] = $item['gd_id'];
            $return_sub['salesretsub_taxcat_id'] = $item['taxcat_id'];
            $return_sub['branch_id'] = $this->branch_id;

            // $return_sub['salesretsub_salesret_id'] = $last_insert->purch_id;
            $return_sub['salesretsub_sales_inv_no'] = $last_insert['salesret_sales_inv_no'];
            $return_sub['salesretsub_date'] = date('Y-m-d', strtotime($request['ret_date']));
            $return_sub['salesretsub_flags'] = 1;
            $return_sub["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);
            $return_sub['is_web'] = 1;
            $return_sub['sync_completed'] = 1;

            SalesReturnSub::create($return_sub);
            $this->updateStocksQuantity($return_sub, $request);
        }
        app('App\Http\Controllers\Api\Migration\ledgerOpeingbalanceController')->setsalesreturnsubtovoucher($last_insert->salesret_id);
        return parent::displayMessage('Sales Return  Successfull');
    }

    public function getCustomerId($ledId)
    {
        $res = Acc_customer::where('ledger_id', $ledId)->first();
        return $res->cust_id;
    }

    public function updateStocksQuantity($stock, $request)
    {

        $zerobs = BranchStocks::where('bs_prd_id', $stock['salesretsub_prod_id'])->where('bs_branch_id', 0)->first();
        $zerobs['bs_stock_quantity'] = $zerobs['bs_stock_quantity'] + $stock['salesretsub_qty'];
        $zerobs['bs_stock_quantity_shop'] = $zerobs['bs_stock_quantity_shop'] + $stock['salesretsub_qty'];
        // $zerobs['bs_stock_quantity_gd'] = $zerobs['bs_stock_quantity_gd'] + $stock['salesretsub_qty'];
        $zerobs->save();

        $bs = BranchStocks::where('bs_prd_id', $stock['salesretsub_prod_id'])
            ->where('bs_branch_id', $stock['branch_id'])->first();

        if ($stock['salesretsub_godown_id'] > 0) {
            $bs['bs_stock_quantity_gd'] = $bs['bs_stock_quantity_gd'] + $stock['salesretsub_qty'];

            $gdres = GodownStock::select('*')->where('gs_godown_id', $stock['salesretsub_godown_id'])
                ->where('gs_branch_stock_id', $stock['salesretsub_branch_stock_id'])
                ->first();

            if ($gdres) {
                $gdres['gs_qty'] = $gdres['gs_qty'] + $stock['salesretsub_qty'];
                $gdres['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                $gdres->save();
            } else {
                $stockunit = GodownStock::create([

                    'gs_godown_id' => $stock['salesretsub_godown_id'],
                    'gs_branch_stock_id' => $stock['salesretsub_branch_stock_id'],
                    'gs_stock_id' => $stock['salesretsub_stock_id'],
                    'gs_date' => date('Y-m-d', strtotime($request['ret_date'])),
                    'gs_prd_id' => $stock['salesretsub_prod_id'],
                    'gs_qty' => $stock['salesretsub_qty'],
                    'branch_id' => $request->branch_id,
                    'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6),

                ]);
            }
        } else {

            $bs['bs_stock_quantity_shop'] = $bs['bs_stock_quantity_shop'] + $stock['salesretsub_qty'];
        }
        $bs['bs_stock_quantity'] = $bs['bs_stock_quantity'] + $stock['salesretsub_qty'];
        $bs['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
        $bs->save();

        $cp = CompanyStocks::where('cmp_prd_id', $stock['salesretsub_prod_id'])->first();
        $cp['cmp_stock_quantity'] = $cp['cmp_stock_quantity'] + $stock['salesretsub_qty'];
        $cp->save();

        $this->addToDailyStock($stock, $request);
    }

    public function addToDailyStock($stock, $request)
    {
        $dateExist = StockDailyUpdates::select('*')

            ->where('sdu_prd_id', $stock['salesretsub_prod_id'])
            ->where('branch_id', $stock['branch_id'])

            ->where('sdu_date', date('Y-m-d', strtotime($request['ret_date'])))->first();

        if ($dateExist) {
            $bs = StockDailyUpdates::where('sdu_prd_id', $stock['salesretsub_prod_id'])
                ->where('branch_id', $stock['branch_id'])
                ->increment('sdu_stock_quantity', $stock['salesretsub_qty']);

            // ->where('sdu_branch_stock_id', $stock['salesretsub_branch_stock_id'])
            // $bs = StockDailyUpdates::where('sdu_branch_stock_id', $stock['salesretsub_branch_stock_id'])
            // ->where('sdu_date', date('Y-m-d', strtotime($request['ret_date'])))
            // ->increment('sdu_stock_quantity', $stock['salesretsub_qty']);

            StockDailyUpdates::where('sdu_branch_stock_id', $bs['salesretsub_branch_stock_id'])
                ->where('sdu_date', date('Y-m-d', strtotime($request['ret_date'])))
                ->update(['server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)]);
        } else {
            $ds['sdu_branch_stock_id'] = $stock['salesretsub_branch_stock_id'];
            $ds['sdu_stock_id'] = $stock['salesretsub_stock_id'];
            $ds['sdu_prd_id'] = $stock['salesretsub_prod_id'];
            $ds['sdu_date'] = date('Y-m-d', strtotime($request['ret_date']));
            $ds['sdu_stock_quantity'] = $stock['salesretsub_qty'];
            $ds['sdu_gd_id'] = $stock['salesretsub_godown_id'];
            $ds['sdu_batch_id'] = 0;
            $ds['branch_id'] = $this->branch_id;
            $ds['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            StockDailyUpdates::create($ds);
        }
    }

    public function listSalesReturn(Request $request)
    {

        $data = SalesReturnMaster::select('*')->where('salesret_flags', 1);

        if ($this->branch_id > 0) {
            $data = $data->where('branch_id', $this->branch_id);
        }

        $data = $data->orderBy('salesret_id', 'desc')->paginate($this->per_page)->toArray();

        return parent::displayData($data);
    }

    public function listSalesReturnVoid(Request $request)
    {

        $data = SalesReturnMaster::select('*')->where('salesret_flags', 0);

        if ($this->branch_id > 0) {
            $data = $data->where('branch_id', $this->branch_id);
        }

        $data = $data->orderBy('salesret_id', 'desc')->paginate($this->per_page)->toArray();

        return parent::displayData($data);
    }

    public function getSalesRetWithoutInv(Request $request)
    {

        $rltn = [
            'items' => function ($qr) {

                $qr->select(
                    'salesretsub_salesret_no',
                    'salesretsub_rate',
                    'salesretsub_qty as prod_qty',
                    'salesretsub_tax_per',
                    'salesretsub_tax_rate',
                    'salesretsub_prod_id',
                    'products.prd_name as prod_name',
                    DB::raw('(salesretsub_qty * salesretsub_rate) as salesret_price'),
                    'units.unit_display as prod_unit_name'
                )
                    ->join('products', 'prd_id', 'sales_return_sub.salesretsub_prod_id')
                    ->join('units', 'unit_id', 'sales_return_sub.salesretsub_unit_id')
                    //->where('salesretsub_flags',1)
                    ->orderBy('salesretsub_id', 'asc');
            },
        ];

        $data = SalesReturnMaster::select('*')->with($rltn);

        if ($this->branch_id > 0) {
            $data = $data->where('branch_id', $this->branch_id);
        }

        $data = $data->where('salesret_no', $request['ref_no']);

        $data = $data->orderby('salesret_no', 'desc')->first()->toArray();

        $data['total_amt'] = array_sum(array_column($data['items'], 'salesret_price'));
        $data['total_vat'] = array_sum(array_column($data['items'], 'salesretsub_tax_rate'));

        return $data;
    }

    public function SalesReturnInv(Request $request)
    {

        $info = array();
        $where = array();
        $where[] = ['branch_id', '=', $this->branch_id];
        $branch = Acc_branch::where($where)->first();
        $where[] = ['is_web', '=', 1];
        $return_sale_types = ['0' => '1', '1' => '2'];
        $max = SalesReturnMaster::where($where)->max('salesret_branch_ret_no');

        if (empty($max)) {
            $info['branch_rec_no'] = 'W#RT' . $branch['branch_code'] . '0001';
        } else {
            $no = ltrim($max, 'W#RT' . $branch['branch_code']);
            $newNo = +$no + 1;
            $info['branch_rec_no'] = 'W#RT' . $branch['branch_code'] . str_pad($newNo, 4, "0", STR_PAD_LEFT);
        }
        $sales_return['salesret_branch_ret_no'] = $info['branch_rec_no'];

        $sales_return['salesret_no'] = SalesReturnMaster::max('salesret_no') + 1;
        $sales_return['salesret_sales_inv_no'] = ($request['sales_inv_no']) ? $request['sales_inv_no'] : 0;
        $sales_return['salesret_date'] = date('Y-m-d', strtotime($request['ret_date']));
        $sales_return['salesret_flags'] = 1;
        $sales_return['salesret_time'] = date("H:i:s");
        $sales_return["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);
        $sales_return['salesret_cust_id'] = $request['customer_id'];
        $sales_return['salesret_cust_name'] = $request['customer_name'];
        $sales_return['salesret_amount'] = $request['net_amount'];
        $sales_return['salesret_cust_type'] = $request['customer_type'];
        $sales_return['salesret_pay_type'] = $return_sale_types[$request['salesret_pay_type']];
        $sales_return['salesret_added_by'] = $this->usr_id;

        $sales_return['salesret_notes'] = (isset($request['purch_note']) ? $request['purch_note'] : "");
        $sales_return['salesret_sales_amount'] = $request['total_amount'] + $request['total_vat'];
        $sales_return['salesret_cust_tin'] = $request['sales_cust_tin'] ? $request['sales_cust_tin'] : 0;
        $sales_return['salesret_tax'] = $request['total_vat'];
        $sales_return['salesret_godown_id'] = 0;

        $sales_return['salesret_acc_ledger_id'] = ($request['cash_in_account_ledger']) ? $request['cash_in_account_ledger'] : 0;

        $ledg =  Acc_customer::where('cust_id', $request['customer_id'])->first();
        $sales_return['salesret_cust_ledger_id'] = $ledg['ledger_id'] ? $ledg['ledger_id'] : 0;

        $sales_return['branch_id'] = $this->branch_id;
        $sales_return['is_web'] = 1;
        $sales_return['sync_completed'] = 1;

        if ($request['salesret_pay_type'] == 1) { // if Credit


            $ledg_data = Acc_customer::where('cust_id',$request['cash_in_account_ledger'])->first();

            $sales_due_sub['salesduesub_salesdue_id'] = 0;
            $sales_due_sub['salesduesub_inv_no'] = $request['sales_inv_no'];
            $sales_due_sub['salesduesub_ledger_id'] = $ledg_data['ledger_id'];
            $sales_due_sub['salesduesub_date'] = date('Y-m-d', strtotime($request['ret_date']));
            $sales_due_sub['salesduesub_type'] = 1;
            $sales_due_sub['salesduesub_in'] = $request['net_amount'];
            $sales_due_sub['salesduesub_out'] = 0;
            $sales_due_sub['salesduesub_inv_amount'] = 0;
            $sales_due_sub['salesduesub_inv_balance'] = 0;
            $sales_due_sub['salesduesub_added_by'] = $this->usr_id;
            $sales_due_sub['salesduesub_flags'] = 1;
            $sales_due_sub['salesduesub_agent_ledger_id'] = 0;
            $sales_due_sub['branch_id'] = $this->branch_id;
            $sales_due_sub['is_web'] = 1;
            $sales_due_sub["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);
            // $sales_due_sub['salesduesub_ret_no'] = SalesDueSub::max('salesduesub_ret_no') + 1;
            $sales_due_sub['salesduesub_ret_no'] = $sales_return['salesret_no'];

            SalesDueSub::create($sales_due_sub);

           $dueSub= SalesDueSub::where(['salesduesub_inv_no'=>$request['sales_inv_no'],'salesduesub_type'=>0])->first();
       
       if($dueSub['salesduesub_inv_balance'])
       {
        $dueSub['salesduesub_inv_balance'] -= $request['net_amount'];
        $dueSub->save();
       }
         

        }

        $last_insert = SalesReturnMaster::create($sales_return);
        foreach ($request['items'] as $key => $item) {

            $salesub_id = $item['salesub_id'];
            $sale_sub = SalesSub::where('salesub_id', $salesub_id)->first();

            $sale_sub['salesub_rem_qty'] = $sale_sub['salesub_rem_qty'] - $item['qty_rtn'];
            $sale_sub["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);

            $sale_sub->save();

            $return_sub['salesretsub_salesret_no'] = $sales_return['salesret_no'];

            $return_sub['salesretsub_stock_id'] = $item['salesub_stock_id'];
            $return_sub['salesretsub_prod_id'] = $item['salesub_prod_id'];
            $return_sub['salesretsub_branch_stock_id'] = $item['salesub_branch_stock_id'];

            $return_sub['salesretsub_unit_id'] = $item['rtn_unit_id'];
            $return_sub['salesretsub_rate'] = $item['rate_rtn'];
            $return_sub['salesretsub_qty'] = $item['qty_rtn'];

            $return_sub['salesretsub_tax_per'] = $item['tax_rate_rtn'];
            $return_sub['salesretsub_tax_rate'] = $item['rate_rtn'] * ($item['tax_rate_rtn'] / 100);
            $return_sub['salesretsub_godown_id'] = 0;
            $return_sub['salesretsub_taxcat_id'] = $item['prd_tax_cat_id'];
            $return_sub['branch_id'] = $this->branch_id;

            // $return_sub['salesretsub_salesret_id'] = $last_insert->purch_id;
            $return_sub['salesretsub_sales_inv_no'] = $last_insert['salesret_sales_inv_no'];
            $return_sub['salesretsub_date'] = date('Y-m-d', strtotime($request['ret_date']));
            $return_sub['salesretsub_flags'] = 1;
            $return_sub["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);
            $return_sub['is_web'] = 1;
            $return_sub['sync_completed'] = 1;

            SalesReturnSub::create($return_sub);
            $this->updateStocksQuantity($return_sub, $request);
        }
        app('App\Http\Controllers\Api\Migration\ledgerOpeingbalanceController')->setsalesreturnsubtovoucher($last_insert->salesret_id);
        return parent::displayMessage('Sales Return  Successfull');
    }
}
