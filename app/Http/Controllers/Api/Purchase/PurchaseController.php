<?php

namespace App\Http\Controllers;

namespace App\Http\Controllers\Api\Purchase;

use App\Http\Controllers\Api\ApiParent;
use App\Models\Company\Acc_branch;
use App\Models\Product\Product;
use App\Models\Purchase\Purchase;
use App\Models\Purchase\PurchaseReturn;
use App\Models\Purchase\PurchaseReturnSub;
use App\Models\Purchase\PurchaseSub;
use App\Models\Purchase\TmpPurchase;
use App\Models\Purchase\TmpPurchaseSub;
use App\Models\Stocks\BranchStocks;
use App\Models\Stocks\StockBatches;
use App\Models\Accounts\Acc_voucher;
use DB;
use Illuminate\Http\Request;
use validator;

class PurchaseController extends ApiParent
{

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function validatePurchase(Request $request)
    {
        $validator = Validator::make($request->all(), [], []);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }
    }

    public function Purchase(Request $request)
    {   
        $rule = [
            'purch_inv_no' => 'required',
            'purch_supp_id' => 'required',
            'purch_type' => 'required',
            'purch_pay_type' => 'required',
            'purch_amount' => 'required',
            'purch_tax' => 'required',

        ];
        if($request->purch_pay_type == 2){
            $rule['purch_acc_ledger_id'] = 'required|numeric|min:1';
        }
        if($request->purch_type == 4){
            $rule['purch_tax_ledger_id'] = 'required|numeric|min:1';
        }

        $validator = Validator::make( 
            $request->all(),
            $rule,
            [
                'purch_inv_no.required' => 'Required',
                'purch_supp_id.required' => 'Required',
                'purch_inv_date.required' => 'Required',
                'purch_type.required' => 'Required',
                'purch_pay_type.required' => 'Required',
                'purch_amount.required' => 'Required',
                'purch_tax.required' => 'Required',
                'purch_discount.required' => 'Required',
                'purch_friegth.required' => 'Required',
                'prd_tax_cat_id.required' => 'Required',
                'purch_acc_ledger_id.required' => 'Required',
                'purch_acc_ledger_id.min' => 'Required',
                'purch_acc_ledger_id.numeric' => 'Required',
                'purch_tax_ledger_id.required' => 'Required',
                'purch_tax_ledger_id.min' => 'Required',
                'purch_tax_ledger_id.numeric' => 'Required',
                // 'purch_ord_no.required' => 'Required',

            ]
        );
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }

        $save = $request->all();
        $purch_sub = $save['items'];
        unset($save['items']);

        if ($this->branch_id == 0) {
            $b_code = 'CMP';
        } else {
            $b_code = Acc_branch::where('branch_id', $this->branch_id)->first()->branch_name;
        }

        $purch_count = Purchase::where('purch_branch_id', $this->branch_id)->get()->count() + 1;
        $inv_count = $save['purch_inv_no'] ? $save['purch_inv_no'] : Purchase::get()->count() + 1;
        $save['purch_branch_id'] = $this->branch_id;
        $save['purch_frieght'] = isset($save['purch_frieght']) ? $save['purch_frieght'] : 0;
        $save['purch_date'] = isset($save['purch_date'])
        ? date('Y-m-d', strtotime($save['purch_date'])) : date('Y-m-d');
        $save['purch_id2'] = $b_code . '0000' . $purch_count;
        //die( $purch_count."==". $save['purch_id2']);
        $save['purch_inv_no'] = $inv_count;
        $save['branch_id'] = $this->branch_id;
        $save['purch_amount'] = $save['purch_net_amount'];
        $last_insert = Purchase::create($save);

        foreach ($purch_sub as $key => $purchaseSub) {

            $purchaseSub['purchsub_frieght'] = ($purchaseSub['purchsub_frieght'] > 0 && $purchaseSub['purchsub_qty'] > 0) ? ($purchaseSub['purchsub_frieght'] / $purchaseSub['purchsub_qty']) : 0;

            // code for tax cat issue fix
            if (isset($purchaseSub['prd_tax_cat_id'])) {
                $purchaseSub['purchsub_taxcat_id'] = $purchaseSub['prd_tax_cat_id'];
            } else {
                $purchaseSub['prd_tax_cat_id'] = 3;
                if ($purchaseSub['purchsub_tax_per'] == '50') {
                    $purchaseSub['prd_tax_cat_id'] = 2;
                }
                if ($purchaseSub['purchsub_tax_per'] == '5') {
                    $purchaseSub['prd_tax_cat_id'] = 1;
                }
                $purchaseSub['purchsub_taxcat_id'] = $purchaseSub['prd_tax_cat_id'];
            }

            // code for tax cat issue fix

            if ($data = $this->stockOperations($purchaseSub, $save['purch_date'], $purchaseSub['purchsub_frieght'])) {
                $purchaseSub['purchsub_branch_stock_id'] = $data['branch_stock_id'];
                $purchaseSub['purchsub_batch_id'] = $data['batch_id'];
                $purchaseSub['purchsub_expiry'] = $data['expiry'];

                $purchaseSub['purchsub_purch_id'] = $last_insert->purch_id;
                $purchaseSub['branch_id'] = $this->branch_id;
                // $purchaseSub['purchsub_date'] = date('Y-m-d');
                $purchaseSub['purchsub_date'] = $last_insert->purch_date;
                $purchaseSub['branch_id'] = $this->branch_id;
                $purchaseSub['server_sync_time'] = $this->server_sync_time;
                $purchaseSub['purchsub_tax'] = $purchaseSub['taxvalperqty'];
                
                $out = PurchaseSub::create($purchaseSub);
            } else {
                return parent::displayError('Invalid Request');
            }
        }
        app('App\Http\Controllers\Api\Migration\ledgerOpeingbalanceController')->setpurchasetovoucher($last_insert->purch_id);
       
        $msg = "Purchase successfull";
        return response()->json(['message' => $msg,'preview'=>$this->getPreview($last_insert->purch_id),'status' => 200]);

    }

    public function stockOperations($purch, $cal_date, $purchsub_frieght = 0)
    {

        $manufacture_date = isset($purch['manufacture_date']) ? $purch['manufacture_date'] : '';
        $expiry_date = isset($purch['expiry_date']) ? $purch['expiry_date'] : '';
        $batch_code = isset($purch['batch_code']) ? $purch['batch_code'] : '';

        $stockArray = [
            'prd_id' => $purch['purchsub_prd_id'],
            'stock_id' => $purch['purchsub_stock_id'],
            'opening_quantity' => $purch['purchsub_qty'],
            'gd_id' => $purch['purchsub_gd_id'],
            'purchase_rate' => $purch['purchsub_rate'] + $purchsub_frieght,
            'purchase_rate_unit' => 1,
            'purchase_rate_qty' => 1,
            'prd_base_unit_id' => 1,
            'if_purchase' => 1,
            'manufacture_date' => $manufacture_date,
            'expiry_date' => date('Y-m-d', strtotime($expiry_date)),
            'batch_code' => $batch_code,
            'branch_id' => $this->branch_id,
            'usr_id' => $this->usr_id,
            'cal_date' => $cal_date,

        ];

// code for updating last purch rate start
        $whr['bs_prd_id'] = $purch['purchsub_prd_id'];
        $whr['bs_branch_id'] = $this->branch_id;
        $bs_prate = $purch['purchsub_rate'] + $purchsub_frieght;

        $brc = BranchStocks::where($whr)->update(['bs_prate' => $bs_prate]);
// code for updating last purch rate end

        return app('App\Http\Controllers\Api\Stock\OpeningStockController')->openStockStart($stockArray);
    }

    public function purchasePreview(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            ['purch_id' => 'required|exists:purchases,purch_id'],
            ['purch_id.required' => 'Purchase number Required',
                'purch_id.exists' => 'Invalid Purchase No']
        );
        if ($validator->fails()) {
            return parent::displayError($validator->messages());
        }

        return parent::displayData($this->getPreview($request->purch_id));
    }

    public function getPreview($purch_id)
    {
        $slct = ['purch_id', 'purch_inv_no', 'purch_id2', 'purch_no', 'purch_supp_id', 'purch_tax_ledger_id', 'purch_ord_no', 'purch_type', 'purch_pay_type', 'purch_note',
            'purch_date', 'purch_inv_date', \DB::raw('(purch_amount-purch_tax-purch_frieght+purch_discount) as purch_amount'), 'purch_tax', 'purch_discount', 'purch_frieght', \DB::raw('(SELECT supp_name FROM erp_suppliers WHERE erp_suppliers.supp_id = erp_purchases.purch_supp_id ) as supp_name'), 'purch_flag',
            \DB::raw('(SELECT vchtype_name FROM erp_voucher_type WHERE erp_voucher_type.vchtype_id = erp_purchases.purch_pay_type ) as purchase_type'),
        ];

        $amnt = DB::raw('(purchsub_rate * purchsub_qty) as purchase_amount');
        $this->inrSlct = ['purchsub_purch_id', 'purchsub_prd_id', 'purchsub_qty', 'purchsub_rate', 'purchsub_frieght',
            'purchsub_tax', 'purchsub_tax_per', 'purchsub_unit_id', 'purchsub_gd_qty', 'purchsub_gd_id', $amnt];
        $rltn = [
            'items' => function ($qr) {
                $qr->select($this->inrSlct);
            },
            'supplier' => function ($qr) {
                $qr->select('supp_id', 'supp_name');
            },
        ];

        if($this->branch_id>0)
        {
            $data = Purchase::select($slct)->with($rltn)->where('branch_id', $this->branch_id)->where('purch_id', $purch_id)->first();
        }
        else
        {
            $data = Purchase::select($slct)->with($rltn)->where('purch_id', $purch_id)->first();
        }
      

        if (empty($data)) {
            return [];
        }
        $tot_qty = 0;
        foreach ($data['items'] as $k => $val) {

            $prd_id = trim($val['purchsub_prd_id']);
            $out = Product::select('prd_name', 'prd_barcode', 'prd_cat_id')->where('prd_id', $prd_id)->with('category')->first();
            $data['items'][$k]['prd_name'] = $out->prd_name;
            $data['items'][$k]['prd_barcode'] = $out->prd_barcode;
            $data['items'][$k]['category'] = $out->category->cat_name;

            $tot_qty = $tot_qty + $val['purchsub_qty'];
        }

        $data['purch_type_name'] = ($data['purch_pay_type'] == 1) ? 'Credit' : 'Cash';
        $data['total_items'] = count($data['items']);
        $data['total_qty'] = $tot_qty;
        $data['net_amount'] = ($data['purch_amount'] - $data['purch_discount']) + $data['purch_tax'] + $data['purch_frieght'];
        return $data;

    }

    public function purchaseVoid(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            ['purch_id' => 'required|exists:purchases,purch_id'],
            ['purch_id.required' => 'Purchase number Required',
                'purch_id.exists' => 'Invalid Purchase No']
        );
        if ($validator->fails()) {
            return parent::displayError($validator->messages());
        }
        $this->revertPurchase($request->purch_id);
        return parent::displayMessage('Purchase Deleted Successfully');
    }

    public function revertPurchase($purch_id)
    {
        $purch = Purchase::where('purch_id', $purch_id)->first();
        $purch_id = $purch['purch_id'];
        Purchase::where('purch_id', $purch_id)->update(['purch_flag' => 0]);

        $purchaseSub = PurchaseSub::where('purchsub_purch_id', $purch_id)->get()->toArray();

        foreach ($purchaseSub as $k => $item) {

            $item['purchretsub_prd_id'] = $item['purchsub_prd_id'];
            $item['purchretsub_stock_id'] = $item['purchsub_stock_id'];
            $item['purchretsub_qty'] = $item['purchsub_qty'];
            $item['purchretsub_rate'] = $item['purchsub_rate'];
            $item['purchretsub_batch_id'] = $item['purchsub_batch_id'];
            $item['purchretsub_gd_id'] = $item['purchsub_gd_id'];
            $this->remooveFromStock($item, $item['purchsub_date']);

        }
        PurchaseSub::where('purchsub_purch_id', $purch_id)->update(['purchsub_flag' => 0
            , 'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)]);
        Acc_voucher::where('branch_ref_no', $purch['purch_id2'])->update(['vch_flags' => 0
            , 'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)]);
    }

    public function remooveFromStock($purch, $cal_date)
    {

        $stockArray = [
            'prd_id' => $purch['purchretsub_prd_id'],
            'stock_id' => $purch['purchretsub_stock_id'],
            'qty' => $purch['purchretsub_qty'],
            'rate' => $purch['purchretsub_rate'],
            'batch_id' => $purch['purchretsub_batch_id'],
            'gd_id' => $purch['purchretsub_gd_id'],
            'branch_id' => $this->branch_id,
            'usr_id' => $this->usr_id,
            'if_purchase' => 1,
            'cal_date' => $cal_date,
        ];

        return app('App\Http\Controllers\Api\Stock\OpeningStockController')->remooveFromStock($stockArray);

    }

    public function item_sum(Request $purch)
    {
        $items = $purch->items;
        $purch_freight = $purch_amount = $purch_tax = 0;
        foreach ($items as $item) {
            $purch_freight += $item['purchsub_freight'];
            $purch_amount += $item['purchsub_amount'];
            $purch_tax += $item['purchsub_tax'];

        }
        return parent::displayData([
            'purch_freight' => $purch_freight,
            'purch_amount' => $purch_amount,
            'purch_tax' => $purch_tax]);
    }

    public function get_purch_num()
    {
        $purch_date = date("d/m/Y");
        $purch_count = Purchase::get()->count() + 1;
        return parent::displayData(['purch_num' => $purch_count, 'purch_date' => $purch_date]);
    }

    public function updatePurchNote(Request $request)
    {
        $updates = $request->all();

        $dd = Purchase::find($request['purch_id']);
        if ($dd) {
            $dd->fill($updates);
            $dd->save();

            return parent::displayMessage('Updated Successfully');
        } else {
            return parent::displayError('Somthing Went Wrong');
        }
    }

    public function TempPurchase(Request $request)
    {

        // $validator = Validator::make(
        //     $request->all(),
        //     [
        //         'purch_inv_no' => 'required',
        //         'purch_supp_id' => 'required',
        //         'purch_inv_date' => 'required',
        //         'purch_type' => 'required',

        //         'purch_pay_type' => 'required',
        //         'purch_amount' => 'required',
        //         'purch_tax' => 'required',
        //         'purch_discount' => 'required',

        //         'purch_friegth' => 'required',
        //         'purch_tax_ledger_id' => 'required',
        //         'purch_ord_no' => 'required',

        //     ],
        //     [
        //         'purch_inv_no.required' => 'Required',
        //         'purch_supp_id.required' => 'Required',
        //         'purch_inv_date.required' => 'Required',
        //         'purch_type.required' => 'Required',

        //         'purch_pay_type.required' => 'Required',
        //         'purch_amount.required' => 'Required',
        //         'purch_tax.required' => 'Required',
        //         'purch_discount.required' => 'Required',

        //         'purch_friegth.required' => 'Required',
        //         'purch_tax_ledger_id.required' => 'Required',
        //         'purch_ord_no.required' => 'Required',

        //     ]
        // );
        // if ($validator->fails()) {
        //     return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        // }

        $save = $request->all();
        $purch_sub = $save['items'];

        unset($save['items']);
        if ($this->branch_id == 0) {
            $b_code = 'CMP';
        } else {
            $b_code = Acc_branch::where('branch_id', $this->branch_id)->first()->branch_name;
        }

        $save['temp_purch_json'] = json_encode($request->all());

        if (isset($save['draft_id']) && $save['draft_id'] != "") {

            $temp = TmpPurchase::where('purch_id', $save['draft_id'])->first();

            $purch_count = Purchase::where('purch_branch_id', $this->branch_id)->get()->count() + 1;
            $inv_count = isset($save['purch_inv_no']) ? $save['purch_inv_no'] : '';
            $save['purch_branch_id'] = $this->branch_id;
            $save['purch_date'] = isset($save['purch_date'])
            ? date('Y-m-d', strtotime($save['purch_date'])) : date('Y-m-d');
            $save['purch_id2'] = $b_code . '0000' . $purch_count;
            $save['purch_inv_no'] = $inv_count;
            $save['purch_amount'] = $save['purch_amount'];

//  return $save;

            $updateTemp = TmpPurchase::where('purch_id', $save['draft_id'])->get()->first();
            $updateTemp->fill($save)->save();
            $last_insert = TmpPurchase::where('purch_id', $save['draft_id'])->first();

            foreach ($purch_sub as $key => $purchaseSub) {

                $tempItem = TmpPurchaseSub::where('purchsub_purch_id', $save['draft_id'])->where('purchsub_prd_id', $purchaseSub['purchsub_prd_id'])->where('purchsub_rate', $purchaseSub['purchsub_rate'])->where('company_id', $save['company_id'])->first();
                if ($tempItem) {

                    $purchaseSub['purchsub_branch_id'] = 0;
                    $purchaseSub['purchsub_batch_id'] = 0;
                    $purchaseSub['purchsub_expiry'] = 0;
                    if(is_int($purchaseSub['purchsub_frieght'])){
                        $purchaseSub['purchsub_frieght'] = $purchaseSub['purchsub_frieght'];
                    }else{
                        $purchaseSub['purchsub_frieght'] = 0;
                    }
                    $purchaseSub['purchsub_purch_id'] = $last_insert->purch_id;
                    $purchaseSub['branch_id'] = $this->branch_id;

                    $purchaseSub['purchsub_date'] = $save['purch_date'];
                    $ptempItem = TmpPurchaseSub::find($tempItem->purchsub_id)->first();
                    $ptempItem->fill($purchaseSub)->save();

                } else {

                    $purchaseSub['purchsub_branch_id'] = 0;
                    $purchaseSub['purchsub_batch_id'] = 0;
                    $purchaseSub['purchsub_expiry'] = 0;
                    if(is_int($purchaseSub['purchsub_frieght'])){
                        $purchaseSub['purchsub_frieght'] = $purchaseSub['purchsub_frieght'];
                    }else{
                        $purchaseSub['purchsub_frieght'] = 0;
                    }
                    $purchaseSub['purchsub_purch_id'] = $last_insert->purch_id;
                    $purchaseSub['branch_id'] = $this->branch_id;
                    $purchaseSub['purchsub_date'] = date('Y-m-d');
                    $out = TmpPurchaseSub::create($purchaseSub);

                }
            }

        } else {

            $purch_count = Purchase::where('purch_branch_id', $this->branch_id)->get()->count() + 1;
            $inv_count = isset($save['purch_inv_no']) ? $save['purch_inv_no'] : '';
            $save['purch_branch_id'] = $this->branch_id;
            $save['purch_date'] = date('Y-m-d');
            $save['purch_id2'] = $b_code . '0000' . $purch_count;
            $save['purch_inv_no'] = $inv_count;
            $save['purch_amount'] = $save['purch_amount'];

            $last_insert = TmpPurchase::create($save);

            foreach ($purch_sub as $key => $purchaseSub) {

                $purchaseSub['purchsub_branch_id'] = 0;
                $purchaseSub['purchsub_batch_id'] = 0;
                $purchaseSub['purchsub_expiry'] = 0;
                if(is_int($purchaseSub['purchsub_frieght'])){
                    $purchaseSub['purchsub_frieght'] = $purchaseSub['purchsub_frieght'];
                }else{
                    $purchaseSub['purchsub_frieght'] = 0;
                }
                $purchaseSub['purchsub_purch_id'] = $last_insert->purch_id;
                $purchaseSub['branch_id'] = $this->branch_id;
                $purchaseSub['purchsub_date'] = date('Y-m-d');

                $out = TmpPurchaseSub::create($purchaseSub);

            }
        }
        return $last_insert->purch_id;

    }

    public function TempPurchasePreview(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            ['purch_id' => 'required|exists:tmp_purchases,purch_id'],
            ['purch_id.required' => 'Purchase number Required',
                'purch_id.exists' => 'Invalid Purchase No']
        );
        if ($validator->fails()) {
            return parent::displayError($validator->messages());
        }

        $data = TmpPurchase::select('temp_purch_json as data', 'purch_id as draft_id')->where('purch_id', $request->purch_id)->first()->toArray();
        $res['data'] = json_decode(stripslashes($data['data']));
        $res['draft_id'] = $data['draft_id'];

        return $res;

    }

    public function removeTempPurch(Request $request)
    {

        $data = TmpPurchase::where('purch_id', $request->purch_id)->delete();
        if ($data) {
            $data = TmpPurchaseSub::where('purchsub_purch_id', $request->purch_id)->delete();

        }

        return parent::displayMessage('Removed From Draft');

    }

    public function ListTempPurchase(Request $request)
    {

        $slct = ['purch_id', 'purch_inv_no', 'purch_id2', 'purch_no', 'purch_supp_id', 'purch_tax_ledger_id', 'purch_ord_no', 'purch_type', 'purch_note', 'purch_pay_type',
            'purch_date', 'purch_inv_date', 'purch_amount', 'purch_tax', 'purch_discount', 'purch_frieght', \DB::raw('(SELECT supp_name FROM erp_suppliers WHERE erp_suppliers.supp_id = erp_tmp_purchases.purch_supp_id ) as supp_name'),
            \DB::raw('(SELECT vchtype_name FROM erp_voucher_type WHERE erp_voucher_type.vchtype_id = erp_tmp_purchases.purch_pay_type ) as purchase_type'), DB::raw('(purch_amount + purch_tax) as purchase_net_amount'),
        ];

        $amnt = DB::raw('(purchsub_rate * purchsub_qty) as purchase_amount');
        $this->inrSlct = ['purchsub_purch_id', 'purchsub_prd_id', 'purchsub_qty', 'purchsub_rate', 'purchsub_frieght',
            'purchsub_tax', 'purchsub_tax_per', 'purchsub_unit_id', 'purchsub_gd_qty', 'purchsub_gd_id', $amnt];
        $rltn = [
            'items' => function ($qr) {
                $qr->select($this->inrSlct);
            },
            'supplier' => function ($qr) {
                $qr->select('supp_id', 'supp_name');
            },
        ];
        $datas = TmpPurchase::select($slct)->where('purch_flag', 1)->with($rltn)->get();

        if (empty($datas)) {
            return [];
        }
        foreach ($datas as $key => $data) {
            foreach ($data['items'] as $k => $val) {
                $prd_id = trim($val['purchsub_prd_id']);
                $out = Product::select('prd_name', 'prd_barcode')->where('prd_id', $prd_id)->first();

                $datas[$key]['items'][$k]['prd_name'] = $out->prd_name;
                $datas[$key]['items'][$k]['prd_barcode'] = $out->prd_barcode;
            }
        }
        return $datas;

    }

    public function PurchaseReturn(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            [

                'purchret_supp_id' => 'required',
                'purchret_amount' => 'required',
                'purchret_date' => 'required',
                'purchret_discount' => 'required',
                'purchret_pay_type' => 'required',
                'purchret_gd_id' => 'required',
            ],
            [

                'purchret_supp_id.required' => 'Required',
                'purchret_amount.required' => 'Required',
                'purchret_date.required' => 'Required',
                'purchret_discount.required' => 'Required',
                'purchret_pay_type.required' => 'Required',
                'purchret_gd_id.required' => 'Required',
            ]
        );
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }

        $save = $request->all();
        $purch_sub = $save['items'];
        unset($save['items']);
        unset($save['prd_tax_cat_id']);
        unset($save['itemname']);
        unset($save['itenam']);
        if ($this->branch_id == 0) {
            $b_code = 'CMP';
        } else {
            $b_code = Acc_branch::where('branch_id', $this->branch_id)->first()->branch_name;
        }

        $purch_count = PurchaseReturn::where('branch_id', $this->branch_id)->get()->count() + 1;
        $inv_count = isset($save['purchret_inv_no']) ? $save['purchret_inv_no'] : PurchaseReturn::get()->count() + 1;
        $save['branch_id'] = $this->branch_id;
        $save['purchret_date'] = isset($save['purchret_date'])
        ? date('Y-m-d', strtotime($save['purchret_date'])) : date('Y-m-d');
        $save['purchret_id2'] = $b_code . '0000' . $purch_count;
        $save['purchret_inv_no'] = $inv_count;
        $save['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
        $last_insert = PurchaseReturn::create($save);

        foreach ($purch_sub as $key => $purchaseSub) {

            unset($purchaseSub['sl_no']);
            unset($purchaseSub['prd_barcode']);
            unset($purchaseSub['itemname']);

            $purchaseSub['purchretsub_purch_id'] = $last_insert->purchret_id;
            $purchaseSub['branch_id'] = $this->branch_id;

            $purchaseSub['purchretsub_tax'] = $purchaseSub['purchretsub_tax'];

            $purchaseSub['purchretsub_date'] = $save['purchret_date'];
            $purchaseSub['purchretsub_gd_id'] = $last_insert->purchret_gd_id;

            $purchaseSub['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            $out = PurchaseReturnSub::create($purchaseSub);
            $data = $this->remooveFromStock($purchaseSub, $save['purchret_date']);

        }
        app('App\Http\Controllers\Api\Migration\ledgerOpeingbalanceController')->setpurchasereturntovoucher($last_insert->purchret_id);
        return parent::displayMessage('Purchase Successfull');

    }

    public function ListPurchaseReturn(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            ['purchret_id' => 'required|exists:purchase_return'],
            ['purchret_id.required' => 'Purchase number Required',
                'purchret_id.exists' => 'Invalid Purchase No']
        );
        if ($validator->fails()) {
            return parent::displayError($validator->messages());
        }

        $slct = ['purchret_id', 'purchret_date', 'purchret_inv_no', 'purchret_id2', 'purchret_no',
            'purchret_supp_id', 'purchret_tax_ledger_id', 'purchret_type', 'purchret_pay_type',
            'purchret_inv_date', 'purchret_amount', 'purchret_tax', 'purchret_discount',
            'purchret_frieght'];

        $amnt = DB::raw('(purchretsub_rate * purchretsub_qty) as purchase_amount');
        $this->inrSlct = ['prd_name', 'prd_barcode', 'unit_name', 'purchretsub_purch_id', 'purchretsub_prd_id', 'purchretsub_qty', 'purchretsub_rate',
            'purchretsub_tax', 'purchretsub_tax_per', 'purchretsub_unit_id', 'purchretsub_gd_id', $amnt];
        $rltn = [
            'items' => function ($qr) {
                $qr->select($this->inrSlct);
            },
            'ledger' => function ($qr) {
                $qr->select('*');
            },
        ];

        $qr = PurchaseReturn::select($slct);

        if ($request->purchret_id) {
            $qr = $qr->where('purchret_id', $request->purchret_id);
        }
        $qr = $qr->where('purchret_flag', 1);
        $data = $qr->with($rltn)->get();
        return parent::displayData($data);
    }

    public function voidTranReturn(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            ['purchret_id' => 'required|exists:purchase_return,purchret_id'],
            ['purchret_id.required' => 'Return id Required',
                'purchret_id.exists' => 'Invalid Return id']
        );
        if ($validator->fails()) {
            return parent::displayError($validator->messages());
        }
        $this->revertPurchaseReturn($request->purchret_id);
        return parent::displayMessage('Purchase Return Reverted Successfully');
    }

    public function revertPurchaseReturn($purchret_id)
    {

        $purch = PurchaseReturn::where('purchret_id', $purchret_id)->first();

        PurchaseReturn::where('purchret_id', $purchret_id)->update(['purchret_flag' => 0]);

        $purchaseSub = PurchaseReturnSub::where('purchretsub_purch_id', $purchret_id)->get()->toArray();
        foreach ($purchaseSub as $k => $item) {

            $batch = StockBatches::where('sb_batch_code', $item['purchretsub_batch_id'])->first();

            $item['manufacture_date'] = isset($batch['sb_manufacture_date']) ? $batch['sb_manufacture_date'] : '';
            $item['expiry_date'] = isset($batch['sb_expiry_date']) ? $batch['sb_expiry_date'] : '';
            $item['batch_code'] = isset($batch['sb_batch_code']) ? $batch['sb_batch_code'] : '';

            $item['purchsub_prd_id'] = $item['purchretsub_prd_id'];
            $item['purchsub_stock_id'] = $item['purchretsub_stock_id'];
            $item['purchsub_qty'] = $item['purchretsub_qty'];
            $item['purchsub_gd_id'] = $item['purchretsub_gd_id'];
            $item['purchsub_rate'] = $item['purchretsub_rate'];

            $this->stockOperations($item, $item['purchretsub_date']);

            PurchaseReturnSub::where('purchretsub_id', $item['purchretsub_id'])->update(['purchretsub_flag' => 0]);
        }
        Acc_voucher::where('branch_ref_no', $purch['purchret_id2'])->update(['vch_flags' => 0
            , 'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)]);
    }

    public function get_purch_ret_num()
    {
        $purch_ret_count = PurchaseReturn::get()->count() + 1;
        $purch_date = date("d/m/Y");
        return parent::displayData(['purch_ret_num' => $purch_ret_count, 'purch_ret_date' => $purch_date]);
    }

    public function ListAllPurchaseReturn()
    {
        $data = PurchaseReturn::leftjoin('godown_master', 'purchase_return.purchret_gd_id',
            '=', 'godown_master.gd_id')
            ->orderBy('purchret_id','desc')->paginate(10, array(DB::raw('purchret_amount as total'), 'purchret_id', 'purchret_date', 'godown_master.gd_name'));

        return parent::displayData($data);

    }

}
