<?php

namespace App\Http\Controllers;

namespace App\Http\Controllers\Api\Purchase;

use App\Http\Controllers\Api\ApiParent;
use App\Models\Accounts\Acc_ledger;
use App\Models\Purchase\Supplier;
use DB;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Models\Company\Acc_branch;
use App\Models\Accounts\Acc_voucher;

use validator;

// use App\Models\Godown\GodownStock;
// use App\Models\Godown\GodownStockLog;
// use App\Models\Product\Product;
// use App\Models\Stocks\BranchStockLogs;
// use App\Models\Stocks\BranchStocks;
// use App\Models\Stocks\CompanyStocks;
// use App\Models\Stocks\OpeningStockLog;
// use App\Models\Stocks\StockBatches;
// use App\Models\Stocks\StockDailyUpdates;
// use App\Models\Stocks\StockUnitRates;

class SupplierController extends ApiParent
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function validateSupplierAdd(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'supp_name' => 'required|unique:suppliers',
                'supp_code' => 'required',
            ],
            [
                'supp_name.required' => 'Required',
                'supp_code.required' => 'Required',
            ]
        );
        if ($validator->fails()) {
            return parent::displayError($validator->messages());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addNewSupplier(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            [
                'supp_name' => 'required|unique:suppliers',
                'supp_code' => 'required',
            ],
            [
                'supp_name.required' => 'Required',
                'supp_code.required' => 'Required',
                'supp_name.unique' => 'Already Exist',
            ]
        );
        if ($validator->fails()) {
            return parent::displayError($validator->messages());
        }
        $supplier = $request->all();
    if(isset($supplier['op_bal_type'])){
        if($supplier['op_bal_type']){
            $supplier['opening_balance'] = 0 - $supplier['op_bal'];
            }else{
                $supplier['opening_balance'] = 0 + $supplier['op_bal'];
            }
        }else{
            $supplier['opening_balance'] = (isset($supplier['op_bal']) ? $supplier['op_bal'] : 0);
        }

        $ledger =
            [
            'ledger_name' => (isset($supplier['supp_name']) ? $supplier['supp_name'] : ""),
            'ledger_accgrp_id' => 20,
            'ledger_acc_type' => 2,
            'ledger_address' => (isset($supplier['supp_address1']) ? $supplier['supp_address1'] : ""),
            'ledger_tin' => (isset($supplier['supp_tin']) ? $supplier['supp_tin'] : ""),
            'ledger_alias' => (isset($supplier['supp_alias']) ? $supplier['supp_alias'] : ""),
            'ledger_code' => (isset($supplier['supp_code']) ? $supplier['supp_code'] : ""),
            'ledger_email' => (isset($supplier['supp_email']) ? $supplier['supp_email'] : ""),
            'ledger_mobile' => (isset($supplier['supp_mob']) ? $supplier['supp_mob'] : ""),
            'ledger_notes' => (isset($supplier['supp_notes']) ? $supplier['supp_notes'] : ""),
            'ledger_due' => (isset($supplier['supp_due']) ? $supplier['supp_due'] : ""),
            'branch_id' => $this->branch_id,
            'server_sync_time' => $this->server_sync_time,
            'opening_balance' => $supplier['opening_balance'],

        ];

        $res = Acc_ledger::create($ledger);

        if($res->opening_balance != 0){
            $where = array();
            $where[] = ['branch_id','=', $this->branch_id];
            $branch = Acc_branch::where($where)->first();
            $info = array();
            $max = Acc_voucher::max('vch_no');
            $info['vch_no'] = (empty($max)) ? 1 : $max+1;
            $info['vch_ledger_from'] = $res->ledger_id;
            $info['vch_ledger_to'] = $res->ledger_id;
            $info['vch_date'] = date('Y-m-d',(strtotime ( '-1 day' , strtotime ( $branch['branch_open_date']))));
            $info['vch_in'] = (!$supplier['op_bal_type']) ? abs($request->op_bal) : 0;
            $info['vch_out'] = ($supplier['op_bal_type']) ? abs($request->op_bal) : 0;
            $info['vch_vchtype_id'] = 14;
            $info['vch_notes'] = 'Starting balance';
            $info['vch_added_by'] = $this->usr_id;
            $info['vch_id2'] = 0;
            $info['vch_from_group'] = 20;
            $info['vch_flags'] = 1;
            $info['ref_no'] = 0;
            $info['branch_id'] = $this->branch_id;
            $info['is_web'] = 1;
            $info['godown_id'] = 0;
            $info['van_id'] = 0;
            $info['branch_ref_no'] = 0;
            $info['sub_ref_no'] = 0;
            $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            $voucher = Acc_voucher::updateOrCreate(
                    [
                            'vch_ledger_from' => $res->ledger_id, 
                            'vch_ledger_to' => $res->ledger_id,
                            'vch_vchtype_id' => 14,
                            'vch_date' => $info['vch_date'],
                            'branch_id' => $this->branch_id, 
                            'van_id' => 0 
                    ],
                    $info
            );
            Acc_voucher::where(['vch_in'=>0,'vch_out'=>0])->delete();
        }

        $supplier['supp_ledger_id'] = $res['ledger_id'];
        $supplier['supp_branch_id'] = $this->branch_id;
        $supplier['branch_id'] = $this->branch_id;
        $supplier['server_sync_time'] = $this->server_sync_time;
        $res = Supplier::create($supplier);
        return parent::displayMessage("Supplier Added");
    }


    public function listSupplier()
    {
        $data = Supplier::select('*')->where('supp_flag',1)->orderBy('supp_id','desc')
        ->where('branch_id',$this->branch_id)->paginate(100);
        return response()->json(['data' => $data, 'status' => $this->success_stat]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function show(supplier $supplier)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function editSupplier(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'supp_name' => ['required', Rule::unique('suppliers')->ignore($request['supp_id'], 'supp_id')],
                'supp_code' => 'required',
            ],
            [
                'supp_name.required' => 'Required',
                'supp_code.required' => 'Required',
                'supp_name.unique' => 'Already Exist',
            ]
        );
        if ($validator->fails()) {
            return parent::displayError($validator->messages());
        }

        if($request['op_bal_type']){
            $request['opening_balance'] = 0 - $request['op_bal'];
            }else{
                $request['opening_balance'] = 0 + $request['op_bal'];
            }

        $res = 1;
        DB::beginTransaction();
        try {

            $supplier = Supplier::find($request->supp_id);
            $res = $supplier->fill($request->all())->save();

            $data = Acc_ledger::find($supplier->supp_ledger_id);
            $res = $data->fill(['ledger_name' => $request->supp_name,
                'ledger_name' => (isset($request['supp_name']) ? $request['supp_name'] : ""),
                'ledger_address' => (isset($request['supp_address1']) ? $request['supp_address1'] : ""),
                'ledger_tin' => (isset($request['supp_tin']) ? $request['supp_tin'] : ""),
                'ledger_alias' => (isset($request['supp_alias']) ? $request['supp_alias'] : ""),
                'ledger_code' => (isset($request['supp_code']) ? $request['supp_code'] : ""),
                'ledger_email' => (isset($request['supp_email']) ? $request['supp_email'] : ""),
                'ledger_mobile' => (isset($request['supp_mob']) ? $request['supp_mob'] : ""),
                'ledger_notes' => (isset($request['supp_notes']) ? $request['supp_notes'] : ""),
                'ledger_due' => (isset($request['supp_due']) ? $request['supp_due'] : ""),
                'branch_id' => $this->branch_id,
                'server_sync_time' => $this->server_sync_time,
                'opening_balance' => $request['opening_balance']

            ])->save();

            $where = array();
            $where[] = ['branch_id','=', $this->branch_id];
            $branch = Acc_branch::where($where)->first();
            $info = array();
            // Code for remove , at initail no voucher added  
            $where = [
                'vch_ledger_from' => $supplier->supp_ledger_id, 
                'vch_ledger_to' => $supplier->supp_ledger_id,
                'vch_vchtype_id' => 14,
                'vch_date' => date('Y-m-d',(strtotime ( '-1 day' , strtotime ( $branch['branch_open_date'])))),
                'branch_id' => $this->branch_id, 
                'van_id' => 0 
            ];
            $voucher = Acc_voucher::where($where)->first();
            if(empty($voucher)){
                $max = Acc_voucher::max('vch_no');
                $info['vch_no'] = (empty($max)) ? 1 : $max+1;
            }

            $info['vch_ledger_from'] = $supplier->supp_ledger_id;
            $info['vch_ledger_to'] = $supplier->supp_ledger_id;
            $info['vch_date'] = date('Y-m-d',(strtotime ( '-1 day' , strtotime ( $branch['branch_open_date']))));
            $info['vch_in'] = (!$request->op_bal_type) ? abs($request->op_bal) : 0;
            $info['vch_out'] = ($request->op_bal_type) ? abs($request->op_bal) : 0;
            $info['vch_vchtype_id'] = 14;
            $info['vch_notes'] = 'Starting balance';
            $info['vch_added_by'] = $this->usr_id;
            $info['vch_id2'] = 0;
            $info['vch_from_group'] = 20;
            $info['vch_flags'] = 1;
            $info['ref_no'] = 0;
            $info['branch_id'] = $this->branch_id;
            $info['is_web'] = 1;
            $info['godown_id'] = 0;
            $info['van_id'] = 0;
            $info['branch_ref_no'] = 0;
            $info['sub_ref_no'] = 0;
            $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            $voucher = Acc_voucher::updateOrCreate(
                    [
                            'vch_ledger_from' => $supplier->supp_ledger_id, 
                            'vch_ledger_to' => $supplier->supp_ledger_id,
                            'vch_vchtype_id' => 14,
                            'vch_date' => $info['vch_date'],
                            'branch_id' => $this->branch_id, 
                            'van_id' => 0 
                    ],
                    $info
            );
            Acc_voucher::where(['vch_in'=>0,'vch_out'=>0])->delete();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            // throw $e;
            $res = 0;
        }

        if ($res) {
            return parent::displayMessage("Supplier Updated");
        } else {
            return parent::displayError("Opps , Something Went Wrong Please Try Again After Some Times");
        }

    }

    /**
     * @api {get} http://127.0.0.1:8000/api/get_suppliers  Get Supplier Details
     * @apiName getSuppliers
     * @apiGroup Accounts
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)

     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} data Customer Data
     *
     */

    public function getSuppliers()
    {

        return response()->json(['data' => Acc_supplier::get(), 'status' => $this->success_stat]);
    }

    /**
     * @api {Post} http://127.0.0.1:8000/api/search_supplier  Search Ledger
     * @apiName searchSupplier
     * @apiGroup Accounts
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {String} supp_name Supplier Keyword
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} data All Supplier Details
     *
     */

    public function searchSupplier(Request $request)
    {
        if( $this->branch_id>0)
        {
            if ($request['supp_name'] != "") {
                $acc_supp = Supplier::where('supp_name', 'like', '%' . $request['supp_name'] . '%')
                    ->where('branch_id', $this->branch_id)
                    ->take(50)
                    ->get();
                return response()->json(['data' => $acc_supp, 'status' => $this->success_stat]);
            } else {
                $data = Supplier::where('branch_id', $this->branch_id)->get();
                return response()->json(['data' => $data, 'status' => $this->success_stat]);
            }
        }
        else
        {
            if ($request['supp_name'] != "") {
                $acc_supp = Supplier::where('supp_name', 'like', '%' . $request['supp_name'] . '%')
                    ->take(50)
                    ->get();
                return response()->json(['data' => $acc_supp, 'status' => $this->success_stat]);
            } else {
                $data = Supplier::get();
                return response()->json(['data' => $data, 'status' => $this->success_stat]);
            }
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, supplier $supplier)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function destroy(supplier $supplier)
    {
        //
    }

      public function listSupplierList(Request $request)
    {
        if($request->search==""){
      $data = Supplier::select('*')->where('supp_flag',1) ->where('branch_id',$this->branch_id)->orderBy('supp_id','desc')->paginate($this->per_page);            
        }else{
        $data = Supplier::select('*')->where('supp_flag',1)->where('branch_id',$this->branch_id)->where(function ($q) use ($request) {
    $q->where('supp_name', 'like', '%' .  $request->search . '%')->orWhere('supp_alias', 'like', '%' .  $request->search . '%')->orWhere('supp_code', 'like', '%' .  $request->search . '%')->orWhere('opening_balance', 'like', '%' .  $request->search . '%');})->orderBy('supp_id','desc')->paginate($this->per_page);  

    }
 
        return response()->json(['data' => $data, 'status' => $this->success_stat]);

    }

}
