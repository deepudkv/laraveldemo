<?php

namespace App\Http\Controllers;

namespace App\Http\Controllers\Api\Purchase;

use App\Http\Controllers\Api\ApiParent;
use App\Models\Accounts\Acc_ledger;
use App\Models\Purchase\PurchaseSub;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use validator;
class PurchaseSubController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\purchase_sub  $purchase_sub
     * @return \Illuminate\Http\Response
     */
    public function show(purchase_sub $purchase_sub)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\purchase_sub  $purchase_sub
     * @return \Illuminate\Http\Response
     */
    public function edit(purchase_sub $purchase_sub)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\purchase_sub  $purchase_sub
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, purchase_sub $purchase_sub)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\purchase_sub  $purchase_sub
     * @return \Illuminate\Http\Response
     */
    public function destroy(purchase_sub $purchase_sub)
    {
        //
    }
}
