<?php

namespace App\Http\Controllers\Api\Van;

use App\Http\Controllers\Api\ApiParent;
use App\Models\Van\Vanline;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class VanlineController extends ApiParent
{

    // ===================== Code Added By Deepu =======================

    public function vanOperations(Request $request, $type)
    {

        switch ($type) {
            case 'search':
                return $this->searchVanlines($request);
                break;

            case 'add':
                return $this->addVanline($request);
                break;
            case 'list':
                return $this->listVanlines($request);
                break;
            case 'list_all':
                return $this->listAllVanlines($request);
                break;
            case 'update':
                return $this->updateVanline($request);
                break;

            case 'activate':
                return $this->activateVanline($request);
                break;

              case 'all':
                return $this->getVanlines($request);
                break;
                

            default:
                return response()->json(['error' => 'Invalid Endpoint', 'status' => $this->badrequest_stat]);
                break;
        }
    }

    /**
     * @api {post} http://127.0.0.1:8000/api/godown/add  Add Godown
     * @apiName addGodown
     * @apiGroup Godown
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {String} gd_name Godown Name (Required)
     * @apiParam {String} gd_code Godown Code
     * @apiParam {String} gd_cntct_num Godown contact Number
     * @apiParam {String} gd_cntct_person Godown contact person
     * @apiParam {String} gd_address Godown address
     * @apiParam {String} gd_desc Godown Description
     *
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} message Success Message
     *
     * @apiError {String} error Error Message.
     *
     */

    public function addVanline($request)
    {

        $validator = Validator::make(
            $request->all(),
            [
                'vanline_name' => 'required|unique:vanline',
                'vanline_desc' => 'required',

            ],
            [
                'vanline_name.required' => 'Required',
                'vanline_name.unique' => 'Already Exists',
                'vanline_desc.required' => 'Required',

            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }

        $van_line = $request->all();

        Vanline::create($van_line);
        return response()->json(['message' => 'Saved Successfully', 'status' => $this->created_stat]);
    }

    public function listVanlines()
    {
        return parent::displayData(Vanline::where('branch_id',$this->branch_id)->paginate($this->per_page));
    }

    public function listAllVanlines()
    {
        return parent::displayData(Vanline::where('branch_id',$this->branch_id)->get());
    }

    public function searchVanlines($request)
    {
        return parent::displayData(Vanline::where('branch_id',$this->branch_id)->where('vanline_name','like', $request['vanline_name'] . '%')->limit(10)->get());
    }
    /**
     * @api {post} http://127.0.0.1:8000/api/godown/edit  Edit Godown
     * @apiName editGodown
     * @apiGroup Godown
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {BigInt} gd_id Godown Id (Required)
     * @apiParam {String} gd_name Godown Name (Required)
     * @apiParam {String} gd_code Godown Code
     * @apiParam {String} gd_cntct_num Godown contact Number
     * @apiParam {String} gd_cntct_person Godown contact person
     * @apiParam {String} gd_address Godown address
     * @apiParam {String} gd_desc Godown Description
     *
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} message Success Message
     *
     * @apiError {String} error Error Message.
     *
     */

    public function updateVanline($request)
    {

    //return $request['vanline_id'];  
        $validator = Validator::make(
            $request->all(),
            [
                'vanline_id' => 'required',
            'vanline_name' => ['required',Rule::unique('vanline')->ignore($request['vanline_id'], 'vanline_id')],              
                'vanline_desc' => 'required',

            ],
            [
                'vanline_name.required' => 'Required',
                'vanline_name.unique' => 'Already Exists',
                'vanline_desc.required' => 'Required',

            ]

        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }

        $vanline = $request->all();
        $db = Vanline::find($request->vanline_id);
        $db->fill($vanline);
        $db->save();
        return response()->json(['message' => 'Updated Successfully', 'status' => $this->created_stat]);
    }

    /**
     * @api {post} http://127.0.0.1:8000/api/search_godown  Search Godown
     * @apiName searchGodown
     * @apiVersion 0.1.0
     * @apiGroup Godown
     *
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {varchar} gd_name Godown Name
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} data Godowns

     */

    public function activateVanline(Request $request)
    {
        $vanline = $request->all();
        $db = Vanline::find($request->vanline_id);
        $active = $db['vanline_flag'];
        $db['vanline_flag'] = ($active) ? 0 : 1;
        $db['server_sync_time'] =date('ymdHis') . substr(microtime(), 2, 6);
        $db->save();

        return parent::displayMessage(($active) ? 'Inactivated' : 'Activated');
    }

    /**
     * @api {post} http://127.0.0.1:8000/api/get_godowns Get Godowns
     * @apiName getGodownByBranch
     * @apiVersion 0.1.0
     * @apiGroup Godown
     *
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} data Godowns by branch

     */

    public function getGodownByBranch()
    {

        $user = auth()->user();
        $branch_id = ($user != null ? $user->branch_id : 0);

        $gdData = GodownMaster::where('branch_id', $branch_id)->get();

        return parent::displayData($gdData, $this->success_stat);
    }

    public function getAllGodowns(Request $rq)
    {
        $gdData = [];
        if ($gd_name = isset($rq['gd_name']) ? $rq['gd_name'] : 0) {
            $gdData = GodownMaster::select('gd_id', 'gd_name', 'gd_code')->where('gd_name', 'like', '%' . $gd_name . '%')
                ->where('branch_id', $rq->branch_id)->take(10)->get();
        } else {
            $gdData = GodownMaster::select('gd_id', 'gd_name', 'gd_code')->where('branch_id', $rq->branch_id)->take(50)->get();
        }
        $gdData[] = ['gd_id' => 0,
            'gd_name' => 'Shop',
            'gd_code' => 'SP'];
        return parent::displayData($gdData, $this->success_stat);
    }

    // ===================== End of Code Added By Rakesh =======================

      public function getVanlines($request)
    {
        return parent::displayData(Vanline::where('branch_id',$this->branch_id)->get());
    }

    // ==========Amit =====================

}
