<?php

namespace App\Http\Controllers\Api\Van;

use App\Http\Controllers\Api\ApiParent;
use App\Models\Product\Product;
use App\Models\Product\ProdUnit;
use App\Models\Van\Van;
use App\Models\Van\VanCategories;
use App\Models\Van\VanStock;
use App\Models\Van\VanUsers;
use Illuminate\Http\Request;
use App\Models\Accounts\Acc_ledger;
use App\Models\Company\Acc_branch;
use App\Models\Accounts\Acc_voucher;
use Illuminate\Support\Facades\Input;
use Validator;

class VanController extends ApiParent
{

    // ===================== Code Added By Deepu =======================

    public function vanOperations(Request $request, $type)
    {

        switch ($type) {
            case 'add':
                return $this->addVan($request);
                break;
            case 'list':
                return $this->listVan($request);
                break;

            case 'list_all':
                return $this->listAllVans($request);

            case 'list_products':
                return $this->listAllVanProducts($request->keyword);
                break;
            case 'update':
                return $this->updateVan($request);
                break;

            case 'activate':
                return $this->activateVan($request);
                break;

            case 'search':
                return $this->searchVan($request);
                break;

             case 'list_return_product':
                return $this->listAllVanProductsReturn($request);
                break;

            default:
                return response()->json(['error' => 'Invalid Endpoint', 'status' => $this->badrequest_stat]);
                break;
        }
    }

    /**
     * @api {post} http://127.0.0.1:8000/api/godown/add  Add Godown
     * @apiName addGodown
     * @apiGroup Godown
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {String} gd_name Godown Name (Required)
     * @apiParam {String} gd_code Godown Code
     * @apiParam {String} gd_cntct_num Godown contact Number
     * @apiParam {String} gd_cntct_person Godown contact person
     * @apiParam {String} gd_address Godown address
     * @apiParam {String} gd_desc Godown Description
     *
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} message Success Message
     *
     * @apiError {String} error Error Message.
     *
     */

    public function addVan($request)
    {
        $data = $request->all();
        $data['van_code2'] = isset($data['van_code'])?$data['van_code']:0;
        $validator = Validator::make(
            $data,
            [
                'van_name' => 'required|unique:van',
                'van_code' => 'required|unique:van',
                'van_code2' => 'unique:acc_branches,branch_code',
                'van_vanline_id' => 'required',
               // 'usrIds' => 'required',
               // 'van_reg_no' => 'required',
               // 'van_contact_person' => 'required',
               // 'van_contact_phone' => 'required',
               // 'van_contact_mob' => 'required',
                
                'van_ledger_id' => 'required',
                'van_password' => 'required',
                'van_type' => 'required',
               'usr_ids' => 'array|min:1',

            ],
            [
                'van_name.required' => 'Required',
                'van_name.unique' => 'Already Exists',
                'van_code.unique' => 'Van Code Already Exists',
                'van_code.required' => 'Required',
                'van_vanline_id.required' => 'Required',
                'usrIds.required' => 'Required',
                'van_reg_no.required' => 'Required',
                'van_contact_person.required' => 'Required',
                'van_contact_phone.required' => 'Required',
                'van_contact_mob.required' => 'Required',
                'van_password.required' => 'Required',
                'van_type.required' => 'Required',
                'van_ledger_id.required' => 'Required',
                'usr_ids.min' => 'Select Atleast One User',

            ]
        );

        if ($validator->fails()) {
            if(!empty($validator->errors()->get('van_code2'))) {
                $validator->errors()->add("van_code", 'Same Branch Code Exists');
            }
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }

        $Van = $request->all();
        $Van['van_reg_no'] = isset($Van['van_reg_no']) ? $Van['van_reg_no'] : '';
        $Van['van_contact_person'] = isset($Van['van_contact_person']) ? $Van['van_contact_person'] : '';
        $Van['van_contact_mob'] = isset($Van['van_contact_mob']) ? $Van['van_contact_mob'] : '';
        $Van['van_contact_email'] = isset($Van['van_contact_email']) ? $Van['van_contact_email'] : '';
        $Van['van_op_bal'] =isset($Van['van_op_bal']) ? $Van['van_op_bal'] : 0;
        if($Van['all_cat']){
            $Van['all_cat'] = 1;
        }else{
            $Van['all_cat'] = 0;
        }

        $data = Van::create($Van);
if(isset($Van['van_cat'])){
        if(!$Van['all_cat']){

        $vancat['vc_van_id'] = $data->van_id;
        $vancat['branch_id'] = $this->branch_id;
        $vancat['vc_added_by'] = $this->usr_id;
        if(count($Van['van_cat']) > 0){
            foreach ($Van['van_cat'] as $key => $val) {
                $vancat['vc_cat_id'] = $val;
                VanCategories::create($vancat);
                }
            }
        }
    }


    // $data = $request->all();
    $info = array();
    $info['ledger_name'] = 'Cash@' . $Van['van_code'];
    $info['ledger_accgrp_id'] = 3;
    $info['ledger_acc_type'] = 0;
    $info['ledger_return'] = 0;
    // $info['ledger_address'] = $Van['van_contact_address'];
    $info['ledger_address'] = '';
    $info['ledger_notes'] = '';
    $info['ledger_edit'] = 0;
    $info['ledger_flags'] = 1;
    $info['opening_balance'] = $Van['van_op_bal'];
  
    $info['ledger_branch_id'] = $this->branch_id;
    $info['ledger_balance_privacy'] = 0;
    $info['ledger_due'] = 0;
    $info['ledger_tin'] = '';
    $info['ledger_alias'] = 'Cash@' . $Van['van_code'];
    $info['ledger_code'] = '';
    $info['ledger_email'] = '';
    $info['ledger_mobile'] = '';
    $addInfo = array();

    $addInfo['branch_id'] = $this->branch_id;
    $addInfo["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);

    $info = array_merge($info, $addInfo);

    $res = Acc_ledger::updateOrCreate(
        [
            'ledger_name' => 'Cash@' . $Van['van_code'],
            'branch_id' => $request->branch_id, 
        ],
        $info
    );
    // $res = Acc_ledger::create($info);
    // $res->ledger_id;
  
    $where = array();
    $where[] = ['branch_id','=', $this->branch_id];
    $branch = Acc_branch::where($where)->first();
    $info = array();
    $max = Acc_voucher::max('vch_no');
    $info['vch_no'] = (empty($max)) ? 1 : $max+1;
    $info['vch_ledger_from'] = $res->ledger_id;
    $info['vch_ledger_to'] = $res->ledger_id;
    $info['vch_date'] = date('Y-m-d',(strtotime ( '-1 day' , strtotime ( $branch['branch_open_date']))));
    $info['vch_in'] = ($request->van_op_bal >= 0) ? abs($request->van_op_bal) : 0;
    $info['vch_out'] = ($request->van_op_bal < 0) ? abs($request->van_op_bal) : 0;
    $info['vch_vchtype_id'] = 14;
    $info['vch_notes'] = 'Starting balance';
    $info['vch_added_by'] = $this->usr_id;
    $info['vch_id2'] = 0;
    $info['vch_from_group'] = 3;
    $info['vch_flags'] = 1;
    $info['ref_no'] = 0;
    $info['branch_id'] = $this->branch_id;
    $info['is_web'] = 1;
    $info['godown_id'] = 0;
    $info['van_id'] = 0;
    $info['branch_ref_no'] = 0;
    $info['sub_ref_no'] = 0;
    $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
    $voucher = Acc_voucher::updateOrCreate(
        [
                'vch_ledger_from' => $res->ledger_id, 
                'vch_ledger_to' => $res->ledger_id,
                'vch_vchtype_id' => 14,
                'branch_id' => $this->branch_id, 
                'van_id' => 0 
        ],
        $info
    );
    Acc_voucher::where(['vch_in'=>0,'vch_out'=>0])->delete();
    $where = array();
    $where[] = ['van_id','=', $data->van_id];
    Van::where($where)->update(['van_cash_ledger_id' => $res->ledger_id, 'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)]);


    
        if($data->van_id){
            $addInfo = array();
            $addInfo["server_sync_time"] = $request->server_sync_time;
            $addInfo["branch_id"] = $request->branch_id;
            $info = array();
            $info = array_merge($info, $addInfo);
            $info["van_id"] = $data->van_id;
            foreach ($request->usr_ids as $val) {
                $info["user_id"] = $val;
                $data = VanUsers::create($info);
            }
            return parent::displayMessage('Saved Successfully');
        } else {
            return response()->json(['message' => 'Error Adding Van', 'status' => $this->badrequest_stat]);
        }
       
    }

    /**
     * @api {post} http://127.0.0.1:8000/api/godown/edit  Edit Godown
     * @apiName editGodown
     * @apiGroup Godown
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {BigInt} gd_id Godown Id (Required)
     * @apiParam {String} gd_name Godown Name (Required)
     * @apiParam {String} gd_code Godown Code
     * @apiParam {String} gd_cntct_num Godown contact Number
     * @apiParam {String} gd_cntct_person Godown contact person
     * @apiParam {String} gd_address Godown address
     * @apiParam {String} gd_desc Godown Description
     *
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} message Success Message
     *
     * @apiError {String} error Error Message.
     *
     */
    public function listVan()
    {

        $rltn = [

            'ledger' => function ($qr) {
                $qr->select('ledger_id', 'ledger_name');
            },
            'vanlines' => function ($qr) {
                $qr->select('vanline_id', 'vanline_name');
            },
            'vanusers' => function ($qr) {
                $qr->select('usr_username','usr_name','usr_id')->where('branch_id',$this->branch_id)->where('van_user_flag', 1);
            },
            'vanCatgory' => function ($qr) {
                $qr->select('vc_van_id','vc_cat_id','vc_id')->where('branch_id',$this->branch_id)->where('vc_cat_flag', 1);
            },
        ];

        return parent::displayData(Van::where('branch_id',$this->branch_id)->with($rltn)->paginate($this->per_page));
    }

    public function listAllVans()
    {
        return parent::displayData(Van::where('branch_id',$this->branch_id)->get());
    }

    public function listAllVanProducts()
    {

        $this->van_id = Input::get('vanid');

        $qty_sum = \DB::raw('sum(vantransub_qty)as total_stock_count');
        $data = Product::select('branch_stocks.bs_stock_quantity_van as total_stock', 'prd_id', 'prd_name', 'prd_barcode', 'prd_alias', 'prd_base_unit_id', 'van_transfer_sub.*', $qty_sum)
        //  ->where('prd_name', 'like', $rq->keyword . '%')
            ->Join('van_transfer_sub', function ($join) {
                $join->on('products.prd_id', 'van_transfer_sub.vantransub_prod_id')
                    ->where('van_transfer_sub.branch_id', $this->branch_id)
                    ->where('van_transfer_sub.vantransub_van_id', $this->van_id)
                    ->where('van_transfer_sub.vantransub_flags', 1);
            })->Join('branch_stocks', function ($join) {
            $join->on('van_transfer_sub.vantransub_branch_stock_id', 'branch_stocks.branch_stock_id');
        })->with(array('base_unit' => function ($qr) {
            $qr->select('unit_id','unit_base_qty' ,'unit_code','unit_name', 'unit_display');
        }))->groupBy('vantransub_prod_id')->get()->toArray();

        foreach ($data as $k => $v) {
           
            $da = $this->findVanAvgRate($v);
            $rate = isset($da[0]['vanstock_last_tran_rate'])?$da[0]['vanstock_last_tran_rate']:0;
            $avgrate = isset($da[0]['vanstock_avg_tran_rate'])?$da[0]['vanstock_avg_tran_rate']:0;
            $data[$k]['vantransub_purch_rate'] = $rate ? $rate : 0;
            $data[$k]['vantransub_avg_purch_rate'] = $avgrate ? round($avgrate) : 0;
            $units = ProdUnit::select('prod_units.produnit_unit_id as sur_unit_id', 'units.unit_base_qty', 'units.unit_name', 'units.unit_code')->join('units', 'units.unit_id', 'prod_units.produnit_unit_id')->where('prod_units.produnit_prod_id', $v['prd_id'])->get();
            $data[$k]['prd_units'] = $units;
            $data[$k]['base_unit']['sur_unit_id'] = $data[$k]['base_unit']['unit_id'];
        }
        return parent::displayData($data);
    }

    public function findVanAvgRate($stock)
    {
        $whr = ['vanstock_prod_id' => $stock['vantransub_prod_id'],
            'vanstock_stock_id' => $stock['vantransub_stock_id'],
            'vanstock_branch_stock_id' => $stock['vantransub_branch_stock_id'],
            'vanstock_van_id' => $stock['vantransub_van_id']];

        $vanStk = VanStock::where($whr)->get()->toArray();
        return $vanStk;
 //   print_r( $vanStk[0] );die($vanStk[0]['vanstock_avg_tran_rate']."==");
        //return [$vanStk[0]['vanstock_last_tran_rate'], $vanStk[0]['vanstock_avg_tran_rate']];

    }

    public function searchVan($request)
    {

        $van_name = isset($request['van_name'])?$request['van_name']:0;
        if ($van_name) {
            $van = Van::where('branch_id',$this->branch_id)->where('van_name', 'like',   $van_name . '%')->limit(10)->get();
            return response()->json(['data' => $van, 'status' => $this->success_stat]);
        } else {
            $data = Van::where('branch_id',$this->branch_id)->take(20)->get();
            return response()->json(['data' => $data, 'status' => $this->success_stat]);
        }
        return parent::displayData(Van::where('van_name', 'like', $request['van_name'] . '%')->where('branch_id',$this->branch_id)->limit(10)->get());
    }
    
    
    public function updateVan($request)
    {
        $data = $request->all();
        $data['van_code2'] = $data['van_code'];
        $validator = Validator::make(
            $data,
            [
                'van_id' => 'required',
                'van_name' => 'required',
                'editUsrIds' => 'array|min:1',
                // 'van_password' => 'required',
                'van_code' => 'required|unique:van,van_code,' . $request->van_id . ',van_id',
                'van_code2' => 'unique:acc_branches,branch_code',
                'van_type' => 'required',
                'van_ledger_id' => 'required',
                'van_vanline_id' => 'required',
            ],
            [
                'van_name.required' => 'Required',
                'van_name.unique' => 'Already Exists',
                'editUsrIds.min' => 'Select Atleast One User',            
                'van_code.required' => 'Required',
                'van_code.unique' => 'Van code already exists',
                'van_vanline_id.required' => 'Required',
                'usrIds.required' => 'Required',
                'van_reg_no.required' => 'Required',
                'van_contact_person.required' => 'Required',
                'van_contact_phone.required' => 'Required',
                'van_contact_mob.required' => 'Required',
                // 'van_password.required' => 'Required',
                'van_type.required' => 'Required',
                'van_ledger_id.required' => 'Required',
                'usr_ids.min' => 'Select Atleast One User',
            ]
        );
        if ($validator->fails()) {
            if(!empty($validator->errors()->get('van_code2'))) {
                $validator->errors()->add("van_code", 'Same Branch Code Exists');
            }
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }

        $Van = $request->all();

        if(empty($Van['van_password']))
            unset($Van['van_password']);
           
            
        $db = Van::find($request->van_id);
        $db->fill($Van);
        $db->save();

        if(isset($Van['van_cat'])){
            if(!$Van['all_cat']){
    $oldCats = VanCategories::where('vc_van_id', $request->van_id)->where('branch_id', $request['branch_id'])->get()->pluck('vc_cat_id')->toArray();
    sort($oldCats); 
    sort($Van['van_cat']);         
    
    // print_r('old');
    // print_r($oldCats);
    // print_r('new');
    // print_r($Van['van_cat']);die('---');
    if ($oldCats != $Van['van_cat']){
            $vancat['vc_van_id'] = $request->van_id;
            $vancat['branch_id'] = $this->branch_id;
            $vancat['vc_added_by'] = $this->usr_id;
            $vancat['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);

            VanCategories::where('vc_van_id', $request->van_id)->where('branch_id', $request['branch_id'])->delete();

            if(count($Van['van_cat']) > 0){
                foreach ($Van['van_cat'] as $key => $val) {
                    $vancat['vc_cat_id'] = $val;
                    VanCategories::create($vancat);
                    }
                }

            }

            }
        }



        // to get 
        $addInfo = array();

        $addInfo["server_sync_time"] = $request->server_sync_time;
        $addInfo["branch_id"] = $request->branch_id;
        
        VanUsers::where('van_id', $request->van_id)->delete();

        $info = array();
        $info = array_merge($info, $addInfo);
        $info["van_id"] = $request->van_id;  
        foreach ($request->editUsrIds as $userId) {
            $info["user_id"] = $userId;
            $data = VanUsers::create($info);
        } 


        $info = array();
        $info['ledger_name'] = 'Cash@' . $Van['van_code'];
        $info['ledger_accgrp_id'] = 3;
        $info['ledger_acc_type'] = 0;
        $info['ledger_return'] = 0;
        // $info['ledger_address'] = $Van['van_contact_address'];
        $info['ledger_address'] = '';
        $info['ledger_notes'] = '';
        $info['ledger_edit'] = 0;
        $info['ledger_flags'] = 1;
        $info['opening_balance'] = $Van['van_op_bal'];
    
        $info['ledger_branch_id'] = $this->branch_id;
        $info['ledger_balance_privacy'] = 0;
        $info['ledger_due'] = 0;
        $info['ledger_tin'] = '';
        $info['ledger_alias'] = 'Cash@' . $Van['van_code'];
        $info['ledger_code'] = '';
        $info['ledger_email'] = '';
        $info['ledger_mobile'] = '';
        $addInfo = array();

        $addInfo['branch_id'] = $this->branch_id;
        $addInfo["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);

        $info = array_merge($info, $addInfo);
        $condtion =  [
            'branch_id' => $request->branch_id, 
        ];
        if($db->van_cash_ledger_id != 0) {
            $condtion['ledger_id'] = $db->van_cash_ledger_id;
        } else {
            $condtion['ledger_name'] = 'Cash@' . $Van['van_code'];
        }

        $res = Acc_ledger::updateOrCreate($condtion ,$info);
  
        $where = array();
        $where[] = ['branch_id','=', $this->branch_id];
        $branch = Acc_branch::where($where)->first();
        $info = array();
       
        $info['vch_ledger_from'] = $res->ledger_id;
        $info['vch_ledger_to'] = $res->ledger_id;
        $info['vch_date'] = date('Y-m-d',(strtotime ( '-1 day' , strtotime ( $branch['branch_open_date']))));
        $info['vch_in'] = ($request->van_op_bal >= 0) ? abs($request->van_op_bal) : 0;
        $info['vch_out'] = ($request->van_op_bal < 0) ? abs($request->van_op_bal) : 0;
        $info['vch_vchtype_id'] = 14;
        $info['vch_notes'] = 'Starting balance';
        $info['vch_added_by'] = $this->usr_id;
        $info['vch_id2'] = 0;
        $info['vch_from_group'] = 3;
        $info['vch_flags'] = 1;
        $info['ref_no'] = 0;
        $info['branch_id'] = $this->branch_id;
        $info['is_web'] = 1;
        $info['godown_id'] = 0;
        $info['van_id'] = 0;
        $info['branch_ref_no'] = 0;
        $info['sub_ref_no'] = 0;
        $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);

        if($db->van_cash_ledger_id == 0) {
            $max = Acc_voucher::max('vch_no');
            $info['vch_no'] = (empty($max)) ? 1 : $max+1;

            $where = array();
            $where[] = ['van_id','=', $request->van_id];
            Van::where($where)->update(['van_cash_ledger_id' => $res->ledger_id, 'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)]);
        }

        $voucher = Acc_voucher::updateOrCreate(
            [
                    'vch_ledger_from' => $res->ledger_id, 
                    'vch_ledger_to' => $res->ledger_id,
                    'vch_vchtype_id' => 14,
                    'branch_id' => $this->branch_id, 
                    'van_id' => 0 
            ],
            $info
        );
        Acc_voucher::where(['vch_in'=>0,'vch_out'=>0])->delete();
       
        // $removeUsers = array_diff($request->oldUsrIds, $request->editUsrIds);
        // $newUsers = array_diff($request->editUsrIds, $request->oldUsrIds);
     
        // if(!empty($removeUsers)){
        //     $info = array();
        //     $info = array_merge($info, $addInfo);
        //     $info["van_id"] = $request->van_id;
        //     foreach($removeUsers as $userId){
        //         $info["van_user_flag"] = 0;
        //         $db1 = VanUsers::where([['van_id', '=', $request->van_id ], ['user_id', '=', $userId]])
        //         ->orderBy('id','desc')
        //         ->take(1)
        //         ->update($info);
        //     }
        // }
        // if(!empty($newUsers)){
        //     $info = array();
        //     $info = array_merge($info, $addInfo);
        //     $info["van_id"] = $request->van_id;  
        //     foreach ($newUsers as $userId) {
        //         $info["user_id"] = $userId;
        //         $data = VanUsers::create($info);
        //     } 
        // } 
        return response()->json(['message' => 'Updated Successfully', 'status' => $this->created_stat]);
    }
    public function activateVan(Request $request)
    {

        $db = Van::find($request->van_id);
        $active = $db['van_flag'];
        $db['van_flag'] = ($active) ? 0 : 1;
        $db['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
        $db->save();

        return parent::displayMessage(($active) ? 'Inactivated' : 'Activated');
    }

    /**
     * @api {post} http://127.0.0.1:8000/api/search_godown  Search Godown
     * @apiName searchGodown
     * @apiVersion 0.1.0
     * @apiGroup Godown
     *
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {varchar} gd_name Godown Name
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} data Godowns

     */

    public function searchGodown(Request $request)
    {
        if ($request['gd_name'] != "") {
            $gdData = GodownMaster::where('branch_id', $branch_id)->where('gd_name', 'like', '%' . $request['gd_name'] . '%')->where('branch_id', $request->branch_id)->take(50)->get();

            return response()->json(['data' => $gdData, 'status' => $this->success_stat]);
        } else {

            $data = GodownMaster::where('branch_id', $branch_id)->where('branch_id', $request->branch_id)->take(50)->get();
            return response()->json(['data' => $data, 'status' => $this->success_stat]);
        }
    }

    /**
     * @api {post} http://127.0.0.1:8000/api/get_godowns Get Godowns
     * @apiName getGodownByBranch
     * @apiVersion 0.1.0
     * @apiGroup Godown
     *
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} data Godowns by branch

     */

    public function getGodownByBranch()
    {

        $user = auth()->user();
        $branch_id = ($user != null ? $user->branch_id : 0);

        $gdData = GodownMaster::where('branch_id', $branch_id)->get();

        return parent::displayData($gdData, $this->success_stat);
    }

    public function getAllGodowns(Request $rq)
    {
        $gdData = [];
        if ($gd_name = isset($rq['gd_name']) ? $rq['gd_name'] : 0) {
            $gdData = GodownMaster::select('gd_id', 'gd_name', 'gd_code')->where('gd_name', 'like', '%' . $gd_name . '%')
                ->where('branch_id', $rq->branch_id)->take(10)->get();
        } else {
            $gdData = GodownMaster::select('gd_id', 'gd_name', 'gd_code')->where('branch_id', $rq->branch_id)->take(50)->get();
        }
        $gdData[] = ['gd_id' => 0,
            'gd_name' => 'Shop',
            'gd_code' => 'SP'];
        return parent::displayData($gdData, $this->success_stat);
    }

    // ===================== End of Code Added By Rakesh =======================

     public function listAllVanProductsReturn(Request $rq)
    {
        $product_name = Product::where('prd_barcode',$rq->brcode)->pluck('products.prd_name'); 
       // echo count($product_name);
       // exit;
        if (count($product_name)==0){
         $product_name = ProdUnit::where('produnit_ean_barcode',$rq->brcode)->join('products', 'prod_units.produnit_prod_id', '=', 'products.prd_id')->pluck('products.prd_name');
         } 
                
        if (count($product_name)>0){
        $this->van_id = $rq->van_id;
        $qty_sum = \DB::raw('sum(vantransub_qty)as total_stock_count');
        $data = Product::select('branch_stocks.bs_stock_quantity_van as total_stock', 'prd_id', 'prd_name', 'prd_barcode', 'prd_alias', 'prd_base_unit_id', 'van_transfer_sub.*', $qty_sum)
        //  ->where('prd_name', 'like', $rq->keyword . '%')
            ->Join('van_transfer_sub', function ($join) {
                $join->on('products.prd_id', 'van_transfer_sub.vantransub_prod_id')
                    ->where('van_transfer_sub.branch_id', $this->branch_id)
                    ->where('van_transfer_sub.vantransub_van_id', $this->van_id)
                    ->where('van_transfer_sub.vantransub_flags', 1);
            })->Join('branch_stocks', function ($join){
            $join->on('van_transfer_sub.vantransub_branch_stock_id', 'branch_stocks.branch_stock_id');
        })->with(array('base_unit' => function ($qr) {
            $qr->select('unit_id','unit_base_qty' ,'unit_code','unit_name', 'unit_display');
        }))->groupBy('vantransub_prod_id')->where('prd_name', '=',$product_name[0])->get()->toArray();

        foreach ($data as $k => $v) {
           
            $da = $this->findVanAvgRate($v);
            $rate = isset($da[0]['vanstock_last_tran_rate'])?$da[0]['vanstock_last_tran_rate']:0;
            $avgrate = isset($da[0]['vanstock_avg_tran_rate'])?$da[0]['vanstock_avg_tran_rate']:0;
            $data[$k]['vantransub_purch_rate'] = $rate ? $rate : 0;
            $data[$k]['vantransub_avg_purch_rate'] = $avgrate ? round($avgrate) : 0;
            $units = ProdUnit::select('prod_units.produnit_unit_id as sur_unit_id', 'units.unit_base_qty', 'units.unit_name', 'units.unit_code')->join('units', 'units.unit_id', 'prod_units.produnit_unit_id')->where('prod_units.produnit_prod_id', $v['prd_id'])->get();
            $data[$k]['prd_units'] = $units;
            $data[$k]['base_unit']['sur_unit_id'] = $data[$k]['base_unit']['unit_id'];
        }
    }else{
    $data = [];    
    }
    // end count product_name
        return parent::displayData($data);
    }

}
