<?php

namespace App\Http\Controllers\Api\Van;

use App\Http\Controllers\Api\ApiParent;
use App\Models\Godown\GodownStock;
use App\Models\Godown\GodownStockLog;
use App\Models\Product\Product;
use App\Models\Product\ProdUnit;
use App\Models\Sales\SalesReturnSub;
use App\Models\Sales\SalesSub;
use App\Models\Stocks\BranchStocks;
use App\Models\Stocks\StockUnitRates;
use App\Models\Van\Van;
use App\Models\Van\VanDailyStocks;
use App\Models\Van\VanStock;
use App\Models\Van\VanTransfer;
use App\Models\Van\VanTransferReturn;
use App\Models\Van\VanTransferReturnSub;
use App\Models\Van\VanTransferSub;
use App\Models\BranchTransfer\BranchTransferSub;
use DB;
use Illuminate\Http\Request;
use Validator;

class VanTransferController extends ApiParent
{

    // ===================== Code Added By Deepu =======================
    public function getNxtVanTranId()
    {
        $tran_id = VanTransfer::get()->count() + 1;
        return parent::displayData(['tran_id' => $tran_id]);
    }

    public function getNxtVanRetTranId()
    {
        $tran_id = VanTransferReturn::get()->count() + 1;
        return parent::displayData(['tran_id' => $tran_id]);
    }

    public function serachProduct(Request $rq)
    {

        $results = [];
        if ($rq->keyword || $rq->brcode) {
            $this->key = $rq->keyword;
            $this->barcode = $rq->brcode;
            $results = Product::select('prd_id', 'prd_barcode', 'prd_name', 'prd_alias', 'prd_base_unit_id', 'branch_stock_id', 'bs_stock_id', 'bs_prate', 'bs_avg_prate', 'bs_srate', 'bs_stock_quantity', 'bs_stock_quantity_shop', 'bs_stock_quantity_gd', 'bs_stock_quantity_van')

                ->Join('branch_stocks', function ($join) {
                    $join->on('products.prd_id', 'branch_stocks.bs_prd_id')
                        ->where('bs_branch_id', $this->branch_id)
                        ->where('prd_stock_status', 1);
                    if ($this->key) {
                        $join->where('prd_name', 'like', '%' . $this->key . '%');
                    } else {
                        $join->where('prd_barcode', $this->barcode);
                    }

                })->groupBy('products.prd_id')->take(40)->get();
            if (count($results) == 0) {
                $prdData = ProdUnit::where('produnit_ean_barcode', $this->barcode)->join('products', 'prod_units.produnit_prod_id', '=', 'products.prd_id')->first();
                $eanunit = isset($prdData->produnit_unit_id) ? $prdData->produnit_unit_id : 0;
                $product_name = isset($prdData->prd_name) ? $prdData->prd_name : 0;
                $this->prd_id = isset($prdData->prd_id) ? $prdData->prd_id : 0;
                if ($product_name) {
                    //  $eanunit = ProdUnit::where('produnit_ean_barcode', $this->barcode)->join('products', 'prod_units.produnit_prod_id', '=', 'products.prd_id')->pluck('prod_units.produnit_unit_id');
                    $results = Product::select('prd_id', 'prd_barcode', 'prd_name', 'prd_alias', 'prd_base_unit_id', 'branch_stock_id', 'bs_stock_id', 'bs_prate', 'bs_avg_prate', 'bs_srate', 'bs_stock_quantity', 'bs_stock_quantity_shop', 'bs_stock_quantity_gd', 'bs_stock_quantity_van')

                        ->Join('branch_stocks', function ($join) {
                            $join->on('products.prd_id', 'branch_stocks.bs_prd_id')
                                ->where('bs_branch_id', $this->branch_id)
                                ->where('prd_stock_status', 1);
                            $join->where('prd_id', $this->prd_id);

                        })->groupBy('products.prd_id')->take(10)->get();
                }
                // return $results;
                if (isset($results['0'])) {
                    $results['0']->eanunit = $eanunit;
                }

            }
            //exit;
            foreach ($results as $k => $row) {

                //return $row['eanunit'];
                $results[$k]['last_rate'] = $this->getLastTransferRate($row['prd_id'], $row['prd_base_unit_id'], $rq->branch_received,1);
                $units = ProdUnit::select('prod_units.produnit_unit_id as sur_unit_id', 'units.unit_base_qty',
                    'units.unit_name', 'units.unit_code')->join('units', 'units.unit_id', 'prod_units.produnit_unit_id')
                    ->where('prod_units.produnit_prod_id', $row['prd_id'])->get();

                $slctqr['branch_stock_id'] = $row['branch_stock_id'];
                $slctqr['sur_prd_id'] = $row['prd_id'];

                foreach ($units as $d => $value) {

                    $slctqr['sur_unit_id'] = $value['sur_unit_id'];
                    $value = StockUnitRates::where($slctqr)->first();
                    $units[$d]['sale_rate'] = isset($value['sur_unit_rate']) ? $value['sur_unit_rate'] : ($row['bs_srate'] * $units[$d]['unit_base_qty']);
                    $units[$d]['purch_rate'] = $row['bs_prate'] * $units[$d]['unit_base_qty'];

                    $units[$d]['last_rate'] = $this->getLastTransferRate($row['prd_id'], $value['sur_unit_id'], $rq->branch_received,$units[$d]['unit_base_qty']);
                }
                $results[$k]['prd_units'] = $units;
                $results[$k]['gdstock'] = GodownStock::select('godown_stocks.gs_godown_id as gd_id', 'godown_stocks.gs_qty', 'godown_master.gd_name')->join('godown_master', 'godown_master.gd_id', 'godown_stocks.gs_godown_id')->where('godown_stocks.gs_branch_stock_id', $row['branch_stock_id'])->get();
                foreach ($units as $k2 => $val) {
                    if ($val['unit_base_qty'] == 1) {
                        $base_unit_name = $val['unit_name'];
                        $base_unit_code = $val['unit_code'];
                    }
                }
                $results[$k]['base_unit_name'] = $base_unit_name;
                $results[$k]['base_unit_code'] = $base_unit_code;

                if (isset($row['eanunit'])) {
                    foreach ($units as $k2 => $uval) {
                        if ($uval['sur_unit_id'] == $row['eanunit']) {
                            $uval['base_unit_name'] = $uval['unit_name'];
                            $uval['base_unit_code'] = $uval['unit_code'];

                            $eanunit = $uval;

                        }
                    }
                    $results[$k]['eanunit'] = $eanunit;
                } else {
                    $results[$k]['eanunit'] = 0;
                }
            }
        }
        return parent::displayData($results);
    }

    public function getLastTransferRate($prd_id, $unit_id, $branch_id,$baseqty)
    {

        if ($branch_id) {

            $whr['stocktrsub_prd_id'] = $prd_id;
            $whr['stocktrsub_to'] = $branch_id;
            $whr['stocktrsub_unit_id'] = $unit_id;
            $whr['branch_id'] = $this->branch_id;
            $whr['stocktrsub_flags'] = 1;
            $tran = BranchTransferSub::where($whr)->latest('stocktrsub_num')->first();

            return isset($tran->stocktrsub_rate)?$tran->stocktrsub_rate*$baseqty:0;
        }
        return 0;

    }

    public function addStocktoVan(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            [
                'vantran_van_id' => 'required',
                'vantran_price' => 'required',
                'vantran_purch_price' => 'required',
                'vantran_date' => 'required',

            ],
            [
                'vantran_van_id.required' => 'Required',
                'vantran_van_id.exist' => 'Invalid Van',
                'vantran_purch_price.required' => 'Required',

                'vantran_date.required' => 'Required',
                'vantran_price.required' => 'Required',

            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }

        $van_transfer = $request->all();

        $van_transfer['vantran_added_by'] = $this->usr_id;
        $van_transfer['branch_id'] = $this->branch_id;
        $van_transfer['server_sync_time'] = $this->server_sync_time;
        $van_transfer['vantran_date'] = date('Y-m-d', strtotime($van_transfer['vantran_date']));
        $last_insert = VanTransfer::create($van_transfer);


      
        foreach ($van_transfer['items'] as $key => $van_transfer_sub) {
            $van_transfer_sub['vantransub_vantran_id'] = $last_insert->vantran_id;
            $van_transfer_sub['vantransub_van_id'] = $van_transfer['vantran_van_id'];
            $van_transfer_sub['vantransub_date'] = $van_transfer['vantran_date'];
            $van_transfer_sub['vantransub_godown_id'] = $van_transfer['vantran_godown_id'];
            $van_transfer_sub['vantransub_added_by'] = $this->usr_id;
            $van_transfer_sub['branch_id'] = $this->branch_id;
            $van_transfer_sub['server_sync_time'] = $this->server_sync_time;
            VanTransferSub::create($van_transfer_sub);

            $this->updateVanStocks($van_transfer_sub);

            if ($van_transfer_sub['vantransub_godown_id'] > 0) {
                $this->GodownToVanStockUpdations($van_transfer_sub);
            }
        }

        return response()->json(['message' => 'Saved Successfully', 'status' => $this->created_stat]);
    }

    public function GodownToVanStockUpdations($stock)
    {

        $whr = ['gs_godown_id' => $stock['vantransub_godown_id'],
            'gs_branch_stock_id' => $stock['vantransub_branch_stock_id'],
            'gs_prd_id' => $stock['vantransub_prod_id']];

        if ($gdStk = GodownStock::where($whr)->first()) {

            $gdStk['gs_qty'] += $stock['vantransub_qty'];
            $gdStk['server_sync_time'] = $this->server_sync_time;
            $gdStk->save();

        } else {

            $gdStock = [
                'gs_godown_id' => $stock['vantransub_godown_id'],
                'gs_branch_stock_id' => $stock['vantransub_branch_stock_id'],
                'gs_stock_id' => $stock['vantransub_stock_id'],
                'gs_prd_id' => $stock['vantransub_prod_id'],
                'gs_qty' => $stock['vantransub_qty'],
                'gs_date' => date('Y-m-d'),
                'gs_flag' => 1,
                'branch_id' => $this->branch_id, 'server_sync_time' => $this->server_sync_time,
            ];
            $gdStk = GodownStock::create($gdStock);
        }

        $gdLog['gsl_gdwn_stock_id'] = $gdStk->gs_id;
        $gdLog['gsl_branch_stock_id'] = $stock['vantransub_branch_stock_id'];
        $gdLog['gsl_stock_id'] = $stock['vantransub_stock_id'];
        $gdLog['gsl_prd_id'] = $stock['vantransub_prod_id'];
        $gdLog['gsl_prod_unit'] = $stock['vantransub_unit_id'];
        $gdLog['gsl_qty'] = $stock['vantransub_qty'];
        $gdLog['gsl_from'] = $stock['vantransub_godown_id'];
        $gdLog['gsl_to'] = 0;
        $gdLog['gsl_tran_vanid'] = $stock['vantransub_van_id'];
        $gdLog['gsl_vchr_type'] = 26;
        $gdLog['gsl_date'] = date('Y-m-d');
        $gdLog['gsl_added_by'] = $this->usr_id;
        $gdLog['branch_id'] = $this->branch_id;
        $gdLog['server_sync_time'] = $this->server_sync_time;

        GodownStockLog::create($gdLog);

    }

    public function updateVanStocks($stock)
    {

        $whr = ['vanstock_prod_id' => $stock['vantransub_prod_id'],
            'vanstock_stock_id' => $stock['vantransub_stock_id'],
            'vanstock_branch_stock_id' => $stock['vantransub_branch_stock_id'],
            'vanstock_van_id' => $stock['vantransub_van_id']];

        $vanStk = VanStock::where($whr)->first();

        if (!empty($vanStk)) {

            $total_amount = ($vanStk['vanstock_qty'] * $vanStk['vanstock_avg_tran_rate']) + ($stock['vantransub_qty'] * $stock['vantransub_purch_rate']);
            $avg_rate = $total_amount / ($stock['vantransub_qty'] + $vanStk['vanstock_qty']);

            $vanStk['vanstock_qty'] = $vanStk['vanstock_qty'] + $stock['vantransub_qty'];
            $vanStk['vanstock_last_tran_rate'] = $stock['vantransub_purch_rate'];
            $vanStk['vanstock_avg_tran_rate'] = $avg_rate;
            $vanStk['server_sync_time'] = $this->server_sync_time;

            $vanStk->save();
        } else {
            $insert = ['vanstock_qty' => $stock['vantransub_qty'],
                'vanstock_last_tran_rate' => $stock['vantransub_purch_rate'],
                'vanstock_avg_tran_rate' => $stock['vantransub_purch_rate'],
                'branch_id' => $this->branch_id, 'server_sync_time' => $this->server_sync_time];

            VanStock::create(array_merge($whr, $insert));
        }
        $change = $stock['vantransub_qty'];
        $column = ($stock['vantransub_godown_id'] > 0) ? "bs_stock_quantity_gd" : "bs_stock_quantity_shop";
        BranchStocks::where('branch_stock_id', $stock['vantransub_branch_stock_id'])
            ->update([
                'bs_stock_quantity_van' => \DB::raw("bs_stock_quantity_van +$change"),
                $column => \DB::raw("$column -$change"),
                'server_sync_time' => $this->server_sync_time,
            ]);

        $this->updateInDailyVanStocks($stock);
        return true;
    }

    public function updateInDailyVanStocks($stock, $deduct = false)
    {

        $whr = ['vds_prd_id' => $stock['vantransub_prod_id'],
            'vds_stock_id' => $stock['vantransub_stock_id'],
            'vds_branch_stock_id' => $stock['vantransub_branch_stock_id'],
            'vds_van_id' => $stock['vantransub_van_id'],
            'vds_date' => $stock['vantransub_date'],
        ];

        $vanStk = VanDailyStocks::where($whr)->first();

        if (!empty($vanStk)) {

            $vanStk['vds_stock_quantity'] = ($deduct == 1) ? ($vanStk['vds_stock_quantity'] - $stock['vantransub_qty'])
            : ($vanStk['vds_stock_quantity'] + $stock['vantransub_qty']);
            $vanStk['server_sync_time'] = $this->server_sync_time;
            $vanStk->save();
        } else {

            $insert = ['branch_id' => $this->branch_id,
                'server_sync_time' => $this->server_sync_time,
                'vds_stock_quantity' => ($deduct == 1) ? (0 - $stock['vantransub_qty']) : $stock['vantransub_qty']];
            VanDailyStocks::create(array_merge($whr, $insert));
        }

        return true;
    }

    public function returnvanTransfer(Request $rq)
    {

        $return_transfer['vantranret_vantran_id'] = isset($rq->vantranret_vantran_id) ? $rq->vantranret_vantran_id : 0;
        $return_transfer['vantranret_date'] = isset($rq->return_date) ? date('Y-m-d', strtotime($rq->return_date)) : date('Y-m-d');
        $return_transfer['vantranret_van_id'] = isset($rq->vantran_van_id) ? $rq->vantran_van_id : 0;
        $return_transfer['vantranret_godown_id'] = isset($rq->vantran_godown_id) ? $rq->vantran_godown_id : 0;
        $return_transfer['vantranret_price'] = isset($rq->vantran_price) ? $rq->vantran_price : 0;
        $return_transfer['vantranret_purch_price'] = isset($rq->vantranret_purch_price) ? $rq->vantranret_purch_price : 0;
        $return_transfer['vantranret_added_by'] = $this->usr_id;
        $return_transfer['vantranret_flag'] = 1;
        $return_transfer['branch_id'] = $this->branch_id;
        $return_transfer['server_sync_time'] = $this->server_sync_time;
        $van_data = Van::select('van_ledger_id')->where('van_id', $rq->vantran_van_id)
            ->pluck('van_ledger_id')->toArray();

        $return_transfer['vantranret_van_ledger_id'] = isset($van_data[0]) ? $van_data[0] : 0;

        $return_transfer['vantranret_notes'] = isset($rq->vantranret_notes) ? $rq->vantranret_notes : "";
        $last_insert = VanTransferReturn::create($return_transfer);

        $items = $rq->items;

        foreach ($items as $k => $item) {

            $return_transfer_sub['vantransub_vantran_id'] = $last_insert['vantranret_vantran_id'];
            $return_transfer_sub['vantranretsub_vantranret_id'] = $last_insert['vantranret_id'];
            $return_transfer_sub['vantranretsub_van_id'] = $last_insert['vantranret_van_id'];
            $return_transfer_sub['vantranretsub_godown_id'] = $last_insert['vantranret_godown_id'];
            $return_transfer_sub['vantranretsub_rate'] = $item['vantransub_ret_rate'];
            $return_transfer_sub['vantranretsub_purch_rate'] = $item['vantransub_avg_purch_rate'];

            $return_transfer_sub['vantranretsub_qty'] = $item['vantransub_qty'];
            $return_transfer_sub['vantranretsub_prod_id'] = $item['vantransub_prod_id'];
            $return_transfer_sub['vantranretsub_stock_id'] = $item['van_branchstock_id'];
            $return_transfer_sub['vantransub_branch_stock_id'] = $item['vantransub_branch_stock_id'];
            $return_transfer_sub['vantranretsub_unit_id'] = $item['vantransub_unitid'];
            $return_transfer_sub['vantranretsub_date'] = $last_insert['vantranret_date'];
            $return_transfer_sub['vantranretsub_flags'] = 1;
            $return_transfer_sub['vantranretsub_added_by'] = $this->usr_id;
            $return_transfer_sub['branch_id'] = $this->branch_id;

            $return_transfer_sub['server_sync_time'] = $this->server_sync_time;
            VanTransferReturnSub::create($return_transfer_sub);

            $change = $item['vantransub_qty'];
            $column = ($last_insert['vantranret_godown_id'] > 0) ? "bs_stock_quantity_gd" : "bs_stock_quantity_shop";
            BranchStocks::where('branch_stock_id', $item['vantransub_branch_stock_id'])
                ->update([
                    'bs_stock_quantity_van' => \DB::raw("bs_stock_quantity_van -$change"),
                    $column => \DB::raw("$column +$change"),
                    'server_sync_time' => $this->server_sync_time,
                ]);

            $stock = ['vantransub_prod_id' => $item['vantransub_prod_id'],
                'vantransub_stock_id' => $item['van_branchstock_id'],
                'vantransub_branch_stock_id' => $item['vantransub_branch_stock_id'],
                'vantransub_ret_rate' => $item['vantransub_ret_rate'],
                'vantransub_qty' => $item['vantransub_qty'],
                'vantransub_unit_id' => $item['vantransub_unitid'],
                'vantransub_van_id' => $last_insert['vantranret_van_id'],
                'vantransub_godown_id' => $last_insert['vantranret_godown_id'],

            ];

            $this->deductStockQuantity($stock);
            $vtran_return = array_merge($stock, [
                'vantransub_qty' => $item['vantransub_qty'],
                'vantransub_date' => $last_insert['vantranret_date'],
            ]);
            $this->updateInDailyVanStocks($vtran_return, $deduct = 1);

            if ($last_insert['vantranret_godown_id'] > 0) {
                $this->vanToGodownStockUpdations($vtran_return);
            }
        }
        return response()->json(['message' => 'Saved Successfully', 'status' => $this->created_stat]);

    }

    public function updateBranchStocks($branch_stock_id, $vantransub_qty, $shop)
    {

        $bstck = BranchStocks::where('branch_stock_id', $branch_stock_id)->first();

        $bstck['bs_stock_quantity_van'] -= $vantransub_qty;

        if ($shop) {
            $bstck['bs_stock_quantity_shop'] += $vantransub_qty;
        } else {
            $bstck['bs_stock_quantity_gd'] += $vantransub_qty;
        }
        $bstck['server_sync_time'] = $this->server_sync_time;
        $bstck->save();

    }

    public function vanToGodownStockUpdations($stock)
    {

        $whr = ['gs_godown_id' => $stock['vantransub_godown_id'],
            'gs_branch_stock_id' => $stock['vantransub_branch_stock_id'],
            'gs_prd_id' => $stock['vantransub_prod_id']];

        $this->updateBranchStocks($stock['vantransub_branch_stock_id'], $stock['vantransub_qty'], 0);
        if ($gdStk = GodownStock::where($whr)->first()) {

            $gdStk['gs_qty'] -= $stock['vantransub_qty'];
            $gdStk['server_sync_time'] = $this->server_sync_time;
            $gdStk->save();

        } else {

            $gdStock = [
                'gs_godown_id' => $stock['vantransub_godown_id'],
                'gs_branch_stock_id' => $stock['vantransub_branch_stock_id'],
                'gs_stock_id' => $stock['vantransub_stock_id'],
                'gs_prd_id' => $stock['vantransub_prod_id'],
                'gs_qty' => (0 - $stock['vantransub_qty']),
                'gs_date' => date('Y-m-d'),
                'gs_flag' => 1,
                'branch_id' => $this->branch_id, 'server_sync_time' => $this->server_sync_time,
            ];

            $gdStk = GodownStock::create($gdStock);
        }
        $gdLog['gsl_gdwn_stock_id'] = $gdStk->gs_id;
        $gdLog['gsl_branch_stock_id'] = $stock['vantransub_branch_stock_id'];
        $gdLog['gsl_stock_id'] = $stock['vantransub_stock_id'];
        $gdLog['gsl_prd_id'] = $stock['vantransub_prod_id'];
        $gdLog['gsl_prod_unit'] = $stock['vantransub_unit_id'];
        $gdLog['gsl_qty'] = $stock['vantransub_qty'];
        $gdLog['gsl_from'] = 0;
        $gdLog['gsl_to'] = $stock['vantransub_godown_id'];
        $gdLog['gsl_tran_vanid'] = $stock['vantransub_van_id'];
        $gdLog['gsl_vchr_type'] = 27;
        $gdLog['gsl_date'] = date('Y-m-d');
        $gdLog['gsl_added_by'] = $this->usr_id;
        $gdLog['branch_id'] = $this->branch_id;
        $gdLog['server_sync_time'] = $this->server_sync_time;

        GodownStockLog::create($gdLog);
    }

    public function deductStockQuantity($stock)
    {
        $whr = ['vanstock_prod_id' => $stock['vantransub_prod_id'],
            'vanstock_stock_id' => $stock['vantransub_stock_id'],
            'vanstock_branch_stock_id' => $stock['vantransub_branch_stock_id'],
            'vanstock_van_id' => $stock['vantransub_van_id']];

        $vanStk = VanStock::where($whr)->first();
        if (!empty($vanStk)) {

            $last_rate = isset($stock['vantransub_ret_rate']) ? $stock['vantransub_ret_rate'] : $vanStk['vanstock_avg_tran_rate'];

            $total_amount = ($vanStk['vanstock_qty'] * $vanStk['vanstock_avg_tran_rate']) - ($stock['vantransub_qty'] * $last_rate);

            $remaining_qty = ($vanStk['vanstock_qty'] - $stock['vantransub_qty']);
            $avg_rate = ($remaining_qty > 0) ? $total_amount / $remaining_qty : 0;

            $vanStk['vanstock_qty'] = $vanStk['vanstock_qty'] - $stock['vantransub_qty'];
            //$vanStk['vanstock_last_tran_rate'] = $stock['vantransub_purch_rate'];
            $vanStk['vanstock_avg_tran_rate'] = $avg_rate;
            $vanStk['server_sync_time'] = $this->server_sync_time;
            $vanStk->save();
        } else {
            $insert = ['vanstock_qty' => $stock['vantransub_qty'],
                'vanstock_last_tran_rate' => 0,
                'vanstock_avg_tran_rate' => 0,
                'branch_id' => $this->branch_id, 'server_sync_time' => $this->server_sync_time];

            VanStock::create(array_merge($whr, $insert));
        }
    }

    public function listTransferInfo(Request $request)
    {

        $results = Product::select('prd_id', 'prd_barcode', 'prd_name', 'prd_alias', 'prd_base_unit_id',
            'vantransub_purch_rate', 'vantransub_qty',
            'vantransub_prod_id', 'vantransub_stock_id', 'vantransub_branch_stock_id')

            ->Join('van_transfer_sub', function ($join) {
                $join->on('products.prd_id', 'van_transfer_sub.vantransub_prod_id')
                    ->where('van_transfer_sub.branch_id', $this->branch_id)
                    ->where('vantransub_van_id', $request->van_id)
                    ->where('prd_name', 'like', '%' . $request->keyword . '%');

            })->take(10)->get()->toArray();
    }

    public function vanTranVoid(Request $request)
    {
        if ($request->vantran_id) {
            $validator = Validator::make(
                $request->all(),
                ['vantran_id' => 'required|exists:van_transfer,vantran_id'],
                ['vantran_id.required' => 'Required']
            );
            if ($validator->fails()) {
                return parent::displayError($validator->messages());
            }
            // reverting van transfer - (van transfer void)
            $this->revertVanTran($request->vantran_id);
            return parent::displayMessage('Reverted  Transfer Successfully');
        } else {
            return parent::displayError('Invalid Ref Id');
        }
    }

    public function revertVanTran($vantran_id)
    {
        VanTransfer::where('vantran_id', $vantran_id)->update(['vantran_flags' => 0]);
        $vantranSub = VanTransferSub::where('vantransub_vantran_id', $vantran_id)->get()->toArray();
        foreach ($vantranSub as $k => $stock) {
            // print_r($stock['vantransub_id']);exit;

            $this->deductStockQuantity($stock);
            $this->updateInDailyVanStocks($stock, $deduct = 1);

            if ($stock['vantransub_godown_id'] > 0) {
                $this->vanToGodownStockUpdations($stock);
            } else {
                $this->updateBranchStocks($stock['vantransub_branch_stock_id'], $stock['vantransub_qty'], 1);
            }

            VanTransferSub::where('vantransub_vantran_id', $vantran_id)->update(['vantransub_flags' => 0]);
        }
    }

    public function vanTranReturnVoid(Request $request)
    {
        if ($request->vantranret_id) {
            $validator = Validator::make(
                $request->all(),
                ['vantranret_id' => 'required|exists:van_transfer_return,vantranret_id'],
                ['vantranret_id.required' => 'Required']
            );
            if ($validator->fails()) {
                return parent::displayError($validator->messages());
            }
            $this->revertVanTranReturn($request->vantranret_id);

            return parent::displayMessage('Reverted  Transfer  Return Successfully');
        } else {
            return parent::displayError('Invalid Ref Id');
        }
    }

    public function revertVanTranReturn($vantranret_id)
    {
        VanTransferReturn::where('vantranret_id', $vantranret_id)->update(['vantranret_flag' => 0]);
        $vantranSub = VanTransferReturnSub::where('vantranretsub_vantranret_id', $vantranret_id)->get()->toArray();

        foreach ($vantranSub as $k => $stock) {

            $this->updateVanStocksReturnRevert($stock);

            if ($stock['vantranretsub_godown_id'] > 0) {
                $this->GodownToVanStockUpdationsReturnRevert($stock);
            }
            // else {
            //     $this->calculationRevertReturn($stock['vantransub_branch_stock_id'],
            //     $stock['vantranretsub_qty'], 1);
            // }

            VanTransferReturnSub::where('vantranretsub_id', $stock['vantranretsub_id'])->
                update(['vantranretsub_flags' => 0, 'server_sync_time' => $this->server_sync_time]);
        }
    }

    public function updateVanStocksReturnRevert($stock)
    {

        $whr = ['vanstock_prod_id' => $stock['vantranretsub_prod_id'],
            'vanstock_stock_id' => $stock['vantranretsub_stock_id'],
            'vanstock_branch_stock_id' => $stock['vantransub_branch_stock_id'],
            'vanstock_van_id' => $stock['vantranretsub_van_id']];

        $vanStk = VanStock::where($whr)->first();

        if (!empty($vanStk)) {

            $total_amount = ($vanStk['vanstock_qty'] * $vanStk['vanstock_avg_tran_rate']) + ($stock['vantranretsub_qty'] * $stock['vantranretsub_purch_rate']);

            $tot = $stock['vantranretsub_qty'] + $vanStk['vanstock_qty'];
            $avg_rate = ($tot > 0) ? $total_amount / ($tot) : 0;

            $vanStk['vanstock_qty'] = $vanStk['vanstock_qty'] + $stock['vantranretsub_qty'];
            $vanStk['vanstock_last_tran_rate'] = $stock['vantranretsub_purch_rate'];
            $vanStk['vanstock_avg_tran_rate'] = $avg_rate;
            $vanStk['server_sync_time'] = $this->server_sync_time;
            $vanStk->save();
        } else {
            $insert = ['vanstock_qty' => $stock['vantranretsub_qty'],
                'vanstock_last_tran_rate' => $stock['vantranretsub_purch_rate'],
                'vanstock_avg_tran_rate' => $stock['vantranretsub_purch_rate'],
                'server_sync_time' => $this->server_sync_time,
                'branch_id' => $this->branch_id];

            VanStock::create(array_merge($whr, $insert));
        }
        $change = $stock['vantranretsub_qty'];
        $column = ($stock['vantranretsub_godown_id'] > 0) ? "bs_stock_quantity_gd" : "bs_stock_quantity_shop";
        BranchStocks::where('branch_stock_id', $stock['vantransub_branch_stock_id'])
            ->update([
                'bs_stock_quantity_van' => \DB::raw("bs_stock_quantity_van +$change"),
                $column => \DB::raw("$column -$change"),
                'server_sync_time' => $this->server_sync_time,
            ]);

        $this->updateInDailyVanStocksReturnRevert($stock);
        return true;
    }

    public function updateInDailyVanStocksReturnRevert($stock, $deduct = false)
    {

        $whr = ['vds_prd_id' => $stock['vantranretsub_prod_id'],
            'vds_stock_id' => $stock['vantranretsub_stock_id'],
            'vds_branch_stock_id' => $stock['vantransub_branch_stock_id'],
            'vds_van_id' => $stock['vantranretsub_van_id'],
            'vds_date' => $stock['vantranretsub_date']];

        $vanStk = VanDailyStocks::where($whr)->first();

        if (!empty($vanStk)) {

            $vanStk['vds_stock_quantity'] = ($deduct == 1) ? ($vanStk['vds_stock_quantity'] - $stock['vantranretsub_qty'])
            : ($vanStk['vds_stock_quantity'] + $stock['vantranretsub_qty']);
            $vanStk['server_sync_time'] = $this->server_sync_time;
            $vanStk->save();
        } else {

            $insert = ['server_sync_time' => $this->server_sync_time, 'branch_id' => $this->branch_id, 'vds_stock_quantity' => ($deduct == 1) ? (0 - $stock['vantranretsub_qty']) : $stock['vantranretsub_qty']];
            VanDailyStocks::create(array_merge($whr, $insert));
        }

        return true;
    }

    public function calculationRevertReturn($branch_stock_id, $vantransub_qty, $shop)
    {
        $bstck = BranchStocks::where('branch_stock_id', $branch_stock_id)->first();

        $bstck['bs_stock_quantity_van'] += $vantransub_qty;

        if ($shop) {
            $bstck['bs_stock_quantity_shop'] -= $vantransub_qty;
        } else {
            $bstck['bs_stock_quantity_gd'] -= $vantransub_qty;
        }

        $bstck['server_sync_time'] = $this->server_sync_time;
        $bstck->save();

    }

    public function GodownToVanStockUpdationsReturnRevert($stock)
    {

        $whr = ['gs_godown_id' => $stock['vantranretsub_godown_id'],
            'gs_branch_stock_id' => $stock['vantransub_branch_stock_id'],
            'gs_prd_id' => $stock['vantranretsub_prod_id']];

        // $this->calculationRevertReturn($stock['vantransub_branch_stock_id'], $stock['vantranretsub_qty'], 0);
        if ($gdStk = GodownStock::where($whr)->first()) {

            $gdStk['gs_qty'] += $stock['vantranretsub_qty'];
            $gdStk['server_sync_time'] = $this->server_sync_time;
            $gdStk->save();

        } else {

            $gdStock = [
                'gs_godown_id' => $stock['vantranretsub_godown_id'],
                'gs_branch_stock_id' => $stock['vantransub_branch_stock_id'],
                'gs_stock_id' => $stock['vantranretsub_stock_id'],
                'gs_prd_id' => $stock[''],
                'gs_qty' => $stock['vantranretsub_qty'],
                'gs_date' => date('Y-m-d'),
                'gs_flag' => 1,
                'server_sync_time' => $this->server_sync_time,
                'branch_id' => $this->branch_id,
            ];
            $gdStk = GodownStock::create($gdStock);
        }

        $gdLog['gsl_gdwn_stock_id'] = $gdStk->gs_id;
        $gdLog['gsl_branch_stock_id'] = $stock['vantransub_branch_stock_id'];
        $gdLog['gsl_stock_id'] = $stock['vantranretsub_stock_id'];
        $gdLog['gsl_prd_id'] = $stock['vantranretsub_prod_id'];
        $gdLog['gsl_prod_unit'] = $stock['vantranretsub_unit_id'];
        $gdLog['gsl_qty'] = $stock['vantranretsub_qty'];
        $gdLog['gsl_from'] = $stock['vantranretsub_godown_id'];
        $gdLog['gsl_to'] = 0;
        $gdLog['gsl_tran_vanid'] = $stock['vantranretsub_van_id'];
        $gdLog['gsl_vchr_type'] = 26;
        $gdLog['gsl_date'] = date('Y-m-d');
        $gdLog['gsl_added_by'] = $this->usr_id;
        $gdLog['branch_id'] = $this->branch_id;
        $gdLog['server_sync_time'] = $this->server_sync_time;

        GodownStockLog::create($gdLog);

    }

    public function serachSoldProduct(Request $rq)
    {

        $results = [];
        if ($rq->keyword || $rq->brcode) {
            $this->key = $rq->keyword;
            $this->barcode = $rq->brcode;
            $results = Product::select('prd_id', 'prd_tax_cat_id', 'prd_barcode', 'prd_tax', 'prd_name', 'prd_alias', 'prd_base_unit_id', 'branch_stock_id', 'bs_stock_id', 'bs_prate', 'bs_avg_prate', 'bs_srate', 'bs_stock_quantity', 'bs_stock_quantity_shop', 'bs_stock_quantity_gd', 'bs_stock_quantity_van')

                ->Join('branch_stocks', function ($join) {
                    $join->on('products.prd_id', 'branch_stocks.bs_prd_id')
                        ->where('bs_branch_id', $this->branch_id)
                        ->where('prd_stock_status', 1);
                    if ($this->key) {
                        $join->where('prd_name', 'like', '%' . $this->key . '%');
                    } else {
                        $join->where('prd_barcode', $this->barcode);
                    }

                })
            // ->leftjoin('sale_sub', function ($join) {
            //     $join->on('branch_stocks.branch_stock_id', 'sale_sub.salesub_branch_stock_id');
            // })
                ->groupBy('products.prd_id')->take(10)->get();
            if (count($results) == 0) {
                $product_name = ProdUnit::where('produnit_ean_barcode', $this->barcode)->join('products', 'prod_units.produnit_prod_id', '=', 'products.prd_id')->pluck('products.prd_name');

                if (count($product_name) > 0) {
                    $results = Product::select('prd_id', 'prd_barcode', 'prd_name', 'prd_alias', 'prd_base_unit_id', 'branch_stock_id', 'bs_stock_id', 'bs_prate', 'bs_avg_prate', 'bs_stock_quantity', 'bs_stock_quantity_shop', 'bs_stock_quantity_gd', 'bs_stock_quantity_van')

                        ->Join('branch_stocks', function ($join) {
                            $join->on('products.prd_id', 'branch_stocks.bs_prd_id')
                                ->where('bs_branch_id', $this->branch_id)
                                ->where('prd_stock_status', 1);
                            if ($this->key) {
                                $join->where('prd_name', 'like', '%' . $this->key . '%');
                            } else {
                                $join->where('prd_name', $product_name[0]);
                            }

                        })
                        ->Join('sale_sub', function ($join) use ($product_name) {
                            $join->on('branch_stocks.branch_stock_id', 'sale_sub.salesub_branch_stock_id');
                        })
                        ->groupBy('products.prd_id')->take(10)->get();

                    $prd_ean_unit_id = ProdUnit::where('produnit_ean_barcode', $this->barcode)->join('products', 'prod_units.produnit_prod_id', '=', 'products.prd_id')->pluck('prod_units.produnit_unit_id');
                }

            }

            foreach ($results as $k => $row) {

                $units = ProdUnit::select('prod_units.produnit_unit_id as sur_unit_id',
                    'units.unit_base_qty', 'units.unit_name', 'units.unit_code', 'stock_unit_rates.sur_unit_rate')
                    ->join('units', 'units.unit_id', 'prod_units.produnit_unit_id')
                    ->join('stock_unit_rates', 'prod_units.produnit_unit_id', 'stock_unit_rates.sur_unit_id')
                    ->where('stock_unit_rates.branch_stock_id', $row['branch_stock_id'])
                    ->where('prod_units.produnit_prod_id', $row['prd_id'])->get();
                $results[$k]['prd_units'] = $units;

                $wher['salesub_prod_id'] = $row['prd_id'];
                $wher['branch_id'] = $this->branch_id;
                $wher['salesub_flags'] = 1;
                $out = SalesSub::select(DB::raw("SUM(salesub_qty) as total_qty"))->where($wher)->get()->toArray();

                $results[$k]['total_sold'] = ($out[0]['total_qty'] > 0) ? $out[0]['total_qty'] : 0;
                $whr['salesretsub_branch_stock_id'] = $row['branch_stock_id'];
                $whr['salesretsub_flags'] = 1;

                $out2 = SalesReturnSub::select(DB::raw("SUM(salesretsub_qty) as total_qty"))->where($whr)->get()->toArray();
                $results[$k]['total_retuned'] = ($out2[0]['total_qty'] > 0) ? $out2[0]['total_qty'] : 0;
                $max_returnable = ($results[$k]['total_sold'] - $results[$k]['total_retuned']);
                $results[$k]['max_returnable'] = ($max_returnable > 0) ? $max_returnable : 0;

                $results[$k]['gdstock'] = GodownStock::select('godown_stocks.gs_godown_id as gd_id', 'godown_stocks.gs_qty', 'godown_master.gd_name')->join('godown_master', 'godown_master.gd_id', 'godown_stocks.gs_godown_id')->where('godown_stocks.gs_branch_stock_id', $row['branch_stock_id'])->get();
                $base_unit_name = $base_unit_code = '';
                foreach ($units as $k2 => $val) {
                    if ($val['unit_base_qty'] == 1) {
                        $base_unit_name = $val['unit_name'];
                        $base_unit_code = $val['unit_code'];
                    }
                }

                $results[$k]['base_unit_name'] = $base_unit_name;
                $results[$k]['base_unit_code'] = $base_unit_code;
            }
            return parent::displayData($results);
        }
    }
}
