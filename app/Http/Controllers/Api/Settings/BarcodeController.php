<?php

namespace App\Http\Controllers\Api\Settings;

use App\Http\Controllers\Api\ApiParent;
use Illuminate\Http\Request;
use App\Models\Settings\BarCode;
use App\Common\Corelib;
use Illuminate\Validation\Rule;
use validator;

class BarcodeController extends ApiParent
{


	public function barcodeOperations(Request $rq, $rq_type)
    {

        switch ($rq_type) {

            case "viewdetails":return $this->viewDetails($rq);
            break;

           case "addprofile":return $this->addProfile($rq);
           break;

           case "editprofile":return $this->editProfile($rq);
           break;

           case "edititem":return $this->editItem($rq);
           break;

           case "editbarcode":return $this->editBarcode($rq);
           break;

           case "editprice":return $this->editPrice($rq);
           break;

           case "editcurr":return $this->editCurr($rq);
           break;

           case "editprate":return $this->editPrate($rq);
           break;

           case "editmfg":return $this->editMfg($rq);
           break;

           case "editexp":return $this->editExp($rq);
           break;

           case "editshop":return $this->editShop($rq);
           break;

            default:return response()->json(['error' => 'Invalid Endpoint', 'status' => $this->badrequest_stat]);
            break;
        }
    }
	

   	public function viewDetails(Request $request)
	{	
     $data =  BarCode::all();
	return response()->json(['data' => $data, 'status' => $this->success_stat]);	
	}


    public function addProfile(Request $request)
    {

    $validator = Validator::make(
            $request->all(),
            [
                'barset_profile' => 'required|unique:settings_barcode_print',               
            ],
            [
              'barset_profile.required' => 'Required'                
            ]
        );
        if ($validator->fails()) {
        return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        } 

      $data = $request->all(); 
      $barset_id = BarCode::count()+1;
      $data['barset_id'] = $barset_id;
      $data['barset_barcode'] ='0,0,0,1A,1,2,50,B';
      $data['barset_product'] ='0,0,0,1,1,1,N';
      $data['barset_currency'] ='0,0,0,1,1,1,N';
      $data['barset_price'] ='0,0,0,1,1,1,N';
      $data['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);     
      $added  = BarCode::create($data);
      $res = BarCode::where('barset_id', $barset_id)->first();
            

    return response()->json(['data' => $res, 'status' => $this->success_stat]); 
    }

       public function editProfile(Request $request)
    {

    $validator = Validator::make(
            $request->all(),
            [
                'barset_profile' => ['required', Rule::unique('settings_barcode_print')->ignore($request['barset_id'], 'barset_id')],               
            ],
            [
              'barset_profile.required' => 'Required'                
            ]
        );
        if ($validator->fails()) {
        return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        } 

      $data = $request->all();
      $data['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);  
      $id = $request->barset_id;
      BarCode::where('barset_id', $id)
            ->update($data);  

    return response()->json(['data' => 'ok', 'status' => $this->success_stat]); 
    }



    public function editItem(Request $request)
    {
      
      $barset_product = $request->sp_item_x.','.$request->sp_item_y.','.$request->rotation.
      ','.$request->item_font.','.$request->sp_item_mult_x.','.$request->sp_item_mult_y.','.'N';

      $data = ['barset_product'=> $barset_product,
      'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)];
      $id = $request->barset_id;
      BarCode::where('barset_id', $id)
            ->update($data);  

    return response()->json(['data' => 'ok', 'status' => $this->success_stat]); 
   }


      public function editBarcode(Request $request)
    {
        if($request->barcode_display==true)
        {$displ = "B";}else{$displ = "N";}
      
      $barset_barcode = $request->start_position_x.','.$request->start_position_y.','.$request->rotation.
      ',1A,'.$request->narrow_bar_width.','.$request->wide_bar_width.','.$request->barcode_height.','.$displ;
   
      $data = ['barset_barcode'=> $barset_barcode,'barset_dark'=>$request->print_darkness,
    'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)];
      $id = $request->barset_id;
      BarCode::where('barset_id', $id)
            ->update($data);  

    return response()->json(['data' => 'ok', 'status' => $this->success_stat]); 
   }


     public function editPrice(Request $request)
    {

     if($request->sp_price_enable==true)
        {$sp_price_enable = 1;}else{$sp_price_enable = 0;}

        if($request->sp_price_encrypt==true)
        {$sp_price_encrypt = 1;}else{$sp_price_encrypt = 0;} 

        if($request->sp_price_en_dtail==true)
        {$barset_pdet_enable = 1;}else{$barset_pdet_enable = 0;}      
      
      $barset_price = $request->sp_price_x.','.$request->sp_price_y.','.$request->rotation_price_id.
      ','.$request->price_font.','.$request->sp_price_mult_x.','.$request->sp_price_mult_y.','.'N';

       $barset_pdet_epl = $request->sp_price_pdet_x.','.$request->sp_price_pdet_y.','
       .$request->rotation_price_pdet_id.','.$request->price_pdet_font.','.$request->sp_price_pdet_mult_x.','.$request->sp_price_pdet_mult_y.','.'N';
     
      $data = ['barset_price'=> $barset_price,'barset_pdet_epl'=>$barset_pdet_epl,
      'barset_pdet_text'=>$request->barset_pdet_text,'barset_price_enable'=> $sp_price_enable,
      'barset_price_encrypt'=> $sp_price_encrypt,'barset_pdet_enable'=>$barset_pdet_enable,
    'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)];

      $id = $request->barset_id;
      BarCode::where('barset_id', $id)
            ->update($data);  

    return response()->json(['data' => 'ok', 'status' => $this->success_stat]); 
   }

    public function editCurr(Request $request)
    {
      
      $barset_curr = $request->sp_curr_x.','.$request->sp_curr_y.','.$request->rotation_curr_id.
      ','.$request->curr_font.','.$request->sp_curr_mult_x.','.$request->sp_curr_mult_y.','.'N';  
     
      $data = ['barset_currency'=> $barset_curr,'barset_currency_short'=>$request->barset_currency_short,
     'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)];
      $id = $request->barset_id;
      BarCode::where('barset_id', $id)
            ->update($data);  

    return response()->json(['data' => 'ok', 'status' => $this->success_stat]); 
   }

    public function editPrate(Request $request)
    {

    if($request->sp_prate_enable==true)
    {$barset_prate_enable = 1;}else{$barset_prate_enable = 0;}  

    if($request->sp_prate_encrypt==true)
    {$barset_prate_encrypt = 1;}else{$barset_prate_encrypt = 0;}  
      
      $barset_prate = $request->sp_prate_x.','.$request->sp_prate_y.','.$request->rotation_prate_id.
      ','.$request->prate_font.','.$request->sp_prate_mult_x.','.$request->sp_prate_mult_y.','.'N';  
     
      $data = ['barset_prate_epl'=> $barset_prate,'barset_prate_enable'=>$barset_prate_enable,
      'barset_prate_encrypt'=>$barset_prate_encrypt,
      'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)];

      $id = $request->barset_id;
      BarCode::where('barset_id', $id)
            ->update($data);  

    return response()->json(['data' => 'ok', 'status' => $this->success_stat]); 
   }

    public function editMfg(Request $request)
    {

    if($request->sp_mfg_enable==true)
    {$barset_mfg_enable = 1;}else{$barset_mfg_enable = 0;}  

      $barset_mfg_epl = $request->sp_mfg_x.','.$request->sp_mfg_y.','.$request->rotation_mfg_id.
      ','.$request->mfg_font.','.$request->sp_mfg_mult_x.','.$request->sp_mfg_mult_y.','.'N';  
     
      $data = ['barset_mfg_epl'=> $barset_mfg_epl,'barset_mfg_enable'=>$barset_mfg_enable,
    'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)];
      $id = $request->barset_id;
      BarCode::where('barset_id', $id)
            ->update($data);  

    return response()->json(['data' => 'ok', 'status' => $this->success_stat]); 
   }

     public function editExp(Request $request)
    {

     if($request->sp_exp_enable==true)
    {$barset_exp_enable = 1;}else{$barset_exp_enable = 0;}  
      
     $barset_exp_epl = $request->sp_exp_x.','.$request->sp_exp_y.','.$request->rotation_exp_id.
    ','.$request->exp_font.','.$request->sp_exp_mult_x.','.$request->sp_exp_mult_y.','.'N';  

    $data = ['barset_exp_epl'=> $barset_exp_epl,'barset_exp_enable'=>$barset_exp_enable,
    'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)];
    $id = $request->barset_id;
    BarCode::where('barset_id', $id)
          ->update($data);  

    return response()->json(['data' => 'ok', 'status' => $this->success_stat]); 
   }

   public function editShop(Request $request)
    {

         if($request->sp_shop_enable==true)
    {$barset_shop_enable = 1;}else{$barset_shop_enable = 0;} 

    $barset_shop_epl = $request->sp_shop_x.','.$request->sp_shop_y.','.$request->rotation_shop_id.
    ','.$request->shop_font.','.$request->sp_shop_mult_x.','.$request->sp_shop_mult_y.','.'N';  

    $data = ['barset_shop_epl'=> $barset_shop_epl,'barset_shop_enable'=>$barset_shop_enable,
    'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)];
    $id = $request->barset_id;
    BarCode::where('barset_id', $id)
          ->update($data);  

    return response()->json(['data' => 'ok', 'status' => $this->success_stat]); 
   }



}
