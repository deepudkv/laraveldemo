<?php

namespace App\Http\Controllers\Api\Settings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Settings\RateCodes;
use App\Http\Controllers\Api\ApiParent;

use App\Models\Settings\TaxCategory;



class TaxCategoryController extends ApiParent {

  public function customOperations(Request $request, $type) {

      switch ($type) {
          case 'getall':
              return $this->getAll($request);
              break;
          default:
              return response()->json(['error' => 'Invalid Endpoint', 'status' => $this->badrequest_stat]);
              break;
      }

  }

  public function getAll($request) {
    $out = TaxCategory::get();
    return parent::displayData($out);
    
  }

}
