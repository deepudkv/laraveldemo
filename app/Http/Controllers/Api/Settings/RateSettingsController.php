<?php

namespace App\Http\Controllers\Api\Settings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Settings\RateCodes;
use App\Http\Controllers\Api\ApiParent;



class RateSettingsController extends ApiParent
{
    public function setRates(Request $rq){
        
$zero = $rq['zero'];
$one = $rq['one'];
$two = $rq['two'];
$three = $rq['three'];
$four = $rq['four'];
$five = $rq['five'];
$six = $rq['six'];
$seven = $rq['seven'];
$eight = $rq['eight'];
$nine = $rq['nine'];
$dot = $rq['dot'];



        $code = "$zero;$one;$two;$three;$four;$five;$six;$seven;$eight;$nine;$dot";
        $input = ['rcd_code'=> $code,'rcd_added_by'=>$this->usr_id];
        $input['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
        $data = RateCodes::find(11);
        $data->fill($input)->save();

        if($data){
            return parent::displayMessage('Updated Successfull');

        }


  }

  public function getRates(Request $rq){

    
    $data = RateCodes::find(11);
    return $data;
  }

}
