<?php

namespace App\Http\Controllers\Api\Settings;

use App\Http\Controllers\Api\ApiParent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Settings\MenuMaster;


class MenuController extends ApiParent
{
    
    public function Menu(Request $request, $type)
    {

        switch ($type) {
            case 'get_menu_by_group':
                return parent::displayData($this->getMenuByGroup($request));
                break;

            default:
                return parent::displayError('Invalid Endpoint', $this->badrequest_stat);
                break;
        }
    }


    public function getMenuByGroup($request){


        $rltn = [
            'subMenus' => function ($qr) {
                $qr->select('menu_id','menu_name','menu_prnt_id')->where('menu_depth',1)->where('menu_flag',1);
            },
        ];

        $res = MenuMaster::select('*')->with($rltn)->where('menu_grp_id',$request['grp_id'])->where('menu_depth',0)->where('menu_flag',1)->get();

    return $res;

    }

}
