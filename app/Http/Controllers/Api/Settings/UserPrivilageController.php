<?php

namespace App\Http\Controllers\Api\Settings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Settings\userPrivilages;
use App\Http\Controllers\Api\ApiParent;
use App\Models\Settings\MenuMaster;


class UserPrivilageController extends ApiParent
{

    public function Privilage(Request $request, $type)
    {

        switch ($type) {
            case 'update_privilage':
                return parent::displayData($this->updatePrivilage($request));
                break;

            case 'get_user_privilages':
                return parent::displayData($this->getUserPrivilages($request));
                break;

            default:
                return parent::displayError('Invalid Endpoint', $this->badrequest_stat);
                break;
        }
    }




    public function updatePrivilage($request)
    {

        $request = $request->all();

        $menus = MenuMaster::where('menu_grp_id', $request['acc_grp'])->where('menu_flag', 1)->get()->pluck('menu_id')->toArray();

        $privilage['up_usr_id'] = $request['ledger_id'];

        userPrivilages::join('menu_master', 'menu_id', 'up_menu_id')->where('menu_grp_id', $request['acc_grp'])->where('up_usr_id', $request['ledger_id'])->delete();


        foreach ($menus as $mns) {
            if (isset($request['menuIds' . '[' . $mns . ']']) && !$request['menuIds' . '[' . $mns . ']']) {

                $privilage['up_menu_id'] = $mns;
                // $privilage['up_show_menu'] = $value['showMenu'];
                // $privilage['up_add'] = $value['add'];
                // $privilage['up_edit'] = $value['edit'];
                // $privilage['up_void'] = $value['void'];
                $privilage['up_flags'] = 1;
                $privilage['branch_id'] = $this->branch_id;
                $privilage['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);

                $res = userPrivilages::create($privilage);
            }
        }

        
            return "Successfully Updated";
       
    }

    public function getUserPrivilages($request)
    {
        
        $result = userPrivilages::where('up_usr_id', $request['usr_id'])->where('up_flags', 1)->get()->pluck('up_menu_id')->toArray();

        $menus = MenuMaster::where('menu_flag', 1)->get()->pluck('menu_id')->toArray();

        $menuIds = array();
        foreach ($menus as $key => $value) {
            if ($value) {
                if (in_array($value, $result)) {
                    $menuIds[$value] = false;
                } else {
                    $menuIds[$value] = true;
                }
            }
        }
        if ($menuIds) {
            return $menuIds;
        } else {
            return [];
        }
    }
}
