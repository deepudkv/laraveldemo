<?php
namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Company\Acc_branch;
use App\Models\Company\CompanySettings;
use App\Models\Userdetails\UserType;
use App\User;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Validator;
use App\Models\Settings\userPrivilages;


class UserController extends Controller
{

    // Success
    public $success_stat = 200;
    public $created_stat = 201;
    // errors
    public $badrequest_stat = 400;
    public $unauthorized_stat = 401;
    public $forbidden_stat = 403;
    public $notfound_stat = 404;
    public $methodnotallowed_stat = 405;
    public $conflict_stat = 409;
    public $lenghthrequired_stat = 411;
    public $preconditionfailed_stat = 412;
    public $toomanyrequests_stat = 429;
    public $internalservererror_stat = 500;
    public $serviceunavailable_stat = 503;

/**
 * @api {get} http://127.0.0.1:8000/api/login  Login
 * @apiName login
 * @apiVersion 0.1.0
 * @apiGroup User
 *
 * @apiHeader {String} Authorization Authorization (Required)
 * @apiHeader {String} Accept Accept (Required)
 *
 * @apiParam {varchar} usr_email User Email
 * @apiParam {varchar} usr_password User Password
 *
 * @apiSuccess {String} status Status Code
 * @apiSuccess {String} data All Users

 */


public function test()
{
    echo  date('Y-m-d H:i:s', strtotime('2020-03-03T08:32:38.055Z')); die();

}
    public function login()
    {
       
        if (Auth::attempt(['usr_username' => request('usr_email'), 'usr_password' => request('usr_password')])) {

   
            $user = User::Join('user_types', function ($join) {
                $join->on('user_types.type_id', 'users.usr_type');
            })->where('usr_id',Auth::id())->first();


            if($user->usr_active == 0)
            {
                return response()->json(['error' => ' Account Inactivated ', 'status' => $this->badrequest_stat]);
            }
           
            $token = $user->createToken('MyApp')->accessToken;
            $name = $user->usr_name;

            $company_data = CompanySettings::first();
            $branch_data = Acc_branch::where('branch_id',$user->branch_id)->first();
            // $userPrivilage = userPrivilages::where('up_usr_id', $user->usr_id)->where('up_flags', 1)->get()->pluck('up_menu_id')->toArray();
            // return $user->usr_id;
            $req['usr_id'] = $user->usr_id;
            $userPrivilage = app('App\Http\Controllers\Api\Settings\UserPrivilageController')->getUserPrivilages($req);
            $user->branch_name = isset($branch_data->branch_name)?$branch_data->branch_name:'';
            $user->branch_disp_name = isset($branch_data->branch_display_name)?$branch_data->branch_display_name:$company_data->cmp_name;
            $user->branch_code = isset($branch_data->branch_code)?$branch_data->branch_code:$company_data->cmp_code;
            $user->branch_address = isset($branch_data->branch_address)?$branch_data->branch_address:'';
            $user->img_url = isset($branch_data->img_url)?$branch_data->img_url:'';
            $user->branch_phone = isset($branch_data->branch_phone)?$branch_data->branch_phone:'';
            $user->branch_mob = isset($branch_data->branch_mob)?$branch_data->branch_mob:'';
            $user->branch_tin = isset($branch_data->branch_tin)?$branch_data->branch_tin:'';
            $user->branch_reg_no = isset($branch_data->branch_reg_no)?$branch_data->branch_reg_no:'';
            $user->branch_open_date = isset($branch_data->branch_open_date)?$branch_data->branch_open_date:'';
            $user->branch_email = isset($branch_data->branch_email)?$branch_data->branch_email:'';

        
            $branch_id = $user->branch_id;

            
            
            $cmpToken = encrypt(array(
                'company_code' => request('company_code'), 
            ));
    
            return response()->json(['cmptoken' => $cmpToken, 'token' => $token,'profile' => $user, 'usr_type'=> $user->user_type, 'branch_id' => $branch_id, 'name' => $name, 'status' => $this->success_stat,'usrPrvlg'=>$userPrivilage]);

        } else {
            return response()->json(['error' => 'Invalid Login', 'status' => $this->badrequest_stat]);
        }

    }

    /**
     * @api {get} http://127.0.0.1:8000/api/usr_register  Register User
     * @apiName usrRegister
     * @apiVersion 0.1.0
     * @apiGroup User
     *
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {int} usr_type User Type
     * @apiParam {varchar} usr_name Name
     * @apiParam {varchar} usr_username User Name
     * @apiParam {varchar} usr_email User Email
     * @apiParam {varchar} usr_nickname User Nikname
     * @apiParam {varchar} usr_phone User Phone
     * @apiParam {varchar} usr_mobile User Mobile
     * @apiParam {varchar} usr_password User Password
     * @apiParam {varchar} usr_address User Address
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} data All Users
     * @apiError error Error Message.
     */

    public function usrRegister(Request $request)
    
    {

        

        if(isset($request->usr_type)&&$request->usr_type==1)
        {
            $validate_array=[
                'usr_name' => 'required',
                'usr_type' => 'required',
                'usr_username' => 'required|unique:users|min:4',
                'usr_email' => 'nullable|email|unique:users',
                'usr_nickname' => 'nullable|min:3',
                'usr_password' => 'required',
                'c_password' => 'required|same:usr_password',
                'usr_mobile' => 'nullable|regex:/^([0-9\s\-\+\(\)]*)$/'
    
            ];
        }
        else
        {
            $validate_array=[
                'usr_name' => 'required',
                'usr_type' => 'required',
                'usr_username' => 'required|unique:users|min:4',
                'usr_email' => 'nullable|email|unique:users',
                'usr_nickname' => 'nullable|min:3',
                'usr_password' => 'required',
                'c_password' => 'required|same:usr_password',
                'usr_mobile' => 'nullable|regex:/^([0-9\s\-\+\(\)]*)$/',
                'branch_id' => 'required|exists:acc_branches,branch_id',
    
            ];
        }
        $validator = Validator::make($request->all(), $validate_array, [
            'usr_name.required' => 'Required',
            'usr_username.required' => 'Required',
            'usr_username.min' => 'Minimum 4 Character',
            'usr_username.unique' => 'Already Exists',
            'usr_type.required' => 'Required',
            'branch_id.required' => 'Required',
            'usr_email.email' => 'Invalid Input',
            'usr_email.unique' => 'Already Exists',
            'usr_nickname.min' => 'Enter minimum 3 charecters',
            'usr_password.required' => 'Required',
            'c_password.required' => 'Please Re-Enter Password',
            'c_password.same' => 'Password does not match',
            'usr_mobile.required' => 'Required',
            'usr_mobile.regex' => 'Invalid',
            'usr_mobile.min' => 'Maximum 12 digits',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }

        $input = $request->all();
        unset($input['c_password']);
        $input['pass_md5'] = md5($input['usr_password']);
        $input['usr_password'] = bcrypt($input['usr_password']);
        $input["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);

    
        if (is_null($input['usr_cash_ledger_id'])) {
            $input['usr_cash_ledger_id'] = 0;
        }
        $user = User::create($input);
        $success['token'] = $user->createToken('MyApp')->accessToken;
        $success['usr_first_name'] = $user->usr_first_name;
        return response()->json(['data' => 'User Added Successfully', 'status' => $this->success_stat]);

    }

/**
 * @api {get} http://127.0.0.1:8000/api/details  User Details
 * @apiName details
 * @apiVersion 0.1.0
 * @apiGroup User
 *
 * @apiHeader {String} Authorization Authorization (Required)
 * @apiHeader {String} Accept Accept (Required)
 *
 * @apiSuccess {String} status Status Code
 * @apiSuccess {String} data  Users Details

 */

    public function details()
    {
         $user = Auth::user();
        return response()->json(['data' => $user, 'status' => $this->success_stat]);

    }

    public function usersList()
    {
        $data = User::select('users.*','user_types.*','acc_branches.branch_name')->Join('user_types', function ($join) {
            $join->on('users.usr_type', 'user_types.type_id');
        })->Join('acc_branches', function ($join) {
            $join->on('users.branch_id', 'acc_branches.branch_id');
        })->orderBy('users.usr_id','desc')->paginate(100);




        return response()->json(['data' => $data, 'status' => $this->success_stat]);

    }
    public function branchList()
    {
        $data = Acc_branch::select('*')->orderBy('branch_id','desc')->paginate(20);
        return response()->json(['data' => $data, 'status' => $this->success_stat]);

    }
    public function activateUser(Request $request)
    {
        $db = User::find($request->user_id);
        $active = $db['usr_active'];
        $db['usr_active'] = ($active) ? 0 : 1;
        $db['server_sync_time'] =date('ymdHis') . substr(microtime(), 2, 6);
        $db->save();
        return response()->json(['message' => ($active) ? 'Inactivated' : 'Activated', 'status' => $this->success_stat]);
        //return parent::displayMessage(($active) ? 'Inactivated' : 'Activated');
    }

/**
 * @api {get} http://127.0.0.1:8000/api/logout  Logout
 * @apiName logout
 * @apiVersion 0.1.0
 * @apiGroup User
 *
 * @apiHeader {String} Authorization Authorization (Required)
 * @apiHeader {String} Accept Accept (Required)
 *
 * @apiSuccess {String} status Status Code

 */

    public function logout()
    {
        if (Auth::check()) {
            $usr_id = Auth::user()->usr_id;
            DB::delete("delete from `erp_oauth_access_tokens` where `erp_oauth_access_tokens`.`user_id` =" . $usr_id);
            return response()->json(['message' => 'Successfully Logged'], $this->success_stat);
        }
        return response()->json(['error' => 'Unauthorised', 'status' => $this->badrequest_stat]);

    }

/**
 * @api {get} http://127.0.0.1:8000/api/users  All User
 * @apiName allUsers
 * @apiVersion 0.1.0
 * @apiGroup User
 *
 * @apiHeader {String} Authorization Authorization (Required)
 * @apiHeader {String} Accept Accept (Required)
 *
 * @apiSuccess {String} status Status Code
 * @apiSuccess {String} data All Users

 */

    public function allUsers()
    {

        $user = user::all();
        return response()->json(['data' => $user, 'status' => $this->success_stat]);
    }

/**
 * @api {post} http://127.0.0.1:8000/api/search_users  Search Users
 * @apiName searchUsers
 * @apiVersion 0.1.0
 * @apiGroup User
 *
 * @apiHeader {String} Authorization Authorization (Required)
 * @apiHeader {String} Accept Accept (Required)
 *
 * @apiParam {varchar} usr_name User Name
 *
 * @apiSuccess {String} status Status Code
 * @apiSuccess {String} data All Users

 */

    public function searchUsers(Request $request)
    {
        if ($request['usr_name'] != "") {
            $users = user::where('usr_name', 'like', '%' . $request['usr_name'] . '%')->take(50)->get();

            return response()->json(['data' => $users, 'status' => $this->success_stat]);
        } else {
            $data = user::take(50)->get();
            return response()->json(['data' => $data, 'status' => $this->success_stat]);
        }
    }

/**
 * @api {post} http://127.0.0.1:8000/api/edit_user_details  Edit Users
 * @apiName editUser
 * @apiVersion 0.1.0
 * @apiGroup User
 *
 * @apiHeader {String} Authorization Authorization (Required)
 * @apiHeader {String} Accept Accept (Required)
 *
 * @apiParam {Bigint} usr_id User id
 * @apiParam {int} usr_type User Type
 * @apiParam {varchar} usr_name Name
 * @apiParam {varchar} usr_username User Name
 * @apiParam {varchar} usr_email User Email
 * @apiParam {varchar} usr_nickname User Nikname
 * @apiParam {varchar} usr_phone User Phone
 * @apiParam {varchar} usr_mobile User Mobile
 * @apiParam {varchar} usr_password User Password
 * @apiParam {varchar} usr_address User Address
 *
 * @apiSuccess {String} status Status Code
 * @apiSuccess {String} data All Users
 * @apiError {String} error Error Message.
 */

    public function editUser(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'usr_id' => 'required',
            // 'usr_type' => 'required',
            'usr_name' => 'required',
            'usr_username' => ['min:4', 'required', Rule::unique('users')->ignore($request['usr_id'], 'usr_id')],
            'usr_email' => 'nullable|email',
            'usr_nickname' => 'nullable|min:4',
            'usr_mobile' => 'nullable|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            'c_password' => 'same:usr_password',
        ], [
            'usr_id' => 'Required',
            // 'usr_type.required' => 'Required',
            'usr_name.required' => 'Required',
            'usr_username.required' => 'Required',
            'usr_username.unique' => 'Already Exists',
            'usr_phone.regex' => 'Invalid',
            'usr_email.mail' => 'Invalid',
            'usr_mobile.regex' => 'Invalid',
            'c_password.same' => 'Password does not match',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }

        $input = $request->all();
    

        if( isset($input['usr_password'])&&!empty($input['usr_password']))
        {
            $input['pass_md5'] = md5($input['usr_password']);
            $input['usr_password'] = bcrypt($input['usr_password']);
        }
        unset($input['c_password']);
        
        $input["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);
        $data = user::where('usr_id', $input['usr_id']);
        $res = $data->update($input);
        if ($res) {
            return response()->json(['message' => 'Updated Successfully', 'status' => $this->created_stat]);
        } else {
            return response()->json(['message' => 'User Details Not Updated', 'status' => $this->badrequest_stat]);
        }

    }

    /**
     * @api {get} http://127.0.0.1:8000/api/get_user_types  Get User Types
     * @apiName getUsertypes
     * @apiGroup User
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} data All User Types
     *
     */
    public function getUsertypes()
    {

        return response()->json(['data' => UserType::get(), 'status' => $this->success_stat]);
    }

    public function getSubUsertypes()
    {

        return response()->json(['data' => UserType::where('type_id','>',1)->get(), 'status' => $this->success_stat]);
    }

}
