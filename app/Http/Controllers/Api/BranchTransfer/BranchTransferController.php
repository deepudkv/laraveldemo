<?php

namespace App\Http\Controllers\Api\BranchTransfer;

use App\Http\Controllers\Api\ApiParent;
use App\Models\BranchTransfer\BranchTransfer;
use App\Models\BranchTransfer\BranchTransferSub;
use App\Models\Godown\GodownStock;
use App\Models\Godown\GodownStockLog;
use App\Models\Product\Product;
use App\Models\Product\ProdUnit;
use App\Models\Product\Units;
use App\Models\Stocks\BranchStocks;
use App\Models\Stocks\CompanyStocks;
use App\Models\Stocks\StockDailyUpdates;

use App\Models\Accounts\Acc_voucher;
use DB;
use Illuminate\Http\Request;

class BranchTransferController extends ApiParent
{

    // ===================== Code Added By Deepu =======================
    public function getNxtVanTranId()
    {
        return parent::displayData(['tran_id' => BranchTransfer::get()->count() + 1]);
    }
    public function updateStatus(Request $request)
    {

        if ($request->status == 1) {
            $sub_items = BranchTransferSub::where('stocktrsub_stocktr_id', $request->stocktr_id)

                ->where('stocktrsub_flags', 1)->get()->toArray();

            foreach ($sub_items as $sub_array) {

                $prd_id = $sub_array['stocktrsub_prd_id'];
                $to_branch = $sub_array['stocktrsub_to'];
                $rate = $sub_array['stocktrsub_rate'];
                $qty = $sub_array['stocktrsub_qty'];

                $where = ['bs_prd_id' => $prd_id, 'bs_branch_id' => $to_branch];

                $bs = BranchStocks::where($where)->first();

                if (isset($bs->bs_prd_id)) {

                    $old_p_amount = $bs->bs_stock_quantity * $bs->bs_avg_prate;
                    $new_p_amount = $qty * $rate;

                    if ($new_p_amount > 0) {
                        $bs_avg_prate = (abs($old_p_amount) + abs($new_p_amount)) / (abs($bs->bs_stock_quantity) + abs($qty));
                    } else {
                        $bs_avg_prate = $bs->bs_avg_prate;
                    }

                    BranchStocks::where($where)->update([
                        'bs_stock_quantity_shop' => \DB::raw("bs_stock_quantity_shop+$qty"),
                        'bs_stock_quantity' => \DB::raw("bs_stock_quantity+$qty"),
                        'bs_avg_prate' => $bs_avg_prate,
                        'bs_prate' => $rate,
                        'server_sync_time' => $this->server_sync_time,
                    ]);
                    $stock_info = BranchStocks::where($where)->first();

                    $stocktrsub_branch_stock_id = $stock_info['branch_stock_id'];
                    $cmp_stock_id = $stock_info['bs_stock_id'];

                    $dateExist = StockDailyUpdates::select('*')->where('sdu_branch_stock_id', $stocktrsub_branch_stock_id)
                        ->where('sdu_date', date('Y-m-d'))->first();

                    if ($dateExist) {

                        $column_name = 'sdu_stock_quantity';
                        StockDailyUpdates::where('sdu_branch_stock_id', $stocktrsub_branch_stock_id)
                            ->where('sdu_date', date('Y-m-d'))->update([
                            $column_name => \DB::raw("$column_name+$qty"),
                            'server_sync_time' => $this->server_sync_time,
                        ]);

                    } else {

                        $ds['sdu_branch_stock_id'] = $stocktrsub_branch_stock_id;
                        $ds['sdu_stock_id'] = $cmp_stock_id;
                        $ds['sdu_prd_id'] = $prd_id;
                        $ds['sdu_date'] = date('Y-m-d');
                        $ds['sdu_stock_quantity'] = $qty;
                        $ds['sdu_gd_id'] = 0;
                        $ds['sdu_batch_id'] = 0;
                        $ds['branch_id'] = $to_branch;
                        $ds['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                        StockDailyUpdates::create($ds);

                    }

                } else {

                    $company = CompanyStocks::where('cmp_prd_id', $prd_id)->first();

                    $branchStk["bs_stock_id"] = $company->cmp_stock_id;
                    $branchStk["bs_prd_id"] = $prd_id;
                    $branchStk["bs_prate"] = $rate;
                    $branchStk["bs_avg_prate"] = $rate;
                    $branchStk["bs_srate"] = 0;
                    $branchStk["bs_branch_id"] = $to_branch;
                    $branchStk["bs_stock_quantity"] = $qty;
                    $branchStk["bs_stock_quantity_shop"] = $qty;
                    $branchStk["bs_stock_quantity_gd"] = 0;
                    $branchStk["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);
                    $bstocks = BranchStocks::create($branchStk);



                    $ds['sdu_branch_stock_id'] = $bstocks->branch_stock_id;
                    $ds['sdu_stock_id'] =$company->cmp_stock_id;
                    $ds['sdu_prd_id'] = $prd_id;
                    $ds['sdu_date'] = date('Y-m-d');
                    $ds['sdu_stock_quantity'] = $qty;
                    $ds['sdu_gd_id'] = 0;
                    $ds['sdu_batch_id'] = 0;
                    $ds['branch_id'] = $to_branch;
                    $ds['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                    StockDailyUpdates::create($ds);


                }

            }

        }

        $update['stocktr_accepted'] = $request->status;
        $update['stocktr_accepted_by'] = $request->usr_id;
        $update['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
        BranchTransfer::where('stocktr_id', $request->stocktr_id)->update($update);

        $update2['stocktrsub_accepted'] = $request->status;
        $update2['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
        BranchTransferSub::where('stocktrsub_stocktr_id', $request->stocktr_id)->where('stocktrsub_flags', 1)->update($update2);
        // for voucher entry
        app('App\Http\Controllers\Api\Migration\ledgerOpeingbalanceController')->stocktransfertovoucher($request->stocktr_id);
        
        return response()->json(['message' => ($request->status == 1) ? 'Transfer Accepted' : 'Transfer Rejected', 'status' => $this->created_stat]);
    }

    public function branchTransfer(Request $request)
    {

        $input = $request->all();

        $next_id =  BranchTransfer::get()->count() + 1;
        $stock_transfer['stocktr_id'] = $next_id;
        $stock_transfer['stocktr_date'] = date('Y-m-d', strtotime($input['tran_date']));
        $stock_transfer['stocktr_amount'] = $input['total_amount'];
        $stock_transfer['stocktr_notes'] = isset($input['stocktr_notes']) ? $input['stocktr_notes'] : '';
        $stock_transfer['stocktr_to'] = $input['branch_to'];
        $stock_transfer['stocktr_ledger_id'] = 0;
        $stock_transfer['branch_id'] = $this->branch_id;
        $stock_transfer['stocktr_added_by'] = $this->usr_id;
        $stock_transfer['server_sync_time'] = $this->server_sync_time;
        $last_insert = BranchTransfer::create($stock_transfer);

        // $items = array_reverse($input['items']);

        foreach ($input['items'] as $key => $sub_array) {

            $stock_transfer_sub['stocktrsub_id'] = BranchTransferSub::get()->count() + 1;
            $stock_transfer_sub['stocktrsub_stocktr_id'] = $next_id;
            $stock_transfer_sub['stocktrsub_prd_id'] = $sub_array['prd_id'];
            $stock_transfer_sub['stocktrsub_branch_stock_id'] = $sub_array['vantransub_branch_stock_id'];
            $stock_transfer_sub['stocktrsub_qty'] = $sub_array['vantransub_qty'];
            $stock_transfer_sub['stocktrsub_gd_id'] = $sub_array['gd_id'];
            $stock_transfer_sub['stocktrsub_rate'] = $sub_array['purchase_rate'];
            $stock_transfer_sub['stocktrsub_prate'] = $sub_array['vantransub_purch_rate'];
            $stock_transfer_sub['stocktrsub_date'] = date('Y-m-d', strtotime($input['tran_date']));
            $stock_transfer_sub['stocktrsub_unit_id'] = $sub_array['vantransub_unit_id'];
            $stock_transfer_sub['branch_id'] = $this->branch_id;
            $stock_transfer_sub['stocktrsub_to'] = $input['branch_to'];

            $stock_transfer_sub['stocktrsub_added_by'] = $this->usr_id;
            $stock_transfer_sub['stocktrsub_accepted'] = 0;

            $stock_transfer_sub['server_sync_time'] = $this->server_sync_time;
            $stock_transfer_sub['stocktrsub_flags'] = 1;



            BranchTransferSub::create($stock_transfer_sub);

            if ($sub_array['gd_id'] > 0) {
                $column = "bs_stock_quantity_gd";

                $this->deductQunatityFromGodown($input['tran_date'], $sub_array['gd_id'], $sub_array['vantransub_branch_stock_id'], $sub_array['prd_id'], $sub_array['vantransub_qty']);
            } else {
                $column = "bs_stock_quantity_shop";
            }
            $change = $sub_array['vantransub_qty'];
            $column2 = "bs_stock_quantity";

            BranchStocks::where('branch_stock_id', $sub_array['vantransub_branch_stock_id'])
                ->update([
                    $column => \DB::raw("$column -$change"),
                    $column2 => \DB::raw("$column2 -$change"),
                    'server_sync_time' => $this->server_sync_time,
                ]);

            $dateExist = StockDailyUpdates::select('*')->where('sdu_branch_stock_id', $sub_array['vantransub_branch_stock_id'])
                ->where('sdu_date', $stock_transfer['stocktr_date'])->first();

            if ($dateExist) {

                $column_name = 'sdu_stock_quantity';
                StockDailyUpdates::where('sdu_branch_stock_id', $sub_array['vantransub_branch_stock_id'])
                    ->where('sdu_date', $stock_transfer['stocktr_date'])->update([
                    $column_name => \DB::raw("$column_name -$change"),
                    'server_sync_time' => $this->server_sync_time,
                ]);

            } else {
                $ds['sdu_branch_stock_id'] = $sub_array['vantransub_branch_stock_id'];
                $ds['sdu_stock_id'] = $sub_array['vantransub_stock_id'];
                $ds['sdu_prd_id'] = $sub_array['vantransub_prod_id'];
                $ds['sdu_date'] = $stock_transfer['stocktr_date'];
                $ds['sdu_stock_quantity'] = (0 - $change);
                $ds['sdu_gd_id'] = 0;
                $ds['sdu_batch_id'] = 0;
                $ds['branch_id'] = $this->branch_id;
                $ds['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                StockDailyUpdates::create($ds);

            }

        }
        return response()->json(['cat_id'=>$next_id,'message' => 'Transfered  Successfully', 'status' => $this->created_stat]);
    }

    public function deductQunatityFromGodown($date, $gd_id, $branch_stock_id, $prd_id, $qty)
    {

        $whr['gs_godown_id'] = $gd_id;
        $whr['gs_branch_stock_id'] = $branch_stock_id;
        $whr['gs_prd_id'] = $prd_id;
        $whr['gs_flag'] = 1;

        $gstock = GodownStock::where($whr)->first();

        // $gdLog['gsl_godown_id'] = $gd_id;
        $gdLog['gsl_branch_stock_id'] = $branch_stock_id;
        $gdLog['gsl_stock_id'] = $prd_id;
        $gdLog['gsl_prd_id'] = $prd_id;
        $gdLog['gsl_prod_unit'] = 0;
        $gdLog['gsl_qty'] = $qty * -1;
        $gdLog['gsl_from'] = 0;
        $gdLog['gsl_to'] = $gd_id;
        $gdLog['gsl_tran_vanid'] = 0;
        $gdLog['gsl_vchr_type'] = 30;
        $gdLog['gsl_date'] = $date;
        $gdLog['gsl_added_by'] = $this->usr_id;
        $gdLog['branch_id'] = $this->branch_id;
        $gdLog['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);

        if (!empty($gstock)) {
            $gstock['gs_qty'] = $gstock['gs_qty'] - $qty;
            $gstock['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            $gstock->save();

            $gdLog['gsl_gdwn_stock_id'] = $gstock->gs_id;
        } else {
            $gds = GodownStock::create([

                'gs_godown_id' => $gd_id,
                'gs_branch_stock_id' => $branch_stock_id,
                'gs_stock_id' => $prd_id,
                'gs_date' => $date,
                'gs_prd_id' => $prd_id,
                'gs_qty' => $qty * -1,
                'branch_id' => $this->branch_id,
                'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6),

            ]);
            $gdLog['gsl_gdwn_stock_id'] = $gds->gs_id;
        }

        GodownStockLog::create($gdLog);
    }

    public function addQunatityFromGodown($date, $gd_id, $branch_stock_id, $prd_id, $qty)
    {

        $whr['gs_godown_id'] = $gd_id;
        $whr['gs_branch_stock_id'] = $branch_stock_id;
        $whr['gs_prd_id'] = $prd_id;
        $whr['gs_flag'] = 1;

        $gstock = GodownStock::where($whr)->first();

        // $stockLog['gsl_godown_id'] = $gd_id;
        $gdLog['gsl_branch_stock_id'] = $branch_stock_id;
        $gdLog['gsl_stock_id'] = $prd_id;
        $gdLog['gsl_prd_id'] = $prd_id;
        $gdLog['gsl_prod_unit'] = 0;
        $gdLog['gsl_qty'] = $qty;
        $gdLog['gsl_from'] = 0;
        $gdLog['gsl_to'] = $gd_id;
        $gdLog['gsl_tran_vanid'] = 0;
        $gdLog['gsl_vchr_type'] = 30;
        $gdLog['gsl_date'] = $date;
        $gdLog['gsl_added_by'] = $this->usr_id;
        $gdLog['branch_id'] = $this->branch_id;
        $gdLog['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);

        if (!empty($gstock)) {
            $gstock['gs_qty'] = $gstock['gs_qty'] + $qty;
            $gstock['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            $gstock->save();

            $stockLog['gsl_gdwn_stock_id'] = $gstock->gs_id;
        } else {
            $gds = GodownStock::create([

                'gs_godown_id' => $gd_id,
                'gs_branch_stock_id' => $branch_stock_id,
                'gs_stock_id' => $prd_id,
                'gs_date' => $date,
                'gs_prd_id' => $prd_id,
                'gs_qty' => $qty,
                'branch_id' => $this->branch_id,
                'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6),

            ]);
            $stockLog['gsl_gdwn_stock_id'] = $gds->gs_id;
        }

        GodownStockLog::create($stockLog);
    }

    public function transferpreview(Request $request)
    {

        $amnt = DB::raw('(stocktrsub_rate * stocktrsub_qty) as purchase_amount');
        $this->inrSlct = ['stocktrsub_prd_id', 'stocktrsub_branch_stock_id', 'stocktrsub_qty', 'stocktrsub_rate',
            'stocktrsub_prate', 'stocktrsub_unit_id', $amnt];

        $this->stocktrsub_stocktr_id = $request->prdn_id;
        $rltn = [
            'items' => function ($qr) {
                $qr->select('*')->where('stocktrsub_flags', 1)->where('stocktrsub_stocktr_id', $this->stocktrsub_stocktr_id);
            },
            'branch' => function ($qr) {
                $qr->select('branch_id', 'branch_name', 'branch_display_name', 'branch_code', 'branch_address', 'branch_phone', 'branch_mob', 'branch_tin', 'branch_reg_no');
            },
            'frombranch' => function ($qr) {
                $qr->select('branch_id', 'branch_name', 'branch_display_name', 'branch_code', 'branch_address', 'branch_phone', 'branch_mob', 'branch_tin', 'branch_reg_no');
            },
        ];
        $qry = BranchTransfer::select('*')->where('stocktr_id', $request->prdn_id)->with($rltn);

        $data = $qry->first()->toArray();

        foreach ($data['items'] as $k => $item) {

            $dd = Product::select('prd_name', 'prd_barcode')
                ->where('prd_id', $item['stocktrsub_prd_id'])->first();
            $data['items'][$k]['sl_no'] = $k + 1;
            $data['items'][$k]['prd_name'] = $dd['prd_name'];
            $data['items'][$k]['prd_barcode'] = $dd['prd_barcode'];

            $data['items'][$k]['unit_code'] = Units::where('unit_id', $item['stocktrsub_unit_id'])->pluck('unit_code');

        }
        $data['from_branch'] = $data['frombranch']['branch_name'];
        $data['to_branch'] = $data['branch']['branch_name'];

        $data['stocktr_num'] = $data['stocktr_id'];

        return $data;
    }

    public function transferList(Request $request)
    {

        $amnt = DB::raw('(stocktrsub_rate * stocktrsub_qty) as purchase_amount');
        $this->inrSlct = ['stocktrsub_prd_id', 'stocktrsub_branch_stock_id', 'stocktrsub_qty', 'stocktrsub_rate',
            'stocktrsub_prate', 'stocktrsub_unit_id', $amnt];
        $rltn = [
            'items' => function ($qr) {
                $qr->select('*');
            },
            'branch' => function ($qr) {
                $qr->select('branch_id', 'branch_name', 'branch_display_name', 'branch_code', 'branch_address', 'branch_phone', 'branch_mob', 'branch_tin', 'branch_reg_no');
            },
            'frombranch' => function ($qr) {
                $qr->select('branch_id', 'branch_name', 'branch_display_name', 'branch_code', 'branch_address', 'branch_phone', 'branch_mob', 'branch_tin', 'branch_reg_no');
            },
        ];
        $qry = BranchTransfer::select('*')->where('branch_id', $this->branch_id)
            ->where('stocktr_flags', 1)->with($rltn);

        $data = $qry->orderBy('stocktr_id', 'DESC')->paginate($this->per_page)->toArray();

        foreach ($data['data'] as $t => $transfer) {

            foreach ($transfer['items'] as $k => $item) {
                //
                $dd = Product::select('prd_name', 'prd_barcode')
                    ->where('prd_id', $item['stocktrsub_prd_id'])->first();
                $data['data'][$t]['items'][$k]['prd_name'] = $dd['prd_name'];
                $data['data'][$t]['items'][$k]['prd_barcode'] = $dd['prd_barcode'];

            }

        }
        return $data;
    }

    public function sendList(Request $request)
    {
        $amnt = DB::raw('(stocktrsub_rate * stocktrsub_qty) as purchase_amount');
        $this->inrSlct = ['stocktrsub_prd_id', 'stocktrsub_branch_stock_id', 'stocktrsub_qty', 'stocktrsub_rate',
            'stocktrsub_prate', 'stocktrsub_unit_id', $amnt];
        $rltn = [
            'items' => function ($qr) {
                $qr->select('*');
            },
            'branch' => function ($qr) {
                $qr->select('branch_id', 'branch_name', 'branch_display_name', 'branch_code', 'branch_address', 'branch_phone', 'branch_mob', 'branch_tin', 'branch_reg_no');
            },
            'frombranch' => function ($qr) {
                $qr->select('branch_id', 'branch_name', 'branch_display_name', 'branch_code', 'branch_address', 'branch_phone', 'branch_mob', 'branch_tin', 'branch_reg_no');
            },
        ];
        $qry = BranchTransfer::select('*')->where('stocktr_accepted', $request->status)
            ->where('stocktr_flags', 1)
            ->where('branch_id', $this->branch_id)->with($rltn);

        $data = $qry->orderBy('stocktr_id', 'DESC')->paginate($this->per_page)->toArray();

        foreach ($data['data'] as $t => $transfer) {

            foreach ($transfer['items'] as $k => $item) {
                //
                $dd = Product::select('prd_name', 'prd_barcode')
                    ->where('prd_id', $item['stocktrsub_prd_id'])->first();
                $data['data'][$t]['items'][$k]['prd_name'] = $dd['prd_name'];
                $data['data'][$t]['items'][$k]['prd_barcode'] = $dd['prd_barcode'];

            }

        }
        return $data;

    }

    public function sendListSummary(Request $rq)
    {
       
        $this->inrSlct = ['stocktrsub_prd_id', 'stocktrsub_branch_stock_id', 'stocktrsub_qty', 'stocktrsub_rate',
            'stocktrsub_prate', 'stocktrsub_unit_id'];
        $rltn = [
            'items' => function ($qr) {
                $qr->select('*');
            },
            'branch' => function ($qr) {
                $qr->select('branch_id', 'branch_name', 'branch_display_name', 'branch_code', 'branch_address', 'branch_phone', 'branch_mob', 'branch_tin', 'branch_reg_no');
            },
            'frombranch' => function ($qr) {
                $qr->select('branch_id', 'branch_name', 'branch_display_name', 'branch_code', 'branch_address', 'branch_phone', 'branch_mob', 'branch_tin', 'branch_reg_no');
            },
        ];
        $qry = BranchTransfer::select('*')->where('stocktr_accepted', 1)
            ->where('stocktr_flags', 1)
            ->where('branch_id', $this->branch_id)->with($rltn);

            if ($ptype = isset($rq['period_type']) ? $rq['period_type'] : 0) {
                list($date1, $date2) = $this->datesFromPeriods($ptype, $rq);
    
                if ($date1 && $date2) {
                    $qry = $qry->whereBetween('stocktr_date', [$date1, $date2]);
                }
            }

            $inv_filter = isset($rq['inv_filter'])?$rq['inv_filter']:0;
            $inv_val2 = isset($rq['inv_val1'])?$rq['inv_val1']:0;
            $inv_val1 = isset($rq['inv_val'])?$rq['inv_val']:0;
           
            $added_by = isset($rq['added_by'])?$rq['added_by']:0;

            if($added_by)
            {
                $qry = $qry->where('stocktr_added_by', $added_by);
            }

        if ($inv_filter && $inv_val1) {
            switch ($inv_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $qry = $qry->where('stocktr_id', $inv_filter, $inv_val1);
                    break;
                case 'between':
                    if ($inv_val2) {
                        $qry = $qry->whereBetween('stocktr_id', [$inv_val1, $inv_val2]);
                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }

        $data = $qry->orderBy('stocktr_id', 'DESC')->paginate(1000)->toArray();
        $amount =0;
        foreach ($data['data'] as $t => $transfer) {

            $amount += $transfer['stocktr_amount'];
            //$data['data']['voucher_no']  ='';
            //Acc_voucher::where('branch_ref_no',$transfer['stocktr_id'])->where('vch_vchtype_id',18)->pluck('vch_no');
            foreach ($transfer['items'] as $k => $item) {
                //
                $dd = Product::select('prd_name', 'prd_barcode')
                    ->where('prd_id', $item['stocktrsub_prd_id'])->first();
                $data['data'][$t]['items'][$k]['prd_name'] = $dd['prd_name'];
                $data['data'][$t]['items'][$k]['prd_barcode'] = $dd['prd_barcode'];

            }

        }

        $data['total_amount'] =$amount;
        return $data;

    }

    public function receivedListSummary(Request $rq)
    {
        $amnt = DB::raw('(stocktrsub_rate * stocktrsub_qty) as purchase_amount');
        $this->inrSlct = ['stocktrsub_prd_id', 'stocktrsub_branch_stock_id', 'stocktrsub_qty', 'stocktrsub_rate',
            'stocktrsub_prate', 'stocktrsub_unit_id', $amnt];
        $rltn = [
            'items' => function ($qr) {
                $qr->select('*');
            },
            'branch' => function ($qr) {
                $qr->select('branch_id', 'branch_name', 'branch_display_name', 'branch_code', 'branch_address', 'branch_phone', 'branch_mob', 'branch_tin', 'branch_reg_no');
            },
            'frombranch' => function ($qr) {
                $qr->select('branch_id', 'branch_name', 'branch_display_name', 'branch_code', 'branch_address', 'branch_phone', 'branch_mob', 'branch_tin', 'branch_reg_no');
            },
        ];
        $qry = BranchTransfer::select('*')->where('stocktr_accepted', 1)
            ->where('stocktr_flags', 1)
            ->where('stocktr_to', $this->branch_id)->with($rltn);

            if ($ptype = isset($rq['period_type']) ? $rq['period_type'] : 0) {
                list($date1, $date2) = $this->datesFromPeriods($ptype, $rq);
    
                if ($date1 && $date2) {
                    $qry = $qry->whereBetween('stocktr_date', [$date1, $date2]);
                }
            }

            $inv_filter = isset($rq['inv_filter'])?$rq['inv_filter']:0;
            $inv_val2 = isset($rq['inv_val1'])?$rq['inv_val1']:0;
            $inv_val1 = isset($rq['inv_val'])?$rq['inv_val']:0;
           
            $added_by = isset($rq['added_by'])?$rq['added_by']:0;

            if($added_by)
            {
                $qry = $qry->where('stocktr_accepted_by', $added_by);
            }
            
        if ($inv_filter && $inv_val1) {
            switch ($inv_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $qry = $qry->where('stocktr_id', $inv_filter, $inv_val1);
                    break;
                case 'between':
                    if ($inv_val2) {
                        $qry = $qry->whereBetween('stocktr_id', [$inv_val1, $inv_val2]);
                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }

        $data = $qry->orderBy('stocktr_id', 'DESC')->paginate(1000)->toArray();
        $amount =0;
        foreach ($data['data'] as $t => $transfer) {

            $amount += $transfer['stocktr_amount'];
            //$data['data']['voucher_no']  ='';
            //Acc_voucher::where('branch_ref_no',$transfer['stocktr_id'])->where('vch_vchtype_id',18)->pluck('vch_no');
            foreach ($transfer['items'] as $k => $item) {
                //
                $dd = Product::select('prd_name', 'prd_barcode')
                    ->where('prd_id', $item['stocktrsub_prd_id'])->first();
                $data['data'][$t]['items'][$k]['prd_name'] = $dd['prd_name'];
                $data['data'][$t]['items'][$k]['prd_barcode'] = $dd['prd_barcode'];

            }

        }

        $data['total_amount'] =$amount;
        return $data;

    }
    public function datesFromPeriods($pt, $rq)
    {
        switch ($pt) {

            case 't':
                $date1 = $date2 = date('Y-m-d');
                break;

            case 'ld':
                $date1 = $date2 = date('Y-m-d', strtotime("-1 days"));
                break;

            case 'lw':
                $previous_week = strtotime("-1 week +1 day");
                $start_week = strtotime("last sunday midnight", $previous_week);
                $end_week = strtotime("next saturday", $start_week);
                $date1 = date("Y-m-d", $start_week);
                $date2 = date("Y-m-d", $end_week);
                break;

            case 'lm':
                $date1 = date("Y-m-d", strtotime("first day of previous month"));
                $date2 = date("Y-m-d", strtotime("last day of previous month"));
                break;

            case 'ly':
                $year = date('Y') - 1;
                $date1 = $year . "-01-01";
                $date2 = $year . "-12-31";
                break;

            case 'c':
                $date1 = isset($rq['date1']) ? date('Y-m-d', strtotime($rq['date1'])) : 0;
                $date2 = isset($rq['date2']) ? date('Y-m-d', strtotime($rq['date2'])) : 0;

                break;

            default:
                return false;
                break;
        }
        return [$date1, $date2];
    }
    public function transferReceived(Request $request)
    {

        $amnt = DB::raw('(stocktrsub_rate * stocktrsub_qty) as purchase_amount');
        $this->inrSlct = ['stocktrsub_prd_id', 'stocktrsub_branch_stock_id', 'stocktrsub_qty', 'stocktrsub_rate',
            'stocktrsub_prate', 'stocktrsub_unit_id', $amnt];
        $rltn = [
            'items' => function ($qr) {
                $qr->select('*');
            },
            'branch' => function ($qr) {
                $qr->select('branch_id', 'branch_name', 'branch_display_name', 'branch_code', 'branch_address', 'branch_phone', 'branch_mob', 'branch_tin', 'branch_reg_no');
            },
            'frombranch' => function ($qr) {
                $qr->select('branch_id', 'branch_name', 'branch_display_name', 'branch_code', 'branch_address', 'branch_phone', 'branch_mob', 'branch_tin', 'branch_reg_no');
            },
        ];
        $qry = BranchTransfer::select('*')->where('stocktr_accepted', $request->status)
            ->where('stocktr_flags', 1)->where('stocktr_to', $this->branch_id)->with($rltn);

        $data = $qry->orderBy('stocktr_id', 'DESC')->paginate($this->per_page)->toArray();

        foreach ($data['data'] as $t => $transfer) {

            foreach ($transfer['items'] as $k => $item) {
                //
                $dd = Product::select('prd_name', 'prd_barcode')
                    ->where('prd_id', $item['stocktrsub_prd_id'])->first();
                $data['data'][$t]['items'][$k]['prd_name'] = $dd['prd_name'];
                $data['data'][$t]['items'][$k]['prd_barcode'] = $dd['prd_barcode'];

            }

        }
        return $data;
    }
    public function serachProduct(Request $rq)
    {

        $results = [];
        if ($rq->keyword || $rq->brcode) {
            $this->key = $rq->keyword;
            $this->barcode = $rq->brcode;
            $results = Product::select('prd_id', 'prd_barcode', 'prd_name', 'prd_alias', 'prd_base_unit_id', 'branch_stock_id', 'bs_stock_id', 'bs_prate', 'bs_avg_prate', 'bs_srate', 'bs_stock_quantity', 'bs_stock_quantity_shop', 'bs_stock_quantity_gd', 'bs_stock_quantity_van')

                ->Join('branch_stocks', function ($join) {
                    $join->on('products.prd_id', 'branch_stocks.bs_prd_id')
                        ->where('bs_branch_id', $this->branch_id)
                        ->where('prd_stock_status', 1);
                    if ($this->key) {
                        $join->where('prd_name', 'like', '%' . $this->key . '%');
                    } else {
                        $join->where('prd_barcode', $this->barcode);
                    }

                })->groupBy('products.prd_id')->take(10)->get();
            if (count($results) == 0) {
                $product_name = ProdUnit::where('produnit_ean_barcode', $this->barcode)->join('products', 'prod_units.produnit_prod_id', '=', 'products.prd_id')->pluck('products.prd_name');

                if (count($product_name) > 0) {
                    $results = Product::select('prd_id', 'prd_barcode', 'prd_name', 'prd_alias', 'prd_base_unit_id', 'branch_stock_id', 'bs_stock_id', 'bs_prate', 'bs_avg_prate', 'bs_stock_quantity', 'bs_stock_quantity_shop', 'bs_stock_quantity_gd', 'bs_stock_quantity_van')

                        ->Join('branch_stocks', function ($join) use ($product_name) {
                            $join->on('products.prd_id', 'branch_stocks.bs_prd_id')
                                ->where('bs_branch_id', $this->branch_id)
                                ->where('prd_stock_status', 1);
                            if ($this->key) {
                                $join->where('prd_name', 'like', '%' . $this->key . '%');
                            } else {
                                $join->where('prd_name', $product_name[0]);
                            }

                        })->groupBy('products.prd_id')->take(10)->get();
                }

            }
            //exit;
            foreach ($results as $k => $row) {

                $units = ProdUnit::select('prod_units.produnit_unit_id as sur_unit_id', 'units.unit_base_qty', 'units.unit_name', 'units.unit_code')->join('units', 'units.unit_id', 'prod_units.produnit_unit_id')->where('prod_units.produnit_prod_id', $row['prd_id'])->get();
                $results[$k]['prd_units'] = $units;
                $results[$k]['gdstock'] = GodownStock::select('godown_stocks.gs_godown_id as gd_id', 'godown_stocks.gs_qty', 'godown_master.gd_name')->join('godown_master', 'godown_master.gd_id', 'godown_stocks.gs_godown_id')->where('godown_stocks.gs_branch_stock_id', $row['branch_stock_id'])->get();
                foreach ($units as $k2 => $val) {
                    if ($val['unit_base_qty'] == 1) {
                        $base_unit_name = $val['unit_name'];
                        $base_unit_code = $val['unit_code'];
                    }
                }
                $results[$k]['base_unit_name'] = $base_unit_name;
                $results[$k]['base_unit_code'] = $base_unit_code;
            }
        }
        return parent::displayData($results);
    }

    public function listTransferInfo(Request $request)
    {

        $results = Product::select('prd_id', 'prd_barcode', 'prd_name', 'prd_alias', 'prd_base_unit_id',
            'vantransub_purch_rate', 'vantransub_qty',
            'vantransub_prod_id', 'vantransub_stock_id', 'vantransub_branch_stock_id')

            ->Join('van_transfer_sub', function ($join) {
                $join->on('products.prd_id', 'van_transfer_sub.vantransub_prod_id')
                    ->where('van_transfer_sub.branch_id', $this->branch_id)
                    ->where('vantransub_van_id', $request->van_id)
                    ->where('prd_name', 'like', '%' . $request->keyword . '%');

            })->take(10)->get()->toArray();
    }

    // $column = "bs_stock_quantity_shop";
    // $column2 = "bs_stock_quantity";

    public function transferVoid(Request $request)
    {
        $out = BranchTransfer::where('stocktr_id', $request->tran_id)->first();

        if ($out->stocktr_accepted == '1') {
            return response()->json(['message' => 'Transfer  Alreday accepted by branch', 'status' => $this->created_stat]);
        }

        $sub_items = BranchTransferSub::where('stocktrsub_stocktr_id', $request->tran_id)->get()->toArray();

        foreach ($sub_items as $sub_array) {

            $stocktrsub_date = $sub_array['stocktrsub_date'];
            $stocktrsub_branch_stock_id = $sub_array['stocktrsub_branch_stock_id'];

            $prd_id = $sub_array['stocktrsub_prd_id'];
            $from_branch = $sub_array['branch_id'];
            $rate = $sub_array['stocktrsub_rate'];
            $qty = $sub_array['stocktrsub_qty'];


            if($sub_array['stocktrsub_gd_id']>0)
            {
                
                $this->addQunatityFromGodown($stocktrsub_date,$sub_array['stocktrsub_gd_id'],$sub_array['stocktrsub_branch_stock_id'],$sub_array['stocktrsub_prd_id'],$sub_array['stocktrsub_qty']);
            }

            $where = ['bs_prd_id' => $prd_id, 'bs_branch_id' => $from_branch];

            $bs = BranchStocks::where($where)->first();

            if ($bs->bs_prd_id) {

                $old_p_amount = $bs->bs_stock_quantity * $bs->bs_avg_prate;
                $new_p_amount = $qty * $rate;

                if ($new_p_amount > 0 && ($bs->bs_stock_quantity - $qty) > 0) {
                    $bs_avg_prate = ($old_p_amount - $new_p_amount) / ($bs->bs_stock_quantity - $qty);
                } else {
                    $bs_avg_prate = $bs->bs_avg_prate;
                }

                BranchStocks::where($where)->update([
                    'bs_stock_quantity_shop' => \DB::raw("bs_stock_quantity_shop+$qty"),
                    'bs_stock_quantity' => \DB::raw("bs_stock_quantity+$qty"),
                    'bs_avg_prate' => $bs_avg_prate,
                    'bs_prate' => $rate,
                    'server_sync_time' => $this->server_sync_time,
                ]);

                $dateExist = StockDailyUpdates::select('*')->where('sdu_branch_stock_id', $stocktrsub_branch_stock_id)
                    ->where('sdu_date', $stocktrsub_date)->first();

                if ($dateExist) {

                    $column_name = 'sdu_stock_quantity';
                    StockDailyUpdates::where('sdu_branch_stock_id', $stocktrsub_branch_stock_id)
                        ->where('sdu_date', $stocktrsub_date)->update([
                        $column_name => \DB::raw("$column_name+$qty"),
                        'server_sync_time' => $this->server_sync_time,
                    ]);

                }

            }
        }

        $update['stocktr_flags'] = 0;
        $update['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
        BranchTransfer::where('stocktr_id', $request->tran_id)->update($update);

        $update2['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
        $update2['stocktrsub_flags'] = 0;
        BranchTransferSub::where('stocktrsub_stocktr_id', $request->tran_id)->update($update2);

        return response()->json(['message' => 'Reverted Transfer  Successfully', 'status' => $this->created_stat]);

    }

    public function updateTransfer(Request $request)
    {
        $out = BranchTransfer::where('stocktr_id', $request->stocktr_id)->first();

        if ($out->stocktr_accepted == '1') {
            return response()->json(['message' => 'Transfer  Alreday accepted by branch', 'status' => $this->created_stat]);
        }

        $sub_items = BranchTransferSub::where('stocktrsub_flags', 1)->
            where('stocktrsub_stocktr_id', $request->stocktr_id)->get()->toArray();

        foreach ($sub_items as $sub_array) {

            $stocktrsub_date = $sub_array['stocktrsub_date'];
            $stocktrsub_branch_stock_id = $sub_array['stocktrsub_branch_stock_id'];

            $prd_id = $sub_array['stocktrsub_prd_id'];
            $from_branch = $sub_array['branch_id'];
            $rate = $sub_array['stocktrsub_rate'];
            $qty = $sub_array['stocktrsub_qty'];

            $where = ['bs_prd_id' => $prd_id, 'bs_branch_id' => $from_branch];

            $bs = BranchStocks::where($where)->first();

            if ($bs->bs_prd_id) {

                $old_p_amount = $bs->bs_stock_quantity * $bs->bs_avg_prate;
                $new_p_amount = $qty * $rate;

                if ($new_p_amount > 0 && ($bs->bs_stock_quantity - $qty) > 0) {
                    $bs_avg_prate = ($old_p_amount - $new_p_amount) / ($bs->bs_stock_quantity - $qty);
                } else {
                    $bs_avg_prate = $bs->bs_avg_prate;
                }

                BranchStocks::where($where)->update([
                    'bs_stock_quantity_shop' => \DB::raw("bs_stock_quantity_shop+$qty"),
                    'bs_stock_quantity' => \DB::raw("bs_stock_quantity+$qty"),
                    'bs_avg_prate' => $bs_avg_prate,
                    'bs_prate' => $rate,
                    'server_sync_time' => $this->server_sync_time,
                ]);

                $dateExist = StockDailyUpdates::select('*')->where('sdu_branch_stock_id', $stocktrsub_branch_stock_id)
                    ->where('sdu_date', $stocktrsub_date)->first();

                if ($dateExist) {

                    $column_name = 'sdu_stock_quantity';
                    StockDailyUpdates::where('sdu_branch_stock_id', $stocktrsub_branch_stock_id)
                        ->where('sdu_date', $stocktrsub_date)->update([
                        $column_name => \DB::raw("$column_name+$qty"),
                        'server_sync_time' => $this->server_sync_time,
                    ]);

                }

            }
        }

        $update['stocktr_flags'] = -1;
        $update['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
        BranchTransfer::where('stocktr_id', $request->stocktr_id)->update($update);

        $update2['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
        $update2['stocktrsub_flags'] = -1;
        BranchTransferSub::where('stocktrsub_stocktr_id', $request->stocktr_id)->update($update2);

        $this->addNewTransfer($request);
        return response()->json(['message' => 'Transfered  Updated  Successfully', 'status' => $this->created_stat]);
    }

    public function addNewTransfer($request)
    {

        $input = $request->all();

        $stock_transfer['stocktr_id'] = BranchTransfer::get()->count() + 1;
        $stock_transfer['stocktr_date'] = date('Y-m-d', strtotime($input['tran_date']));
        $stock_transfer['stocktr_amount'] = $input['stocktr_amount'];
        $stock_transfer['stocktr_notes'] = isset($input['stocktr_notes']) ? $input['stocktr_notes'] : '';
        $stock_transfer['stocktr_to'] = $input['branch_to'];
        $stock_transfer['stocktr_ledger_id'] = 0;
        $stock_transfer['branch_id'] = $this->branch_id;
        $stock_transfer['stocktr_added_by'] = $this->usr_id;
        $stock_transfer['server_sync_time'] = $this->server_sync_time;
        $last_insert = BranchTransfer::create($stock_transfer);

        foreach ($input['items'] as $key => $sub_array) {

            $stock_transfer_sub['stocktrsub_id'] = BranchTransferSub::get()->count() + 1;
            $stock_transfer_sub['stocktrsub_stocktr_id'] = $last_insert['stocktr_id'];
            $stock_transfer_sub['stocktrsub_prd_id'] = isset($sub_array['prd_id']) ? $sub_array['prd_id'] : $sub_array['stocktrsub_prd_id'];
            $stock_transfer_sub['stocktrsub_branch_stock_id'] = isset($sub_array['vantransub_branch_stock_id']) ? $sub_array['vantransub_branch_stock_id'] : $sub_array['stocktrsub_branch_stock_id'];
            $stock_transfer_sub['stocktrsub_qty'] = $sub_array['stocktrsub_qty'];
            $stock_transfer_sub['stocktrsub_rate'] = $sub_array['stocktrsub_rate'];
            $stock_transfer_sub['stocktrsub_prate'] = isset($sub_array['vantransub_purch_rate']) ? $sub_array['vantransub_purch_rate'] : $sub_array['stocktrsub_prate'];
            $stock_transfer_sub['stocktrsub_date'] = date('Y-m-d', strtotime($input['tran_date']));
            $stock_transfer_sub['stocktrsub_unit_id'] = isset($sub_array['vantransub_unit_id']) ? $sub_array['vantransub_unit_id'] : $sub_array['stocktrsub_unit_id'];
            $stock_transfer_sub['branch_id'] = $this->branch_id;
            $stock_transfer_sub['stocktrsub_to'] = $input['branch_to'];

            $stock_transfer_sub['stocktrsub_added_by'] = $this->usr_id;
            $stock_transfer_sub['stocktrsub_accepted'] = 0;

            $stock_transfer_sub['server_sync_time'] = $this->server_sync_time;
            $stock_transfer_sub['stocktrsub_flags'] = 1;
            BranchTransferSub::create($stock_transfer_sub);

            $change = $stock_transfer_sub['stocktrsub_qty'];
            $column = "bs_stock_quantity_shop";
            $column2 = "bs_stock_quantity";

            $info = BranchStocks::where('branch_stock_id', $stock_transfer_sub['stocktrsub_branch_stock_id'])->first();

            $info['bs_stock_quantity_shop'] = $info['bs_stock_quantity_shop'] - $change;
            $info['bs_stock_quantity'] = $info['bs_stock_quantity'] - $change;
            $info['server_sync_time'] = $this->server_sync_time;
            $info->save();
            //    ->update([
            //             'bs_stock_quantity_shop' => \DB::raw("bs_stock_quantity_shop -$change"),
            //             'bs_stock_quantity'=> \DB::raw("bs_stock_quantity -$change"),
            //             'server_sync_time' => $this->server_sync_time,
            //         ]);

            $dateExist = StockDailyUpdates::select('*')->where('sdu_branch_stock_id', $stock_transfer_sub['stocktrsub_branch_stock_id'])
                ->where('sdu_date', $stock_transfer['stocktr_date'])->first();

            if ($dateExist) {

                $column_name = 'sdu_stock_quantity';
                StockDailyUpdates::where('sdu_branch_stock_id', $stock_transfer_sub['stocktrsub_branch_stock_id'])
                    ->where('sdu_date', $stock_transfer['stocktr_date'])->update([
                    $column_name => \DB::raw("sdu_stock_quantity -$change"),
                    'server_sync_time' => $this->server_sync_time,
                ]);

            } else {
                $ds['sdu_branch_stock_id'] = $stock_transfer_sub['stocktrsub_branch_stock_id'];
                $ds['sdu_stock_id'] = isset($sub_array['vantransub_stock_id']) ? $sub_array['vantransub_stock_id'] : 0;
                $ds['sdu_prd_id'] = $stock_transfer_sub['stocktrsub_prd_id'];
                $ds['sdu_date'] = $stock_transfer['stocktr_date'];
                $ds['sdu_stock_quantity'] = (0 - $change);
                $ds['sdu_gd_id'] = 0;
                $ds['sdu_batch_id'] = 0;
                $ds['branch_id'] = $this->branch_id;
                $ds['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                StockDailyUpdates::create($ds);

            }

        }

    }

}
