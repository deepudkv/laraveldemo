<?php
//  created by deepu

namespace App\Http\Controllers\Api\Stock;

use App\Http\Controllers\Api\ApiParent;
use App\Models\Damage\DaMage;
use App\Models\Godown\GodownMaster;
use App\Models\Godown\GodownStock;
use App\Models\Godown\GodownStockLog;
use App\Models\Product\Product;
use App\Models\Product\ProdUnit;
use App\Models\Product\Units;
use App\Models\Purchase\PurchaseSub;
use App\Models\Stocks\BranchStockLogs;
use App\Models\Stocks\BranchStocks;
use App\Models\Stocks\CompanyStocks;
use App\Models\Stocks\OpeningStockLog;
use App\Models\Stocks\StockBatches;
use App\Models\Stocks\StockDailyUpdates;
use App\Models\Stocks\StockUnitRates;
use DB;
use Illuminate\Http\Request;
use Validator;

class StockController extends ApiParent
{

    public function stock(Request $rq, $rq_type)
    {

        switch ($rq_type) {

            case "search":return $this->StockSearch($rq);
                break;

            case "add":return $this->addStock($rq);
                break;

            case "update":return $this->updateStock($rq);
                break;

            case "open_stock":return $this->openStock($rq);
                break;

            case "calc_prate":return $this->calcAvgPrate($rq, $rq);
                break;

            case "search_rates":return $this->searchRates($rq);
                break;

            case "update_qty":return $this->quantityUpdate($rq);
                break;

            case "get_unit_rates_branchvise":return $this->getBranchvise($rq);
                break;

            case "update_sales_rates":return $this->updateSalesRates($rq);
                break;

            case "stock_set":return $this->stockSet($rq);
                break;

            case "stock_search":return $this->stockSearchDamage($rq);
                break;

            case "godown_stock":return $this->getGodownStock($rq);
                break;

            case "add_damage":return $this->addDamage($rq);
                break;

            case "list_damage":return $this->listDamage($rq);
                break;

            default:return response()->json(['error' => 'Invalid Endpoint', 'status' => $this->badrequest_stat]);
                break;
        }
    }

    /**
     * @api {post} http://127.0.0.1:8000/api/stock/update Update Stock
     * @apiName updateStock
     * @apiGroup Stock
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {bigint} prd_id Product id (Required)
     * @apiParam {bigint} stock_id Stock id (Required)
     * @apiParam {int(11)} purchase_rate Product purchase rate
     * @apiParam {array} unit_rates Product's  rate for each units (Required) (Values: int unit_id ,varchar unit_rate)
     * @apiParam {array} branch_ids (Required) ( if adding stock to branches )

     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} message success message
     * @apiError {String} error Error Message.
     */

    public function updateStock($rq)
    {

        $validator = Validator::make(
            $rq->all(),
            [
                'prd_id' => 'required|numeric|exists:company_stocks,cmp_prd_id',
                'stock_id' => 'required|numeric|exists:company_stocks,cmp_stock_id',
                'unit_rates' => 'required',
            ],
            [
                'prd_id.required' => 'Required',
                'stock_id.required' => 'Required',
                'unit_rates.required' => 'Required',
            ]
        );
        if ($validator->fails()) {
            return parent::displayError($validator->messages());
        }

        $cmp_stock_id = CompanyStocks::where('cmp_prd_id', $rq->prd_id)->where('cmp_stock_id', $rq->stock_id)->pluck('cmp_stock_id')->first();
        $unit_rates = is_array($rq->unit_rates) ? $rq->unit_rates : null;

        if ($cmp_stock_id > 0 && !empty($unit_rates)) {
            return $this->updateStockUnitRates($rq, $unit_rates);
        } else {
            return parent::displayError('Stock not exist for the product');
        }
    }

    public function updateStockUnitRates($rq, $unit_rates)
    {

        $whr['bs_branch_id'] = $this->branch_id;
        $whr['bs_stock_id'] = $sr["sur_stock_id"] = $rq->stock_id;
        $whr['bs_prd_id'] = $sr["sur_prd_id"] = $rq->prd_id;

        if ($bsid = BranchStocks::where($whr)->pluck('branch_stock_id')->first()) {

            foreach ($unit_rates as $unit) {

                $unitdata = Units::where('unit_id', $unit["unit_id"])->get()->toArray();
                if ($unitdata[0]['unit_type'] == 1) {
                    $update['bs_srate'] = $unit["unit_rate"];
                    $update['server_sync_time'] = $this->server_sync_time;
                    BranchStocks::where($whr)->update($update);
                }
                $rate_exist = StockUnitRates::where('branch_stock_id', $bsid)
                    ->where('sur_unit_id', $unit["unit_id"])->pluck('branch_stock_id')->first();

                if ($rate_exist) {

                    $sur_id = StockUnitRates::where('branch_stock_id', $bsid)
                        ->where('sur_unit_id', $unit["unit_id"])
                        ->update(['sur_unit_rate' => $unit["unit_rate"],
                            'sur_branch_id' => $this->branch_id, 'server_sync_time' => $this->server_sync_time]);
                } else {

                    $sr["branch_stock_id"] = $bsid;
                    $sr["sur_unit_id"] = $unit['unit_id'];
                    $sr["sur_unit_rate"] = $unit['unit_rate'];
                    $sr["server_sync_time"] = $this->server_sync_time;
                    $sr["sur_branch_id"] = $this->branch_id;
                    StockUnitRates::create($sr);
                }

            }
            return parent::displayMessage('Updated S56');
        } else {
            return parent::displayError('Stock not exist for the product');
        }

        // } else {
        //     // stock rates updation in case if company  is logged in
        //     // adding stock to new branches and updating rates
        //     $default_stock = BranchStocks::select(['bs_stock_id', 'bs_prd_id', 'bs_prate', 'bs_avg_prate'])->where($whr)->first()->toarray();

        //     $branches = $rq->branch_ids;
        //     $branches[] = 0;
        //     $branches = array_unique($branches);

        //     StockUnitRates::where('sur_stock_id', $rq->stock_id)->where('sur_prd_id', $rq->prd_id)->delete();

        //     foreach ($branches as $branch_id) {
        //         $whr['bs_branch_id'] = $sr["sur_branch_id"] = $default_stock["bs_branch_id"] = $branch_id;
        //         $sr["branch_stock_id"] = BranchStocks::where($whr)->pluck('branch_stock_id')->first();

        //         if (!$sr["branch_stock_id"]) {
        //             $ns = BranchStocks::create($default_stock);
        //             $sr["branch_stock_id"] = $ns['branch_stock_id'];
        //         }
        //         foreach ($unit_rates as $unit) {
        //             $sr["sur_unit_id"] = $unit['unit_id'];
        //             $sr["sur_unit_rate"] = $unit['unit_rate'];

        //             $sur_id = StockUnitRates::where('branch_stock_id', $sr["branch_stock_id"])
        //             ->where('sur_unit_id', $sr["sur_unit_id"])
        //             ->where('sur_unit_rate', $sr["sur_unit_rate"])
        //             ->pluck('sur_id')->first();
        //             if (!$sur_id) {
        //                 StockUnitRates::create($sr);
        //             }
        //         }
        //     }
        // }

    }

    public function addStock($rq)
    {

        $validator = $this->validateStockAdd($rq);
        if ($validator->fails()) {
            return parent::displayError($validator->messages());
        }
        $stock_id = CompanyStocks::where('cmp_prd_id', $rq->prd_id)->pluck('cmp_stock_id')->first();
        if ($stock_id > 0) {
            $bs = BranchStocks::where('bs_stock_id', $stock_id)->where('bs_branch_id', $rq->branch_id)->first();
            // if stock not exist add stock to branch
            if (!$bs) {
                $nstock['bs_branch_id'] = $this->branch_id;
                $nstock['bs_stock_id'] = $stock_id;
                $nstock['bs_prd_id'] = $rq->prd_id;
                $nstock['bs_prate'] = $rq->purchase_rate;

                BranchStocks::create($nstock);
                $bs = BranchStocks::where('bs_stock_id', $stock_id)->where('bs_branch_id', $rq->branch_id)->first();
            }
            // end stock adding to  branch
            if (isset($bs->bs_stock_id)) {
                $rq->gd_qty = is_array($rq->gdstock) ? array_sum(array_column($rq->gdstock, 'quantity')) : 0;
                $rq->tot_qty = $rq->shop_quantity + $rq->gd_qty;
                // stock operations starts
                return $this->openStockOperations($rq, $bs);
            }

        } else {
            return parent::displayError('Stock not exist');
        }

    }
    public function validateStockAdd($request)
    {
        return Validator::make(
            $request->all(),
            [
                'prd_id' => 'required|exists:company_stocks,cmp_prd_id',
                'unit_rates' => 'required',
                'shop_quantity' => 'required|numeric',
                'purchase_rate' => 'required|numeric',
            ],
            [
                'prd_id.required' => 'Required',
                'unit_rates.required' => 'Required',
                'shop_quantity.required' => 'Required',
                'shop_quantity.numeric' => 'Invalid Input',
                'purchase_rate.required' => 'Required',
                'purchase_rate.numeric' => 'Invalid Input',
            ]
        );
    }

    /**
     * @api {post} http://127.0.0.1:8000/api/stock/open_stock Open Stock
     * @apiName openStock
     * @apiGroup Stock
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {varchar(500)} prd_id Product id (Required)

     * @apiParam {array} unit_rates Unit id & unit Rates (ex: [ { "unit_id":1 ,  "unit_rate" : 7.26 }, { "unit_id":6 , "unit_rate" : 12.26 }])
     * @apiParam {int} stock_quantity stock quantity
     * @apiParam {double} purchase_rate Purchase Rate
     * @apiParam {int} stock_id Stock Id
     * @apiParam {array} gdstock Qodown Stock Details (ex: [ { "gd_id":1 ,  "quantity" : 7.26 }])
     * @apiParam {date} expiry_date Expiry Date.
     * @apiParam {date} manufacture_date Manufacture Date.

     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} message success message
     * @apiError {String} error Error Message.
     */

    public function openStock($rq)
    {

        $validator = $this->validateOpenStock($rq);
        if ($validator->fails()) {
            return parent::displayError($validator->messages());
        }

        $stock_id = CompanyStocks::where('cmp_prd_id', $rq->prd_id)->where('cmp_stock_id', $rq->stock_id)->pluck('cmp_stock_id');
        $bs = BranchStocks::where('bs_stock_id', $stock_id)->where('bs_branch_id', $rq->branch_id)->first();

        if (isset($bs->bs_stock_id)) {
            $rq->gd_qty = is_array($rq->gdstock) ? array_sum(array_column($rq->gdstock, 'quantity')) : 0;
            $rq->tot_qty = $rq->shop_quantity + $rq->gd_qty;

            // stock operations starts
            return $this->openStockOperations($rq, $bs);

        } else {
            return parent::displayError('Stock not exist');
        }

    }

    public function validateOpenStock($request)
    {
        return Validator::make(
            $request->all(),
            [
                'prd_id' => 'required|exists:company_stocks,cmp_prd_id',
                'stock_id' => 'required|exists:company_stocks,cmp_stock_id',
                'unit_rates' => 'required',
                'shop_quantity' => 'required|numeric',
                'purchase_rate' => 'required|numeric',
            ],
            [
                'prd_id.required' => 'Required',
                'stock_id.required' => 'Required',
                'unit_rates.required' => 'Required',
                'shop_quantity.required' => 'Required',
                'shop_quantity.numeric' => 'Invalid Input',
                'purchase_rate.required' => 'Required',
                'purchase_rate.numeric' => 'Invalid Input',
            ]
        );
    }
    public function openStockOperations($rq, $bs)
    {
        // calcBranchAvgPrate start
        $old_p_amount = $bs->bs_stock_quantity * $bs->bs_avg_prate;
        $new_p_amount = $rq->tot_qty * $rq->purchase_rate;

        if ($new_p_amount > 0) {
            $bs_avg_prate = (abs($old_p_amount) + abs($new_p_amount)) / (abs($bs->bs_stock_quantity) + abs($rq->tot_qty));
        } else {
            $bs_avg_prate = $bs->bs_avg_prate;
        }

        // calcBranchAvgPrate ends

        // updating branch_stocks  and keeping logs - start
        $bs['bs_avg_prate'] = $bs_avg_prate;
        $bs['bs_stock_quantity_shop'] = $bs['bs_stock_quantity_shop'] + $rq->shop_quantity;
        $bs['bs_stock_quantity_gd'] = $bs['bs_stock_quantity_gd'] + $rq->gd_qty;
        $bs['bs_stock_quantity'] = $bs['bs_stock_quantity'] + $rq->tot_qty;
        $bs['bs_prate'] = $rq->purchase_rate;
        $bs->save();

        $bsLog['bsl_branch_stock_id'] = $bs['branch_stock_id'];
        $bsLog['bsl_stock_id'] = $bs['bs_stock_id'];
        $bsLog['bsl_prd_id'] = $bs['bs_prd_id'];
        $bsLog['bsl_shop_quantity'] = $rq->shop_quantity;
        $bsLog['bsl_gd_quantity'] = $rq->tot_qty;
        $bsLog['bsl_prate'] = $rq->purchase_rate;
        $bsLog['bsl_avg_prate'] = 0;
        BranchStockLogs::create($bsLog);
        // updating branch_stocks  and keeping logs ends

        //calcCompanyAvgPrate start
        $company = BranchStocks::where('bs_stock_id', $bs->bs_stock_id)->where('bs_branch_id', 0)->first();
        $old_stockp_amount = $company->tot_qty * $company->bs_prate;
        if ($new_p_amount > 0) {
            $bs_avg_prate2 = (abs($old_stockp_amount) + abs($new_p_amount)) / (abs($company->bs_stock_quantity) + abs($rq->tot_qty));
        } else {
            $bs_avg_prate2 = $company->bs_avg_prate;
        }

        //calcCompanyAvgPrate ends

        // related changes in company stocks
        if ($rq->branch_id != 0) {
            BranchStocks::where('bs_stock_id', $company->bs_stock_id)->where('bs_branch_id', 0)->update(
                [

                    'bs_avg_prate' => $bs_avg_prate2,
                    'bs_stock_quantity_shop' => \DB::raw("bs_stock_quantity_shop+$rq->shop_quantity"),
                    'bs_stock_quantity_gd' => \DB::raw("bs_stock_quantity_gd+$rq->gd_qty"),
                    'bs_stock_quantity' => \DB::raw("bs_stock_quantity+$rq->tot_qty"),
                ]);
        }

        CompanyStocks::where('cmp_stock_id', $company->bs_stock_id)->update(
            [
                'cmp_avg_prate' => $bs_avg_prate2,
                'cmp_stock_quantity' => \DB::raw("cmp_stock_quantity+$rq->tot_qty"),
            ]);
        // related  changes in company stocks ends
        return $this->stockOperations($rq, $bs);

    }
    public function stockOperations($rq, $bs)
    {
        $this->updateInDailyStocks($bs, $rq->tot_qty);
        if (isset($rq->expiry_date) && isset($rq->manufacture_date) && isset($rq->batch_code)) {

            $sb_id = StockBatches::where('sb_branch_stock_id', $bs['branch_stock_id'])
                ->where('sb_batch_code', $rq->batch_code)->pluck('sb_id')->first();
            if ($sb_id) {
                StockBatches::where('sb_id', $sb_id)->update([
                    'sb_manufacture_date' => date('Y-m-d', strtotime($rq->manufacture_date)),
                    'sb_expiry_date' => date('Y-m-d', strtotime($rq->expiry_date)),
                ]);
            } else {
                StockBatches::create([
                    'sb_batch_code' => $rq->batch_code,
                    'sb_branch_stock_id' => $bs['branch_stock_id'],
                    'sb_stock_id' => $bs['bs_stock_id'],
                    'sb_prd_id' => $bs['bs_prd_id'],
                    'sb_manufacture_date' => date('Y-m-d', strtotime($rq->manufacture_date)),
                    'sb_expiry_date' => date('Y-m-d', strtotime($rq->expiry_date)),
                    'branch_id' => $rq->branch_id,
                ]);
            }

        }
        if (is_array($rq->unit_rates) && !empty($rq->unit_rates)) {
            foreach ($rq->unit_rates as $unit) {

                $urate['branch_stock_id'] = $bs['branch_stock_id'];
                $urate['sur_unit_id'] = $unit['unit_id'];

                if ($stckrate = StockUnitRates::select('*')->where($urate)->first()) {
                    $stckrate['sur_unit_rate'] = $unit['unit_rate'];
                    $stckrate->save();
                } else {

                    $urate['sur_prd_id'] = $bs['bs_prd_id'];
                    $urate['sur_stock_id'] = $bs['bs_stock_id'];
                    $urate['sur_unit_rate'] = $unit['unit_rate'];
                    StockUnitRates::create($urate);
                }
            }
        }
        if (is_array($rq->gdstock) && !empty($rq->gdstock)) {
            $this->updateGoddownStocks($bs, $rq->gdstock);
        }
        return response()->json(['message' => "Updated", 'status' => $this->success_stat]);
    }

    public function updateInDailyStocks($bs, $new_qty)
    {

        $date = date('Y-m-d');
        $dateExist = StockDailyUpdates::select('*')->where('sdu_branch_stock_id', $bs['branch_stock_id'])->where('sdu_stock_id', $bs['bs_stock_id'])
            ->where('sdu_prd_id', $bs['bs_prd_id'])->where('sdu_date', $date)->first();

        if ($dateExist) {
            StockDailyUpdates::where('sdu_branch_stock_id', $bs['branch_stock_id'])->where('sdu_stock_id', $bs['bs_stock_id'])
                ->where('sdu_prd_id', $bs['bs_prd_id'])->where('sdu_date', $date)->increment('sdu_stock_quantity', $new_qty);

        } else {
            $ds['sdu_branch_stock_id'] = $bs['branch_stock_id'];
            $ds['sdu_stock_id'] = $bs['bs_stock_id'];
            $ds['sdu_prd_id'] = $bs['bs_prd_id'];
            $ds['sdu_date'] = $date;
            $ds['sdu_stock_quantity'] = $new_qty;
            StockDailyUpdates::create($ds);

        }
        $opLog['opstklog_branch_stock_id'] = $bs['branch_stock_id'];
        $opLog['opstklog_stock_id'] = $bs['bs_stock_id'];
        $opLog['opstklog_prd_id'] = $bs['bs_prd_id'];
        $opLog['opstklog_date'] = $date;
        $opLog['opstklog_stock_quantity_add'] = $new_qty;
        $opLog['opstklog_prate'] = $bs['bs_prate'];
        $opLog['branch_id'] = $this->branch_id;
        $prdUnit = Product::select('prd_base_unit_id')
            ->where('prd_id', $bs['bs_prd_id'])
            ->first();
        $opLog['opstklog_unit_id'] = $prdUnit['prd_base_unit_id'];

        OpeningStockLog::create($opLog);

    }

    public function updateGoddownStocks($bd, $gd_data)
    {
        $sl['gsl_from'] = 0;
        $sl['gsl_branch_stock_id'] = $ns['gs_branch_stock_id'] = $bd['branch_stock_id'];
        $sl['gsl_stock_id'] = $ns['gs_stock_id'] = $bd['bs_stock_id'];
        $sl['gsl_prd_id'] = $ns['gs_prd_id'] = $bd['bs_prd_id'];
        $whr = $ns;
        $sl['gsl_added_by'] = $ns['gs_added_by'] = $this->usr_id;
        $sl['branch_id'] = $ns['branch_id'] = $this->branch_id;

        foreach ($gd_data as $gd) {
            $sl['gsl_to'] = $whr['gs_godown_id'] = $ns['gs_godown_id'] = $gd['gd_id'];
            $sl['gsl_qty'] = $ns['gs_qty'] = $gd['quantity'];

            if ($gdstck = GodownStock::select('*')->where($whr)->first()) {
                $gdstck['gs_qty'] = $gdstck['gs_qty'] + $gd['quantity'];
                $gdstck->save();
            } else {
                $gdstck = GodownStock::create($ns);
            }
            $sl['gsl_gdwn_stock_id'] = $gdstck->gs_id;
            GodownStockLog::create($sl);
        }
    }

    public function searchProducts($request)
    {

        $results = [];
        if ($request->keyword) {

            $results = Product::select('prd_id', 'prd_name', 'prd_alias', 'prd_base_unit_id')->where('prd_name', 'like', '%' . $request->keyword . '%')->take(10)
                ->with(array('prd_units' => function ($query) {
                    $query->select('unit_id', 'unit_name');
                }))->get('erp_products');
        }
        return parent::displayData($results, $this->success_stat);

    }

    /**
     * @api {post} http://127.0.0.1:8000/api/stock/search Search Stock
     * @apiName searchStock
     * @apiGroup Stock
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {String} keyword Keyword to search

     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} data Stock Details

     * @apiError {String} error Error Message.
     */

    public function StockSearch($rq)
    {
        // die("ghj");
        $stk_stat = isset($rq['stk_stat']) ? 1 : 0;

        $results = [];
        if ($rq->keyword) {
            $qr = Product::select('prd_id', 'prd_name', 'prd_alias', 'prd_base_unit_id', 'branch_stock_id', 'bs_stock_id', 'bs_prate', 'bs_avg_prate', 'bs_stock_quantity', 'bs_stock_quantity_shop', 'bs_stock_quantity_gd')
                ->where('prd_name', 'like', '%' . $rq->keyword . '%');
            if ($stk_stat) {
                $qr->where('prd_stock_status', 1);
            }

            $results = $qr->Join('branch_stocks', function ($join) {
                $join->on('products.prd_id', 'branch_stocks.bs_prd_id')->where('bs_branch_id', $this->branch_id);
            })->groupBy('products.prd_id')->take(10)->get();
            foreach ($results as $k => $row) {

                // print_r($row);
                //die("==".$row['prd_id']);
                $results[$k]['units_rates'] = StockUnitRates::select('sur_id', 'sur_unit_id', 'sur_unit_rate', 'unit_name', 'unit_type', 'unit_base_qty')
                    ->where('branch_stock_id', $row['branch_stock_id'])
                    ->Join('units', function ($join) {
                        $join->on('units.unit_id', 'stock_unit_rates.sur_unit_id');})->get();

                $results[$k]['stock_status'] = empty($results[$k]['units_rates']) ? false : true;
                $results[$k]['prd_units'] = ProdUnit::select('prod_units.produnit_prod_id', 'prod_units.produnit_unit_id as sur_unit_id', 'units.unit_name')->join('units', 'units.unit_id', 'prod_units.produnit_unit_id')->where('prod_units.produnit_prod_id', $row['prd_id'])->get();
                $results[$k]['gdstock'] = GodownStock::select('godown_stocks.gs_godown_id as gd_id', 'godown_stocks.gs_qty', 'godown_master.gd_name')->join('godown_master', 'godown_master.gd_id', 'godown_stocks.gs_godown_id')->where('godown_stocks.gs_branch_stock_id', $row['branch_stock_id'])->get();

                if ($this->usr_type == 3) {
                    $results[$k]['branch_id'] = [];
                } else {
                    $branches = BranchStocks::select('bs_branch_id')->where('bs_stock_id', $row['bs_stock_id'])->where('bs_branch_id', '>', 0)->get()->toArray();
                    $results[$k]['branch_id'] = array_column($branches, 'bs_branch_id');
                }
            }
        }
        return parent::displayData($results, $this->success_stat);
    }

    /**
     * @api {post} http://127.0.0.1:8000/api/stock_search/batch Search Batch
     * @apiName batch_search
     * @apiGroup Stock
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {String} keyword Keyword to search

     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} data Stock Details

     * @apiError {String} error Error Message.
     */

    public function batchSearch(Request $request)
    {

        $results = [];
        if ($request->keyword) {

            if ($request->branch_stock_id) {

                if ($request->like = true) {
                    $results = StockBatches::select('sb_batch_code', 'sb_expiry_date', 'sb_manufacture_date')->where('sb_branch_stock_id', $request->branch_stock_id)
                        ->where('sb_batch_code', 'like', '%' . $request->keyword . '%');
                    if ($this->branch_id > 0) {
                        $results = $results->where('branch_id', $this->branch_id);
                    }

                    $results = $results->get();
                } else {
                    $results = StockBatches::select('sb_batch_code', 'sb_expiry_date', 'sb_manufacture_date')->where('sb_branch_stock_id', $request->branch_stock_id)
                        ->where('sb_batch_code', $request->keyword);
                    if ($this->branch_id > 0) {
                        $results = $results->where('branch_id', $this->branch_id);
                    }

                    $results = $results->get();
                }

            } else {

                if ($request->like) {
                    $results = StockBatches::select('sb_batch_code', 'sb_expiry_date', 'sb_manufacture_date')
                        ->where('sb_batch_code', 'like', '%' . $request->keyword . '%');
                    if ($this->branch_id > 0) {
                        $results = $results->where('branch_id', $this->branch_id);
                    }

                    $results = $results->take(10)->get();
                } else {

                    $results = StockBatches::select('sb_batch_code', 'sb_expiry_date', 'sb_manufacture_date')
                        ->where('sb_batch_code', $request->keyword);
                    if ($this->branch_id > 0) {
                        $results = $results->where('branch_id', $this->branch_id);
                    }

                    $results = $results->take(10)->get();
                }

            }
        }
        return parent::displayData($results);
    }

    public function calcAvgPrate($rq, $old_stk)
    {
        // calcBranchAvgPrate start
        $old_p_amount = $old_stk->bs_stock_quantity * $old_stk->bs_avg_prate;
        $new_p_amount = $rq->tot_qty * $rq->purchase_rate;
        $bs_avg_prate = (abs($old_p_amount) + abs($new_p_amount)) / (abs($old_stk->bs_stock_quantity) + abs($rq->tot_qty));
        BranchStocks::where('branch_stock_id', $old_stk->branch_stock_id)->update(['bs_avg_prate' => $bs_avg_prate]);
        // calcBranchAvgPrate end

        //calcCompanyAvgPrate start
        $company = BranchStocks::where('bs_stock_id', $old_stk->bs_stock_id)->where('bs_branch_id', 0)->first();
        $old_stockp_amount = $company->tot_qty * $company->bs_avg_prate;
        $bs_avg_prate2 = (abs($old_stockp_amount) + abs($new_p_amount)) / (abs($company->bs_stock_quantity) + abs($rq->tot_qty));

        if ($rq->branch_id != 0) {
            BranchStocks::where('bs_stock_id', $company->bs_stock_id)->where('bs_branch_id', 0)->update(['bs_avg_prate' => $bs_avg_prate2]);
        }

        CompanyStocks::where('cmp_stock_id', $company->bs_stock_id)->update(['cmp_avg_prate' => $bs_avg_prate2]);
        //calcCompanyAvgPrate end

    }

    public function productSearch(Request $rq)
    {
        $results = [];
        if ($rq->keyword) {
            $rltn = ['units' => function ($qr) {
                $qr->select('unit_id', 'unit_name', 'unit_code', 'unit_type', 'unit_base_qty', 'produnit_id', 'produnit_ean_barcode')
                    ->orderby('unit_id', 'asc');
            },
                'batches' => function ($qr) {
                    $qr->select('sb_batch_code', 'sb_manufacture_date', 'sb_expiry_date');
                },
            ];
            $results = Product::select('prd_id', 'prd_name', 'prd_alias', 'prd_tax', 'prd_base_unit_id', 'cmp_stock_id')
                ->where('prd_name', 'like', '%' . $rq->keyword . '%')->with($rltn)
                ->Join('company_stocks', function ($join) {
                    $join->on('products.prd_id', 'company_stocks.cmp_prd_id');
                })->groupBy('products.prd_id')->take(40)->get();

            foreach ($results as $k => $row) {

                $duid = trim($row['prd_base_unit_id']);

                $branch_stocks = BranchStocks::select('bs_prate')
                    ->where('bs_prd_id', $row['prd_id'])->where('bs_branch_id', $this->branch_id)->first();
                $results[$k]['purchase_rate'] = isset($branch_stocks->bs_prate) ? $branch_stocks->bs_prate : 0;

                foreach ($results[$k]['units'] as $key => $unit) {

                    $unit = $unit->toArray();

                    $ean = ProdUnit::select('produnit_ean_barcode')->where('produnit_unit_id', $unit['unit_id'])->where('produnit_prod_id', $row['prd_id'])->first();
                    $produnit_ean_barcode = isset($ean->produnit_ean_barcode) ? $ean->produnit_ean_barcode : '';

                    $unit_rate = StockUnitRates::select('sur_unit_rate')
                        ->where('sur_branch_id', $this->branch_id)
                        ->where('sur_unit_id', $unit['unit_id'])
                        ->where('sur_stock_id', $row['cmp_stock_id'])->first();

                    $unit['unit_rate'] = isset($unit_rate->sur_unit_rate) ? $unit_rate->sur_unit_rate : 0;
                    $unit['produnit_ean_barcode'] = isset($produnit_ean_barcode) ? $produnit_ean_barcode : 0;

                    if ($duid == $unit['unit_id']) {
                        $results[$k]['ean'] = $produnit_ean_barcode;
                    }
                    $results[$k]['units'][$key] = $unit;
                }

                if ($this->usr_type == 3) {
                    $results[$k]['branch_id'] = [];
                } else {
                    $branches = BranchStocks::select('bs_branch_id')->where('bs_stock_id', $row['bs_stock_id'])->where('bs_branch_id', '>', 0)->get()->toArray();
                    $results[$k]['branch_id'] = array_column($branches, 'bs_branch_id');
                }
            }
        }
        return parent::displayData($results, $this->success_stat);
    }

    public function productPurchaseSearch(Request $rq)
    {

        $results = [];

        if ($rq->keyword) {
            $rltn = ['units' => function ($qr) {
                $qr->select('unit_id', 'unit_code', 'unit_display', 'unit_name', 'unit_type', 'unit_base_qty', 'produnit_id', 'produnit_ean_barcode');
            },
                'batches' => function ($qr) {
                    $qr->select('sb_batch_code', 'sb_manufacture_date', 'sb_expiry_date');
                },
            ];
            $key = "'" . $rq->keyword . "%'";

            if ($rq->ean) {

                //  $prd_id_array = Product::select('prd_id','prd_barcode','prd_ean')->where('prd_barcode', 'like', $key)->orWhere('prd_ean', 'like', $key)->get()->toArray();
                $data = \DB::SELECT("select `prd_id`, `prd_barcode`, `prd_ean` from `erp_products` where `prd_barcode` like $key or `prd_ean` like $key ");
                $prd_id_array = json_decode(json_encode($data, true));
                $prd_ids = array_column($prd_id_array, 'prd_id');

                if (empty($prd_ids)) {
                    return parent::displayData([]);
                }

                $results = Product::select('prd_id', 'prd_name', 'prd_alias', 'prd_barcode', 'prd_ean', 'prd_tax', 'prd_base_unit_id', 'cmp_stock_id', 'prd_tax_cat_id')

                    ->where('prd_type', 1)
                    ->whereIn('prd_id', [$prd_ids])->with($rltn)
                    ->Join('company_stocks', function ($join) {
                        $join->on('products.prd_id', 'company_stocks.cmp_prd_id');
                    })->groupBy('products.prd_id')->take($this->per_page)->get();

            } else {
                if ($alias = isset($rq['alias']) ? $rq['alias'] : 0) {
                    $results = Product::select('prd_id', 'prd_name', 'prd_alias', 'prd_barcode', 'prd_ean', 'prd_tax', 'prd_base_unit_id', 'cmp_stock_id', 'prd_tax_cat_id')
                        ->where('prd_type', 1)
                        ->where('prd_alias', 'like', '%' . $rq->keyword . '%')->with($rltn)
                        ->Join('company_stocks', function ($join) {
                            $join->on('products.prd_id', 'company_stocks.cmp_prd_id');
                        })->groupBy('products.prd_id')->take($this->per_page)->get();

                } else {
                    $results = Product::select('prd_id', 'prd_name', 'prd_alias', 'prd_barcode', 'prd_ean', 'prd_tax', 'prd_base_unit_id', 'cmp_stock_id', 'prd_tax_cat_id')
                        ->where('prd_type', 1)
                        ->where('prd_name', 'like', '%' . $rq->keyword . '%')->with($rltn)
                        ->Join('company_stocks', function ($join) {
                            $join->on('products.prd_id', 'company_stocks.cmp_prd_id');
                        })->groupBy('products.prd_id')->take(60)->get();
                }

            }

            foreach ($results as $k => $row) {

                $duid = trim($row['prd_base_unit_id']);

                $branch_stocks = BranchStocks::select('branch_stock_id', 'bs_stock_id', 'bs_prd_id', 'bs_prate', 'bs_stock_quantity', 'bs_avg_prate')
                    ->where('bs_prd_id', $row['prd_id'])->where('bs_branch_id', $this->branch_id)->first();
                // $results[$k]['purchase_rate'] = isset($branch_stocks->bs_prate) ? $branch_stocks->bs_prate : 0;
                $results[$k]['purchase_rate'] = PurchaseSub::select('purchsub_rate')->where('purchsub_prd_id', $row['prd_id'])->where('branch_id', $this->branch_id)->latest()->value('purchsub_rate');
                $results[$k]['current_stock'] = isset($branch_stocks->bs_stock_quantity) ? $branch_stocks->bs_stock_quantity : 0;
                $results[$k]['avg_rate'] = isset($branch_stocks->bs_avg_prate) ? $branch_stocks->bs_avg_prate : 0;

                $results[$k]['branch_stock_id'] = isset($branch_stocks->branch_stock_id) ? $branch_stocks->branch_stock_id : 0;
                $results[$k]['last_purchase_date'] = $this->findLastPurchDate($branch_stocks);
                foreach ($results[$k]['units'] as $key => $unit) {

                    if ($unit['unit_id'] == $row['prd_base_unit_id']) {
                        $results[$k]['prd_base_unit_name'] = $unit['unit_code'];
                        $results[$k]['prd_base_unit_name_master'] = $unit['unit_name'];
                        $results[$k]['unit_display'] = $unit['unit_display'];
                    }
                    $unit = $unit->toArray();

                    $ean = ProdUnit::select('produnit_ean_barcode')->where('produnit_unit_id', $unit['unit_id'])->where('produnit_prod_id', $row['prd_id'])->first();
                    $produnit_ean_barcode = isset($ean->produnit_ean_barcode) ? $ean->produnit_ean_barcode : '';

                    $unit_rate = StockUnitRates::select('sur_unit_rate')
                        ->where('sur_branch_id', $this->branch_id)
                        ->where('sur_unit_id', $unit['unit_id'])
                        ->where('sur_stock_id', $row['cmp_stock_id'])->first();

                    $unit['unit_rate'] = isset($unit_rate->sur_unit_rate) ? $unit_rate->sur_unit_rate : 0;
                    $unit['produnit_ean_barcode'] = isset($produnit_ean_barcode) ? $produnit_ean_barcode : 0;

                    if ($duid == $unit['unit_id']) {
                        $results[$k]['ean'] = $produnit_ean_barcode;
                    }
                    $results[$k]['units'][$key] = $unit;
                }

                if ($this->usr_type == 3) {
                    $results[$k]['branch_id'] = [];
                } else {
                    $branches = BranchStocks::select('bs_branch_id')->where('bs_stock_id', $row['bs_stock_id'])->where('bs_branch_id', '>', 0)->get()->toArray();
                    $results[$k]['branch_id'] = array_column($branches, 'bs_branch_id');
                }
            }
        }

        return parent::displayData($results, $this->success_stat);
    }

    public function findLastPurchDate($stocks)
    {

        if (isset($stocks->bs_prd_id) && isset($stocks->bs_stock_id)) {
            $db = PurchaseSub::select('created_at')->where('purchsub_prd_id', $stocks->bs_prd_id)->where('purchsub_stock_id', $stocks->bs_stock_id)->where('branch_id', $this->branch_id)
                ->orderBy('purchsub_id', 'desc')->first();

            return isset($db->created_at) ? $db->created_at : '';
        }
        return '';
    }

    public function isStockProduct($prd_id)
    {

        if ($prd_id) {
            $return = Product::select('*')->where('prd_id', $prd_id)
                ->get()->toArray();
            return isset($return['0']['prd_stock_status']) ? trim($return['0']['prd_stock_status']) : 1;
        } else {
            return 1;
        }

    }

    public function productPurchaseSearchUean(Request $rq)
    {

        $query = Product::join('prod_units', 'products.prd_id', '=', 'prod_units.produnit_prod_id')->join('units', 'units.unit_id', '=', 'prod_units.produnit_unit_id')->where('prod_units.produnit_ean_barcode', $rq->keyword)->first();
        // echo $query->unit_name;
        //exit;

        if ($query) {
            $rq->keyword = $query->prd_name;
            $results = [];

            if ($rq->keyword) {
                $rltn = ['units' => function ($qr) {
                    $qr->select('unit_id', 'unit_code', 'unit_display', 'unit_name', 'unit_type', 'unit_base_qty', 'produnit_id', 'produnit_ean_barcode');
                },
                    'batches' => function ($qr) {
                        $qr->select('sb_batch_code', 'sb_manufacture_date', 'sb_expiry_date');
                    },
                ];
                $key = "'" . $rq->keyword . "%'";

                if ($rq->ean) {

                    //  $prd_id_array = Product::select('prd_id','prd_barcode','prd_ean')->where('prd_barcode', 'like', $key)->orWhere('prd_ean', 'like', $key)->get()->toArray();
                    $data = \DB::SELECT("select `prd_id`, `prd_barcode`, `prd_ean` from `erp_products` where `prd_barcode` like $key or `prd_ean` like $key ");
                    $prd_id_array = json_decode(json_encode($data, true));
                    $prd_ids = array_column($prd_id_array, 'prd_id');

                    if (empty($prd_ids)) {
                        return parent::displayData([]);
                    }

                    $results = Product::select('prd_id', 'prd_name', 'prd_alias', 'prd_barcode', 'prd_ean', 'prd_tax', 'prd_base_unit_id', 'cmp_stock_id')

                        ->where('prd_type', 1)
                        ->whereIn('prd_id', [$prd_ids])->with($rltn)
                        ->Join('company_stocks', function ($join) {
                            $join->on('products.prd_id', 'company_stocks.cmp_prd_id');
                        })->take($this->per_page)->get();

                } else {
                    if ($alias = isset($rq['alias']) ? $rq['alias'] : 0) {
                        $results = Product::select('prd_id', 'prd_name', 'prd_alias', 'prd_barcode', 'prd_ean', 'prd_tax', 'prd_base_unit_id', 'cmp_stock_id')
                            ->where('prd_type', 1)
                            ->where('prd_alias', 'like', '%' . $rq->keyword . '%')->with($rltn)
                            ->Join('company_stocks', function ($join) {
                                $join->on('products.prd_id', 'company_stocks.cmp_prd_id');
                            })->take($this->per_page)->get();

                    } else {
                        $results = Product::select('prd_id', 'prd_name', 'prd_alias', 'prd_barcode', 'prd_ean', 'prd_tax', 'prd_base_unit_id', 'cmp_stock_id')
                            ->where('prd_type', 1)
                            ->where('prd_name', 'like', '%' . $rq->keyword . '%')->with($rltn)
                            ->Join('company_stocks', function ($join) {
                                $join->on('products.prd_id', 'company_stocks.cmp_prd_id');
                            })->take($this->per_page)->get();
                    }

                }

                foreach ($results as $k => $row) {

                    $duid = trim($row['prd_base_unit_id']);

                    $branch_stocks = BranchStocks::select('branch_stock_id', 'bs_stock_id', 'bs_prd_id', 'bs_prate', 'bs_stock_quantity', 'bs_avg_prate')
                        ->where('bs_prd_id', $row['prd_id'])->where('bs_branch_id', $this->branch_id)->first();
                    $results[$k]['purchase_rate'] = isset($branch_stocks->bs_prate) ? $branch_stocks->bs_prate : 0;
                    $results[$k]['current_stock'] = isset($branch_stocks->bs_stock_quantity) ? $branch_stocks->bs_stock_quantity : 0;
                    $results[$k]['avg_rate'] = isset($branch_stocks->bs_avg_prate) ? $branch_stocks->bs_avg_prate : 0;

                    $results[$k]['branch_stock_id'] = isset($branch_stocks->branch_stock_id) ? $branch_stocks->branch_stock_id : 0;
                    $results[$k]['last_purchase_date'] = $this->findLastPurchDate($branch_stocks);
                    foreach ($results[$k]['units'] as $key => $unit) {

                        if ($unit['unit_id'] == $row['prd_base_unit_id']) {
                            $results[$k]['prd_base_unit_name'] = $unit['unit_code'];
                            $results[$k]['prd_base_unit_name_master'] = $unit['unit_name'];
                            $results[$k]['prd_base_unit_name_bcode'] = $query->unit_name;
                            $results[$k]['prd_base_unit_id_bcode'] = $query->unit_id;
                            $results[$k]['prd_base_unit_qty_bcode'] = $query->unit_base_qty;
                            $results[$k]['unit_display'] = $unit['unit_display'];
                        }
                        $unit = $unit->toArray();

                        $ean = ProdUnit::select('produnit_ean_barcode')->where('produnit_unit_id', $unit['unit_id'])->where('produnit_prod_id', $row['prd_id'])->first();
                        $produnit_ean_barcode = isset($ean->produnit_ean_barcode) ? $ean->produnit_ean_barcode : '';

                        $unit_rate = StockUnitRates::select('sur_unit_rate')
                            ->where('sur_branch_id', $this->branch_id)
                            ->where('sur_unit_id', $unit['unit_id'])
                            ->where('sur_stock_id', $row['cmp_stock_id'])->first();

                        $unit['unit_rate'] = isset($unit_rate->sur_unit_rate) ? $unit_rate->sur_unit_rate : 0;
                        $unit['produnit_ean_barcode'] = isset($produnit_ean_barcode) ? $produnit_ean_barcode : 0;

                        if ($duid == $unit['unit_id']) {
                            $results[$k]['ean'] = $produnit_ean_barcode;
                        }
                        $results[$k]['units'][$key] = $unit;
                    }

                    if ($this->usr_type == 3) {
                        $results[$k]['branch_id'] = [];
                    } else {
                        $branches = BranchStocks::select('bs_branch_id')->where('bs_stock_id', $row['bs_stock_id'])->where('bs_branch_id', '>', 0)->get()->toArray();
                        $results[$k]['branch_id'] = array_column($branches, 'bs_branch_id');
                    }
                }
            }

            return parent::displayData($results, $this->success_stat);

        } else { $results = [];

            return parent::displayData($results, $this->success_stat);
        }
    }

    public function productPurchaseSearchBarcode(Request $rq)
    {

        $results = [];

        if ($rq->keyword) {
            $rltn = ['units' => function ($qr) {
                $qr->select('unit_id', 'unit_code', 'unit_display', 'unit_name', 'unit_type', 'unit_base_qty', 'produnit_id', 'produnit_ean_barcode');
            },
                'batches' => function ($qr) {
                    $qr->select('sb_batch_code', 'sb_manufacture_date', 'sb_expiry_date');
                },
            ];
            $key = "'" . $rq->keyword . "'";

            if ($rq->ean) {

                //  $prd_id_array = Product::select('prd_id','prd_barcode','prd_ean')->where('prd_barcode', 'like', $key)->orWhere('prd_ean', 'like', $key)->get()->toArray();
                $data = \DB::SELECT("select `prd_id`, `prd_barcode`, `prd_ean` from `erp_products` where `prd_barcode` like $key or `prd_ean` like $key ");
                $prd_id_array = json_decode(json_encode($data, true));
                $prd_ids = array_column($prd_id_array, 'prd_id');

                if (empty($prd_ids)) {
                    return parent::displayData([]);
                }

                $results = Product::select('prd_id', 'prd_name', 'prd_alias', 'prd_barcode', 'prd_ean', 'prd_tax', 'prd_base_unit_id', 'cmp_stock_id')
                    ->whereIn('prd_id', [$prd_ids])->with($rltn)
                    ->Join('company_stocks', function ($join) {
                        $join->on('products.prd_id', 'company_stocks.cmp_prd_id');
                    })->take($this->per_page)->get();

            } else {
                if ($alias = isset($rq['alias']) ? $rq['alias'] : 0) {
                    $results = Product::select('prd_id', 'prd_name', 'prd_alias', 'prd_barcode', 'prd_ean', 'prd_tax', 'prd_base_unit_id', 'cmp_stock_id')
                        ->where('prd_alias', 'like', '%' . $rq->keyword . '%')->with($rltn)
                        ->Join('company_stocks', function ($join) {
                            $join->on('products.prd_id', 'company_stocks.cmp_prd_id');
                        })->take($this->per_page)->get();

                } else {
                    $results = Product::select('prd_id', 'prd_name', 'prd_alias', 'prd_barcode', 'prd_ean', 'prd_tax', 'prd_base_unit_id', 'cmp_stock_id')
                        ->where('prd_barcode', '=', $rq->keyword)->with($rltn)
                        ->Join('company_stocks', function ($join) {
                            $join->on('products.prd_id', 'company_stocks.cmp_prd_id');
                        })->take($this->per_page)->get();
                }

            }

            foreach ($results as $k => $row) {

                $duid = trim($row['prd_base_unit_id']);

                $branch_stocks = BranchStocks::select('branch_stock_id', 'bs_stock_id', 'bs_prd_id', 'bs_prate', 'bs_stock_quantity', 'bs_avg_prate')
                    ->where('bs_prd_id', $row['prd_id'])->where('bs_branch_id', $this->branch_id)->first();
                $results[$k]['purchase_rate'] = isset($branch_stocks->bs_prate) ? $branch_stocks->bs_prate : 0;
                $results[$k]['current_stock'] = isset($branch_stocks->bs_stock_quantity) ? $branch_stocks->bs_stock_quantity : 0;
                $results[$k]['avg_rate'] = isset($branch_stocks->bs_avg_prate) ? $branch_stocks->bs_avg_prate : 0;

                $results[$k]['branch_stock_id'] = isset($branch_stocks->branch_stock_id) ? $branch_stocks->branch_stock_id : 0;
                $results[$k]['last_purchase_date'] = $this->findLastPurchDate($branch_stocks);
                foreach ($results[$k]['units'] as $key => $unit) {

                    if ($unit['unit_id'] == $row['prd_base_unit_name']) {
                        // $results[$k]['prd_base_unit_name'] = $unit['unit_code'];
                        // $results[$k]['prd_base_unit_name_master'] = $unit['unit_name'];
                        // $results[$k]['unit_display'] = $unit['unit_display'];
                        $results[$k]['prd_base_unit_name'] = $unit['unit_code'];
                        $results[$k]['prd_base_unit_name_master'] = $unit['unit_name'];
                        $results[$k]['prd_base_unit_name_bcode'] = $query->unit_name;
                        $results[$k]['prd_base_unit_id_bcode'] = $query->unit_id;
                        $results[$k]['prd_base_unit_qty_bcode'] = $query->unit_base_qty;
                        $results[$k]['unit_display'] = $unit['unit_display'];
                    }
                    $unit = $unit->toArray();

                    $ean = ProdUnit::select('produnit_ean_barcode')->where('produnit_unit_id', $unit['unit_id'])->where('produnit_prod_id', $row['prd_id'])->first();
                    $produnit_ean_barcode = isset($ean->produnit_ean_barcode) ? $ean->produnit_ean_barcode : '';

                    $unit_rate = StockUnitRates::select('sur_unit_rate')
                        ->where('sur_branch_id', $this->branch_id)
                        ->where('sur_unit_id', $unit['unit_id'])
                        ->where('sur_stock_id', $row['cmp_stock_id'])->first();

                    $unit['unit_rate'] = isset($unit_rate->sur_unit_rate) ? $unit_rate->sur_unit_rate : 0;
                    $unit['produnit_ean_barcode'] = isset($produnit_ean_barcode) ? $produnit_ean_barcode : 0;

                    if ($duid == $unit['unit_id']) {
                        $results[$k]['ean'] = $produnit_ean_barcode;
                    }
                    $results[$k]['units'][$key] = $unit;
                }

                if ($this->usr_type == 3) {
                    $results[$k]['branch_id'] = [];
                } else {
                    $branches = BranchStocks::select('bs_branch_id')->where('bs_stock_id', $row['bs_stock_id'])->where('bs_branch_id', '>', 0)->get()->toArray();
                    $results[$k]['branch_id'] = array_column($branches, 'bs_branch_id');
                }
            }
        }
        return parent::displayData($results, $this->success_stat);
    }

    public function searchStocksProduct(Request $request)
    {
        if ($request->gd_id > 0) {
            return $this->productByGodown($request);
        } else {
            return $this->productByShop($request);
        }
    }

    public function productByGodown(Request $rq)
    {
        $results = [];
        $this->gd_id = $rq->gd_id;
        if ($rq->keyword) {
            $rltn = ['units' => function ($qr) {
                $qr->select('unit_id', 'unit_name', 'unit_code', 'unit_type', 'unit_base_qty', 'produnit_id', 'produnit_ean_barcode');
            },
                'batches' => function ($qr) {
                    $qr->select('sb_batch_code', 'sb_manufacture_date', 'sb_expiry_date');
                },
                'base_unit' => function ($qr) {
                    $qr->select('unit_id', 'unit_code', 'unit_name', 'unit_type', 'unit_base_qty', 'unit_code');
                },
            ];

            if ($rq->search_type == 'name') {
                $results = Product::select('prd_id', 'prd_tax', 'prd_barcode', 'prd_name', 'gs_qty as avialble_qty',
                    'prd_alias', 'prd_tax', 'prd_base_unit_id', 'branch_stock_id', 'bs_stock_id', 'bs_batch_id',
                    'bs_prate', 'bs_avg_prate', 'prd_tax_cat_id')
                    ->where('prd_name', 'like', $rq->keyword . '%')->with($rltn)
                    ->Join('branch_stocks', function ($join) {
                        $join->on('products.prd_id', 'branch_stocks.bs_prd_id')
                            ->where('branch_stocks.bs_branch_id', $this->branch_id);
                    })->Join('godown_stocks', function ($join) {
                    $join->on('godown_stocks.gs_branch_stock_id', 'branch_stocks.branch_stock_id')
                        ->where('gs_godown_id', $this->gd_id);
                })->take(20)->get();
            } else {
                $results = Product::select('prd_id', 'prd_tax', 'prd_barcode', 'prd_name', 'gs_qty as avialble_qty',
                    'prd_alias', 'prd_tax', 'prd_base_unit_id', 'branch_stock_id', 'bs_stock_id', 'bs_batch_id',
                    'bs_prate', 'bs_avg_prate', 'prd_tax_cat_id')
                    ->where('prd_alias', 'like', $rq->keyword . '%')->with($rltn)
                    ->Join('branch_stocks', function ($join) {
                        $join->on('products.prd_id', 'branch_stocks.bs_prd_id')
                            ->where('branch_stocks.bs_branch_id', $this->branch_id);
                    })->Join('godown_stocks', function ($join) {
                    $join->on('godown_stocks.gs_branch_stock_id', 'branch_stocks.branch_stock_id')
                        ->where('gs_godown_id', $this->gd_id);
                })->take(20)->get();

            }
        }
        return parent::displayData($results, $this->success_stat);
    }

    public function productByShop(Request $rq)
    {
        $results = [];
        if ($rq->keyword) {
            $rltn = ['units' => function ($qr) {
                $qr->select('unit_id', 'unit_name', 'unit_code', 'unit_type', 'unit_base_qty', 'produnit_id', 'produnit_ean_barcode');
            },
                'batches' => function ($qr) {
                    $qr->select('sb_batch_code', 'sb_manufacture_date', 'sb_expiry_date');
                },
                'base_unit' => function ($qr) {
                    $qr->select('unit_id', 'unit_name', 'unit_code', 'unit_type', 'unit_base_qty');
                },
            ];

            if ($rq->search_type == 'name') {
                $results = Product::select('prd_id', 'prd_tax', 'prd_barcode', 'prd_name', 'bs_stock_quantity_shop as avialble_qty',
                    'prd_alias', 'prd_tax', 'prd_base_unit_id', 'branch_stock_id', 'bs_stock_id', 'bs_batch_id',
                    'bs_prate', 'bs_avg_prate', 'prd_tax_cat_id')
                    ->where('prd_name', 'like', $rq->keyword . '%')->with($rltn)
                    ->Join('branch_stocks', function ($join) {
                        $join->on('products.prd_id', 'branch_stocks.bs_prd_id')
                            ->where('branch_stocks.bs_branch_id', $this->branch_id);;
                    })->take(20)->get();
            } else {
                $results = Product::select('prd_id', 'prd_tax', 'prd_barcode', 'prd_name', 'bs_stock_quantity_shop as avialble_qty',
                    'prd_alias', 'prd_tax', 'prd_base_unit_id', 'branch_stock_id', 'bs_stock_id', 'bs_batch_id',
                    'bs_prate', 'bs_avg_prate', 'prd_tax_cat_id')
                    ->where('prd_alias', 'like', $rq->keyword . '%')->with($rltn)
                    ->Join('branch_stocks', function ($join) {
                        $join->on('products.prd_id', 'branch_stocks.bs_prd_id')
                            ->where('branch_stocks.bs_branch_id', $this->branch_id);;
                    })->take(20)->get();
            }

        }
        return parent::displayData($results, $this->success_stat);
    }

    public function searchStocksProductBarcode(Request $request)
    {
        if ($request->gd_id > 0) {
            return $this->productByGodownBarcode($request);
        } else {
            return $this->productByShopBarcode($request);
        }
    }

    public function productByGodownBarcode(Request $rq)
    {
        $results = [];
        $this->gd_id = $rq->gd_id;
        if ($rq->keyword) {
            $rltn = ['units' => function ($qr) {
                $qr->select('unit_id', 'unit_name', 'unit_code', 'unit_type', 'unit_base_qty', 'produnit_id', 'produnit_ean_barcode');
            },
                'batches' => function ($qr) {
                    $qr->select('sb_batch_code', 'sb_manufacture_date', 'sb_expiry_date');
                },
                'base_unit' => function ($qr) {
                    $qr->select('unit_id', 'unit_name', 'unit_code', 'unit_type', 'unit_base_qty');
                },
            ];
            $results = Product::select('prd_id', 'prd_tax', 'prd_barcode', 'prd_name', 'gs_qty as avialble_qty',
                'prd_alias', 'prd_tax', 'prd_base_unit_id', 'branch_stock_id', 'bs_stock_id', 'bs_batch_id',
                'bs_prate', 'bs_avg_prate')
                ->where('prd_barcode', $rq->keyword)->with($rltn)
                ->Join('branch_stocks', function ($join) {
                    $join->on('products.prd_id', 'branch_stocks.bs_prd_id')
                        ->where('branch_stocks.bs_branch_id', $this->branch_id);
                })->Join('godown_stocks', function ($join) {
                $join->on('godown_stocks.gs_branch_stock_id', 'branch_stocks.branch_stock_id')
                    ->where('gs_godown_id', $this->gd_id);
            })->take(20)->get();
        }
        if (count($results) == 0) {
            $product_name = ProdUnit::where('produnit_ean_barcode', $rq->keyword)->join('products', 'prod_units.produnit_prod_id', '=', 'products.prd_id')->pluck('products.prd_name');
            if (count($product_name) > 0) {

                $results = Product::select('prd_id', 'prd_tax', 'prd_barcode', 'prd_name', 'bs_stock_quantity_shop as avialble_qty',
                    'prd_alias', 'prd_tax', 'prd_base_unit_id', 'branch_stock_id', 'bs_stock_id', 'bs_batch_id',
                    'bs_prate', 'bs_avg_prate')
                    ->where('prd_name', $product_name[0])->with($rltn)
                    ->Join('branch_stocks', function ($join) {
                        $join->on('products.prd_id', 'branch_stocks.bs_prd_id')
                            ->where('branch_stocks.bs_branch_id', $this->branch_id);;
                    })->take(20)->get();
            }

        }

        return parent::displayData($results, $this->success_stat);
    }

    public function productByShopBarcode(Request $rq)
    {
        $results = [];
        if ($rq->keyword) {
            $rltn = ['units' => function ($qr) {
                $qr->select('unit_id', 'unit_name', 'unit_code', 'unit_type', 'unit_base_qty', 'produnit_id', 'produnit_ean_barcode');
            },
                'batches' => function ($qr) {
                    $qr->select('sb_batch_code', 'sb_manufacture_date', 'sb_expiry_date');
                },
                'base_unit' => function ($qr) {
                    $qr->select('unit_id', 'unit_name', 'unit_code', 'unit_type', 'unit_base_qty');
                },
            ];
            $results = Product::select('prd_id', 'prd_tax', 'prd_barcode', 'prd_name', 'bs_stock_quantity_shop as avialble_qty',
                'prd_alias', 'prd_tax', 'prd_base_unit_id', 'branch_stock_id', 'bs_stock_id', 'bs_batch_id',
                'bs_prate', 'bs_avg_prate')
                ->where('prd_barcode', $rq->keyword)->with($rltn)
                ->Join('branch_stocks', function ($join) {
                    $join->on('products.prd_id', 'branch_stocks.bs_prd_id')
                        ->where('branch_stocks.bs_branch_id', $this->branch_id);;
                })->take(20)->get();

        }
        if (count($results) == 0) {
            $product_name = ProdUnit::where('produnit_ean_barcode', $rq->keyword)->join('products', 'prod_units.produnit_prod_id', '=', 'products.prd_id')->pluck('products.prd_name');
            if (count($product_name) > 0) {

                $results = Product::select('prd_id', 'prd_tax', 'prd_barcode', 'prd_name', 'bs_stock_quantity_shop as avialble_qty',
                    'prd_alias', 'prd_tax', 'prd_base_unit_id', 'branch_stock_id', 'bs_stock_id', 'bs_batch_id',
                    'bs_prate', 'bs_avg_prate')
                    ->where('prd_name', $product_name[0])->with($rltn)
                    ->Join('branch_stocks', function ($join) {
                        $join->on('products.prd_id', 'branch_stocks.bs_prd_id')
                            ->where('branch_stocks.bs_branch_id', $this->branch_id);;
                    })->take(20)->get();
            }

        }
        return parent::displayData($results, $this->success_stat);
    }

    public function searchRates($rq)
    {
        $stk_stat = isset($rq['stk_stat']) ? 1 : 0;

        $results = [];
        if ($rq->keyword) {
            $qr = Product::select('prd_id', 'prd_name', 'prd_alias', 'prd_base_unit_id', 'branch_stock_id', 'bs_stock_id', 'bs_prate', 'bs_avg_prate', 'bs_stock_quantity', 'bs_stock_quantity_shop', 'bs_stock_quantity_gd')
                ->where('prd_name', 'like', '%' . $rq->keyword . '%');
            if ($stk_stat) {
                $qr->where('prd_stock_status', 1);
            }

            $results = $qr->Join('branch_stocks', function ($join) {
                $join->on('products.prd_id', 'branch_stocks.bs_prd_id')->where('bs_branch_id', $this->branch_id);
            })->groupBy('products.prd_id')->take(10)->get();
            foreach ($results as $k => $row) {
                $results[$k]['units_rates'] = StockUnitRates::select('sur_id', 'sur_unit_id', 'sur_unit_rate', 'unit_name', 'unit_type', 'unit_base_qty')
                    ->where('branch_stock_id', $row['branch_stock_id'])->where('branch_stock_id', $row['branch_stock_id'])
                    ->Join('units', function ($join) {
                        $join->on('units.unit_id', 'stock_unit_rates.sur_unit_id');})->get();

                $results[$k]['stock_status'] = empty($results[$k]['units_rates']) ? false : true;
                $results[$k]['prd_units'] = ProdUnit::select('prod_units.produnit_unit_id as sur_unit_id', 'units.unit_name')->join('units', 'units.unit_id', 'prod_units.produnit_unit_id')->where('prod_units.produnit_prod_id', $row['prd_id'])->get();
                $results[$k]['gdstock'] = GodownStock::select('godown_stocks.gs_godown_id as gd_id', 'godown_stocks.gs_qty', 'godown_master.gd_name')->join('godown_master', 'godown_master.gd_id', 'godown_stocks.gs_godown_id')->where('godown_stocks.gs_branch_stock_id', $row['branch_stock_id'])->get();
                $results[$k]['prd_qty'] = BranchStocks::select('bs_stock_quantity')->where('bs_prd_id', $row['prd_id'])->where('bs_branch_id', $this->branch_id)->first();
                // $results[$k]['prd_qty'] = $qty;

                if ($this->usr_type == 3) {
                    $results[$k]['branch_id'] = [];
                } else {
                    $branches = BranchStocks::select('bs_branch_id')->where('bs_stock_id', $row['bs_stock_id'])->where('bs_branch_id', '>', 0)->get()->toArray();
                    $results[$k]['branch_id'] = array_column($branches, 'bs_branch_id');
                }
            }
        }
        return parent::displayData($results, $this->success_stat);
    }

    public function quantityUpdate($rq)
    {

        $results = "ok";
        return parent::displayData($results, $this->success_stat);

    }

    public function getBranchvise($rq)
    {
        $results = StockUnitRates::leftjoin('units', 'stock_unit_rates.sur_unit_id', '=', 'units.unit_id')->where('sur_prd_id', $rq->prd_id)->where('sur_branch_id', $rq->branchid)->get(array('units.unit_name as unit_name',
            'stock_unit_rates.sur_unit_rate as unit_rate'));
        return parent::displayData($results, $this->success_stat);
    }

    public function updateSalesRates($rq)
    {
        if ($rq->flag == 1) {
            StockUnitRates::where('sur_branch_id', $this->branch_id)->delete();}

        if (($rq->total == 0) || (($rq->total - $rq->skip) > 100)) {
            $results = StockUnitRates::where('sur_branch_id', $rq->branchid)->skip($rq->skip)->take(100)->get();
            $flag = 'L';
        } else {
            $results = StockUnitRates::where('sur_branch_id', $rq->branchid)->skip($rq->skip)->take(100)->get();
            $flag = 'E';

        }
        //   foreach ($results->chunk(500) as $resultchunk) {
        foreach ($results as $result) {
            $bs_prate = BranchStocks::where('bs_prd_id', $result->sur_prd_id)
                ->where('bs_branch_id', $rq->branchid)->first();
            $branch_stocks = BranchStocks::where('bs_prd_id', $result->sur_prd_id)
                ->where('bs_branch_id', $this->branch_id)->first();

            $base_unit = Product::where('prd_base_unit_id', $result->sur_unit_id)->first();

            if ($base_unit) {
                $bs_srate = $result->sur_unit_rate;
            }

            if (!$branch_stocks) {
                if ($base_unit) {
                    $insert_branch_stock =
                    BranchStocks::create(['bs_prd_id' => $result->sur_prd_id, 'bs_stock_id' => $result->sur_stock_id,
                        'bs_branch_id' => $this->branch_id,
                        'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6), 'bs_prate' => $bs_prate->bs_prate, 'bs_avg_prate' => $bs_prate->bs_prate, 'bs_srate' => $bs_srate]);
                }
                // end base unit

                $inserts[] = ['branch_stock_id' => $insert_branch_stock->branch_stock_id,
                    'sur_stock_id' => $result->sur_stock_id,
                    'sur_prd_id' => $result->sur_prd_id,
                    'sur_unit_id' => $result->sur_unit_id,
                    'sur_unit_rate' => $result->sur_unit_rate,
                    'sur_branch_id' => $this->branch_id,
                    'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)];

            } else {

                if ($base_unit) {
                    BranchStocks::where('bs_prd_id', $result->sur_prd_id)
                        ->where('bs_branch_id', $this->branch_id)->update(['bs_prate' => $bs_prate->bs_prate, 'bs_avg_prate' => $bs_prate->bs_prate, 'bs_srate' => $bs_srate, 'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)]);
                }
                //end base unit

                $inserts[] = ['branch_stock_id' => $branch_stocks->branch_stock_id,
                    'sur_stock_id' => $result->sur_stock_id,
                    'sur_prd_id' => $result->sur_prd_id,
                    'sur_unit_id' => $result->sur_unit_id,
                    'sur_unit_rate' => $result->sur_unit_rate,
                    'sur_branch_id' => $this->branch_id,
                    'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)];
            }
            // end branchstocks loop

        }
        //end for
        //  }
        // end for chunk loop
        if (isset($inserts)) {StockUnitRates::insert($inserts);}

        $total = StockUnitRates::where('sur_branch_id', $rq->branchid)->count();
        $c_insert = StockUnitRates::where('sur_branch_id', $this->branch_id)->count();
        return parent::displayData(['total' => $total, 'insert' => $c_insert, 'flag' => $flag], $this->success_stat);
    }

    public function stockSet($rq)
    {
        $branch_stocks = BranchStocks::where('bs_prd_id', $rq->prd_id)
            ->where('bs_branch_id', $this->branch_id)->first();

        $company_stocks = CompanyStocks::where('cmp_prd_id', $rq->prd_id)
            ->first();

        if ($branch_stocks) {
            return parent::displayData(['stock' => 0, 'cmp_stock_id' => $company_stocks->cmp_stock_id], $this->success_stat);
        } else {
            return parent::displayData(['stock' => 1, 'cmp_stock_id' => $company_stocks->cmp_stock_id], $this->success_stat);
        }
    }

    public function productPurchaseSearchNew(Request $rq)
    {

        $results = [];

        if ($rq->keyword) {
            $rltn = ['units' => function ($qr) {
                $qr->select('unit_id', 'unit_code', 'unit_display', 'unit_name', 'unit_type', 'unit_base_qty', 'produnit_id', 'produnit_ean_barcode');
            },
                'batches' => function ($qr) {
                    $qr->select('sb_batch_code', 'sb_manufacture_date', 'sb_expiry_date');
                },
            ];

            $results = Product::select('prd_id', 'prd_name', 'prd_alias', 'prd_barcode', 'prd_ean', 'prd_tax', 'prd_base_unit_id', 'cmp_stock_id')
                ->where('prd_type', 1)
                ->where('prd_name', 'like', '%' . $rq->keyword . '%')->with($rltn)
                ->Join('company_stocks', function ($join) {
                    $join->on('products.prd_id', 'company_stocks.cmp_prd_id');
                })->groupBy('products.prd_id')->take(60)->get();

            foreach ($results as $k => $row) {

                $duid = trim($row['prd_base_unit_id']);

                $branch_stocks = BranchStocks::select('branch_stock_id', 'bs_stock_id', 'bs_prd_id', 'bs_prate', 'bs_stock_quantity', 'bs_avg_prate')
                    ->where('bs_prd_id', $row['prd_id'])->where('bs_branch_id', $this->branch_id)->first();
                $results[$k]['purchase_rate'] = isset($branch_stocks->bs_prate) ? $branch_stocks->bs_prate : 0;
                $results[$k]['current_stock'] = isset($branch_stocks->bs_stock_quantity) ? $branch_stocks->bs_stock_quantity : 0;
                $results[$k]['avg_rate'] = isset($branch_stocks->bs_avg_prate) ? $branch_stocks->bs_avg_prate : 0;
                $results[$k]['branch_stock_id'] = isset($branch_stocks->branch_stock_id) ? $branch_stocks->branch_stock_id : 0;
                $results[$k]['last_purchase_date'] = $this->findLastPurchDate($branch_stocks);
                foreach ($results[$k]['units'] as $key => $unit) {

                    if ($unit['unit_id'] == $row['prd_base_unit_id']) {
                        $results[$k]['prd_base_unit_name'] = $unit['unit_code'];
                        $results[$k]['prd_base_unit_name_master'] = $unit['unit_name'];
                        $results[$k]['unit_display'] = $unit['unit_display'];
                    }
                    $unit = $unit->toArray();

                    $ean = ProdUnit::select('produnit_ean_barcode')->where('produnit_unit_id', $unit['unit_id'])->where('produnit_prod_id', $row['prd_id'])->first();
                    $produnit_ean_barcode = isset($ean->produnit_ean_barcode) ? $ean->produnit_ean_barcode : '';

                    $unit_rate = StockUnitRates::select('sur_unit_rate')
                        ->where('sur_branch_id', $this->branch_id)
                        ->where('sur_unit_id', $unit['unit_id'])
                        ->where('sur_stock_id', $row['cmp_stock_id'])->first();

                    $unit['unit_rate'] = isset($unit_rate->sur_unit_rate) ? $unit_rate->sur_unit_rate : 0;
                    $unit['produnit_ean_barcode'] = isset($produnit_ean_barcode) ? $produnit_ean_barcode : 0;

                    if ($duid == $unit['unit_id']) {
                        $results[$k]['ean'] = $produnit_ean_barcode;
                    }
                    $results[$k]['units'][$key] = $unit;
                }

                if ($this->usr_type == 3) {
                    $results[$k]['branch_id'] = [];
                } else {
                    $branches = BranchStocks::select('bs_branch_id')->where('bs_stock_id', $row['bs_stock_id'])->where('bs_branch_id', '>', 0)->get()->toArray();
                    $results[$k]['branch_id'] = array_column($branches, 'bs_branch_id');
                }
            }
        }

        return parent::displayData($results, $this->success_stat);
    }

    public function stockSearchDamage(Request $rq)
    {
        $data = Product::join('branch_stocks', 'products.prd_id', '=', 'branch_stocks.bs_prd_id')->where('products.prd_name', 'like', '%' . $rq->keyword . '%')->where('branch_stocks.bs_branch_id', '=', $this->branch_id)->get();
        return response()->json(['data' => $data, 'status' => $this->success_stat]);
    }

    public function getGodownStock(Request $rq)
    {
        if ($rq->gd_id == 0) {
            $data = BranchStocks::where('bs_prd_id', '=', $rq->prd_id)->where('bs_branch_id', '=', $this->branch_id)->get();
        } else {
            $data = GodownStock::where('gs_prd_id', $rq->prd_id)->where('gs_godown_id', $rq->gd_id)->get();
        }
        return response()->json(['data' => $data, 'status' => $this->success_stat]);
    }

    public function getAllGodownsStock(Request $rq)
    {
        $gdData = [];
        if ($gd_name = isset($rq['gd_name']) ? $rq['gd_name'] : 0) {
            $gdData = GodownMaster::select('gd_id', 'gd_name', 'gd_code')->where('gd_name', 'like', '%' . $gd_name . '%')
                ->where('branch_id', $rq->branch_id)->take(100)->get();
        } else {
            $gdData = GodownMaster::select('gd_id', 'gd_name', 'gd_code')->where('branch_id', $rq->branch_id)->get();
        }
        $gdData[] = ['gd_id' => 0,
            'gd_name' => 'Shop',
            'gd_code' => 'SP'];
        return parent::displayData($gdData, $this->success_stat);
    }

    public function getAllGodownsItemwise(Request $rq)
    {
        $gdData = [];
        if ($gd_name = isset($rq['gd_name']) ? $rq['gd_name'] : 0) {
            $gdData = GodownMaster::join('godown_stocks', 'godown_master.gd_id', '=', 'godown_stocks.gs_godown_id')->where('gd_name', 'like', '%' . $gd_name . '%')->where('godown_stocks.gs_prd_id', $rq->prd_id)
                ->where('godown_stocks.branch_id', $rq->branch_id)->take(100)->get();
        } else {
            $gdData = GodownMaster::join('godown_stocks', 'godown_master.gd_id', '=', 'godown_stocks.gs_godown_id')
                ->where('godown_stocks.gs_prd_id', $rq->prd_id)->where('godown_stocks.branch_id', $rq->branch_id)->take(100)->get();
        }

        return parent::displayData($gdData, $this->success_stat);
    }
    public function addDamage(Request $rq)
    {
        $prdlists = $rq->prdlist;
        $damage_id = DaMage::count() + 1;
        foreach ($prdlists as $prdlist) {
            $data = BranchStocks::where('bs_prd_id', '=', $prdlist['prd_id'])->where('bs_branch_id', '=', $this->branch_id)->first();
            $resData[] = array(
                'id' => $data->bs_prd_id,
                'bs_stock_quantity' => $data->bs_stock_quantity,
                'bs_stock_quantity_gd' => $data->bs_stock_quantity_gd,
                'bs_prate' => $data->bs_prate,
            );
            if ($prdlist['gsl_from'] != 0) {
                $data_gd = GodownStock::where('gs_prd_id', $prdlist['prd_id'])->where('gs_godown_id', $prdlist['gsl_from'])->where('branch_id', '=', $this->branch_id)->first();

                $resDataGd[] = array(
                    'gs_id' => $data_gd->gs_id,
                    'gs_prd_id' => $data_gd->gs_prd_id,
                    'gs_qty' => $data_gd->gs_qty,
                );

                GodownStock::where('gs_prd_id', $prdlist['prd_id'])->where('gs_godown_id', $prdlist['gsl_from'])->where('branch_id', '=', $this->branch_id)->update(['gs_qty' => DB::raw('gs_qty -' . $prdlist['qty'])]);
                BranchStocks::where('bs_prd_id', '=', $prdlist['prd_id'])->where('bs_branch_id', '=', $this->branch_id)->update(['bs_stock_quantity' => DB::raw('bs_stock_quantity -' . $prdlist['qty']), 'bs_stock_quantity_gd' => DB::raw('bs_stock_quantity_gd -' . $prdlist['qty'])]);
                BranchStocks::where('bs_prd_id', '=', $prdlist['prd_id'])->where('bs_branch_id', '=', 0)->update(['bs_stock_quantity' => DB::raw('bs_stock_quantity -' . $prdlist['qty']), 'bs_stock_quantity_gd' => DB::raw('bs_stock_quantity_gd -' . $prdlist['qty'])]);

                DaMage::create(
                    ['damage_id' => $damage_id, 'damage_stock_id' => $data->bs_stock_id, 'damage_prod_id' => $data->bs_prd_id,
                        'damage_qty' => $prdlist['qty'], 'damage_rem_qty' => $prdlist['qty'], 'damage_purch_rate' => $data->bs_prate,
                        'damage_notes' => 'nil', 'damage_serial' => 'to implement',
                        'damage_date' => date('Y-m-d', strtotime($prdlist['damage_date'])), 'damage_added_by' => 'to implement', 'damage_flags' => 'to implement',
                        'damage_unit_id' => 'to implement', 'damage_unit_id' => 'to implement', 'branch_id' => $this->branch_id,
                        'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6),
                    ]
                );

            } else {
                BranchStocks::where('bs_prd_id', '=', $prdlist['prd_id'])->where('bs_branch_id', '=', $this->branch_id)->update(['bs_stock_quantity' => DB::raw('bs_stock_quantity -' . $prdlist['qty']), 'bs_stock_quantity_shop' => DB::raw('bs_stock_quantity_shop -' . $prdlist['qty'])]);
                BranchStocks::where('bs_prd_id', '=', $prdlist['prd_id'])->where('bs_branch_id', '=', 0)->update(['bs_stock_quantity' => DB::raw('bs_stock_quantity -' . $prdlist['qty']), 'bs_stock_quantity_shop' => DB::raw('bs_stock_quantity_shop -' . $prdlist['qty'])]);

                DaMage::create(
                    ['damage_id' => $damage_id, 'damage_stock_id' => $data->bs_stock_id, 'damage_prod_id' => $data->bs_prd_id,
                        'damage_qty' => $prdlist['qty'], 'damage_rem_qty' => $prdlist['qty'], 'damage_purch_rate' => $data->bs_prate,
                        'damage_notes' => 'nil', 'damage_serial' => 'to implement',
                        'damage_date' => date('Y-m-d', strtotime($prdlist['damage_date'])), 'damage_added_by' => 'to implement', 'damage_flags' => 'to implement',
                        'damage_unit_id' => 'to implement', 'damage_unit_id' => 'to implement', 'branch_id' => $this->branch_id,
                        'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6),
                    ]
                );

            }
        }
        return parent::displayData($resData, $this->success_stat);
    }

    public function listDamage(Request $rq)
    {

        $data = DaMage::join('products', 'products.prd_id', '=', 'damages.damage_prod_id')->get();
        return response()->json(['data' => $data, 'status' => $this->success_stat]);

    }

}
