<?php
//  created by deepu

namespace App\Http\Controllers\Api\Stock;

use App\Http\Controllers\Api\ApiParent;
use App\Models\Company\Acc_branch;
use App\Models\Godown\GodownStock;
use App\Models\Godown\GodownStockLog;
use App\Models\Product\Product;
use App\Models\Stocks\BranchStockLogs;
use App\Models\Stocks\BranchStocks;
use App\Models\Stocks\CompanyStocks;
use App\Models\Stocks\OpeningStockLog;
use App\Models\Stocks\StockBatches;
use App\Models\Stocks\StockDailyUpdates;
use App\Models\Stocks\StockUnitRates;
use DB;
use Illuminate\Http\Request;
use Validator;

class OpeningStockController extends ApiParent
{

    public function openStockValidation($stock)
    {
        $prd_id = isset($stock['prd_id']) ? $stock['prd_id'] : 0;
        $stock_id = isset($stock['stock_id']) ? $stock['stock_id'] : 0;
        $opening_quantity = isset($stock['opening_quantity']) ? $stock['opening_quantity'] : 0;
        $purchase_rate = isset($stock['purchase_rate']) ? $stock['purchase_rate'] : 0;
        $mrp_rates = isset($stock['mrp_rates']) ? $stock['mrp_rates'] : 0;

        $prateqty = isset($stock['purchase_rate_qty']) ? $stock['purchase_rate_qty'] : 0;
        if ($prd_id && $stock_id && $opening_quantity && !empty($mrp_rates) && $prateqty) {

            if ($prateqty > 1 && $purchase_rate > 0) {
                $stock['purchase_rate'] = round($purchase_rate / $prateqty);
            }
            return $stock;
        } else {

            return parent::displayError('Invalid Request11');
        }

    }

    public function openStocks(Request $rq)
    {

        $open_stocks = isset($rq->stocks) ? $rq->stocks : [];
        if (!empty($open_stocks)) {
            foreach ($open_stocks as $stock_data) {

                $this->openStockStart($stock_data);
            }
            return parent::displayMessage('Stock Updated');
        } else {
            return parent::displayError('Invalid Request');
        }
    }

    public function openStockStart($stk)
    {

        if (isset($stk['if_purchase'])) {
            $this->branch_id = $stk['branch_id'];
            $this->usr_id = $stk['usr_id'];
            $stock = $stk;
        } else {

            $stock = $this->openStockValidation($stk);
        }
        $company = CompanyStocks::where('cmp_prd_id', $stock['prd_id'])->where('cmp_stock_id', $stock['stock_id'])->get()->toArray();
        $bs_stock_id = isset($company[0]['cmp_stock_id']) ? trim($company[0]['cmp_stock_id']) : 0;
        if ($bs_stock_id) {
            $bs = BranchStocks::where('bs_stock_id', $bs_stock_id)->where('bs_branch_id', $this->branch_id)->first();
            if (isset($bs->bs_stock_id)) {
                return $this->updateStock($stock, $bs);
            } else {
                return $this->createStock($bs_stock_id, $stock);
            }
        }
        return 0;

    }

    public function createStock($bs_stock_id, $stock)
    {

        $stk_stat = app('App\Http\Controllers\Api\Stock\StockController')->isStockProduct($stock['prd_id']);
        if ($stk_stat) {

        } else {
            $stock['opening_quantity'] = 0;
        }
        $gd_id = isset($stock['gd_id']) ? $stock['gd_id'] : 0;
        if ($gd_id > 0) {
            $shop_qty = 0;
            $gd_qty = $stock['opening_quantity'];
        } else {
            $shop_qty = $stock['opening_quantity'];
            $gd_qty = 0;
        }
        $tot_qty = $shop_qty + $gd_qty;

        $branchStk["bs_stock_id"] = $bsLog['bsl_stock_id'] = $stkRates["sur_stock_id"] = $bs_stock_id;
        $branchStk["bs_prd_id"] = $bsLog['bsl_prd_id'] = $stkRates["sur_prd_id"] = $stock['prd_id'];
        $branchStk["bs_prate"] = $bsLog['bsl_prate'] = $bsLog['bsl_avg_prate'] = $branchStk["bs_avg_prate"] = $stock['purchase_rate'];
        $branchStk["bs_srate"] = isset($stock['sales_rate']) ? $stock['sales_rate'] : 0;
        $branchStk["bs_branch_id"] = $stkRates["sur_branch_id"] = $this->branch_id;
        $branchStk["bs_stock_quantity"] = $tot_qty;
        $branchStk["bs_stock_quantity_shop"] = $bsLog['bsl_shop_quantity'] = $shop_qty;
        $branchStk["bs_stock_quantity_gd"] = $bsLog['bsl_gd_quantity'] = $gd_qty;
        $branchStk["server_sync_time"] = $stkRates["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);
        //STEP -1 adding branch stock //
        $bstocks = BranchStocks::create($branchStk);
        //adding branch stock end//

        //STEP -2 keeping branch logs ends
        $bsLog['bsl_branch_stock_id'] = $bstocks->branch_stock_id;
        $bsLog['bsl_branch_id'] = $this->branch_id;
        $bsLog['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
        BranchStockLogs::create($bsLog);

        $company = BranchStocks::where('bs_stock_id', $bs_stock_id)->where('bs_branch_id', 0)->first();
        $new_p_amount = $tot_qty * $stock['purchase_rate'];

        $old_stockp_amount = $company->bs_stock_quantity * $company->bs_avg_prate;
        if ($new_p_amount > 0) {
            $bs_avg_prate2 = (abs($old_stockp_amount) + abs($new_p_amount)) / (abs($company->bs_stock_quantity) + abs($tot_qty));
        } else {
            $bs_avg_prate2 = $company->bs_avg_prate;
        }

        //calcCompanyAvgPrate ends

        //STEP -5 related changes in company stocks
        if ($this->branch_id != 0) {
            BranchStocks::where('bs_stock_id', $company->bs_stock_id)->where('bs_branch_id', 0)->update(
                [
                    'bs_avg_prate' => $bs_avg_prate2,
                    'bs_stock_quantity_shop' => \DB::raw("bs_stock_quantity_shop+$shop_qty"),
                    'bs_stock_quantity_gd' => \DB::raw("bs_stock_quantity_gd+$gd_qty"),
                    'bs_stock_quantity' => \DB::raw("bs_stock_quantity+$tot_qty"),
                    'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6),
                ]);
        }

        CompanyStocks::where('cmp_stock_id', $company->bs_stock_id)->update(
            [
                'cmp_avg_prate' => $bs_avg_prate2,
                'cmp_stock_quantity' => \DB::raw("cmp_stock_quantity+$tot_qty"),
                'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6),
            ]);
        // related  changes in company stocks ends
        return $this->proceedstockOperations($stock, $bstocks, $gd_qty, $stock['gd_id']);

    }

    public function updateStock($stock, $bs)
    {
        $stk_stat = app('App\Http\Controllers\Api\Stock\StockController')->isStockProduct($bs['bs_prd_id']);
        if (!$stk_stat) {
            $stock['opening_quantity'] = 0;
        }
        $gd_id = isset($stock['gd_id']) ? $stock['gd_id'] : 0;
        if ($gd_id > 0) {
            $shop_qty = 0;
            $gd_qty = $stock['opening_quantity'];
        } else {
            $shop_qty = $stock['opening_quantity'];
            $gd_qty = 0;
        }
        $tot_qty = $shop_qty + $gd_qty;
        // calcBranchAvgPrate start
        $old_p_amount = $bs->bs_stock_quantity * $bs->bs_avg_prate;
        $new_p_amount = $tot_qty * $stock['purchase_rate'];

        if ($new_p_amount > 0) {
            $bs_avg_prate = (abs($old_p_amount) + abs($new_p_amount)) / (abs($bs->bs_stock_quantity) + abs($tot_qty));
        } else {
            $bs_avg_prate = $bs->bs_avg_prate;
        }

        // calcBranchAvgPrate ends

        // updating branch_stocks  and keeping logs - start

        $bs['bs_stock_quantity_shop'] = $bs['bs_stock_quantity_shop'] + $shop_qty;
        $bs['bs_stock_quantity_gd'] = $bs['bs_stock_quantity_gd'] + $gd_qty;
        $bs['bs_stock_quantity'] = $bs['bs_stock_quantity'] + $tot_qty;

        if(isset($stock['sales_rate']))
        {
            $bs['bs_prate'] =$stock['purchase_rate'];
        }
        if(isset($stock['sales_rate']))
        {
            $bs['bs_srate'] = $stock['sales_rate'];
        }
        $bs['bs_avg_prate'] = $bs_avg_prate;
        $bs['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
        $bs->save();

        $bsLog['bsl_branch_stock_id'] = $bs['branch_stock_id'];
        $bsLog['bsl_stock_id'] = $bs['bs_stock_id'];
        $bsLog['bsl_prd_id'] = $bs['bs_prd_id'];
        $bsLog['bsl_shop_quantity'] = $shop_qty;
        $bsLog['bsl_gd_quantity'] = $gd_qty;
        $bsLog['bsl_prate'] = isset($stock['purchase_rate']) ? $stock['purchase_rate'] : 0;
        $bsLog['bsl_srate'] = isset($stock['sales_rate']) ? $stock['sales_rate'] : 0;
        $bsLog['bsl_avg_prate'] = $bs_avg_prate;
        $bsLog["bsl_branch_id"] = $this->branch_id;
        $bsLog['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
        BranchStockLogs::create($bsLog);
        // updating branch_stocks  and keeping logs ends

        //calcCompanyAvgPrate start
        $company = BranchStocks::where('bs_stock_id', $bs->bs_stock_id)->where('bs_branch_id', 0)->first();

        $old_stockp_amount = $company->bs_stock_quantity * $company->bs_avg_prate;
        if ($new_p_amount > 0) {
            $bs_avg_prate2 = (abs($old_stockp_amount) + abs($new_p_amount)) / (abs($company->bs_stock_quantity) + abs($tot_qty));
        } else {
            $bs_avg_prate2 = $company->bs_avg_prate;
        }

        //calcCompanyAvgPrate ends

        // related changes in company stocks
        if ($this->branch_id != 0) {
            BranchStocks::where('bs_stock_id', $company->bs_stock_id)->where('bs_branch_id', 0)->update(
                [

                    'bs_avg_prate' => $bs_avg_prate2,
                    'bs_stock_quantity_shop' => \DB::raw("bs_stock_quantity_shop+$shop_qty"),
                    'bs_stock_quantity_gd' => \DB::raw("bs_stock_quantity_gd+$gd_qty"),
                    'bs_stock_quantity' => \DB::raw("bs_stock_quantity+$tot_qty"),
                    'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6),
                ]);
        }

        CompanyStocks::where('cmp_stock_id', $company->bs_stock_id)->update(
            [
                'cmp_avg_prate' => $bs_avg_prate2,
                'cmp_stock_quantity' => \DB::raw("cmp_stock_quantity+$tot_qty"),
                'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6),
            ]);
        // related  changes in company stocks ends
        return $this->proceedstockOperations($stock, $bs, $gd_qty, $stock['gd_id']);

    }
    public function proceedstockOperations($stock, $bs, $gd_qty, $gd_id)
    {
        $sb_id = $expiry_date = null;
        $batch_quantity = $stock['opening_quantity'];
        if (isset($stock['expiry_date']) && isset($stock['manufacture_date']) && isset($stock['batch_code'])) {

            $sb_id = StockBatches::where('sb_batch_code', $stock['batch_code'])->pluck('sb_id')->first();
            $expiry_date = isset($stock['expiry_date']) ? date('Y-m-d', strtotime($stock['expiry_date'])) : 0;
            if ($sb_id) {

                StockBatches::where('sb_id', $sb_id)->update([
                    'sb_manufacture_date' => date('Y-m-d', strtotime($stock['manufacture_date'])),
                    'sb_expiry_date' => $expiry_date,
                    'sb_quantity' => \DB::raw("sb_quantity+ $batch_quantity"),
                    'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6),
                ]);
            } else {

                $newbatch = [
                    'sb_batch_code' => $stock['batch_code'],
                    'sb_branch_stock_id' => $bs['branch_stock_id'],
                    'sb_stock_id' => $bs['bs_stock_id'],
                    'sb_prd_id' => $bs['bs_prd_id'],
                    'sb_quantity' => $batch_quantity,
                    'sb_manufacture_date' => date('Y-m-d', strtotime($stock['manufacture_date'])),
                    'sb_expiry_date' => $expiry_date,
                    'branch_id' => $this->branch_id,
                    'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6),
                ];
                $batches = StockBatches::create($newbatch);
                $sb_id = $batches->sb_id;
            }

            if ($sb_id) {
                if (isset($stock['if_purchase'])) {
                    // print_r($stock);die();
                    //  purchaseSub::where('purchsub_id', $stock['purchsub_id'])
                    //  ->update(['purchsub_batch_id' => $sb_id, 'purchsub_expiry' => $expiry_date]);
                } else {
                    BranchStocks::where('branch_stock_id', $bs->branch_stock_id)->update(['bs_batch_id' => $sb_id, 'bs_expiry' => $expiry_date
                        , 'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)]);
                }
            }

        }
        $opening_stock = isset($stock['if_purchase']) ? 0 : 1;

        $cal_date = isset($stock['cal_date']) ? $stock['cal_date'] : 0;
        $this->insertInDailyStocks($bs, $stock['opening_quantity'], $gd_id, $sb_id, $opening_stock, $cal_date);

        if (isset($stock['mrp_rates']) && !empty($stock['mrp_rates'])) {
            foreach ($stock['mrp_rates'] as $unit) {

                $urate['branch_stock_id'] = $bs['branch_stock_id'];
                $urate['sur_unit_id'] = $unit['unit_id'];

                if ($stckrate = StockUnitRates::select('*')->where($urate)->first()) {
                    $stckrate['sur_unit_rate'] = $unit['unit_rate'];
                    $stckrate->save();
                } else {

                    $urate['sur_prd_id'] = $bs['bs_prd_id'];
                    $urate['sur_stock_id'] = $bs['bs_stock_id'];
                    $urate['sur_unit_rate'] = $unit['unit_rate'];
                    $urate["sur_branch_id"] = $this->branch_id;
                    $urate["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);
                    StockUnitRates::create($urate);
                }
            }
        }
        if ($gd_qty > 0) {
            $this->GoddownStocksChanges($bs, $gd_id, $gd_qty);
        }

        $return = ['branch_stock_id' => $bs['branch_stock_id'],
            'batch_id' => $sb_id,
            'expiry' => $expiry_date];
        return $return;
    }

    public function insertInDailyStocks($bs, $new_qty, $gd_id = 0, $batch_id = 0, $opening_stock, $cal_date = null)
    {
       

        if ($opening_stock) {
            $branch = Acc_branch::where('branch_id', $this->branch_id)->first();
            $date = date('Y-m-d', (strtotime('-1 day', strtotime($branch['branch_open_date']))));
        } else {
            $date = $cal_date ? $cal_date : date('Y-m-d');
        }

        $dateExist = StockDailyUpdates::select('*')->where('sdu_branch_stock_id', $bs['branch_stock_id'])->where('sdu_stock_id', $bs['bs_stock_id'])
            ->where('sdu_prd_id', $bs['bs_prd_id'])->where('sdu_date', $date)->first();

        if ($dateExist) {
            StockDailyUpdates::where('sdu_branch_stock_id', $bs['branch_stock_id'])->where('sdu_stock_id', $bs['bs_stock_id'])
                ->where('sdu_prd_id', $bs['bs_prd_id'])->where('sdu_date', $date)->increment('sdu_stock_quantity', $new_qty);

            StockDailyUpdates::where('sdu_branch_stock_id', $bs['branch_stock_id'])->where('sdu_stock_id', $bs['bs_stock_id'])
                ->where('sdu_prd_id', $bs['bs_prd_id'])->where('sdu_date', $date)
                ->update(['server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)]);

        } else {
            $ds['sdu_branch_stock_id'] = $bs['branch_stock_id'];
            $ds['sdu_stock_id'] = $bs['bs_stock_id'];
            $ds['sdu_prd_id'] = $bs['bs_prd_id'];
            $ds['sdu_date'] = $date;
            $ds['sdu_stock_quantity'] = $new_qty;
            $ds['sdu_gd_id'] = $gd_id;
            $ds['sdu_batch_id'] = $batch_id;
            $ds['branch_id'] = $this->branch_id;
            $ds['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            StockDailyUpdates::create($ds);

        }

        if ($opening_stock) {
            $opLog['opstklog_branch_stock_id'] = $bs['branch_stock_id'];
            $opLog['opstklog_stock_id'] = $bs['bs_stock_id'];
            $opLog['opstklog_prd_id'] = $bs['bs_prd_id'];
            $opLog['opstklog_date'] = date('Y-m-d');

            $opLog['opstklog_stock_quantity_add'] = $new_qty;
            $opLog['opstklog_gd_id'] = $gd_id;
            $opLog['opstklog_batch_id'] = $batch_id;
            $opLog['opstklog_prate'] = $bs['bs_prate'];
            $opLog['opstklog_srate'] = $bs['bs_srate'];
            $opLog['branch_id'] = $this->branch_id;
            $opLog['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);

            $prdUnit = Product::select('prd_base_unit_id')
                ->where('prd_id', $bs['bs_prd_id'])
                ->first();
            $opLog['opstklog_unit_id'] = $prdUnit['prd_base_unit_id'];

            OpeningStockLog::create($opLog);
        }

    }

    public function GoddownStocksChanges($bd, $gd_id, $gd_qty)
    {
        $sl['gsl_from'] = 0;
        $sl['gsl_branch_stock_id'] = $ns['gs_branch_stock_id'] = $bd['branch_stock_id'];
        $sl['gsl_stock_id'] = $ns['gs_stock_id'] = $bd['bs_stock_id'];
        $sl['gsl_prd_id'] = $ns['gs_prd_id'] = $bd['bs_prd_id'];
        $whr = $ns;
        $sl['gsl_added_by'] = $ns['gs_added_by'] = $this->usr_id;
        $sl['branch_id'] = $ns['branch_id'] = $this->branch_id;

        $sl['gsl_to'] = $whr['gs_godown_id'] = $ns['gs_godown_id'] = $gd_id;
        $sl['gsl_qty'] = $ns['gs_qty'] = $gd_qty;

        if ($gdstck = GodownStock::select('*')->where($whr)->first()) {
            $gdstck['gs_qty'] = $gdstck['gs_qty'] + $gd_qty;

            $gdstck->server_sync_time = date('ymdHis') . substr(microtime(), 2, 6);
            $gdstck->save();
        } else {
            $ns['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            $gdstck = GodownStock::create($ns);
        }
        $sl['gsl_gdwn_stock_id'] = $gdstck->gs_id;
        $sl['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
        GodownStockLog::create($sl);

    }

    public function validateStockRevert($request)
    {
        // print_r($request->toArray());
        $stockArray = [
            'prd_id' => $request['prd_id'],
            'stock_id' => $request['stock_id'],
            'qty' => $request['qty'],
            'rate' => $request['rate'],
            'batch_id' => $request['batch_id'],
            'gd_id' => $request['gd_id'],

        ];
        $this->remooveStock($stockArray);
    }

    public function revertOpeningStock(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            [
                'prd_id' => 'required|exists:products',
                'stock_id' => 'required',
                'qty' => 'required|numeric',
                'rate' => 'required|numeric',
                'batch_id' => 'required|numeric',
                'stock_id' => 'required',

            ],
            [
                'prd_id.required' => 'Required',
                'prd_id.exists' => 'Invalid',
                'stock_id.required' => 'Required',
                'qty.required' => 'Required',
                'rate.required' => 'Required',
                'batch_id.required' => 'Required',

            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }

        $this->remooveFromStock($request);
        return parent::displayMessage('Stock Remooved');
    }

    public function remooveFromStock($stk)
    {

        if (isset($stk['if_purchase'])) {
            $this->branch_id = $stk['branch_id'];
            $this->usr_id = $stk['usr_id'];

            $bs = BranchStocks::where('bs_prd_id', $stk['prd_id'])->
                where('bs_stock_id', $stk['stock_id'])->where('bs_branch_id', $this->branch_id)->first();
        } else {
            $bs = BranchStocks::where('bs_prd_id', $stk['prd_id'])->
                where('branch_stock_id', $stk['stock_id'])->where('bs_branch_id', $this->branch_id)->first();
        }

        if (isset($bs->bs_stock_id)) {
            return $this->remooveStock($stk, $bs);
        }
        return $bs;

    }

    public function remooveStock($stock, $bs)
    {

        $gd_id = isset($stock['gd_id']) ? $stock['gd_id'] : 0;
        if ($gd_id > 0) {
            $shop_qty = 0;
            $gd_qty = $stock['qty'];
        } else {
            $shop_qty = $stock['qty'];
            $gd_qty = 0;
        }
        $purchase_qty = $shop_qty + $gd_qty;
        // calcBranchAvgPrate start

        $old_p_amount = $bs->bs_stock_quantity * $bs->bs_avg_prate;
        $purchase_amount = $purchase_qty * $stock['rate'];

        $qty_remains = $bs->bs_stock_quantity - $purchase_qty;
        if ($purchase_amount > 0 && $qty_remains > 0) {
            $bs_avg_prate = ($old_p_amount - $purchase_amount) / ($bs->bs_stock_quantity - $purchase_qty);
        } else {
            $bs_avg_prate = $bs->bs_avg_prate;
        }

        // calcBranchAvgPrate ends
        // updating branch_stocks  and keeping logs - start

        $bs['bs_stock_quantity_shop'] = $bs['bs_stock_quantity_shop'] - $shop_qty;
        $bs['bs_stock_quantity_gd'] = $bs['bs_stock_quantity_gd'] - $gd_qty;
        $bs['bs_stock_quantity'] = $bs['bs_stock_quantity'] - $purchase_qty;
        $bs['bs_avg_prate'] = $bs_avg_prate;
        $bs->save();

        $bsLog['bsl_branch_stock_id'] = $bs['branch_stock_id'];
        $bsLog['bsl_stock_id'] = $bs['bs_stock_id'];
        $bsLog['bsl_prd_id'] = $bs['bs_prd_id'];
        $bsLog['bsl_shop_quantity'] = $shop_qty;
        $bsLog['bsl_gd_quantity'] = $gd_qty;
        $bsLog['bsl_revert'] = 1;
        $bsLog['bsl_avg_prate'] = $bs_avg_prate;
        $bsLog['bsl_branch_id'] = $this->branch_id;
        BranchStockLogs::create($bsLog);
        // updating branch_stocks  and keeping logs ends

        //calcCompanyAvgPrate start
        $company = BranchStocks::where('bs_stock_id', $bs->bs_stock_id)->where('bs_branch_id', 0)->first();

        $old_stockp_amount = $company->bs_stock_quantity * $company->bs_avg_prate;

        if ($purchase_amount > 0) {
            $bs_avg_prate2 = $company->bs_avg_prate;
            if (($company->bs_stock_quantity - $purchase_qty) > 0) {
                $bs_avg_prate2 = ($old_stockp_amount - $purchase_amount) / ($company->bs_stock_quantity - $purchase_qty);
            }

        } else {
            $bs_avg_prate2 = $company->bs_avg_prate;
        }

        //calcCompanyAvgPrate ends
        // related changes in company stocks

        if ($this->branch_id != 0) {
            BranchStocks::where('bs_stock_id', $company->bs_stock_id)->where('bs_branch_id', 0)->update(
                [
                    'bs_avg_prate' => $bs_avg_prate2,
                    'bs_stock_quantity_shop' => \DB::raw("bs_stock_quantity_shop-$shop_qty"),
                    'bs_stock_quantity_gd' => \DB::raw("bs_stock_quantity_gd-$gd_qty"),
                    'bs_stock_quantity' => \DB::raw("bs_stock_quantity-$purchase_qty"),
                ]);
        }

        CompanyStocks::where('cmp_stock_id', $company->bs_stock_id)->update(
            [
                'cmp_avg_prate' => $bs_avg_prate2,
                'cmp_stock_quantity' => \DB::raw("cmp_stock_quantity-$purchase_qty"),
            ]);
        // related  changes in company stocks ends
        return $this->revertStockOperations($stock, $bs, $gd_qty, $stock['gd_id']);

    }

    public function revertStockOperations($stock, $bs, $gd_qty, $gd_id)
    {
        $opening_stock = isset($stock['if_purchase']) ? 0 : 1;
        $opstklog_id = isset($stock['opstklog_id']) ? $stock['opstklog_id'] : 0;
        $this->remooveFromDailyStocks($bs, $stock['qty'], $opstklog_id, $opening_stock, $stock['cal_date']);
        $batch_quantity = $stock['qty'];
        $batch_id = $stock['batch_id'] > 0 ? $stock['batch_id'] : 0;

        if ($batch_id) {
            StockBatches::where('sb_id', $batch_id)->update(['sb_quantity' => \DB::raw("sb_quantity-$batch_quantity")]);
        }

        if ($gd_qty > 0) {
            $this->RemooveFromGoddownStocks($bs, $gd_id, $gd_qty);
        }

    }

    public function remooveFromDailyStocks($bs, $qty, $opstklog_id, $opening_stock, $cal_date = null)
    {

        $date = $cal_date ? $cal_date : date('Y-m-d');
        $dateExist = StockDailyUpdates::select('*')->where('sdu_branch_stock_id', $bs['branch_stock_id'])->where('sdu_stock_id', $bs['bs_stock_id'])
            ->where('sdu_prd_id', $bs['bs_prd_id'])->where('sdu_date', $date)->first();

        if ($dateExist) {
            StockDailyUpdates::where('sdu_branch_stock_id', $bs['branch_stock_id'])->where('sdu_stock_id', $bs['bs_stock_id'])
                ->where('sdu_prd_id', $bs['bs_prd_id'])->where('sdu_date', $date)->decrement('sdu_stock_quantity', $qty);

        } else {
            $ds['sdu_branch_stock_id'] = $bs['branch_stock_id'];
            $ds['sdu_stock_id'] = $bs['bs_stock_id'];
            $ds['sdu_prd_id'] = $bs['bs_prd_id'];
            $ds['sdu_date'] = $date;
            $ds['sdu_stock_quantity'] = "-" . $qty;
            $ds['branch_id'] = $this->branch_id;
            StockDailyUpdates::create($ds);

        }
        // $opLog['opstklog_branch_stock_id'] = $bs['branch_stock_id'];
        // $opLog['opstklog_stock_id'] = $bs['bs_stock_id'];
        // $opLog['opstklog_prd_id'] = $bs['bs_prd_id'];
        // $opLog['opstklog_date'] = $date;
        // $opLog['opstklog_stock_quantity_rem'] = $qty;
        // $opLog['opstklog_type'] = 1;
        // $opLog['opstklog_prate'] = $bs['bs_prate'];
        // $opLog['opstklog_srate'] = $bs['bs_srate'];
        // $opLog['branch_id'] = $this->branch_id;
        // $prdUnit = Product::select('prd_base_unit_id')
        //     ->where('prd_id', $bs['bs_prd_id'])
        //     ->first();
        // $opLog['opstklog_unit_id'] = $prdUnit['prd_base_unit_id'];

        // OpeningStockLog::create($opLog);
        if ($opening_stock) {

            if ($opstklog_id > 0) {
                OpeningStockLog::where('opstklog_id', $opstklog_id)->update(['opstklog_type' => 1,
                    'opstklog_stock_quantity_rem' => $qty]);
            }
        }

    }

    public function RemooveFromGoddownStocks($bd, $gd_id, $gd_qty)
    {

        $whr['gs_branch_stock_id'] = $bd['branch_stock_id'];
        $whr['gs_stock_id'] = $bd['bs_stock_id'];
        $whr['gs_prd_id'] = $bd['bs_prd_id'];
        $whr['gs_godown_id'] = $gd_id;

        if ($gdstck = GodownStock::select('*')->where($whr)->first()) {
            $gdstck['gs_qty'] = $gdstck['gs_qty'] - $gd_qty;
            $gdstck->save();
            if ($sl = GodownStockLog::select('*')->where('gsl_gdwn_stock_id', $gdstck->gs_id)->first()) {
                $sl['gsl_flag'] = 0;
                $sl->save();
            }
        }
        return true;

    }

    public function listOpeningStock(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            [
                'prd_id' => 'required|exists:products',

            ],
            [
                'prd_id.required' => 'Required',
                'prd_id.exists' => 'Invalid',
            ]
        );

        if ($validator->fails()) {
            return parent::displayError($validator->messages(), $this->badrequest_stat);
        }

        $result = Product::select(
            'prd_id',
            'prd_name',
            'prd_barcode',
            'prd_ean',
            'opening_stock_log.opstklog_id',
            'opening_stock_log.opstklog_date as open_stock_date',
            'opening_stock_log.opstklog_stock_quantity_add as qty',
            'opening_stock_log.opstklog_prate as rate',
            'opening_stock_log.opstklog_gd_id as gd_id',
            'opening_stock_log.opstklog_batch_id as batch_id',
            'opening_stock_log.opstklog_branch_stock_id as stock_id',
            'opening_stock_log.opstklog_srate as mrp',

            DB::raw('erp_opening_stock_log.opstklog_stock_quantity_add*erp_opening_stock_log.opstklog_prate AS purchase_amt')

        )->join('branch_stocks', 'products.prd_id', 'branch_stocks.bs_prd_id')->join('opening_stock_log', 'branch_stocks.branch_stock_id',
            'opening_stock_log.opstklog_branch_stock_id')->where('opening_stock_log.opstklog_type', 0)
            ->where('prd_id', $request->prd_id)->where('branch_stocks.bs_branch_id', $request->branch_id)->get()->toArray();

        return parent::displayData($result, $this->badrequest_stat);

    }

    public function fixGdninOpenStock(Request $request)
    {
        $res = [];
        $openstock = OpeningStockLog::get();
        $branchlog = BranchStockLogs::get();
        $rr = [];
        $i = 0;
        foreach ($openstock as $key => $osl) {

            if ($this->checkIfRowMatches($osl['opstklog_prd_id'], $osl['opstklog_branch_stock_id'], $osl['opstklog_stock_quantity_add'], $osl['opstklog_date'])) {

                $prd_name = Product::where('prd_id', $osl['opstklog_prd_id'])->first()->prd_name;
                $rr['No.'] = $i + 1;

                $rr['qunatity'] = $osl['opstklog_stock_quantity_add'];
                $rr['name'] = $prd_name;
                $rr['date'] = $osl['opstklog_date'];

                $out[] = $rr;
                $i++;
            }

        }

        echo json_encode($out);die();
        return $res;
    }

    public function checkIfRowMatches($pid, $bid, $qty, $date)
    {

        $star_date = $date . " 00:00:00";
        $end_date = $date . " 23:59:59";

        $dat = BranchStockLogs::where('bsl_prd_id', $pid)->where('bsl_branch_stock_id', $bid)
            ->where('bsl_shop_quantity', '!=', 0)

        //->whereRaw("created_at between '$star_date' AND '$end_date' ")
            ->get();

        if (count($dat) > 0) {
            return 1;
        } else {
            return 0;
        }
    }

}
