<?php

namespace App\Http\Controllers\Api\Godown;

use App\Http\Controllers\Api\ApiParent;
use App\Models\Godown\GodownStock;
use App\Models\Godown\GodownStockLog;
use App\Models\Product\Units;
use App\Models\Stocks\BranchStocks;
use App\Models\Stocks\StockUnitRates;
use Illuminate\Http\Request;
use App\Models\Godown\GodownTransfer;
use validator;

class GodownTransferController extends ApiParent
{

    public function GodownTransfer(Request $request, $type)
    {

        switch ($type) {
            case 'add':
                return $this->addGodownTrans($request);
                break;
                case 'list':
                    return $this->listGodownTrans($request);
                    break;
            case 'edit':
                return $this->editGodownTrans($request);
                break;
        }
    }

    /**
     * @api {post} http://127.0.0.1:8000/api/godown_transfer/add Add Godown Stock Transfer
     * @apiName addGodownTrans
     * @apiGroup Godown
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {bigint} gsl_from Godown Transfer From Id (Required)
     * @apiParam {bigint} gsl_to Godown Transfer To Id (Required)
     * @apiParam {int} gsl_vchr_type Godown Voucher Type
     * @apiParam {string} trans_type Transfer type (G-G :- Godown to godown ,G-S : godown to shop ,S-G: Shop to godown)
     * @apiParam {array} trans_stocks Transfering Stock Details ( ex : [ { "prod_id":3 , "branch_stock_id" :5 ,"gsl_stock_id":2, "prod_unit_id" : 2,"prod_qty":2 }] )
     * @apiParam {string} gt_notes Godown Note
     *
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} message Success Message
     *
     * @apiError {String} error Error Message.
     *
     */

    public function addGodownTrans($request)
    {

        $validator = Validator::make(
            $request->all(),
            [
                'gsl_from' => 'required',
                'gsl_to' => 'required',

            ],
            [
                'gsl_from.required' => 'Required',
                'gsl_from.exists' => 'Invalid',
                'gsl_to.required' => 'Required',
                'gsl_to.exists' => 'Invalid',

            ]
        );

        if ($validator->fails()) {
            return parent::displayError($validator->messages(), $this->badrequest_stat);
        }

        if ($request['gsl_from'] == 0 && $request['gsl_to'] != 0) {
            $request->trans_type = 'S-G';
        } elseif ($request['gsl_from'] != 0 && $request['gsl_to'] == 0) {
            $request->trans_type = 'G-S';
        } elseif ($request['gsl_from'] != 0 && $request['gsl_to'] != 0) {
            $request->trans_type = 'G-G';
        } else {
            return parent::displayMessage('Invalid_Transfer', $this->badrequest_stat);
        }

        $date = date('y-m-d',strtotime(str_replace('-','/', $request['tranfer_date'])));
        $result = array();
        foreach ($request->trans_stocks as $k => $v) {
            $id = $v['branch_stock_id'];
            $unitQty = $this->isBaseUnit($v['prod_unit_id']);
            $result[$id][] = $v['prod_qty'] * $unitQty;
        }

        $trans_quantity = array();
        foreach ($result as $key => $value) {
            $trans_quantity[] = array('branch_stock_id' => $key, 'prod_qty' => array_sum($value));
        }

        switch ($request->trans_type) {
            case 'G-S':

                foreach ($trans_quantity as $key => $tq) {

                    $stockunit = GodownStock::where('gs_branch_stock_id', $tq['branch_stock_id'])->where('gs_godown_id', $request['gsl_from'])->first();
                    if ($stockunit) {
                        $gdTransLog['gsl_gdwn_stock_id'] = $stockunit->gs_id;

                        $stockunit->gs_qty = $stockunit->gs_qty - $tq['prod_qty'];
                        $stockunit->server_sync_time = $this->server_sync_time;
                        $stockunit->save();

                        $brhStk = BranchStocks::find($tq['branch_stock_id']);

                        $brhStk->bs_stock_quantity_shop = $brhStk->bs_stock_quantity_shop + $tq['prod_qty'];
                        $brhStk->bs_stock_quantity_gd = $brhStk->bs_stock_quantity_gd - $tq['prod_qty'];
                        $brhStk->server_sync_time = $this->server_sync_time;
                        $brhStk->save();
                    } else {
                        $prod = BranchStocks::where('branch_stock_id', $tq['branch_stock_id'])->first();

                        $prod->bs_stock_quantity_shop = $prod->bs_stock_quantity_shop - $tq['prod_qty'];
                        $prod->bs_stock_quantity_gd = $prod->bs_stock_quantity_gd + $tq['prod_qty'];
                        $prod->server_sync_time = $this->server_sync_time;
                        $prod->save();

                        $date = date('y-m-d',strtotime(str_replace('-','/', $request['tranfer_date'])));

                        $stockunit = GodownStock::create([

                            'gs_godown_id' => $request->gsl_from,
                            'gs_branch_stock_id' => $tq['branch_stock_id'],
                            'gs_stock_id' => $prod['bs_stock_id'],
                            'gs_date' => $date,
                            'gs_prd_id' => $prod['bs_prd_id'],
                            'gs_qty' => -$tq['prod_qty'],
                            'branch_id' => $request->branch_id,
                            'server_sync_time' => $this->server_sync_time

                        ]);

                    }

                }

                break;
            case 'S-G':

                foreach ($trans_quantity as $key => $tq) {

                    $stockdet = GodownStock::where('gs_branch_stock_id', $tq['branch_stock_id'])->where('gs_godown_id', $request['gsl_to'])->first();

                    if ($stockdet) {
                        $gdTransLog['gsl_gdwn_stock_id'] = $stockdet->gs_id;

                        $stockdet->gs_qty = $stockdet->gs_qty + $tq['prod_qty'];
                        $stockdet->save();

                        $brhStk = BranchStocks::find($tq['branch_stock_id']);

                        $brhStk->bs_stock_quantity_shop = $brhStk->bs_stock_quantity_shop - $tq['prod_qty'];
                        $brhStk->bs_stock_quantity_gd = $brhStk->bs_stock_quantity_gd + $tq['prod_qty'];
                        $brhStk->server_sync_time = $this->server_sync_time;
                        $brhStk->save();
                    } else {
                        $prod = BranchStocks::where('branch_stock_id', $tq['branch_stock_id'])->first();

                        $prod->bs_stock_quantity_shop = $prod->bs_stock_quantity_shop - $tq['prod_qty'];
                        $prod->bs_stock_quantity_gd = $prod->bs_stock_quantity_gd + $tq['prod_qty'];
                        $prod->server_sync_time = $this->server_sync_time;
                        $prod->save();
                        $date = date('y-m-d',strtotime(str_replace('-','/', $request['tranfer_date'])));

                        $stockdet = GodownStock::create([
                            'gs_godown_id' => $request->gsl_to,
                            'gs_branch_stock_id' => $tq['branch_stock_id'],
                            'gs_stock_id' => $prod['bs_stock_id'],
                            'gs_date' => $date,
                            'gs_prd_id' => $prod['bs_prd_id'],
                            'gs_qty' => $tq['prod_qty'],
                            'branch_id' => $request->branch_id,
                            'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)
                        ]);
                        $gdTransLog['gsl_gdwn_stock_id'] = $stockdet->gs_id;
                    }

                }

                break;
            case 'G-G':

                foreach ($trans_quantity as $key => $tq) {

                    $prod = BranchStocks::where('branch_stock_id', $tq['branch_stock_id'])->first();

                    $stockfrom = GodownStock::where('gs_branch_stock_id', $tq['branch_stock_id'])->where('gs_godown_id', $request['gsl_from'])->first();

                    if ($stockfrom) {

                        $gdTransLog['gsl_gdwn_stock_id'] = $stockfrom->gs_id;

                        $stockfrom->gs_qty = $stockfrom->gs_qty - $tq['prod_qty'];
                        $stockfrom->server_sync_time = $this->server_sync_time;
                        $stockfrom->save();

                    } else {
                        $date = date('y-m-d',strtotime(str_replace('-','/', $request['tranfer_date'])));

                        $GdStock = GodownStock::create([

                            'gs_godown_id' => $request->gsl_from,
                            'gs_branch_stock_id' => $tq['branch_stock_id'],
                            'gs_stock_id' => $prod['bs_stock_id'],
                            'gs_date' => $date,
                            'gs_prd_id' => $prod['bs_prd_id'],
                            'gs_qty' => -$tq['prod_qty'],
                            'branch_id' => $request->branch_id,
                            'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)

                        ]);

                        $gdTransLog['gsl_gdwn_stock_id'] = $GdStock->gs_id;

                    }

                    $stockto = GodownStock::where('gs_branch_stock_id', $tq['branch_stock_id'])->where('gs_godown_id', $request['gsl_to'])->first();

                    if ($stockto) {

                        $gdTransLog['gsl_gdwn_stock_id'] = $stockto['gs_id'];
                        $stockto->gs_qty = $stockto->gs_qty + $tq['prod_qty'];
                        $stockto->server_sync_time = $this->server_sync_time;
                        $stockto->save();

                    } else {
                        $date = date('y-m-d',strtotime(str_replace('-','/', $request['tranfer_date'])));

                        $GdStock = GodownStock::create([
                            'gs_godown_id' => $request->gsl_to,
                            'gs_branch_stock_id' => $tq['branch_stock_id'],
                            'gs_stock_id' => $prod['bs_stock_id'],
                            'gs_date' => $date,
                            'gs_prd_id' => $prod['bs_prd_id'],
                            'gs_qty' => $tq['prod_qty'],
                            'branch_id' => $request->branch_id,
                            'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)

                        ]);
                        $gdTransLog['gsl_gdwn_stock_id'] = $GdStock->gs_id;

                    }
                }

                break;
            default:
                return parent::displayMessage('Invalid_Transfer', $this->badrequest_stat);
                break;
        }

        $units = array_column($request->trans_stocks, 'prod_unit_id');
        $stockid = array_column($request->trans_stocks, 'branch_stock_id');

        $stockunit = StockUnitRates::whereIn('branch_stock_id', $stockid)->whereIn('sur_unit_id', $units)->get();
        $request['gt_trans_price'] = $stockunit->sum('sur_unit_rate');
        $request['gt_added_by'] = $this->usr_id;

        $trn_id =  GodownTransfer::max('gdtrn_inv_id');
        $gdTran['gdtrn_inv_id'] = $trn_id + 1;        
        $gdTran['gdtrn_from'] = $request['gsl_from'];
        $gdTran['gdtrn_to'] = $request['gsl_to'];
        $gdTran['gdtrn_date'] = date('y-m-d',strtotime(str_replace('-','/', $request['tranfer_date'])));
        $gdTran['gdtrn_time'] = date('H:i:s',time());
        $gdTran['gdtrn_datetime'] = date('y-m-d',strtotime(str_replace('-','/', $request['tranfer_date']))).' '.date('H:i:s',time());
        $gdTran['gdtrn_added_by'] = $request['gt_added_by'];
        $gdTran['gdtrn_notes'] = $request['gt_notes'];
        
        $gdTran['branch_id'] = $this->branch_id;
        

        $tran = GodownTransfer::create($gdTran);

        $gdTransLog['gsl_tran_inv_id'] = $gdTran['gdtrn_inv_id'];
        $gdTransLog['gsl_added_by'] = $this->usr_id;
        $gdTransLog['gsl_vchr_type'] = $request['gsl_vchr_type'];
        $gdTransLog['branch_id'] = $request['branch_id'];
        $gdTransLog['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
        $gdTransLog['gsl_from'] = $request['gsl_from'];
        $gdTransLog['gsl_to'] = $request['gsl_to'];

        foreach ($request->trans_stocks as $key => $sval) {

            $gdTransLog['gsl_prd_id'] = $sval['prod_id'];
            $gdTransLog['gsl_branch_stock_id'] = $sval['branch_stock_id'];
            $gdTransLog['gsl_date'] = date('y-m-d',strtotime(str_replace('-','/', $request['tranfer_date'])));
            $gdTransLog['gsl_stock_id'] = $sval['gsl_stock_id'];
            $gdTransLog['gsl_prod_unit'] = $sval['prod_unit_id'];
            $gdTransLog['gsl_qty'] = $sval['prod_qty'];

            GodownStockLog::create($gdTransLog);
        }

        return parent::displayMessage('Saved Successfully', $this->success_stat);
    }

    public function listGodownTrans($request)
    {

        $rltn = [
            'transfers' => function ($qr) {
  
                $qr->select('gsl_tran_inv_id','gsl_qty as prod_qty','gsl_prd_id','products.prd_name as prod_name','units.unit_display as prod_unit_name')
                            ->join('products','prd_id','godown_stock_log.gsl_prd_id')
                            ->join('units','unit_id','godown_stock_log.gsl_prod_unit')
                            ->orderBy('gsl_id', 'asc');
            },
        ];

        $data = GodownTransfer::select('*')->with($rltn);

        if($this->branch_id > 0)
        $data = $data->where('branch_id',$this->branch_id);

        $data = $data->orderby('gdtrn_inv_id','desc')->paginate($this->per_page);

    return $data;

    }

    public function isBaseUnit($unit_id)
    {

        $is_base = Units::find($unit_id);

        if ($is_base['unit_type'] == 1) {
            return true;
        } else {
            return $is_base->unit_base_qty;
        }

    }


    public function get_godown_tran_num()
    {
        $purch_date = date("d/m/Y");
        $purch_count =  GodownTransfer::max('gdtrn_inv_id') + 1;
        return parent::displayData(['gd_num' => $purch_count,'gd_date'=>$purch_date]);
    }

}
