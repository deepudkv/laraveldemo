<?php

namespace App\Http\Controllers\Api\Godown;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiParent;
use App\Models\Godown\GodownStock;
use App\Models\Godown\GodownStockLog;
// use App\Http\Requests\GodownStock;
use validator;

class GodownStockController extends ApiParent
{

    public function GodownStock(Request $request, $type)
    {

        switch ($type) {
            case 'add':
                return $this->addGodownStock($request);
                break;
            case 'edit':
                return $this->editGodown($request);
                break;
        }
    }


    /**
     * @api {post} http://127.0.0.1:8000/api/godown_stock/add  Add Godown Stock
     * @apiName addGodownStock
     * @apiGroup Godown
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {bigint} gs_godown_id Godown Id (Required)
     * @apiParam {bigint} gs_branch_stock_id Branch Stock Id (Required)
     * @apiParam {bigint} gs_stock_id Main Stock Id (Required)
     * @apiParam {bigint} gs_prd_id Prooduct Id (Required)
     * @apiParam {int} gs_qty Godown Stock Quantity (Required)
     * @apiParam {date} gs_date Godown Date
     * @apiParam {int} branch_id Branch id
     * 
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} message Success Message
     * 
     * @apiError {String} error Error Message.
     * 
     */


    public function addGodownStock($request)
    {

        $validator = Validator::make(
            $request->all(),
            [
                'gs_godown_id' => 'required|exists:godown_master,gd_id',
                'gs_branch_stock_id' => 'required|exists:branch_stocks,branch_stock_id',
                'gs_stock_id' => 'required|exists:company_stocks,cmp_stock_id',
                'gs_prd_id' => 'required|exists:products,prd_id',
                'gs_qty' => 'required',

            ],
            [
                'gs_godown_id.required' => 'Required',
                'gs_godown_id.exists' => 'Invalid',
                'gs_branch_stock_id.required' => 'Required',
                'gs_branch_stock_id.exists' => 'Invalid',
                'gs_stock_id.required' => 'Required',
                'gs_stock_id.exists' => 'Invalid',
                'gs_prd_id.required' => 'Required',
                'gs_prd_id.exists' => 'Invalid',
                'gs_qty.required' => 'Required',

            ]
        );

        if ($validator->fails())
            return parent::displayError($validator->messages(), $this->badrequest_stat);

        $gdres = GodownStock::select('*')->where('gs_godown_id', $request['gs_godown_id'])
        ->where('gs_branch_stock_id', $request['gs_branch_stock_id'])
            ->where('gs_prd_id', $request['gs_prd_id'])->first();

        $stockLog['gsl_godown_id'] = $request->gs_godown_id;
        $stockLog['gsl_branch_stock_id'] = $request->gs_branch_stock_id;
        $stockLog['gsl_stock_id'] = $request->gs_stock_id;
        $stockLog['gsl_prd_id'] = $request->gs_prd_id;
        $stockLog['gsl_qty'] = $request->gs_qty;
        $stockLog['gsl_added_by'] = $this->usr_id;
        $stockLog['branch_id'] = $request->branch_id;
        $stockLog['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
        if ($gdres) {

            $gdres['gs_qty'] = $gdres['gs_qty'] + $request['gs_qty'];
            $gdres->save();
            $stockLog['gsl_gdwn_stock_id'] = $gdres->gs_id;

            GodownStockLog::create($stockLog);
        } else {
            $gds = GodownStock::create($request->all());

            $stockLog['gsl_gdwn_stock_id'] = $gds->gs_id;

            GodownStockLog::create($stockLog);
        }

        return parent::displayMessage('Saved_successfully', $this->success_stat);
    }
}
