<?php

namespace App\Http\Controllers\Api\Godown;

use App\Http\Controllers\Api\ApiParent;
use App\Models\Godown\GodownMaster;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class GodownController extends ApiParent
{

    // ===================== Code Added By Rakesh =======================

    public function Godown(Request $request, $type)
    {

        switch ($type) {
            case 'add':
                return $this->addGodown($request);
                break;
            case 'edit':
                return $this->editGodown($request);
                break;
            case 'list':
                return $this->listGodown($request);
                break;
            // case 'searchByName':
            // return $this->searchGodown($request);
            // break;
            default:
                return response()->json(['error' => 'Invalid Endpoint', 'status' => $this->badrequest_stat]);
                break;
        }
    }

    /**
     * @api {post} http://127.0.0.1:8000/api/godown/add  Add Godown
     * @apiName addGodown
     * @apiGroup Godown
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {String} gd_name Godown Name (Required)
     * @apiParam {String} gd_code Godown Code
     * @apiParam {String} gd_cntct_num Godown contact Number
     * @apiParam {String} gd_cntct_person Godown contact person
     * @apiParam {String} gd_address Godown address
     * @apiParam {String} gd_desc Godown Description
     *
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} message Success Message
     *
     * @apiError {String} error Error Message.
     *
     */

    public function addGodown($request)
    {

        $validator = Validator::make(
            $request->all(),
            [
                'gd_name' => 'required|unique:godown_master',
                'gd_code' => 'unique:godown_master',

            ],
            [
                'gd_name.required' => 'Required',
                'gd_name.unique' => 'Already Exists',
                'gd_code.unique' => 'Already Exists',

            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }

        $godown = $request->all();
        GodownMaster::create($godown);
        return response()->json(['message' => 'Saved Successfully', 'status' => $this->created_stat]);
    }

    /**
     * @api {post} http://127.0.0.1:8000/api/godown/edit  Edit Godown
     * @apiName editGodown
     * @apiGroup Godown
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {BigInt} gd_id Godown Id (Required)
     * @apiParam {String} gd_name Godown Name (Required)
     * @apiParam {String} gd_code Godown Code
     * @apiParam {String} gd_cntct_num Godown contact Number
     * @apiParam {String} gd_cntct_person Godown contact person
     * @apiParam {String} gd_address Godown address
     * @apiParam {String} gd_desc Godown Description
     *
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} message Success Message
     *
     * @apiError {String} error Error Message.
     *
     */

    public function editGodown($request)
    {

        $validator = Validator::make(
            $request->all(),
            [
                'gd_id' => 'required',
                'gd_name' => ['required', Rule::unique('godown_master')->ignore($request->gd_id, 'gd_id')],
                'gd_code' => Rule::unique('godown_master')->ignore($request->gd_id, 'gd_id'),

            ],
            [
                'gd_id.required' => 'Required',
                'gd_name.required' => 'Required',
                'gd_name.unique' => 'Already Exists',
                'gd_code.unique' => 'Already Exists',

            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }

        $godown = $request->all();
        $godownmMaster = GodownMaster::find($request->gd_id);
        $godownmMaster->fill($godown);
        $godownmMaster->save();
        return response()->json(['message' => 'Updated Successfully', 'status' => $this->created_stat]);
    }

    /**
     * @api {post} http://127.0.0.1:8000/api/search_godown  Search Godown
     * @apiName searchGodown
     * @apiVersion 0.1.0
     * @apiGroup Godown
     *
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {varchar} gd_name Godown Name
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} data Godowns

     */

    public function searchGodown(Request $request)
    {
        if ($request['gd_name'] != "") {
            $gdData = GodownMaster::where('gd_name', 'like', '%' . $request['gd_name'] . '%')
            ->where('branch_id', $request->branch_id)->take(5)->get();

            return response()->json(['data' => $gdData, 'status' => $this->success_stat]);
        } else {

            $data = GodownMaster::where('branch_id', $request->branch_id)->take(5)->get();
            return response()->json(['data' => $data, 'status' => $this->success_stat]);
        }
    }

    /**
     * @api {post} http://127.0.0.1:8000/api/get_godowns Get Godowns
     * @apiName getGodownByBranch
     * @apiVersion 0.1.0
     * @apiGroup Godown
     *
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} data Godowns by branch

     */

    public function getGodownByBranch()
    {

        $user = auth()->user();
        $branch_id = ($user != null ? $user->branch_id : 0);

        $gdData = GodownMaster::where('branch_id', $branch_id)->get();

        return parent::displayData($gdData, $this->success_stat);
    }

    public function getAllGodowns(Request $rq)
    {
        $gdData = [];
        if ($gd_name = isset($rq['gd_name']) ? $rq['gd_name'] : 0) {
            $gdData = GodownMaster::select('gd_id', 'gd_name', 'gd_code')->where('gd_name', 'like', '%' . $gd_name . '%')
                                    ->where('branch_id', $rq->branch_id)->take(100)->get();
        } else {
            $gdData = GodownMaster::select('gd_id', 'gd_name', 'gd_code')->where('branch_id', $rq->branch_id)->get();
        }
        $gdData[] = ['gd_id' => 0,
            'gd_name' => 'Shops',
            'gd_code' => 'SP'];
        return parent::displayData($gdData, $this->success_stat);
    }

    

    // ===================== End of Code Added By Rakesh =======================


    // ===================== Satrt of Code Added By Yasir Poongadan =======================
    public function listGodown(Request $rq) {
        $result = [];
        $qry = GodownMaster::select('gd_id', 'gd_name', 'gd_code');
        if($rq->branch_id) {
            $qry->where('branch_id', $rq->branch_id);
        }
        $result = $qry->get()->toArray();  
        $gdData = ['gd_id' => 0,
            'gd_name' => 'Shops',
            'gd_code' => 'SP']; 
        array_unshift($result, $gdData);
        $gdData = ['gd_id' => -1,
            'gd_name' => 'None',
            'gd_code' => 'None']; 
        array_unshift($result, $gdData);
        return parent::displayData($result, $this->success_stat);
    }

    // ===================== End of Code Added By Yasir Poongadan =======================

}
