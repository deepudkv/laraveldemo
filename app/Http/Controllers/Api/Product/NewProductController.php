<?php

namespace App\Http\Controllers\Api\Product;

use App\Http\Controllers\Api\ApiParent;
use App\Models\Product\Feature;
use App\Models\Product\ProdFeatures;
use App\Models\Product\Product;
use App\Models\Product\ProdUnit;
use App\Models\Product\Units;
use App\Models\Stocks\BranchStocks;
use App\Models\Stocks\CompanyStocks;
use App\Models\Stocks\StockUnitRates;
use App\Rules\barCode;
use Illuminate\Http\Request;
use Validator;

class NewProductController extends ApiParent
{

    /**
     * @api {post} http://127.0.0.1:8000/api/add_product Add Product
     * @apiName addProduct
     * @apiGroup Product
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {varchar(500)} prd_name Product Name (Required,unique,max:150,min:3)
     * @apiParam {mediumtext} prd_alias Product Alias (Required,unique,max:50,min:3)
     * @apiParam {int(11)} prd_cat_id Category Id (Required,numeric,exists)
     * @apiParam {int(11)} prd_sub_cat_id Sub Category Id (nullable,numeric,exists)
     * @apiParam {int(11)} prd_supplier Manufacturer ID (nullable,numeric)
     * @apiParam {mediumtext} prd_short_name Product Short Name (Required,unique,max:50,min:3)
     * @apiParam {int(11)} prd_min_stock Product Min Stock (nullable,numeric)
     * @apiParam {int(11)} prd_max_stock Product Max Stock (nullable,numeric)
     * @apiParam {int(11)} prd_base_unit_id Base Unit ID (nullable,numeric,exists)
     * @apiParam {int(11)} prd_default_unit_id Default Unit ID (nullable,numeric,exists)
     * @apiParam {varchar(50)} prd_code Product Code (nullable|unique)
     * @apiParam {int(11)} prd_tax Product Tax Percentage
     * @apiParam {varchar(50)} prd_tax_code Product Tax Code
     * @apiParam {varchar} prd_barcode Product Bar code
     * @apiParam {text} prd_desc Product Description
     * @apiParam {tinyint(4)} prd_minstock_alert Minimum product stock Alert (values 1 or 0)
     * @apiParam {tinyint(4)} prd_maxstock_alert Maximum product stock Alert(values 1 or 0)
     * @apiParam {int(11)} prd_exp_dur Product Expriry Duration
     * @apiParam {mediumtext} prd_remarks Product Remarks
     * @apiParam {array} prd_unit_ids Product's Unit Id's and corresponding ean barcode for each units like as [ { "unit_id":1 , "ean_barcode" : "325FTHGJDGFDR4T", "unit_rate" : 89.26  }]
     * @apiParam {array} features Product's Features (Values: int feat_id ,varchar feat_val)
     * @apiParam {int(11)} purchase_rate Product purchase rate
     * @apiParam {array} unit_rates Product's  rate for each units  (Values: int unit_id ,varchar unit_rate)
     * @apiParam {array} branch_ids ( if adding stock to branches )

     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} message success message
     * @apiError {String} error Error Message.
     */

    public function addNewproduct(Request $rq)
    {
        $validator = Validator::make(
            $rq->all(),
            [
                'prd_name' => 'required|unique:products|max:500|min:3',
                'prd_alias' => 'required|unique:products|max:500|min:3',
                'prd_short_name' => 'required|unique:products|max:150|min:3',
                'prd_code' => 'nullable|unique:products',

                'prd_cat_id' => 'required|exists:categories,cat_id',
                'prd_sub_cat_id' => 'nullable|exists:subcategories,subcat_id',
                'prd_base_unit_id' => 'required|exists:units,unit_id',
                'prd_base_unit_ean' => [new barCode],

                'prd_supplier' => 'nullable',
                'prd_min_stock' => 'required_if:prd_minstock_alert,1|nullable|numeric',
                'prd_max_stock' => 'required_if:prd_maxstock_alert,1|nullable|numeric',
                'prd_tax_cat_id' => 'required|numeric|min:1',

            ],
            [
                'prd_name.required' => 'Required',
                'prd_name.min' => 'Minimum 3 charecters',
                'prd_name.max' => 'Maximum 500 charecters',
                'prd_alias.min' => 'Minimum 3 charecters',
                'prd_alias.max' => 'Maximum 500 charecters',
                'prd_alias.unique' => 'Already exists',
                'prd_name.unique' => 'Already exists',
                'prd_short_name.min' => 'Minimum 3 charecters',
                'prd_short_name.max' => 'Maximum 150 charecters',
                'prd_short_name.unique' => 'Already exists',
                'prd_code.unique' => 'Already exists',
                'prd_cat_id.required' => 'Required',
                'prd_sub_cat_id.required' => 'Required',
                'prd_alias.required' => 'Required',
                'prd_short_name.required' => 'Required',
                'prd_cat_id.exists' => 'Invalid',
                'prd_sub_cat_id.exists' => 'Invalid',
                'prd_alias.unique' => 'Already exists',
                'prd_short_name.unique' => 'Already exist',
                'prd_min_stock.numeric' => 'Should be numeric',
                'prd_max_stock.numeric' => 'Should be numeric',
                'prd_min_stock.required_if' => 'Required',
                'prd_max_stock.required_if' => 'Required',
                'prd_base_unit_id' => 'Required',
                'prd_base_unit_ean' => 'Required',
                'prd_tax_cat_id.required' => 'Required',
                'prd_tax_cat_id.numeric' => 'Required',
                'prd_tax_cat_id.min' => 'Required',
            ]
        );
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }

        list($prd_id, $stock_id) = $this->addProductOpertaions($rq);
        return ($prd_id && $stock_id) ? parent::displayData($this->productPreview($prd_id, $stock_id)) :
        parent::displayError('Opertaion Failed');
    }

    public function productPreview($prd_id, $stock_id)
    {

        $rltn = [
            'unit' => function ($qr) {
                $qr->select('unit_id', 'unit_name', 'unit_type');
            },
            'category' => function ($qr) {
                $qr->select('cat_id', 'cat_name');
            },
            'sub_category' => function ($qr) {
                $qr->select('subcat_id', 'subcat_name');
            },
            'manufacturer' => function ($qr) {
                $qr->select('id', 'manftr_comp_name');
            },
        ];

        $info = Product::where('prd_id', $prd_id)->with($rltn)->get();

        foreach ($info as $k => $row) {

            $info[$k]['cmp_stock_id'] = $stock_id;
            $info[$k]['base_unit_name'] = $row['unit']['unit_name'];
            $info[$k]['cat_name'] = $row['category']['cat_name'];
            $info[$k]['subcat_name'] = $row['sub_category']['subcat_name'];
            $info[$k]['manftr_name'] = $row['manufacturer']['manftr_comp_name'];

            // unset($info[$k]['base_unit']);
            unset($info[$k]['category']);
            unset($info[$k]['sub_category']);
            unset($info[$k]['manufacturer']);
        }
        return $info;
    }

    public function addProductOpertaions($rq)
    {

        $product_data = $rq->all();

        if (!$product_data['prd_sub_cat_id']) {
            $product_data['prd_sub_cat_id'] = 0;
        }
        $product_data['prd_added_by'] = $this->usr_id;
        $product_data['prd_barcode'] = $this->generatePrdBarcode();
        $product_data["prd_ean"] = $rq->prd_base_unit_ean;

        $product = Product::create($product_data);

        if ($prd_id = $product->prd_id) {
            if ($this->addProductBaseUnit($prd_id, $rq)) {
                $stock_id = $this->addDefaultStock($prd_id);
                return ($stock_id) ? [$prd_id, $stock_id] : false;
            }
            return false;
        } else {
            return false;
        }
    }

    public function generatePrdBarcode()
    {
        $last_barcode = trim(Product::max('prd_barcode'));

        return ($last_barcode > 0) ? ($last_barcode + 1) : '1000000';

    }

    public function addProductBaseUnit($prd_id, $rq)
    {
        $prd_unit["produnit_prod_id"] = $prd_id;
        $prd_unit["company_id"] = $rq->company_id;
        $prd_unit["branch_id"] = $rq->branch_id;
        $prd_unit["server_sync_time"] = $rq->server_sync_time;
        $prd_unit["produnit_unit_id"] = $rq->prd_base_unit_id;
        $prd_unit["produnit_ean_barcode"] = $rq->prd_base_unit_ean;

        return ProdUnit::create($prd_unit);
    }

    public function addDefaultStock($prd_id)
    {
        $cmpStocks['cmp_prd_id'] = $prd_id;
        $cmpStocks['cmp_avg_prate'] = 0;

        $cmpStocks['server_sync_time'] = $this->server_sync_time;
        $stocks = CompanyStocks::create($cmpStocks);
        if ($stocks->cmp_stock_id) {
            $bstocks["bs_stock_id"] = $stocks->cmp_stock_id;
            $bstocks["bs_prd_id"] = $prd_id;
            $bstocks["bs_prate"] = 0;
            $bstocks["bs_branch_id"] = 0;
            $bstocks['server_sync_time'] = $this->server_sync_time;
            BranchStocks::create($bstocks);
            return $stocks->cmp_stock_id;
        }
        return false;
    }

    public function productValidation($rq)
    {
        $validator = Validator::make(
            $rq->all(),
            [
                'prd_name' => 'required|unique:products|max:500|min:3',
                'prd_alias' => 'required|unique:products|max:500|min:3',
                'prd_short_name' => 'required|unique:products|max:50|min:3',
                'prd_code' => 'nullable|unique:products',

                'prd_cat_id' => 'required|exists:categories,cat_id',
                'prd_sub_cat_id' => 'nullable|exists:subcategories,subcat_id',
                'prd_base_unit_id' => 'required|exists:units,unit_id',
                'prd_base_unit_ean' => 'required',

                'prd_supplier' => 'nullable',
                'prd_min_stock' => 'required_if:prd_minstock_alert,1|nullable|numeric',
                'prd_max_stock' => 'required_if:prd_maxstock_alert,1|nullable|numeric',

            ],
            [
                'prd_name.required' => 'Required',
                'prd_name.min' => 'Minimum 3 charecters',
                'prd_name.max' => 'Maximum 500 charecters',
                'prd_alias.min' => 'Minimum 3 charecters',
                'prd_alias.max' => 'Maximum 150 charecters',
                'prd_alias.unique' => 'Already exists',
                'prd_name.unique' => 'Already exists',
                'prd_short_name.min' => 'Minimum 3 charecters',
                'prd_short_name.max' => 'Maximum 150 charecters',
                'prd_short_name.unique' => 'Already exists',
                'prd_code.unique' => 'Already exists',
                'prd_cat_id.required' => 'Required',
                'prd_sub_cat_id.required' => 'Required',
                'prd_alias.required' => 'Required',
                'prd_short_name.required' => 'Required',
                'prd_cat_id.exists' => 'Invalid',
                'prd_sub_cat_id.exists' => 'Invalid',
                'prd_alias.unique' => 'Already exists',
                'prd_short_name.unique' => 'Already exist',
                'prd_min_stock.numeric' => 'Should be numeric',
                'prd_max_stock.numeric' => 'Should be numeric',
                'prd_min_stock.required_if' => 'Required',
                'prd_max_stock.required_if' => 'Required',
                'prd_base_unit_id' => 'Required',
                'prd_base_unit_ean' => 'Required',
            ]
        );
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }

    }

    public function addDerivedUnits(Request $rq)
    {
        $validator = Validator::make(
            $rq->all(),
            [
                'prd_id' => 'required|exists:products,prd_id',
                'prd_units' => 'required',
            ],
            [
                'prd_id.required' => 'Required',
                'prd_id.exists' => 'Invalid',
                'prd_units' => 'Required',
            ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }
        $punit["produnit_prod_id"] = $rq->prd_id;
        $punit["company_id"] = $rq->company_id;
        $punit["branch_id"] = $rq->branch_id;
        $punit["server_sync_time"] = $rq->server_sync_time;

        if (is_array($rq->prd_units) && !empty($rq->prd_units)) {
            foreach ($rq->prd_units as $unit) {
                $punit["produnit_unit_id"] = $unit['unit_id'];
                $punit["produnit_ean_barcode"] = $unit['ean_barcode'];

                $stat = ProdUnit::create($punit);
            }
            $rltn = [
                'units' => function ($qr) {
                    $qr->select('unit_id', 'unit_name', 'unit_type', 'unit_base_qty');
                },
            ];

            $prd = Product::where('prd_id', $rq->prd_id)->with($rltn)->first()->toArray();

            return $stat ? parent::displayData($prd['units']) : parent::displayError('Failed adding units');
        } else {
            return parent::displayError('Invalid Units');
        }

    }

    public function editDerivedUnits(Request $rq)
    {
        $validator = Validator::make(
            $rq->all(),
            [
                'prd_id' => 'required|exists:products,prd_id',
                'prd_units' => 'required',
            ],
            [
                'prd_id.required' => 'Required',
                'prd_id.exists' => 'Invalid',
                'prd_units' => 'Required',
            ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }
        $punit["produnit_prod_id"] = $rq->prd_id;
        $punit["company_id"] = $rq->company_id;
        $punit["branch_id"] = $this->branch_id;
        $punit["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);

        if (is_array($rq->prd_units) && !empty($rq->prd_units)) {
            $new_unit = array_column($rq->prd_units, 'unit_id');
            // return $new_unit;
            $prdUnits = ProdUnit::where('produnit_prod_id', $rq->prd_id)->get()->pluck('produnit_unit_id')->toArray();
            // return $prdUnits;
            if (count($prdUnits) > 0) {

                foreach ($rq->prd_units as $key => $val) {

                    if (in_array($val['unit_id'], $prdUnits)) {

                        $data = ProdUnit::where('produnit_prod_id', $rq->prd_id)->where('produnit_unit_id', $val['unit_id'])->first();
                        $newval = ['produnit_unit_id' => $val['unit_id'], 'produnit_ean_barcode' => $val['ean_barcode'], 'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)];
                        $data->fill($newval)->save();

                    } else {

                        $stat = ProdUnit::create(['produnit_prod_id' => $rq->prd_id,
                            'produnit_unit_id' => $val['unit_id'],
                            'produnit_ean_barcode' => $val['ean_barcode'],
                            'branch_id' => $this->branch_id,
                            'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)]);

                    }
                }

                foreach ($prdUnits as $key => $val) {
                    // return $val;
                    if (in_array($val, $new_unit)) {

                    } else {

                        $stat = ProdUnit::where('produnit_prod_id', $rq->prd_id)->where('produnit_unit_id', $val)->delete();

                        // return "not in array";
                    }
                }

                $rltn = [
                    'units' => function ($qr) {
                        $qr->select('unit_id', 'unit_name', 'unit_type', 'unit_base_qty');
                    },
                ];

                $prd = Product::where('prd_id', $rq->prd_id)->with($rltn)->first()->toArray();

                return parent::displayData($prd['units']);

            } else {
                // return "empty";
                foreach ($rq->prd_units as $unit) {
                    $punit["produnit_unit_id"] = $unit['unit_id'];
                    $punit["produnit_ean_barcode"] = $unit['ean_barcode'];
                    $punit["branch_id"] = $this->branch_id;
                    $punit["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);
                    $stat = ProdUnit::create($punit);
                }

                $rltn = [
                    'units' => function ($qr) {
                        $qr->select('unit_id', 'unit_name', 'unit_type', 'unit_base_qty');
                    },
                ];

                $prd = Product::where('prd_id', $rq->prd_id)->with($rltn)->first()->toArray();

                return parent::displayData($prd['units']);
            }
        }
        return "done";

        //   if(!empty($prdUnits)){

        //     foreach ($rq->prd_units as $unit) {
        //         $punit["produnit_unit_id"] = $unit['unit_id'];
        //         $punit["produnit_ean_barcode"] = $unit['ean_barcode'];

        //         $stat = ProdUnit::create($punit);
        //     }
        //     $rltn = [
        //         'units' => function ($qr) {
        //             $qr->select('unit_id', 'unit_name', 'unit_type', 'unit_base_qty');
        //         },
        //     ];

        //     $prd = Product::where('prd_id', $rq->prd_id)->with($rltn)->first()->toArray();

        //     return $stat ? parent::displayData($prd['units']) : parent::displayError('Failed adding units');
        // } else {
        //     return parent::displayError('Invalid Units');
        // }

    }

    public function editProdFeatures(Request $rq)
    {

        $validator = Validator::make(
            $rq->all(),
            [
                'fetprod_prod_id' => 'required|exists:products,prd_id',
                "features" => 'required',
            ],
            [
                'fetprod_prod_id.required' => 'Required',
                'fetprod_prod_id.exists' => 'Invalid',
                "features.required" => 'Required',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }
        $new_fet = array_column($rq->features, 'feat_id');
        $prodid = $rq['fetprod_prod_id'];

        $prdFets = ProdFeatures::where('fetprod_prod_id', $prodid)->get()->pluck('fetprod_fet_id')->toArray();

        if (count($prdFets) > 0) {

            foreach ($rq->features as $key => $val) {

                if (in_array($val['feat_id'], $prdFets)) {

                    $data = ProdFeatures::where('fetprod_prod_id', $rq->fetprod_prod_id)->where('fetprod_fet_id', $val['feat_id'])->first();
                    //    return $data;
                    $newval = ['fetprod_fet_id' => $val['feat_id'], 'fetprod_fet_val' => $val['feat_val']];
                    $data->fill($newval)->save();

                } else {

                    $stat = ProdFeatures::create(['fetprod_prod_id' => $rq['fetprod_prod_id'],
                        'fetprod_fet_id' => $val['feat_id'],
                        'fetprod_fet_val' => $val['feat_val'],
                        'branch_id' => $this->branch_id,
                        'server_sync_time' => $this->server_sync_time,
                    ]);

                }
            }

            foreach ($prdFets as $key => $val) {
                // return $val;
                if (in_array($val, $new_fet)) {

                } else {

                    $stat = ProdFeatures::where('fetprod_prod_id', $rq['fetprod_prod_id'])->where('fetprod_fet_id', $val)->delete();

                    // return "not in array";
                }
            }

            $prd = ProdFeatures::select('feature_products.fetprod_fet_val as feat_val', 'features.feat_name', 'features.feat_id')->where('fetprod_prod_id', $rq->fetprod_prod_id)->
                join('features', 'features.feat_id', 'feature_products.fetprod_fet_id')->
                get();

            return parent::displayData($prd);

        } else {
            // return "empty";
            foreach ($rq->features as $fet) {
                $stat = ProdFeatures::create(['fetprod_prod_id' => $rq['fetprod_prod_id'],
                    'fetprod_fet_id' => $fet['feat_id'],
                    'fetprod_fet_val' => $fet['feat_val'],
                    'branch_id' => $this->branch_id,
                    'server_sync_time' => $this->server_sync_time,
                ]);
            }
            return response()->json(['message' => 'Updated Successfully', 'status' => $this->success_stat]);

            if ($fres) {
                return response()->json(['message' => 'Updated Successfully', 'status' => $this->success_stat]);
            }
        }
    }

    public function addFeatures(Request $rq)
    {
        $validator = Validator::make(
            $rq->all(),
            [
                'prd_id' => 'required|exists:products,prd_id',
                'prd_features' => 'required',
            ],
            [
                'prd_id.required' => 'Required',
                'prd_id.exists' => 'Invalid',
                'prd_features' => 'Required',
            ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }

        $features = $rq->prd_features ? $rq->prd_features : [];

        if (is_array($features) && !empty($features)) {

            $ProdFeatures["fetprod_prod_id"] = $rq->prd_id;
            $ProdFeatures["company_id"] = $rq->company_id;
            $ProdFeatures["branch_id"] = $rq->branch_id;
            $ProdFeatures["server_sync_time"] = $rq->server_sync_time;

            foreach ($features as $feature) {
                $ProdFeatures["fetprod_fet_id"] = $feature['feat_id'];
                $ProdFeatures["fetprod_fet_val"] = $feature['feat_val'];

                $stat = ProdFeatures::create($ProdFeatures);
            }
            return parent::displayMessage('Features added');

            if (!$stat) {
                return $stat ? parent::displayMessage('Features added') :
                parent::displayError('Failed adding features');
            }

        } else {
            return parent::displayError('Invalid Features');
        }

    }

    public function updateStockRates(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'prd_id' => 'required|exists:products,prd_id',
                'cmp_stock_id' => 'required|exists:company_stocks,cmp_stock_id',
                'unit_rates' => 'required',
                'purchase_rate' => 'required',
            ],
            [
                'prd_id.required' => 'Required',
                'prd_id.exists' => 'Invalid',
                'cmp_stock_id.required' => 'Required',
                'cmp_stock_id.exists' => 'Invalid',

                'unit_rates.required' => 'Required',
                'purchase_rate.required' => 'Required',
                'purchase_rate.numeric' => 'Invalid Input',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }

        $purchase_rate = $request->filled('purchase_rate') ? $request->purchase_rate : 0;

        // $companyStocks['cmp_prd_id'] = $request->prd_id;
        // $companyStocks['cmp_avg_prate'] = $purchase_rate;
        // $companyStocks['server_sync_time']=$request->server_sync_time;
        $stock_id = $request->cmp_stock_id;
        $prd_id = $request->prd_id;
        // CompanyStocks::where('cmp_stock_id', $stock_id)->update($companyStocks);
        //updating company stocks//
        // BranchStocks::where('bs_stock_id', $stock_id)->where('bs_prd_id', $prd_id)->delete(); //updating company stocks//

        //code for adding stocks to company//
        $unit_rates = $request['unit_rates'];
        foreach ($unit_rates as $k => $rate) {
            if (isset($rate['unit_rate']) && $rate['unit_rate'] >= 0) {
                $base_unit = Product::where('prd_base_unit_id', $rate['unit_id'])->where('prd_id', $request->prd_id)->first();
                if ($base_unit) {
                    $bs_srate = $rate['unit_rate'];
                }

            }
        }

        $branch_ids = $request['branch_ids'];
        $branch_ids[] = $this->branch_id;
        if (!empty($branch_ids) && $stock_id && !empty($unit_rates)) {

            $branch_stocks["bs_stock_id"] = $stock_rates["sur_stock_id"] = $stock_id;
            $branch_stocks["bs_prd_id"] = $stock_rates["sur_prd_id"] = $prd_id;
            $branch_stocks["bs_prate"] = $purchase_rate;
            $branch_stocks["bs_srate"] = $bs_srate;
            $branch_stocks["bs_avg_prate"] = $purchase_rate;
            $branch_stocks["server_sync_time"] = $request->server_sync_time;

            foreach ($branch_ids as $branch_id) {

                $branch_stocks["bs_branch_id"] = $stock_rates["sur_branch_id"] = $branch_id;
                $bstocks = BranchStocks::create($branch_stocks); //adding stocks to company branch//

                $stock_rates["branch_stock_id"] = $bstocks->branch_stock_id;

                foreach ($unit_rates as $k => $rate) {
                    if (isset($rate['unit_rate']) && $rate['unit_rate'] >= 0) {

                        $stock_rates["sur_unit_id"] = $rate['unit_id'];
                        $stock_rates["sur_unit_rate"] = $rate['unit_rate'];
                        $stock_rates["server_sync_time"] = $request->server_sync_time;
                        StockUnitRates::create($stock_rates);

                        $out = Units::select('unit_id', 'unit_name', 'unit_type', 'unit_base_qty')->where('unit_id', $rate['unit_id'])->get()->toArray();
                        $out[0]['unit_rate'] = $rate['unit_rate'];
                        $units[$k] = $out[0];
                    } //adding branch stock rates//
                }
                $units_exist = array_column($units, 'unit_id');
                $prd_units = ProdUnit::select('unit_id', 'unit_name', 'unit_type', 'unit_base_qty')->join('units', 'units.unit_id', 'prod_units.produnit_unit_id')->
                    where('prod_units.produnit_prod_id', $prd_id)->whereNotIn('produnit_unit_id', $units_exist)->get()->toArray();
                foreach ($prd_units as $k => $prdunits) {
                    $prd_units[$k]['unit_rate'] = 0;
                }

                $units = array_merge($units, $prd_units);
            }
            return parent::displayData(['purchase_rate' => $purchase_rate, 'branch_ids' => $request['branch_ids'], 'units' => $units]);
        } //end code for adding stocks//

    }

    public function addListNewproduct(Request $rq)
    {
        $validator = Validator::make(
            $rq->all(),
            [

                'prd_unit_ids' => 'required',
                'unit_ean' => [new barCode],

            ],
            [
                'prd_unit_ids.required' => 'Required',
            ]
        );
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }

        return response()->json(['data' => 'Validation Succes', 'status' => $this->success_stat]);
    }

    public function addListFeature(Request $rq)
    {
        $validator = Validator::make(
            $rq->all(),
            [

                'feat_id' => 'required',
                'feat_value' => 'required',

            ],
            [
                'feat_id.required' => 'Required',
                'feat_value.required' => 'Required',
            ]
        );
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }

        return response()->json(['data' => 'Validation Succes', 'status' => $this->success_stat]);
    }

}
