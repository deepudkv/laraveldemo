<?php

namespace App\Http\Controllers\Api\Product;

use App\Http\Controllers\Api\ApiParent;
use App\Models\Product\Category;
use App\Models\Product\Subcategory;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

use App\Models\Product\Product;
use App\Models\Settings\TaxCategory;

use Validator;

class CategoryController extends ApiParent
{

    /**
     * @api {get} http://127.0.0.1:8000/api/get_categories  Get Categories Details
     * @apiName getCategories
     * @apiVersion 0.1.0
     * @apiGroup Category
     *
     *
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} cat Categories
     * @apiSuccess {String} subcat Sub Categories

     */

    public function getCategories()
    {

        $cat = Category::where('cat_flag', 1)->select('cat_id', 'cat_name', 'cat_pos', 'cat_remarks', 'cat_tax_cat_id')->get();
        $subcat = Subcategory::where('subcat_flag', 1)->select('subcat_id', 'subcat_parent_category', 'subcat_name', 'subcat_remarks')->get();

        return response()->json(['cat' => $cat, 'subcat' => $subcat], $this->success_stat);
    }

    /**
     * @api {post} http://127.0.0.1:8000/api/search_categories  Search Category
     * @apiName searchCategories
     * @apiVersion 0.1.0
     * @apiGroup Category
     *
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {varchar} cat_name Category Name
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} data Categories
     *
     *
     */

    public function searchCategories(Request $request)
    {
        if ($request['cat_name'] != "") {
            $cat = Category::where('cat_flag', 1)->where('cat_name', 'like', '%' . $request['cat_name'] . '%')->take(50)->get();

            return response()->json(['data' => $cat, 'status' => $this->success_stat]);
        } else {
            $data = Category::take(5)->get();
            return response()->json(['data' => $data, 'status' => $this->success_stat]);
        }
    }

    /**
     * @api {post} http://127.0.0.1:8000/api/add_category  Add Category
     * @apiName addCategory
     * @apiGroup Category
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam  {varchar} cat_name Category Name (Required)
     * @apiParam  {mediumtext} cat_remarks Category Remarks
     * @apiParam  {varchar} tinyint Category flag
     * @apiParam  {varchar} tinyint Category POS
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} message Success Message
     *
     * @apiError error Error Message.
     *
     */

    public function addCategory(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            [
                'cat_name' => 'required|unique:categories',
                'cat_tax_cat_id' => 'required|numeric|min:1',

            ],
            [
                'cat_name.required' => 'Required',
                'cat_tax_cat_id.required' => 'Required',
                'cat_tax_cat_id.numeric' => 'Required',
                'cat_tax_cat_id.min' => 'Required',
                'cat_name.unique' => 'Already Exists',

            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }

        $category_data = $request->all();
        $max = Category::max('cat_id');
        $category_data['cat_id'] = (empty($max)) ? 1 : $max + 1;
        Category::create($category_data);
        return response()->json(['message' => 'Saved Successfully', 'status' => $this->created_stat]);
    }

    /**
     * @api {post} http://127.0.0.1:8000/api/edit_category  Edit Category
     * @apiName editCategory
     * @apiGroup Category
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {varchar} cat_name Category Name (Required)
     * @apiParam {bigint} cat_id Category ID (Required)
     * @apiParam  {mediumtext} cat_remarks Category Remarks
     * @apiParam  {varchar} tinyint Category flag
     * @apiParam  {varchar} tinyint Category POS
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} message Success Message
     *
     * @apiError {String} error Error Message.
     *
     */

    public function editCategory(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            [
                'cat_id' => 'required|exists:categories',
                'cat_tax_cat_id' => 'required|numeric|min:1',
                'cat_name' => ['required', Rule::unique('categories')->ignore($request['cat_id'], 'cat_id')],

            ],
            [
                'cat_id.required' => 'Required',
                'cat_tax_cat_id.required' => 'Required',
                'cat_tax_cat_id.numeric' => 'Required',
                'cat_tax_cat_id.min' => 'Required',
                'cat_name.required' => 'Required',
                'cat_id.exists' => 'Invalid',
                'cat_name.unique' => 'Already Exists',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }

        $input = $request->all();
        unset($input['branch_id']);
        $id = $input['cat_id'];
        $data = Category::find($id);
        // update all product tax cat id, if any chnage in tax_cat_id
        if ($data->cat_tax_cat_id != $input['cat_tax_cat_id']) {
            $where = array();
            $where[] = ['taxcat_id','=', $input['cat_tax_cat_id']];
            $taxCat = TaxCategory::where($where)->first();
            
            $where = array();
            $where[] = ['prd_cat_id','=', $id];
            $updt = array(
                'prd_tax' => $taxCat['taxcat_tax_per'],
                'prd_tax_cat_id' => $taxCat['taxcat_id'],
                'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6),
            );
            Product::where($where)->update($updt);
            
        }
        $data->fill($input)->save();
        return response()->json(['message' => 'Updated Successfully', 'status' => $this->created_stat]);
    }

    /**
     * @api {get} http://127.0.0.1:8000/api/get_subcategories/{id}  Get Sub Categories
     * @apiName getSubCategories
     * @apiGroup Category
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {int} id  Category Id (Required)
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} data Sub Categories
     *
     * @apiError {String} error Error Message.
     *
     */

    public function getSubCategories($cat_id)
    {

        if ($cat_id == "") {
            return response()->json(['error' => "Please Select a category", 'status' => $this->badrequest_stat]);
        }

        $subcat = Subcategory::where('subcategories.subcat_flag', 1)->select('subcategories.subcat_id', 'subcategories.subcat_parent_category', 'subcategories.subcat_name', 'subcategories.subcat_remarks', 'categories.cat_name as subcat_parent_name')->join('categories', 'subcategories.subcat_parent_category', '=', 'categories.cat_id')->where('subcategories.subcat_id', $cat_id)->get();
        if (count($subcat) > 0) {
            return response()->json(['data' => $subcat, 'status' => $this->success_stat]);
        } else {
            return response()->json(['data' => "No result found", 'status' => $this->success_stat]);
        }

    }

    /**
     * @api {post} http://127.0.0.1:8000/api/add_subcategory  Add Sub Category
     * @apiName addSubCategory
     * @apiGroup Category
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {int} subcat_parent_category Parent Category Id (Required)
     * @apiParam {varchar} subcat_name Sub Category Name (Required,Unique)
     * @apiHeader {mediumtext} subcat_remarks Sub Cat Remarks
     * @apiHeader {tinyint} subcat_flag Sub Cat Flag
     * @apiHeader {tinyint} subcat_pos Sub Cat POS
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} message Success Message
     *
     * @apiError {String} error Error Message.
     *
     */

    public function addSubCategory(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            [
                'subcat_parent_category' => 'required|exists:categories,cat_id',
                'subcat_name' => 'required|unique:subcategories',

            ],
            [
                'subcat_parent_category.required' => 'Required',
                'subcat_parent_category.exists' => 'Invalid',
                'subcat_name.required' => 'Required',
                'subcat_name.unique' => 'Already Exists',

            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }

        if (is_null($request['subcat_pos'])) {
            $request['subcat_pos'] = 0;
        }

        $category_data = $request->all();
        $max = Subcategory::max('subcat_id');
        $category_data['subcat_id'] = (empty($max)) ? 1 : $max + 1;
        Subcategory::create($category_data);
        return response()->json(['message' => 'Saved Successfully', 'status' => $this->created_stat]);
    }

    /**
     * @api {post} http://127.0.0.1:8000/api/edit_subcategory  Edit Sub Category
     * @apiName editSubCategory
     * @apiGroup Category
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {bigint(20)} subcat_id Sub Category Id (Required)
     * @apiParam {int(11)} subcat_parent_category Parent Category Id (Required)
     * @apiParam {varchar(150)} subcat_name Sub Category Name (Required,Unique)
     * @apiHeader {mediumtext} subcat_remarks Sub Cat Remarks
     * @apiHeader {tinyint} subcat_flag Sub Cat Flag
     * @apiHeader {tinyint} subcat_pos Sub Cat POS
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} message Success Message
     *
     * @apiError {String} error Error Message.
     *
     */

    public function editSubCategory(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            [
                'subcat_id' => 'required|exists:subcategories',
                'subcat_parent_category' => 'required|exists:categories,cat_id',
                'subcat_name' => ['required', Rule::unique('subcategories')->ignore($request['subcat_id'], 'subcat_id')],

            ],
            [
                'subcat_id.exists' => 'Invalid',
                'subcat_id.required' => 'Required',
                'subcat_parent_category.required' => 'Required',
                'subcat_parent_category.exists' => 'Invalid',

            ]
        );
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }

        $input = $request->all();
        unset($input['branch_id']);
        $id = $input['subcat_id'];
        $data = Subcategory::find($id);
        $data->fill($input)->save();
        return response()->json(['message' => 'Updated Successfully', 'status' => $this->created_stat]);
    }

    /**
     * @api {get} http://127.0.0.1:8000/api/get_allsubcategories/{id}  Get Sub Category
     * @apiName getAllSubCategories
     * @apiGroup Category
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {int(11)} id Sub Category Id (Required)
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} data Sub categories
     *
     */

    public function getAllSubCategories($par_cat_id)
    {

        $subcats = Subcategory::where('subcat_flag', 1)->where('subcat_parent_category', $par_cat_id)->select('subcat_id', 'subcat_name')->get();
        return response()->json(['data' => $subcats, 'status' => $this->success_stat]);
    }

    /**
     * @api {post} http://127.0.0.1:8000/api/search_subcat  Search Sub Category
     * @apiName searchSubCat
     * @apiGroup Category
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {String} subcat_name Sub Category Name
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} data Sub categories
     *
     *
     */

    public function searchSubCat(Request $request)
    {
        if ($request['subcat_name'] != "") {
            $users = Subcategory::where('subcat_flag', 1)->where('subcat_name', 'like', '%' . $request['subcat_name'] . '%')->take(50)->get();

            return response()->json(['data' => $users, 'status' => $this->success_stat]);
        } else {
            $data = Subcategory::where('subcat_flag', 1)->take(50)->get();
            return response()->json(['data' => $data, 'status' => $this->success_stat]);
        }
    }
}
