<?php

namespace App\Http\Controllers\Api\Product;


use App\Http\Controllers\Api\ApiParent;
use Illuminate\Http\Request;
use App\Models\Product\Damage;
use Validator;


class DamageController extends ApiParent
{



	/**
	 * @api {post} http://127.0.0.1:8000/api/add_damaged_product Add Damage Product
	 * @apiName addDamagedProduct
	 * @apiGroup Product
	 * @apiVersion 0.1.0
	 * @apiHeader {String} Authorization Authorization (Required)
	 * @apiHeader {String} Accept Accept (Required)
	 *
	 * @apiParam {bigint} damage_prod_id Damaged Product Id (Required)
	 * @apiParam {double} damage_qty Damaged Product Quantity (Required)
	 * @apiParam {bigint} damage_unit_id Damaged Products Unit Id 
	 * @apiParam {bigint} damage_stock_id Damaged Products Stock Id 
	 * @apiParam {double} damage_rem_qty Damaged Products Remaining Quantity 
	 * @apiParam {double)} damage_purch_rate Purchase Rate
	 * @apiParam {varchar} damage_notes Notes
	 * @apiParam {varchar)} damage_serail Serail 
	 * @apiParam {date} damage_date Damage Date
	 * @apiParam {int} damage_added_by Damage Added Date
	 *  
	 * 
	 * @apiError {String} error Error Message.
	 * 
	 */



	public function addDamagedProduct(Request $request)
	{


		$validator = Validator::make(
			$request->all(),
			[

				'damage_prod_id' => 'required|exists:products',
				'damage_qty' => 'required',
				'damage_unit_id' => 'exists:units',


			],
			[

				'damage_prod_id.required' => 'Damage Product id required',
				'damage_qty.required' => 'Damage Product Quantity required',

			]
		);

		if ($validator->fails())
			return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);

		$data = $request->all();
		$res = Damage::create($data);
		if ($res)
			return response()->json(['message' => 'Saved Successfully', 'status' => $this->created_stat]);

		else
			return response()->json(['message' => 'Error While Adding Product, Please Contact Your Administrator', 'status' => $this->badrequest_stat]);
	}



	/**
	 * @api {post} http://127.0.0.1:8000/api/view_damaged_product View Damage Product
	 * @apiName viewDamagedProduct
	 * @apiGroup Product
	 * @apiVersion 0.1.0
	 * @apiHeader {String} Authorization Authorization (Required)
	 * @apiHeader {String} Accept Accept (Required)
	 *
	 * @apiParam {bigint} prd_id Damaged Product Id (Required)
	 * @apiParam {varchar} prd_name Damaged Product Name (Required)
	 *  
	 * @apiError {String} error Error Message.
	 * 
	 */


	public function viewDamagedProduct(Request $request)
	{
		$data = $request->all();

		$validator = Validator::make($request->all(), [
			'prd_name' => 'required',
			'prd_id' => 'required',
		], [
			'prd_name.required' => 'Product Name required',
			'prd_id.required' => 'Product Id Required'

		]);
		if ($validator->fails())
			return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);

		$res = Damage::select('damages.*', 'damages.id as damage_id', 'products.prd_name', 'products.prd_id')
			->join('products', 'damages.damage_prod_id', '=', 'products.prd_id')

			->where('products.prd_name', 'like', '%' . $data['prd_name'] . '%')
			->where('products.prd_id', $data['prd_id'])
			->where('damages.company_id', $data['company_id'])
			->where('damages.branch_id', $data['branch_id'])
			->get();

		if ($res) {
			return response()->json(['data' => $res, 'status' => $this->success_stat]);
		} else {
			return response()->json(['data' => 'No Result found', 'status' => $this->success_stat]);
		}
	}



	/**
	 * @api {post} http://127.0.0.1:8000/api/delete_damaged_product Delete Damage Product
	 * @apiName deleteDamegedProduct
	 * @apiGroup Product
	 * @apiVersion 0.1.0
	 * @apiHeader {String} Authorization Authorization (Required)
	 * @apiHeader {String} Accept Accept (Required)
	 *
	 * @apiParam {bigint} damage_prod_id Damaged Product Id (Required)
	 * @apiParam {bigint} damage_id Damage Id (Required)
	 *  
	 * @apiError {String} error Error Message.
	 * 
	 */




	public function deleteDamegedProduct(Request $request)
	{

		$data = $request->all();

		$validator = Validator::make($request->all(), [
			'damage_id' => 'required',
			'damage_prod_id' => 'required|exists:damages,damage_prod_id',

		], [
			'damage_id.required' => 'Damage Id Required',
			'damage_prod_id.required' => 'Product Id Required',
			'damage_prod_id.exists' => 'This Product is not added to Damages',


		]);
		if ($validator->fails())
			return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);

		$res = Damage::where('id', $data['damage_id'])->where('damage_prod_id', $data['damage_prod_id'])->where('company_id', $data['company_id'])->where('branch_id', $data['branch_id'])->delete();
		if ($res) {
			return response()->json(['data' => 'Removed Successfully', 'status' => $this->success_stat]);
		} else {
			return response()->json(['data' => 'Could Not Remove , Invalid Product', 'status' => $this->badrequest_stat]);
		}
	}





	/*
 *
     * @api {get} http://127.0.0.1:8000/api/get_damaged_product Get Damage Product
     * @apiName getDamagedProduct
     * @apiGroup Product
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
	 * 
     * @apiError {String} error Error Message.
     * 
     */


	public function getDamagedProduct()
	{

		return response()->json(['data' => Damage::get(), 'status' => $this->success_stat]);
	}
}
