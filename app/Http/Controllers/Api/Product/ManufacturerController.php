<?php

namespace App\Http\Controllers\Api\Product;


use App\Http\Controllers\Api\ApiParent;
use Illuminate\Http\Request;
use App\Models\Product\Manufacturer;
use Illuminate\Validation\Rule;
use Validator;




class ManufacturerController extends ApiParent
{



	/**
	 * @api {post} http://127.0.0.1:8000/api/add_manufacturer Add Manufacture
	 * @apiName addManufacturer
	 * @apiGroup Manufacture
	 * @apiVersion 0.1.0
	 * @apiHeader {String} Authorization Authorization (Required)
	 * @apiHeader {String} Accept Accept (Required)
	 * 
	 * @apiParam {varchar} manftr_comp_name Company Name (Required)
	 * @apiParam {varchar} manftr_comp_code Company Code (Required)
	 * @apiParam {varchar} manftr_comp_email Company Email (unique)
	 * @apiParam {mediumtext} manftr_comp_address Company Adderss 
	 * @apiParam {varchar} manftr_comp_phone Company Phone 
	 * @apiParam {varchar} manftr_comp_mobile Company Mobile 
	 * @apiParam {varchar} manftr_comp_fax Company Fax
	 * @apiParam {varchar} manftr_comp_contact_person Company Contact Person
	 * @apiParam {varchar} manftr_comp_website Company Website
	 * @apiParam {varchar} manftr_comp_notes Company Notes 
	 * 
	 * @apiSuccess {String} status Status Code
     * @apiSuccess {String} message Success Message
     *
	 * @apiError {String} error Error Message.
	 * 
	 */



	function addManufacturer(Request $request)
	{

		$validator = Validator::make(
			$request->all(),
			[
				'manftr_comp_name' => 'required',
				'manftr_comp_code' => 'required|unique:manufacturer',
				'manftr_comp_email' => 'nullable|unique:manufacturer',

			],
			[
				'manftr_comp_name.required' => 'Required',
				'manftr_comp_code.required' => 'Required',
				'manftr_comp_code.unique' => 'Already Exists',
				'manftr_comp_email.unique' => 'Already Exists',

			]
		);


		if ($validator->fails())
			return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);

		$data = $request->all();
		$data['manftr_comp_addedby'] = $this->usr_id;
		$res = Manufacturer::create($data);
		if ($res) {
			return response()->json(['message' => 'Saved Successfully', 'status' => $this->created_stat]);
		} else {
			return response()->json(['message' => 'Error While Adding, Please Contact Your Administrator', 'status' => $this->badrequest_stat]);
		}
	}



	/**
	 * @api {post} http://127.0.0.1:8000/api/edit_manufacturer Edit Manufacture
	 * @apiName editManufacturer
	 * @apiGroup Manufacture
	 * @apiVersion 0.1.0
	 * @apiHeader {String} Authorization Authorization (Required)
	 * @apiHeader {String} Accept Accept (Required)
	 * 
	 * @apiParam {int} id Company id (Required)
	 * @apiParam {varchar} manftr_comp_name Company Name (Required)
	 * @apiParam {varchar} manftr_comp_code Company Code (Required)
	 * @apiParam {varchar} manftr_comp_email Company Email (unique)
	 * @apiParam {mediumtext} manftr_comp_address Company Adderss 
	 * @apiParam {varchar} manftr_comp_phone Company Phone 
	 * @apiParam {varchar} manftr_comp_mobile Company Mobile 
	 * @apiParam {varchar} manftr_comp_fax Company Fax
	 * @apiParam {varchar} manftr_comp_contact_person Company Contact Person
	 * @apiParam {varchar} manftr_comp_website Company Website
	 * @apiParam {varchar} manftr_comp_notes Company Notes
	 * 
	 * 
	 * @apiSuccess {String} status Status Code
     * @apiSuccess {String} message Success Message
     *
	 * @apiError {String} error Error Message.
	 * 
	 */


	public function editManufacturer(Request $request)
	{

		$validator = Validator::make(
			$request->all(),
			[
				'id' => 'required|exists:manufacturer',
				'manftr_comp_name' => 'required',
				'manftr_comp_code' => ['required', Rule::unique('manufacturer')->ignore($request['id'], 'id')],
				'manftr_comp_email' => ['nullable', 'email', Rule::unique('manufacturer')->ignore($request['id'], 'id')],
			],
			[
				'id.required' => 'Required',
				'id.exists' => 'Invalid',
				'manftr_comp_name.required' => 'Required',
				'manftr_comp_code.required' => 'Required ',
				'manftr_comp_code.unique' => 'Already Exists',
				'manftr_comp_email.unique' => 'Already Exists',

			]
		);

		if ($validator->fails())
			return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);

		$input = $request->all();
		unset($input['branch_id']);
		$id = $input['id'];
		$data = Manufacturer::find($id);
		$res = $data->update($input);
		if ($res) {
			return response()->json(['message' => 'Updated Successfully', 'status' => $this->created_stat]);
		} else {
			return response()->json(['message' => 'Error While Updating, Please Contact Your Administrator', 'status' => $this->badrequest_stat]);
		}
	}


	/**
	 * @api {get} http://127.0.0.1:8000/api/get_manufacturers Get Manufactures
	 * @apiName getManufacturer
	 * @apiGroup Manufacture
	 * @apiVersion 0.1.0
	 * @apiHeader {String} Authorization Authorization (Required)
	 * @apiHeader {String} Accept Accept (Required)
	 * 
	 * @apiSuccess {String} status Status Code
     * @apiSuccess {String} message Success Message
	 * @apiError {String} error Error Message.
	 * 
	 */



	public function getManufacturer()
	{

		return response()->json(['data' => Manufacturer::where('manftr_comp_flags', 1)->get(), 'status' => $this->success_stat]);
	}



	/**
	 * @api {get} http://127.0.0.1:8000/api/search_manufacturers  Search Manufactures
	 * @apiName searchManufacture
	 * @apiVersion 0.1.0
	 * @apiGroup Manufacture
	 *
	 * @apiHeader {String} Authorization Authorization (Required)
	 * @apiHeader {String} Accept Accept (Required)
	 *
	 * @apiParam {varchar} manftr_comp_name Manufacture Name 
	 *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} data Manufacturers
	 */

	public function searchManufacture(Request $request)
	{
		if ($request['manftr_comp_name'] != "") {
			$mnfctr = Manufacturer::where('manftr_comp_name', 'like', '%' .  $request['manftr_comp_name'] . '%')->take(5)->get();

			return response()->json(['data' => $mnfctr, 'status' => $this->success_stat]);
		} else {
			$data = Manufacturer::take(5)->get();
			return response()->json(['data' => $data, 'status' => $this->success_stat]);
		}
	}
}
