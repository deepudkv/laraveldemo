<?php

namespace App\Http\Controllers\Api\Product;

use App\Http\Controllers\Api\ApiParent;
use App\Models\Product\Units;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class UnitsController extends ApiParent
{

    /**
     * @api {get} http://127.0.0.1:8000/api/get_units Get Units
     * @apiName getUnits
     * @apiGroup Units
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} data Units Details
     *
     * @apiError {String} error Error Message.
     *
     */

    public function getUnits()
    {
        return response()->json(['data' => Units::where('unit_flag',1)->get(), 'status' => $this->success_stat]);
    }

    /**
     * @api {get} http://127.0.0.1:8000/api/get_base_units  Get Base Units
     * @apiName getBaseUnits
     * @apiGroup Units
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} data All Base Units Details
     *
     */

   

    public function getBaseUnits($unit_id)
    {
        $units = Units::where('unit_flag', 1)
            ->where('unit_id', "!=", $unit_id)
            ->where('unit_type', 1)
            ->get();
        return response()->json(['data' => $units, 'status' => $this->success_stat]);
    }


    /**
     * @api {post} http://127.0.0.1:8000/api/get_base_unit_details Get Base Unit Details
     * @apiName getBaseUnitDetails
     * @apiGroup Units
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {int} unit_base_id Base Unit id (Required)
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} data Base Unit Details
     * @apiError {String} error Error Message.
     *
     */

     public function getBaseUnitDetails(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            [
                'unit_base_id' => 'required|exists:units,unit_id',

            ],
            [
                'unit_base_id.required' => 'Required',
                'unit_base_id.exists' => 'Invalid',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }

        return response()->json(['data' => Units::where('unit_flag', 1)->where('unit_type', 1)->where('unit_id', $request['unit_base_id'])->get(), 'status' => $this->success_stat]);
    }
    /**
     * @api {get} http://127.0.0.1:8000/api/get_drvd_units/{id} Get Derived Units
     * @apiName getDerivedUnits
     * @apiGroup Units
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} data Derived Unit Details
     * @apiError {String} error Error Message.
     *
     */

    public function getDerivedUnits($unit_base_id)
    {

        return response()->json(['data' => Units::where('unit_flag', 1)->where('unit_type', 0)->where('unit_base_id', $unit_base_id)->get(), 'status' => $this->success_stat]);
    }

    /**
     * @api {get} http://127.0.0.1:8000/api/has_drvd_units/{id} Has Derived Units
     * @apiName hasDerivedUnits
     * @apiGroup Units
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} derived_units Derived Units
     *
     * @apiError {String} error Error Message.
     *
     */

    public function hasDerivedUnits($baseunit_id)
    {
        $res = Units::where('unit_flag', 1)->where('unit_type', 0)->where('unit_base_id', $baseunit_id)->get()->isNotEmpty();

        return response()->json(['derived_units' => $res, 'status' => $this->success_stat]);
    }

    /**
     * @api {post} http://127.0.0.1:8000/api/add_unit Add Units
     * @apiName addUnits
     * @apiGroup Units
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {varchar} unit_name Unit Name (Required,Unique)
     * @apiParam {char} unit_code Unit Code (Required,Unique)
     * @apiParam {int} unit_type Unit Type (Required,Values : 0 - derived,1 - base)
     * @apiParam {int} unit_base_id Base Unit Id (Required)
     * @apiParam {int} unit_base_qty Base Unit Quantity (required_if:unit_type,0|nullable|numeric)
     * @apiParam {char} unit_display Unit Display
     * @apiParam {mediumtext} unit_remarks Unit Remarks
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} message Success message
     * @apiError {String} error Error Message.
     *
     */

    public function addUnits(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            [
                'unit_name' => 'required|unique:units',
                'unit_code' => 'required|unique:units',
                'unit_type' => 'required',
                'unit_base_id' => 'required',
                'unit_base_qty' => 'required_if:unit_type,0|nullable|numeric',
                'unit_display' => 'required',
            ],
            [
                'unit_name.required' => 'Required',
                'unit_name.unique' => 'Already Exits',
                'unit_code.required' => 'Required',
                'unit_code.unique' => 'Already Exists',
                'unit_type.required' => 'Required',
                'unit_base_id.required' => 'Required ',
                'unit_base_qty.required_if' => 'Required',
                'unit_base_qty.numeric' => 'Invalid Type',
                'unit_display.required' => 'Required',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }

        $unit_data = $request->all();

        Units::create($unit_data);
        return response()->json(['message' => 'Saved successfully', 'status' => $this->created_stat]);
    }

 /**
     * @api {post} http://127.0.0.1:8000/api/edit_unit Edit Units
     * @apiName editUnits
     * @apiGroup Units
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {int} unit_id Unit Id (Required)
     * @apiParam {varchar} unit_name Unit Name (Required,Unique)
     * @apiParam {char} unit_code Unit Code (Required,Unique)
     * @apiParam {int} unit_type Unit Type (Required,Values :     0 - derived,1 - base)
     * @apiParam {int} unit_base_id Base Unit Id (Required)
     * @apiParam {int} unit_base_qty Base Unit Quantity (required_if:unit_type,0|nullable|numeric)
     * @apiParam {char} unit_display Unit Display
     * @apiParam {mediumtext} unit_remarks Unit Remarks
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} message Success message
     *
     * @apiError {String} error Error Message.
     *
     */


    public function editUnits(Request $request)
    {

        $input = $request->all();
        unset($input['branch_id']);
        $id = $input['unit_id'];

        $validator = Validator::make(
            $request->all(),
            [
                'unit_id' => 'required|exists:units',
                'unit_name' => ['required', Rule::unique('units')->ignore($request['unit_id'], 'unit_id')],
                'unit_code' => ['required', Rule::unique('units')->ignore($request['unit_id'], 'unit_id')],
                'unit_type' => 'required',
                'unit_base_id' => 'required',
                'unit_base_qty' => 'required|numeric',
                'unit_display' => 'required',
            ],
            [
                'unit_id.required' => 'Required',
                'unit_id.exists' => 'Invalid',
                'unit_name.required' => 'Required',

                'unit_code.required' => 'Required',

                'unit_type.required' => 'Required',
                'unit_base_id.required' => 'Required',
                'unit_base_qty.required' => 'Required',
                'unit_base_qty.numeric' => 'Invalid',
                'unit_display.required' => 'Required',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }

        $data = Units::find($id);
        $data->fill($input)->save();
        return response()->json(['message' => 'Updated successfully', 'status' => $this->created_stat]);
    }

    /**
     * @api {get} http://127.0.0.1:8000/api/search_units  Search Units
     * @apiName searchUnits
     * @apiVersion 0.1.0
     * @apiGroup Units
     *
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {varchar} unit_name Unit Name
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} data Units

     */

    public function searchUnits(Request $request)
    {
        if ($request['unit_name'] != "") {
            $data = Units::where('unit_flag',1)->where('unit_name', 'like', '%' . $request['unit_name'] . '%')
                ->withCount([

                    'children as derived_units_count',
                ])

                ->take(50)
                ->get();
            return response()->json(['data' => $data, 'status' => $this->success_stat]);
        } else {
            $data = Units::take(5)->get();
            return response()->json(['data' => $data, 'status' => $this->success_stat]);
        }
    }

    /**
     * @api {get} http://127.0.0.1:8000/api/get_base_units  Get Base Units
     * @apiName getBaseUnitsAll
     * @apiVersion 0.1.0
     * @apiGroup User
     *
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} data Units

     */  

    public function getBaseUnitsAll()
    {

        $res = Units::where('unit_flag', 1)->where('unit_type', 1)->get();
        return response()->json(['data' => $res, 'status' => $this->success_stat]);

    }
}
