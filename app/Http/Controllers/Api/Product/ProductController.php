<?php

namespace App\Http\Controllers\Api\Product;

use App\Http\Controllers\Api\ApiParent;
use App\Models\Godown\GodownStock;
use App\Models\Production\ProductionProducts;
use App\Models\Production\ProductionSub;
use App\Models\Product\Feature;
use App\Models\Product\ProdFeatures;
use App\Models\Product\Product;
use App\Models\Product\ProdUnit;
use App\Models\Product\Subcategory;
use App\Models\Purchase\PurchaseSub;
use App\Models\Sales\SalesSub;
use App\Models\Stocks\BranchStocks;
use App\Models\Stocks\CompanyStocks;
use App\Models\Stocks\StockDailyUpdates;
use App\Models\Stocks\StockUnitRates;
use App\Models\Van\VanDailyStocks;
use App\Models\Van\VanTransferSub;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class ProductController extends ApiParent
{

    /**
     * @api {post} http://127.0.0.1:8000/api/add_product Add Product
     * @apiName addProduct
     * @apiGroup Product
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {varchar(500)} prd_name Product Name (Required,unique,max:150,min:3)
     * @apiParam {mediumtext} prd_alias Product Alias (Required,unique,max:50,min:3)
     * @apiParam {int(11)} prd_cat_id Category Id (Required,numeric,exists)
     * @apiParam {int(11)} prd_sub_cat_id Sub Category Id (nullable,numeric,exists)
     * @apiParam {int(11)} prd_supplier Manufacturer ID (nullable,numeric)
     * @apiParam {mediumtext} prd_short_name Product Short Name (Required,unique,max:50,min:3)
     * @apiParam {int(11)} prd_min_stock Product Min Stock (nullable,numeric)
     * @apiParam {int(11)} prd_max_stock Product Max Stock (nullable,numeric)
     * @apiParam {int(11)} prd_base_unit_id Base Unit ID (nullable,numeric,exists)
     * @apiParam {int(11)} prd_default_unit_id Default Unit ID (nullable,numeric,exists)
     * @apiParam {varchar(50)} prd_code Product Code (nullable|unique)
     * @apiParam {int(11)} prd_tax Product Tax Percentage
     * @apiParam {varchar(50)} prd_tax_code Product Tax Code
     * @apiParam {varchar} prd_barcode Product Bar code
     * @apiParam {text} prd_desc Product Description
     * @apiParam {tinyint(4)} prd_minstock_alert Minimum product stock Alert (values 1 or 0)
     * @apiParam {tinyint(4)} prd_maxstock_alert Maximum product stock Alert(values 1 or 0)
     * @apiParam {int(11)} prd_exp_dur Product Expriry Duration
     * @apiParam {mediumtext} prd_remarks Product Remarks
     * @apiParam {array} prd_unit_ids Product's Unit Id's and corresponding ean barcode for each units like as [ { "unit_id":1 , "ean_barcode" : "325FTHGJDGFDR4T", "unit_rate" : 89.26  }]
     * @apiParam {array} features Product's Features (Values: int feat_id ,varchar feat_val)
     * @apiParam {int(11)} purchase_rate Product purchase rate
     * @apiParam {array} unit_rates Product's  rate for each units  (Values: int unit_id ,varchar unit_rate)
     * @apiParam {array} branch_ids ( if adding stock to branches )

     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} message success message
     * @apiError {String} error Error Message.
     */

    public function addProduct(Request $request)
    {

        if ($request->basic) {
            return $this->basicValidation($request);
        } elseif ($request->unit == 1) {
            return $this->unitValidation($request);
        } elseif ($request->clrp == "D54ERK7") {
            return $this->cleanProductDb();
        } else {
            //validation sucess and calling product adding function
            return $this->addProductDetsils($request);
        }

    }

    public function basicValidation(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'prd_name' => 'required|unique:products|max:150|min:3',
                'prd_alias' => 'required|unique:products|max:50|min:3',
                'prd_cat_id' => 'required|exists:categories,cat_id',
                'prd_sub_cat_id' => 'nullable|exists:subcategories,subcat_id',
                'prd_supplier' => 'nullable',
                'prd_short_name' => 'required|unique:products|max:50|min:3',
                'prd_min_stock' => 'required_if:prd_minstock_alert,1|nullable|numeric',
                'prd_max_stock' => 'required_if:prd_maxstock_alert,1|nullable|numeric',
                'prd_code' => 'nullable|unique:products',

            ],
            [
                'prd_name.required' => 'Required',
                'prd_name.min' => 'Minimum 3 charecters',
                'prd_name.max' => 'Maximum 150 charecters',
                'prd_alias.min' => 'Minimum 3 charecters',
                'prd_alias.max' => 'Maximum 150 charecters',
                'prd_alias.unique' => 'Already exists',
                'prd_name.unique' => 'Already exists',
                'prd_short_name.min' => 'Minimum 3 charecters',
                'prd_short_name.max' => 'Maximum 150 charecters',
                'prd_short_name.unique' => 'Already exists',
                'prd_code.unique' => 'Already exists',
                'prd_cat_id.required' => 'Required',
                'prd_sub_cat_id.required' => 'Required',
                'prd_alias.required' => 'Required',
                'prd_short_name.required' => 'Required',
                'prd_cat_id.exists' => 'Invalid',
                'prd_sub_cat_id.exists' => 'Invalid',
                'prd_alias.unique' => 'Already exists',
                'prd_short_name.unique' => 'Already exist',
                'prd_min_stock.numeric' => 'Should be numeric',
                'prd_max_stock.numeric' => 'Should be numeric',
                'prd_min_stock.required_if' => 'Required',
                'prd_max_stock.required_if' => 'Required',
            ]
        );
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }

    }

    public function unitValidation(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'prd_base_unit_id' => 'required|exists:units,unit_id',
                'prd_default_unit_id' => 'exists:units,unit_id',
                // "prd_unit_ids.*" => "nullable|numeric|distinct",
            ],
            [
                'prd_base_unit_id.required' => 'Required',
                'prd_base_unit_id.exists' => 'Invalid',
                'prd_default_unit_id.exists' => 'Invalid',

            ]
        );
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }

    }

    public function addProductDetsils(Request $request)
    {

        $request['prd_sub_cat_id'] = $request->filled('prd_sub_cat_id') ? $request->prd_sub_cat_id : 0;
        $request['prd_supplier'] = $request->filled('prd_supplier') ? $request->prd_supplier : 0;
        $request['prd_min_stock'] = $request->filled('prd_min_stock') ? $request->prd_min_stock : 0;
        $request['prd_max_stock'] = $request->filled('prd_max_stock') ? $request->prd_max_stock : 0;
        $request['prd_default_unit_id'] = $request->filled('prd_default_unit_id') ? $request->prd_default_unit_id : 0;
        $request['prd_exp_dur'] = $request->filled('prd_exp_dur') ? $request->prd_exp_dur : 0;
        $request['prd_tax'] = $request->filled('prd_tax') ? $request->prd_tax : 0;
        $request['prd_remarks'] = $request->filled('prd_remarks') ? $request->prd_remarks : "";

        $product_data = $request->all();
        $product_data['prd_added_by'] = $this->usr_id;
        $product_data['prd_barcode'] = $this->generatePrdBarcode();
        $data = Product::create($product_data);
        $prodid = $data->prd_id;

        if (!$prodid) {
            return response()->json(['message' => 'Invalid Request', 'status' => $this->badrequest_stat]);
        }

        $units = $request['prd_unit_ids'];
        if (is_array($units) && !empty($units)) {

            $ProdUnit["produnit_prod_id"] = $prodid;
            $ProdUnit["company_id"] = $request->company_id;
            $ProdUnit["branch_id"] = $request->branch_id;
            $ProdUnit["server_sync_time"] = $request->server_sync_time;
            foreach ($units as $unit) {
                $ProdUnit["produnit_unit_id"] = $unit['unit_id'];
                $ProdUnit["produnit_ean_barcode"] = $unit['ean_barcode'];

                $stat1 = ProdUnit::create($ProdUnit);
            }
        } else {
            return response()->json(['message' => 'Something Went Wrong', 'status' => $this->badrequest_stat]);
        }

        $features = $request['features'];

        if (is_array($features) && !empty($features)) {

            $ProdFeatures["fetprod_prod_id"] = $prodid;
            $ProdFeatures["company_id"] = $request->company_id;
            $ProdFeatures["branch_id"] = $request->branch_id;
            $ProdFeatures["server_sync_time"] = $request->server_sync_time;

            foreach ($features as $feature) {
                $ProdFeatures["fetprod_fet_id"] = $feature['feat_id'];
                $ProdFeatures["fetprod_fet_val"] = $feature['feat_val'];

                ProdFeatures::create($ProdFeatures);
            }
            if (!$stat1) {
                return response()->json(['message' => 'Error Adding units', 'status' => $this->badrequest_stat]);
            }

        }

        $purchase_rate = $request->filled('purchase_rate') ? $request->purchase_rate : 0;
        $companyStocks['cmp_prd_id'] = $prodid;
        $companyStocks['cmp_avg_prate'] = $purchase_rate;
        $stocks = CompanyStocks::create($companyStocks); //adding company stocks//
        $stock_id = $stocks->cmp_stock_id;

        //code for adding stocks to company//
        $unit_rates = $request['unit_rates'];
        $branch_ids = $request['branch_ids'];

        $branch_ids[] = 0;
        if (!empty($branch_ids) && $stock_id && !empty($unit_rates)) {

            $branch_stocks["bs_stock_id"] = $stock_rates["sur_stock_id"] = $stock_id;
            $branch_stocks["bs_prd_id"] = $stock_rates["sur_prd_id"] = $prodid;
            $branch_stocks["bs_prate"] = $purchase_rate;
            $branch_stocks["bs_avg_prate"] = $purchase_rate;

            foreach ($branch_ids as $branch_id) {

                $branch_stocks["bs_branch_id"] = $stock_rates["sur_branch_id"] = $branch_id;
                $bstocks = BranchStocks::create($branch_stocks); //adding stocks to company branch//
                $stock_rates["branch_stock_id"] = $bstocks->branch_stock_id;

                foreach ($unit_rates as $rate) {
                    if (isset($rate['unit_rate']) && $rate['unit_rate'] > 0) {

                        $stock_rates["sur_unit_id"] = $rate['unit_id'];
                        $stock_rates["sur_unit_rate"] = $rate['unit_rate'];
                        StockUnitRates::create($stock_rates);

                    } //adding branch stock rates//
                }}
        } //end code for adding stocks//

        return response()->json(['message' => "Saved Successfully", 'status' => $this->created_stat]);
    }

    public function generatePrdBarcode()
    {
        $last_barcode = trim(Product::max('prd_barcode'));

        return ($last_barcode > 0) ? ($last_barcode + 1) : '1000000';

    }

    public function cleanProductDb()
    {
        //return response()->json(['message' => "Product Data Truncated", 'status' => $this->success_stat]);
        Product::query()->truncate();
        ProdUnit::query()->truncate();
        ProdFeatures::query()->truncate();
        CompanyStocks::query()->truncate();
        BranchStocks::query()->truncate();
        StockUnitRates::query()->truncate();

        return response()->json(['message' => "Product Data Truncated", 'status' => $this->success_stat]);
    }

    /**
     * @api {post} http://127.0.0.1:8000/api/edit_product  Edit Product
     * @apiName editProduct
     * @apiGroup Product
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {int(11)} prd_id Product id (Required,exists)
     * @apiParam {varchar(500)} prd_name Product Name (Required,unique,max:150,min:3)
     * @apiParam {mediumtext} prd_alias Product Alias (Required,unique,max:50,min:3)
     * @apiParam {int(11)} prd_cat_id Category Id (Required,numeric,exists)
     * @apiParam {int(11)} prd_sub_cat_id Sub Category Id (nullable,numeric,exists)
     * @apiParam {int(11)} prd_supplier Manufacturer ID (nullable,numeric)
     * @apiParam {mediumtext} prd_short_name Product Short Name (Required,unique,max:50,min:3)
     * @apiParam {int(11)} prd_min_stock Product Min Stock (nullable,numeric)
     * @apiParam {int(11)} prd_max_stock Product Max Stock (nullable,numeric)
     * @apiParam {varchar} prd_barcode Product Bar code
     * @apiParam {int(11)} prd_base_unit_id Base Unit ID (nullable,numeric,exists)
     * @apiParam {int(11)} prd_default_unit_id Default Unit ID (nullable,numeric,exists)
     * @apiParam {varchar(50)} prd_code Product Code (nullable|unique)
     * @apiParam {int(11)} prd_tax Product Tax Percentage
     * @apiParam {varchar(50)} prd_tax_code Product Tax Code
     * @apiParam {text} prd_desc Product Description
     * @apiParam {tinyint(4)} prd_minstock_alert Minimum product stock Alert (values 1 or 0)
     * @apiParam {tinyint(4)} prd_maxstock_alert Maximum product stock Alert(values 1 or 0)
     * @apiParam {int(11)} prd_exp_dur Product Expriry Duration
     * @apiParam {mediumtext} prd_remarks Product Remarks
     *
     * @apiError {String} error Error Message.
     *
     */

    public function editProduct(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            [
                'prd_id' => 'required|exists:products',
                'prd_name' => ['required', 'max:500', 'min:3', Rule::unique('products')->ignore($request['prd_id'], 'prd_id')],
                'prd_alias' => ['required', 'max:500', 'min:3', Rule::unique('products')->ignore($request['prd_id'], 'prd_id')],
                'prd_short_name' => ['required', 'max:500', 'min:3', Rule::unique('products')->ignore($request['prd_id'], 'prd_id')],
                'prd_code' => ['nullable', Rule::unique('products')->ignore($request['prd_id'], 'prd_id')],
                'prd_cat_id' => 'required|numeric|exists:categories,cat_id',
                'prd_sub_cat_id' => 'nullable',
                'prd_supplier' => 'nullable|numeric',
                'prd_min_stock' => 'required_if:prd_minstock_alert,1|nullable|numeric',
                'prd_max_stock' => 'required_if:prd_maxstock_alert,1|nullable|numeric',
                'prd_tax_cat_id' => 'required|numeric|min:1',
                // 'prd_base_unit_id' => 'required|exists:units,unit_id',
                // 'prd_base_unit_ean' => 'required',

            ],
            [
                'prd_id.required' => 'Required',
                'prd_id.exists' => 'Invalid',
                'prd_name.required' => 'Required',
                'prd_name.min' => 'Minimum 3 characters',
                'prd_name.max' => 'Maximum 500 characters',
                'prd_name.unique' => 'Already exists',
                'prd_alias.min' => 'Minimum 3 charecters',
                'prd_alias.max' => 'Maximum 500 charecters',
                'prd_alias.required' => 'Required',
                'prd_alias.unique' => 'Already exists',
                'prd_alias.min' => 'Minimum 3 characters',
                'prd_alias.max' => 'Maximum 500 charecters',
                'prd_short_name.required' => 'Required',
                'prd_short_name.unique' => 'Already exists',
                'prd_short_name.min' => 'Minimum 3 characters',
                'prd_short_name.max' => 'Maximum 50 characters',
                'prd_min_stock.required_if' => 'Required',
                'prd_max_stock.required_if' => 'Required',

                'prd_code.unique' => 'Already exists',
                'prd_cat_id.required' => 'Required',
                'prd_cat_id.exists' => 'Invalid',
                // 'prd_sub_cat_id.exists' => 'Invalid',
                'prd_min_stock.numeric' => 'Product minimum  should be numeric',
                'prd_max_stock.numeric' => 'Product maximum  should be numeric',
                'prd_tax_cat_id.required' => 'Required',
                'prd_tax_cat_id.numeric' => 'Required',
                'prd_tax_cat_id.min' => 'Required',

            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }

        $request['prd_sub_cat_id'] = ($request->filled('prd_sub_cat_id') ? $request->prd_sub_cat_id : 0);
        $request['prd_supplier'] = ($request->filled('prd_supplier') ? $request->prd_supplier : 0);
        $request['prd_min_stock'] = ($request->filled('prd_min_stock') ? $request->prd_min_stock : 0);
        $request['prd_max_stock'] = ($request->filled('prd_max_stock') ? $request->prd_max_stock : 0);
        $request['prd_exp_dur'] = ($request->filled('prd_exp_dur') ? $request->prd_exp_dur : 0);
        $request['prd_tax'] = ($request->filled('prd_tax') ? $request->prd_tax : 0);

        if (isset($request->prd_base_unit_ean)) {
            $request['prd_ean'] = ($request->filled('prd_base_unit_ean') ? $request->prd_base_unit_ean : 0);
        }

        $updates = $request->all();
        Product::find($updates['prd_id'])->fill($updates)->save();
        ProdUnit::where([['produnit_prod_id', $request->prd_id], ['produnit_unit_id', $request->prd_baseunitid]])->update(['produnit_ean_barcode' => $request->prd_base_unit_ean, 'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)]);

        return response()->json(['message' => 'Updated Successfully', 'status' => $this->created_stat]);
    }

    /**
     * @api {get} http://127.0.0.1:8000/api/get_products  Get Full Products
     * @apiName getProducts
     * @apiGroup Product
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} data Products
     * @apiError {String} error Error Message.
     *
     */

    public function deleteProduct(Request $request)
    {

        if ($request->prd_id) {

            Product::where('prd_id', $request->prd_id)->update(['prd_flag' => 5]);
            return response()->json(['message' => 'Product Deleted Successfully', 'status' => $this->created_stat]);
        } else {
            return response()->json(['error' => 'Invalid Product', 'status' => $this->badrequest_stat]);
        }

    }

    public function getProducts()
    {

        return response()->json(['data' => Product::where('prd_flag', 1)->get(), 'status' => $this->success_stat]);
    }

    public function getProdRelations($id)
    {

        $request['prd_id'] = $id;
        $validator = Validator::make(
            $request,
            [
                'prd_id' => 'required|exists:products',
            ],
            [
                'prd_id.required' => 'Required',
                'prd_id.exists' => 'Invalid',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        } else {
            return response()->json([
                'data' =>
                Product::where('prd_id', $id)->with('units')->with('features')->get(),
                'status' => $this->success_stat,
            ]);
        }
    }

    /**
     * @api {get} http://127.0.0.1:8000/api/get_prod_detail/{id}  Product Details
     * @apiName getProductdetail
     * @apiGroup Product
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {int} id Product Id (Get Method)
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} product Product Details
     * @apiSuccess {String} derunits Product derunits details
     * @apiSuccess {String} baseunit Product baseunit details
     * @apiSuccess {String} subcategories Product subcategories details
     * @apiSuccess {String} derunitids Product derunitids details
     * @apiSuccess {String} features Product features details
     * @apiError {String} error Error Message.
     *
     */

    public function getProductdetail($id)
    {

        $request['prd_id'] = $id;
        $validator = Validator::make(
            $request,
            [
                'prd_id' => 'required|exists:products',
            ],
            [
                'prd_id.required' => 'Required',
                'prd_id.exists' => 'Invalid product Id',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        } else {

            return response()->json([
                'product' => Product::where('prd_id', $id)->get(),
                'baseunit' => ProdUnit::select('prod_units.produnit_unit_id as baseunit_id', 'units.unit_name')->join('units', 'units.unit_id', 'prod_units.produnit_unit_id')->where('prod_units.produnit_prod_id', $id)->where('units.unit_type', 1)->get(),
                'derunits' => ProdUnit::select('prod_units.produnit_unit_id as derunit_id', 'units.unit_name')->join('units', 'units.unit_id', 'prod_units.produnit_unit_id')->where('prod_units.produnit_prod_id', $id)->where('units.unit_type', 0)->get(),
                'subcategories' => Subcategory::select('subcategories.subcat_id', 'subcategories.subcat_name')->join('products', 'products.prd_cat_id', 'subcategories.subcat_parent_category')->where('products.prd_id', $id)->get(),
                'derunitids' => ProdUnit::join('units', 'units.unit_id', 'prod_units.produnit_unit_id')->where('prod_units.produnit_prod_id', $id)->where('units.unit_type', 0)->pluck('prod_units.produnit_unit_id')->toArray(),
                'features' => ProdFeatures::select('feature_products.fetprod_fet_id as feat_id', 'feature_products.fetprod_fet_val as feat_val', 'features.feat_name')->join('features', 'features.feat_id', 'feature_products.fetprod_fet_id')->where('feature_products.fetprod_prod_id', $id)->get(),

                'is_delatable' => $this->checkIsProductDeletable($id),
                'status' => $this->success_stat,
            ]);
        }
    }

    public function checkIsProductDeletable($id)
    {

        $a = PurchaseSub::where(['purchsub_prd_id' => $id])->count();
        $b = SalesSub::where(['salesub_prod_id' => $id])->count();
        $c = VanTransferSub::where(['vantransub_prod_id' => $id])->count();
        $d = ProductionProducts::where(['prdnprd_prd_id' => $id])->count();
        $e = ProductionSub::where(['prdnsub_prd_id' => $id])->count();

        $f = GodownStock::where(['gs_prd_id' => $id])->count();
        $g = StockDailyUpdates::where(['sdu_prd_id' => $id])->count();
        $h = VanDailyStocks::where(['vds_prd_id' => $id])->count();

        if ($a > 0 || $b > 0 || $c > 0 || $d > 0 || $e > 0 || $f > 0 || $g > 0 || $h > 0) {
            return 0;
        }
        return 1;

    }

    // ======================= FEATURES ==========================

    /**
     * @api {post} http://127.0.0.1:8000/api/add_feature Add Features
     * @apiName addFeature
     * @apiGroup Product
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {int} feat_name Feature Name (Required,Unique)
     * @apiParam {varchar} feat_description Feature Description
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} message success message
     * @apiError {String} error Error Message.
     *
     */

    public function addFeature(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'feat_name' => 'required|unique:features',
            ],
            [
                'feat_name.required' => 'Required',
                'feat_name.unique' => 'Already exists',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }

        $data = $request->all();
        $data['feat_name'] = ucfirst($data['feat_name']);
        Feature::create($data);
        return response()->json(['message' => 'Saved Successfully', 'status' => $this->created_stat]);
    }

    /**
     * @api {post} http://127.0.0.1:8000/api/edit_feature Edit Features
     * @apiName editFeature
     * @apiGroup Product
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {bigint} feat_id Feature id (Required)
     * @apiParam {varchar(100)} feat_name Feature Name (Required,Unique)
     * @apiParam {varchar(200)} feat_description Feature Description
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} message success message
     * @apiError {String} error Error Message.
     *
     */

    public function editFeature(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            [
                'feat_id' => 'required',
                'feat_name' => ['required', Rule::unique('features')->ignore($request['feat_id'], 'feat_id')],
            ],
            [
                'feat_name.required' => 'Required',
                'feat_name.unique' => 'Already exists',
            ]
        );
        $id = $request['feat_id'];
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }

        $input = $request->all();
        unset($input['branch_id']);
        $data = Feature::find($id);
        $data->fill($input)->save();
        return response()->json(['message' => 'Updated Successfully', 'status' => $this->success_stat]);
    }

    /**
     * @api {get} http://127.0.0.1:8000/api/get_features Get Features
     * @apiName getFeatures
     * @apiGroup Product
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} data Features
     * @apiError {String} error Error Message.
     *
     */

    public function getFeatures()
    {

        return response()->json(['data' => Feature::get(), 'status' => $this->success_stat]);
    }

    /**
     * @api {get} http://127.0.0.1:8000/api/search_features Search Features
     * @apiName searchFeatures
     * @apiGroup Product
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {string} feat_name Feature Name to Search

     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} data Feature Details
     *
     * @apiError {String} error Error Message.
     *
     */
    public function searchFeatures(Request $request)
    {
        if ($request['feat_name'] != "") {
            $fet = Feature::where('feat_name', 'like', '%' . $request['feat_name'] . '%')->take(5)->get();

            return response()->json(['data' => $fet, 'status' => $this->success_stat]);
        } else {
            $data = Feature::take(5)->get();
            return response()->json(['data' => $data, 'status' => $this->success_stat]);
        }
    }

    //    =================== PRODUCT UNITS ======================

    /**
     * @api {post} http://127.0.0.1:8000/api/add_prod_units Add Product Units
     * @apiName addProdUnits
     * @apiGroup Product
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {int(11)} produnit_prod_id Product id (Required,exists)
     * @apiParam {int(11)} produnit_unit_id Unit id (Required,exists)
     *
     * @apiError {String} error Error Message.
     *
     */

    public function addProdUnits(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'produnit_prod_id' => 'required|exists:products,prd_id',
                'produnit_unit_id' => 'required|exists:units,unit_id',

            ],
            [
                'produnit_prod_id.required' => 'Required',
                'produnit_prod_id.exists' => 'Invalid',
                'produnit_unit_id.required' => 'Required',
                'produnit_unit_id.exists' => 'Invalid',

            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }

        $data = $request->all();

        ProdUnit::create($data);
        return response()->json(['message' => 'Saved Successfully', 'status' => $this->created_stat]);
    }

    /**
     * @api {post} http://127.0.0.1:8000/api/edit_prod_units Edit Product Units
     * @apiName addProdUnits
     * @apiGroup Product
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {int(11)} produnit_prod_id Product id (Required,exists)
     * @apiParam {array} prd_unit_ids Unit id (Required,exists)
     *
     * @apiError {String} error Error Message.
     *
     */

    public function editProdUnits(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            [

                "produnit_prod_id" => 'required|exists:products,prd_id',
                "prd_unit_ids.*" => "nullable|numeric|distinct",
            ],
            [
                'prd_unit_ids.required' => 'Required',
                'prd_unit_ids.numeric' => 'Should be numeric',

                'prd_unit_ids.*.distinct' => 'Please Remove Duplicate Units From List',
                'produnit_prod_id.exists' => 'Invalid',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }

        $units = array($request['prd_unit_ids']);
        $prodid = $request['produnit_prod_id'];

        if (is_array($units)) {

            $units = $units[0];

            $updates_unit["produnit_prod_id"] = $prodid;
            $updates_unit["company_id"] = $request->company_id;
            $updates_unit["branch_id"] = $request->branch_id;
            $updates_unit["server_sync_time"] = $request->server_sync_time;

            ProdUnit::where('produnit_prod_id', $prodid)->where('branch_id', $request['branch_id'])->where('company_id', $request['company_id'])->delete();

            foreach ($units as $val) {
                $updates_unit["produnit_unit_id"] = $val;

                $ures = ProdUnit::create($updates_unit);
            }
        } else {
            return response()->json(['message' => 'Error Units Format', 'status' => $this->success_stat]);
        }

        if ($ures) {
            return response()->json(['message' => 'Updated Successfully', 'status' => $this->success_stat]);
        } else {
            return response()->json(['message' => 'Updation Failed, Please contact your Administrator', 'status' => $this->success_stat]);
        }
    }

    /**
     * @api {post} http://127.0.0.1:8000/api/get_prod_units Get Product Units
     * @apiName getProdUnits
     * @apiGroup Product
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} data Product Units
     * @apiError {String} error Error Message.
     *
     */

    public function getProdUnits(Request $rq)
    {
        $pid = $rq['prd_id'];

        $data = ProdUnit::select('units.unit_id', 'units.unit_name', 'units.unit_code', 'units.unit_display', 'units.unit_type', 'units.unit_base_id', 'units.unit_base_qty', 'prod_units.produnit_ean_barcode as ean_barcode')
            ->join('units', 'units.unit_id', 'prod_units.produnit_unit_id')
            ->where('prod_units.produnit_prod_id', $pid)->get();

        return response()->json(['data' => $data, 'status' => $this->success_stat]);
    }

    // ================== PRODUCT FEATURES ====================

    /**
     * @api {post} http://127.0.0.1:8000/api/add_prod_features Add Product Features
     * @apiName addProdFeatures
     * @apiGroup Product
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {int(11)} fetprod_prod_id Product id (Required,exists)
     * @apiParam {array} fetprod_fet_id Unit id (Required,exists)
     *
     * @apiError {String} error Error Message.
     *
     */

    public function addProdFeatures(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'fetprod_prod_id' => 'required|exists:products,prd_id',
                'fetprod_fet_id' => 'required|exists:features,feat_id',

            ],
            [
                'fetprod_prod_id.required' => 'Required',
                'fetprod_prod_id.exists' => 'Invalid',
                'fetprod_fet_id.required' => 'Required',
                'fetprod_fet_id.exists' => 'Invalid',

            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }

        $data = $request->all();
        $prodid = $data->prd_id;
        $features = array($request['features']);
        if (is_array($features)) {

            $features = $features[0];
            $updates_fet["fetprod_prod_id"] = $prodid;
            $updates_fet["company_id"] = $request->company_id;
            $updates_fet["branch_id"] = $request->branch_id;
            $updates_fet["server_sync_time"] = $request->server_sync_time;

            $delfp = ProdFeatures::where('fetprod_prod_id', $prodid)->where('branch_id', $request['branch_id'])->where('company_id', $request['company_id'])->delete();
            if ($delfp) {
                foreach ($features as $fval) {

                    $updates_fet["fetprod_fet_id"] = $fval['feat_id'];
                    $updates_fet["fetprod_fet_val"] = $fval['feat_val'];

                    ProdFeatures::create($updates_fet);
                }
            } else {
                return response()->json(['message' => 'Invalid product or branch', 'status' => $this->success_stat]);
            }
        } else {
            return response()->json(['message' => 'Error Feature Format', 'status' => $this->success_stat]);
        }
    }

    /**
     * @api {post} http://127.0.0.1:8000/api/edit_prod_features Edit Product Features
     * @apiName editProdFeatures
     * @apiGroup Product
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {int(11)} fetprod_prod_id Product id (Required,exists)
     * @apiParam {array} features Features (Values : feat_id , feat_val)
     *
     * @apiError {String} error Error Message.
     *
     */

    public function editProdFeatures(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            [
                'fetprod_prod_id' => 'required|exists:products,prd_id',
                "features" => 'required',
            ],
            [
                'fetprod_prod_id.required' => 'Required',
                'fetprod_prod_id.exists' => 'Invalid',
                "features.required" => 'Required',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }

        $prodid = $request['fetprod_prod_id'];

        $features = array($request['features']);
        if (is_array($features)) {

            $features = $features[0];
            $updates_fet["fetprod_prod_id"] = $prodid;
            $updates_fet["company_id"] = $request->company_id;
            $updates_fet["branch_id"] = $request->branch_id;
            $updates_fet["server_sync_time"] = $request->server_sync_time;

            ProdFeatures::where('fetprod_prod_id', $prodid)->where('branch_id', $request['branch_id'])->where('company_id', $request['company_id'])->delete();

            foreach ($features as $fval) {

                $updates_fet["fetprod_fet_id"] = $fval['feat_id'];
                $updates_fet["fetprod_fet_val"] = $fval['feat_val'];

                $fres = ProdFeatures::create($updates_fet);
            }

            if ($fres) {
                return response()->json(['message' => 'Updated Successfully', 'status' => $this->success_stat]);
            }
        }
    }

    /**
     * @api {get} http://127.0.0.1:8000/api/get_prod_features Get All Product Features
     * @apiName getProdFeatures
     * @apiGroup Product
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} data Product Features
     * @apiError {String} error Error Message.
     *
     */
    public function getProdFeatures()
    {

        return response()->json(['data' => ProdFeatures::get(), 'status' => $this->success_stat]);
    }

    public function seed_products(Request $rq)
    {

        $data = $rq->all();
        for ($i = 0; $i < $data['count']; $i++) {
            $rq['prd_name'] = str_random($i + 1) . $data['prd_name'] . time();
            $rq['prd_alias'] = str_random($i + 1) . $data['prd_alias'] . time();
            $rq['prd_short_name'] = str_random($i + 1) . $data['prd_short_name'] . time();
            $res = $this->addProductDetsils($rq);
        }
        if ($res) {
            return response()->json(['data' => "$rq->count Products added successfully", 'status' => $this->success_stat]);
        } else {
            return response()->json(['error' => 'something went wrong!', 'status' => $this->badrequest_stat]);
        }

    }

    /**
     * @api {post} http://127.0.0.1:8000/api/get_search Search Proucts By Name
     * @apiName getSearch
     * @apiGroup Product
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {varchar} prod_name Product Name
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} data Products
     * @apiError {String} error Error Message.
     *
     */

    public function getSearch(Request $request)
    {

        $stk_stat = isset($request['stk_stat']) ? 1 : 0;

        if ($request['prod_name'] != "") {

            if ($stk_stat) {
                return response()->json(['data' => Product::where('prd_stock_status', 1)
                        ->where('prd_flag', 1)
                        ->where('prd_name', 'like', '%' . $request['prod_name'] . '%')
                        ->join('units', 'units.unit_id', 'products.prd_base_unit_id')
                        ->groupBy('products.prd_id')->take(70)->get(), 'status' => $this->success_stat]);
            }

            return response()->json(['data' => Product::where('prd_name', 'like', '%' . $request['prod_name'] . '%')
                    ->where('prd_flag', 1)
                    ->join('units', 'units.unit_id', 'products.prd_base_unit_id')
                    ->groupBy('products.prd_id')->take(70)->get(), 'status' => $this->success_stat]);
        } else {

            if ($stk_stat) {
                return response()->json(['data' => Product::where('prd_stock_status', 1)
                        ->where('prd_flag', 1)->join('units', 'units.unit_id', 'products.prd_base_unit_id')
                        ->groupBy('products.prd_id')->take(70)->get(), 'status' => $this->success_stat]);
            }
            return response()->json(['data' => Product::join('units', 'units.unit_id', 'products.prd_base_unit_id')->groupBy('prd_id')->take(70)->get(), 'status' => $this->success_stat]);

        }
    }
    /*
     * @api {get} http://127.0.0.1:8000/api/testproduct/{id}  Product Details
     * @apiName Test
     * @apiGroup Product
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {int} id Product Id (Get Method)
     *
     * @apiError {String} error Error Message.
     *
     */

    public function testProd($id)
    {
        $request['prd_id'] = $id;
        $validator = Validator::make(
            $request,
            [
                'prd_id' => 'required|exists:products',
            ],
            [
                'prd_id.required' => 'Required',
            ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        } else {
            $prod = Product::find($id);
            return response()->json(['data' => $prod, 'tt' => $prod->units]);
        }
    }

    public function unitItemWise(Request $rq)
    {
        $data = ProdUnit::join('units', 'prod_units.produnit_unit_id', 'units.unit_id')->where('produnit_prod_id', $rq->prd_id)->get();
        return response()->json(['data' => $data, 'status' => $this->success_stat]);
    }

}
