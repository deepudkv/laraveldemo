<?php

namespace App\Http\Controllers\Api\Company;

use App\Http\Controllers\Api\ApiParent;
use App\Models\Company\Acc_branch;
use App\Models\Company\Company;
use App\Models\Company\CompanySettings;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use validator;

class BranchController extends ApiParent
{

    /**
     * @api {post} http://127.0.0.1:8000/api/add_branch  Add Branch
     * @apiName addBranch
     * @apiGroup Branch
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {varchar} branch_name Branch name (Required)
     * @apiParam {varchar} branch_code Barnch Code (Required)
     * @apiParam {varchar} branch_address Barnch Address
     * @apiParam {varchar} branch_notes Barnch Notes
     * @apiParam {int} branch_default Barnch Default
     * @apiParam {smallint} branch_flags Barnch Status Flag
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} message Success Message
     *
     * @apiError {String} message Error Message
     * @apiError {String} status Status Code
     *
     */
    public function addBranch(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            [
                'branch_name' => 'required',
                'branch_code' => 'required|unique:acc_branches',
                'branch_password' => 'required',
                'confirm_password' => 'required',
                'confirm_password' => 'same:branch_password',
                'branch_email' => 'email',
                'branch_display_name' => 'required',
                'branch_open_date' => 'required',
            ],
            [
                'branch_name.required' => 'Required',
                'branch_name.unique' => 'Already Exisit',
                'branch_code.required' => 'Required',
                'branch_code.unique' => 'Already Exisit',
                'branch_password.required' => 'Required',
                'branch_email.email' => 'Invalid Email',
                'confirm_password.same' => 'Password does not match',
                'confirm_password.required' => 'Required',
                'branch_display_name' => 'Required',
                'branch_open_date' => 'Required',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }

        $data = $request->all();
        $data["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);
        unset($data['confirm_password']);
        $data['branch_password'] = md5($data['branch_password']);
        $data['branch_open_date'] = date('Y-m-d',strtotime($data['branch_open_date']));
        
        $res = Acc_branch::create($data);
        $dues = app('App\Http\Controllers\Api\Migration\ledgerOpeingbalanceController')->setTaxLedger(false);
        if ($res) {
            return response()->json(['message' => 'Branch Created', 'status' => $this->created_stat]);
        } else {
            return response()->json(['message' => 'Invalid Data', 'status' => $this->badrequest_stat]);
        }
    }

    /**
     * @api {post} http://127.0.0.1:8000/api/edit_branch  Edit Branch
     * @apiName editBranch
     * @apiGroup Branch
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {Int} b_id Branch Id to Edit (Required)
     * @apiParam {Int} branch_id Branch Id of Auth User  (Required)
     * @apiParam {varchar} branch_name Branch name (Required)
     * @apiParam {varchar} branch_code Barnch Code
     * @apiParam {varchar} branch_address Barnch Address
     * @apiParam {varchar} branch_notes Barnch Notes
     * @apiParam {int} branch_default Barnch Default
     * @apiParam {smallint} branch_flags Barnch Status Flag
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} message Success Message
     *
     * @apiError {String} message Error Message
     * @apiError {String} status Status Code
     *
     */

    public function editBranch(Request $request)
    {

        $id = $request['b_id'];
        $request['branch_id'] = $request['b_id'];
        unset($request['b_id']);

        $validator = Validator::make(
            $request->all(),
            [
                'branch_id' => 'required|exists:acc_branches',
                'branch_name' => 'required',
                'branch_code' => ['required', Rule::unique('acc_branches')->ignore($request['branch_id'], 'branch_id')],
                'confirm_password' => 'same:branch_password',
                'branch_display_name' => 'required',
            ],
            [

                'branch_display_name.required' => 'Required',
                'branch_name.required' => 'Required',
                'branch_code.required' => 'Required',
                'branch_code.unique' => 'Already Exists',
                'confirm_password.same' => 'Password does not match',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }

        $input = $request->all();
        $input['branch_open_date'] = date('Y-m-d',strtotime($input['branch_open_date']));
        $input["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);
        $data = Acc_branch::find($id);
        if (isset($input['branch_password'])) {
            $input['branch_password'] = md5($input['branch_password']);
        } else {
            unset($input['branch_password']);
        }
        unset($input['branch_password_old']);
        unset($input['confirm_password']);
        $res = $data->fill($input)->save();
        if ($res) {
            return response()->json(['message' => 'Branch edited', 'status' => $this->created_stat]);
        } else {
            return response()->json(['message' => 'Branch not  edited', 'status' => $this->badrequest_stat]);
        }
    }

    /**
     * @api {get} http://127.0.0.1:8000/api/get_branches  Get All Branches
     * @apiName getBranches
     * @apiGroup Branch
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} data All Branch Data
     *
     */
    public function getBranches()
    {

        $branch =Acc_branch::get()->toArray();
        return response()->json(['data' =>$branch,'selected' => array_column($branch,'branch_id'), 'status' => $this->success_stat]);
    }


    public function getBranchesExceptLoginBranch() {
        $info = Acc_branch::where('branch_id','!=',$this->branch_id)->get();
        return parent::displayData($info);
    }

    /**
     * @api {Post} http://127.0.0.1:8000/api/search_branch  Search Branches
     * @apiName searchBranch
     * @apiGroup Branch
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {String} branch_name Branch Keyword
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} data Branch Data
     *
     */

    public function searchBranch(Request $request)
    {

        if ($request->branch_id == 0 || $request->getallbranch) {
            $qry = Acc_branch::where('branch_name', 'like', '%' . $request['branch_name'] . '%');
            if($this->branch_id != 0){
                $qry->where('branch_id', '!=', $this->branch_id);
            }
        }else{
            $qry = Acc_branch::where('branch_id', $this->branch_id)->where('branch_name', 'like', '%' . $request['branch_name'] . '%');
        }
        // if ($request->branch_id > 0) {
        //     $qry = Acc_branch::where('branch_id', $this->branch_id)->where('branch_name', 'like', '%' . $request['branch_name'] . '%');
        // } else {
        //     $qry = Acc_branch::where('branch_name', 'like', '%' . $request['branch_name'] . '%');
        // }

        $acc_ledger = $qry->take(50)->get();
        return response()->json(['data' => $acc_ledger, 'status' => $this->success_stat]);

    }

    public function branchData(Request $request)
    {
        return response()->json(['data' => Acc_branch::where('branch_id', $request->user_branch_id)->first(), 'status' => $this->success_stat]);
    }

    public function companyData(Request $request)
    {
        return parent::displayData(CompanySettings::first());
    }

    public function updateCompany(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            [
                // 'cmp_finyear_end' => 'required',
                'cmp_finyear_start' => 'required',
                'cmp_code' => 'required',
                'cmp_email' => 'required',
                'cmp_name' => 'required',

            ],
            [
                // 'cmp_finyear_end.required' => 'Required',
                'cmp_finyear_start.required' => 'Required',
                'cmp_code.required' => 'Required',
                'cmp_email.required' => 'Required',
                'cmp_name.required' => 'Required',
            ]
        );
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }

        $update = $request->all();
        unset($update['cmp_id']);
        unset($update['company_id']);
        unset($update['branch_id']);
        $update["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);

        $update['cmp_finyear_start'] = date('Y-m-d', strtotime($update['cmp_finyear_start']));
        $update['cmp_finyear_end'] = date('Y-m-d', strtotime($update['cmp_finyear_end']));
        $data = CompanySettings::where('cmp_id', $request['cmp_id'])->update($update);

        return parent::displayMessage('Deatils Updated');
    }

    public function usersList()
    {
        if ($this->branch_id > 0) {
            $data = User::select('users.*', 'user_types.*','acc_branches.branch_name')->Join('user_types', function ($join) {
                $join->on('users.usr_type', 'user_types.type_id');
            })
            ->Join('acc_branches', function ($join) {
                $join->on('users.branch_id', 'acc_branches.branch_id');
            })->where('users.branch_id', $this->branch_id)->orderBy('users.usr_id', 'desc')->paginate(100);
        } else {
            $data = User::select('users.*','user_types.*','acc_branches.branch_name')->Join('user_types', function ($join) {
                $join->on('users.usr_type', 'user_types.type_id');
            })->leftJoin('acc_branches', function ($join) {
                $join->on('users.branch_id', 'acc_branches.branch_id');
            })->orderBy('users.usr_id','desc')->paginate(100);

            //print_r($data);die("56765");
    
        }
        return response()->json(['data' => $data, 'status' => $this->success_stat]);

    }


/**
 * @api {post} http://127.0.0.1:8000/api/search_users  Search Users
 * @apiName searchUsers
 * @apiVersion 0.1.0
 * @apiGroup User
 *
 * @apiHeader {String} Authorization Authorization (Required)
 * @apiHeader {String} Accept Accept (Required)
 *
 * @apiParam {varchar} usr_name User Name
 *
 * @apiSuccess {String} status Status Code
 * @apiSuccess {String} data All Users

 */

public function searchUsers(Request $request)
{
    if ($request['usr_name'] != "") {
        $users = user::where('usr_name', 'like', '%' . $request['usr_name'] . '%');
        
        if($this->branch_id >0)
        $users = $users->where('branch_id',$this->branch_id);

        $users = $users->take(50)->get();

        return response()->json(['data' => $users, 'status' => $this->success_stat]);
    } else {

        if($this->branch_id >0){
            $data = user::where('branch_id',$this->branch_id)->take(50)->get();
        }else{
            $data = user::take(50)->get();
        }
       
        return response()->json(['data' => $data, 'status' => $this->success_stat]);
    }
}

    public function getBranchesUnit(Request $rq)
    {
        return response()->json(['data' => Acc_branch::join('stock_unit_rates', 'stock_unit_rates.sur_branch_id', '=' ,'acc_branches.branch_id')->where('stock_unit_rates.sur_prd_id',$rq->prd_id)->where('acc_branches.branch_id','!=',$this->branch_id)->groupBy('acc_branches.branch_name')->get(array('acc_branches.branch_name as branch_name',
    'acc_branches.branch_id as branch_id')), 'status' => $this->success_stat]);
    }

    public function getStockBranches()
    {
        return response()->json(['data' => Acc_branch::join('stock_unit_rates', 'stock_unit_rates.sur_branch_id', '=' ,'acc_branches.branch_id')->where('acc_branches.branch_id','!=',$this->branch_id)->groupBy('acc_branches.branch_name')->get(array('acc_branches.branch_name as branch_name',
    'acc_branches.branch_id as branch_id')), 'status' => $this->success_stat]);
    }

}

//
