<?php

namespace App\Http\Controllers\Api\Reports;

use App\Http\Controllers\Api\ApiParent;
use App\Models\BranchTransfer\BranchTransfer;
use App\Models\BranchTransfer\BranchTransferSub;
use App\Models\Product\Product;
use App\Models\Product\ProdUnit;
use App\Models\Product\Units;
use App\Models\Stocks\BranchStocks;
use App\Models\Stocks\CompanyStocks;
use App\Models\Stocks\StockDailyUpdates;
use DB;


class TransferReportController extends ApiParent
{
    public function Report(Request $request, $type)
    {

        switch ($type) {
            case 'summary':
                return parent::displayData($this->productSummary($request));
                break;
                
            case 'product_based':
                return parent::displayData($this->productBasedTransfer());
                break;

            default:
                return parent::displayError('Invalid Endpoint', $this->badrequest_stat);
                break;
        }
    }

    /**
     * @api {post} http://127.0.0.1:8000/api/product_report/summary  Product Summary
     * @apiName productSummary
     * @apiGroup Report
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {bigint} unitid Unit Id
     * @apiParam {bigint} cat_id Category Id
     * @apiParam {bigint} sub_cat_id Subcategory Id
     * @apiParam {bigint} manufacture_id Manufacture Id
     * @apiParam {int} feat_id Feature Id
     * @apiParam {string} feat_val Feature Value
     * @apiParam {string} vat_type Vat Filter Type (Between,<,>,<=,>=,=)
     * @apiParam {int} vat_val_1 Vat Filter Value 1
     * @apiParam {int} vat_val_2 Vat Filter Value 2
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} data Product Summary
     *
     *
     */

    public function productSummary($request)
    {

        $rltn = [

            'category' => function ($qr) {
                $qr->select('cat_id', 'cat_name');
            },
            'sub_category' => function ($qr) {
                $qr->select('subcat_id', 'subcat_name');
            },

            'features' => function ($qr) {
                $qr->select('feat_id', 'fetprod_prod_id', 'feat_name', 'fetprod_fet_val');
            },

            'units' => function ($qr) {
                $qr->select('unit_id', 'unit_name')->where('unit_type', 1);
            },

        ];

        $qry = Product::select(
            'prd_id',
            'prd_barcode',
            'prd_ean',
            'prd_name',
            'prd_alias',
            'prd_code',
            'prd_cat_id',
            'prd_sub_cat_id',
            \DB::raw('(SELECT cat_name FROM erp_categories WHERE erp_products.prd_cat_id = erp_categories.cat_id ) as cat_name'),

            DB::raw('(SELECT subcat_name FROM erp_subcategories WHERE erp_products.prd_sub_cat_id = erp_subcategories.subcat_id ) as subcat_name')
        )
            ->orderBy('cat_name', 'ASC')->with($rltn);

        if ($request['unitid'] != "") {
            $qry = $qry->where('prd_base_unit_id', $request['unitid']);
        }

        if ($request['prd_id'] != "") {
            $qry = $qry->where('prd_id', $request['prd_id']);
        }
        if (is_int($request['prd_stock_stat'])) {
            $qry = $qry->where('prd_stock_status', $request['prd_stock_stat']);
        }
        
        if ($request['cat_id'] != "") {
            $qry = $qry->where('prd_cat_id', $request['cat_id']);
        }

        if ($request['sub_cat_id'] != "") {
            $qry = $qry->where('prd_sub_cat_id', $request['sub_cat_id']);
        }
        if ($request['prd_ean'] != "") {
            $qry = $qry->where('prd_ean', $request['prd_ean']);
        }

        if ($request['manufacture_id'] != "") {
            $qry = $qry->where('prd_supplier', $request['manufacture_id']);
        }

        if ($request['feat_id'] != "") {

            $featPrd = ProdFeatures::select('fetprod_prod_id')->where('fetprod_fet_id', $request['feat_id']);

            if ($request['feat_val'] != "") {
                $featPrd->where('fetprod_fet_val', $request['feat_val']);
            }

            $featPrd->get()->pluck('fetprod_prod_id')->toArray();

            $qry->whereIn('prd_id', $featPrd);
        }

        if ($request['vat_type'] != "") {

            switch ($request['vat_type']) {
                case '<':
                    $qry = $qry->where('prd_tax', '<', $request['vat_val_1']);
                    break;
                case '<=':
                    $qry = $qry->where('prd_tax', '<=', $request['vat_val_1']);
                    break;
                case '=':
                    $qry = $qry->where('prd_tax', '=', $request['vat_val_1']);
                    break;
                case '>':
                    $qry = $qry->where('prd_tax', '>', $request['vat_val_1']);
                    break;
                case '>=':
                    $qry = $qry->where('prd_tax', '>=', $request['vat_val_1']);
                    break;
                case 'between':
                    $qry = $qry->whereBetween('prd_tax', [$request['vat_val_1'], $request['vat_val_2']]);
                    break;
                default:
                    return parent::displayError('Invalid Entry', $this->badrequest_stat);
                    break;
            }
        }

        $data = $qry->get()->toArray();
        $result['data'] =  $qry->paginate(10);
        $result['total_products'] = count(array_column($data, 'prd_id'));
        $result['total_categories'] = count(array_unique(array_column($data, 'prd_cat_id')));
      


        return $result;


    }



}
