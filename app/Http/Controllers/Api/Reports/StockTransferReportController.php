<?php

namespace App\Http\Controllers\Api\Reports;

use App\Http\Controllers\Api\ApiParent;
use App\Models\BranchTransfer\BranchTransfer;
use App\Models\BranchTransfer\BranchTransferSub;
use DB;
use Illuminate\Http\Request;

class StockTransferReportController extends ApiParent
{

    public function Report(Request $request, $type)
    {

        switch ($type) {
            case 'product_based':
                return parent::displayData($this->productReport($request));
                break;
            case 'product_received':
                return parent::displayData($this->productReceiptReport($request));
                break;

            case 'summary':
                return parent::displayData($this->summaryReport($request));
                break;
            default:
                return parent::displayError('Invalid Endpoint', $this->badrequest_stat);
                break;
        }
    }
    public function summaryReport(Request $request)
    {
        $amnt = DB::raw('(stocktrsub_rate * stocktrsub_qty) as purchase_amount');
        $this->inrSlct = ['stocktrsub_prd_id', 'stocktrsub_branch_stock_id', 'stocktrsub_qty', 'stocktrsub_rate',
            'stocktrsub_prate', 'stocktrsub_unit_id', $amnt];
        $rltn = [
            'items' => function ($qr) {
                $qr->select('*');
            },
            'branch' => function ($qr) {
                $qr->select('branch_id', 'branch_name', 'branch_display_name', 'branch_code', 'branch_address', 'branch_phone', 'branch_mob', 'branch_tin', 'branch_reg_no');
            },
            'frombranch' => function ($qr) {
                $qr->select('branch_id', 'branch_name', 'branch_display_name', 'branch_code', 'branch_address', 'branch_phone', 'branch_mob', 'branch_tin', 'branch_reg_no');
            },
        ];
        $qry = BranchTransfer::select('*')->where('stocktr_accepted',1)
            ->where('stocktr_flags', 1)
            ->where('branch_id', $this->branch_id)->with($rltn);

        $data = $qry->orderBy('stocktr_id', 'DESC')->get()->toArray();
        $excel = [];
        foreach ($data as $t => $transfer) {
            $excel[$t]['Transferd Id'] = $transfer['stocktr_id'];
            $excel[$t]['Transfer Date'] = $transfer['stocktr_date'];
            $excel[$t]['Transferd To'] = $transfer['branch']['branch_name'];
            $excel[$t]['Total Amount'] = $transfer['stocktr_amount'];
            $excel[$t]['Remark'] = $transfer['stocktr_notes'];

        }
        return $excel;

    }
    public function productReport(Request $rqst)
    {
        $inv_filter = isset($rqst['inv_filter']) ? $rqst['inv_filter'] : 0;
        $inv_val = isset($rqst['inv_val']) ? $rqst['inv_val'] : 0;
        $inv_val2 = isset($rqst['inv_val2']) ? $rqst['inv_val2'] : 0;

        $godown_id = isset($rqst['godown_id']) ? $rqst['godown_id'] : -5;

        $totQty = DB::raw('sum(stocktrsub_qty) as totQty');
        $totalAmount = DB::raw('(sum(stocktrsub_qty * stocktrsub_rate)) as totalAmount');
        $slct = ['products.prd_name', 'categories.cat_name', $totQty, $totalAmount];

        $qry = BranchTransferSub::select($slct);

        $qry->join('products', 'stock_transfer_sub.stocktrsub_prd_id', 'products.prd_id')
            ->join('categories', 'categories.cat_id', 'products.prd_cat_id')
            ->where('stock_transfer_sub.stocktrsub_accepted', 1)
            ->where('stock_transfer_sub.stocktrsub_flags', 1)
            ->where('stock_transfer_sub.branch_id', $this->branch_id);

        if ($rqst['branch_to']) {
            $qry->where('stocktrsub_to', $rqst['branch_to']);
        }

        if ($godown_id > -5) {
            $qry->where('stocktrsub_gd_id', $godown_id);
        }

        if ($rqst['prd_id']) {
            $qry = $qry->where('products.prd_id', $rqst['prd_id']);
        }

        if ($rqst['prd_cat_id']) {
            $qry = $qry->where('categories.cat_id', $rqst['prd_cat_id']);
        }

        if ($inv_filter && $inv_val) {
            switch ($inv_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $qry->where('stocktrsub_stocktr_id', $inv_filter, $inv_val);
                    break;
                case 'between':
                    if ($inv_val2) {
                        $qry->whereBetween('stocktrsub_stocktr_id', [$inv_val, $inv_val2]);
                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }

        if ($ptype = isset($rqst['period_type']) ? $rqst['period_type'] : 0) {
            list($date1, $date2) = $this->datesFromPeriods($ptype, $rqst);

            if ($date1 && $date2) {
                $qry = $qry->whereBetween('stocktrsub_date', [$date1, $date2]);
            }
        }

        $allData = $qry->groupBy('products.prd_name')->orderby('cat_name', 'asc')->get()->toArray();
        $data = $qry->groupBy('products.prd_name')->orderby('cat_name', 'asc')->paginate($this->per_page)->toArray();

        if (empty($data['data'])) {
            return $data;
        }

        foreach ($data['data'] as $key => $val) {
            $result[$val['cat_name']][] = $val;
        }

        $i = 0;

        foreach ($result as $key => $val) {
            $cat['cat_name'] = $key;
            $cat['amount'] = array_sum(array_column($val, 'totalAmount'));
            $cat['qty'] = array_sum(array_column($val, 'totQty'));
            $cat['rate'] = round($cat['amount'] / $cat['qty'], 2);

            $res[$i]['category'][] = $cat;
            $res[$i]['items'] = $val;
            $i++;
        }
        $totalRes['total_products'] = count($allData);
        $totalRes['total_purchase_amount'] = array_sum(array_column($allData, 'totalAmount'));
        $totalRes['total_qty'] = array_sum(array_column($allData, 'totQty'));
        $totalRes['total_categories'] = count(array_unique(array_column($allData, 'cat_name')));

        $data['alldetais'] = $totalRes;
        $data['data'] = $res;

        if ($rqst['period_type'] != "") {
            $data['Date']['date1'] = date("d-F-Y", strtotime($date1));
            $data['Date']['date2'] = date("d-F-Y", strtotime($date2));
        } else {
            $data['Date'] = "";
        }
        return $data;

    }
    public function productReceiptReport(Request $rqst)
    {
        $inv_filter = isset($rqst['inv_filter']) ? $rqst['inv_filter'] : 0;
        $inv_val = isset($rqst['inv_val']) ? $rqst['inv_val'] : 0;
        $inv_val2 = isset($rqst['inv_val2']) ? $rqst['inv_val2'] : 0;

        $totQty = DB::raw('sum(stocktrsub_qty) as totQty');
        $totalAmount = DB::raw('(sum(stocktrsub_qty * stocktrsub_rate)) as totalAmount');
        $slct = ['products.prd_name', 'categories.cat_name', $totQty, $totalAmount];

        $qry = BranchTransferSub::select($slct);

        $qry->join('products', 'stock_transfer_sub.stocktrsub_prd_id', 'products.prd_id')
            ->join('categories', 'categories.cat_id', 'products.prd_cat_id')
            ->where('stock_transfer_sub.stocktrsub_accepted', 1)
            ->where('stock_transfer_sub.stocktrsub_flags', 1)
            ->where('stock_transfer_sub.stocktrsub_to', $this->branch_id);

        if ($rqst['branch_to']) {
            $qry->where('stock_transfer_sub.branch_id', $rqst['branch_to']);
        }

        if ($rqst['prd_id']) {
            $qry = $qry->where('products.prd_id', $rqst['prd_id']);
        }

        if ($rqst['prd_cat_id']) {
            $qry = $qry->where('categories.cat_id', $rqst['prd_cat_id']);
        }

        if ($inv_filter && $inv_val) {
            switch ($inv_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $qry->where('stocktrsub_stocktr_id', $inv_filter, $inv_val);
                    break;
                case 'between':
                    if ($inv_val2) {
                        $qry->whereBetween('stocktrsub_stocktr_id', [$inv_val, $inv_val2]);
                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }

        if ($ptype = isset($rqst['period_type']) ? $rqst['period_type'] : 0) {
            list($date1, $date2) = $this->datesFromPeriods($ptype, $rqst);

            if ($date1 && $date2) {
                $qry = $qry->whereBetween('stocktrsub_date', [$date1, $date2]);
            }
        }

        $allData = $qry->groupBy('products.prd_name')->orderby('cat_name', 'asc')->get()->toArray();
        $data = $qry->groupBy('products.prd_name')->orderby('cat_name', 'asc')->paginate($this->per_page)->toArray();

        if (empty($data['data'])) {
            return $data;
        }

        foreach ($data['data'] as $key => $val) {
            $result[$val['cat_name']][] = $val;
        }

        $i = 0;

        foreach ($result as $key => $val) {
            $cat['cat_name'] = $key;
            $cat['amount'] = array_sum(array_column($val, 'totalAmount'));
            $cat['qty'] = array_sum(array_column($val, 'totQty'));
            $cat['rate'] = round($cat['amount'] / $cat['qty'], 2);

            $res[$i]['category'][] = $cat;
            $res[$i]['items'] = $val;
            $i++;
        }
        $totalRes['total_products'] = count($allData);
        $totalRes['total_purchase_amount'] = array_sum(array_column($allData, 'totalAmount'));
        $totalRes['total_qty'] = array_sum(array_column($allData, 'totQty'));
        $totalRes['total_categories'] = count(array_unique(array_column($allData, 'cat_name')));

        $data['alldetais'] = $totalRes;
        $data['data'] = $res;

        if ($rqst['period_type'] != "") {
            $data['Date']['date1'] = date("d-F-Y", strtotime($date1));
            $data['Date']['date2'] = date("d-F-Y", strtotime($date2));
        } else {
            $data['Date'] = "";
        }
        return $data;

    }
    public function datesFromPeriods($pt, $rq)
    {
        switch ($pt) {

            case 't':
                $date1 = $date2 = date('Y-m-d');
                break;

            case 'ld':
                $date1 = $date2 = date('Y-m-d', strtotime("-1 days"));
                break;

            case 'lw':
                $previous_week = strtotime("-1 week +1 day");
                $start_week = strtotime("last sunday midnight", $previous_week);
                $end_week = strtotime("next saturday", $start_week);
                $date1 = date("Y-m-d", $start_week);
                $date2 = date("Y-m-d", $end_week);
                break;

            case 'lm':
                $date1 = date("Y-m-d", strtotime("first day of previous month"));
                $date2 = date("Y-m-d", strtotime("last day of previous month"));
                break;

            case 'ly':
                $year = date('Y') - 1;
                $date1 = $year . "-01-01";
                $date2 = $year . "-12-31";
                break;

            case 'c':
                $date1 = isset($rq['date1']) ? date('Y-m-d', strtotime($rq['date1'])) : 0;
                $date2 = isset($rq['date2']) ? date('Y-m-d', strtotime($rq['date2'])) : 0;

                break;

            default:
                return false;
                break;
        }
        return [$date1, $date2];
    }

}
