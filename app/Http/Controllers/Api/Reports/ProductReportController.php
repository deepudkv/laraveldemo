<?php

namespace App\Http\Controllers\Api\Reports;
use App\Models\Company\Acc_branch;
use App\Http\Controllers\Api\ApiParent;
use App\Models\Product\Category;
use App\Models\Product\Manufacturer;
use App\Models\Product\ProdFeatures;
use App\Models\Product\Product;
use App\Models\Stocks\BranchStocks;
use DB;
use Illuminate\Http\Request;

class ProductReportController extends ApiParent
{
    public function Report(Request $request, $type)
    {

        switch ($type) {
            case 'summary':
                return parent::displayData($this->productSummary($request));
                break;

            case 'stock_summary':
                return parent::displayData($this->productStockSummary($request));
                break;

            case 'category':
                return parent::displayData($this->category());
                break;

            case 'details':
                return parent::displayData($this->productDetails($request));
                break;

            case 'manufacture_details':
                return parent::displayData($this->manufactureDetails($request));
                break;

            case 'manufacture_summary':
                return parent::displayData($this->manufactureSummary($request));
                break;

            default:
                return parent::displayError('Invalid Endpoint', $this->badrequest_stat);
                break;
        }
    }
    public function productStockSummary($request)
    {

        $rltn = [

            'category' => function ($qr) {
                $qr->select('cat_id', 'cat_name');
            },

            'units' => function ($qr) {
                $qr->select('unit_id', 'unit_name')->where('unit_type', 1);
            },

        ];

        $qry = Product::select(
            'prd_id',
            'prd_barcode',
            'prd_ean',
            'prd_name',
            'prd_alias',
            'prd_code',
            'prd_cat_id',
            'prd_sub_cat_id',
            'cmp_stock_quantity',
            'unit_name',
            \DB::raw('(SELECT cat_name FROM erp_categories WHERE erp_products.prd_cat_id = erp_categories.cat_id ) as cat_name'),
            DB::raw('(SELECT subcat_name FROM erp_subcategories WHERE erp_products.prd_sub_cat_id = erp_subcategories.subcat_id ) as subcat_name')
        )  ->join('company_stocks', 'products.prd_id', 'company_stocks.cmp_prd_id')
        ->join('units', 'products.prd_base_unit_id', 'units.unit_id')
            ->orderBy('cat_name', 'ASC');

        if ($request['unitid'] != "") {
            $qry = $qry->where('prd_base_unit_id', $request['unitid']);
        }

        if ($request['prd_id'] != "") {
            $qry = $qry->where('prd_id', $request['prd_id']);
        }
        if (is_int($request['prd_stock_stat'])) {
            $qry = $qry->where('prd_stock_status', $request['prd_stock_stat']);
        }

        if ($request['cat_id'] != "") {
            $qry = $qry->where('prd_cat_id', $request['cat_id']);
        }

        if ($request['sub_cat_id'] != "") {
            $qry = $qry->where('prd_sub_cat_id', $request['sub_cat_id']);
        }
        if ($request['prd_ean'] != "") {
            $qry = $qry->where('prd_barcode', $request['prd_ean']);
        }

        if ($request['manufacture_id'] != "") {
            $qry = $qry->where('prd_supplier', $request['manufacture_id']);
        }

        if ($request['osq_type'] != "") {

            switch ($request['osq_type']) {
                case '<':
                    $qry = $qry->where('cmp_stock_quantity', '<', $request['osq_val1']);
                    break;
                case '<=':
                    $qry = $qry->where('cmp_stock_quantity', '<=', $request['osq_val1']);
                    break;
                case '=':
                    $qry = $qry->where('cmp_stock_quantity', '=', $request['osq_val1']);
                    break;
                case '>':
                    $qry = $qry->where('cmp_stock_quantity', '>', $request['osq_val1']);
                    break;
                case '>=':
                    $qry = $qry->where('cmp_stock_quantity', '>=', $request['osq_val1']);
                    break;
                case 'between':
                    $qry = $qry->where('cmp_stock_quantity', [$request['osq_val1'], $request['osq_val2']]);
                    break;
                default:
                $qry = $qry->where('cmp_stock_quantity', '>', 0);
                    break;
            }
        }
        else
        {
            $qry = $qry->where('cmp_stock_quantity', '>', 0);
        }

        $data = $qry->get()->toArray();
        $result['data'] = $qry->paginate(20);
        $result['total_products'] = count(array_column($data, 'prd_id'));
        $result['total_categories'] = count(array_unique(array_column($data, 'prd_cat_id')));

        if (isset($request['Selbranch'])) {
            $request['Selbranch'] = Acc_branch::whereIn('branch_id',$request['Selbranch'])->get()->toArray();
          
        }
        else
        {

            $request['Selbranch'] = Acc_branch::get()->toArray();
        
         
        }

        foreach ($result['data'] as $k1 => $val1) {

          
            foreach ($request['Selbranch'] as $k => $value) {
               // return  $val1['prd_id'];
                $whr['bs_prd_id'] = $val1['prd_id'];
                $whr['bs_branch_id'] = $value['branch_id'];
                $out= BranchStocks::where($whr)->first();
                $stock[$k]['stock']=   isset($out['bs_stock_quantity'])?$out['bs_stock_quantity']:0;
                $stock[$k]['branch_name']=  $value['branch_name'];
            }
            $result['data'][$k1]['branch_stocks'] = $stock;
        }

        return $result;

    }
    /**
     * @api {post} http://127.0.0.1:8000/api/product_report/summary  Product Summary
     * @apiName productSummary
     * @apiGroup Report
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {bigint} unitid Unit Id
     * @apiParam {bigint} cat_id Category Id
     * @apiParam {bigint} sub_cat_id Subcategory Id
     * @apiParam {bigint} manufacture_id Manufacture Id
     * @apiParam {int} feat_id Feature Id
     * @apiParam {string} feat_val Feature Value
     * @apiParam {string} vat_type Vat Filter Type (Between,<,>,<=,>=,=)
     * @apiParam {int} vat_val_1 Vat Filter Value 1
     * @apiParam {int} vat_val_2 Vat Filter Value 2
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} data Product Summary
     *
     *
     */

    public function productSummary($request)
    {

        $rltn = [

            'category' => function ($qr) {
                $qr->select('cat_id', 'cat_name');
            },
            'sub_category' => function ($qr) {
                $qr->select('subcat_id', 'subcat_name');
            },

            'features' => function ($qr) {
                $qr->select('feat_id', 'fetprod_prod_id', 'feat_name', 'fetprod_fet_val');
            },

            'units' => function ($qr) {
                $qr->select('unit_id', 'unit_name')->where('unit_type', 1);
            },

        ];

        $qry = Product::select(
            'prd_id',
            'prd_barcode',
            'prd_ean',
            'prd_name',
            'prd_alias',
            'prd_code',
            'prd_cat_id',
            'prd_sub_cat_id',
            \DB::raw('(SELECT cat_name FROM erp_categories WHERE erp_products.prd_cat_id = erp_categories.cat_id ) as cat_name'),

            DB::raw('(SELECT subcat_name FROM erp_subcategories WHERE erp_products.prd_sub_cat_id = erp_subcategories.subcat_id ) as subcat_name')
        )
            ->orderBy('cat_name', 'ASC')->with($rltn);

        if ($request['unitid'] != "") {
            $qry = $qry->where('prd_base_unit_id', $request['unitid']);
        }

        if ($request['prd_id'] != "") {
            $qry = $qry->where('prd_id', $request['prd_id']);
        }
        if (is_int($request['prd_stock_stat'])) {
            $qry = $qry->where('prd_stock_status', $request['prd_stock_stat']);
        }

        if ($request['cat_id'] != "") {
            $qry = $qry->where('prd_cat_id', $request['cat_id']);
        }

        if ($request['sub_cat_id'] != "") {
            $qry = $qry->where('prd_sub_cat_id', $request['sub_cat_id']);
        }
        if ($request['prd_ean'] != "") {
            $qry = $qry->where('prd_ean', $request['prd_ean']);
        }

        if ($request['manufacture_id'] != "") {
            $qry = $qry->where('prd_supplier', $request['manufacture_id']);
        }

        if ($request['feat_id'] != "") {

            $featPrd = ProdFeatures::select('fetprod_prod_id')->where('fetprod_fet_id', $request['feat_id']);

            if ($request['feat_val'] != "") {
                $featPrd->where('fetprod_fet_val', $request['feat_val']);
            }

            $featPrd->get()->pluck('fetprod_prod_id')->toArray();

            $qry->whereIn('prd_id', $featPrd);
        }

        if ($request['vat_type'] != "") {

            switch ($request['vat_type']) {
                case '<':
                    $qry = $qry->where('prd_tax', '<', $request['vat_val_1']);
                    break;
                case '<=':
                    $qry = $qry->where('prd_tax', '<=', $request['vat_val_1']);
                    break;
                case '=':
                    $qry = $qry->where('prd_tax', '=', $request['vat_val_1']);
                    break;
                case '>':
                    $qry = $qry->where('prd_tax', '>', $request['vat_val_1']);
                    break;
                case '>=':
                    $qry = $qry->where('prd_tax', '>=', $request['vat_val_1']);
                    break;
                case 'between':
                    $qry = $qry->whereBetween('prd_tax', [$request['vat_val_1'], $request['vat_val_2']]);
                    break;
                default:
                    return parent::displayError('Invalid Entry', $this->badrequest_stat);
                    break;
            }
        }

        $data = $qry->get()->toArray();
        $result['data'] = $qry->paginate(10);
        $result['total_products'] = count(array_column($data, 'prd_id'));
        $result['total_categories'] = count(array_unique(array_column($data, 'prd_cat_id')));

        return $result;

    }

    /**
     * @api {post} http://127.0.0.1:8000/api/product_report/manufacture_details  Manufcture Details
     * @apiName manufactureDetails
     * @apiGroup Report
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {int} mnfct_id Manufacture Id
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} Data Manufacture Details
     *
     *
     */

    public function manufactureDetails($request)
    {

        $res = 1;
        DB::beginTransaction();
        try {
            $rltn = [
                'product' => function ($qr) {
                    $qr->select('prd_name', 'prd_supplier', 'prd_code', 'prd_cat_id', \DB::raw('(SELECT cat_name FROM erp_categories WHERE erp_products.prd_cat_id = erp_categories.cat_id ) as category'))
                        ->orderBy('category', 'DESC');
                },

            ];

            $manuftrData = Manufacturer::with($rltn)->where('id', $request['mnfct_id'])->first();
            // return $manuftrData;

            if (count($manuftrData['product']) > 0) {
                foreach ($manuftrData['product'] as $key => $val) {
                    $items[$val['category']][] = $val;
                }

                $i = 0;
                foreach ($items as $key => $val) {
                    $category['name'] = $key;

                    $sort[$i]['category'][] = $category;
                    $sort[$i]['items'] = $val;

                    $i++;
                }

                unset($manuftrData['product']);
                $manuftrData['product'] = $sort;

                return $manuftrData;

            } else {
                $manuftrData = Manufacturer::where('id', $request['mnfct_id'])->first();
                return $manuftrData;
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return json_encode("Somthing went wrong, Please try again later", $this->badrequest_stat);
// throw $e;
            $res = 0;
        }

    }
    /**
     * @api {post} http://127.0.0.1:8000/api/product_report/manufacture_summary  Manufcture Summary
     * @apiName manufactureSummary
     * @apiGroup Report
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} Data Manufacture Summary
     *
     *
     */

    public function manufactureSummary()
    {

        $manuftrData = Manufacturer::select('id', 'manftr_comp_name', 'manftr_comp_code', 'manftr_comp_website')
            ->withCount('product')
            ->paginate(10);

        return $manuftrData;
    }

    // =========================== End of Code By Rakesh ================================

    public function category()
    {
        $rltn = ['sub_cat' => function ($qr) {
            $qr->select('subcat_id', 'subcat_parent_category', 'subcat_name')->where('subcat_flag', 1);
        }];
        return Category::select('cat_id', 'cat_name')->where('cat_flag', 1)->with($rltn)->get();
    }

    public function productDetails($rq)
    {

        $rltn = [
            'base_unit' => function ($qr) {
                $qr->select('unit_id', 'unit_name');
            },
            'category' => function ($qr) {
                $qr->select('cat_id', 'cat_name');
            },
            'sub_category' => function ($qr) {
                $qr->select('subcat_id', 'subcat_name');
            },
            'manufacturer' => function ($qr) {
                $qr->select('id', 'manftr_comp_name');
            },
            'units' => function ($qr) {
                $qr->select('unit_id', 'unit_name', 'unit_base_id', 'unit_base_qty', 'unit_code', 'unit_display');
            },
            'features' => function ($qr) {
                $qr->select('feat_id', 'feat_name', 'fetprod_fet_val');
            },
            'branch_stocks' => function ($qr) {
                if ($this->usr_type == 3) {
                    $qr->select(
                        'branch_id',
                        'branch_name',
                        'bs_stock_quantity',
                        'bs_stock_quantity_shop',
                        'bs_stock_quantity_gd',
                        'bs_prate',
                        'bs_avg_prate',
                        'bs_srate',
                        DB::raw('(bs_stock_quantity * bs_prate) as purchase_amount'),
                        DB::raw('(bs_stock_quantity * bs_avg_prate) as avg_purchase_amount')
                    )->where('bs_branch_id', $this->branch_id);
                } else {
                    $qr->select(
                        'branch_id',
                        'branch_name',
                        'bs_stock_quantity',
                        'bs_stock_quantity_shop',
                        'bs_stock_quantity_gd',
                        'bs_prate',
                        'bs_avg_prate',
                        'bs_srate',
                        DB::raw('(bs_stock_quantity * bs_prate) as purchase_amount'),
                        DB::raw('(bs_stock_quantity * bs_avg_prate) as avg_purchase_amount')
                    );
                }
            },
        ];

        $info = Product::where('prd_name', 'like', '%' . $rq->keyword . '%')->with($rltn)->take(70)->get();

        foreach ($info as $k => $row) {

            $info[$k]['base_unit_name'] = $row['base_unit']['unit_name'];
            $info[$k]['cat_name'] = $row['category']['cat_name'];
            $info[$k]['subcat_name'] = $row['sub_category']['subcat_name'];
            $info[$k]['manftr_name'] = $row['manufacturer']['manftr_comp_name'];
            unset($info[$k]['base_unit']);
            unset($info[$k]['category']);
            unset($info[$k]['sub_category']);
            unset($info[$k]['manufacturer']);
            $info[$k]['total_stocks'] = [];
            if ($this->usr_type < 3) {
                $info[$k]['total_stocks'] = BranchStocks::where('bs_prd_id', $info[0]['prd_id'])->where('bs_branch_id', 0)->take(1)->get()->toArray();
            }
        }
        return $info;
    }
}
