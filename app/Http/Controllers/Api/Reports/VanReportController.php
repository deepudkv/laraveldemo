<?php

namespace App\Http\Controllers\Api\Reports;

use App\Http\Controllers\Api\ApiParent;
use App\Models\Product\Category;
use App\Models\Product\Product;
use App\Models\Van\Van;
use App\Models\Van\VanDailyStocks;
use App\Models\Van\VanStock;
use App\Models\Van\VanTransfer;
use App\Models\Van\VanTransferReturn;
use App\Models\Van\VanTransferReturnSub;
use App\Models\Van\VanTransferSub;
use DB;
use Illuminate\Http\Request;

class VanReportcontroller extends ApiParent
{

    public function vanItemReport(Request $request)
    {

        if ($request['rep_type'] == 1) { // TYPE 1 IS FOR TRANSFER & RETURN

            $vanSelqty = DB::raw('sum(vantransub_qty) as trans_stock_qty');
            $avg = DB::raw('(sum(vantransub_purch_rate * vantransub_qty)/ sum(vantransub_qty) )as trans_avg_rate');
            $amt = DB::raw('(sum(vantransub_purch_rate * vantransub_qty)) as trans_amount');

        } elseif ($request['rep_type'] == 3) { // TYPE 3 IS FOR RETURN
            $vanSelqty = DB::raw('sum(vantranretsub_qty) as stock_qty');
            $avg = DB::raw('AVG(vantranretsub_rate) as avg_rate');
            $amt = DB::raw('(sum(vantranretsub_rate * vantranretsub_qty)) as amount');

        } else { // TYPE  FOR TRANSFER ONLY
            $vanSelqty = DB::raw('sum(vantransub_qty) as stock_qty');
            $avg = DB::raw('AVG(vantransub_purch_rate) as avg_rate');
            $amt = DB::raw('(sum(vantransub_purch_rate * vantransub_qty)) as amount');
        }

        if ($request['rep_type'] == 1) {
            $slct = ['products.prd_name', 'categories.cat_name', 'vantransub_van_id as vanid', $vanSelqty, $avg, $amt,
                'b.ret_stock_qty', 'b.ret_avg_rate', 'b.retamount'];

        } else {
            $slct = ['products.prd_name', 'categories.cat_name', $vanSelqty, $avg, $amt];
        }

        if ($request['rep_type'] == 3) {
            $qry = VanTransferReturnSub::select($slct);

        } else {
            $qry = VanTransferSub::select($slct);

        }

        if ($request['rep_type'] == 3) {

            $qry->join('products', 'van_transfer_return_sub.vantranretsub_prod_id', 'products.prd_id')
                ->join('van', 'van_transfer_return_sub.vantranretsub_van_id', 'van.van_id')
                ->join('categories', 'categories.cat_id', 'products.prd_cat_id')
                ->join('van_stock', function ($join) {
                    $join->on('van_stock.vanstock_branch_stock_id', 'van_transfer_return_sub.vantransub_branch_stock_id');
                    $join->on('van_stock.vanstock_van_id', 'van_transfer_return_sub.vantranretsub_van_id');
                });

        } else {
            $qry->join('products', 'van_transfer_sub.vantransub_prod_id', 'products.prd_id')
                ->join('van', 'van_transfer_sub.vantransub_van_id', 'van.van_id')
                ->join('categories', 'categories.cat_id', 'products.prd_cat_id')
                ->join('van_transfer', 'van_transfer.vantran_id', 'van_transfer_sub.vantransub_vantran_id')
                ->join('van_stock', function ($join) {
                    $join->on('van_stock.vanstock_branch_stock_id', 'van_transfer_sub.vantransub_branch_stock_id');
                    $join->on('van_stock.vanstock_van_id', 'van_transfer_sub.vantransub_van_id');
                });
        }

        if ($request['rep_type'] == 1) {

            $trans_return = VanTransferReturnSub::select(DB::raw("sum(vantranretsub_qty) AS 'ret_stock_qty',AVG(vantranretsub_rate) as ret_avg_rate,(sum(vantranretsub_rate * vantranretsub_qty)) as retamount,vantranretsub_prod_id,vantranretsub_flags"));
            if ($request['van_id'] != "") {
                $trans_return = $trans_return->where('vantranretsub_van_id', $request['van_id']);
            }

            if ($request['usersel'] != "") {
                $trans_return = $trans_return->where('vantranretsub_added_by', $request['usersel']);
            }

            $trans_return->where('vantranretsub_flags', 1)
                ->groupBy('vantranretsub_prod_id');
            $qry->leftjoinSub($trans_return, 'b', function ($join) {
                $join->on('van_transfer_sub.vantransub_prod_id', "=", 'b.vantranretsub_prod_id');
            });
        }

        if ($request['rep_type'] == 3) {

            if ($request['van_id'] != "") {
                $qry = $qry->where('vantranretsub_van_id', $request['van_id']);
            }
            if ($request['usersel'] != "") {
                $qry = $qry->where('vantranretsub_added_by', $request['usersel']);
            }

            $qry->where('van_transfer_return_sub.vantranretsub_flags', 1);

            if ($this->branch_id > 0) {
                $qry->where('van_transfer_return_sub.branch_id', $this->branch_id);
            }

        } else {

            $qry->where('van_transfer_sub.vantransub_flags', 1);

            if ($this->branch_id > 0) {
                $qry->where('van_transfer_sub.branch_id', $this->branch_id);
            }

        }

// FILTERS

        if ($request['prd_id'] != "") {
            $qry = $qry->where('products.prd_id', $request['prd_id']);
        }

        if ($request['prd_cat_id'] != "") {
            $qry = $qry->where('categories.cat_id', $request['prd_cat_id']);
        }

        if ($request['manfact_id'] != "") {
            $qry = $qry->where('products.prd_supplier', $request['manfact_id']);
        }

        if ($request['rep_type'] != 3) {
            if ($request['van_id'] != "") {
                $qry = $qry->where('vantransub_van_id', $request['van_id']);
            }

            if ($request['usersel'] != "") {
                $qry = $qry->where('vantransub_added_by', $request['usersel']);
            }
        }

        if ($ptype = isset($request['period_type']) ? $request['period_type'] : 0) {

            list($date1, $date2) = $this->datesFromPeriods($ptype, $request);

            if ($date1 && $date2) {
                if ($request['rep_type'] == 3) {
                    $qry = $qry->whereBetween('vantranretsub_date', [$date1, $date2]);
                } else {
                    $qry = $qry->whereBetween('vantransub_date', [$date1, $date2]);

                }
            }

        }

        // FILTERS END

        $allData = $qry->groupBy('products.prd_name')->orderby('cat_name', 'asc')->get()->toArray();
        $data = $qry->groupBy('products.prd_name')->orderby('cat_name', 'asc')->paginate($this->per_page)->toArray();

        if (count($data['data']) > 0) {

            if ($request['rep_type'] == 2) {
                $data['reportType'] = 'Transfer Report';
            } elseif ($request['rep_type'] == 3) {
                $data['reportType'] = 'Returns Report';
            } elseif ($request['rep_type'] == 1) {
                $data['reportType'] = 'Transfer & Returns Report';
            }

            if ($request['van_id'] != "") {
                $van_name = Van::select('van_name')->where('van_id', $request['van_id'])->get()->pluck('van_name');
                $data['reportBy'] = $van_name[0];
            }

// Loop For getting Diffrence of transfer and return report

            if ($request['rep_type'] == 1) {
                foreach ($data['data'] as $key => $val) {
                    // print_r($val['trans_stock_qty']);exit;
                    if ($val['trans_stock_qty'] == 0) {
                        $val['trans_stock_qty'] = 1;
                    }
                    if (empty($val['ret_stock_qty'])) {
                        $val['ret_stock_qty'] = 1;

                    }
                    $allData[$key]['trans_avg_rate'] = $val['trans_amount'] / $val['trans_stock_qty'];
                    $data['data'][$key]['trans_avg_rate'] = $val['trans_amount'] / $val['trans_stock_qty'];

                    $allData[$key]['ret_avg_rate'] = $val['retamount'] / $val['ret_stock_qty'];
                    $data['data'][$key]['ret_avg_rate'] = $val['retamount'] / $val['ret_stock_qty'];

                }
            } else {
                foreach ($data['data'] as $key => $val) {
                    $data['data'][$key]['avg_rate'] = $val['amount'] / $val['stock_qty'];
                }
            }
// return $data;
            if ($request['rep_type'] == 1) {
                foreach ($allData as $key => $val) {
                    $allData[$key]['stock_qty'] = $val['trans_stock_qty'] - $val['ret_stock_qty'];
                    $allData[$key]['avg_rate'] = $val['trans_avg_rate'] - $val['ret_avg_rate'];
                    $allData[$key]['amount'] = $val['trans_amount'] - $val['retamount'];

                }
                if ($request['rep_type'] == 1) {
                    foreach ($data['data'] as $key => $val) {
                        $data['data'][$key]['stock_qty'] = $val['trans_stock_qty'] - $val['ret_stock_qty'];
                        $data['data'][$key]['amount'] = $val['trans_amount'] - $val['retamount'];

                        $amt = $data['data'][$key]['amount'];
                        $qty = $data['data'][$key]['stock_qty'];
                        if ($qty == 0 || $qty == "") {
                            $qty = 1;
                        }
                        $data['data'][$key]['avg_rate'] = $amt / $qty;

                    }
                }

            }
// END

            foreach ($data['data'] as $key => $val) {
                $result[$val['cat_name']][] = $val;
            }

            $i = 0;

            foreach ($result as $key => $val) {
                $cat['cat_name'] = $key;
                $cat['amount'] = array_sum(array_column($val, 'amount'));
                $cat['qty'] = array_sum(array_column($val, 'stock_qty'));
                $cat['rate'] = array_sum(array_column($val, 'avg_rate'));

                $res[$i]['category'][] = $cat;
                $res[$i]['items'] = $val;
                $i++;
            }
            $totalRes['total_products'] = count($allData);
            $totalRes['total_purchase_amount'] = array_sum(array_column($allData, 'amount'));
            $totalRes['total_qty'] = array_sum(array_column($allData, 'stock_qty'));
            $totalRes['total_categories'] = count(array_unique(array_column($allData, 'cat_name')));

            $data['alldetais'] = $totalRes;
            $data['data'] = $res;

            if ($request['period_type'] != "") {
                $data['Date']['date1'] = date("d-F-Y", strtotime($date1));
                $data['Date']['date2'] = date("d-F-Y", strtotime($date2));
            } else {
                $data['Date'] = "";
            }

            return $data;

        } else {
            if ($request['period_type'] != "") {
                $data['Date']['date1'] = date("d-F-Y", strtotime($date1));
                $data['Date']['date2'] = date("d-F-Y", strtotime($date2));
            } else {
                $data['Date'] = "";
            }

            $res = [];
            $data['data'] = $res;

            return $data;
        }

    }

    public function invoiceBasedReport(Request $request)
    {
        if ($request['van_id'] != "") {

            if ($request['rep_type'] == 1) {

                $vanSelqty = DB::raw('(SELECT sum(vantransub_qty) FROM erp_van_transfer_sub WHERE erp_van_transfer_sub.vantransub_prod_id = erp_products.prd_id and erp_van_transfer_sub.vantransub_van_id = erp_van.van_id) as trans_stock_qty');
                $vanSeltransRetqty = DB::raw('(SELECT sum(vantranretsub_qty) FROM erp_van_transfer_return_sub WHERE erp_van_transfer_return_sub.vantranretsub_prod_id = erp_products.prd_id and erp_van_transfer_sub.vantransub_van_id = erp_van.van_id) as ret_stock_qty');

            } elseif ($request['rep_type'] == 3) {

                $vanSelqty = DB::raw('(SELECT sum(vantranretsub_qty) FROM erp_van_transfer_return_sub WHERE erp_van_transfer_return_sub.vantranretsub_prod_id = erp_products.prd_id) as stock_qty');

                // $vanSelqty = DB::raw('(SELECT sum(vantranretsub_qty) FROM erp_van_transfer_sub WHERE erp_van_transfer_sub.vantransub_prod_id = erp_products.prd_id and erp_van_transfer_sub.vantransub_branch_stock_id = erp_van_transfer_return_sub.vantransub_branch_stock_id and erp_van_transfer_sub.vantransub_unit_id = erp_van_transfer_return_sub.vantranretsub_unit_id and erp_van_transfer_sub.vantransub_van_id = erp_van.van_id) as stock_qty');
            } else {

                $vanSelqty = DB::raw('(SELECT sum(vantranretsub_qty) FROM erp_van_transfer_return_sub WHERE erp_van_transfer_return_sub.vantranretsub_prod_id = erp_products.prd_id and erp_van_transfer_return_sub.vantranretsub_van_id = erp_van.van_id) as stock_qty');

            }
        } else {

            if ($request['rep_type'] == 1) {
                $vanSelqty = DB::raw('(SELECT sum(vantransub_qty) FROM erp_van_transfer_sub WHERE erp_van_transfer_sub.vantransub_prod_id = erp_products.prd_id) as trans_stock_qty');
                $vanSeltransRetqty = DB::raw('(SELECT sum(vantranretsub_qty) FROM erp_van_transfer_return_sub WHERE erp_van_transfer_return_sub.vantranretsub_prod_id = erp_products.prd_id) as ret_stock_qty');

            } elseif ($request['rep_type'] == 3) {
                $vanSelqty = DB::raw('(SELECT sum(vantranretsub_qty) FROM erp_van_transfer_return_sub WHERE erp_van_transfer_return_sub.vantranretsub_prod_id = erp_products.prd_id and erp_van_transfer_return_sub.vantranretsub_flags = 1) as stock_qty');
            } else {
                $vanSelqty = DB::raw('(SELECT sum(vantranretsub_qty) FROM erp_van_transfer_return_sub WHERE erp_van_transfer_return_sub.vantranretsub_prod_id = erp_products.prd_id  and erp_van_transfer_sub.vantransub_flags = 1) as stock_qty');
            }
        }

        $van_tran_filter = isset($request['van_tran_filter']) ? $request['van_tran_filter'] : 0;
        $trans_val1 = isset($request['trans_val1']) ? $request['trans_val1'] : 0;
        $trans_val2 = isset($request['trans_val2']) ? $request['trans_val2'] : 0;

        // $amnt = DB::raw('(vantransub_purch_rate * vantransub_qty) as purchase_amount');

        if ($request['rep_type'] == 3) {
            $slct = ['van_transfer_return_sub.vantranretsub_vantran_id', 'vantranretsub_van_id as vantransub_van_id', 'vantranretsub_purch_rate',
                'vantranretsub_rate as vantransub_purch_rate', 'van_transfer_return_sub.vantranretsub_qty as vantransub_qty', 'vantranretsub_prod_id as vantransub_prod_id', 'van_transfer_return_sub.vantransub_branch_stock_id',
                'vantranretsub_date as vantransub_date', 'products.prd_name', 'products.prd_barcode', 'categories.cat_name', $vanSelqty];
        } else {
            $slct = ['van_transfer_sub.vantransub_vantran_id', 'vantransub_van_id', 'vantransub_godown_id', 'vantransub_rate',
                'vantransub_purch_rate', 'vantransub_qty', 'vantransub_prod_id', 'vantransub_stock_id', 'van_transfer_sub.vantransub_branch_stock_id',
                'van_transfer_sub.vantransub_unit_id', 'vantransub_date', 'products.prd_name', 'products.prd_barcode', 'categories.cat_name', $vanSelqty];
        }

        // if ($request['rep_type'] == 1) {
        //     $slct = array_push($slct,$vanSelqty, $vanSeltransRetqty);
        // }else{
        //     $slct = array_push($slct,$vanSelqty);
        // }

        if ($request['rep_type'] == 3) {

            $qry = VanTransferReturnSub::select($slct);

        } else {

            $qry = VanTransferSub::select($slct);

        }
// $qry->join('products', 'van_transfer_sub.vantransub_prod_id', 'products.prd_id')
        //     ->join('van', 'van_transfer_sub.vantransub_van_id', 'van.van_id')
        //     ->join('categories', 'categories.cat_id', 'products.prd_cat_id')
        //     ->join('van_transfer', 'van_transfer.vantran_id', 'van_transfer_sub.vantransub_vantran_id');

        if ($request['rep_type'] == 1) {
            $qry->leftjoin('van_transfer_return_sub', function ($join) {
                $join->on('van_transfer_return_sub.vantransub_branch_stock_id', '=', 'van_transfer_sub.vantransub_branch_stock_id');
                $join->on('van_transfer_sub.vantransub_unit_id', '=', 'van_transfer_return_sub.vantranretsub_unit_id');
                $join->where('van_transfer_return_sub.vantranretsub_flags', 1);

            });
        }
        if ($request['rep_type'] == 3) {

            $qry->join('products', 'van_transfer_return_sub.vantranretsub_prod_id', 'products.prd_id')
                ->join('van', 'van_transfer_return_sub.vantranretsub_van_id', 'van.van_id')
                ->join('categories', 'categories.cat_id', 'products.prd_cat_id');
            // ->join('van_transfer', 'van_transfer.vantran_id', 'van_transfer_return_sub.vantranretsub_vantran_id');

            // $qry->rightjoin('van_transfer_return_sub', function ($join) {
            //     $join->on('van_transfer_return_sub.vantransub_branch_stock_id', '=', 'van_transfer_sub.vantransub_branch_stock_id');
            //     $join->on('van_transfer_sub.vantransub_unit_id', '=', 'van_transfer_return_sub.vantranretsub_unit_id');
            //     $join->on('van_transfer_sub.vantransub_van_id', '=', 'van_transfer_return_sub.vantranretsub_van_id');

            //     $join->where('van_transfer_return_sub.vantranretsub_flags',1);

            // });
        } else {
            $qry->join('products', 'van_transfer_sub.vantransub_prod_id', 'products.prd_id')
                ->join('van', 'van_transfer_sub.vantransub_van_id', 'van.van_id')
                ->join('categories', 'categories.cat_id', 'products.prd_cat_id')
                ->join('van_transfer', 'van_transfer.vantran_id', 'van_transfer_sub.vantransub_vantran_id');

        }
        if ($request['rep_type'] == 3) {
            $qry->where('van_transfer_return_sub.vantranretsub_flags', 1);

            if ($this->branch_id > 0) {
                $qry->where('van_transfer_return_sub.branch_id', $this->branch_id);
            }

        } else {
            $qry->where('van_transfer.vantran_flags', 1);

            if ($this->branch_id > 0) {
                $qry->where('van_transfer_sub.branch_id', $this->branch_id);
            }

        }

        if ($ptype = isset($request['period_type']) ? $request['period_type'] : 0) {

            list($date1, $date2) = $this->datesFromPeriods($ptype, $request);

            if ($date1 && $date2) {

                //$qry = $qry->whereBetween('vantransub_date', [$date1, $date2]);
                if ($request['rep_type'] == 3) {
                    $qry = $qry->whereBetween('vantranretsub_date', [$date1, $date2]);
                } else {
                    $qry = $qry->whereBetween('vantransub_date', [$date1, $date2]);

                }
            }
            // return "dsf";
        }

        if ($van_tran_filter && $trans_val1) {
            switch ($van_tran_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $qry = $qry->where('vantransub_vantran_id', $van_tran_filter, $trans_val1);
                    break;
                case 'between':
                    if ($trans_val1) {
                        $qry = $qry->whereBetween('vantransub_vantran_id', [$trans_val1, $trans_val2]);
                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }

        if ($request['prd_id'] != "") {
            $qry = $qry->where('products.prd_id', $request['prd_id']);
        }

        if ($request['cat_id'] != "" || $request['manfact_id'] != "") {

            $prdctQr = Product::select('prd_id');
            if ($request['cat_id'] != '') {
                $prdctQr = $prdctQr->where('prd_cat_id', $request['cat_id']);
            }
            if ($request['manfact_id'] != '') {
                $prdctQr = $prdctQr->where('prd_supplier', $request['manfact_id']);
            }
            $prdIds = $prdctQr->get()->pluck('prd_id')->toArray();
            if (empty($prdIds)) {
                return [];
            } else {
                $qry = $qry->whereIn('vantransub_prod_id', $prdIds);
            }

        }

        if ($request['van_id'] != "") {

            if ($request['rep_type'] == 3) {
                $qry = $qry->where('vantranretsub_van_id', $request['van_id']);
            } else {
                $qry = $qry->where('vantransub_van_id', $request['van_id']);

            }
        }

        $allData = $qry->orderby('cat_name', 'asc')->get()->toArray();
        $data = $qry->orderby('cat_name', 'asc')->paginate($this->per_page)->toArray();
        if (empty($data['data'])) {
            return $data;
        }

        // return $data;

        // if($request['rep_type'] == 1){
        //     foreach ($allData as $key => $val) {
        //         $allData[$key]['vantranretsub_qty'] = $val['trans_stock_qty'] - $val['ret_stock_qty'] ;
        //             }
        //         }

        foreach ($allData as $key => $val) {

            $allData[$key]['purchase_amount'] = $allData[$key]['vantransub_qty'] * $val['vantransub_purch_rate'];
        }

        if ($request['rep_type'] == 1) {
            foreach ($data['data'] as $key => $val) {
                $data['data'][$key]['vantransub_qty'] = $val['trans_stock_qty'] - $val['ret_stock_qty'];
            }
        }

        foreach ($data['data'] as $key => $val) {

            $data['data'][$key]['purchase_amount'] = $data['data'][$key]['vantransub_qty'] * $val['vantransub_purch_rate'];
        }
        // return $data;

        foreach ($data['data'] as $key => $val) {
            $result[$val['cat_name']][] = $val;
        }
        $cat['amount'] = array_sum(array_column($data['data'], 'purchase_amount'));
        $cat['qty'] = array_sum(array_column($data['data'], 'vantransub_qty'));

        $cat['rate'] = ($cat['qty'] > 0 ? round(($cat['amount'] / $cat['qty']), 2) : 0);

        foreach ($result as $key => $val) {
            $cat['name'] = $key;
            $item[] = $val;
        }

        foreach ($item as $key => $prval) {
            foreach ($prval as $key => $pval) {
                $resu[$pval['prd_name']][] = $pval;
            }
        }

        $i = 0;
        foreach ($resu as $key => $val) {
            $prd[$i]['prod']['name'] = $key;
            $prd[$i]['items'] = $val;
            $i++;
        }

        $res['category'][] = $cat;
        $res['products'] = $prd;

        unset($data['data']);
        $data['data'][] = $res;

        $totalRes['total_products'] = count($allData);
        $totalRes['total_purchase_amount'] = array_sum(array_column($allData, 'purchase_amount'));
        $totalRes['total_qty'] = array_sum(array_column($allData, 'vantransub_qty'));
        $totalRes['total_categories'] = count(array_unique(array_column($allData, 'cat_name')));

        $data['alldetais'] = $totalRes;

        if ($request['rep_type'] == 2) {
            $data['reportType'] = 'Transfer Report';
        } elseif ($request['rep_type'] == 3) {
            $data['reportType'] = 'Returns Report';
        }
        if ($request['van_id'] != "") {
            $van_name = Van::select('van_name')->where('van_id', $request['van_id'])->get()->pluck('van_name');
            $data['reportBy'] = $van_name[0];
        }

        if ($request['period_type'] != "") {
            $data['Date']['date1'] = date("d-F-Y", strtotime($date1));
            $data['Date']['date2'] = date("d-F-Y", strtotime($date2));
        } else {
            $data['Date'] = "";
        }

        return $data;

    }

    public function getproductList($array)
    {
        $k = 0;
        foreach ($array as $key => $iarray) {

            $pd['name'] = $iarray[0]['prd_name'];
            $pd['amount'] = array_sum(array_column($iarray, 'purchase_amount'));
            $pd['qty'] = array_sum(array_column($iarray, 'vantransub_qty'));
            $pd['rate'] = round($pd['amount'] / $pd['qty']);
            $return[$k]['product'][] = $pd;
            $return[$k]['items'] = $iarray;
            $k++;
        }
        return $return;
    }
    public function vanTransferReport(Request $request)
    {

        $amnt = DB::raw('(purchsub_rate * purchsub_qty) as purchase_amount');
        $this->inrSlct = ['purchsub_purch_id', 'purchsub_prd_id', 'purchsub_qty', 'purchsub_rate', 'purchsub_frieght',
            'purchsub_tax', 'purchsub_tax_per', 'purchsub_unit_id', 'purchsub_gd_qty', 'purchsub_gd_id', $amnt];
        $rltn = [
            'items' => function ($qr) {
                $qr->select('*');
            },
            'van' => function ($qr) {
                $qr->select('van_id', 'van_name');
            },
            'gd' => function ($qr) {
                $qr->select('gd_id', 'gd_name');
            },
            'branch' => function ($qr) {
                $qr->select('branch_id', 'branch_name', 'branch_display_name', 'branch_code', 'branch_address', 'branch_phone', 'branch_mob', 'img_url', 'branch_tin', 'branch_reg_no');
            },
        ];

        if ($request->status) {
            $qry = VanTransfer::select('*')->where('branch_id', $this->branch_id)->where('vantran_flags', $request->status)->with($rltn);
        } else {
            $qry = VanTransfer::select('*')->where('branch_id', $this->branch_id)->where('vantran_flags', 1)->with($rltn);
        }

        $data = $qry->orderBy('vantran_id', 'DESC')->paginate($this->per_page)->toArray();

        foreach ($data['data'] as $t => $transfer) {

            if (empty($transfer['gd'])) {
                $data['data'][$t]['gd']['0']['gd_id'] = 0;
                $data['data'][$t]['gd']['0']['gd_name'] = 'Shop';
            }
            foreach ($transfer['items'] as $k => $item) {
                //
                $dd = Product::select('prd_name', 'prd_barcode')
                    ->where('prd_id', $item['vantransub_prod_id'])->first();
                $data['data'][$t]['items'][$k]['prd_name'] = $dd['prd_name'];
                $data['data'][$t]['items'][$k]['prd_barcode'] = $dd['prd_barcode'];

            }

        }
        return $data;
    }
    public function vanTransferReport1(Request $request)
    {

        // $company_branch = trim($rq->branch ? $rq->branch : 0);
        // $this->branch_id = trim(($this->usr_type == 2) ? $company_branch : $this->branch_id);

        $inv_filter = isset($rq['inv_filter']) ? $rq['inv_filter'] : 0;
        $inv_val = isset($rq['inv_val']) ? $rq['inv_val'] : 0;
        $inv_val2 = isset($rq['inv_val2']) ? $rq['inv_val2'] : 0;

        $price_filter = isset($rq['purch_price']) ? $rq['purch_price'] : 0;
        $price1 = isset($rq['price1']) ? $rq['price1'] : 0;
        $price2 = isset($rq['price2']) ? $rq['price2'] : 0;

        $dsnt_filter = isset($rq['purch_discount']) ? $rq['purch_discount'] : 0;
        $discount1 = isset($rq['discount1']) ? $rq['discount1'] : 0;
        $discount2 = isset($rq['discount2']) ? $rq['discount2'] : 0;

        if (isset($rq['payment_type']) && $rq['payment_type']) {
            $whr['purch_pay_type'] = $rq['payment_type'];
        }
        if (isset($rq['purch_supp_id']) && $rq['purch_supp_id']) {
            $whr['purch_supp_id'] = $rq['purch_supp_id'];
        }
        $whr['purch_flag'] = 1;
        if (isset($rq['purch_type']) && $rq['purch_type'] == 2) {

            $whr['purch_flag'] = 0;
        }

        $slct = ['purch_id', 'purch_date', 'purch_inv_no', 'purch_id2', 'purch_no', 'purch_supp_id', 'purch_tax_ledger_id', 'purch_ord_no', 'purch_type', 'purch_pay_type',
            'purch_date', 'purch_inv_date', 'purch_amount', 'purch_tax', 'purch_discount', 'purch_frieght'];

        $amnt = DB::raw('(purchsub_rate * purchsub_qty) as purchase_amount');
        $this->inrSlct = ['purchsub_purch_id', 'purchsub_prd_id', 'purchsub_qty', 'purchsub_rate', 'purchsub_frieght',
            'purchsub_tax', 'purchsub_tax_per', 'purchsub_unit_id', 'purchsub_gd_qty', 'purchsub_gd_id', $amnt];
        $rltn = [
            'items' => function ($qr) {
                $qr->select($this->inrSlct);
            },
            'supplier' => function ($qr) {
                $qr->select('supp_id', 'supp_name');
            },
        ];
        $qry = purchase::select($slct)->with($rltn);

        if ($ptype = isset($rq['period_type']) ? $rq['period_type'] : 0) {
            list($date1, $date2) = $this->datesFromPeriods($ptype, $rq);

            if ($date1 && $date2) {
                $qry = $qry->whereBetween('purch_date', [$date1, $date2]);
            }
        }

        $qry = isset($whr) ? $qry->where($whr) : $qry;

        if ($inv_filter && $inv_val) {
            switch ($inv_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $qry = $qry->where('purch_id', $inv_filter, $inv_val);
                    break;
                case 'between':
                    if ($inv_val) {
                        $qry = $qry->whereBetween('purch_id', [$inv_val, $inv_val2]);
                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }

        if ($price_filter && $price1) {
            switch ($price_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $qry = $qry->where('purch_amount', $price_filter, $price1);
                    break;
                case 'between':
                    if ($price2) {
                        $qry = $qry->whereBetween('purch_amount', [$price1, $price2]);
                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }

        if ($dsnt_filter && $discount1) {
            switch ($dsnt_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $qry = $qry->where('purch_discount', $dsnt_filter, $discount1);
                    break;
                case 'between':
                    if ($discount2) {
                        $qry = $qry->whereBetween('purch_discount', [$discount1, $discount2]);
                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }
        $sum = $qry->with($rltn)->get()->toArray();
        foreach ($sum as $k => $val) {
            $sum[$k]['purch_tax'] = round($val['purch_tax'], 2);
            $sum[$k]['purch_amount'] = round($val['purch_amount'], 2);
            $sum[$k]['purch_discount'] = round($val['purch_discount'], 2);
        }

        $data = $qry->orderBy('purch_date', 'ASC')->paginate($this->per_page)->toArray();

        $dates = $supps = [];
        foreach ($data['data'] as $k => $array) {
            $data['data'][$k]['sl_num'] = $k + 1;
            //print_r($array);die
            $data['data'][$k]['supp_name'] = $array['supplier']['supp_name'];
            $data['data'][$k]['purch_type_name'] = ($array['purch_pay_type'] == 1) ? 'Credit' : 'Cash';
            $data['data'][$k]['purch_tax'] = number_format((float) $array['purch_tax'], 2, '.', '');
            $data['data'][$k]['purch_amount'] = number_format((float) $array['purch_amount'], 2, '.', '');
            $data['data'][$k]['purch_discount'] = number_format((float) $array['purch_discount'], 2, '.', '');
            $dates[$array['purch_date']][] = $k;
            $supps[$array['purch_supp_id']][] = $k;
            foreach ($array['items'] as $k2 => $val) {
                // print_r($val);die();

                $data['data'][$k]['items'][$k2]['purchsub_tax'] = number_format((float) $val['purchsub_tax'], 2, '.', '');
                $data['data'][$k]['items'][$k2]['purchase_amount'] = number_format((float) $val['purchase_amount'], 2, '.', '');

            }
        }

        $data['total_purchase'] = count($sum);
        $amnt = array_sum(array_column($sum, 'purch_amount'));
        $data['total_purchase_amount'] = number_format((float) $amnt, 2, '.', '');
        $data['total_tax_amount'] = round(array_sum(array_column($sum, 'purch_tax')), 2);
        $data['amount_exclude_tax'] = $data['total_purchase_amount'] - $data['total_tax_amount'];
        $sum_cash = $qry->with($rltn)->where('purch_pay_type', 2)->get()->toArray();

        $purchase_amount_cash = round(array_sum(array_column($sum_cash, 'purch_amount')), 2);
        $data['purchase_amount_cash'] = number_format((float) $purchase_amount_cash, 2, '.', '');

        $credit_sum = $data['total_purchase_amount'] - $purchase_amount_cash;
        $data['purchase_amount_credit'] = number_format((float) $credit_sum, 2, '.', '');

        $total_days = count($dates);
        $total_supplier = count($supps);

        $data['total_days'] = isset($total_days) ? $total_days : 0;
        $data['total_supplier'] = isset($total_supplier) ? $total_supplier : 0;
        $array = $out = [];
        foreach ($data['data'] as $k => $v) {
            $out[$v['purch_date']][] = $v;
        }
        $j = 0;

        foreach ($out as $k => $v) {

            $list['date'] = $k;

            $list['tot_amount'] = array_sum(array_column($v, 'purch_amount'));
            $list['tot_tax'] = array_sum(array_column($v, 'purch_tax'));
            $list['list'] = $v;

            $array[] = $list;

            $j++;
        }
        //return $array;
        unset($data['data']);
        $data['data'] = $array;

        return $data;
    }
    public function datesFromPeriods($pt, $rq)
    {
        switch ($pt) {

            case 't':
                $date1 = $date2 = date('Y-m-d');
                break;

            case 'ld':
                $date1 = $date2 = date('Y-m-d', strtotime("-1 days"));
                break;

            case 'lw':
                $previous_week = strtotime("-1 week +1 day");
                $start_week = strtotime("last sunday midnight", $previous_week);
                $end_week = strtotime("next saturday", $start_week);
                $date1 = date("Y-m-d", $start_week);
                $date2 = date("Y-m-d", $end_week);
                break;

            case 'lm':
                $date1 = date("Y-m-d", strtotime("first day of previous month"));
                $date2 = date("Y-m-d", strtotime("last day of previous month"));
                break;

            case 'ly':
                $year = date('Y') - 1;
                $date1 = $year . "-01-01";
                $date2 = $year . "-12-31";
                break;

            case 'c':
                $date1 = isset($rq['date1']) ? date('Y-m-d', strtotime($rq['date1'])) : 0;
                $date2 = isset($rq['date2']) ? date('Y-m-d', strtotime($rq['date2'])) : 0;

                break;

            default:
                return false;
                break;
        }
        return [$date1, $date2];
    }
    public function reportByPurchase($rq)
    {

        $amnt = DB::raw('(purchsub_rate * purchsub_qty) as purchase_amount');
        $slct = ['purchsub_purch_id as purch_refno', 'purchsub_date', 'purchsub_prd_id', 'purchsub_qty', 'purchsub_rate', $amnt];
        $rltn = [
            'product' => function ($qr) {
                $qr->select('prd_id', 'prd_name', 'prd_cat_id', 'prd_barcode');
            },
        ];
        $qry = PurchaseSub::select($slct);

        if ($ptype = isset($rq['period_type']) ? $rq['period_type'] : 0) {
            list($date1, $date2) = $this->datesFromPeriods($ptype, $rq);

            if ($date1 && $date2) {
                $qry = $qry->whereBetween('purchsub_date', [$date1, $date2]);
            }
        }

        $purch_filter = isset($rq['purch_filter']) ? $rq['purch_filter'] : 0;
        $purch_val = isset($rq['purch_val']) ? $rq['purch_val'] : 0;
        $purch_val2 = isset($rq['purch_val2']) ? $rq['purch_val2'] : 0;
        if ($purch_filter && $purch_val) {
            switch ($purch_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $qry = $qry->where('purchsub_purch_id', $purch_filter, $purch_val);
                    break;
                case 'between':
                    if ($purch_val2) {
                        $qry = $qry->whereBetween('purchsub_purch_id', [$purch_val, $purch_val2]);
                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }

        if ($rq['prd_id'] != "") {
            $qry = $qry->where('purchsub_prd_id', $request['prd_id']);
        }

        if ($rq['cat_id'] != "" || $rq['manfact_id'] != "") {

            $prdctQr = Product::select('prd_id');
            if ($rq['cat_id'] != '') {
                $prdctQr = $prdctQr->where('prd_cat_id', $rq['cat_id']);
            }
            if ($rq['manfact_id'] != '') {
                $qry = $qry->where('prd_supplier', $rq['manfact_id']);
            }
            $prdIds = $prdctQr->get()->pluck('prd_id')->toArray();
            if (empty($prdIds)) {
                return [];
            } else {
                $qry = $qry->whereIn('purchsub_prd_id', $prdIds);
            }

        }

        if ($rq['sup_id'] != "") {

            $purchIds = Purchase::where('purch_supp_id', $rq['sup_id'])
                ->get()->pluck('purch_id')->toArray();
            if (empty($purchIds)) {
                return [];
            } else {
                $qry = $qry->whereIn('purchsub_purch_id', $purchIds);
            }

        }

        $tot = $qry->with($rltn)->get();

        $tq = $ta = $tp = $tc = 0;
        foreach ($tot as $k => $val) {
            $tq = $val['purchsub_qty'] + $tq;
            $ta = $val['purchase_amount'] + $ta;
            $prd_count[$val['product']['prd_id']] = $k;
            $cat_count[$val['product']['prd_cat_id']] = $k;
        }
        $total = ['total_qty' => $tq, 'purchase_amount' => $ta, 'total_products' => count($prd_count), 'total_cats' => count($cat_count)];

        $data = $qry->with($rltn)->orderBy('purchsub_purch_id', 'ASC')->paginate($this->per_page)->toArray();
        foreach ($data['data'] as $k => $val) {
            $out[$val['product']['prd_cat_id']][$val['purchsub_prd_id']][] = $val;
        }

        $i = 0;
        $k = 0;
        foreach ($out as $cat_id => $val) {

            $cat_array = Category::select('cat_name', 'cat_id')->where('cat_id', $cat_id)->first();
            $result[$i]['category'] = $cat_array;

            $cat_tot_qty = $cat_tot_amount = 0;
            foreach ($val as $prd_id => $purchase) {
                $tot_qty = $tot_amount = 0;
                $result[$i]['product'][$k]['item']['prd_name'] = $purchase[0]['product']['prd_name'];
                foreach ($purchase as $key => $sub) {
                    if ($sub['purchsub_prd_id'] == $prd_id) {
                        $result[$i]['product'][$k]['purchase'][] = $sub;
                        $tot_qty = $tot_qty + $sub['purchsub_qty'];
                        $tot_amount = $tot_amount + $sub['purchase_amount'];

                    }
                }
                $cat_tot_qty = $cat_tot_qty + $tot_qty;
                $cat_tot_amount = $cat_tot_amount + $tot_amount;
                $result[$i]['product'][$k]['item']['purchsub_qty'] = $tot_qty;
                $result[$i]['product'][$k]['item']['purchase_amount'] = $tot_amount;
                $result[$i]['product'][$k]['item']['purchsub_rate'] = round($tot_amount / $tot_qty);
                $k++;

            }
            $result[$i]['category']['purchsub_qty'] = $cat_tot_qty;
            $result[$i]['category']['purchase_amount'] = $cat_tot_amount;
            $result[$i]['category']['purchsub_rate'] = round($cat_tot_amount / $cat_tot_qty);
            $i++;
        }

        foreach ($result as $key => $val) {
            unset($result[$key]['product']);
            foreach ($val['product'] as $k => $out) {
                $result[$key]['products'][] = $out;
            }
        }

        $data['data'] = $result;
        $data['total'] = $total;
        return $data;
    }

    public function reportByProduct($rq)
    {

        $amnt = DB::raw('sum(purchsub_rate * purchsub_qty) as purchase_amount');
        $qty = DB::raw('sum(purchsub_qty) as purchsub_qty');
        $slct = ['purchsub_purch_id', 'purchsub_prd_id', $qty, 'purchsub_rate', $amnt];
        $rltn = [
            'product' => function ($qr) {
                $qr->select('prd_id', 'prd_name', 'prd_cat_id', 'prd_barcode');
            },
        ];
        $qry = PurchaseSub::select($slct);

        if ($ptype = isset($rq['period_type']) ? $rq['period_type'] : 0) {
            list($date1, $date2) = $this->datesFromPeriods($ptype, $rq);

            if ($date1 && $date2) {
                $qry = $qry->whereBetween('purchsub_date', [$date1, $date2]);
            }
        }

        $purch_filter = isset($rq['purch_filter']) ? $rq['purch_filter'] : 0;
        $purch_val = isset($rq['purch_val']) ? $rq['purch_val'] : 0;
        $purch_val2 = isset($rq['purch_val2']) ? $rq['purch_val2'] : 0;
        if ($purch_filter && $purch_val) {
            switch ($purch_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $qry = $qry->where('purchsub_purch_id', $purch_filter, $purch_val);
                    break;
                case 'between':
                    if ($purch_val2) {
                        $qry = $qry->whereBetween('purchsub_purch_id', [$purch_val, $purch_val2]);
                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }

        if ($rq['prd_id'] != "") {
            $qry = $qry->where('purchsub_prd_id', $request['prd_id']);
        }

        if ($rq['cat_id'] != "" || $rq['manfact_id'] != "") {

            $prdctQr = Product::select('prd_id');
            if ($rq['cat_id'] != '') {
                $prdctQr = $prdctQr->where('prd_cat_id', $rq['cat_id']);
            }
            if ($rq['manfact_id'] != '') {
                $qry = $qry->where('prd_supplier', $rq['manfact_id']);
            }
            $prdIds = $prdctQr->get()->pluck('prd_id')->toArray();
            if (empty($prdIds)) {
                return [];
            } else {
                $qry = $qry->whereIn('purchsub_prd_id', $prdIds);
            }

        }

        if ($rq['sup_id'] != "") {

            $purchIds = Purchase::where('purch_supp_id', $rq['sup_id'])
                ->get()->pluck('purch_id')->toArray();
            if (empty($purchIds)) {
                return [];
            } else {
                $qry = $qry->whereIn('purchsub_purch_id', $purchIds);
            }

        }

        $tot = $qry->with($rltn)->get();

        $tq = $ta = $tp = $tc = 0;
        foreach ($tot as $k => $val) {
            $tq = $val['purchsub_qty'] + $tq;
            $ta = $val['purchase_amount'] + $ta;
            $prd_count[$val['product']['prd_id']] = $k;
            $cat_count[$val['product']['prd_cat_id']] = $k;
        }
        $total = ['total_qty' => $tq, 'purchase_amount' => $ta, 'total_products' => count($prd_count), 'total_cats' => count($cat_count)];

        $data = $qry->with($rltn)->groupBy('purchsub_prd_id')->orderBy('purchsub_purch_id', 'ASC')->paginate($this->per_page)->toArray();
        foreach ($data['data'] as $k => $val) {
            $out[$val['product']['prd_cat_id']][$val['purchsub_prd_id']][] = $val;
        }

        $i = 0;
        $k = 0;
        foreach ($out as $cat_id => $val) {

            $cat_array = Category::select('cat_name', 'cat_id')->where('cat_id', $cat_id)->first();
            $result[$i]['category'] = $cat_array;

            $cat_tot_qty = $cat_tot_amount = 0;
            foreach ($val as $prd_id => $purchase) {
                $tot_qty = $tot_amount = 0;
                $result[$i]['product'][$k]['item']['prd_name'] = $purchase[0]['product']['prd_name'];
                foreach ($purchase as $key => $sub) {
                    if ($sub['purchsub_prd_id'] == $prd_id) {
                        $result[$i]['product'][$k]['purchase'][] = $sub;
                        $tot_qty = $tot_qty + $sub['purchsub_qty'];
                        $tot_amount = $tot_amount + $sub['purchase_amount'];

                    }
                }
                $cat_tot_qty = $cat_tot_qty + $tot_qty;
                $cat_tot_amount = $cat_tot_amount + $tot_amount;
                $result[$i]['product'][$k]['item']['purchsub_qty'] = $tot_qty;
                $result[$i]['product'][$k]['item']['purchase_amount'] = $tot_amount;
                $result[$i]['product'][$k]['item']['purchsub_rate'] = round($tot_amount / $tot_qty);
                $k++;

            }
            $result[$i]['category']['purchsub_qty'] = $cat_tot_qty;
            $result[$i]['category']['purchase_amount'] = $cat_tot_amount;
            $result[$i]['category']['purchsub_rate'] = round($cat_tot_amount / $cat_tot_qty);
            $i++;
        }

        foreach ($result as $key => $val) {
            unset($result[$key]['product']);
            foreach ($val['product'] as $k => $out) {
                $result[$key]['products'][] = $out;
            }
        }

        $data['data'] = $result;
        $data['total'] = $total;
        return $data;
    }

// old van stock report
    public function vanTransReportByPeriod1old(Request $rq)
    {

        if ($rq['van_id'] != "") {

            $vanSelqty = DB::raw('(SELECT sum(vantransub_qty) FROM erp_van_transfer_sub WHERE erp_van_transfer_sub.vantransub_prod_id = erp_products.prd_id and erp_van_transfer_sub.vantransub_van_id = erp_van.van_id and erp_van_transfer_sub.vantransub_flags = 1) as trans_stock_qty');
            $vanSeltransRetqty = DB::raw('(SELECT sum(vantranretsub_qty) FROM erp_van_transfer_return_sub WHERE erp_van_transfer_return_sub.vantranretsub_prod_id = erp_van_transfer_sub.vantransub_prod_id and erp_van_transfer_return_sub.vantransub_branch_stock_id = erp_van_transfer_sub.vantransub_branch_stock_id and erp_van_transfer_sub.vantransub_van_id = erp_van_transfer_return_sub.vantranretsub_van_id and erp_van_transfer_sub.vantransub_flags = 1 and erp_van_transfer_return_sub.vantranretsub_flags = 1) as ret_stock_qty');

            $vanSelrate = DB::raw('(SELECT sum(vantransub_purch_rate) FROM erp_van_transfer_sub WHERE erp_van_transfer_sub.vantransub_prod_id = erp_products.prd_id and erp_van_transfer_sub.vantransub_van_id = erp_van.van_id and erp_van_transfer_sub.vantransub_flags = 1) as purch_rate');
            $vanSelprate = DB::raw('(SELECT (sum(vantransub_purch_rate) * sum(vantransub_qty)) FROM erp_van_transfer_sub WHERE erp_van_transfer_sub.vantransub_prod_id = erp_products.prd_id and erp_van_transfer_sub.vantransub_van_id = erp_van.van_id and erp_van_transfer_sub.vantransub_flags = 1) as purch_amount');

        } else {

            $vanSelqty = DB::raw('(SELECT sum(vantransub_qty) FROM erp_van_transfer_sub WHERE erp_van_transfer_sub.vantransub_prod_id = erp_products.prd_id and erp_van_transfer_sub.vantransub_flags = 1) as trans_stock_qty');
            $vanSeltransRetqty = DB::raw('(SELECT sum(vantranretsub_qty) FROM erp_van_transfer_return_sub WHERE erp_van_transfer_return_sub.vantranretsub_prod_id = erp_products.prd_id and erp_van_transfer_sub.vantransub_flags = 1 and erp_van_transfer_return_sub.vantranretsub_flags) as ret_stock_qty');

            $vanSelrate = DB::raw('(SELECT sum(vantransub_purch_rate) FROM erp_van_transfer_sub WHERE erp_van_transfer_sub.vantransub_prod_id = erp_products.prd_id and erp_van_transfer_sub.vantransub_flags = 1) as purch_rate');
            $vanSelprate = DB::raw('(SELECT (sum(vantransub_purch_rate) * sum(vantransub_qty)) FROM erp_van_transfer_sub WHERE erp_van_transfer_sub.vantransub_prod_id = erp_products.prd_id and erp_van_transfer_sub.vantransub_flags = 1) as purch_amount');
        }

        $qry = VanTransferSub::select('vantransub_prod_id', 'vantransub_stock_id', 'vantransub_van_id'
            , 'products.prd_name', 'van.van_name', 'van.van_id', 'van_transfer_sub.vantransub_branch_stock_id', $vanSelqty, $vanSeltransRetqty, $vanSelrate, $vanSelprate);

        $qry->join('products', 'products.prd_id', 'van_transfer_sub.vantransub_prod_id');

        $qry->leftjoin('van_transfer_return_sub', function ($join) {
            $join->on('van_transfer_return_sub.vantransub_branch_stock_id', '=', 'van_transfer_sub.vantransub_branch_stock_id');
            $join->on('van_transfer_sub.vantransub_unit_id', '=', 'van_transfer_return_sub.vantranretsub_unit_id');
            $join->where('van_transfer_return_sub.vantranretsub_flags', 1);
        });

        $qry->join('van', 'van.van_id', 'van_transfer_sub.vantransub_van_id');

        if ($this->branch_id > 0) {
            $qry->where('van_transfer_sub.branch_id', $this->branch_id);
        }

        $qry->where('vantransub_flags', 1);

        if ($this->branch_id > 0) {
            $qry->where('van_transfer_sub.branch_id', $this->branch_id);
        }

        if ($ptype = isset($rq['period_type']) ? $rq['period_type'] : 0) {

            list($date1, $date2) = $this->datesFromPeriods($ptype, $rq);

            if ($date1 && $date2) {

                $qry = $qry->whereBetween('vantransub_date', [$date1, $date2]);

            }

        }

        if ($rq['prd_id'] != "") {
            $qry = $qry->where('vantransub_prod_id', $rq['prd_id']);
        }

        if ($rq['cat_id'] != "" || $rq['manfact_id'] != "") {

            $prdctQr = Product::select('prd_id');
            if ($rq['cat_id'] != '') {
                $prdctQr = $prdctQr->where('products.prd_cat_id', $rq['cat_id']);
            }
            if ($rq['manfact_id'] != '') {
                $prdctQr = $prdctQr->where('products.prd_supplier', $rq['manfact_id']);
            }
            $prdIds = $prdctQr->get()->pluck('prd_id')->toArray();
            if (empty($prdIds)) {
                return [];
            } else {
                $qry = $qry->whereIn('vantransub_prod_id', $prdIds);
            }

        }

        if ($rq['van_id'] != "") {
            $qry = $qry->where('vantransub_van_id', $rq['van_id']);

        }

        $allData = $qry->groupby('vantransub_prod_id')->orderby('van_name')->get()->toArray();

        $data = $qry->groupby('vantransub_prod_id')->orderby('van_name')->paginate($this->per_page)->toArray();

        if ($allData) {

            foreach ($data['data'] as $key => $val) {

                $purch_rate = VanStock::select(DB::raw('(sum(vanstock_avg_tran_rate * vanstock_qty) / sum(vanstock_qty)) as avg_rate'))
                    ->where('vanstock_branch_stock_id', $val['vantransub_branch_stock_id']);
                if ($rq['van_id'] != "") {
                    $purch_rate->where('vanstock_van_id', $val['van_id']);
                }
                $avg_purch_rate = $purch_rate->get();
                $data['data'][$key]['purch_rate'] = $val['purch_rate'] = round($avg_purch_rate[0]['avg_rate'], 2);
                $data['data'][$key]['stock_qty'] = $val['trans_stock_qty'] - $val['ret_stock_qty'];
                $data['data'][$key]['purch_amount'] = $data['data'][$key]['stock_qty'] * $val['purch_rate'];
            }
            foreach ($allData as $key => $val) {
                $purch_rate = VanStock::select(DB::raw('(sum(vanstock_avg_tran_rate * vanstock_qty) / sum(vanstock_qty)) as avg_rate'))->where('vanstock_branch_stock_id', $val['vantransub_branch_stock_id']);
                if ($rq['van_id'] != "") {
                    $purch_rate->where('vanstock_van_id', $val['van_id']);
                }
                $avg_purch_rate = $purch_rate->get();
                $allData[$key]['purch_rate'] = $val['purch_rate'] = round($avg_purch_rate[0]['avg_rate'], 2);
                $allData[$key]['stock_qty'] = $val['trans_stock_qty'] - $val['ret_stock_qty'];
                $allData[$key]['purch_amount'] = $allData[$key]['stock_qty'] * $val['purch_rate'];

            }

            $totalRes['total_products'] = count($allData);
            $totalRes['total_purchase_amount'] = array_sum(array_column($allData, 'purch_amount'));
            $totalRes['total_stock_qty'] = array_sum(array_column($allData, 'stock_qty'));
            $totalRes['total_van'] = count(array_unique(array_column($allData, 'van_id')));

            if ($rq['van_id'] != "") {
                $van_name = Van::select('van_name')->where('van_id', $rq['van_id'])->get()->pluck('van_name');
                $data['reportBy'] = $van_name;
            }

            $data['totalDetails'] = $totalRes;

            if ($rq['period_type'] != "") {
                $data['Date']['date1'] = date("d-F-Y", strtotime($date1));
                $data['Date']['date2'] = date("d-F-Y", strtotime($date2));
            } else {
                $data['Date'] = "";
            }

            return parent::displayData($data);
        } else {
            return parent::displayData('');
        }

    }
// old van stock report end

    public function vanTransReportByPeriod(Request $rq)
    {

        $qry = VanDailyStocks::select('products.prd_name')->groupBy('vds_branch_stock_id')
            ->selectRaw('sum(vds_stock_quantity) as stock_qty, vds_prd_id,vds_stock_id,vds_van_id,vds_branch_stock_id');

        $qry->join('products', 'products.prd_id', 'van_daily_stocks.vds_prd_id');

        if ($this->branch_id > 0) {
            $qry = $qry->where('van_daily_stocks.branch_id', $this->branch_id);
        }

        if ($ptype = isset($rq['period_type']) ? $rq['period_type'] : 0) {

            list($date1, $date2) = $this->datesFromPeriods($ptype, $rq);

            if ($date1 && $date2) {

                $qry = $qry->whereBetween('vds_date', [$date1, $date2]);

            }

        }

        if ($rq['prd_id'] != "") {
            $qry = $qry->where('vds_prd_id', $rq['prd_id']);
        }

        if ($rq['cat_id'] != "" || $rq['manfact_id'] != "") {

            $prdctQr = Product::select('prd_id');
            if ($rq['cat_id'] != '') {
                $prdctQr = $prdctQr->where('products.prd_cat_id', $rq['cat_id']);
            }
            if ($rq['manfact_id'] != '') {
                $prdctQr = $prdctQr->where('products.prd_supplier', $rq['manfact_id']);
            }
            $prdIds = $prdctQr->get()->pluck('prd_id')->toArray();
            if (empty($prdIds)) {
                return [];
            } else {
                $qry = $qry->whereIn('vds_prd_id', $prdIds);
            }

        }

        if ($rq['van_id'] != "") {
            $qry = $qry->where('vds_van_id', $rq['van_id']);

        }

        $allData = $qry->get()->toArray();
        $data = $qry->paginate(10)->toArray();

        if ($allData) {
            foreach ($data['data'] as $key => $val) {

                $purch_rate = VanStock::select(DB::raw('(sum(vanstock_avg_tran_rate * vanstock_qty) / sum(vanstock_qty)) as avg_rate'))
                    ->where('vanstock_branch_stock_id', $val['vds_branch_stock_id']);
                if ($rq['van_id'] != "") {
                    $purch_rate->where('vanstock_van_id', $val['vds_van_id']);
                }
                $avg_purch_rate = $purch_rate->get();
                $data['data'][$key]['purch_rate'] = $val['purch_rate'] = round($avg_purch_rate[0]['avg_rate'], 2);
                $data['data'][$key]['purch_amount'] = $data['data'][$key]['stock_qty'] * $val['purch_rate'];
            }
            foreach ($allData as $key => $val) {
                $purch_rate = VanStock::select(DB::raw('(sum(vanstock_avg_tran_rate * vanstock_qty) / sum(vanstock_qty)) as avg_rate'))->where('vanstock_branch_stock_id', $val['vds_branch_stock_id']);
                if ($rq['van_id'] != "") {
                    $purch_rate->where('vanstock_van_id', $val['vds_van_id']);
                }
                $avg_purch_rate = $purch_rate->get();
                $allData[$key]['purch_rate'] = $val['purch_rate'] = round($avg_purch_rate[0]['avg_rate'], 2);
                $allData[$key]['purch_amount'] = $allData[$key]['stock_qty'] * $val['purch_rate'];

            }

            $totalRes['total_products'] = count($allData);
            $totalRes['total_purchase_amount'] = array_sum(array_column($allData, 'purch_amount'));
            $totalRes['total_stock_qty'] = array_sum(array_column($allData, 'stock_qty'));
            $totalRes['total_van'] = count(array_unique(array_column($allData, 'vds_van_id')));

            if ($rq['van_id'] != "") {
                $van_name = Van::select('van_name')->where('van_id', $rq['van_id'])->get()->pluck('van_name');
                $data['reportBy'] = $van_name;
            }

            $data['totalDetails'] = $totalRes;

            if ($rq['period_type'] != "") {
                $data['Date']['date1'] = date("d-F-Y", strtotime($date1));
                $data['Date']['date2'] = date("d-F-Y", strtotime($date2));
            } else {
                $data['Date'] = "";
            }

            return parent::displayData($data);
        } else {
            return parent::displayData('');
        }

        return $result;
    }

    public function vanTransferReturnReport(Request $request)
    {
        $rltn = [
            'items' => function ($qr) {
                $qr->select('*');
            },
            'van' => function ($qr) {
                $qr->select('van_id', 'van_name');
            },
            'gd' => function ($qr) {
                $qr->select('gd_id', 'gd_name');
            },
        ];

        $stat = isset($request->status) ? $request->status : 1;
        $qry = VanTransferReturn::select('*')
        ->where('branch_id', $this->branch_id)->where('vantranret_flag', $stat)->with($rltn);

        $data = $qry->orderBy('vantranret_id', 'DESC')->paginate($this->per_page)->toArray();

        foreach ($data['data'] as $t => $transfer) {

            if (empty($transfer['gd'])) {
                $data['data'][$t]['gd']['0']['gd_id'] = 0;
                $data['data'][$t]['gd']['0']['gd_name'] = 'Shop';
            }
            foreach ($transfer['items'] as $k => $item) {
                //
                $dd = Product::select('prd_name', 'prd_barcode')
                    ->where('prd_id', $item['vantranretsub_prod_id'])->first();
                $data['data'][$t]['items'][$k]['prd_name'] = $dd['prd_name'];
                $data['data'][$t]['items'][$k]['prd_barcode'] = $dd['prd_barcode'];

            }

        }
        return $data;
    }

    public function vanTransferPreiview(Request $request)
    {

        $amnt = DB::raw('(purchsub_rate * purchsub_qty) as purchase_amount');
        $this->inrSlct = ['purchsub_purch_id', 'purchsub_prd_id', 'purchsub_qty', 'purchsub_rate', 'purchsub_frieght',
            'purchsub_tax', 'purchsub_tax_per', 'purchsub_unit_id', 'purchsub_gd_qty', 'purchsub_gd_id', $amnt];
        $rltn = [
            'items' => function ($qr) {
                $qr->select('*');
            },
            'van' => function ($qr) {
                $qr->select('van_id', 'van_name');
            },
            'gd' => function ($qr) {
                $qr->select('gd_id', 'gd_name');
            },
        ];
        $qry = VanTransfer::select('*')->where('branch_id', $this->branch_id)->where('vantran_flags', 1)->where('vantran_id', $request->purch_id)->with($rltn);

        $data = $qry->get()->toArray();
        $tot = 0;
        foreach ($data as $t => $transfer) {

            if (empty($transfer['gd'])) {
                $data[$t]['gd']['0']['gd_id'] = 0;
                $data[$t]['gd']['0']['gd_name'] = 'Shop';
            }
            foreach ($transfer['items'] as $k => $item) {

                $tot = $tot + $item['vantransub_qty'];
                $dd = Product::select('prd_name', 'prd_barcode')
                    ->where('prd_id', $item['vantransub_prod_id'])->first();
                $data[$t]['items'][$k]['prd_name'] = $dd['prd_name'];
                $data[$t]['items'][$k]['prd_barcode'] = $dd['prd_barcode'];

            }

        }
        $data['tot_qty'] = $tot;
        return ($tot > 0) ? $data : 0;
    }

    public function vanTransferReturnPreiview(Request $request)
    {
        $rltn = [
            'items' => function ($qr) {
                $qr->select('*');
            },
            'van' => function ($qr) {
                $qr->select('van_id', 'van_name');
            },
            'gd' => function ($qr) {
                $qr->select('gd_id', 'gd_name');
            },
        ];
        $qry = VanTransferReturn::select('*')->where('vantranret_flag', 1)
            ->where('vantranret_id', $request->purch_id)->with($rltn);

        $data = $qry->get()->toArray();
        $tot = 0;

        foreach ($data as $t => $transfer) {

            if (empty($transfer['gd'])) {
                $data[$t]['gd']['0']['gd_id'] = 0;
                $data[$t]['gd']['0']['gd_name'] = 'Shop';
            }
            foreach ($transfer['items'] as $k => $item) {

                $tot = $tot + $item['vantranretsub_qty'];
                $dd = Product::select('prd_name', 'prd_barcode')
                    ->where('prd_id', $item['vantranretsub_prod_id'])->first();
                $data[$t]['items'][$k]['prd_name'] = $dd['prd_name'];
                $data[$t]['items'][$k]['prd_barcode'] = $dd['prd_barcode'];
            }
        }
        $data['tot_qty'] = $tot;
        return ($tot > 0) ? $data : 0;
    }

}
