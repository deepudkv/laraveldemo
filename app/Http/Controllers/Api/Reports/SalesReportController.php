<?php

namespace App\Http\Controllers\Api\Reports;

use App\Http\Controllers\Api\ApiParent;
use App\Models\Accounts\Acc_customer;
use App\Models\Accounts\Acc_ledger;
use App\Models\Accounts\Acc_voucher;
use App\Models\Godown\GodownStock;
use App\Models\Godown\GodownStockLog;
use App\Models\Product\ProdUnit;
use App\Models\Sales\SalesDueSub;
use App\Models\Sales\SalesMaster;
use App\Models\Sales\SalesReturnMaster;
use App\Models\Sales\SalesReturnSub;
use App\Models\Sales\SalesSub;
use App\Models\Stocks\BranchStocks;
use App\Models\Stocks\CompanyStocks;
use App\Models\Stocks\StockDailyUpdates;
use App\Models\Van\VanDailyStocks;
use App\Models\Van\VanStock;
use DB;
use Illuminate\Http\Request;

class SalesReportController extends ApiParent
{

    public function Report(Request $request, $type)
    {

        switch ($type) {
            case 'summary':
                return parent::displayData($this->salesSummary($request));
                break;

            case 'due_summary':
                return parent::displayData($this->salesDueSummary($request));
                break;

            case 'return_summary':
                return parent::displayData($this->salesReturnSummary($request));
                break;

            case 'product_invoice':
                return parent::displayData($this->productinvoice($request));
                break;

            case 'product_report':
                return parent::displayData($this->productReport($request));
                break;

            case 'sales_invoice':
                return parent::displayData($this->saleInvDetail($request));
                break;

            case 'sales_invoice_data':
                return parent::displayData($this->saleInvDetailData($request));
                break;

            case 'sales_invoice_bybinv':
                return parent::displayData($this->saleInvDetailByBranch($request));
                break;

            case 'sales_void':
                return $this->salesVoid($request);
                break;

            case 'sales_return_void':
                return $this->salesReturnVoid($request);
                break;

            case 'sales_return_invoice':
                return parent::displayData($this->saleReturnInvDetail($request));
                break;

            case 'get_sales_and_return_total':
                return parent::displayData($this->getsaleReturnTotal($request));
                break;

            default:
                return parent::displayError('Invalid Endpoint', $this->badrequest_stat);
                break;
        }
    }

    public function salesReturnVoid($request)
    {

        if ($sales_inv_num = isset($request['sales_inv_num']) ? $request['sales_inv_num'] : 0) {

            $row_exist = SalesReturnMaster::where('salesret_flags', '1')->where('salesret_no', $sales_inv_num)->first();

            if ($row_exist) {

                $this->proceedSalesReturnVoidOperations($sales_inv_num, $row_exist['salesret_branch_ret_no']);

                return parent::displayMessage('Sales Return  Voided Successfully');

            } else {

                return parent::displayError('Sales Return Already Voided', $this->badrequest_stat);
            }

        } else {
            return parent::displayError('Invalid Inv Number', $this->badrequest_stat);
        }

    }

    public function proceedSalesReturnVoidOperations($salesret_inv_num, $branchRetNo)
    {
        SalesReturnMaster::where('salesret_no', $salesret_inv_num)->update(['salesret_flags' => 0
            , 'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)]);

        $products = SalesReturnSub::where('salesretsub_salesret_no', $salesret_inv_num)->get()->toArray();

        foreach ($products as $k => $val) {

            $prd_id = $val['salesretsub_prod_id'];
            $branch_id = $val['branch_id'];
            $salesub_qty = $val['salesretsub_qty'];
            $godown_id = $val['godown_id'];
            $van_id = $val['van_id'];
            $date = $val['salesretsub_date'];

            $whr['bs_branch_id'] = $branch_id;
            $whr['bs_prd_id'] = $prd_id;
            $bs = BranchStocks::where($whr)->first();
            $branch_stock_id = $bs['branch_stock_id'];
            $bs['bs_stock_quantity'] = $bs['bs_stock_quantity'] - $salesub_qty;
            $bs['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            if ($godown_id) {
                $bs['bs_stock_quantity_gd'] = $bs['bs_stock_quantity_gd'] - $salesub_qty;
            } else {
                if ($van_id) {
                    $bs['bs_stock_quantity_van'] = $bs['bs_stock_quantity_van'] - $salesub_qty;
                } else {
                    $bs['bs_stock_quantity_shop'] = $bs['bs_stock_quantity_shop'] - $salesub_qty;
                }
            }
            $bs->save();

            $whr['bs_branch_id'] = 0;
            $whr['bs_prd_id'] = $prd_id;
            $bsc = BranchStocks::where($whr)->first();

            if (!empty($bsc)) {

                $bsc['bs_stock_quantity'] = $bsc['bs_stock_quantity'] - $salesub_qty;
                $bsc['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                if ($godown_id) {
                    $bsc['bs_stock_quantity_gd'] = $bsc['bs_stock_quantity_gd'] - $salesub_qty;
                } else {
                    if ($van_id) {
                        $bsc['bs_stock_quantity_van'] = $bsc['bs_stock_quantity_van'] - $salesub_qty;
                    } else {
                        $bsc['bs_stock_quantity_shop'] = $bsc['bs_stock_quantity_shop'] - $salesub_qty;
                    }
                }
                $bsc->save();
            }

            $whr2['cmp_prd_id'] = $prd_id;
            $cmpstk = CompanyStocks::where($whr2)->first();

            $cmpstk['cmp_stock_quantity'] = $cmpstk['cmp_stock_quantity'] - $salesub_qty;
            $cmpstk['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            $cmpstk->save();

            $whr3['sdu_date'] = $date;
            $whr3['sdu_prd_id'] = $prd_id;
            $whr3['branch_id'] = $branch_id;
            $dayStk = StockDailyUpdates::where($whr3)->first();

            if (!empty($dayStk)) {

                $dayStk['sdu_stock_quantity'] = $dayStk['sdu_stock_quantity'] - $salesub_qty;
                $dayStk['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                $dayStk->save();

            } else {

                $ds['sdu_branch_stock_id'] = $branch_stock_id;
                $ds['sdu_stock_id'] = $prd_id;
                $ds['sdu_prd_id'] = $prd_id;
                $ds['sdu_date'] = $date;
                $ds['sdu_stock_quantity'] = $salesub_qty * -1;
                $ds['sdu_gd_id'] = $godown_id;
                $ds['sdu_batch_id'] = 0;
                $ds['branch_id'] = $this->branch_id;
                $ds['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                StockDailyUpdates::create($ds);
            }

            if ($van_id) {
                $this->deductStockFromVan($branch_id, $prd_id, $salesub_qty, $van_id, $branch_stock_id, $date);
            }
            if ($godown_id) {
                $this->deductStockToGodown($branch_id, $prd_id, $salesub_qty, $godown_id, $branch_stock_id, $date);
            }

        }

        Acc_voucher::where('branch_ref_no', $branchRetNo)->update(['vch_flags' => 0
            , 'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)]);

        SalesReturnSub::where('salesretsub_salesret_no', $salesret_inv_num)->update(['salesretsub_flags' => 0,
            'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)]);

        SalesDueSub::where('salesduesub_ret_no', $salesret_inv_num)->update(['salesduesub_flags' => 0
            , 'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)]);
    }

    public function deductStockFromVan($branch_id, $prd_id, $salesub_qty, $van_id, $branch_stock_id, $date)
    {

        $whr = ['vanstock_prod_id' => $prd_id,
            'vanstock_branch_stock_id' => $branch_stock_id,
            'vanstock_van_id' => $van_id];

        $vanStk = VanStock::where($whr)->first();

        if (!empty($vanStk)) {
            $vanStk['vanstock_qty'] = $vanStk['vanstock_qty'] - $salesub_qty;
            $vanStk['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            $vanStk->save();
        } else {
            $insert = ['vanstock_qty' => $salesub_qty * -1,
                'vanstock_last_tran_rate' => 0,
                'vanstock_stock_id' => $prd_id,
                'vanstock_avg_tran_rate' => 0,
                'branch_id' => $branch_id, 'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)];

            VanStock::create(array_merge($whr, $insert));
        }

        $where = ['vds_prd_id' => $prd_id,
            'vds_branch_stock_id' => $branch_stock_id,
            'vds_van_id' => $van_id,
            'vds_date' => $date,
        ];

        $vanDailyStk = VanDailyStocks::where($where)->first();

        if (!empty($vanDailyStk)) {

            $vanDailyStk['vds_stock_quantity'] = $vanDailyStk['vds_stock_quantity'] - $salesub_qty;
            $vanDailyStk['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            $vanDailyStk->save();
        } else {

            $insert = ['branch_id' => $branch_id,
                'vds_stock_id' => $prd_id,
                'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6),
                'vds_stock_quantity' => $salesub_qty * -1,
            ];
            VanDailyStocks::create(array_merge($where, $insert));
        }

    }
    public function deductStockToGodown($branch_id, $prd_id, $salesub_qty, $godown_id, $branch_stock_id, $date)
    {

        $gdres = GodownStock::select('*')->where('gs_godown_id', $godown_id)
            ->where('branch_id', $branch_id)
            ->where('gs_prd_id', $prd_id)
            ->first();

        if ($gdres) {
            $gdres['gs_qty'] = $gdres['gs_qty'] - $salesub_qty;
            $gdres['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            $gdres->save();
        } else {
            $gdres = GodownStock::create([

                'gs_godown_id' => $godown_id,
                'gs_branch_stock_id' => $branch_stock_id,
                'gs_stock_id' => $prd_id,
                'gs_date' => date('Y-m-d'),
                'gs_prd_id' => $prd_id,
                'gs_qty' => $salesub_qty * -1,
                'branch_id' => $branch_id,
                'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6),

            ]);
        }

        $gdLog['gsl_gdwn_stock_id'] = $gdres->gs_id;
        $gdLog['gsl_branch_stock_id'] = $branch_stock_id;
        $gdLog['gsl_stock_id'] = $prd_id;
        $gdLog['gsl_prd_id'] = $prd_id;
        $gdLog['gsl_prod_unit'] = 0;
        $gdLog['gsl_qty'] = $salesub_qty * -1;
        $gdLog['gsl_from'] = 0;
        $gdLog['gsl_to'] = $godown_id;
        $gdLog['gsl_tran_vanid'] = 0;
        $gdLog['gsl_vchr_type'] = 4;
        $gdLog['gsl_date'] = date('Y-m-d');
        $gdLog['gsl_added_by'] = $this->usr_id;
        $gdLog['branch_id'] = $branch_id;
        $gdLog['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);

        GodownStockLog::create($gdLog);

    }

    public function salesVoid($request)
    {

        if ($sales_inv_num = isset($request['sales_inv_num']) ? $request['sales_inv_num'] : 0) {

            $row_exist = SalesReturnMaster::where('salesret_flags', '1')->where('salesret_sales_inv_no', $sales_inv_num)->first();

            if ($row_exist) {
                return parent::displayError('Sales Already Returned! You Can Void the sale after Sales Return Void ', $this->badrequest_stat);
            } else {
                $this->proceedSalesVoidOperations($sales_inv_num);

                return parent::displayMessage('Sales Voided Successfully');
            }

        } else {
            return parent::displayError('Invalid Inv Number', $this->badrequest_stat);
        }

    }

    public function addStockToVan($branch_id, $prd_id, $salesub_qty, $van_id, $branch_stock_id, $date)
    {

        $whr = ['vanstock_prod_id' => $prd_id,
            'vanstock_branch_stock_id' => $branch_stock_id,
            'vanstock_van_id' => $van_id];

        $vanStk = VanStock::where($whr)->first();

        if (!empty($vanStk)) {
            $vanStk['vanstock_qty'] = $vanStk['vanstock_qty'] + $salesub_qty;
            $vanStk['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            $vanStk->save();
        } else {
            $insert = ['vanstock_qty' => $salesub_qty,
                'vanstock_last_tran_rate' => 0,
                'vanstock_stock_id' => $prd_id,
                'vanstock_avg_tran_rate' => 0,
                'branch_id' => $branch_id, 'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)];

            VanStock::create(array_merge($whr, $insert));
        }

        $where = ['vds_prd_id' => $prd_id,
            'vds_branch_stock_id' => $branch_stock_id,
            'vds_van_id' => $van_id,
            'vds_date' => $date,
        ];

        $vanDailyStk = VanDailyStocks::where($where)->first();

        if (!empty($vanDailyStk)) {

            $vanDailyStk['vds_stock_quantity'] = $vanDailyStk['vds_stock_quantity'] + $salesub_qty;
            $vanDailyStk['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            $vanDailyStk->save();
        } else {

            $insert = ['branch_id' => $branch_id,
                'vds_stock_id' => $prd_id,
                'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6),
                'vds_stock_quantity' => $salesub_qty,
            ];
            VanDailyStocks::create(array_merge($where, $insert));
        }

    }
    public function addStockToGodown($branch_id, $prd_id, $salesub_qty, $godown_id, $branch_stock_id, $date)
    {

        $gdres = GodownStock::select('*')->where('gs_godown_id', $godown_id)
            ->where('branch_id', $branch_id)
            ->where('gs_prd_id', $prd_id)
            ->first();

        if ($gdres) {
            $gdres['gs_qty'] = $gdres['gs_qty'] + $salesub_qty;
            $gdres['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            $gdres->save();
        } else {
            $gdres = GodownStock::create([

                'gs_godown_id' => $godown_id,
                'gs_branch_stock_id' => $branch_stock_id,
                'gs_stock_id' => $prd_id,
                'gs_date' => date('Y-m-d'),
                'gs_prd_id' => $prd_id,
                'gs_qty' => $salesub_qty,
                'branch_id' => $branch_id,
                'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6),

            ]);
        }

        $gdLog['gsl_gdwn_stock_id'] = $gdres->gs_id;
        $gdLog['gsl_branch_stock_id'] = $branch_stock_id;
        $gdLog['gsl_stock_id'] = $prd_id;
        $gdLog['gsl_prd_id'] = $prd_id;
        $gdLog['gsl_prod_unit'] = 0;
        $gdLog['gsl_qty'] = $salesub_qty;
        $gdLog['gsl_from'] = 0;
        $gdLog['gsl_to'] = $godown_id;
        $gdLog['gsl_tran_vanid'] = 0;
        $gdLog['gsl_vchr_type'] = 4;
        $gdLog['gsl_date'] = date('Y-m-d');
        $gdLog['gsl_added_by'] = $this->usr_id;
        $gdLog['branch_id'] = $branch_id;
        $gdLog['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);

        GodownStockLog::create($gdLog);

    }

    public function proceedSalesVoidOperations($sales_inv_num)
    {

        $sales = SalesMaster::where('sales_inv_no', $sales_inv_num)->first();
        SalesMaster::where('sales_inv_no', $sales_inv_num)->update(['sales_flags' => 0
            , 'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)]);

        SalesDueSub::where('salesduesub_inv_no', $sales_inv_num)->update(['salesduesub_flags' => 0,
            'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)]);

        Acc_voucher::where('branch_ref_no', $sales['sales_branch_inv'])->update(['vch_flags' => 0,
            'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)]);

        $products = SalesSub::where('salesub_sales_inv_no', $sales_inv_num)->get()->toArray();

        foreach ($products as $k => $val) {

            $prd_id = $val['salesub_prod_id'];
            $branch_id = $val['branch_id'];
            $salesub_qty = $val['salesub_qty'];
            $godown_id = $val['godown_id'];
            $van_id = $val['van_id'];
            $date = $val['salesub_date'];

            $whr['bs_branch_id'] = $branch_id;
            $whr['bs_prd_id'] = $prd_id;
            $bs = BranchStocks::where($whr)->first();

            $branch_stock_id = $bs['branch_stock_id'];

            $bs['bs_stock_quantity'] = $bs['bs_stock_quantity'] + $salesub_qty;
            $bs['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            if ($godown_id) {
                $bs['bs_stock_quantity_gd'] = $bs['bs_stock_quantity_gd'] + $salesub_qty;
            } else {
                if ($van_id) {
                    $bs['bs_stock_quantity_van'] = $bs['bs_stock_quantity_van'] + $salesub_qty;
                } else {
                    $bs['bs_stock_quantity_shop'] = $bs['bs_stock_quantity_shop'] + $salesub_qty;
                }
            }
            $bs->save();

            $whr['bs_branch_id'] = 0;
            $whr['bs_prd_id'] = $prd_id;
            $bsc = BranchStocks::where($whr)->first();

            if (!empty($bsc)) {

                $bsc['bs_stock_quantity'] = $bsc['bs_stock_quantity'] + $salesub_qty;
                $bsc['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                if ($godown_id) {
                    $bsc['bs_stock_quantity_gd'] = $bsc['bs_stock_quantity_gd'] + $salesub_qty;
                } else {
                    if ($van_id) {
                        $bsc['bs_stock_quantity_van'] = $bsc['bs_stock_quantity_van'] + $salesub_qty;
                    } else {
                        $bsc['bs_stock_quantity_shop'] = $bsc['bs_stock_quantity_shop'] + $salesub_qty;
                    }
                }
                $bsc->save();
            }

            $whr2['cmp_prd_id'] = $prd_id;
            $cmpstk = CompanyStocks::where($whr2)->first();

            $cmpstk['cmp_stock_quantity'] = $cmpstk['cmp_stock_quantity'] + $salesub_qty;
            $cmpstk['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            $cmpstk->save();

            $whr3['sdu_date'] = $date;
            $whr3['sdu_prd_id'] = $prd_id;
            $whr3['branch_id'] = $branch_id;
            $dayStk = StockDailyUpdates::where($whr3)->first();

            if (!empty($dayStk)) {

                $dayStk['sdu_stock_quantity'] = $dayStk['sdu_stock_quantity'] + $salesub_qty;
                $dayStk['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                $dayStk->save();

            } else {

                $ds['sdu_branch_stock_id'] = $branch_stock_id;
                $ds['sdu_stock_id'] = $prd_id;
                $ds['sdu_prd_id'] = $prd_id;
                $ds['sdu_date'] = $date;
                $ds['sdu_stock_quantity'] = $salesub_qty;
                $ds['sdu_gd_id'] = $godown_id;
                $ds['sdu_batch_id'] = 0;
                $ds['branch_id'] = $this->branch_id;
                $ds['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                StockDailyUpdates::create($ds);
            }

            if ($van_id) {
                $this->addStockToVan($branch_id, $prd_id, $salesub_qty, $van_id, $branch_stock_id, $date);
            }
            if ($godown_id) {
                $this->addStockToGodown($branch_id, $prd_id, $salesub_qty, $godown_id, $branch_stock_id, $date);
            }

        }
        SalesSub::where('salesub_sales_inv_no', $sales_inv_num)->update(['salesub_flags' => 0,
            'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)]);
    }

    public function salesReturnSummary($request)
    {

        if (!$request['datewise']) {
            $sales_total = DB::raw('SUM(salesret_amount) as sales_total');
            $sales_tax = DB::raw('SUM(salesret_tax) as sales_tax');

        } else {
            $sales_total = 'salesret_amount as sales_total';
            $sales_tax = 'salesret_tax as sales_tax';
        }

        $qry = SalesReturnMaster::select('salesret_id', 'salesret_branch_ret_no', 'salesret_sales_inv_no', 'salesret_no', 'salesret_cust_name as  sales_cust_name', 'salesret_pay_type as sales_pay_type', 'salesret_date',
            'salesret_sales_branch_inv_no as sales_branch_inv', 'salesret_sales_van_inv_no as sales_van_inv'
            , $sales_total, 'salesret_discount', $sales_tax, 'salesret_notes');

        $rltn = [
            'customer' => function ($qr) {
                $qr->select('cust_id', 'name', 'vat_no');
            },
        ];

        if ($ptype = isset($request['period_type']) ? $request['period_type'] : 0) {

            if (isset($request['time1']) && isset($request['time2'])) {
                list($date1, $date2, $time1, $time2) = $this->datesFromPeriods($ptype, $request);
            } else {
                list($date1, $date2) = $this->datesFromPeriods($ptype, $request);

            }
            if ($date1 && $date2) {
                $qry = $qry->whereBetween('salesret_date', [$date1, $date2]);
            }
            if (isset($request['time1']) && isset($request['time2'])) {
                if ($time1 && $time2) {

                    $qry = $qry->whereBetween('salesret_time', [$time1, $time2]);
                }
            }
        }

        $inv_filter = $request['inv_filter'];
        $inv_val1 = $request['inv_val1'];
        $inv_val2 = $request['inv_val2'];

        if ($inv_filter && $inv_val1) {
            switch ($inv_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $qry = $qry->where('salesret_sales_inv_no', $inv_filter, $inv_val1);
                    break;
                case 'between':
                    if ($inv_val2) {
                        $qry = $qry->whereBetween('salesret_sales_inv_no', [$inv_val1, $inv_val2]);
                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }

        $ref_filter = $request['ref_filter'];
        $ref_val1 = $request['ref_val1'];
        $ref_val2 = $request['ref_val2'];
        if ($ref_filter && $ref_val1) {
            switch ($ref_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $qry = $qry->where('salesret_no', $ref_filter, $ref_val1);
                    break;
                case 'between':
                    if ($ref_val2) {
                        $qry = $qry->whereBetween('salesret_no', [$ref_val1, $ref_val2]);
                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }

        $sales_filter = $request['sales_filter'];
        $sales_val1 = $request['sales_val1'];
        $sales_val2 = $request['sales_val2'];

        if ($sales_filter && $sales_val1) {
            switch ($sales_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $qry = $qry->where('salesret_amount', $sales_filter, $sales_val1);
                    break;
                case 'between':
                    if ($sales_val1) {
                        $qry = $qry->whereBetween('salesret_amount', [$sales_val1, $sales_val2]);
                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }

        $sales_disc_filter = $request['sales_disc_filter'];
        $disc_val1 = $request['disc_val1'];
        $disc_val2 = $request['disc_val2'];

        if ($sales_disc_filter && $disc_val1) {
            switch ($sales_disc_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $qry = $qry->where('salesret_discount', $sales_disc_filter, $disc_val1);
                    break;
                case 'between':
                    if ($disc_val1) {
                        $qry = $qry->whereBetween('salesret_discount', [$disc_val1, $disc_val2]);
                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }

        if ($request['pay_type'] != '') {
            $qry = $qry->where('salesret_pay_type', $request['pay_type']);
        }

        if ($request['cust_type'] != '') {
            $qry = $qry->where('salesret_cust_type', $request['cust_type']);
        }

        if ($request['cust_id'] != '') {
            $qry = $qry->where('salesret_cust_id', $request['cust_id']);
        }
        // if ($request['vat_reg_no'] != '') {
        //     $qry = $qry->where('salesret_cust_id', $request['vat_reg_no']);
        // }

        // if ($request['gd_id'] != '') {
        //     $qry = $qry->where('godown_id', $request['gd_id']);
        // }

        // if ($request['agent_id'] != '') {
        //     $qry = $qry->where('sales_agent_ledger_id', $request['agent_id']);
        // }
        // if ($request['added_by'] != '') {
        //     $qry = $qry->where('sales_added_by', $request['added_by']);
        // }

        if (isset($request['sales_flags']) && $request['sales_flags'] != 1) {

            $allres = $qry = $qry->where('salesret_flags', 0);
            $qry = $qry->where('salesret_flags', 0);

        } else {

            $qry = $qry->where('salesret_flags', 1);
            $allres = $qry = $qry->where('salesret_flags', 1);

        }

        if ($this->branch_id > 0) {
            $qry = $qry->where('branch_id', $this->branch_id);
        } else {

            if ($this->active_branch_id == 'All') {

            } else {
                $qry = $qry->where('branch_id', $this->active_branch_id);
            }
        }

        $allres = $qry = $qry1 = $qry->where('sync_completed', 1);
        $data = $allres->orderby('salesret_date', 'desc');
        // return $this->branch_id;

        if (!$request['datewise']) {
            $data = $allres->groupby('salesret_date');
            $pageLimit = 31;
        } else {
            $pageLimit = 30;

        }

        $data = $allres->get()->toArray();

        $qry = $qry->with($rltn)->orderby('salesret_date', 'desc');

        if (!$request['datewise']) {
            $qry = $qry->groupby('salesret_date');
        }

        if (isset($request['export']) && $request['export'] == 'export') {
            $res['data'] = $qry->get()->toArray();
        } else {
            $res = $qry->paginate($pageLimit)->toArray();

        }

        if ($data) {
            foreach ($res['data'] as $k => $v) {
                $out[$v['salesret_date']][] = $v;
            }
            // return $out;exit;

            foreach ($data as $k => $val) {
                $tot[$val['salesret_date']][] = $val;
            }

            foreach ($out as $k => $v) {

                $list = $this->getItemTotal($tot, $k);

                $list['sales_date'] = $k;
                $list['tot_disc_amt'] = array_sum(array_column($v, 'salesret_discount'));
                $list['list'] = $v;
                $array[] = $list;
            }

            $credit = $qry1->where('salesret_pay_type', 2)->get()->toArray();

            $result['data'] = $array;
            $result['tot_result'] = count($data);
            $result['tot_days'] = count(array_unique(array_column($data, 'sales_date')));
            $result['tot_inv_amt'] = round(array_sum(array_column($data, 'sales_total')), 2);
            $result['tot_tax_amt'] = round(array_sum(array_column($data, 'sales_tax')), 2);
            $result['tot_credit'] = round(array_sum(array_column($credit, 'sales_total')), 2);
            $result['tot_cash'] = $result['tot_inv_amt'] - $result['tot_credit'];

            $result['tot_disc_amt'] = array_sum(array_column($data, 'salesret_discount'));

            if ($request['period_type'] != "") {
                $result['Date']['date1'] = date("d-F-Y", strtotime($date1));
                $result['Date']['date2'] = date("d-F-Y", strtotime($date2));
            } else {
                $result['Date'] = "";
            }
            if (!$request['datewise']) {
                $result['shodet'] = false;
            } else {
                $result['shodet'] = true;

            }

            $res['data'] = $result;
            return $res;
        } else {
            return $res;
        }
    }
    public function salesSummary($request)
    {

        if (!$request['datewise']) {
            $sales_total = DB::raw('SUM(sales_total) as sales_total');
            $sales_tax = DB::raw('SUM(sales_tax) as sales_tax');

        } else {
            $sales_total = 'sales_total';
            $sales_tax = 'sales_tax';
        }
        if (!$request['datewise']) {
            $qry = SalesMaster::select('sales_id', 'sales_inv_no', 'sales_cust_name', 'sales_pay_type', 'sales_date', 'sales_branch_inv', 'sales_van_inv'
                , $sales_total, 'sales_disc_promo', 'sales_discount', 'sales_discount', $sales_tax, 'sales_notes');
        } else {
            $qry = SalesMaster::select('sales_id', 'sales_inv_no', 'sales_cust_name', 'sales_pay_type', 'sales_date', 'sales_branch_inv', 'sales_van_inv'
                , $sales_total, 'sales_disc_promo', 'sales_discount', 'sales_discount', $sales_tax, 'sales_notes',
                DB::raw('SUM(salesub_qty * salesub_discount) as item_disc'));
        }
        if ($request['datewise']) {
            $qry->join('sale_sub', 'sales_master.sales_inv_no', '=', 'sale_sub.salesub_sales_inv_no');
        }
        $rltn = [
            'customer' => function ($qr) {
                $qr->select('cust_id', 'name', 'vat_no');
            },
            // 'item_disc' => function ($qr) {
            //     $qr->select(DB::raw('SUM(salesub_qty * salesub_discount) as item_disc'),'salesub_sales_inv_no');
            //     $qr->groupby('salesub_sales_inv_no');
            // }
        ];

        if ($ptype = isset($request['period_type']) ? $request['period_type'] : 0) {

            if (isset($request['time1']) && isset($request['time2'])) {
                list($date1, $date2, $time1, $time2) = $this->datesFromPeriods($ptype, $request);
            } else {
                list($date1, $date2) = $this->datesFromPeriods($ptype, $request);

            }
            if ($date1 && $date2) {
                $qry = $qry->whereBetween('sales_date', [$date1, $date2]);
            }
            if (isset($request['time1']) && isset($request['time2'])) {
                if ($time1 && $time2) {

                    $qry = $qry->whereBetween('sales_time', [$time1, $time2]);
                }
            }
        }

        $inv_filter = $request['inv_filter'];
        $inv_val1 = $request['inv_val1'];
        $inv_val2 = $request['inv_val2'];

        if ($inv_filter && $inv_val1) {
            switch ($inv_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $qry = $qry->where('sales_id', $inv_filter, $inv_val1);
                    break;
                case 'between':
                    if ($inv_val1) {
                        $qry = $qry->whereBetween('sales_id', [$inv_val1, $inv_val2]);
                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }

        $sales_filter = $request['sales_filter'];
        $sales_val1 = $request['sales_val1'];
        $sales_val2 = $request['sales_val2'];

        if ($sales_filter && $sales_val1) {
            switch ($sales_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $qry = $qry->where('sales_inv_no', $sales_filter, $sales_val1);
                    break;
                case 'between':
                    if ($sales_val1) {
                        $qry = $qry->whereBetween('sales_inv_no', [$sales_val1, $sales_val2]);
                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }

        $sales_disc_filter = $request['sales_disc_filter'];
        $disc_val1 = $request['disc_val1'];
        $disc_val2 = $request['disc_val2'];

        if ($sales_disc_filter && $disc_val1) {
            switch ($sales_disc_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $qry = $qry->where('salesret_discount', $sales_disc_filter, $disc_val1);
                    break;
                case 'between':
                    if ($disc_val1) {
                        $qry = $qry->whereBetween('salesret_discount', [$disc_val1, $disc_val2]);
                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }

        if ($request['pay_type'] != '') {
            $qry = $qry->where('sales_pay_type', $request['pay_type']);
        }

        if ($request['cust_type'] != '') {
            $qry = $qry->where('sales_cust_type', $request['cust_type']);
        }

        if ($request['cust_id'] != '') {
            $qry = $qry->where('sales_cust_id', $request['cust_id']);
        }
        if ($request['vat_reg_no'] != '') {
            $qry = $qry->where('sales_cust_id', $request['vat_reg_no']);
        }

        if ($request['gd_id'] != '') {
            $qry = $qry->where('godown_id', $request['gd_id']);
        }

        if ($request['agent_id'] != '') {
            $qry = $qry->where('sales_agent_ledger_id', $request['agent_id']);
        }
        if ($request['added_by'] != '') {
            $qry = $qry->where('sales_added_by', $request['added_by']);
        }

        if (isset($request['sales_flags']) && $request['sales_flags'] != 1) {

            $allres = $qry = $qry->where('sales_flags', 0);
            $qry = $qry->where('sales_flags', 0);

        } else {

            $qry = $qry->where('sales_flags', 1);
            $allres = $qry = $qry->where('sales_flags', 1);

        }

        if ($this->branch_id > 0) {
            $qry = $qry->where('sales_master.branch_id', $this->branch_id);
        } else {

            if ($this->active_branch_id != 'All') {
                $qry = $qry->where('sales_master.branch_id', $this->active_branch_id);
            }

        }

        $qry = $qry->where('sales_master.sync_completed', 1);
        $allres = $allres->orderby('sales_date', 'desc');

        // return $this->branch_id;

        if (!$request['datewise']) {
            $data = $allres->groupby('sales_date');
            $pageLimit = 31;
        } else {
            $data = $allres->groupby('salesub_sales_inv_no');

            $pageLimit = 30;

        }

        $data = $allres->get()->toArray();

        $qry = $qry->with($rltn)->orderby('sales_date', 'desc');

        if (!$request['datewise']) {
            $qry = $qry->groupby('sales_date');
        }

        if (isset($request['export']) && $request['export'] == 'export') {
            $res['data'] = $qry->get()->toArray();
        } else {
            $res = $qry->paginate($pageLimit)->toArray();
        }

        // $res = $qry->paginate($pageLimit)->toArray();

        if ($data) {
            foreach ($res['data'] as $k => $v) {
                $out[$v['sales_date']][] = $v;
            }

            foreach ($data as $k => $val) {
                $tot[$val['sales_date']][] = $val;
            }

            foreach ($out as $k => $v) {

                $list = $this->getItemTotal($tot, $k);

                $list['sales_date'] = $k;
                $list['list'] = $v;
                $array[] = $list;
            }

            $result['data'] = $array;
            $result['tot_result'] = count($data);
            $result['tot_days'] = count(array_unique(array_column($data, 'sales_date')));
            $result['tot_inv_amt'] = array_sum(array_column($data, 'sales_total'));
            $result['tot_vat_amt'] = array_sum(array_column($data, 'sales_tax'));
            $result['tot_excl_vat'] = $result['tot_inv_amt'] - $result['tot_vat_amt'];
            $result['tot_disc_amt'] = array_sum(array_column($data, 'sales_discount'));
            $result['tot_disc_promo'] = array_sum(array_column($data, 'sales_disc_promo'));
            $result['tot_item_disc'] = array_sum(array_column($data, 'item_disc'));

            if ($request['period_type'] != "") {
                $result['Date']['date1'] = date("d-F-Y", strtotime($date1));
                $result['Date']['date2'] = date("d-F-Y", strtotime($date2));
            } else {
                $result['Date'] = "";
            }
            if (!$request['datewise']) {
                $result['shodet'] = false;
            } else {
                $result['shodet'] = true;

            }

            $res['data'] = $result;
            return $res;
        } else {
            return $res;
        }
    }
    public function salesDueSummary($request)
    {

        $qry = SalesDueSub::select();

        $rltn = [
            'user' => function ($qr) {
                $qr->select('usr_id','usr_name');
            },
            'agent' => function ($qr) {
                $qr->select('ledger_id','ledger_name');
            },
            'ledger' => function ($qr) {
                $qr->select('ledger_id','ledger_name');
            },
            'invoice' => function ($qr) {
                $qr->select('*');
            }
            
        ];

        if ($ptype = isset($request['period_type']) ? $request['period_type'] : 0) {

            list($date1, $date2) = $this->datesFromPeriods($ptype, $request);
            if ($date1 && $date2) {
                $qry = $qry->whereBetween('salesduesub_date', [$date1, $date2]);
            }
        }

        $inv_filter = $request['inv_filter'];
        $inv_val1 = $request['inv_val1'];
        $inv_val2 = $request['inv_val2'];

        if ($inv_filter && $inv_val1) {
            switch ($inv_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $qry = $qry->where('salesduesub_inv_no', $inv_filter, $inv_val1);
                    break;
                case 'between':
                    if ($inv_val2) {
                        $qry = $qry->whereBetween('salesduesub_inv_no', [$inv_val1, $inv_val2]);
                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }

        $sales_filter = $request['sales_filter'];
        $sales_val1 = $request['sales_val1'];
        $sales_val2 = $request['sales_val2'];

        if ($sales_filter && $sales_val1) {
            switch ($sales_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $qry = $qry->where('salesduesub_inv_amount', $sales_filter, $sales_val1);
                    break;
                case 'between':
                    if ($sales_val1) {
                        $qry = $qry->whereBetween('salesduesub_inv_amount', [$sales_val1, $sales_val2]);
                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }

        $sales_disc_filter = $request['bal_filter'];
        $disc_val1 = $request['bal_val1'];
        $disc_val2 = $request['bal_val2'];

        if ($sales_disc_filter && $disc_val1) {
            switch ($sales_disc_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $qry = $qry->where('salesduesub_inv_balance', $sales_disc_filter, $disc_val1);
                    break;
                case 'between':
                    if ($disc_val1) {
                        $qry = $qry->whereBetween('salesduesub_inv_balance', [$disc_val1, $disc_val2]);
                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }

        if ($request['cust_id'] != '') {
            $qry = $qry->where('salesduesub_ledger_id', $request['cust_id']);
        }

        if ($request['agent_id'] != '') {
            $qry = $qry->where('salesduesub_agent_ledger_id', $request['agent_id']);
        }
        if ($request['added_by'] != '') {
            $qry = $qry->where('salesduesub_added_by', $request['added_by']);
        }

        $qry = $qry->where('salesduesub_flags', 1);

        if ($this->branch_id > 0) {
            $qry = $qry->where('sales_due_sub.branch_id', $this->branch_id);
        } else {
            if ($this->active_branch_id > 0) {
                $qry = $qry->where('sales_due_sub.branch_id', $this->active_branch_id);
            }
        }

        $qry = $qry->where('sales_due_sub.salesduesub_type', 0);
        $qry = $qry->where('sales_due_sub.sync_completed', 1);
        $qry = $qry->orderby('salesduesub_date', 'desc');

        $pageLimit = 30;

        $qry = $qry->with($rltn)->orderby('salesduesub_date', 'desc');

        $out = $qry->paginate($pageLimit)->toArray();

        if ($request['period_type'] != "") {
            $out['Date']['date1'] = date("d-F-Y", strtotime($date1));
            $out['Date']['date2'] = date("d-F-Y", strtotime($date2));
        } else {
            $out['Date'] = "";
        }

        return $out;

    }

    public function getItemTotal($v, $k)
    {

        $list = [
            'tot_amount' => array_sum(array_column($v[$k], 'sales_total')),
            'tot_tax' => array_sum(array_column($v[$k], 'sales_tax')),
        ];

        return $list;

    }

    public function productinvoice($request)
    {
        $amnt = DB::raw('((salesub_rate) * salesub_qty) as sale_amount');
        $rate = DB::raw('(salesub_rate) as salesub_rate');

        $salesub_discount = DB::raw('((salesub_discount) * salesub_qty) as salesub_discount');

        $slct = ['salesub_sales_inv_no as inv_no', 'salesub_date', 'salesub_prod_id', 'salesub_qty', $rate,
            $amnt, 'products.prd_name', 'products.prd_barcode', 'categories.cat_name', 'branch_inv_no', 'van_inv_no', $salesub_discount];
        $qry = SalesSub::select($slct);
        $qry->join('products', 'products.prd_id', '=', 'sale_sub.salesub_prod_id');
        $qry->join('categories', 'categories.cat_id', '=', 'products.prd_cat_id');

        if ($this->branch_id > 0) {
            $qry = $qry->where('sale_sub.branch_id', $this->branch_id);
        } else {

            if ($this->active_branch_id == 'All') {

            } else {
                $qry = $qry->where('sale_sub.branch_id', $this->active_branch_id);
            }

        }

        $qry = $qry->where('salesub_flags', 1);
        $qry = $qry->where('sync_completed', 1);

        $inv_filter = $request['inv_filter'];
        $inv_val1 = $request['inv_val1'];
        $inv_val2 = $request['inv_val2'];

        if ($inv_filter && $inv_val1) {
            switch ($inv_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $qry = $qry->where('salesub_sales_inv_no', $inv_filter, $inv_val1);
                    break;
                case 'between':
                    if ($inv_val1) {
                        $qry = $qry->whereBetween('salesub_sales_inv_no', [$inv_val1, $inv_val2]);
                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }

        $rate_filter = $request['rate_filter'];
        $rate_val1 = $request['rate_val1'];
        $rate_val2 = $request['rate_val2'];

        if ($rate_filter && $rate_val1) {
            switch ($rate_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $qry = $qry->where('salesub_rate', $rate_filter, $rate_val1);
                    break;
                case 'between':
                    if ($rate_val1) {
                        $qry = $qry->whereBetween('salesub_rate', [$rate_val1, $rate_val2]);
                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }

        $qty_filter = $request['qty_filter'];
        $qty_val1 = $request['qty_val1'];
        $qty_val2 = $request['qty_val2'];

        if ($qty_filter && $qty_val1) {
            switch ($qty_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $qry = $qry->where('salesub_qty', $qty_filter, $qty_val1);
                    break;
                case 'between':
                    if ($qty_val1) {
                        $qry = $qry->whereBetween('salesub_qty', [$qty_val1, $qty_val2]);
                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }

        $barcode_filter = $request['barcode_filter'];
        $bar_val1 = $request['bar_val1'];
        $bar_val2 = $request['bar_val2'];

        if ($barcode_filter && $bar_val1) {
            switch ($barcode_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $qry = $qry->where('products.prd_barcode', $barcode_filter, $bar_val1);
                    break;
                case 'between':
                    if ($bar_val1) {
                        $qry = $qry->whereBetween('products.prd_barcode', [$bar_val1, $bar_val2]);
                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }

        if ($ptype = isset($request['period_type']) ? $request['period_type'] : 0) {

            list($date1, $date2) = $this->datesFromPeriods($ptype, $request);

            if ($date1 && $date2) {
                $qry = $qry->whereBetween('salesub_date', [$date1, $date2]);
            }
        }

        if ($request['prd_id'] != "") {
            $qry = $qry->where('products.prd_id', $request['prd_id']);
        }

        if ($request['added_by'] != "") {
            $qry = $qry->where('salesub_added_by', $request['added_by']);
        }

        if ($request['cat_id'] != "") {
            $qry = $qry->where('categories.cat_id', $request['cat_id']);
        }

        if ($request['branch_inv_no'] != "") {
            $qry = $qry->where('sales_branch_inv', $request['branch_inv_no']);
        }
        $total = clone $qry;
        // $tot = $qry->orderBy('categories.cat_name', 'ASC')->get();

        $total = $total->get()->toArray();

        $data = $qry->orderBy('salesub_sales_inv_no', 'ASC')->paginate($this->per_page)->toArray();
        if ($data['data']) {
            foreach ($data['data'] as $key => $value) {
                $out[$value['prd_name']][] = $value;
            }

            $i = 0;
            foreach ($out as $key => $val) {
                $result[$i]['prd_name'] = $key;
                $result[$i]['items'] = $val;
                $i++;
            }

            $data['data'] = $result;

            if ($request['period_type'] != "") {
                $data['Date']['date1'] = date("d-F-Y", strtotime($date1));
                $data['Date']['date2'] = date("d-F-Y", strtotime($date2));
            } else {
                $data['Date'] = "";
            }
            $data['total_amount'] = round(array_sum(array_column($total, 'sale_amount')), 2);
            $data['total_discount_amount'] = round(array_sum(array_column($total, 'salesub_discount')), 2);
            $data['total_qty_sold'] = round(array_sum(array_column($total, 'salesub_qty')), 2);
            return $data;
        } else {
            return [];
        }
    }

    public function productReport($request)
    {

        if ($request['rep_type'] == 2) { // Sold Report
            $amnt = DB::raw('SUM((salesub_rate) * salesub_qty) as sale_amount');
            $avgamnt = DB::raw('(SUM((salesub_rate) * salesub_qty) / SUM(salesub_qty) ) as sale_avg_amount');

            $rate = DB::raw('SUM(salesub_rate) as salesub_rate');
            $qty = DB::raw('SUM(salesub_qty) as salesub_qty');
            $avg_tax = DB::raw('AVG(salesub_tax_rate) as avg_tax');
            $tax = DB::raw('SUM(salesub_tax_rate) as tax_amt');

            $slct = ['salesub_sales_inv_no as inv_no', 'salesub_date', 'salesub_prod_id', $tax, $avg_tax, $qty, $rate, $avgamnt,
                $amnt, 'products.prd_name', 'subcategories.subcat_id', 'subcategories.subcat_name', 'products.prd_barcode', 'categories.cat_name', 'categories.cat_id', 'branch_inv_no', 'van_inv_no', 'salesub_discount'];
            $qry = SalesSub::select($slct);
            $qry->join('products', 'products.prd_id', '=', 'sale_sub.salesub_prod_id');
            $qry->join('categories', 'categories.cat_id', '=', 'products.prd_cat_id');
            $qry->leftjoin('subcategories', 'subcategories.subcat_id', '=', 'products.prd_sub_cat_id');

            if ($this->branch_id > 0) {
                $qry = $qry->where('sale_sub.branch_id', $this->branch_id);
            } else {

                if ($this->active_branch_id == 'All') {

                } else {
                    $qry = $qry->where('sale_sub.branch_id', $this->active_branch_id);
                }

            }

            $qry = $qry->where('salesub_flags', 1);
            $qry = $qry->where('sync_completed', 1);

            $inv_filter = $request['inv_filter'];
            $inv_val1 = $request['inv_val1'];
            $inv_val2 = $request['inv_val2'];

            if ($inv_filter && isset($inv_val1)) {
                switch ($inv_filter) {
                    case '<':
                    case '<=':
                    case '=':
                    case '>':
                    case '>=':
                        $qry = $qry->where('salesub_sales_inv_no', $inv_filter, $inv_val1);
                        break;
                    case 'between':
                        if ($inv_val1) {
                            $qry = $qry->whereBetween('salesub_sales_inv_no', [$inv_val1, $inv_val2]);
                        }
                        break;
                    default:
                        return parent::displayError('Invalid Request', $this->badrequest_stat);
                        break;
                }
            }

            $vat_filter = $request['vat_filter'];
            $vat_val1 = $request['vat_val1'];
            $vat_val2 = $request['vat_val2'];

            if ($vat_filter && isset($vat_val1)) {
                switch ($vat_filter) {
                    case '<':
                        $qry = $qry->where('salesub_tax_per', $vat_filter, $vat_val1);

                    case '<=':
                        $qry = $qry->where('salesub_tax_per', $vat_filter, $vat_val1);

                    case '=':
                        $qry = $qry->where('salesub_tax_per', $vat_filter, $vat_val1);

                    case '>':
                        $qry = $qry->where('salesub_tax_per', $vat_filter, $vat_val1);

                    case '>=':
                        $qry = $qry->where('salesub_tax_per', $vat_filter, $vat_val1);
                        break;
                    case 'between':
                        if ($vat_val1) {
                            $qry = $qry->whereBetween('salesub_tax_per', [$vat_val1, $vat_val2]);
                        }
                        break;
                    default:
                        return parent::displayError('Invalid Request', $this->badrequest_stat);
                        break;
                }
            }

            if ($ptype = isset($request['period_type']) ? $request['period_type'] : 0) {

                list($date1, $date2) = $this->datesFromPeriods($ptype, $request);

                if ($date1 && $date2) {
                    $qry = $qry->whereBetween('salesub_date', [$date1, $date2]);
                }
            }

            if ($request['prd_id'] != "") {
                $qry = $qry->where('products.prd_id', $request['prd_id']);
            }

            if ($request['prd_manufact_id'] != "") {
                $qry = $qry->where('products.prd_supplier', $request['prd_manufact_id']);
            }

            if (isset($request['gd_id']) && is_int($request['gd_id'])) {
                $qry = $qry->where('godown_id', $request['gd_id']);
            }

            if ($request['added_by'] != "") {
                $qry = $qry->where('salesub_added_by', $request['added_by']);
            }

            if ($request['cat_id'] != "") {
                $qry = $qry->where('categories.cat_id', $request['cat_id']);
            }

            if (is_int($request['sub_cat_id'])) {
                $qry = $qry->where('products.prd_sub_cat_id', $request['sub_cat_id']);
            }

            if ($request['branch_inv_no'] != "") {
                $qry = $qry->where('sales_branch_inv', $request['branch_inv_no']);
            }

            $tot = $qry->orderBy('categories.cat_name', 'ASC')->orderBy('subcategories.subcat_name', 'ASC')->groupby('salesub_branch_stock_id')->get()->toArray();

            $data = $qry->orderBy('categories.cat_name', 'ASC')->orderBy('subcategories.subcat_name', 'ASC')->groupby('salesub_branch_stock_id')->paginate($this->per_page)->toArray();
            //    return $data;
            if ($data['data']) {
                foreach ($data['data'] as $key => $value) {
                    $out[$value['prd_name']][] = $value;
                }

                $i = 0;
                foreach ($out as $key => $val) {
                    $result[$i]['prd_name'] = $key;
                    $result[$i]['items'] = $val;
                    $i++;
                }

                $data['tot_inv_amt'] = array_sum(array_column($tot, 'sale_amount'));
                $data['total_avg_vat_amt'] = array_sum(array_column($tot, 'avg_tax'));
                $data['total_vat_amt'] = array_sum(array_column($tot, 'tax_amt'));

                $data['data'] = $result;

                if ($request['period_type'] != "") {
                    $data['Date']['date1'] = date("d-F-Y", strtotime($date1));
                    $data['Date']['date2'] = date("d-F-Y", strtotime($date2));
                } else {
                    $data['Date'] = "";
                }

                if ($request['rep_type'] == 2) {
                    $data['reportType'] = 'Sold Product Report';
                } elseif ($request['rep_type'] == 3) {
                    $data['reportType'] = 'Returned Product Report';
                } elseif ($request['rep_type'] == 1) {
                    $data['reportType'] = 'Sold & Returne Product Report';
                }

                return $data;
            } else {
                return [];
            }

        } elseif ($request['rep_type'] == 3) { // Retun Report

            $amnt = DB::raw('SUM((salesretsub_rate) * salesretsub_qty) as sale_amount');
            $avgamnt = DB::raw('(SUM((salesretsub_rate) * salesretsub_qty) / SUM(salesretsub_qty) ) as sale_avg_amount');

            $rate = DB::raw('SUM(salesretsub_rate) as salesub_rate');
            $qty = DB::raw('SUM(salesretsub_qty) as salesub_qty');
            $avg_tax = DB::raw('AVG(salesretsub_tax_rate) as avg_tax');
            $tax = DB::raw('SUM(salesretsub_tax_rate) as tax_amt');

            $slct = ['salesretsub_sales_inv_no as inv_no', 'salesretsub_date as salesub_date', 'salesretsub_prod_id', $tax, $avg_tax, $qty, $rate, $avgamnt,
                $amnt, 'products.prd_name', 'subcategories.subcat_id', 'subcategories.subcat_name', 'products.prd_barcode', 'categories.cat_name', 'categories.cat_id', 'salesret_sales_branch_inv_no'];
            $qry = SalesReturnSub::select($slct);
            $qry->join('products', 'products.prd_id', '=', 'sales_return_sub.salesretsub_prod_id');
            $qry->join('categories', 'categories.cat_id', '=', 'products.prd_cat_id');
            $qry->join('sales_return_master', 'sales_return_master.salesret_no', '=', 'sales_return_sub.salesretsub_salesret_no');
            $qry->leftjoin('subcategories', 'subcategories.subcat_id', '=', 'products.prd_sub_cat_id');

            if ($this->branch_id > 0) {
                $qry = $qry->where('sales_return_sub.branch_id', $this->branch_id);
            }

            $qry = $qry->where('salesretsub_flags', 1);
            $qry = $qry->where('sales_return_sub.sync_completed', 1);

            $inv_filter = $request['inv_filter'];
            $inv_val1 = $request['inv_val1'];
            $inv_val2 = $request['inv_val2'];

            if ($inv_filter && isset($inv_val1)) {
                switch ($inv_filter) {
                    case '<':
                    case '<=':
                    case '=':
                    case '>':
                    case '>=':
                        $qry = $qry->where('salesretsub_sales_inv_no', $inv_filter, $inv_val1);
                        break;
                    case 'between':
                        if ($inv_val1) {
                            $qry = $qry->whereBetween('salesretsub_sales_inv_no', [$inv_val1, $inv_val2]);
                        }
                        break;
                    default:
                        return parent::displayError('Invalid Request', $this->badrequest_stat);
                        break;
                }
            }

            $vat_filter = $request['vat_filter'];
            $vat_val1 = $request['vat_val1'];
            $vat_val2 = $request['vat_val2'];

            if ($vat_filter && isset($vat_val1)) {
                switch ($vat_filter) {
                    case '<':
                        $qry = $qry->where('salesretsub_tax_per', $vat_filter, $vat_val1);

                    case '<=':
                        $qry = $qry->where('salesretsub_tax_per', $vat_filter, $vat_val1);

                    case '=':
                        $qry = $qry->where('salesretsub_tax_per', $vat_filter, $vat_val1);

                    case '>':
                        $qry = $qry->where('salesretsub_tax_per', $vat_filter, $vat_val1);

                    case '>=':
                        $qry = $qry->where('salesretsub_tax_per', $vat_filter, $vat_val1);
                        break;
                    case 'between':
                        if ($vat_val1) {
                            $qry = $qry->whereBetween('salesretsub_tax_per', [$vat_val1, $vat_val2]);
                        }
                        break;
                    default:
                        return parent::displayError('Invalid Request', $this->badrequest_stat);
                        break;
                }
            }

            if ($ptype = isset($request['period_type']) ? $request['period_type'] : 0) {

                list($date1, $date2) = $this->datesFromPeriods($ptype, $request);

                if ($date1 && $date2) {
                    $qry = $qry->whereBetween('salesretsub_date', [$date1, $date2]);
                }
            }

            if ($request['prd_id'] != "") {
                $qry = $qry->where('products.prd_id', $request['prd_id']);
            }

            if ($request['prd_manufact_id'] != "") {
                $qry = $qry->where('products.prd_supplier', $request['prd_manufact_id']);
            }

            if (isset($request['gd_id']) && is_int($request['gd_id'])) {
                $qry = $qry->where('salesretsub_godown_id', $request['gd_id']);
            }

            if ($request['added_by'] != "") {
                $qry = $qry->where('salesub_added_by', $request['added_by']);
            }

            if ($request['cat_id'] != "") {
                $qry = $qry->where('categories.cat_id', $request['cat_id']);
            }
            if (is_int($request['sub_cat_id'])) {
                $qry = $qry->where('products.prd_sub_cat_id', $request['sub_cat_id']);
            }
            if ($request['branch_inv_no'] != "") {
                $qry = $qry->where('sales_return_master.salesret_sales_branch_inv_no', $request['branch_inv_no']);
            }

            $tot = $qry->orderBy('salesretsub_sales_inv_no', 'ASC')->orderBy('subcategories.subcat_name', 'ASC')->groupby('salesretsub_branch_stock_id')->get()->toArray();

            $data = $qry->orderBy('salesretsub_sales_inv_no', 'ASC')->orderBy('subcategories.subcat_name', 'ASC')->groupby('salesretsub_branch_stock_id')->paginate($this->per_page)->toArray();
            // return $data;
            if ($data['data']) {
                foreach ($data['data'] as $key => $value) {
                    $out[$value['prd_name']][] = $value;
                }

                $i = 0;
                foreach ($out as $key => $val) {
                    $result[$i]['prd_name'] = $key;
                    $result[$i]['items'] = $val;
                    $i++;
                }

                $data['tot_inv_amt'] = array_sum(array_column($tot, 'sale_amount'));
                $data['total_avg_vat_amt'] = array_sum(array_column($tot, 'avg_tax'));
                $data['total_vat_amt'] = array_sum(array_column($tot, 'tax_amt'));

                $data['data'] = $result;

                if ($request['period_type'] != "") {
                    $data['Date']['date1'] = date("d-F-Y", strtotime($date1));
                    $data['Date']['date2'] = date("d-F-Y", strtotime($date2));
                } else {
                    $data['Date'] = "";
                }

                if ($request['rep_type'] == 2) {
                    $data['reportType'] = 'Sold Product Report';
                } elseif ($request['rep_type'] == 3) {
                    $data['reportType'] = 'Returned Product Report';
                } elseif ($request['rep_type'] == 1) {
                    $data['reportType'] = 'Sold & Returne Product Report';
                }

                return $data;
            } else {
                return [];
            }

        } elseif ($request['rep_type'] == 1) { // Sales & Retun Report

            $retids = SalesReturnSub::where('salesretsub_flags', 1)->where('sync_completed', 1)->groupby('salesretsub_branch_stock_id')->pluck('salesretsub_branch_stock_id')->toArray();
            $salids = SalesSub::where('salesub_flags', 1)->where('sync_completed', 1)->groupby('salesub_branch_stock_id')->pluck('salesub_branch_stock_id')->toArray();
            $allids = array_merge($salids, $retids);
            $bsIds = array_unique($allids);

            $avgamnt = DB::raw('(SUM((IFNULL(salesretsub_rate,0) * IFNULL(salesretsub_qty,0)) - ( IFNULL(salesub_rate,0) * IFNULL(salesub_qty,0))) / SUM(IFNULL(salesretsub_qty,0) - IFNULL(salesub_qty,0))) as sale_avg_amount');

            $rate = DB::raw('SUM(IFNULL(salesub_rate,0) - IFNULL(salesretsub_rate,0)) as salesub_rate');
            $qty = DB::raw('SUM(IFNULL(salesub_qty,0) - IFNULL(salesretsub_qty,0    )) as salesub_qty');
            $avg_tax = DB::raw('AVG(IFNULL(salesub_tax_rate,0)) as avg_tax');
            $tax = DB::raw('SUM(IFNULL(salesub_tax_rate,0)) as tax_amt');

            $slct = ['products.prd_name', 'products.prd_barcode', 'categories.cat_name', 'branch_stocks.branch_stock_id', 'subcategories.subcat_id', 'subcategories.subcat_name'];

            $rltn = [

                'sales_sub' => function ($qr) use ($request) {
                    $sqty = DB::raw('SUM(IFNULL(salesub_qty,0)) as s_qty');
                    $amnt = DB::raw('SUM(IFNULL(salesub_rate,0) *  IFNULL(salesub_qty,0)) as sale_amount');

                    $avgamnt = DB::raw('(SUM((salesub_rate) * salesub_qty) / SUM(salesub_qty) ) as sale_avg_amount');
                    $avg_tax = DB::raw('AVG(salesub_tax_rate) as avg_tax');
                    $tax = DB::raw('SUM(salesub_tax_rate) as tax_amt');

                    $vat_filter = $request['vat_filter'];
                    $vat_val1 = $request['vat_val1'];
                    $vat_val2 = $request['vat_val2'];

                    $inv_filter = $request['inv_filter'];
                    $inv_val1 = $request['inv_val1'];
                    $inv_val2 = $request['inv_val2'];

                    $qr->select('salesub_branch_stock_id', $sqty, $amnt, $tax, $avg_tax, $avgamnt);
                    $qr->where('salesub_flags', 1);
                    $qr->where('sale_sub.sync_completed', 1);

                    if ($inv_filter && isset($inv_val1)) {
                        switch ($inv_filter) {
                            case '<':
                            case '<=':
                            case '=':
                            case '>':
                            case '>=':
                                $qr->where('salesub_sales_inv_no', $inv_filter, $inv_val1);
                                break;
                            case 'between':
                                if ($inv_val1) {
                                    $qr->whereBetween('salesub_sales_inv_no', [$inv_val1, $inv_val2]);
                                }
                                break;
                            default:
                                return parent::displayError('Invalid Request', $this->badrequest_stat);
                                break;
                        }
                    }

                    if ($request['added_by'] != "") {
                        $qr->where('salesub_added_by', $request['added_by']);
                    }

                    if (isset($request['gd_id']) && is_int($request['gd_id'])) {
                        $qr->where('godown_id', $request['gd_id']);
                    }

                    if ($vat_filter && isset($vat_val1)) {
                        switch ($vat_filter) {
                            case '<':
                                $qr->where('salesub_tax_per', $vat_filter, $vat_val1);

                            case '<=':
                                $qr->where('salesub_tax_per', $vat_filter, $vat_val1);

                            case '=':
                                $qr->where('salesub_tax_per', $vat_filter, $vat_val1);

                            case '>':
                                $qr->where('salesub_tax_per', $vat_filter, $vat_val1);

                            case '>=':
                                $qr->where('salesub_tax_per', $vat_filter, $vat_val1);
                                break;
                            case 'between':
                                if ($vat_val1) {
                                    $qr->whereBetween('salesub_tax_per', [$vat_val1, $vat_val2]);
                                }
                                break;
                            default:
                                return parent::displayError('Invalid Request', $this->badrequest_stat);
                                break;
                        }
                    }

                    if ($ptype = isset($request['period_type']) ? $request['period_type'] : 0) {

                        list($date1, $date2) = $this->datesFromPeriods($ptype, $request);

                        if ($date1 && $date2) {
                            $qr->whereBetween('salesub_date', [$date1, $date2]);
                        }
                    }

                    $qr->groupby('salesub_branch_stock_id');

                },
                'sales_return_sub' => function ($qr) use ($request) {
                    $sretqty = DB::raw('SUM(IFNULL(salesretsub_qty,0)) as ret_qty');
                    $retamnt = DB::raw('SUM(IFNULL(salesretsub_rate,0) *  IFNULL(salesretsub_qty,0)) as sale_amount');
                    $avgamnt = DB::raw('(SUM((salesretsub_rate) * salesretsub_qty) / SUM(salesretsub_qty) ) as sale_avg_amount');
                    $avg_tax = DB::raw('AVG(salesretsub_tax_rate) as avg_tax');
                    $tax = DB::raw('SUM(salesretsub_tax_rate) as tax_amt');

                    $inv_filter = $request['inv_filter'];
                    $inv_val1 = $request['inv_val1'];
                    $inv_val2 = $request['inv_val2'];

                    $vat_filter = $request['vat_filter'];
                    $vat_val1 = $request['vat_val1'];
                    $vat_val2 = $request['vat_val2'];

                    $qr->select('salesretsub_branch_stock_id', $sretqty, $retamnt, $tax, $avg_tax, $avgamnt);
                    $qr->join('sales_return_master', 'sales_return_master.salesret_no', '=', 'sales_return_sub.salesretsub_salesret_no');

                    $qr->where('salesretsub_flags', 1);
                    $qr->where('sales_return_sub.sync_completed', 1);

                    if ($inv_filter && isset($inv_val1)) {
                        switch ($inv_filter) {
                            case '<':
                            case '<=':
                            case '=':
                            case '>':
                            case '>=':
                                $qr->where('salesretsub_sales_inv_no', $inv_filter, $inv_val1);
                                break;
                            case 'between':
                                if ($inv_val1) {
                                    $qr->whereBetween('salesretsub_sales_inv_no', [$inv_val1, $inv_val2]);
                                }
                                break;
                            default:
                                return parent::displayError('Invalid Request', $this->badrequest_stat);
                                break;
                        }
                    }

                    if ($vat_filter && isset($vat_val1)) {
                        // print_r('sdf');

                        switch ($vat_filter) {
                            case '<':
                                $qr->where('salesretsub_tax_per', $vat_filter, $vat_val1);

                            case '<=':
                                $qr->where('salesretsub_tax_per', $vat_filter, $vat_val1);

                            case '=':
                                $qr->where('salesretsub_tax_per', $vat_filter, $vat_val1);

                            case '>':
                                $qr->where('salesretsub_tax_per', $vat_filter, $vat_val1);

                            case '>=':
                                $qr->where('salesretsub_tax_per', $vat_filter, $vat_val1);
                                break;
                            case 'between':
                                if ($vat_val1) {
                                    $qr->whereBetween('salesretsub_tax_per', [$vat_val1, $vat_val2]);
                                }
                                break;
                            default:
                                return parent::displayError('Invalid Request', $this->badrequest_stat);
                                break;
                        }
                    }

                    if ($ptype = isset($request['period_type']) ? $request['period_type'] : 0) {

                        list($date1, $date2) = $this->datesFromPeriods($ptype, $request);

                        if ($date1 && $date2) {
                            $qr->whereBetween('salesretsub_date', [$date1, $date2]);
                        }
                    }

                    if (isset($request['gd_id']) && is_int($request['gd_id'])) {
                        $qr->where('salesretsub_godown_id', $request['gd_id']);
                    }

                    if ($request['added_by'] != "") {
                        $qr->where('salesret_added_by', $request['added_by']);
                    }

                    if ($request['cat_id'] != "") {
                        $qr->where('categories.cat_id', $request['cat_id']);
                    }
                    if (is_int($request['sub_cat_id'])) {
                        $qr->where('products.prd_sub_cat_id', $request['sub_cat_id']);
                    }

                    $qr->groupby('salesretsub_branch_stock_id');

                },

            ];

            $qry = BranchStocks::select($slct);

            $qry->join('products', 'products.prd_id', '=', 'branch_stocks.bs_prd_id');
            $qry->join('categories', 'categories.cat_id', '=', 'products.prd_cat_id');
            $qry->leftjoin('subcategories', 'subcategories.subcat_id', '=', 'products.prd_sub_cat_id');

            if ($this->branch_id > 0) {
                $qry = $qry->where('branch_stocks.bs_branch_id', $this->branch_id);
            }

            $qry = $qry->whereIn('branch_stocks.branch_stock_id', $bsIds);

            if ($request['prd_id'] != "") {
                $qry = $qry->where('products.prd_id', $request['prd_id']);
            }
            if ($request['cat_id'] != "") {
                $qry = $qry->where('categories.cat_id', $request['cat_id']);
            }
            if (is_int($request['sub_cat_id'])) {
                $qry = $qry->where('products.prd_sub_cat_id', $request['sub_cat_id']);
            }
            if ($request['prd_manufact_id'] != "") {
                $qry = $qry->where('products.prd_supplier', $request['prd_manufact_id']);
            }

            $tot = $qry->with($rltn)->orderBy('products.prd_id', 'ASC')->orderBy('subcategories.subcat_name', 'ASC')->groupby('products.prd_id')->get()->toArray();

            $data = $qry->with($rltn)->orderBy('products.prd_id', 'ASC')->orderBy('subcategories.subcat_name', 'ASC')->groupby('products.prd_id')->paginate($this->per_page)->toArray();
            // return $data;
            if ($data['data']) {
                foreach ($data['data'] as $key => $value) {
                    $out[$value['prd_name']][] = $value;
                }

                $i = 0;
                foreach ($out as $key => $val) {
                    $result[$i]['prd_name'] = $key;
                    $result[$i]['items'] = $val;
                    if (count($val[0]['sales_sub']) > 0) {
                        $result[$i]['items'][0]['saleqty'] = $val[0]['sales_sub'][0]['s_qty'];
                        $result[$i]['items'][0]['sales_amt'] = $val[0]['sales_sub'][0]['sale_amount'];
                        $result[$i]['items'][0]['sold_tax_amt'] = $val[0]['sales_sub'][0]['tax_amt'];
                        $result[$i]['items'][0]['sold_avg_tax'] = $val[0]['sales_sub'][0]['avg_tax'];
                        $result[$i]['items'][0]['sold_sale_avg_amount'] = $val[0]['sales_sub'][0]['sale_avg_amount'];

                    } else {
                        $result[$i]['items'][0]['saleqty'] = 0;
                        $result[$i]['items'][0]['sales_amt'] = 0;
                        $result[$i]['items'][0]['sold_tax_amt'] = 0;
                        $result[$i]['items'][0]['sold_avg_tax'] = 0;
                        $result[$i]['items'][0]['sold_sale_avg_amount'] = 0;
                    }
                    if (count($val[0]['sales_return_sub']) > 0) {
                        $result[$i]['items'][0]['saleretqty'] = $val[0]['sales_return_sub'][0]['ret_qty'];
                        $result[$i]['items'][0]['salesret_amt'] = $val[0]['sales_return_sub'][0]['sale_amount'];
                        $result[$i]['items'][0]['ret_tax_amt'] = $val[0]['sales_return_sub'][0]['tax_amt'];
                        $result[$i]['items'][0]['ret_avg_tax'] = $val[0]['sales_return_sub'][0]['avg_tax'];
                        $result[$i]['items'][0]['ret_sale_avg_amount'] = $val[0]['sales_return_sub'][0]['sale_avg_amount'];
                    } else {
                        $result[$i]['items'][0]['saleretqty'] = 0;
                        $result[$i]['items'][0]['salesret_amt'] = 0;
                        $result[$i]['items'][0]['ret_tax_amt'] = 0;
                        $result[$i]['items'][0]['ret_avg_tax'] = 0;
                        $result[$i]['items'][0]['ret_sale_avg_amount'] = 0;
                    }

                    $result[$i]['items'][0]['salesub_qty'] = $result[$i]['items'][0]['saleqty'] - $result[$i]['items'][0]['saleretqty'];
                    $result[$i]['items'][0]['sale_amount'] = $result[$i]['items'][0]['sales_amt'] - $result[$i]['items'][0]['salesret_amt'];
                    $result[$i]['items'][0]['tax_amt'] = $result[$i]['items'][0]['sold_tax_amt'] - $result[$i]['items'][0]['ret_tax_amt'];
                    $result[$i]['items'][0]['avg_tax'] = $result[$i]['items'][0]['tax_amt'] / ($result[$i]['items'][0]['salesub_qty'] ? $result[$i]['items'][0]['salesub_qty'] : 1);
                    $result[$i]['items'][0]['sale_avg_amount'] = $result[$i]['items'][0]['sale_amount'] / ($result[$i]['items'][0]['salesub_qty'] ? $result[$i]['items'][0]['salesub_qty'] : 1);

                    $i++;
                }

                // $data['tot_inv_amt'] = array_sum(array_column($tot, 'sale_amount'));
                // $data['total_avg_vat_amt'] = array_sum(array_column($tot, 'avg_tax'));
                // $data['total_vat_amt'] = array_sum(array_column($tot, 'tax_amt'));

                unset($data['data']);

                $data['data'] = $result;

                if ($ptype = isset($request['period_type']) ? $request['period_type'] : 0) {
                    list($date1, $date2) = $this->datesFromPeriods($ptype, $request);
                }

                if ($request['period_type'] != "") {
                    $data['Date']['date1'] = date("d-F-Y", strtotime($date1));
                    $data['Date']['date2'] = date("d-F-Y", strtotime($date2));
                } else {
                    $data['Date'] = "";
                }

                if ($request['rep_type'] == 2) {
                    $data['reportType'] = 'Sold Product Report';
                } elseif ($request['rep_type'] == 3) {
                    $data['reportType'] = 'Returned Product Report';
                } elseif ($request['rep_type'] == 1) {
                    $data['reportType'] = 'Sold & Returne Product Report';
                }

                return $data;
            } else {
                return [];
            }

        }

    }

    public function getsaleReturnTotal($request)
    { // To Get Sales & Retun Report Total

        $retids = SalesReturnSub::where('salesretsub_flags', 1)->where('sync_completed', 1)->groupby('salesretsub_branch_stock_id')->pluck('salesretsub_branch_stock_id')->toArray();
        $salids = SalesSub::where('salesub_flags', 1)->where('sync_completed', 1)->groupby('salesub_branch_stock_id')->pluck('salesub_branch_stock_id')->toArray();
        $allids = array_merge($salids, $retids);
        $bsIds = array_unique($allids);

        $avgamnt = DB::raw('(SUM((IFNULL(salesretsub_rate,0) * IFNULL(salesretsub_qty,0)) - ( IFNULL(salesub_rate,0) * IFNULL(salesub_qty,0))) / SUM(IFNULL(salesretsub_qty,0) - IFNULL(salesub_qty,0))) as sale_avg_amount');

        $rate = DB::raw('SUM(IFNULL(salesub_rate,0) - IFNULL(salesretsub_rate,0)) as salesub_rate');
        $qty = DB::raw('SUM(IFNULL(salesub_qty,0) - IFNULL(salesretsub_qty,0    )) as salesub_qty');
        $avg_tax = DB::raw('AVG(IFNULL(salesub_tax_rate,0)) as avg_tax');
        $tax = DB::raw('SUM(IFNULL(salesub_tax_rate,0)) as tax_amt');

        $slct = ['products.prd_name', 'products.prd_barcode', 'categories.cat_name', 'branch_stocks.branch_stock_id', 'subcategories.subcat_id', 'subcategories.subcat_name'];

        $rltn = [

            'sales_sub' => function ($qr) use ($request) {
                $sqty = DB::raw('SUM(IFNULL(salesub_qty,0)) as s_qty');
                $amnt = DB::raw('SUM(IFNULL(salesub_rate,0) *  IFNULL(salesub_qty,0)) as sale_amount');

                $avgamnt = DB::raw('(SUM((salesub_rate) * salesub_qty) / SUM(salesub_qty) ) as sale_avg_amount');
                $avg_tax = DB::raw('AVG(salesub_tax_rate) as avg_tax');
                $tax = DB::raw('SUM(salesub_tax_rate) as tax_amt');

                $vat_filter = $request['vat_filter'];
                $vat_val1 = $request['vat_val1'];
                $vat_val2 = $request['vat_val2'];

                $inv_filter = $request['inv_filter'];
                $inv_val1 = $request['inv_val1'];
                $inv_val2 = $request['inv_val2'];

                $qr->select('salesub_branch_stock_id', $sqty, $amnt, $tax, $avg_tax, $avgamnt);
                $qr->where('salesub_flags', 1);
                $qr->where('sale_sub.sync_completed', 1);

                if ($inv_filter && isset($inv_val1)) {
                    switch ($inv_filter) {
                        case '<':
                        case '<=':
                        case '=':
                        case '>':
                        case '>=':
                            $qr->where('salesub_sales_inv_no', $inv_filter, $inv_val1);
                            break;
                        case 'between':
                            if ($inv_val1) {
                                $qr->whereBetween('salesub_sales_inv_no', [$inv_val1, $inv_val2]);
                            }
                            break;
                        default:
                            return parent::displayError('Invalid Request', $this->badrequest_stat);
                            break;
                    }
                }

                if ($request['added_by'] != "") {
                    $qr->where('salesub_added_by', $request['added_by']);
                }

                if (isset($request['gd_id']) && is_int($request['gd_id'])) {
                    $qr->where('godown_id', $request['gd_id']);
                }

                if ($vat_filter && isset($vat_val1)) {
                    switch ($vat_filter) {
                        case '<':
                            $qr->where('salesub_tax_per', $vat_filter, $vat_val1);

                        case '<=':
                            $qr->where('salesub_tax_per', $vat_filter, $vat_val1);

                        case '=':
                            $qr->where('salesub_tax_per', $vat_filter, $vat_val1);

                        case '>':
                            $qr->where('salesub_tax_per', $vat_filter, $vat_val1);

                        case '>=':
                            $qr->where('salesub_tax_per', $vat_filter, $vat_val1);
                            break;
                        case 'between':
                            if ($vat_val1) {
                                $qr->whereBetween('salesub_tax_per', [$vat_val1, $vat_val2]);
                            }
                            break;
                        default:
                            return parent::displayError('Invalid Request', $this->badrequest_stat);
                            break;
                    }
                }

                if ($ptype = isset($request['period_type']) ? $request['period_type'] : 0) {

                    list($date1, $date2) = $this->datesFromPeriods($ptype, $request);

                    if ($date1 && $date2) {
                        $qr->whereBetween('salesub_date', [$date1, $date2]);
                    }
                }

                $qr->groupby('salesub_branch_stock_id');

            },
            'sales_return_sub' => function ($qr) use ($request) {
                $sretqty = DB::raw('SUM(IFNULL(salesretsub_qty,0)) as ret_qty');
                $retamnt = DB::raw('SUM(IFNULL(salesretsub_rate,0) *  IFNULL(salesretsub_qty,0)) as sale_amount');
                $avgamnt = DB::raw('(SUM((salesretsub_rate) * salesretsub_qty) / SUM(salesretsub_qty) ) as sale_avg_amount');
                $avg_tax = DB::raw('AVG(salesretsub_tax_rate) as avg_tax');
                $tax = DB::raw('SUM(salesretsub_tax_rate) as tax_amt');

                $inv_filter = $request['inv_filter'];
                $inv_val1 = $request['inv_val1'];
                $inv_val2 = $request['inv_val2'];

                $vat_filter = $request['vat_filter'];
                $vat_val1 = $request['vat_val1'];
                $vat_val2 = $request['vat_val2'];

                $qr->select('salesretsub_branch_stock_id', $sretqty, $retamnt, $tax, $avg_tax, $avgamnt);
                $qr->join('sales_return_master', 'sales_return_master.salesret_no', '=', 'sales_return_sub.salesretsub_salesret_no');

                $qr->where('salesretsub_flags', 1);
                $qr->where('sales_return_sub.sync_completed', 1);

                if ($inv_filter && isset($inv_val1)) {
                    switch ($inv_filter) {
                        case '<':
                        case '<=':
                        case '=':
                        case '>':
                        case '>=':
                            $qr->where('salesretsub_sales_inv_no', $inv_filter, $inv_val1);
                            break;
                        case 'between':
                            if ($inv_val1) {
                                $qr->whereBetween('salesretsub_sales_inv_no', [$inv_val1, $inv_val2]);
                            }
                            break;
                        default:
                            return parent::displayError('Invalid Request', $this->badrequest_stat);
                            break;
                    }
                }

                if ($vat_filter && isset($vat_val1)) {
                    // print_r('sdf');

                    switch ($vat_filter) {
                        case '<':
                            $qr->where('salesretsub_tax_per', $vat_filter, $vat_val1);

                        case '<=':
                            $qr->where('salesretsub_tax_per', $vat_filter, $vat_val1);

                        case '=':
                            $qr->where('salesretsub_tax_per', $vat_filter, $vat_val1);

                        case '>':
                            $qr->where('salesretsub_tax_per', $vat_filter, $vat_val1);

                        case '>=':
                            $qr->where('salesretsub_tax_per', $vat_filter, $vat_val1);
                            break;
                        case 'between':
                            if ($vat_val1) {
                                $qr->whereBetween('salesretsub_tax_per', [$vat_val1, $vat_val2]);
                            }
                            break;
                        default:
                            return parent::displayError('Invalid Request', $this->badrequest_stat);
                            break;
                    }
                }

                if ($ptype = isset($request['period_type']) ? $request['period_type'] : 0) {

                    list($date1, $date2) = $this->datesFromPeriods($ptype, $request);

                    if ($date1 && $date2) {
                        $qr->whereBetween('salesretsub_date', [$date1, $date2]);
                    }
                }

                if (isset($request['gd_id']) && is_int($request['gd_id'])) {
                    $qr->where('salesretsub_godown_id', $request['gd_id']);
                }

                if ($request['added_by'] != "") {
                    $qr->where('salesret_added_by', $request['added_by']);
                }

                if ($request['cat_id'] != "") {
                    $qr->where('categories.cat_id', $request['cat_id']);
                }
                if (is_int($request['sub_cat_id'])) {
                    $qr->where('products.prd_sub_cat_id', $request['sub_cat_id']);
                }

                $qr->groupby('salesretsub_branch_stock_id');

            },

        ];

        $qry = BranchStocks::select($slct);

        $qry->join('products', 'products.prd_id', '=', 'branch_stocks.bs_prd_id');
        $qry->join('categories', 'categories.cat_id', '=', 'products.prd_cat_id');
        $qry->join('subcategories', 'subcategories.subcat_id', '=', 'products.prd_sub_cat_id');

        if ($this->branch_id > 0) {
            $qry = $qry->where('branch_stocks.bs_branch_id', $this->branch_id);
        }

        $qry = $qry->whereIn('branch_stocks.branch_stock_id', $bsIds);

        if ($request['prd_id'] != "") {
            $qry = $qry->where('products.prd_id', $request['prd_id']);
        }
        if ($request['cat_id'] != "") {
            $qry = $qry->where('categories.cat_id', $request['cat_id']);
        }
        if (is_int($request['sub_cat_id'])) {
            $qry = $qry->where('products.prd_sub_cat_id', $request['sub_cat_id']);
        }

        if ($request['prd_manufact_id'] != "") {
            $qry = $qry->where('products.prd_supplier', $request['prd_manufact_id']);
        }

        $tot = $qry->with($rltn)->orderBy('products.prd_id', 'ASC')->groupby('products.prd_id')->get()->toArray();

        //  $data = $qry->with($rltn)->orderBy('products.prd_id', 'ASC')->groupby('products.prd_id')->paginate($this->per_page)->toArray();
        //  return $tot;
        if ($tot) {
            //  foreach ($data['data'] as $key => $value) {
            //      $out[$value['prd_name']][] = $value;
            //  }

            $i = 0;
            foreach ($tot as $key => $val) {
                //  $result[$i]['prd_name'] = $key;
                //  $result[$i]['items'] = $val;
                if (count($val['sales_sub']) > 0) {
                    $tot[$i]['saleqty'] = $val['sales_sub'][0]['s_qty'];
                    $tot[$i]['sales_amt'] = $val['sales_sub'][0]['sale_amount'];
                    $tot[$i]['sold_tax_amt'] = $val['sales_sub'][0]['tax_amt'];
                    $tot[$i]['sold_avg_tax'] = $val['sales_sub'][0]['avg_tax'];
                    $tot[$i]['sold_sale_avg_amount'] = $val['sales_sub'][0]['sale_avg_amount'];

                } else {
                    $tot[$i]['saleqty'] = 0;
                    $tot[$i]['sales_amt'] = 0;
                    $tot[$i]['sold_tax_amt'] = 0;
                    $tot[$i]['sold_avg_tax'] = 0;
                    $tot[$i]['sold_sale_avg_amount'] = 0;
                }
                if (count($val['sales_return_sub']) > 0) {
                    $tot[$i]['saleretqty'] = $val['sales_return_sub'][0]['ret_qty'];
                    $tot[$i]['salesret_amt'] = $val['sales_return_sub'][0]['sale_amount'];
                    $tot[$i]['ret_tax_amt'] = $val['sales_return_sub'][0]['tax_amt'];
                    $tot[$i]['ret_avg_tax'] = $val['sales_return_sub'][0]['avg_tax'];
                    $tot[$i]['ret_sale_avg_amount'] = $val['sales_return_sub'][0]['sale_avg_amount'];
                } else {
                    $tot[$i]['saleretqty'] = 0;
                    $tot[$i]['salesret_amt'] = 0;
                    $tot[$i]['ret_tax_amt'] = 0;
                    $tot[$i]['ret_avg_tax'] = 0;
                    $tot[$i]['ret_sale_avg_amount'] = 0;
                }

                $tot[$i]['salesub_qty'] = $tot[$i]['saleqty'] - $tot[$i]['saleretqty'];
                $tot[$i]['sale_amount'] = $tot[$i]['sales_amt'] - $tot[$i]['salesret_amt'];
                $tot[$i]['tax_amt'] = $tot[$i]['sold_tax_amt'] - $tot[$i]['ret_tax_amt'];
                $tot[$i]['avg_tax'] = $tot[$i]['tax_amt'] / ($tot[$i]['salesub_qty'] ? $tot[$i]['salesub_qty'] : 1);
                $tot[$i]['sale_avg_amount'] = $tot[$i]['sale_amount'] / ($tot[$i]['salesub_qty'] ? $tot[$i]['salesub_qty'] : 1);

                $i++;
            }

            $data['tot_inv_amt'] = array_sum(array_column($tot, 'sale_amount'));
            $data['total_avg_vat_amt'] = array_sum(array_column($tot, 'avg_tax'));
            $data['total_vat_amt'] = array_sum(array_column($tot, 'tax_amt'));

            //  unset($data['data']);

            $data['data'] = $tot;

            if ($ptype = isset($request['period_type']) ? $request['period_type'] : 0) {
                list($date1, $date2) = $this->datesFromPeriods($ptype, $request);
            }

            if ($request['period_type'] != "") {
                $data['Date']['date1'] = date("d-F-Y", strtotime($date1));
                $data['Date']['date2'] = date("d-F-Y", strtotime($date2));
            } else {
                $data['Date'] = "";
            }

            if ($request['rep_type'] == 2) {
                $data['reportType'] = 'Sold Product Report';
            } elseif ($request['rep_type'] == 3) {
                $data['reportType'] = 'Returned Product Report';
            } elseif ($request['rep_type'] == 1) {
                $data['reportType'] = 'Sold & Returne Product Report';
            }

            return $data;
        } else {
            return [];
        }

    }

    public function test_array_qry()
    {

        $res = DB::select("SELECT salesub_sales_inv_no,branch_inv_no,
       JSON_OBJECTAGG('inv',salesub_sales_inv_no) inv_no,
       JSON_ARRAYAGG(salesub_rate) rate , JSON_ARRAYAGG(salesub_qty) qty,salesub_date FROM erp_sale_sub
       GROUP BY salesub_prod_id");

        return $res;
    }

    public function saleInvDetail($request)
    {

        $rltn = [
            'sales_sub' => function ($qr) {

                $amnt = DB::raw('(salesub_rate * salesub_qty) as sale_amount');
                $totalAmt = DB::raw('(salesub_rate + salesub_tax_rate) * salesub_qty as sale_total_amount ');
                $salesTax = DB::raw('(salesub_tax_rate * salesub_qty) as salesub_tax_rate');
                $qr->select('sale_sub.*', 'products.prd_barcode', 'salesub_sales_inv_no', 'salesub_rate', 'salesub_qty', 'salesub_discount', $salesTax
                    , 'salesub_tax_per', $amnt, $totalAmt, 'products.prd_name', 'units.unit_display', 'salesub_unit_id')
                    ->join('products', 'prd_id', 'sale_sub.salesub_prod_id')
                    ->join('units', 'unit_id', 'sale_sub.salesub_unit_id')

                    ->orderBy('salesub_id', 'asc');
            },
        ];

        $slct = ['sales_id', 'sales_discount as tot_disc', 'sales_acc_ledger_id', 'sales_discount', 'sales_flags', 'sales_inv_no', 'sales_date', 'sales_time', 'sales_pay_type', 'sales_cust_id', 'sales_cust_name', 'sales_cust_address', 'sales_cust_tin', 'users.usr_name', 'users.usr_nickname',
            'acc_branches.branch_name', 'acc_branches.branch_display_name', 'acc_branches.branch_address', 'acc_branches.branch_phone', 'acc_branches.branch_mob', 'acc_branches.branch_code', 'acc_branches.branch_reg_no', 'acc_branches.branch_tin', 'sales_branch_inv'];
        $data = SalesMaster::select($slct)
            ->join('users', 'usr_id', 'sales_master.sales_added_by')
            ->join('acc_branches', 'acc_branches.branch_id', 'sales_master.branch_id')

            ->with($rltn);
        if ($this->branch_id > 0) {
            $data = $data->where('sales_master.branch_id', $this->branch_id);
        }

        $data = $data->where('sales_inv_no', $request['sales_inv_no'])->first();

        if ($data) {
            $data = $data->toArray();
            //$data['tot_amount'] = array_sum(array_column($data['sales_sub'], 'sale_total_amount'));
            $data['tot_tax'] = array_sum(array_column($data['sales_sub'], 'salesub_tax_rate'));
            // $data['tot_disc'] = array_sum(array_column($data['sales_sub'], 'salesub_discount'));
            $data['tot_rate'] = array_sum(array_column($data['sales_sub'], 'sale_amount'));

            $data['tot_amount'] = $data['tot_rate'] + $data['tot_tax'] - $data['tot_disc'];
            $data['is_returned'] = 0;
            $row_exist = SalesReturnMaster::where('salesret_flags', '1')->where('salesret_sales_inv_no', $data['sales_inv_no'])->first();

            if ($row_exist) {
                $data['is_returned'] = 1;
            }

        } else {

            $rltn = [
                'sales_sub' => function ($qr) {

                    $amnt = DB::raw('(salesub_rate * salesub_qty) as sale_amount');
                    $totalAmt = DB::raw('((salesub_rate * salesub_qty) + (salesub_tax_rate * salesub_qty) - salesub_discount) as sale_total_amount ');
                    $salesTax = DB::raw('(salesub_tax_rate * salesub_qty) as salesub_tax_rate');
                    $qr->select('salesub_branch_stock_id', 'salesub_stock_id', 'salesub_prod_id',

                        'salesub_sales_inv_no', 'salesub_rate', 'salesub_qty', 'salesub_discount', $salesTax
                        , 'salesub_tax_per', $amnt, $totalAmt, 'products.prd_name',
                        'units.unit_display', 'salesub_unit_id')
                        ->join('products', 'prd_id', 'sale_sub.salesub_prod_id')
                        ->join('units', 'unit_id', 'sale_sub.salesub_unit_id')

                        ->orderBy('salesub_id', 'asc');
                },
            ];

            $slct = ['sales_id', 'sales_discount as tot_disc', 'sales_discount as tot_disc', 'sales_acc_ledger_id', 'sales_discount', 'sales_flags', 'sales_inv_no', 'sales_date', 'sales_time', 'sales_pay_type', 'sales_cust_id', 'sales_cust_name', 'sales_cust_address', 'sales_cust_tin', 'users.usr_name', 'users.usr_nickname',
                'acc_branches.branch_name', 'acc_branches.branch_display_name', 'acc_branches.branch_address', 'acc_branches.branch_phone', 'acc_branches.branch_mob', 'acc_branches.branch_code', 'acc_branches.branch_reg_no', 'acc_branches.branch_tin', 'sales_branch_inv'];
            $data = SalesMaster::select($slct)
                ->join('users', 'usr_id', 'sales_master.sales_added_by')
                ->join('acc_branches', 'acc_branches.branch_id', 'sales_master.branch_id')

                ->with($rltn);
            if ($this->branch_id > 0) {
                $data = $data->where('sales_master.branch_id', $this->branch_id);
            }

            $data = $data->where('sales_branch_inv', $request['sales_inv_no'])->first();

            if ($data) {
                $data = $data->toArray();

                //$data['tot_amount'] = array_sum(array_column($data['sales_sub'], 'sale_total_amount'));
                // $data['tot_disc'] = array_sum(array_column($data['sales_sub'], 'salesub_discount'));

                $data['tot_tax'] = array_sum(array_column($data['sales_sub'], 'salesub_tax_rate'));
                $data['tot_rate'] = array_sum(array_column($data['sales_sub'], 'sale_amount'));
                $data['tot_amount'] = $data['tot_rate'] + $data['tot_tax'] - $data['tot_disc'];

                $data['is_returned'] = 0;
                $row_exist = SalesReturnMaster::where('salesret_flags', '1')->where('salesret_sales_inv_no', $data['sales_inv_no'])->first();

                if ($row_exist) {
                    $data['is_returned'] = 1;
                }

            } else {

                $data = [];
            }
        }

        return $data;

    }
    public function saleInvDetailData($request)
    {

        $rltn = [
            'sales_sub' => function ($qr) {

                $amnt = DB::raw('(salesub_rate * salesub_qty) as sale_amount');
                $totalAmt = DB::raw('(salesub_rate + salesub_tax_rate - salesub_discount) * salesub_qty as sale_total_amount ');
                $salesTax = DB::raw('(salesub_tax_rate * salesub_qty) as salesub_tax_rate');
                $qr->select('sale_sub.*', 'sale_sub.branch_id as sale_sub_branch_id', 'products.prd_barcode', 'salesub_sales_inv_no', 'salesub_rate', 'salesub_qty', 'salesub_discount', $salesTax
                    , 'salesub_tax_per', $amnt, $totalAmt, 'products.prd_name', 'products.prd_barcode',
                    'units.*', 'salesub_unit_id')
                    ->join('products', 'prd_id', 'sale_sub.salesub_prod_id')
                    ->join('units', 'unit_id', 'sale_sub.salesub_unit_id')

                    ->orderBy('salesub_id', 'asc');
            },
        ];

        $slct = ['sales_id', 'sales_discount as tot_disc', 'sales_acc_ledger_id', 'sales_discount', 'sales_flags', 'sales_inv_no', 'sales_date', 'sales_time', 'sales_pay_type', 'sales_cust_id', 'sales_cust_name', 'sales_cust_address', 'sales_cust_tin', 'users.usr_name', 'users.usr_nickname',
            'acc_branches.branch_name', 'acc_branches.branch_display_name', 'acc_branches.branch_address', 'acc_branches.branch_phone', 'acc_branches.branch_mob', 'acc_branches.branch_code', 'acc_branches.branch_reg_no', 'acc_branches.branch_tin', 'sales_branch_inv'];
        $data = SalesMaster::select($slct)
            ->join('users', 'usr_id', 'sales_master.sales_added_by')
            ->join('acc_branches', 'acc_branches.branch_id', 'sales_master.branch_id')

            ->with($rltn);

        $data = $data->where('sales_master.branch_id', $this->branch_id)->where('sales_master.sales_flags', 1);

        $data = $data->where('sales_inv_no', $request['sales_inv_no'])->first();

        if ($data) {
            $data = $data->toArray();
            $data['tot_tax'] = array_sum(array_column($data['sales_sub'], 'salesub_tax_rate'));
            $data['tot_rate'] = array_sum(array_column($data['sales_sub'], 'sale_amount'));
            $data['tot_amount'] = $data['tot_rate'] + $data['tot_tax'] - $data['tot_disc'];
            $data['is_returned'] = 0;
            $row_exist = SalesReturnMaster::where('salesret_flags', '1')->where('salesret_sales_inv_no', $data['sales_inv_no'])->first();

            if ($row_exist) {
                $data['is_returned'] = 1;
            }

        } else {

            $rltn = [
                'sales_sub' => function ($qr) {

                    $amnt = DB::raw('(salesub_rate * salesub_qty) as sale_amount');
                    $totalAmt = DB::raw('((salesub_rate * salesub_qty) + (salesub_tax_rate * salesub_qty) - salesub_discount) as sale_total_amount ');
                    $salesTax = DB::raw('(salesub_tax_rate * salesub_qty) as salesub_tax_rate');
                    $qr->select('sale_sub.*', 'salesub_branch_stock_id', 'salesub_stock_id', 'salesub_prod_id',
                        'sale_sub.branch_id as sale_sub_branch_id',
                        'salesub_sales_inv_no', 'salesub_rate', 'salesub_qty', 'salesub_discount', $salesTax
                        , 'salesub_tax_per', $amnt, $totalAmt, 'products.prd_name', 'products.prd_barcode', 'units.*', 'units.unit_name', 'units.unit_display',
                        'units.unit_base_qty', 'units.unit_code', 'salesub_unit_id')
                        ->join('products', 'prd_id', 'sale_sub.salesub_prod_id')
                        ->join('units', 'unit_id', 'sale_sub.salesub_unit_id')

                        ->orderBy('salesub_id', 'asc');
                },
            ];

            $slct = ['sales_id', 'sales_discount as tot_disc', 'sales_acc_ledger_id', 'sales_discount', 'sales_flags', 'sales_inv_no', 'sales_date', 'sales_time', 'sales_pay_type', 'sales_cust_id', 'sales_cust_name', 'sales_cust_address', 'sales_cust_tin', 'users.usr_name', 'users.usr_nickname',
                'acc_branches.branch_name', 'acc_branches.branch_display_name', 'acc_branches.branch_address', 'acc_branches.branch_phone', 'acc_branches.branch_mob', 'acc_branches.branch_code', 'acc_branches.branch_reg_no', 'acc_branches.branch_tin', 'sales_branch_inv'];
            $data = SalesMaster::select($slct)
                ->join('users', 'usr_id', 'sales_master.sales_added_by')
                ->join('acc_branches', 'acc_branches.branch_id', 'sales_master.branch_id')
                ->with($rltn);

            $data = $data->where('sales_master.branch_id', $this->branch_id)->where('sales_master.sales_flags', 1);
            $data = $data->where('sales_branch_inv', $request['sales_inv_no'])->first();

            if ($data) {
                $data = $data->toArray();

                $data['tot_tax'] = array_sum(array_column($data['sales_sub'], 'salesub_tax_rate'));
                $data['tot_rate'] = array_sum(array_column($data['sales_sub'], 'sale_amount'));
                $data['tot_amount'] = $data['tot_rate'] + $data['tot_tax'] - $data['tot_disc'];

                $data['is_returned'] = 0;
                $row_exist = SalesReturnMaster::where('salesret_flags', '1')->where('salesret_sales_inv_no', $data['sales_inv_no'])->first();

                if ($row_exist) {
                    $data['is_returned'] = 1;
                }

            } else {

                $data = [];
            }
        }

        $data['cust_balance'] = 0;
        if ($sales_cust_id = isset($data['sales_cust_id']) ? $data['sales_cust_id'] : 0) {

            $ledger = Acc_customer::where('cust_id', $sales_cust_id)->first();

            $amnt = DB::raw('(IFNULL(sum(salesduesub_out),0) - IFNULL(sum(salesduesub_in),0)) as bal_amt');
            $where = array();
            $where[] = ['salesduesub_ledger_id', '=', $ledger['ledger_id']];
            $where[] = ['salesduesub_flags', '=', 1];
            $balance = SalesDueSub::select($amnt)
                ->where($where)
                ->first();
            $data['cust_balance'] = $balance['bal_amt'];

        }

        $Acccashledger = Acc_ledger::find($data['sales_acc_ledger_id']);
        $data['sales_acc_ledger_name'] = $Acccashledger['ledger_name'];
        foreach ($data['sales_sub'] as $k => $prdcts) {

            $qty_in_base = $prdcts['unit_base_qty'] * $prdcts['salesub_qty'];

            $remain_qty_in_base = $prdcts['unit_base_qty'] * $prdcts['salesub_rem_qty'];

            $units = ProdUnit::select('prod_units.produnit_unit_id as sur_unit_id',
                'units.unit_base_qty', 'units.unit_display',
                'units.unit_name', 'units.unit_code')
                ->join('units', 'units.unit_id', 'prod_units.produnit_unit_id')
                ->where('prod_units.produnit_prod_id', $prdcts['salesub_prod_id'])->get();

            // $slctqr['sur_prd_id'] = $prdcts['salesub_prod_id'];
            // $slctqr['sur_branch_id'] = $prdcts['sale_sub_branch_id'];

            foreach ($units as $d => $unt) {

                // $slctqr['sur_unit_id'] = $unt['sur_unit_id'];
                // $value = StockUnitRates::where($slctqr)->first();

                if ($unt['unit_base_qty'] > $qty_in_base) {
                    $units[$d]['sold_qunatity'] = 'invalid';
                    $units[$d]['rem_qunatity'] = 'invalid';
                } else {
                    $units[$d]['sold_qunatity'] = $qty_in_base / $unt['unit_base_qty'];

                    $units[$d]['rem_qunatity'] = $remain_qty_in_base / $unt['unit_base_qty'];
                    if ($unt['unit_id'] == $prdcts['salesub_unit_id']) {
                        $units[$d]['sur_unit_rate'] = $prdcts['salesub_rate'];
                    } else {
                        $units[$d]['sur_unit_rate'] = $prdcts['salesub_rate'] / $units[$d]['sold_qunatity'];
                    }
                }

            }
            $data['sales_sub'][$k]['units'] = $units;
        }

        return $data;

    }
    public function saleInvDetailByBranch($request)
    {
        $rltn = [
            'sales_sub' => function ($qr) {

                $amnt = DB::raw('(salesub_rate * salesub_qty) as sale_amount');
                $totalAmt = DB::raw('((salesub_rate * salesub_qty) + (salesub_tax_rate * salesub_qty) - salesub_discount) as sale_total_amount ');
                $salesTax = DB::raw('(salesub_tax_rate * salesub_qty) as salesub_tax_rate');
                $qr->select('salesub_branch_stock_id', 'salesub_stock_id', 'salesub_prod_id',

                    'salesub_sales_inv_no', 'salesub_rate', 'salesub_qty', 'salesub_discount', $salesTax
                    , 'salesub_tax_per', $amnt, $totalAmt, 'products.prd_name', 'units.unit_display', 'salesub_unit_id')
                    ->join('products', 'prd_id', 'sale_sub.salesub_prod_id')
                    ->join('units', 'unit_id', 'sale_sub.salesub_unit_id')

                    ->orderBy('salesub_id', 'asc');
            },
        ];

        $slct = ['sales_id', 'sales_discount as tot_disc', 'sales_acc_ledger_id', 'sales_discount', 'sales_flags', 'sales_inv_no', 'sales_date', 'sales_time', 'sales_pay_type', 'sales_cust_id', 'sales_cust_name', 'sales_cust_address', 'sales_cust_tin', 'users.usr_name', 'users.usr_nickname',
            'acc_branches.branch_name', 'acc_branches.branch_display_name', 'acc_branches.branch_address', 'acc_branches.branch_phone', 'acc_branches.branch_mob', 'acc_branches.branch_code', 'acc_branches.branch_reg_no', 'acc_branches.branch_tin', 'sales_branch_inv'];
        $data = SalesMaster::select($slct)
            ->join('users', 'usr_id', 'sales_master.sales_added_by')
            ->join('acc_branches', 'acc_branches.branch_id', 'sales_master.branch_id')

            ->with($rltn);

        $data = $data->where('sales_branch_inv', $request['sales_inv_no'])->first();

        if ($data) {
            $data = $data->toArray();

            // $data['tot_amount'] = array_sum(array_column($data['sales_sub'], 'sale_total_amount'));
            // $data['tot_tax'] = array_sum(array_column($data['sales_sub'], 'salesub_tax_rate'));
            // $data['tot_disc'] = array_sum(array_column($data['sales_sub'], 'salesub_discount'));
            // $data['tot_rate'] = array_sum(array_column($data['sales_sub'], 'sale_amount'));

            $data['tot_tax'] = array_sum(array_column($data['sales_sub'], 'salesub_tax_rate'));
            $data['tot_rate'] = array_sum(array_column($data['sales_sub'], 'sale_amount'));
            $data['tot_amount'] = $data['tot_rate'] + $data['tot_tax'] - $data['tot_disc'];
            $data['is_returned'] = 0;
            $row_exist = SalesReturnMaster::where('salesret_flags', '1')->where('salesret_sales_inv_no', $data['sales_inv_no'])->first();

            if ($row_exist) {
                $data['is_returned'] = 1;
            }

        } else {

            $data = [];
        }

        return $data;

    }

    public function datesFromPeriods($pt, $rq)
    {
        switch ($pt) {

            case 't':
                $date1 = $date2 = date('Y-m-d');
                break;

            case 'ld':
                $date1 = $date2 = date('Y-m-d', strtotime("-1 days"));
                break;

            case 'lw':
                $previous_week = strtotime("-1 week +1 day");
                $start_week = strtotime("last sunday midnight", $previous_week);
                $end_week = strtotime("next saturday", $start_week);
                $date1 = date("Y-m-d", $start_week);
                $date2 = date("Y-m-d", $end_week);
                break;

            case 'lm':
                $date1 = date("Y-m-d", strtotime("first day of previous month"));
                $date2 = date("Y-m-d", strtotime("last day of previous month"));
                break;

            case 'ly':
                $year = date('Y') - 1;
                $date1 = $year . "-01-01";
                $date2 = $year . "-12-31";
                break;

            case 'c':
                $date1 = isset($rq['date1']) ? date('Y-m-d', strtotime($rq['date1'])) : 0;
                $date2 = isset($rq['date2']) ? date('Y-m-d', strtotime($rq['date2'])) : 0;
                $time1 = isset($rq['time1']) && $rq['time1'] != '' ? $rq['time1'] : 0;
                $time2 = isset($rq['time2']) && $rq['time2'] != '' ? $rq['time2'] : 0;
                break;
            default:
                return false;
                break;
        }

        if (isset($rq['time1']) && isset($rq['time2'])) {
            return [$date1, $date2, $time1, $time2];
        } else {
            return [$date1, $date2];
        }

    }

    public function saleReturnInvDetail($request)
    {

        $rltn = [
            'sales_sub' => function ($qr) {

                $amnt = DB::raw('(salesretsub_rate * salesretsub_qty) as sale_amount');
                $totalAmt = DB::raw('(salesretsub_rate+ salesretsub_tax_rate) * salesretsub_qty as sale_total_amount ');
                $salesTax = DB::raw('(salesretsub_tax_rate * salesretsub_qty) as salesub_tax_rate');
                $qr->select('*', $amnt, $totalAmt, $salesTax)
                    ->join('products', 'prd_id', 'sales_return_sub.salesretsub_prod_id')
                    ->join('units', 'unit_id', 'sales_return_sub.salesretsub_unit_id')

                ;
            },
        ];

        $slct = ['*', 'users.usr_name', 'users.usr_nickname',
            'acc_branches.branch_name', 'acc_branches.branch_address', 'acc_branches.branch_phone', 'acc_branches.branch_mob', 'acc_branches.branch_code', 'acc_branches.branch_reg_no', 'acc_branches.branch_tin'];
        $data = SalesReturnMaster::
            select($slct)
            ->join('users', 'usr_id', 'sales_return_master.salesret_added_by')
            ->join('acc_branches', 'acc_branches.branch_id', 'sales_return_master.branch_id')->with($rltn);

        if ($this->branch_id > 0) {
            $data = $data->where('sales_return_master.branch_id', $this->branch_id);
        }

        $data = $data->where('sales_return_master.salesret_no', $request['sales_inv_no'])->first();

        if (!$data) {
            $data = SalesReturnMaster::
                select($slct)
                ->join('users', 'usr_id', 'sales_return_master.salesret_added_by')
                ->join('acc_branches', 'acc_branches.branch_id', 'sales_return_master.branch_id')->with($rltn);

            if ($this->branch_id > 0) {
                $data = $data->where('sales_return_master.branch_id', $this->branch_id);
            }
            $data = $data->where('sales_return_master.salesret_branch_ret_no', $request['sales_inv_no'])->first();

        }

        if ($data) {
            $data = $data->toArray();
            $data['tot_tax'] = array_sum(array_column($data['sales_sub'], 'salesub_tax_rate'));
            $data['tot_rate'] = array_sum(array_column($data['sales_sub'], 'sale_amount'));
            $data['tot_amount'] = $data['tot_rate'] + $data['tot_tax'] - $data['tot_disc'];

        } else {

            $data = [];

        }

        return $data;

    }

}
