<?php

namespace App\Http\Controllers\Api\Reports;

use App\Http\Controllers\Api\ApiParent;
use App\Models\Accounts\Acc_group;
use App\Models\Accounts\Acc_ledger;
use App\Models\Accounts\Acc_voucher;
use App\Models\Company\Acc_branch;
use DB;
use Illuminate\Http\Request;

class LedgerReportController extends ApiParent
{

    public function ledgerDetail(Request $request)
    {
        if ($request['ledger_sel_id']) {

            $ledDet = Acc_ledger::select('acc_ledgers.*', 'acc_groups.accgrp_name', 'acc_branches.branch_name')->where('ledger_id', $request['ledger_sel_id'])
                ->leftjoin('acc_groups', 'acc_groups.accgrp_id', 'acc_ledgers.ledger_accgrp_id')
                ->leftjoin('acc_branches', 'acc_branches.branch_id', 'acc_ledgers.branch_id')
                ->first();

            $query = Acc_voucher::select('acc_voucher.*', 'voucher_type.vchtype_name', 'acc_ledgers.ledger_name')->where('vch_ledger_to', $request['ledger_sel_id'])
                ->where('vch_vchtype_id', '!=', 14)
                ->where('vch_flags', '=', 1)
                ->leftjoin('acc_ledgers', 'acc_ledgers.ledger_id', 'acc_voucher.vch_ledger_from')->leftjoin('voucher_type', 'acc_voucher.vch_vchtype_id', 'voucher_type.vchtype_id');
            if ($this->branch_id) {
                $query->where('acc_voucher.branch_id', '=', $this->branch_id);
            }
            // branch open date

            $branchDet = Acc_branch::where('branch_id', $this->branch_id)->first('branch_open_date');
            $ledDet['branch_date'] = $branchDet->branch_open_date;
            $branchOpenPrevDate = date('Y-m-d', strtotime('-1 day', strtotime($branchDet->branch_open_date)));
            $date_ob = date('Y-m-d');
            $ledDet['from'] = $ledDet['branch_date'];
            $ledDet['to'] = date('Y-m-d');
            if (isset($request['period_type']) && $request['period_type'] != '') {
                switch ($request['period_type']) {
                    case 't':
                        $date_ob = date('Y-m-d', strtotime("-1 days"));
                        $ledDet['from'] = date('Y-m-d');
                        if ($branchDet->branch_open_date > $ledDet['from']) {
                            $ledDet['from'] = $branchDet->branch_open_date;
                        }
                        $ledDet['to'] = $ledDet['from'];
                        $res = $query->where(DB::raw('DATE(erp_acc_voucher.vch_date)'), $ledDet['from']);
                        break;
                    case 'ld':
                        $date_ob = date('Y-m-d', strtotime("-2 days"));
                        $ledDet['from'] = date('Y-m-d', strtotime("-1 days"));
                        if ($branchDet->branch_open_date > $ledDet['from']) {
                            $ledDet['from'] = $branchDet->branch_open_date;
                        }
                        $ledDet['to'] = $ledDet['from'];
                        $res = $query->where(DB::raw('DATE(erp_acc_voucher.vch_date)'), $ledDet['from']);
                        break;
                    case 'lw':
                        $date_ob = date('Y-m-d', strtotime("-8 days"));
                        $ledDet['from'] = date('Y-m-d', strtotime("-7 days"));
                        if ($branchDet->branch_open_date > $ledDet['from']) {
                            $ledDet['from'] = $branchDet->branch_open_date;
                        }
                        $ledDet['to'] = date('Y-m-d');
                        $res = $query->whereBetween(DB::raw('DATE(erp_acc_voucher.vch_date)'), [$ledDet['from'], $ledDet['to']]);
                        break;
                    case 'lm':
                        $date_ob = date('Y-m-d', strtotime("-1 months -1 days"));
                        $ledDet['from'] = date('Y-m-d', strtotime("-1 months"));
                        if ($branchDet->branch_open_date > $ledDet['from']) {
                            $ledDet['from'] = $branchDet->branch_open_date;
                        }
                        $ledDet['to'] = date('Y-m-d');
                        $res = $query->whereBetween(DB::raw('DATE(erp_acc_voucher.vch_date)'), [$ledDet['from'], $ledDet['to']]);
                        break;
                    case 'ly':
                        $date_ob = date('Y-m-d', strtotime("-1 year -1 days"));
                        $ledDet['from'] = date('Y-m-d', strtotime("-1 year"));
                        if ($branchDet->branch_open_date > $ledDet['from']) {
                            $ledDet['from'] = $branchDet->branch_open_date;
                        }
                        $ledDet['to'] = date('Y-m-d');
                        $res = $query->whereBetween(DB::raw('DATE(erp_acc_voucher.vch_date)'), [$ledDet['from'], $ledDet['to']]);
                        break;
                    case 'c':
                        $date_ob = date('Y-m-d', strtotime("-1 days", strtotime($request['date1'])));
                        $ledDet['from'] = $request['date1'];
                        if ($branchDet->branch_open_date > $ledDet['from']) {
                            $ledDet['from'] = $branchDet->branch_open_date;
                        }
                        $ledDet['to'] = $request['date2'];
                        $res = $query->whereBetween(DB::raw('DATE(erp_acc_voucher.vch_date)'), [$ledDet['from'], $ledDet['to']]);
                        break;
                }
            }

            if (isset($request['added_by']) && is_int($request['added_by'])) {

                $res = $query->where('vch_added_by', $request['added_by']);

            }

            if (isset($request['vch_id']) && is_int($request['vch_id'])) {

                $res = $query->where('vch_vchtype_id', $request['vch_id']);

            }

            $ledDet['vouchers'] = $query->orderby('vch_date', 'asc')->get()->toArray();
            //    return ($ledDet['vouchers']);
            // exit;
            $ledDet['total_in_amt'] = round(array_sum(array_column($ledDet['vouchers'], 'vch_in')), 2);
            $ledDet['total_out_amt'] = round(array_sum(array_column($ledDet['vouchers'], 'vch_out')), 2);
            $ledDet['curr_total'] = $ledDet['total_out_amt'] - $ledDet['total_in_amt'];

            if ($ledDet['curr_total'] > 0) {
                $ledDet['curr_total_disp_cr'] = $ledDet['curr_total'] . ' Cr';
            } else {
                $ledDet['curr_total_disp_dr'] = -($ledDet['curr_total']) . ' Dr';
            }

            // yearly opening balance

            $inOutYear = Acc_voucher::where([
                ['branch_id', '=', $this->branch_id],
                ['vch_vchtype_id', '=', 14],
                ['vch_ledger_to', '=', $request['ledger_sel_id']],
                ['vch_date', '=', date('Y-m-d 00:00:00', strtotime($branchOpenPrevDate))],
                ['vch_flags', '=', 1],
            ])->first();

            if ($inOutYear) {
                $ledDet['op_balance_yearly'] = $inOutYear->vch_out - $inOutYear->vch_in;
            } else {
                $ledDet['op_balance_yearly'] = 0;
            }

            // previous opening balance

            $inOut = Acc_voucher::select(DB::raw('SUM(vch_in) as vch_in'), DB::raw('SUM(vch_out) as vch_out'))
                ->where('vch_ledger_to', $request['ledger_sel_id'])
                ->where('vch_flags', '=', 1)
                ->where('vch_vchtype_id', '!=', 14)
                ->whereBetween(DB::raw('DATE(erp_acc_voucher.vch_date)'), [$branchDet->branch_open_date, $date_ob])->get();
            // echo Config::get('database.connections.mysql.database');
            // echo $inOut[0]->vch_out;
            // echo $inOut[0]->vch_in;
            // exit;
            // return $inOut;

            $ledDet['op_balance'] = $inOut[0]->vch_out - $inOut[0]->vch_in;

            // previous opening balance + yearly opening balance

            $ledDet['op_balance'] = (($ledDet['op_balance_yearly']) + ($ledDet['op_balance']));

            if ($ledDet['op_balance'] > 0) {
                $ledDet['op_balance_disp_cr'] = $ledDet['op_balance'] . ' Cr';
            } else {
                $ledDet['op_balance_disp_dr'] = -($ledDet['op_balance']) . ' Dr';
            }
            $ledDet['cl_balance'] = $ledDet['curr_total'] + $ledDet['op_balance'];

            if ($ledDet['cl_balance'] > 0) {
                $ledDet['cl_balance_disp_cr'] = $ledDet['cl_balance'] . ' Cr';
            } else {
                $ledDet['cl_balance_disp_dr'] = -($ledDet['cl_balance']) . ' Dr';
            }

        } else {
            $ledDet = Acc_ledger::select('acc_ledgers.*', 'acc_groups.accgrp_name', 'acc_branches.branch_name')->where('ledger_id', $request['ledger_id'])
                ->leftjoin('acc_groups', 'acc_groups.accgrp_id', 'acc_ledgers.ledger_accgrp_id')
                ->leftjoin('acc_branches', 'acc_branches.branch_id', 'acc_ledgers.branch_id')
                ->first();

            $query = Acc_voucher::select('acc_voucher.*', 'voucher_type.vchtype_name', 'acc_ledgers.ledger_name')->where('vch_ledger_to', $request['ledger_id'])
                ->where('vch_vchtype_id', '!=', 14)->where('vch_flags', '=', 1)
            // ->where('acc_voucher.branch_id', '=', $this->branch_id)
                ->leftjoin('acc_ledgers', 'acc_ledgers.ledger_id', 'acc_voucher.vch_ledger_from')
                ->leftjoin('voucher_type', 'acc_voucher.vch_vchtype_id', 'voucher_type.vchtype_id');
            if ($this->branch_id) {
                $query->where('acc_voucher.branch_id', '=', $this->branch_id);
            }
            $ledDet['vouchers'] = $query->orderby('vch_date', 'asc')->get()->toArray();
            $ledDet['total_in_amt'] = round(array_sum(array_column($ledDet['vouchers'], 'vch_in')), 2);
            $ledDet['total_out_amt'] = round(array_sum(array_column($ledDet['vouchers'], 'vch_out')), 2);
            $ledDet['curr_total'] = $ledDet['total_out_amt'] - $ledDet['total_in_amt'];

            // branch open date

            if ($this->branch_id) {
                $branchDet = Acc_branch::where('branch_id', $this->branch_id)->first('branch_open_date');
            } else {
                $branchDet = Acc_branch::where('branch_id', 1)->first('branch_open_date');
            }

            $ledDet['branch_date'] = $branchDet->branch_open_date;
            $branchOpenPrevDate = date('Y-m-d', strtotime('-1 day', strtotime($branchDet->branch_open_date)));

            // yearly opening balance

            $inOut = Acc_voucher::where([
                ['branch_id', '=', $this->branch_id],
                ['vch_vchtype_id', '=', 14],
                ['vch_ledger_to', '=', $request['ledger_id']],
                ['vch_date', '=', date('Y-m-d 00:00:00', strtotime($branchOpenPrevDate))],
                ['vch_flags', '=', 1],

            ])->first();

            if ($inOut) {$ledDet['op_balance'] = $inOut->vch_out - $inOut->vch_in;} else { $ledDet['op_balance'] = 0;}
            if ($ledDet['op_balance'] > 0) {
                $ledDet['op_balance_disp_cr'] = $ledDet['op_balance'] . ' Cr';
            } else {
                $ledDet['op_balance_disp_dr'] = -($ledDet['op_balance']) . ' Dr';
            }

            // current total

            if ($ledDet['curr_total'] > 0) {
                $ledDet['curr_total_disp_cr'] = $ledDet['curr_total'] . ' Cr';
            } else {
                $ledDet['curr_total_disp_dr'] = -($ledDet['curr_total']) . ' Dr';
            }

            //closing balance

            $ledDet['cl_balance'] = $ledDet['curr_total'] + $ledDet['op_balance'];

            if ($ledDet['cl_balance'] > 0) {
                $ledDet['cl_balance_disp_cr'] = $ledDet['cl_balance'] . ' Cr';
            } else {
                $ledDet['cl_balance_disp_dr'] = -($ledDet['cl_balance']) . ' Dr';
            }
            $ledDet['from'] = date('Y-m-d', strtotime($branchDet->branch_open_date));
            $ledDet['to'] = date('Y-m-d H:i:s');
        }

        return parent::displayData($ledDet);

    }

    public function ledgerDetailExcel(Request $request)
    {
        if ($request['ledger_sel_id']) {

            $ledDet = Acc_ledger::select('acc_ledgers.*', 'acc_groups.accgrp_name', 'acc_branches.branch_name')->where('ledger_id', $request['ledger_sel_id'])
                ->leftjoin('acc_groups', 'acc_groups.accgrp_id', 'acc_ledgers.ledger_accgrp_id')
                ->leftjoin('acc_branches', 'acc_branches.branch_id', 'acc_ledgers.branch_id')
                ->first();

            $query = Acc_voucher::select('acc_voucher.*', 'voucher_type.vchtype_name', 'acc_ledgers.ledger_name')->where('vch_ledger_to', $request['ledger_sel_id'])
                ->where('vch_vchtype_id', '!=', 14)
                ->where('vch_flags', '=', 1)
                ->leftjoin('acc_ledgers', 'acc_ledgers.ledger_id', 'acc_voucher.vch_ledger_from')->leftjoin('voucher_type', 'acc_voucher.vch_vchtype_id', 'voucher_type.vchtype_id');
            if ($this->branch_id) {
                $query->where('acc_voucher.branch_id', '=', $this->branch_id);
            }
            // branch open date

            $branchDet = Acc_branch::where('branch_id', $this->branch_id)->first('branch_open_date');
            $ledDet['branch_date'] = $branchDet->branch_open_date;
            $branchOpenPrevDate = date('Y-m-d', strtotime('-1 day', strtotime($branchDet->branch_open_date)));
            $date_ob = date('Y-m-d');
            $ledDet['from'] = $ledDet['branch_date'];
            $ledDet['to'] = date('Y-m-d');
            if (isset($request['period_type']) && $request['period_type'] != '') {
                switch ($request['period_type']) {
                    case 't':
                        $date_ob = date('Y-m-d', strtotime("-1 days"));
                        $ledDet['from'] = date('Y-m-d');
                        if ($branchDet->branch_open_date > $ledDet['from']) {
                            $ledDet['from'] = $branchDet->branch_open_date;
                        }
                        $ledDet['to'] = $ledDet['from'];
                        $res = $query->where(DB::raw('DATE(erp_acc_voucher.vch_date)'), $ledDet['from']);
                        break;
                    case 'ld':
                        $date_ob = date('Y-m-d', strtotime("-2 days"));
                        $ledDet['from'] = date('Y-m-d', strtotime("-1 days"));
                        if ($branchDet->branch_open_date > $ledDet['from']) {
                            $ledDet['from'] = $branchDet->branch_open_date;
                        }
                        $ledDet['to'] = $ledDet['from'];
                        $res = $query->where(DB::raw('DATE(erp_acc_voucher.vch_date)'), $ledDet['from']);
                        break;
                    case 'lw':
                        $date_ob = date('Y-m-d', strtotime("-8 days"));
                        $ledDet['from'] = date('Y-m-d', strtotime("-7 days"));
                        if ($branchDet->branch_open_date > $ledDet['from']) {
                            $ledDet['from'] = $branchDet->branch_open_date;
                        }
                        $ledDet['to'] = date('Y-m-d');
                        $res = $query->whereBetween(DB::raw('DATE(erp_acc_voucher.vch_date)'), [$ledDet['from'], $ledDet['to']]);
                        break;
                    case 'lm':
                        $date_ob = date('Y-m-d', strtotime("-1 months -1 days"));
                        $ledDet['from'] = date('Y-m-d', strtotime("-1 months"));
                        if ($branchDet->branch_open_date > $ledDet['from']) {
                            $ledDet['from'] = $branchDet->branch_open_date;
                        }
                        $ledDet['to'] = date('Y-m-d');
                        $res = $query->whereBetween(DB::raw('DATE(erp_acc_voucher.vch_date)'), [$ledDet['from'], $ledDet['to']]);
                        break;
                    case 'ly':
                        $date_ob = date('Y-m-d', strtotime("-1 year -1 days"));
                        $ledDet['from'] = date('Y-m-d', strtotime("-1 year"));
                        if ($branchDet->branch_open_date > $ledDet['from']) {
                            $ledDet['from'] = $branchDet->branch_open_date;
                        }
                        $ledDet['to'] = date('Y-m-d');
                        $res = $query->whereBetween(DB::raw('DATE(erp_acc_voucher.vch_date)'), [$ledDet['from'], $ledDet['to']]);
                        break;
                    case 'c':
                        $date_ob = date('Y-m-d', strtotime("-1 days", strtotime($request['date1'])));
                        $ledDet['from'] = $request['date1'];
                        if ($branchDet->branch_open_date > $ledDet['from']) {
                            $ledDet['from'] = $branchDet->branch_open_date;
                        }
                        $ledDet['to'] = $request['date2'];
                        $res = $query->whereBetween(DB::raw('DATE(erp_acc_voucher.vch_date)'), [$ledDet['from'], $ledDet['to']]);
                        break;
                }
            }

            if (isset($request['added_by']) && is_int($request['added_by'])) {

                $res = $query->where('vch_added_by', $request['added_by']);

            }

            if (isset($request['vch_id']) && is_int($request['vch_id'])) {

                $res = $query->where('vch_vchtype_id', $request['vch_id']);

            }

            $ledDet['vouchers'] = $query->orderby('vch_date', 'asc')->get()->toArray();
            //    return ($ledDet['vouchers']);
            // exit;
            $ledDet['total_in_amt'] = round(array_sum(array_column($ledDet['vouchers'], 'vch_in')), 2);
            $ledDet['total_out_amt'] = round(array_sum(array_column($ledDet['vouchers'], 'vch_out')), 2);
            $ledDet['curr_total'] = $ledDet['total_out_amt'] - $ledDet['total_in_amt'];

            if ($ledDet['curr_total'] > 0) {
                $ledDet['curr_total_disp_cr'] = $ledDet['curr_total'] . ' Cr';
            } else {
                $ledDet['curr_total_disp_dr'] = -($ledDet['curr_total']) . ' Dr';
            }

            // yearly opening balance

            $inOutYear = Acc_voucher::where([
                ['branch_id', '=', $this->branch_id],
                ['vch_vchtype_id', '=', 14],
                ['vch_ledger_to', '=', $request['ledger_sel_id']],
                ['vch_date', '=', date('Y-m-d 00:00:00', strtotime($branchOpenPrevDate))],
                ['vch_flags', '=', 1],
            ])->first();

            if ($inOutYear) {
                $ledDet['op_balance_yearly'] = $inOutYear->vch_out - $inOutYear->vch_in;
            } else {
                $ledDet['op_balance_yearly'] = 0;
            }

            // previous opening balance

            $inOut = Acc_voucher::select(DB::raw('SUM(vch_in) as vch_in'), DB::raw('SUM(vch_out) as vch_out'))
                ->where('vch_ledger_to', $request['ledger_sel_id'])
                ->where('vch_flags', '=', 1)
                ->where('vch_vchtype_id', '!=', 14)
                ->whereBetween(DB::raw('DATE(erp_acc_voucher.vch_date)'), [$branchDet->branch_open_date, $date_ob])->get();
            // echo Config::get('database.connections.mysql.database');
            // echo $inOut[0]->vch_out;
            // echo $inOut[0]->vch_in;
            // exit;
            // return $inOut;

            $ledDet['op_balance'] = $inOut[0]->vch_out - $inOut[0]->vch_in;

            // previous opening balance + yearly opening balance

            $ledDet['op_balance'] = (($ledDet['op_balance_yearly']) + ($ledDet['op_balance']));

            if ($ledDet['op_balance'] > 0) {
                $ledDet['op_balance_disp_cr'] = $ledDet['op_balance'] . ' Cr';
            } else {
                $ledDet['op_balance_disp_dr'] = -($ledDet['op_balance']) . ' Dr';
            }
            $ledDet['cl_balance'] = $ledDet['curr_total'] + $ledDet['op_balance'];

            if ($ledDet['cl_balance'] > 0) {
                $ledDet['cl_balance_disp_cr'] = $ledDet['cl_balance'] . ' Cr';
            } else {
                $ledDet['cl_balance_disp_dr'] = -($ledDet['cl_balance']) . ' Dr';
            }

        } else {
            $ledDet = Acc_ledger::select('acc_ledgers.*', 'acc_groups.accgrp_name', 'acc_branches.branch_name')->where('ledger_id', $request['ledger_id'])
                ->leftjoin('acc_groups', 'acc_groups.accgrp_id', 'acc_ledgers.ledger_accgrp_id')
                ->leftjoin('acc_branches', 'acc_branches.branch_id', 'acc_ledgers.branch_id')
                ->first();

            $query = Acc_voucher::select('acc_voucher.*', 'voucher_type.vchtype_name', 'acc_ledgers.ledger_name')->where('vch_ledger_to', $request['ledger_id'])
                ->where('vch_vchtype_id', '!=', 14)->where('vch_flags', '=', 1)
            // ->where('acc_voucher.branch_id', '=', $this->branch_id)
                ->leftjoin('acc_ledgers', 'acc_ledgers.ledger_id', 'acc_voucher.vch_ledger_from')
                ->leftjoin('voucher_type', 'acc_voucher.vch_vchtype_id', 'voucher_type.vchtype_id');
            if ($this->branch_id) {
                $query->where('acc_voucher.branch_id', '=', $this->branch_id);
            }
            $ledDet['vouchers'] = $query->orderby('vch_date', 'asc')->get()->toArray();
            $ledDet['total_in_amt'] = round(array_sum(array_column($ledDet['vouchers'], 'vch_in')), 2);
            $ledDet['total_out_amt'] = round(array_sum(array_column($ledDet['vouchers'], 'vch_out')), 2);
            $ledDet['curr_total'] = $ledDet['total_out_amt'] - $ledDet['total_in_amt'];

            // branch open date

            if ($this->branch_id) {
                $branchDet = Acc_branch::where('branch_id', $this->branch_id)->first('branch_open_date');
            } else {
                $branchDet = Acc_branch::where('branch_id', 1)->first('branch_open_date');
            }

            $ledDet['branch_date'] = $branchDet->branch_open_date;
            $branchOpenPrevDate = date('Y-m-d', strtotime('-1 day', strtotime($branchDet->branch_open_date)));

            // yearly opening balance

            $inOut = Acc_voucher::where([
                ['branch_id', '=', $this->branch_id],
                ['vch_vchtype_id', '=', 14],
                ['vch_ledger_to', '=', $request['ledger_id']],
                ['vch_date', '=', date('Y-m-d 00:00:00', strtotime($branchOpenPrevDate))],
                ['vch_flags', '=', 1],

            ])->first();

            if ($inOut) {$ledDet['op_balance'] = $inOut->vch_out - $inOut->vch_in;} else { $ledDet['op_balance'] = 0;}
            if ($ledDet['op_balance'] > 0) {
                $ledDet['op_balance_disp_cr'] = $ledDet['op_balance'] . ' Cr';
            } else {
                $ledDet['op_balance_disp_dr'] = -($ledDet['op_balance']) . ' Dr';
            }

            // current total

            if ($ledDet['curr_total'] > 0) {
                $ledDet['curr_total_disp_cr'] = $ledDet['curr_total'] . ' Cr';
            } else {
                $ledDet['curr_total_disp_dr'] = -($ledDet['curr_total']) . ' Dr';
            }

            //closing balance

            $ledDet['cl_balance'] = $ledDet['curr_total'] + $ledDet['op_balance'];

            if ($ledDet['cl_balance'] > 0) {
                $ledDet['cl_balance_disp_cr'] = $ledDet['cl_balance'] . ' Cr';
            } else {
                $ledDet['cl_balance_disp_dr'] = -($ledDet['cl_balance']) . ' Dr';
            }
            $ledDet['from'] = date('Y-m-d', strtotime($branchDet->branch_open_date));
            $ledDet['to'] = date('Y-m-d H:i:s');
        }


        $k=0;
        foreach ($ledDet['vouchers'] as $key => $v) {

            $excel[$k]['Vch.No'] = $v['vch_no'];
            $excel[$k]['Particulars'] = $v['ledger_name'];
            $excel[$k]['Ref. No'] = $v['branch_ref_no'];
            $excel[$k]['Vocher Type'] = $v['vchtype_name'];
          
            $excel[$k]['Notes'] = $v['vch_notes'];
            $excel[$k]['Date'] = $v['vch_date'];
            $excel[$k]['Debit Amount'] = $v['vch_in'];
            $excel[$k]['Credit Amount'] = $v['vch_out'];
            $k++;
        }

        return parent::displayData($excel);

    }

    public function ledgerDetailPdf(Request $request)
    {
        $ledDet['to'] = date('Y-m-d H:i:s');
        return parent::displayData($ledDet);

    }

    public function ledgerList(Request $request)
    {
        $where = array();
        $where[] = ['accgrp_flags', '=', 1];
        $where[] = ['accgrp_depth', '=', 0];

        if (isset($request['accgrp_id']) && $request['accgrp_id'] != '') {
            $where[] = ['accgrp_id', '=', $request['accgrp_id']['accgrp_parent']];
        }

        $rltn = [
            'subGroups' => function ($qr) use ($request) {
                $rltn = [
                    'ledgers' => function ($qr) use ($request) {
                        $qr->select('*');

                        if ($this->branch_id > 0) {
                            $qr->where('branch_id', '=', $this->branch_id);
                        }

                    },
                ];

                $qr->select('*')->where('accgrp_depth', 1)->with($rltn);
                if (isset($request['accgrp_id']) && $request['accgrp_id'] != ''
                    && $request['accgrp_id']['accgrp_depth'] != 0) {

                    $qr->where('accgrp_id', '=', $request['accgrp_id']['accgrp_id']);

                }
            },

            'ledgers' => function ($qr) use ($request) {
                $qr->select('*');
                if (isset($request['accgrp_id']) && $request['accgrp_id'] != '') {
                    $qr->where('ledger_accgrp_id', '=', $request['accgrp_id']['accgrp_id']);

                }

                if ($this->branch_id > 0) {
                    $qr->where('branch_id', '=', $this->branch_id);
                }

            },
        ];

        $qry = Acc_group::select('*')->with($rltn)->where($where)->paginate($this->per_page);

        return parent::displayData($qry);

    }

}
