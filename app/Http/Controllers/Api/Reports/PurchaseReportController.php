<?php

namespace App\Http\Controllers\Api\Reports;

use App\Http\Controllers\Api\ApiParent;
use App\Models\Product\Category;
use App\Models\Product\Product;
use App\Models\Purchase\Purchase;
use App\Models\Purchase\PurchaseReturn;
use App\Models\Purchase\PurchaseReturnSub;
use App\Models\Purchase\PurchaseSub;
use App\Models\Purchase\Supplier;
use DB;
use Illuminate\Http\Request;

class PurchaseReportController extends ApiParent
{
    public function Report(Request $request, $type)
    {

        switch ($type) {
            case 'summary':
                return parent::displayData($this->purchaseSummary($request));
                break;

            case 'purchase_based':
                return parent::displayData($this->reportByPurchase($request));
                break;

            case 'product_based':
                return parent::displayData($this->reportByProduct($request));
                break;

            case 'return_summary':
                return parent::displayData($this->purchaseReturnSummary($request));
                break;

            case 'return_product':
                return parent::displayData($this->returnByProduct($request));
                break;
            case 'purchase_return_summary':
                return parent::displayData($this->purchaseandReturnSummary($request));
                break;

            default:
                return parent::displayError('Invalid Endpoint', $this->badrequest_stat);
                break;
        }
    }

    public function purchaseSummary($rq)
    {

        // $company_branch = trim($rq->branch ? $rq->branch : 0);
        // $this->branch_id = trim(($this->usr_type == 2) ? $company_branch : $this->branch_id);

        $inv_filter = isset($rq['inv_filter']) ? $rq['inv_filter'] : 0;
        $inv_val = isset($rq['inv_val']) ? $rq['inv_val'] : 0;
        $inv_val2 = isset($rq['inv_val2']) ? $rq['inv_val2'] : 0;

        $price_filter = isset($rq['purch_price']) ? $rq['purch_price'] : 0;
        $price1 = isset($rq['price1']) ? $rq['price1'] : 0;
        $price2 = isset($rq['price2']) ? $rq['price2'] : 0;

        $dsnt_filter = isset($rq['purch_discount']) ? $rq['purch_discount'] : 0;
        $discount1 = isset($rq['discount1']) ? $rq['discount1'] : 0;
        $discount2 = isset($rq['discount2']) ? $rq['discount2'] : 0;

        if (isset($rq['payment_type']) && $rq['payment_type']) {
            $whr['purch_pay_type'] = $rq['payment_type'];
        }
        if (isset($rq['purch_supp_id']) && $rq['purch_supp_id']) {
            $whr['purch_supp_id'] = $rq['purch_supp_id'];
        }
        $whr['purch_flag'] = 1;
        if (isset($rq['purch_type']) && $rq['purch_type'] == 2) {

            $whr['purch_flag'] = 0;
        }

        if (isset($rq['purch_type']) && $rq['purch_type']) {
            $whr['purch_type'] = $rq['purch_type'];
        }

        if (isset($rq['is_year_rep']) && $rq['is_year_rep']) {
            $date = DB::raw('MONTHNAME(purch_date) as purch_date');
        } else {
            $date = 'purch_date';

        }

        $slct = ['purch_id', $date, 'purch_inv_no', 'purch_id2', 'purch_no', 'purch_supp_id', 'purch_tax_ledger_id', 'purch_ord_no', 'purch_type', 'purch_pay_type',
            'purch_inv_date', 'purch_amount', 'purch_tax', 'purch_discount', 'purch_frieght', 'purch_note'];

        $amnt = DB::raw('(purchsub_rate * purchsub_qty) as purchase_amount');
        $this->inrSlct = ['purchsub_purch_id', 'purchsub_prd_id', 'purchsub_qty', 'purchsub_rate', 'purchsub_frieght',
            'purchsub_tax', 'purchsub_tax_per', 'purchsub_unit_id', 'purchsub_gd_qty', 'purchsub_gd_id', $amnt];
        $rltn = [
            'items' => function ($qr) {
                $qr->select($this->inrSlct);
            },
            'supplier' => function ($qr) {
                $qr->select('supp_id', 'supp_name');
            },
        ];
        $qry = purchase::select($slct)->with($rltn);

        if ($ptype = isset($rq['period_type']) ? $rq['period_type'] : 0) {
            list($date1, $date2) = $this->datesFromPeriods($ptype, $rq);

            if ($date1 && $date2) {
                $qry = $qry->whereBetween('purch_date', [$date1, $date2]);
            }
        }

        $qry = isset($whr) ? $qry->where($whr) : $qry;

        if ($inv_filter && $inv_val) {
            switch ($inv_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $qry = $qry->where('purch_id', $inv_filter, $inv_val);
                    break;
                case 'between':
                    if ($inv_val) {
                        $qry = $qry->whereBetween('purch_id', [$inv_val, $inv_val2]);
                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }

        if ($price_filter && $price1) {
            switch ($price_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $qry = $qry->where('purch_amount', $price_filter, $price1);
                    break;
                case 'between':
                    if ($price2) {
                        $qry = $qry->whereBetween('purch_amount', [$price1, $price2]);
                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }

        if ($dsnt_filter && $discount1) {
            switch ($dsnt_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $qry = $qry->where('purch_discount', $dsnt_filter, $discount1);
                    break;
                case 'between':
                    if ($discount2) {
                        $qry = $qry->whereBetween('purch_discount', [$discount1, $discount2]);
                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }
        if ($this->branch_id > 0) {
            $qry = $qry->where('branch_id', $this->branch_id);
        } else {

            if ($this->active_branch_id == 'All') {

            } else {
                $qry = $qry->where('branch_id', $this->active_branch_id);

            }
        }

        $sum = $qry->with($rltn)->get()->toArray();
        foreach ($sum as $k => $val) {
            $sum[$k]['purch_tax'] = round($val['purch_tax'], 2);
            $sum[$k]['purch_amount'] = round($val['purch_amount'], 2);
            $sum[$k]['purch_discount'] = round($val['purch_discount'], 2);
            $sum[$k]['purch_frieght'] = round($val['purch_frieght'], 2);
        }

        // print_r($sum);die();
        if (isset($rq['export']) && $rq['export'] == 'export') {

            $data['data'] = $qry->orderBy('purch_date', 'ASC')->get()->toArray();
        } else {
            $data = $qry->orderBy('purch_date', 'ASC')->paginate(31)->toArray();

        }
        foreach ($data['data'] as $key => $val) {

            $data['data'][$key]['purchase_amount'] = $val['purch_amount'];
        }

        foreach ($sum as $key => $val) {

            $sum[$key]['purchase_amount'] = $val['purch_amount'];
        }
// return "dsfd"; return $sum;

        $dates = $supps = [];
        foreach ($data['data'] as $k => $array) {
            $data['data'][$k]['sl_num'] = $k + 1;
            //print_r($array);die
            $data['data'][$k]['supp_name'] = $array['supplier']['supp_name'];
            $data['data'][$k]['purch_type_name'] = ($array['purch_pay_type'] == 1) ? 'Credit' : 'Cash';
            $data['data'][$k]['purch_tax'] = number_format((float) $array['purch_tax'], 2, '.', '');
            $data['data'][$k]['purchase_amount'] = number_format((float) $array['purchase_amount'], 2, '.', '');
            $data['data'][$k]['purch_discount'] = number_format((float) $array['purch_discount'], 2, '.', '');
            $dates[$array['purch_date']][] = $k;
            $supps[$array['purch_supp_id']][] = $k;
            foreach ($array['items'] as $k2 => $val) {
                // print_r($val);die();

                $data['data'][$k]['items'][$k2]['purchsub_tax'] = number_format((float) $val['purchsub_tax'], 2, '.', '');
                $data['data'][$k]['items'][$k2]['purchase_amount'] = number_format((float) $val['purchase_amount'], 2, '.', '');

            }
        }

        //print_r(array_sum((array_column($sum, 'purch_frieght'))));die();
        $data['total_purchase'] = count($sum);
        $amnt = array_sum(array_column($sum, 'purchase_amount'));
        $data['total_purchase_amount'] = number_format((float) $amnt, 2, '.', '');
        $data['total_tax_amount'] = round(array_sum(array_column($sum, 'purch_tax')), 2);
        $data['total_purch_frieght'] = round(array_sum(array_column($sum, 'purch_frieght')), 2);
        $data['total_purch_discount'] = round(array_sum(array_column($sum, 'purch_discount')), 2);
        //die(   $data['total_purch_frieght'] );
        $data['amount_exclude_tax'] = $data['total_purchase_amount'] - $data['total_tax_amount'];
        $sum_cash = $qry->with($rltn)->where('purch_pay_type', 2)->get()->toArray();

        $purchase_amount_cash = round(array_sum(array_column($sum_cash, 'purchase_amount')), 2);
        $data['purchase_amount_cash'] = number_format((float) $purchase_amount_cash, 2, '.', '');

        $credit_sum = $data['total_purchase_amount'] - $purchase_amount_cash;
        $data['purchase_amount_credit'] = number_format((float) $credit_sum, 2, '.', '');

        $total_days = count($dates);
        $total_supplier = count($supps);

        $data['total_days'] = isset($total_days) ? $total_days : 0;
        $data['total_supplier'] = isset($total_supplier) ? $total_supplier : 0;
        $array = $out = [];
        foreach ($data['data'] as $k => $v) {
            $out[$v['purch_date']][] = $v;
        }
        $j = 0;

        foreach ($out as $k => $v) {

            $list['date'] = $k;

            $list['tot_amount'] = array_sum(array_column($v, 'purchase_amount'));
            $list['tot_tax'] = array_sum(array_column($v, 'purch_tax'));
            $list['list'] = $v;

            $array[] = $list;

            $j++;
        }

        if (isset($rq['is_year_rep']) && $rq['is_year_rep']) {
            $data['report_type'] = 'year';
        } else {
            $data['report_type'] = 'date';
        }

        //return $array;
        unset($data['data']);
        $data['data'] = $array;

        if ($rq['period_type'] != "") {
            $data['Date']['date1'] = date("d-F-Y", strtotime($date1));
            $data['Date']['date2'] = date("d-F-Y", strtotime($date2));
        } else {
            $data['Date'] = "";
        }

        return $data;
    }
    public function datesFromPeriods($pt, $rq)
    {
        switch ($pt) {

            case 't':
                $date1 = $date2 = date('Y-m-d');
                break;

            case 'ld':
                $date1 = $date2 = date('Y-m-d', strtotime("-1 days"));
                break;

            case 'lw':
                $previous_week = strtotime("-1 week +1 day");
                $start_week = strtotime("last sunday midnight", $previous_week);
                $end_week = strtotime("next saturday", $start_week);
                $date1 = date("Y-m-d", $start_week);
                $date2 = date("Y-m-d", $end_week);
                break;

            case 'lm':
                $date1 = date("Y-m-d", strtotime("first day of previous month"));
                $date2 = date("Y-m-d", strtotime("last day of previous month"));
                break;

            case 'ly':
                $year = date('Y') - 1;
                $date1 = $year . "-01-01";
                $date2 = $year . "-12-31";
                break;

            case 'c':
                $date1 = isset($rq['date1']) ? date('Y-m-d', strtotime($rq['date1'])) : 0;
                $date2 = isset($rq['date2']) ? date('Y-m-d', strtotime($rq['date2'])) : 0;

                break;

            default:
                return false;
                break;
        }
        return [$date1, $date2];
    }
    public function reportByPurchase($rq)
    {

        $amnt = DB::raw('(purchsub_rate * purchsub_qty) as purchase_amount');
        $slct = ['purchsub_purch_id as purch_refno', 'purchsub_date', 'purchsub_prd_id', 'purchsub_qty', 'purchsub_rate', $amnt];
        $rltn = [
            'product' => function ($qr) {
                $qr->select('prd_id', 'prd_name', 'prd_cat_id', 'prd_barcode');
            },
        ];
        $qry = PurchaseSub::select($slct);
        $qry->join('products', 'products.prd_id', '=', 'purchase_subs.purchsub_prd_id');
        $qry->join('categories', 'categories.cat_id', '=', 'products.prd_cat_id');
        if ($ptype = isset($rq['period_type']) ? $rq['period_type'] : 0) {
            list($date1, $date2) = $this->datesFromPeriods($ptype, $rq);

            if ($date1 && $date2) {
                $qry = $qry->whereBetween('purchsub_date', [$date1, $date2]);
            }
        }

        $purch_filter = isset($rq['purch_filter']) ? $rq['purch_filter'] : 0;
        $purch_val = isset($rq['purch_val']) ? $rq['purch_val'] : 0;
        $purch_val2 = isset($rq['purch_val2']) ? $rq['purch_val2'] : 0;
        if ($purch_filter && $purch_val) {
            switch ($purch_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $qry = $qry->where('purchsub_purch_id', $purch_filter, $purch_val);
                    break;
                case 'between':
                    if ($purch_val2) {
                        $qry = $qry->whereBetween('purchsub_purch_id', [$purch_val, $purch_val2]);
                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }

        $purch_qty_filter = isset($rq['purch_qty_filter']) ? $rq['purch_qty_filter'] : 0;
        $purch_qty = isset($rq['purch_qty']) ? $rq['purch_qty'] : 0;
        $purch_qty2 = isset($rq['purch_qty2']) ? $rq['purch_qty2'] : 0;
        if ($purch_qty_filter && $purch_qty) {
            switch ($purch_qty_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $qry = $qry->where('purchsub_qty', $purch_qty_filter, $purch_qty);
                    break;
                case 'between':
                    if ($purch_qty2) {
                        $qry = $qry->whereBetween('purchsub_qty', [$purch_qty, $purch_qty2]);
                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }

        $purch_rate_filter = isset($rq['purch_rate_filter']) ? $rq['purch_rate_filter'] : 0;
        $purch_rate = isset($rq['purch_rate']) ? $rq['purch_rate'] : 0;
        $purch_rate2 = isset($rq['purch_rate2']) ? $rq['purch_rate2'] : 0;
        if ($purch_rate_filter && $purch_rate) {
            switch ($purch_rate_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $qry = $qry->where('purchsub_rate', $purch_rate_filter, $purch_rate);
                    break;
                case 'between':
                    if ($purch_rate2) {
                        $qry = $qry->whereBetween('purchsub_rate', [$purch_rate, $purch_rate2]);
                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }

        if ($rq['prd_id'] != "") {
            $qry = $qry->where('purchsub_prd_id', $rq['prd_id']);
        }

        if ($rq['cat_id'] != "" || $rq['manfact_id'] != "") {

            $prdctQr = Product::select('prd_id');
            if ($rq['cat_id'] != '') {
                $prdctQr = $prdctQr->where('prd_cat_id', $rq['cat_id']);
            }
            if ($rq['manfact_id'] != '') {
                $prdctQr = $prdctQr->where('prd_supplier', $rq['manfact_id']);
            }
            $prdIds = $prdctQr->get()->pluck('prd_id')->toArray();
            if (empty($prdIds)) {
                return [];
            } else {
                $qry = $qry->whereIn('purchsub_prd_id', $prdIds);
            }

        }

        if ($rq['sup_id'] != "") {

            $purchIds = Purchase::where('purch_supp_id', $rq['sup_id'])
                ->get()->pluck('purch_id')->toArray();
            if (empty($purchIds)) {
                return [];
            } else {
                $qry = $qry->whereIn('purchsub_purch_id', $purchIds);
            }

        }
        if ($this->branch_id > 0) {
            $qry = $qry->where('purchase_subs.branch_id', $this->branch_id);
        } else {

            if ($this->active_branch_id) {
                if ($this->active_branch_id == 'All') {

                } else {
                    $qry = $qry->where('purchase_subs.branch_id', $this->active_branch_id);

                }
            }

        }

        $qry = $qry->where('purchsub_flag', 1);

        $tot = $qry->with($rltn)->orderby('categories.cat_name', 'ASC')->get();
// return $tot;

        if (count($tot) > 0) {
            $tq = $ta = $tp = $tc = 0;
            foreach ($tot as $k => $val) {
                $tq = $val['purchsub_qty'] + $tq;
                $ta = $val['purchase_amount'] + $ta;
                $prd_count[$val['product']['prd_id']] = $k;
                $cat_count[$val['product']['prd_cat_id']] = $k;
            }
            $total = ['total_qty' => $tq, 'purchase_amount' => $ta, 'total_products' => count($prd_count), 'total_cats' => count($cat_count)];

            $data = $qry->with($rltn)->orderBy('purchsub_purch_id', 'ASC')->paginate($this->per_page)->toArray();
            foreach ($data['data'] as $k => $val) {
                $out[$val['product']['prd_cat_id']][$val['purchsub_prd_id']][] = $val;
            }

            $i = 0;
            $k = 0;
            foreach ($out as $cat_id => $val) {

                $cat_array = Category::select('cat_name', 'cat_id')->where('cat_id', $cat_id)->first();
                // $result[$i]['category'][] = $cat_array;

                $cat_tot_qty = $cat_tot_amount = 0;
                foreach ($val as $prd_id => $purchase) {
                    $tot_qty = $tot_amount = 0;
                    $purchDet['prd_name'] = $purchase[0]['product']['prd_name'];
                    foreach ($purchase as $key => $sub) {
                        if ($sub['purchsub_prd_id'] == $prd_id) {

                            $sub['prd_barcode'] = $sub['product']['prd_barcode'];
                            $result[$i]['product'][$k]['purchase'][] = $sub;
                            $tot_qty = $tot_qty + $sub['purchsub_qty'];
                            $tot_amount = $tot_amount + $sub['purchase_amount'];

                        }
                    }
                    $cat_tot_qty = $cat_tot_qty + $tot_qty;
                    $cat_tot_amount = $cat_tot_amount + $tot_amount;
                    $purchDet['purchsub_qty'] = $tot_qty;
                    $purchDet['purchase_amount'] = $tot_amount;
                    $purchDet['purchsub_rate'] = round($tot_amount / $tot_qty);

                    $result[$i]['product'][$k]['item'][] = $purchDet;
                    $k++;

                }
                $cat_array['purchsub_qty'] = $cat_tot_qty;
                $cat_array['purchase_amount'] = $cat_tot_amount;
                $cat_array['purchsub_rate'] = round($cat_tot_amount / $cat_tot_qty);

                $result[$i]['category'][] = $cat_array;
                $i++;
            }

            foreach ($result as $key => $val) {
                unset($result[$key]['product']);
                foreach ($val['product'] as $k => $out) {
                    $result[$key]['products'][] = $out;
                }
            }

            $data['data'] = $result;
            $data['allTotal'] = $total;

            if ($rq['period_type'] != "") {
                $data['Date']['date1'] = date("d-F-Y", strtotime($date1));
                $data['Date']['date2'] = date("d-F-Y", strtotime($date2));
            } else {
                $data['Date'] = "";
            }

            return $data;

        } else {
            return 'No Data Found';
        }
    }

    public function reportByProduct($rq)
    {

        // purchase & return select
        if ($rq['rep_type'] == 1) {
            $vanSelqty = DB::raw('sum(purchsub_qty) as trans_stock_qty');
            $vanSeltransRetqty = DB::raw('(SELECT sum(purchretsub_qty) FROM erp_purchase_return_sub WHERE erp_purchase_return_sub.purchretsub_prd_id = erp_purchase_subs.purchsub_prd_id and erp_purchase_subs.branch_id = erp_purchase_return_sub.branch_id) as ret_stock_qty');

        } elseif ($rq['rep_type'] == 3) {
            $vanSelqty = DB::raw('(SELECT sum(purchretsub_qty) FROM erp_purchase_return_sub WHERE erp_purchase_return_sub.purchretsub_prd_id = erp_purchase_subs.purchsub_prd_id and erp_purchase_subs.branch_id = erp_purchase_return_sub.branch_id and erp_purchase_return_sub.purchretsub_unit_id = erp_purchase_subs.purchsub_unit_id) as purchsub_qty');
        } else {
            $vanSelqty = DB::raw('sum(purchsub_qty) as purchsub_qty');

        }

        // purchase & return select end

        $amnt = DB::raw('sum(purchsub_rate * purchsub_qty) as purchase_amount');
        if ($rq['rep_type'] == 1) {
            $slct = ['purchsub_purch_id', 'purchsub_prd_id', 'purchsub_rate', $amnt, $vanSelqty, $vanSeltransRetqty];

        } else {
            $slct = ['purchsub_purch_id', 'purchsub_prd_id', 'purchsub_rate', $amnt, $vanSelqty];
        }
        $rltn = [
            'product' => function ($qr) {
                $qr->select('prd_id', 'prd_name', 'prd_cat_id', 'prd_barcode');
            },
        ];
        $qry = PurchaseSub::select($slct);

        // Purchase & Return Filter

        if ($rq['rep_type'] == 1) {
            $qry->leftjoin('purchase_return_sub', function ($join) {
                $join->on('purchase_return_sub.purchretsub_prd_id', '=', 'purchase_subs.purchsub_prd_id');
                $join->on('purchase_subs.purchsub_unit_id', '=', 'purchase_return_sub.purchretsub_unit_id');

            });
        }
        if ($rq['rep_type'] == 3) {
            $qry->join('purchase_return_sub', function ($join) {
                $join->on('purchase_return_sub.purchretsub_prd_id', '=', 'purchase_subs.purchsub_prd_id');
                $join->on('purchase_subs.purchsub_unit_id', '=', 'purchase_return_sub.purchretsub_unit_id');

            });
        }

        // purchase & return filte end

        if ($ptype = isset($rq['period_type']) ? $rq['period_type'] : 0) {
            list($date1, $date2) = $this->datesFromPeriods($ptype, $rq);

            if ($date1 && $date2) {
                $qry = $qry->whereBetween('purchsub_date', [$date1, $date2]);
            }
        }

        $purch_filter = isset($rq['purch_filter']) ? $rq['purch_filter'] : 0;
        $purch_val = isset($rq['purch_val']) ? $rq['purch_val'] : 0;
        $purch_val2 = isset($rq['purch_val2']) ? $rq['purch_val2'] : 0;
        if ($purch_filter && $purch_val) {
            switch ($purch_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $qry = $qry->where('purchsub_purch_id', $purch_filter, $purch_val);
                    break;
                case 'between':
                    if ($purch_val2) {
                        $qry = $qry->whereBetween('purchsub_purch_id', [$purch_val, $purch_val2]);
                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }

        $purch_qty_filter = isset($rq['purch_qty_filter']) ? $rq['purch_qty_filter'] : 0;
        $purch_qty = isset($rq['purch_qty']) ? $rq['purch_qty'] : 0;
        $purch_qty2 = isset($rq['purch_qty2']) ? $rq['purch_qty2'] : 0;
        if ($purch_qty_filter && $purch_qty) {
            switch ($purch_qty_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $qry = $qry->where('purchsub_qty', $purch_qty_filter, $purch_qty);
                    break;
                case 'between':
                    if ($purch_qty2) {
                        $qry = $qry->whereBetween('purchsub_qty', [$purch_qty, $purch_qty2]);
                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }

        $purch_rate_filter = isset($rq['purch_rate_filter']) ? $rq['purch_rate_filter'] : 0;
        $purch_rate = isset($rq['purch_rate']) ? $rq['purch_rate'] : 0;
        $purch_rate2 = isset($rq['purch_rate2']) ? $rq['purch_rate2'] : 0;
        if ($purch_rate_filter && $purch_rate) {
            switch ($purch_rate_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $qry = $qry->where('purchsub_rate', $purch_rate_filter, $purch_rate);
                    break;
                case 'between':
                    if ($purch_rate2) {
                        $qry = $qry->whereBetween('purchsub_rate', [$purch_rate, $purch_rate2]);
                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }

        if ($rq['prd_id'] != "") {
            $qry = $qry->where('purchsub_prd_id', $rq['prd_id']);
        }

        if (isset($rq['gd_id']) && is_int($rq['gd_id'])) {
            $qry = $qry->where('purchsub_gd_id', $rq['gd_id']);
        }

        if ($rq['cat_id'] != "" || $rq['manfact_id'] != "") {

            $prdctQr = Product::select('prd_id');
            if ($rq['cat_id'] != '') {
                $prdctQr = $prdctQr->where('prd_cat_id', $rq['cat_id']);
            }
            if ($rq['manfact_id'] != '') {
                $prdctQr = $prdctQr->where('prd_supplier', $rq['manfact_id']);
            }
            $prdIds = $prdctQr->get()->pluck('prd_id')->toArray();
            if (empty($prdIds)) {
                return [];
            } else {
                $qry = $qry->whereIn('purchsub_prd_id', $prdIds);
            }

        }

        if ($rq['sup_id'] != "") {

            $purchIds = Purchase::where('purch_supp_id', $rq['sup_id'])
                ->get()->pluck('purch_id')->toArray();
            if (empty($purchIds)) {
                return [];
            } else {
                $qry = $qry->whereIn('purchsub_purch_id', $purchIds);
            }

        }
        if ($this->branch_id > 0) {
            $qry = $qry->where('purchase_subs.branch_id', $this->branch_id);
        } else {

            if ($this->active_branch_id == 'All') {

            } else {
                $qry = $qry->where('purchase_subs.branch_id', $this->active_branch_id);
            }

        }

        $qry = $qry->where('purchsub_flag', 1);

        $tot = $qry->with($rltn)->groupBy('purchsub_prd_id')->orderBy('purchsub_purch_id', 'ASC')->get();
        if ($rq['rep_type'] == 1) {
            foreach ($tot as $key => $val) {
                $tot[$key]['purchsub_qty'] = $val['trans_stock_qty'] - $val['ret_stock_qty'];
            }
        }

        // return $tot;

        if (count($tot) > 0) {
            $tq = $ta = $tp = $tc = 0;
            foreach ($tot as $k => $val) {
                $tq = $val['purchsub_qty'] + $tq;
                $ta = $val['purchase_amount'] + $ta;
                $prd_count[$val['product']['prd_id']] = $k;
                $cat_count[$val['product']['prd_cat_id']] = $k;
            }
            $total = ['total_qty' => $tq, 'purchase_amount' => $ta, 'total_products' => count($prd_count), 'total_cats' => count($cat_count)];

            $data = $qry->with($rltn)->groupBy('purchsub_prd_id')->orderBy('purchsub_purch_id', 'ASC')->paginate($this->per_page)->toArray();
            if ($rq['rep_type'] == 1) {
                foreach ($data['data'] as $key => $val) {
                    $data['data'][$key]['purchsub_qty'] = $val['trans_stock_qty'] - $val['ret_stock_qty'];
                }
            }

            // return $data;
            foreach ($data['data'] as $k => $val) {
                $out[$val['product']['prd_cat_id']][$val['purchsub_prd_id']][] = $val;
            }

            $i = 0;
            $k = 0;
            foreach ($out as $cat_id => $val) {

                $cat_array = Category::select('cat_name', 'cat_id')->where('cat_id', $cat_id)->first();
                // $result[$i]['category'] = $cat_array;

                $cat_tot_qty = $cat_tot_amount = 0;
                foreach ($val as $prd_id => $purchase) {
                    $tot_qty = $tot_amount = 0;
                    $purchDet['prd_name'] = $purchase[0]['product']['prd_name'];
                    foreach ($purchase as $key => $sub) {
                        if ($sub['purchsub_prd_id'] == $prd_id) {
                            $sub['prd_barcode'] = $sub['product']['prd_barcode'];
                            $result[$i]['product'][$k]['purchase'][] = $sub;

                            $tot_qty = $tot_qty + $sub['purchsub_qty'];
                            $tot_amount = $tot_amount + $sub['purchase_amount'];

                        }
                    }
                    $cat_tot_qty = $cat_tot_qty + $tot_qty;
                    $cat_tot_amount = $cat_tot_amount + $tot_amount;
                    $purchDet['purchsub_qty'] = $tot_qty;
                    $purchDet['purchase_amount'] = $tot_amount;
                    $purchDet['purchsub_rate'] = round($tot_amount / $tot_qty);

                    $result[$i]['product'][$k]['item'][] = $purchDet;
                    $k++;

                }
                $cat_array['purchsub_qty'] = $cat_tot_qty;
                $cat_array['purchase_amount'] = $cat_tot_amount;
                $cat_array['purchsub_rate'] = round($cat_tot_amount / $cat_tot_qty);

                $result[$i]['category'][] = $cat_array;
                $i++;
            }

            foreach ($result as $key => $val) {
                unset($result[$key]['product']);
                foreach ($val['product'] as $k => $out) {
                    $result[$key]['products'][] = $out;
                }
            }

            $data['data'] = $result;
            $data['allTotal'] = $total;

            // Transfer & Returns Type
            if ($rq['rep_type'] == 2) {
                $data['reportType'] = 'Transfer Report';
            } elseif ($rq['rep_type'] == 3) {
                $data['reportType'] = 'Returns Report';
            } elseif ($rq['rep_type'] == 1) {
                $data['reportType'] = 'Transfer & Returns Report';

            }

            if ($rq['period_type'] != "") {
                $data['Date']['date1'] = date("d-F-Y", strtotime($date1));
                $data['Date']['date2'] = date("d-F-Y", strtotime($date2));
            } else {
                $data['Date'] = "";
            }

            return $data;
        } else {
            return '';
        }
    }

    public function purchaseReturnSummary($rq)
    {

        // $company_branch = trim($rq->branch ? $rq->branch : 0);
        // $this->branch_id = trim(($this->usr_type == 2) ? $company_branch : $this->branch_id);

        $pur_ref_filter = isset($rq['pur_ref_filter']) ? $rq['pur_ref_filter'] : 0;
        $ret_ref_filter = isset($rq['ret_ref_filter']) ? $rq['ret_ref_filter'] : 0;

        $pur_val = isset($rq['pur_val']) ? $rq['pur_val'] : 0;
        $pur_val2 = isset($rq['pur_val2']) ? $rq['pur_val2'] : 0;
        $ret_val = isset($rq['ret_val']) ? $rq['ret_val'] : 0;
        $ret_val2 = isset($rq['ret_val2']) ? $rq['ret_val2'] : 0;

        $price_filter = isset($rq['purch_price']) ? $rq['purch_price'] : 0;
        $price1 = isset($rq['price1']) ? $rq['price1'] : 0;
        $price2 = isset($rq['price2']) ? $rq['price2'] : 0;

        // $dsnt_filter = isset($rq['purch_discount']) ? $rq['purch_discount'] : 0;
        // $discount1 = isset($rq['discount1']) ? $rq['discount1'] : 0;
        // $discount2 = isset($rq['discount2']) ? $rq['discount2'] : 0;

        if (isset($rq['payment_type']) && $rq['payment_type']) {
            $whr['purchret_pay_type'] = $rq['payment_type'];
        }
        if (isset($rq['purch_supp_id']) && $rq['purch_supp_id']) {
            $whr['purchret_supp_id'] = $rq['purch_supp_id'];
        }
        if (isset($rq['purch_type']) && $rq['purch_type'] == 2) {

            $whr['purchret_flag'] = 0;
        } else {
            $whr['purchret_flag'] = 1;

        }
        if (isset($rq['is_year_rep']) && $rq['is_year_rep']) {
            $date = DB::raw('MONTHNAME(purchret_date) as purchret_date');
        } else {
            $date = 'purchret_date';

        }

        $slct = ['purchret_id as ret_rf_no', 'purchret_refno', $date, 'purchret_inv_no', 'purchret_type', 'purchret_supp_id', 'purchret_pay_type',
            'purchret_inv_date', 'purchret_amount', 'purchret_tax'];

        $rltn = [

            'supplier' => function ($qr) {
                $qr->select('supp_id', 'supp_name');
            },
            'ledger' => function ($qr) {
                $qr->select('*');
            },
        ];
        $qry = PurchaseReturn::select($slct)->with($rltn);

        if ($ptype = isset($rq['period_type']) ? $rq['period_type'] : 0) {
            list($date1, $date2) = $this->datesFromPeriods($ptype, $rq);

            if ($date1 && $date2) {
                $qry = $qry->whereBetween('purchret_date', [$date1, $date2]);
            }
        }

        $qry = isset($whr) ? $qry->where($whr) : $qry;

        if ($pur_ref_filter && $pur_val) {
            switch ($pur_ref_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $qry = $qry->where('purchret_refno', $pur_ref_filter, $pur_val);
                    break;
                case 'between':
                    if ($pur_val) {
                        $qry = $qry->whereBetween('purchret_refno', [$pur_val, $pur_val2]);
                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }

        if ($ret_ref_filter && $ret_val) {
            switch ($ret_ref_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $qry = $qry->where('purchret_id', $ret_ref_filter, $ret_val);
                    break;
                case 'between':
                    if ($ret_val) {
                        $qry = $qry->whereBetween('purchret_id', [$ret_val, $ret_val2]);
                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }

        if ($price_filter && $price1) {
            switch ($price_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $qry = $qry->where('purchret_amount', $price_filter, $price1);
                    break;
                case 'between':
                    if ($price2) {
                        $qry = $qry->whereBetween('purchret_amount', [$price1, $price2]);
                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }

        if ($this->branch_id > 0) {
            $qry = $qry->where('purchase_return.branch_id', $this->branch_id);
        } else {

            if ($this->active_branch_id == 'All') {

            } else {
                $qry = $qry->where('purchase_return.branch_id', $this->active_branch_id);
            }

        }

        $sum = $qry->with($rltn)->get()->toArray();

        foreach ($sum as $k => $sval) {

            $sum[$k]['return_amt'] = $sval['purchret_amount'] + $sval['purchret_tax'];

        }
        // return $sum;
        $typeCashCount = 0;
        $typeCashAmt = 0;
        $typeCreditCount = 0;
        $typeCreditAmt = 0;
        foreach ($sum as $k => $val) {
            if ($val['purchret_pay_type'] == 1) {
                $typeCreditAmt += $val['return_amt'];
                $typeCreditCount++;
            } elseif ($val['purchret_pay_type'] == 2) {
                $typeCashAmt += $val['return_amt'];
                $typeCashCount++;
            }
        }

        // return $sum;
        if (isset($rq['export']) && $rq['export'] == 'export') {
            $data['data'] = $qry->orderBy('purchret_date', 'ASC')->get()->toArray();
        } else {
            $data = $qry->orderBy('purchret_date', 'ASC')->paginate(31)->toArray();
        }

        foreach ($data['data'] as $k => $val) {

            $data['data'][$k]['return_amt'] = $val['purchret_amount'] + $val['purchret_tax'];

        }
        $array = $out = [];
        foreach ($data['data'] as $k => $v) {
            $out[$v['purchret_date']][] = $v;
        }

        $data['report']['total_purchase'] = count($sum);
        $amnt = array_sum(array_column($sum, 'return_amt'));
        //   return array_column($sum, 'return_amt');

        $j = 0;

        foreach ($out as $k => $v) {

            $list['date'] = $k;

            $list['tot_amount'] = array_sum(array_column($v, 'return_amt'));
            $list['tot_tax'] = array_sum(array_column($v, 'purchret_tax'));
            $list['list'] = $v;

            $array[] = $list;

            $j++;
        }

        if (isset($rq['is_year_rep']) && $rq['is_year_rep']) {
            $data['report_type'] = 'year';
        } else {
            $data['report_type'] = 'date';
        }

        $data['report']['total_cash_count'] = $typeCashCount;
        $data['report']['total_credit_count'] = $typeCreditCount;
        $data['report']['total_cash'] = $typeCashAmt;
        $data['report']['total_credit'] = $typeCreditAmt;
        $data['report']['total_return_amt'] = $amnt;
        if ($rq['period_type'] != "") {
            $data['Date']['date1'] = date("d-F-Y", strtotime($date1));
            $data['Date']['date2'] = date("d-F-Y", strtotime($date2));
        } else {
            $data['Date'] = "";
        }
        //return $array;
        unset($data['data']);
        $data['data'] = $array;

        return $data;
    }

    public function returnByProduct($rq)
    {

        $amnt = DB::raw('sum(purchretsub_rate * purchretsub_qty) as purchase_amount');
        $qty = DB::raw('sum(purchretsub_qty) as purchretsub_qty');
        $slct = ['purchretsub_purch_id', 'purchase_return.purchret_id', 'purchase_return.purchret_refno', 'purchretsub_prd_id', 'purchretsub_date', $qty, 'purchretsub_rate', $amnt];
        $rltn = [
            'product' => function ($qr) {
                $qr->select('prd_id', 'prd_name', 'prd_cat_id', 'prd_barcode');
            },
        ];
        $qry = PurchaseReturnSub::select($slct);

        $qry->join('purchase_return', 'purchase_return.purchret_id', '=', 'purchase_return_sub.purchretsub_purch_id');

        if ($ptype = isset($rq['period_type']) ? $rq['period_type'] : 0) {
            list($date1, $date2) = $this->datesFromPeriods($ptype, $rq);

            if ($date1 && $date2) {
                $qry = $qry->whereBetween('purchretsub_date', [$date1, $date2]);
            }
        }

        $purch_filter = isset($rq['purch_filter']) ? $rq['purch_filter'] : 0;
        $purch_val = isset($rq['purch_val']) ? $rq['purch_val'] : 0;
        $purch_val2 = isset($rq['purch_val2']) ? $rq['purch_val2'] : 0;
        if ($purch_filter && $purch_val) {
            switch ($purch_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $qry = $qry->where('purchretsub_purch_id', $purch_filter, $purch_val);
                    break;
                case 'between':
                    if ($purch_val2) {
                        $qry = $qry->whereBetween('purchretsub_purch_id', [$purch_val, $purch_val2]);
                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }

        $purch_qty_filter = isset($rq['purch_qty_filter']) ? $rq['purch_qty_filter'] : 0;
        $purch_qty = isset($rq['purch_qty']) ? $rq['purch_qty'] : 0;
        $purch_qty2 = isset($rq['purch_qty2']) ? $rq['purch_qty2'] : 0;
        if ($purch_qty_filter && $purch_qty) {
            switch ($purch_qty_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $qry = $qry->where('purchretsub_qty', $purch_qty_filter, $purch_qty);
                    break;
                case 'between':
                    if ($purch_qty2) {
                        $qry = $qry->whereBetween('purchretsub_qty', [$purch_qty, $purch_qty2]);
                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }

        $purch_rate_filter = isset($rq['purch_rate_filter']) ? $rq['purch_rate_filter'] : 0;
        $purch_rate = isset($rq['purch_rate']) ? $rq['purch_rate'] : 0;
        $purch_rate2 = isset($rq['purch_rate2']) ? $rq['purch_rate2'] : 0;
        if ($purch_rate_filter && $purch_rate) {
            switch ($purch_rate_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $qry = $qry->where('purchretsub_rate', $purch_rate_filter, $purch_rate);
                    break;
                case 'between':
                    if ($purch_rate2) {
                        $qry = $qry->whereBetween('purchretsub_rate', [$purch_rate, $purch_rate2]);
                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }

        if ($rq['prd_id'] != "") {
            $qry = $qry->where('purchretsub_prd_id', $rq['prd_id']);
        }

        if ($rq['cat_id'] != "" || $rq['manfact_id'] != "") {

            $prdctQr = Product::select('prd_id');
            if ($rq['cat_id'] != '') {
                $prdctQr = $prdctQr->where('prd_cat_id', $rq['cat_id']);
            }
            if ($rq['manfact_id'] != '') {
                $prdctQr = $prdctQr->where('prd_supplier', $rq['manfact_id']);
            }
            $prdIds = $prdctQr->get()->pluck('prd_id')->toArray();
            if (empty($prdIds)) {
                return [];
            } else {
                $qry = $qry->whereIn('purchretsub_prd_id', $prdIds);
            }

        }

        if ($rq['sup_id'] != "") {

            $purchIds = PurchaseReturn::where('purchret_supp_id', $rq['sup_id'])
                ->get()->pluck('purchret_id')->toArray();
            if (empty($purchIds)) {
                return [];
            } else {
                $qry = $qry->whereIn('purchret_id', $purchIds);
            }

        }

        if ($this->branch_id > 0) {
            $qry = $qry->where('purchase_return_sub.branch_id', $this->branch_id);
        } else {

            if ($this->active_branch_id == 'All') {

            } else {
                $qry = $qry->where('purchase_return_sub.branch_id', $this->active_branch_id);
            }

        }

        $tot = $qry->with($rltn)->groupBy('purchretsub_purch_id')->orderBy('purchretsub_purch_id', 'ASC')->get();
        if (count($tot) > 0) {
            $tq = $ta = $tp = $tc = 0;
            foreach ($tot as $k => $val) {
                $tq = $val['purchretsub_qty'] + $tq;
                $ta = $val['purchase_amount'] + $ta;
                $prd_count[$val['product']['prd_id']] = $k;
                $cat_count[$val['product']['prd_cat_id']] = $k;
            }
            $total = ['total_qty' => $tq, 'purchase_amount' => $ta, 'total_products' => count($prd_count), 'total_cats' => count($cat_count)];

            $data = $qry->with($rltn)->groupBy('purchretsub_prd_id')->orderBy('purchretsub_purch_id', 'ASC')->paginate($this->per_page)->toArray();

            foreach ($data['data'] as $k => $val) {
                $out[$val['product']['prd_cat_id']][$val['purchretsub_prd_id']][] = $val;
            }

            $i = 0;
            $k = 0;
            foreach ($out as $cat_id => $val) {

                $cat_array = Category::select('cat_name', 'cat_id')->where('cat_id', $cat_id)->first();
                // $result[$i]['category'] = $cat_array;

                $cat_tot_qty = $cat_tot_amount = 0;
                foreach ($val as $prd_id => $purchase) {
                    $tot_qty = $tot_amount = 0;
                    $purchDet['prd_name'] = $purchase[0]['product']['prd_name'];
                    foreach ($purchase as $key => $sub) {
                        if ($sub['purchretsub_prd_id'] == $prd_id) {
                            $sub['prd_barcode'] = $sub['product']['prd_barcode'];
                            $result[$i]['product'][$k]['purchase'][] = $sub;

                            $tot_qty = $tot_qty + $sub['purchretsub_qty'];
                            $tot_amount = $tot_amount + $sub['purchase_amount'];

                        }
                    }
                    $cat_tot_qty = $cat_tot_qty + $tot_qty;
                    $cat_tot_amount = $cat_tot_amount + $tot_amount;
                    $purchDet['purchsub_qty'] = $tot_qty;
                    $purchDet['purchase_amount'] = $tot_amount;
                    $purchDet['purchsub_rate'] = round($tot_amount / $tot_qty);

                    $result[$i]['product'][$k]['item'][] = $purchDet;
                    $k++;

                }
                $cat_array['purchretsub_qty'] = $cat_tot_qty;
                $cat_array['purchase_amount'] = $cat_tot_amount;
                $cat_array['purchsub_rate'] = round($cat_tot_amount / $cat_tot_qty);

                $result[$i]['category'][] = $cat_array;
                $i++;
            }

            foreach ($result as $key => $val) {
                unset($result[$key]['product']);
                foreach ($val['product'] as $k => $out) {
                    $result[$key]['products'][] = $out;
                }
            }

            if ($rq['period_type'] != "") {
                $data['Date']['date1'] = date("d-F-Y", strtotime($date1));
                $data['Date']['date2'] = date("d-F-Y", strtotime($date2));
            } else {
                $data['Date'] = "";
            }

            $data['data'] = $result;
            $data['allTotal'] = $total;
            return $data;
        } else {
            return '';
        }
    }

    public function purchaseandReturnSummary($rq)
    {

        $amnt = DB::raw('(purch_amount + purch_tax + purch_frieght - purch_discount) as net_amount');
        $retamnt = DB::raw('(purchret_amount + purchret_tax + purchret_frieght - purchret_discount) as net_amount');

        $purchase = DB::table("purchases")
            ->select("purchases.purch_id as purch_ref_no", "purchases.purch_inv_no as inv_no"
                , "purchases.purch_supp_id as supp", DB::raw('1 as type')
                , "purchases.purch_date as date", $amnt);

        $purchase = $purchase->where('purch_flag', 1);

        if ($ptype = isset($rq['period_type']) ? $rq['period_type'] : 0) {

            list($date1, $date2) = $this->datesFromPeriods($ptype, $rq);

            if ($date1 && $date2) {

                $purchase = $purchase->whereBetween('purch_date', [$date1, $date2]);

            }
            // return "dsf";
        }

        $purch_ref_filter = $rq['purch_ref_filter'];
        $purchref_val1 = $rq['purchref_val1'];
        $purchref_val2 = $rq['purchref_val2'];

        if ($purch_ref_filter && $purchref_val1) {
            switch ($purch_ref_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $purchase = $purchase->where('purch_id', $purch_ref_filter, $purchref_val1);
                    break;
                case 'between':
                    if ($purchref_val1) {
                        $purchase = $purchase->whereBetween('purch_id', [$purchref_val1, $purchref_val2]);
                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }

        $voucher_filter = $rq['voucher_filter'];
        $vchr_val1 = $rq['vchr_val1'];
        $vchr_val2 = $rq['vchr_val2'];

        if ($voucher_filter && $vchr_val1) {

            switch ($voucher_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $purchase = $purchase->where('purch_inv_no', $voucher_filter, $vchr_val1);
                    break;
                case 'between':
                    if ($vchr_val1) {
                        $purchase = $purchase->whereBetween('purch_inv_no', [$vchr_val1, $vchr_val2]);
                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }
        if ($rq['purch_supp_id'] != '') {
            $purchase = $purchase->where('purch_supp_id', $rq['purch_supp_id']);
        }

        if ($rq['add_by'] != '') {
            $purchase = $purchase->where('purch_agent', $rq['add_by']);
        }
        if ($this->branch_id > 0) {
            $purchase = $purchase->where('purchases.branch_id', $this->branch_id);
        } else {
            $purchase = $purchase->where('purchases.branch_id', $this->active_branch_id);
        }

        if ($rq['rep_type'] == 1 || $rq['rep_type'] == 3) {

            $data = $return = DB::table("purchase_return")
                ->select("purchase_return.purchret_id as purch_ref_no"
                    , "purchase_return.purchret_inv_no as inv_no"
                    , "purchase_return.purchret_supp_id as supp", DB::raw('2 as type')
                    , "purchase_return.purchret_date as date", $retamnt);

            if ($rq['purch_supp_id'] != '') {
                $data = $return = $return->where('purchret_supp_id', $rq['purch_supp_id']);
            }

            if ($ptype = isset($rq['period_type']) ? $rq['period_type'] : 0) {

                list($date1, $date2) = $this->datesFromPeriods($ptype, $rq);

                if ($date1 && $date2) {

                    $data = $return = $return->whereBetween('purchret_date', [$date1, $date2]);

                }

            }

            $purch_ref_filter = $rq['purch_ref_filter'];
            $purchref_val1 = $rq['purchref_val1'];
            $purchref_val2 = $rq['purchref_val2'];

            if ($purch_ref_filter && $purchref_val1) {
                switch ($purch_ref_filter) {
                    case '<':
                    case '<=':
                    case '=':
                    case '>':
                    case '>=':
                        $data = $return = $return->where('purchret_id', $purch_ref_filter, $purchref_val1);
                        break;
                    case 'between':
                        if ($purchref_val1) {
                            $data = $return = $return->whereBetween('purchret_id', [$purchref_val1, $purchref_val2]);
                        }
                        break;
                    default:
                        return parent::displayError('Invalid Request', $this->badrequest_stat);
                        break;
                }
            }

            $voucher_filter = $rq['voucher_filter'];
            $vchr_val1 = $rq['vchr_val1'];
            $vchr_val2 = $rq['vchr_val2'];

            if ($voucher_filter && $vchr_val1) {

                switch ($voucher_filter) {
                    case '<':
                    case '<=':
                    case '=':
                    case '>':
                    case '>=':
                        $data = $return = $return->where('purchret_inv_no', $voucher_filter, $vchr_val1);
                        break;
                    case 'between':
                        if ($vchr_val1) {
                            $data = $return = $return->whereBetween('purchret_inv_no', [$vchr_val1, $vchr_val2]);
                        }
                        break;
                    default:
                        return parent::displayError('Invalid Request', $this->badrequest_stat);
                        break;
                }
            }

            $data = $return = $return->where('purchret_flag', 1);

            if ($rq['add_by'] != '') {
                $data = $return = $return->where('purchret_agent', $rq['add_by']);
            }

            if ($rq['rep_type'] == 1) {
                $data = $return = $return->union($purchase);
            }
            if ($this->branch_id > 0) {
                $data = $return = $return->where('branch_id', $this->branch_id);
            } else {
                $data = $return = $return->where('branch_id', $this->active_branch_id);
            }

        } else {
            $data = $purchase;
        }

        $allres = clone $data;

        $allres = $allres->orderby('date', 'desc')->get()->toArray();

        $data = $data->orderby('date', 'desc')->paginate($this->per_page)->toArray();

        // return $allres;
        $totalPurchasedAmt = 0;
        $totalReturnedAmt = 0;
        $balanceAmt = 0;

        foreach ($allres as $key => $val) {

            if ($val->type == 1) {
                $totalPurchasedAmt += $val->net_amount;
            } elseif ($val->type == 2) {
                $totalReturnedAmt += $val->net_amount;
            }
        }

        foreach ($data['data'] as $key => $val) {

            // if ($val->type == 1) {
            //     $totalPurchasedAmt += $val->net_amount;
            // } elseif ($val->type == 2) {
            //     $totalReturnedAmt += $val->net_amount;
            // }

            $data['data'][$key]->supp = $this->getSupplierName($val->supp);

        }

        $balanceAmt = $totalPurchasedAmt - $totalReturnedAmt;
        $data['totalCount']['purchaseTotal'] = $totalPurchasedAmt;
        $data['totalCount']['returnTotal'] = $totalReturnedAmt;
        $data['totalCount']['balanceAmt'] = $balanceAmt;

        if ($rq['rep_type'] == 2) {
            $data['reportType'] = 'Purchase Report';
        } elseif ($rq['rep_type'] == 3) {
            $data['reportType'] = 'Return Report';
        } elseif ($rq['rep_type'] == 1) {
            $data['reportType'] = 'Purchase & Returns Report';
        }

        if ($rq['period_type'] != "") {
            $data['Date']['date1'] = date("d-F-Y", strtotime($date1));
            $data['Date']['date2'] = date("d-F-Y", strtotime($date2));
        } else {
            $data['Date'] = "";
        }

        return $data;

    }

    public function getSupplierName($supp_id)
    {
        $res = Supplier::find($supp_id);
        return $res['supp_name'];
    }

}
