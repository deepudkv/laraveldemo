<?php

namespace App\Http\Controllers\Api\Reports;

use App\Http\Controllers\Api\ApiParent;
use App\Models\Company\Acc_branch;
use App\Models\Purchase\PurchaseReturnSub;
use App\Models\Purchase\PurchaseSub;
use App\Models\Sales\SalesReturnSub;
use App\Models\Sales\SalesSub;
use App\Models\Settings\TaxCategory;
use DB;
use Illuminate\Http\Request;

class BackupVatReport extends ApiParent
{

    public function Report(Request $request, $type)
    {

        switch ($type) {
            case 'input_vat_summary':
                return parent::displayData($this->inputVatSummary($request));
                break;
            case 'output_vat_summary':
                return parent::displayData($this->outVatSummary($request));
                break;

            case 'day_summary':
                return parent::displayData($this->daySummary($request));
                break;
            default:
                return parent::displayError('Invalid Endpoint', $this->badrequest_stat);
                break;
        }
    }

    public function daySummary($request)
    {

        $pt = $ptype = isset($request['period_type']) ? $request['period_type'] : 0;

        if ($pt) {

            if ($pt == 'c') {

                $date1 = isset($request['date1']) ? date('Y-m-d', strtotime($request['date1'])) : 0;
                $date2 = isset($request['date2']) ? date('Y-m-d', strtotime($request['date2'])) : 0;

            } else {
                if ($pt >= 0 && $pt <= 12) {
                    if ($pt < 10) {
                        $pt = "0" . $pt;
                    }
                    $date1 = date("Y-$pt-01");
                    $date2 = date("Y-m-t", strtotime($date1));

                }

                if ($pt == 'a') {
                    $year = date('Y');
                    $date1 = $year . "-01-01";
                    $date2 = $year . "-12-31";

                }
            }

        } else {

            $date1 = date("Y-m-01");
            $date2 = date("Y-m-t");
        }

        $dates = $this->getDatesFromRange($date1, $date2);
        $taxcats = TaxCategory::get()->toArray();
        $i = 0;
        $vat_sum = $net_vat = 0;
        foreach ($dates as $k => $d) {
            $amnt = DB::raw('SUM(purchsub_rate * purchsub_qty) as purch_amount');
            $rate = DB::raw('SUM(purchsub_tax*purchsub_qty) as vat');

            $qry = PurchaseSub::select($rate, $amnt)->where('purchsub_flag', '1')->where('branch_id', $this->branch_id);

            $retamnt = DB::raw('SUM(purchretsub_rate * purchretsub_qty) as purch_ret_amount');
            $retrate = DB::raw('SUM(purchretsub_tax*purchretsub_qty) as vat_deduct');

            $retqry = PurchaseReturnSub::select($retrate, $retamnt)->where('purchretsub_flag', '1')->where('branch_id', $this->branch_id);

            $standQry = clone $qry;
            $pstd = $standQry->where('purchase_subs.purchsub_tax_per', '5')
                ->where('purchsub_date', $d)->get()->toArray();
            $retstandQry = clone $retqry;
            $rstd = $retstandQry->where('purchase_return_sub.purchretsub_tax_per', '5')->where('purchretsub_date', $d)->get()->toArray();

            $zeroQry = clone $qry;
            $zpurch = $zeroQry->where('purchase_subs.purchsub_tax_per', '0')->where('purchsub_date', $d)->get()->toArray();
            $retzeroQry = clone $retqry;
            $rtz = $retzeroQry->where('purchase_return_sub.purchretsub_tax_per', '0')->where('purchretsub_date', $d)->get()->toArray();

            $exemQry = clone $qry;
            $epurch = $exemQry->where('purchase_subs.purchsub_tax_per', '50')->where('purchsub_date', $d)->get()->toArray();
            $retexemQry = clone $retqry;
            $ertn = $retexemQry->where('purchase_return_sub.purchretsub_tax_per', '50')->where('purchretsub_date', $d)->get()->toArray();

            $out[$i]['date'] = $d;
            $out[$i]['standardfive'] = $pstd[0]['purch_amount'] - $rstd[0]['purch_ret_amount'];
            $out[$i]['standardfifty'] = $epurch[0]['purch_amount'] - $ertn[0]['purch_ret_amount'];
            $out[$i]['zero_rated'] = $zpurch[0]['purch_amount'] - $rtz[0]['purch_ret_amount'];

            $out[$i]['standardfive_p'] = isset($pstd[0]['purch_amount']) ? $pstd[0]['purch_amount'] : 0;
            $out[$i]['standardfifty_p'] = isset($epurch[0]['purch_amount']) ? $epurch[0]['purch_amount'] : 0;
            $out[$i]['zero_rated_p'] = isset($zpurch[0]['purch_amount']) ? $zpurch[0]['purch_amount'] : 0;

            $out[$i]['standardfive_r'] = isset($rstd[0]['purch_ret_amount']) ? $rstd[0]['purch_ret_amount'] : 0;
            $out[$i]['standardfifty_r'] = isset($ertn[0]['purch_ret_amount']) ? $ertn[0]['purch_ret_amount'] : 0;
            $out[$i]['zero_rated_r'] = isset($rtz[0]['purch_ret_amount']) ? $rtz[0]['purch_ret_amount'] : 0;

            $vat = $pstd[0]['vat'] + $zpurch[0]['vat'] + $epurch[0]['vat'];
            $vat_deduct = $rstd[0]['vat_deduct'] + $rtz[0]['vat_deduct'] + $ertn[0]['vat_deduct'];

            $out[$i]['vat_purchase'] = $vat;
            $out[$i]['vat_return'] = $vat_deduct;

            $total_purchase = $pstd[0]['purch_amount'] + $zpurch[0]['purch_amount'] + $epurch[0]['purch_amount'];
            $out[$i]['vat'] = $vat - $vat_deduct;
            $out[$i]['total_purchase'] = $total_purchase;

            $amnt = DB::raw('SUM(salesub_rate * salesub_qty) as sale_amount');
            $vat = DB::raw('SUM(salesub_tax_rate*salesub_qty) as vat');

            $vatdeduct = DB::raw('SUM(salesretsub_tax_rate*salesretsub_qty) as vatdeduct');

            $retamnt = DB::raw('SUM(salesretsub_rate * 	salesretsub_qty) as sale_ret_amount');

            $vat_deduct = $vat_deduct2 = $sale_amount_total = 0;
            foreach ($taxcats as $k => $tax) {
                $taxcat_name = $tax['taxcat_name'];

                $taxcat_auto_id = $tax['taxcat_auto_id'];

                if ($taxcat_auto_id == 1) {
                    $display_name = 'five';
                }

                if ($taxcat_auto_id == 2) {
                    $display_name = 'fifty';
                }

                if ($taxcat_auto_id == 3) {
                    $display_name = 'zero';
                }
                $taxcat_id = $tax['taxcat_id'];
                $taxcat_tax_per = $tax['taxcat_tax_per'];

                $rslt = SalesSub::select($amnt, $vat)->where('salesub_taxcat_id', $taxcat_id)->where('branch_id', $this->branch_id)->where('salesub_flags', '1')->where('salesub_date', $d)->get()->toArray();
                $rtrslt = SalesReturnSub::select('salesretsub_sales_inv_no', 'salesretsub_date', 'salesretsub_tax_per',
                    'salesretsub_tax_rate', $vatdeduct
                    , $retamnt)->where('salesretsub_tax_per', $taxcat_tax_per)->where('branch_id', $this->branch_id)->where('salesretsub_date', $d)->where('salesretsub_flags', '1')->get()->toArray();

                $sale_amount = isset($rslt[0]['sale_amount']) ? $rslt[0]['sale_amount'] : 0;
                $sale_ret_amount = isset($rtrslt[0]['sale_ret_amount']) ? $rtrslt[0]['sale_ret_amount'] : 0;

                $vat_deduct = $vat_deduct + $rslt[0]['vat'];

                $vat_deduct2 = $vat_deduct2 + $rtrslt[0]['vatdeduct'];

                $netsale_amount = round($sale_amount - $sale_ret_amount, 2);
                $out[$i][$display_name] = $netsale_amount;
                $out[$i][$taxcat_auto_id] = $netsale_amount;

                $out[$i][$display_name . "_p"] = $sale_amount;
                $out[$i][$display_name . "_r"] = $sale_ret_amount;

                $sale_amount_total = $sale_amount_total + $netsale_amount;
                $out[$i]['sale_amount'] = $sale_amount_total;

                $out[$i]['vat_sale'] = $vat_deduct;
                $out[$i]['vat_salereturn'] = $vat_deduct2;
                $out[$i]['vat_deduct'] = round($vat_deduct - $vat_deduct2, 2);

            }

            $net_vat = $out[$i]['vat_deduct'] - $out[$i]['vat'];
            $out[$i]['net_vat'] = $net_vat;
            $vat_sum = $net_vat + $vat_sum;

            $i++;

        }

        $sum['1sum'] = array_sum(array_column($out, '1'));
        $sum['2sum'] = array_sum(array_column($out, '2'));
        $sum['3sum'] = array_sum(array_column($out, '3'));

        $sum['standardfive'] = array_sum(array_column($out, 'standardfive'));
        $sum['standardfifty'] = array_sum(array_column($out, 'standardfifty'));
        $sum['zero_rated'] = array_sum(array_column($out, 'zero_rated'));

        $sum['vat'] = array_sum(array_column($out, 'vat'));
        $sum['vat_deduct'] = array_sum(array_column($out, 'vat_deduct'));
        $sum['net_vat'] = array_sum(array_column($out, 'net_vat'));

        $sum['sale_amount'] = array_sum(array_column($out, 'sale_amount'));
        $sum['total_purchase'] = array_sum(array_column($out, 'total_purchase'));

        $sum['five'] = array_sum(array_column($out, 'five'));
        $sum['fifty'] = array_sum(array_column($out, 'fifty'));
        $sum['zero'] = array_sum(array_column($out, 'zero'));

        $info['vat_sale'] = array_sum(array_column($out, 'vat_sale'));
        $info['vat_salereturn'] = array_sum(array_column($out, 'vat_salereturn'));
        $info['vat_deduct'] = array_sum(array_column($out, 'vat_deduct'));

        $info['vat_purchase'] = array_sum(array_column($out, 'vat_purchase'));
        $info['vat_return'] = array_sum(array_column($out, 'vat_return'));
        $info['vat'] = array_sum(array_column($out, 'vat'));

        $info['standardfive'] = array_sum(array_column($out, 'standardfive'));
        $info['standardfifty'] = array_sum(array_column($out, 'standardfifty'));
        $info['zero_rated'] = array_sum(array_column($out, 'zero_rated'));

        $info['standardfive_p'] = array_sum(array_column($out, 'standardfive_p'));
        $info['standardfifty_p'] = array_sum(array_column($out, 'standardfifty_p'));
        $info['zero_rated_p'] = array_sum(array_column($out, 'zero_rated_p'));

        $info['standardfive_r'] = array_sum(array_column($out, 'standardfive_r'));
        $info['standardfifty_r'] = array_sum(array_column($out, 'standardfifty_r'));
        $info['zero_rated_r'] = array_sum(array_column($out, 'zero_rated_r'));
        $info['five'] = array_sum(array_column($out, 'five'));
        $info['fifty'] = array_sum(array_column($out, 'fifty'));
        $info['zero'] = array_sum(array_column($out, 'zero'));

        $info['five_p'] = array_sum(array_column($out, 'five_p'));
        $info['fifty_p'] = array_sum(array_column($out, 'fifty_p'));
        $info['zero_p'] = array_sum(array_column($out, 'zero_p'));

        $info['five_r'] = array_sum(array_column($out, 'five_r'));
        $info['fifty_r'] = array_sum(array_column($out, 'fifty_r'));
        $info['zero_r'] = array_sum(array_column($out, 'zero_r'));

        return ['data' => $out, 'date_start' => date("d-F-Y", strtotime($date1)),
            'date_end' => date("d-F-Y", strtotime($date2)),
            'info' => $info, 'sum' => [0 => $sum],
            'vat_sum' => $vat_sum,
            'branch' => Acc_branch::where('branch_id', $this->branch_id)->first(),
            'titles_purchase' => '', 'titles_sales' => ''];

    }

    public function getDatesFromRange($Date1, $Date2)
    {

        $array = array();

        $Variable1 = strtotime($Date1);
        $Variable2 = strtotime($Date2);

        for ($currentDate = $Variable1; $currentDate <= $Variable2;
            $currentDate += (86400)) {

            $Store = date('Y-m-d', $currentDate);
            $array[] = $Store;
        }
        return $array;
    }
    public function inputVatSummary($request)
    {
        // print_r($request['Selbranch']);die('d');

        $inv_filter = isset($request['inv_filter']) ? $request['inv_filter'] : 0;
        $inv_val = isset($request['inv_val']) ? $request['inv_val'] : 0;
        $inv_val2 = isset($request['inv_val2']) ? $request['inv_val2'] : 0;

        // Purchase section
        $amnt = DB::raw('SUM(purchsub_rate * purchsub_qty) as purch_amount');
        $rate = DB::raw('SUM(purchsub_tax*purchsub_qty) as tax_rate');

        $qry = PurchaseSub::select('purchase_subs.purchsub_purch_id', 'purchase_subs.purchsub_date'
            , $rate, $amnt, 'purchase_subs.branch_id', 'purchases.purch_inv_no', 'purchases.purch_frieght', 'purchases.purch_discount', 'suppliers.supp_name as ledger_name', 'suppliers.supp_tin as ledger_tin', 'purchases.purch_type');

        $qry->join('purchases', 'purchases.purch_id', '=', 'purchase_subs.purchsub_purch_id');
        $qry->join('suppliers', 'suppliers.supp_id', '=', 'purchases.purch_supp_id');

        // Purchase qry end

        // Purchase Return section

        $retamnt = DB::raw('SUM(purchretsub_rate * purchretsub_qty) as purch_ret_amount');
        $retrate = DB::raw('SUM(purchretsub_tax*purchretsub_qty) as purch_ret_tax');

        $retqry = PurchaseReturnSub::select('purchase_return_sub.purchretsub_purch_id', 'purchase_return.purchret_inv_no', 'purchase_return_sub.purchretsub_date'
            , $retrate, $retamnt, 'purchase_return_sub.branch_id', 'purchase_return.purchret_discount', 'purchase_return.purchret_frieght', 'suppliers.supp_name as ledger_name', 'suppliers.supp_tin as ledger_tin', 'purchase_return.purchret_type');

        $retqry->join('purchase_return', 'purchase_return.purchret_id', '=', 'purchase_return_sub.purchretsub_purch_id');
        $retqry->join('suppliers', 'suppliers.supp_id', '=', 'purchase_return.purchret_supp_id');

        // Purchase Return section end

        if ($inv_filter && $inv_val) {
            switch ($inv_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $qry = $qry->where('purchase_subs.purchsub_purch_id', $inv_filter, $inv_val);

                    $retqry = $retqry->where('purchase_return_sub.purchretsub_purch_id', $inv_filter, $inv_val);

                    break;
                case 'between':
                    if ($inv_val) {
                        $qry = $qry->whereBetween('purchase_subs.purchsub_purch_id', [$inv_val, $inv_val2]);
                        $retqry = $retqry->whereBetween('purchase_return_sub.purchretsub_purch_id', [$inv_val, $inv_val2]);

                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }

        if ($ptype = isset($request['period_type']) ? $request['period_type'] :0) {
            // return $request['period_type'];
            list($date1, $date2) = $this->datesFromPeriods($request['period_type'], $request);

            if ($date1 && $date2) {
                $qry = $qry->whereBetween('purchase_subs.purchsub_date', [$date1, $date2]);
                $retqry = $retqry->whereBetween('purchase_return_sub.purchretsub_date', [$date1, $date2]);

            }
        }

        if ($this->branch_id > 0) {
            $qry = $qry->where('purchase_subs.branch_id', $this->branch_id);
            $retqry = $retqry->where('purchase_return_sub.branch_id', $this->branch_id);

            $branchDet = Acc_branch::select('branch_name', 'branch_code', 'branch_tin')->where('branch_id', $this->branch_id)->first();

        } else {
            if ($request['Selbranch'] != []) {
                // print_r($request['Selbranch']);die('d');

                $qry = $qry->whereIn('purchase_subs.branch_id', $request['Selbranch']);
                $retqry = $retqry->whereIn('purchase_return_sub.branch_id', $request['Selbranch']);
                $branchDet = Acc_branch::select('branch_name', 'branch_code', 'branch_tin')->where('branch_id', $request['Selbranch'][0])->first();

            } else {

                $branchDet = Acc_branch::select('branch_name', 'branch_code', 'branch_tin')->first();
            }
        }

        $qry = $qry->where('purchase_subs.purchsub_flag', 1);
        $retqry = $retqry->where('purchase_return_sub.purchretsub_flag', 1);

        $data = [];
        if ($taxcat_id = $request['taxcat_id']) {

            $taxcat = TaxCategory::where('taxcat_id', $taxcat_id)->first();
            $taxcat_name = $taxcat->taxcat_name;
            $taxcat_tax_per = $taxcat->taxcat_name;
            $taxcat_dispname = $taxcat->display_name;
            $data['branch'] = $branchDet;
            $data['category']['name'] = $taxcat_name;
            $data['category']['dispname'] = $taxcat_dispname;

            $data['items'] = $qry->where('purchase_subs.purchsub_tax_per', $taxcat_tax_per)->groupby('purchase_subs.purchsub_purch_id')->get()->toArray();
            $data['category']['amt_total'] = array_sum(array_column($data['items'], 'purch_amount'));
            $data['category']['vat_total'] = array_sum(array_column($data['items'], 'tax_rate'));

            $data['ret_cat']['name'] = $taxcat_name . "_return";
            $data['ret_cat']['dispname'] = $taxcat_dispname;
            $data['ret_items'] = $retqry->where('purchase_return_sub.purchretsub_tax_per', $taxcat_tax_per)->groupby('purchase_return_sub.purchretsub_purch_id')->get()->toArray();
            $data['ret_cat']['amt_total'] = array_sum(array_column($data['ret_items'], 'purch_ret_amount'));
            $data['ret_cat']['vat_total'] = array_sum(array_column($data['ret_items'], 'purch_ret_tax'));
            $data = array($data);
        } else {
            $taxcats = TaxCategory::get()->toArray();

            foreach ($taxcats as $k => $val) {
                $taxcat_name = $val['taxcat_name'];
                $taxcat_id = $val['taxcat_id'];
                $taxcat_dispname = $val['display_name'];
                $taxcat_tax_per = $val['taxcat_tax_per'];
                $sqlQry = clone $qry->newQuery();
                $retSqlQryy = clone $retqry->newQuery();
                $data[$k]['branch'] = $branchDet;

                $data[$k]['category']['name'] = $taxcat_name;
                $data[$k]['category']['dispname'] = $taxcat_dispname;
                $purch_type = 2;

                $data[$k]['items'] = $sqlQry->where('purchase_subs.purchsub_tax_per', $taxcat_tax_per)->groupby('purchase_subs.purchsub_purch_id')->get()->toArray();
                $data[$k]['category']['amt_total'] = array_sum(array_column($data[$k]['items'], 'purch_amount'));
                $data[$k]['category']['vat_total'] = array_sum(array_column($data[$k]['items'], 'tax_rate'));

                $data[$k]['ret_cat']['name'] = $taxcat_name . "_return";
                $data[$k]['ret_cat']['dispname'] = $taxcat_dispname;
                $data[$k]['ret_items'] = $retSqlQryy->where('purchase_return_sub.purchretsub_tax_per', $taxcat_tax_per)->groupby('purchase_return_sub.purchretsub_purch_id')->get()->toArray();
                $data[$k]['ret_cat']['amt_total'] = array_sum(array_column($data[$k]['ret_items'], 'purch_ret_amount'));
                $data[$k]['ret_cat']['vat_total'] = array_sum(array_column($data[$k]['ret_items'], 'purch_ret_tax'));
            }

        }
        $result['data'] = $data;
        if ($request['period_type'] != "") {
            $result['Date']['date1'] = date("d-F-Y", strtotime($date1));
            $result['Date']['date2'] = date("d-F-Y", strtotime($date2));
        } else {
            $result['Date'] = "";
        }
        // $tot = $qry->groupby('purchase_subs.purchsub_purch_id')->get()->toArray();
        // $data = $qry->groupby('purchase_subs.purchsub_purch_id')->paginate($this->per_page)->toArray();

        return $result;

    }

    // Function For Output Vat Calculation
    public function outVatSummary($request)
    {

        $inv_filter = isset($request['inv_filter']) ? $request['inv_filter'] : 0;
        $inv_val = isset($request['inv_val']) ? $request['inv_val'] : 0;
        $inv_val2 = isset($request['inv_val2']) ? $request['inv_val2'] : 0;

        // Sales section
        $amnt = DB::raw('SUM(salesub_rate * salesub_qty) as purch_amount');
        $rate = DB::raw('SUM(salesub_tax_rate * salesub_qty) as tax_rate');

        $qry = SalesSub::select('sale_sub.salesub_sales_inv_no as purchsub_purch_id', 'sale_sub.salesub_date as purchsub_date',
            'sale_sub.branch_id as branch_id', 'sale_sub.branch_inv_no as purch_inv_no', $rate, $amnt, 'sales_master.sales_cust_name as ledger_name', 'sales_master.sales_cust_tin as ledger_tin');
        $qry->join('sales_master', 'sales_master.sales_inv_no', '=', 'sale_sub.salesub_sales_inv_no');

        // Sales qry end

        // Sales Return section

        $retamnt = DB::raw('SUM(salesretsub_rate * salesretsub_qty) as purch_ret_amount');
        $retrate = DB::raw('SUM(salesretsub_tax_rate * salesretsub_qty) as purch_ret_tax');

        $retqry = SalesReturnSub::select('sales_return_sub.salesretsub_salesret_no as purchretsub_purch_id', 'sales_return_sub.salesretsub_sales_inv_no as purchret_inv_no', 'sales_return_sub.salesretsub_date as purchretsub_date'
            , 'sales_return_sub.branch_id as branch_id', 'sales_return_sub.branch_id as branch_id', $retrate, $retamnt, 'sales_return_master.salesret_cust_name as ledger_name', 'sales_return_master.salesret_cust_tin as ledger_tin');

        $retqry->join('sales_return_master', 'sales_return_master.salesret_no', '=', 'sales_return_sub.salesretsub_salesret_no');

        // Sales Return section end

        if ($inv_filter && $inv_val) {
            switch ($inv_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $qry = $qry->where('sale_sub.salesub_sales_inv_no', $inv_filter, $inv_val);

                    $retqry = $retqry->where('sales_return_sub.salesretsub_salesret_no', $inv_filter, $inv_val);

                    break;
                case 'between':
                    if ($inv_val) {
                        $qry = $qry->whereBetween('sale_sub.salesub_sales_inv_no', [$inv_val, $inv_val2]);
                        $retqry = $retqry->whereBetween('sales_return_sub.salesretsub_salesret_no', [$inv_val, $inv_val2]);

                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }

        if ($ptype = isset($request['period_type']) ? $request['period_type'] : 0) {
            list($date1, $date2) = $this->datesFromPeriods($ptype, $request);

            if ($date1 && $date2) {
                $qry = $qry->whereBetween('sale_sub.salesub_date', [$date1, $date2]);
                $retqry = $retqry->whereBetween('sales_return_sub.salesretsub_date', [$date1, $date2]);

            }
        }

        if ($this->branch_id > 0) {
            $qry = $qry->where('sale_sub.branch_id', $this->branch_id);
            $retqry = $retqry->where('sales_return_sub.branch_id', $this->branch_id);

            $branchDet = Acc_branch::select('branch_name', 'branch_code', 'branch_tin')->where('branch_id', $this->branch_id)->first();

        } else {
            if ($request['Selbranch'] != []) {
                $qry = $qry->whereIn('sale_sub.branch_id', $request['Selbranch']);
                $retqry = $retqry->whereIn('sales_return_sub.branch_id', $request['Selbranch']);
                $branchDet = Acc_branch::select('branch_name', 'branch_code', 'branch_tin')->where('branch_id', $request['Selbranch'][0])->first();

            } else {
                $branchDet = Acc_branch::select('branch_name', 'branch_code', 'branch_tin')->first();
            }
        }

        $qry = $qry->where('sale_sub.salesub_flags', 1);
        $retqry = $retqry->where('sales_return_sub.salesretsub_flags', 1);

        $data = [];
        if ($taxcat_id = $request['taxcat_id']) {

            $taxcat = TaxCategory::where('taxcat_id', $taxcat_id)->first();
            $taxcat_name = $taxcat->taxcat_name;
            $taxcat_tax_per = $taxcat->taxcat_name;
            $taxcat_dispname = $taxcat->display_name;
            $data['branch'] = $branchDet;
            $data['category']['name'] = $taxcat_name;
            $data['category']['dispname'] = $taxcat_dispname;

            $data['items'] = $qry->where('sale_sub.salesub_tax_per', $taxcat_tax_per)->groupby('sale_sub.salesub_sales_inv_no')->get()->toArray();
            $data['category']['amt_total'] = array_sum(array_column($data['items'], 'purch_amount'));
            $data['category']['vat_total'] = array_sum(array_column($data['items'], 'tax_rate'));

            $data['ret_cat']['name'] = $taxcat_name . "_return";
            $data['ret_cat']['dispname'] = $taxcat_dispname;
            $data['ret_items'] = $retqry->where('sales_return_sub.salesretsub_tax_per', $taxcat_tax_per)->groupby('sales_return_sub.salesretsub_salesret_no')->get()->toArray();
            $data['ret_cat']['amt_total'] = array_sum(array_column($data['ret_items'], 'purch_ret_amount'));
            $data['ret_cat']['vat_total'] = array_sum(array_column($data['ret_items'], 'purch_ret_tax'));
            $data = array($data);
        } else {
            $taxcats = TaxCategory::get()->toArray();

            foreach ($taxcats as $k => $val) {
                $taxcat_name = $val['taxcat_name'];
                $taxcat_id = $val['taxcat_id'];
                $taxcat_dispname = $val['display_name'];
                $taxcat_tax_per = $val['taxcat_tax_per'];
                $sqlQry = clone $qry->newQuery();
                $retSqlQryy = clone $retqry->newQuery();
                $data[$k]['branch'] = $branchDet;

                $data[$k]['category']['name'] = $taxcat_name;
                $data[$k]['category']['dispname'] = $taxcat_dispname;
                $purch_type = 2;

                $data[$k]['items'] = $sqlQry->where('sale_sub.salesub_tax_per', $taxcat_tax_per)->groupby('sale_sub.salesub_sales_inv_no')->get()->toArray();
                $data[$k]['category']['amt_total'] = array_sum(array_column($data[$k]['items'], 'purch_amount'));
                $data[$k]['category']['vat_total'] = array_sum(array_column($data[$k]['items'], 'tax_rate'));

                $data[$k]['ret_cat']['name'] = $taxcat_name . "_return";
                $data[$k]['ret_cat']['dispname'] = $taxcat_dispname;
                $data[$k]['ret_items'] = $retSqlQryy->where('sales_return_sub.salesretsub_tax_per', $taxcat_tax_per)->groupby('sales_return_sub.salesretsub_salesret_no')->get()->toArray();
                $data[$k]['ret_cat']['amt_total'] = array_sum(array_column($data[$k]['ret_items'], 'purch_ret_amount'));
                $data[$k]['ret_cat']['vat_total'] = array_sum(array_column($data[$k]['ret_items'], 'purch_ret_tax'));
            }

        }
        $result['data'] = $data;
        if ($request['period_type'] != "") {
            $result['Date']['date1'] = date("d-F-Y", strtotime($date1));
            $result['Date']['date2'] = date("d-F-Y", strtotime($date2));
        } else {
            $result['Date'] = "";
        }
        // $tot = $qry->groupby('purchase_subs.purchsub_purch_id')->get()->toArray();
        // $data = $qry->groupby('purchase_subs.purchsub_purch_id')->paginate($this->per_page)->toArray();

        return $result;

    }

    public function datesFromPeriods($pt, $rq)
    {
        if ($pt == 'c') {
            $date1 = isset($rq['date1']) ? date('Y-m-d', strtotime($rq['date1'])) : 0;
            $date2 = isset($rq['date2']) ? date('Y-m-d', strtotime($rq['date2'])) : 0;

        } else {
            if ($pt > 0 && $pt <= 12) {

                if ($pt < 10) {
                    $pt = "0" . $pt;
                }

                $date1 = date("Y-$pt-01");
                $date2 = date("Y-m-t", strtotime($date1));
            }

            if ($pt == 'a') {
                $year = date('Y') - 1;
                $date1 = $year . "-01-01";
                $date2 = $year . "-12-31";
            }

        }

        return [$date1, $date2];
    }

}
