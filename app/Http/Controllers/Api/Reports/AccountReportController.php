<?php

namespace App\Http\Controllers\Api\Reports;

use App\Http\Controllers\Api\ApiParent;
use App\Models\Accounts\Acc_group;
use App\Models\Accounts\Acc_voucher;
use App\Models\Accounts\voucherType;
use App\Models\Company\Acc_branch;
use App\Models\Company\CompanySettings;
use App\Models\Stocks\OpeningStockLog;
use App\Models\Stocks\StockDailyUpdates;
use DB;
use Illuminate\Http\Request;

class AccountReportController extends ApiParent
{
    public function Report(Request $request, $type)
    {

        switch ($type) {
            case 'dayBook':
                return parent::displayData($this->dayBook($request));
                break;

            case 'acc_group_summary':
                return $this->accGroupSummary($request);
                break;

            case 'excel_data':
                return $this->excelData($request);
                break;
            // old trail balance loading issue
            // case 'trialbalance':
            //     return $this->trialbalance($request);
            //     break;

            case 'trialbalance':
                return $this->gettrialbalance($request);
                break;

            case 'opening_balance_summary':
                return $this->openingBalanceSummary($request);
                break;

            default:
                return parent::displayError('Invalid Endpoint', $this->badrequest_stat);
                break;
        }
    }

    public function excelData($request)
    {

        $where = array();
        $where[] = ['accgrp_flags', '=', 1];
        $where[] = ['accgrp_depth', '=', 0];

        if (isset($request['group_id']) && $request['group_id'] != '') {

            if ($request['group_id']['accgrp_parent'] > 0) {
                $where[] = ['accgrp_id', '=', $request['group_id']['accgrp_parent']];
            } else {
                $where[] = ['accgrp_id', '=', $request['group_id']['accgrp_id']];
            }

        }

        $rltn = [
            'subGroups' => function ($qr) use ($request) {
                $rltn = [
                    'ledgers' => function ($qr) use ($request) {
                        $qr->select('*');

                        if ($this->branch_id > 0) {
                            $qr->where('acc_ledgers.branch_id', '=', $this->branch_id);
                        } else {
                
                            if ($this->active_branch_id == 'All') {
                
                            } else {
                                $qr->where('acc_ledgers.branch_id', '=', $this->active_branch_id);
                            }
                        }

                    },
                ];

                $qr->select('*')->where('accgrp_depth', 1)->with($rltn);
                if (isset($request['group_id']) && $request['group_id'] != ''
                    && $request['group_id']['accgrp_depth'] != 0) {

                    $qr->where('accgrp_id', '=', $request['group_id']['accgrp_id']);

                }
            },
            'ledgers' => function ($qr) use ($request) {
                $qr->select('*');
                if (isset($request['group_id']) && $request['group_id'] != '') {
                    $qr->where('ledger_accgrp_id', '=', $request['group_id']['accgrp_id']);

                }

                if ($this->branch_id > 0) {
                    $qr->where('acc_ledgers.branch_id', '=', $this->branch_id);
                } else {
        
                    if ($this->active_branch_id == 'All') {
        
                    } else {
                        $qr->where('acc_ledgers.branch_id', '=', $this->active_branch_id);
                    }
                }

            },

        ];
        $total_debit = $total_credit = 0;
        $Accgroup = Acc_group::select('*')->with($rltn)->where($where)->get();
        $company_data = CompanySettings::first();

        $date1 = $date2 = null;
        if ($ptype = isset($request['period_type']) ? $request['period_type'] : 0) {
            list($date1, $date2) = $this->datesFromPeriods($ptype, $request);
        }
        foreach ($Accgroup as $k => $val) {

            foreach ($val['subGroups'] as $k2 => $val2) {

                $Accgroup[$k]['subGroups'][$k2]['vch_in'] = 0;
                $Accgroup[$k]['subGroups'][$k2]['vch_out'] = 0;
                foreach ($val2['ledgers'] as $k3 => $val3) {
                    $balance = $this->getLedgerBalance($request, $val3['ledger_id'], $company_data->cmp_finyear_start, $date1, $date2);

                    if ($balance['vch_in'] > $balance['vch_out']) {

                        $net_calculated = $balance['vch_in'] - $balance['vch_out'];

                        $Accgroup[$k]['subGroups'][$k2]['ledgers'][$k3]['vch_in'] = $net_calculated;
                        $Accgroup[$k]['subGroups'][$k2]['ledgers'][$k3]['vch_out'] = 0;

                        $Accgroup[$k]['vch_in'] += $net_calculated;
                        $Accgroup[$k]['subGroups'][$k2]['vch_in'] += $net_calculated;

                    } else {

                        $net_calculated = $balance['vch_out'] - $balance['vch_in'];

                        $Accgroup[$k]['subGroups'][$k2]['ledgers'][$k3]['vch_in'] = 0;
                        $Accgroup[$k]['subGroups'][$k2]['ledgers'][$k3]['vch_out'] = $net_calculated;

                        $Accgroup[$k]['vch_out'] += $net_calculated;
                        $Accgroup[$k]['subGroups'][$k2]['vch_out'] += $net_calculated;
                    }

                    $total_debit = $total_debit + $balance['vch_in'];
                    $total_credit = $total_credit + $balance['vch_out'];

                }
            }

            foreach ($val['ledgers'] as $k4 => $val4) {
                $balance = $this->getLedgerBalance($request, $val4['ledger_id'], $company_data->cmp_finyear_start, $date1, $date2);

                if ($balance['vch_in'] > $balance['vch_out']) {
                    $net_calculated = $balance['vch_in'] - $balance['vch_out'];
                    $Accgroup[$k]['vch_in'] += $net_calculated;
                    $Accgroup[$k]['ledgers'][$k4]['vch_in'] = $net_calculated;
                    $Accgroup[$k]['ledgers'][$k4]['vch_out'] = 0;
                } else {
                    $net_calculated = $balance['vch_out'] - $balance['vch_in'];
                    $Accgroup[$k]['vch_out'] += $net_calculated;
                    $Accgroup[$k]['ledgers'][$k4]['vch_out'] = $net_calculated;
                    $Accgroup[$k]['ledgers'][$k4]['vch_in'] = 0;
                }

            }

        }

        $i = 0;
        $a = 1;
        foreach ($Accgroup as $k => $val) {

            if ($val['vch_in'] != 0 || $val['vch_out'] != 0) {

                if ($request['disp_type'] != 1) {

                    $excel_data[$i]['Particulars'] = $this->integerToRoman($a) . "-" . $val['accgrp_name'];
                    $excel_data[$i]['Debit Amount'] = $val['vch_in'];
                    $excel_data[$i]['Credit Amount'] = $val['vch_out'];
                    $i++;
                    $a++;
                }
                $b = 1;
                foreach ($val['subGroups'] as $k2 => $val2) {

                    if ($val2['vch_in'] != 0 || $val2['vch_out'] != 0) {

                        if ($request['disp_type'] != 1) {
                            $excel_data[$i]['Particulars'] = "$b-" . $val2['accgrp_name'];
                            $excel_data[$i]['Debit Amount'] = $val2['vch_in'];
                            $excel_data[$i]['Credit Amount'] = $val2['vch_out'];
                            $i++;
                            $b++;
                        }

                        if ($request['disp_type'] != 2) {
                            foreach ($val2['ledgers'] as $k3 => $val3) {
                                if ($val3['vch_in'] != 0 || $val3['vch_out'] != 0) {
                                    $excel_data[$i]['Particulars'] = $val3['ledger_name'];
                                    $excel_data[$i]['Debit Amount'] = $val3['vch_in'];
                                    $excel_data[$i]['Credit Amount'] = $val3['vch_out'];
                                    $i++;

                                }
                            }
                        }

                    }
                }

                if ($request['disp_type'] != 2) {
                    foreach ($val['ledgers'] as $k4 => $val3) {

                        if ($val3['vch_in'] != 0 || $val3['vch_out'] != 0) {
                            $excel_data[$i]['Particulars'] = $val3['ledger_name'];
                            $excel_data[$i]['Debit Amount'] = $val3['vch_in'];
                            $excel_data[$i]['Credit Amount'] = $val3['vch_out'];
                            $i++;

                        }

                    }
                }

            }

        }

        return response()->json(['data' => $excel_data, 'status' => 200]);

    }

    public function integerToRoman($integer)
    {
        // Convert the integer into an integer (just to make sure)
        $integer = intval($integer);
        $result = '';

        // Create a lookup array that contains all of the Roman numerals.
        $lookup = array('M' => 1000,
            'CM' => 900,
            'D' => 500,
            'CD' => 400,
            'C' => 100,
            'XC' => 90,
            'L' => 50,
            'XL' => 40,
            'X' => 10,
            'IX' => 9,
            'V' => 5,
            'IV' => 4,
            'I' => 1);

        foreach ($lookup as $roman => $value) {
            // Determine the number of matches
            $matches = intval($integer / $value);

            // Add the same number of characters to the string
            $result .= str_repeat($roman, $matches);

            // Set the integer to be the remainder of the integer and the value
            $integer = $integer % $value;
        }

        // The Roman numeral should be built, return it
        return $result;
    }

    public function accGroupSummary($request)
    {

        $where = array();
        $where[] = ['accgrp_flags', '=', 1];
        $where[] = ['accgrp_depth', '=', 0];

        if (isset($request['group_id']) && $request['group_id'] != '') {

            if ($request['group_id']['accgrp_parent'] > 0) {
                $where[] = ['accgrp_id', '=', $request['group_id']['accgrp_parent']];
            } else {
                $where[] = ['accgrp_id', '=', $request['group_id']['accgrp_id']];
            }

        }

        $rltn = [
            'subGroups' => function ($qr) use ($request) {
                $rltn = [
                    'ledgers' => function ($qr) use ($request) {
                        $qr->select('*');

                        if ($this->branch_id > 0) {
                            $qr->where('acc_ledgers.branch_id', '=', $this->branch_id);
                        } else {
                
                            if ($this->active_branch_id == 'All') {
                
                            } else {
                                $qr->where('acc_ledgers.branch_id', '=', $this->active_branch_id);
                            }
                        }

                    },
                ];

                $qr->select('*')->where('accgrp_depth', 1)->with($rltn);
                if (isset($request['group_id']) && $request['group_id'] != ''
                    && $request['group_id']['accgrp_depth'] != 0) {

                    $qr->where('accgrp_id', '=', $request['group_id']['accgrp_id']);

                }
            },
            'ledgers' => function ($qr) use ($request) {
                $qr->select('*');
                if (isset($request['group_id']) && $request['group_id'] != '') {
                    $qr->where('ledger_accgrp_id', '=', $request['group_id']['accgrp_id']);

                }

                if ($this->branch_id > 0) {
                    $qr->where('acc_ledgers.branch_id', '=', $this->branch_id);
                } else {
        
                    if ($this->active_branch_id == 'All') {
        
                    } else {
                        $qr->where('acc_ledgers.branch_id', '=', $this->active_branch_id);
                    }
                }

            },

        ];

        $Accgroup = Acc_group::select('accgrp_id','accgrp_name','accgrp_parent','accgrp_depth')->with($rltn)->where($where)->paginate(10);
        $company_data = CompanySettings::first();

        $date1 = $date2 = null;
        if ($ptype = isset($request['period_type']) ? $request['period_type'] : 0) {
            list($date1, $date2) = $this->datesFromPeriods($ptype, $request);
        }
        foreach ($Accgroup as $k => $val) {

            foreach ($val['subGroups'] as $k2 => $val2) {

                $Accgroup[$k]['subGroups'][$k2]['vch_in'] = 0;
                $Accgroup[$k]['subGroups'][$k2]['vch_out'] = 0;
                foreach ($val2['ledgers'] as $k3 => $val3) {
                    $balance = $this->getLedgerBalance($request, $val3['ledger_id'], $company_data->cmp_finyear_start, $date1, $date2);

                    if ($balance['vch_in'] > $balance['vch_out']) {

                        $net_calculated = $balance['vch_in'] - $balance['vch_out'];

                        $Accgroup[$k]['subGroups'][$k2]['ledgers'][$k3]['vch_in'] = $net_calculated;
                        $Accgroup[$k]['subGroups'][$k2]['ledgers'][$k3]['vch_out'] = 0;

                        $Accgroup[$k]['vch_in'] += $net_calculated;
                        $Accgroup[$k]['subGroups'][$k2]['vch_in'] += $net_calculated;

                    } else {

                        $net_calculated = $balance['vch_out'] - $balance['vch_in'];

                        $Accgroup[$k]['subGroups'][$k2]['ledgers'][$k3]['vch_in'] = 0;
                        $Accgroup[$k]['subGroups'][$k2]['ledgers'][$k3]['vch_out'] = $net_calculated;

                        $Accgroup[$k]['vch_out'] += $net_calculated;
                        $Accgroup[$k]['subGroups'][$k2]['vch_out'] += $net_calculated;
                    }



                }
            }

            foreach ($val['ledgers'] as $k4 => $val4) {
                $balance = $this->getLedgerBalance($request, $val4['ledger_id'], $company_data->cmp_finyear_start, $date1, $date2);

                if ($balance['vch_in'] > $balance['vch_out']) {
                    $net_calculated = $balance['vch_in'] - $balance['vch_out'];
                    $Accgroup[$k]['vch_in'] += $net_calculated;
                    $Accgroup[$k]['ledgers'][$k4]['vch_in'] = $net_calculated;
                    $Accgroup[$k]['ledgers'][$k4]['vch_out'] = 0;
                } else {
                    $net_calculated = $balance['vch_out'] - $balance['vch_in'];
                    $Accgroup[$k]['vch_out'] += $net_calculated;
                    $Accgroup[$k]['ledgers'][$k4]['vch_out'] = $net_calculated;
                    $Accgroup[$k]['ledgers'][$k4]['vch_in'] = 0;
                }

            }

        }

        if (isset($date1) && isset($date2)) {
            $info['Date']['date1'] = date("d-F-Y", strtotime($date1));
            $info['Date']['date2'] = date("d-F-Y", strtotime($date2));
        } else {
            $info['Date'] = "";
        }

        return response()->json(['data' => $Accgroup, 'info' => $info, 'status' => 200]);

    }

    public function getLedgerBalance($request, $ledger_id, $cmp_finyear_start, $date1, $date2)
    {

        if ($this->branch_id > 0) {
            $this->branch_active = $this->branch_id;
        } else {

            if ($this->active_branch_id == 'All') {
                $this->branch_active =1;
            } else {
                $this->branch_active = $this->active_branch_id;
            }
        }

        $data = Acc_voucher::select('acc_voucher.vch_in', 'acc_voucher.vch_out')->where([
            ['branch_id', '=', $this->branch_active],
            ['vch_ledger_to', '=', $ledger_id],
            ['vch_flags', '=', 1],

        ]);

        if ($date1 && $date2) {
            $data = $data->whereBetween(DB::raw('DATE(erp_acc_voucher.vch_date)'), [$date1, $date2]);
        } else {
            $data = $data->whereBetween(DB::raw('DATE(erp_acc_voucher.vch_date)'), [$cmp_finyear_start, date('Y-m-d')]);
        }

        $allRes = $data->get()->toArray();

        $result['vch_in'] = array_sum(array_column($allRes, 'vch_in'));
        $result['vch_out'] = array_sum(array_column($allRes, 'vch_out'));

        if (!$date1 && !$date2) {

            if ($this->branch_active) {
                $branchDet = Acc_branch::where('branch_id', $this->branch_active)->first('branch_open_date');
            } else {
                $branchDet = Acc_branch::where('branch_id', 1)->first('branch_open_date');
            }

            $branchOpenPrevDate = date('Y-m-d', strtotime('-1 day', strtotime($branchDet->branch_open_date)));
            $inOut = Acc_voucher::where([
                ['branch_id', '=',$this->branch_active],
                ['vch_vchtype_id', '=', 14],
                ['vch_ledger_to', '=', $ledger_id],
                ['vch_date', '=', date('Y-m-d 00:00:00', strtotime($branchOpenPrevDate))],
                ['vch_flags', '=', 1],

            ])->first();

            if ($inOut) {
                $op_balance = $inOut->vch_out - $inOut->vch_in;
            } else {
                $op_balance = 0;
            }

            if ($op_balance > 0) {

                $result['vch_out'] += abs($op_balance);
            }
            if ($op_balance < 0) {
                $result['vch_in'] += abs($op_balance);
            }

        }

        return $result;
    }

    public function dayBook($request)
    {
        // return  date('Y-m-d',strtotime($request['date1']));

        $vch_filter = isset($request['vch_filter']) ? $request['vch_filter'] : 0;
        $vch_val1 = isset($request['vch_val1']) ? $request['vch_val1'] : 0;
        $vch_val2 = isset($request['vch_val2']) ? $request['vch_val2'] : 0;

        $company_data = CompanySettings::first();

        $data = Acc_voucher::select('acc_voucher.*', 'acc_ledgers.ledger_name', 'voucher_type.vchtype_name')
            ->join('acc_ledgers', 'acc_ledgers.ledger_id', 'acc_voucher.vch_ledger_to')
            ->join('voucher_type', 'voucher_type.vchtype_id', 'acc_voucher.vch_vchtype_id')
            ->where('acc_voucher.vch_id2', 0);

        if ($this->branch_id > 0) {

            $data = $data->where('acc_voucher.branch_id', $this->branch_id);
        } else {

            if ($this->active_branch_id == 'All') {
            } else {
                $data = $data->where('acc_voucher.branch_id', $this->active_branch_id);
            }
        }




        if (isset($request['vch_type']) && is_int($request['vch_type'])) {
            $data = $data->where('acc_voucher.vch_vchtype_id', $request['vch_type']);
        }

        if (isset($request['ledger_id']) && $request['ledger_id'] != '') {
            $data = $data->where('acc_voucher.vch_ledger_to', $request['ledger_id']);
        }

        if (isset($request['added_by']) && $request['added_by'] != '') {
            $data = $data->where('acc_voucher.vch_added_by', $request['added_by']);
        }
        if (isset($request['disp_void']) && $request['disp_void'] != '') {
            $data = $data->where('acc_voucher.vch_flags', $request['disp_void']);
        } else {
            $data = $data->where('acc_voucher.vch_flags', 1);

        }

        if ($ptype = isset($request['period_type']) ? $request['period_type'] : 0) {
            list($date1, $date2) = $this->datesFromPeriods($ptype, $request);

            if ($date1 && $date2) {
                $data = $data->whereBetween(DB::raw('DATE(erp_acc_voucher.vch_date)'), [$date1, $date2]);
            }
        }

        if ($vch_filter && $vch_val1) {
            switch ($vch_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $data = $data->where('vch_no', $vch_filter, $vch_val1);
                    break;
                case 'between':
                    if ($vch_val2) {
                        $data = $data->whereBetween('vch_no', [$vch_val1, $vch_val2]);
                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }

        $data = $data->whereBetween(DB::raw('DATE(erp_acc_voucher.vch_date)'), [$company_data->cmp_finyear_start, date('Y-m-d')]);

        $allRes = $data->get()->toArray();

        $result = $data->orderBy('acc_voucher.vch_date', 'desc')->paginate($this->per_page)->toArray();

        $result['debit_amnt'] = array_sum(array_column($allRes, 'vch_in'));
        $result['credit_amnt'] = array_sum(array_column($allRes, 'vch_out'));

        if ($request['period_type'] != "") {
            $result['Date']['date1'] = date("d-F-Y", strtotime($date1));
            $result['Date']['date2'] = date("d-F-Y", strtotime($date2));
        } else {
            $result['Date'] = "";
        }

        if (isset($request['show_notes']) && $request['show_notes'] != '') {
            $result['show_notes'] = true;

        } else {
            $result['show_notes'] = false;

        }

        return $result;
    }

    public function datesFromPeriods($pt, $rq)
    {
        switch ($pt) {

            case 't':
                $date1 = $date2 = date('Y-m-d');
                break;

            case 'ld':
                $date1 = $date2 = date('Y-m-d', strtotime("-1 days"));
                break;

            case 'lw':
                $previous_week = strtotime("-1 week +1 day");
                $start_week = strtotime("last sunday midnight", $previous_week);
                $end_week = strtotime("next saturday", $start_week);
                $date1 = date("Y-m-d", $start_week);
                $date2 = date("Y-m-d", $end_week);
                break;

            case 'lm':
                $date1 = date("Y-m-d", strtotime("first day of previous month"));
                $date2 = date("Y-m-d", strtotime("last day of previous month"));
                break;

            case 'ly':
                $year = date('Y') - 1;
                $date1 = $year . "-01-01";
                $date2 = $year . "-12-31";
                break;

            case 'c':
                $date1 = isset($rq['date1']) ? date('Y-m-d', strtotime($rq['date1'])) : 0;
                $date2 = isset($rq['date2']) ? date('Y-m-d', strtotime($rq['date2'])) : 0;

                break;

            default:
                return false;
                break;
        }
        return [$date1, $date2];
    }

    public function datesFromPeriodsNew($pt, $rq)
    {
        $date1 = $date2 = date('Y-m-d');
        switch ($pt) {

            case 't':
                $date1 = $date2 = date('Y-m-d');
                break;

            case 'ld':
                $date1 = $date2 = date('Y-m-d', strtotime("-1 days"));
                break;

            case 'lw':
                $date1 = date("Y-m-d", strtotime("-1 week"));
                $date2 = date('Y-m-d');
                break;

            case 'lm':
                $date1 = date("Y-m-d", strtotime("-1 month"));
                $date2 = date('Y-m-d');
                break;

            case 'ly':
                $date1 = date("Y-m-d", strtotime("-1 year"));
                $date2 = date('Y-m-d');
                break;

            case 'c':
                $date1 = (isset($rq['date1']) && $rq['date1']) ? date('Y-m-d', strtotime($rq['date1'])) : date('Y-m-d');
                $date2 = (isset($rq['date2']) && $rq['date2']) ? date('Y-m-d', strtotime($rq['date2'])) : date('Y-m-d');
                break;

            default:
                $date1 = '';
                $date2 = date('Y-m-d');
                break;
        }

        if($this->branch_id>0)
       {
        $branchDet = Acc_branch::where('branch_id', $this->branch_id)->first('branch_open_date');
       }
       else
       {
        if ($this->active_branch_id == 'All') {
            $branchDet = Acc_branch::where('branch_id',1)->first('branch_open_date');
        } else {
          
            $branchDet = Acc_branch::where('branch_id', $this->active_branch_id)->first('branch_open_date');
        }
        
       }
      
        // return var_dump($branchDet->branch_open_date);
        // exit;
        if ($branchDet->branch_open_date > $date1 || $date1 == '') {
            $date1 = $branchDet->branch_open_date;
        }
        return [$date1, $date2, $branchDet->branch_open_date];
    }

    public function searchVchrtype(Request $request)
    {
        if ($request['vchtype_name'] != "") {
            $vouchers = voucherType::where('vchtype_name', 'like', '%' . $request['vchtype_name'] . '%')
                ->take(50)
                ->get();
            return response()->json(['data' => $vouchers, 'status' => $this->success_stat]);
        } else {
            $data = array();
            return response()->json(['data' => $data, 'status' => $this->success_stat]);
        }
    }

    public function trialbalance($request)
    {

        $where = array();
        $where[] = ['accgrp_flags', '=', 1];
        $where[] = ['accgrp_depth', '=', 0];

        $ptype = (isset($request['period_type']) && $request['period_type'] != '') ? $request['period_type'] : 'all';
        list($date1, $date2, $branchOpenDate) = $this->datesFromPeriodsNew($ptype, $request);

        $rltn = [
            'subGroups' => function ($qr) use ($ptype, $date1, $date2, $branchOpenDate) {
                $rltn1 = [
                    'ledgers' => function ($qr) use ($ptype, $date1, $date2, $branchOpenDate) {
                        $qr->withCount(['balance as balance' => function ($query) use ($ptype, $date1, $date2, $branchOpenDate) {
                            $query->select(DB::raw('(IFNULL(sum(vch_in),0) - IFNULL(sum(vch_out),0))'));
                            if ($date1 != $branchOpenDate) {
                                $query->where(DB::raw('DATE(vch_date)'), '>=', $date1);
                                $query->where(DB::raw('DATE(vch_date)'), '<=', $date2);
                            }
                            $query->where('vch_flags', '=', 1);
                            $query->where('branch_id', '=', $this->branch_id);
                        }]);

                        if ($this->branch_id > 0) {
                            $qr->where('acc_ledgers.branch_id', '=', $this->branch_id);
                            $qr->where('acc_ledgers.ledger_accgrp_id', '!=', 23)
                                ->orWhere(function ($q) {
                                    $q->where('acc_ledgers.ledger_accgrp_id', '=', 23);
                                    $q->where('acc_ledgers.ledger_branch_id', '!=', $this->branch_id);
                                });
                        }

                    },
                ];
                $qr->select('*')->where('accgrp_depth', 1)->with($rltn1);
            },
            'ledgers' => function ($qr) use ($ptype, $date1, $date2, $branchOpenDate) {

                $qr->withCount(['balance as balance' => function ($query) use ($ptype, $date1, $date2, $branchOpenDate) {
                    $query->select(DB::raw('(IFNULL(sum(vch_in),0) - IFNULL(sum(vch_out),0))'));
                    if ($date1 != $branchOpenDate) {
                        $query->where(DB::raw('DATE(vch_date)'), '>=', $date1);
                        $query->where(DB::raw('DATE(vch_date)'), '<=', $date2);
                    }
                    $query->where('vch_flags', '=', 1);
                    $query->where('branch_id', '=', $this->branch_id);
                }]);

                if ($this->branch_id > 0) {
                    $qr->where('acc_ledgers.branch_id', '=', $this->branch_id);
                }

            },

        ];
        $total_debit = $total_credit = 0;
        $Accgroup = Acc_group::select('*')->with($rltn)->where($where)->paginate(10000);

        // $company_data = CompanySettings::first();

        // $date1 = $date2 = null;
        // if ($ptype = isset($request['period_type']) ? $request['period_type'] : 0) {
        //     list($date1, $date2, $branchOpenDate) = $this->datesFromPeriods($ptype, $request);
        // }
        foreach ($Accgroup as $k => $val) {

            $Accgroup[$k]['display'] = false;
            foreach ($val['subGroups'] as $k2 => $val2) {

                $Accgroup[$k]['subGroups'][$k2]['balance'] = 0;
                $Accgroup[$k]['subGroups'][$k2]['display'] = false;

                foreach ($val2['ledgers'] as $k3 => $val3) {

                    $Accgroup[$k]['balance'] += $val3['balance'];
                    $Accgroup[$k]['subGroups'][$k2]['balance'] += $val3['balance'];
                    if ($val3['balance'] != 0) {
                        $Accgroup[$k]['display'] = true;
                        $Accgroup[$k]['subGroups'][$k2]['display'] = true;
                    }

                    // $Accgroup[$k]['subGroups'][$k2]['vch_out'] = $Accgroup[$k]['subGroups'][$k2]['vch_out'] + $balance['vch_out'];

                    $Accgroup[$k]['subGroups'][$k2]['ledgers'][$k3]['gtz'] = 0;
                    $Accgroup[$k]['subGroups'][$k2]['ledgers'][$k3]['ltz'] = 0;
                    if ($val3['balance'] > 0) {
                        $Accgroup[$k]['subGroups'][$k2]['ledgers'][$k3]['gtz'] = abs($val3['balance']);
                    }
                    if ($val3['balance'] < 0) {
                        $Accgroup[$k]['subGroups'][$k2]['ledgers'][$k3]['ltz'] = abs($val3['balance']);
                    }
                    // $total_debit += $val3['balance'];
                    // $total_credit += $val3['balance'];

                }
                $Accgroup[$k]['subGroups'][$k2]['gtz'] = 0;
                $Accgroup[$k]['subGroups'][$k2]['ltz'] = 0;
                if ($Accgroup[$k]['subGroups'][$k2]['balance'] > 0) {
                    $Accgroup[$k]['subGroups'][$k2]['gtz'] = abs($Accgroup[$k]['subGroups'][$k2]['balance']);
                }
                if ($Accgroup[$k]['subGroups'][$k2]['balance'] < 0) {
                    $Accgroup[$k]['subGroups'][$k2]['ltz'] = abs($Accgroup[$k]['subGroups'][$k2]['balance']);
                }
            }
            foreach ($val['ledgers'] as $k4 => $val4) {

                $Accgroup[$k]['balance'] += $val4['balance'];
                if ($val4['balance'] != 0) {
                    $Accgroup[$k]['display'] = true;
                }

                $Accgroup[$k]['ledgers'][$k4]['gtz'] = 0;
                $Accgroup[$k]['ledgers'][$k4]['ltz'] = 0;
                if ($val4['balance'] > 0) {
                    $Accgroup[$k]['ledgers'][$k4]['gtz'] = abs($val4['balance']);
                }
                if ($val4['balance'] < 0) {
                    $Accgroup[$k]['ledgers'][$k4]['ltz'] = abs($val4['balance']);
                }
            }

            if ($val['accgrp_name'] == 'Current Assets') {
                // chek is starting date is branch opening date
                $openingStock = 0;
                if ($date1 == $branchOpenDate) {
                    $rateColumn = 'bs_prate';
                    $qry = OpeningStockLog::select(DB::raw('(IFNULL(sum(opstklog_prate * opstklog_stock_quantity_add),0)) as open_stock'))
                        ->where('opstklog_type', '=', 0);
                    if ($this->branch_id > 0) {
                        $qry->where('branch_id', '=', $this->branch_id);
                    }
                    $openingStk = $qry->first('open_stock');
                    $openingStock = $openingStk->open_stock;
                } else {

                    $rateColumn = (isset($request['disp_purch_rate']) && $request['disp_purch_rate'] == 'avg') ? 'bs_avg_prate' : 'bs_prate';
                    $uptoDateStock = StockDailyUpdates::select(DB::raw('(IFNULL(sum(' . $rateColumn . ' * sdu_stock_quantity),0)) as uptodate_stock'))
                        ->join('branch_stocks', 'branch_stocks.branch_stock_id', 'stock_daily_updates.sdu_branch_stock_id')
                        ->where(DB::raw('DATE(sdu_date)'), '>=', $branchOpenDate)
                        ->where(DB::raw('DATE(sdu_date)'), '<=', $date1)->first('uptodate_stock');
                    $openingStock += $uptoDateStock->uptodate_stock;
                }

                // if ($ptype != 'all') {

                // }

                $Accgroup[$k]['balance'] += $openingStock;
                $newRow = array('ledger_name' => "Opening Stock",
                    'balance' => $openingStock,
                    'gtz' => ($openingStock > 0) ? $openingStock : 0,
                    'ltz' => ($openingStock < 0) ? abs($openingStock) : 0);
                $Accgroup[$k]['ledgers'][] = $newRow;
            }

            $Accgroup[$k]['gtz'] = 0;
            $Accgroup[$k]['ltz'] = 0;
            if ($Accgroup[$k]['balance'] > 0) {
                $Accgroup[$k]['gtz'] = abs($Accgroup[$k]['balance']);
            }
            if ($Accgroup[$k]['balance'] < 0) {
                $Accgroup[$k]['ltz'] = abs($Accgroup[$k]['balance']);
            }
            $total_debit += $Accgroup[$k]['gtz'];
            $total_credit += $Accgroup[$k]['ltz'];
        }
        $info['opening_bal'] = $openingStock;
        if ($date1 == $branchOpenDate) {
            $branchOpenPrevDate = date('Y-m-d', strtotime('-1 day', strtotime($branchOpenDate)));
            $openingBalance = Acc_voucher::select(DB::raw('(IFNULL(sum(vch_in),0) - IFNULL(sum(vch_out),0)) as op_bal_sum'))
                ->where([
                    ['branch_id', '=', $this->branch_id],
                    ['vch_vchtype_id', '=', 14],
                    [DB::raw('DATE(vch_date)'), '=', date('Y-m-d', strtotime($branchOpenPrevDate))],
                    ['vch_flags', '=', 1],
                ])->first();
            $info['opening_bal'] = $openingBalance['op_bal_sum'] + $openingStock;
        }
        if ($info['opening_bal'] > 0) {
            $total_credit += abs($info['opening_bal']);
        }
        if ($info['opening_bal'] < 0) {
            $total_debit += abs($info['opening_bal']);
        }
        $info['total_debit'] = $total_debit;
        $info['total_credit'] = $total_credit;

        if (isset($date1) && isset($date2)) {
            $info['Date']['date1'] = date("d-F-Y", strtotime($date1));
            $info['Date']['date2'] = date("d-F-Y", strtotime($date2));
            $info['Date']['branch_open_date'] = date("d-F-Y", strtotime($branchOpenDate));
        } else {
            $info['Date'] = "";
        }

        return response()->json(['data' => $Accgroup, 'info' => $info, 'status' => 200]);

    }

    public function gettrialbalance($request)
    {

        $ptype = (isset($request['period_type']) && $request['period_type'] != '') ? $request['period_type'] : 'all';
        list($date1, $date2, $branchOpenDate) = $this->datesFromPeriodsNew($ptype, $request);
        $query = Acc_voucher::select(DB::raw('(IFNULL(sum(vch_in),0) - IFNULL(sum(vch_out),0)) as balance'), 'vch_ledger_to', 'vch_from_group');
        if ($date1 != $branchOpenDate) {
            $query->where(DB::raw('DATE(vch_date)'), '>=', $date1);
            $query->where(DB::raw('DATE(vch_date)'), '<=', $date2);
        }
        $query->where('vch_flags', '=', 1);
        $query->groupby('vch_ledger_to');
        // $query->groupby('vch_from_group');

        if ($this->branch_id > 0) {
            $this->branch_active  = $this->branch_id;
        } else {
            $this->branch_active = $this->active_branch_id;
        }

        $ledgBal = $query->where('branch_id', '=', $this->branch_active)->get()->keyBy('vch_ledger_to');
        $info = array();

        $where = array();
        $where[] = ['accgrp_flags', '=', 1];
        $where[] = ['accgrp_depth', '=', 0];

        $rltn = [
            'subGroups' => function ($qr) use ($ptype, $date1, $date2, $branchOpenDate) {
                $rltn1 = [
                    'ledgers' => function ($qr) use ($ptype, $date1, $date2, $branchOpenDate) {
                        
                            $qr->where('acc_ledgers.branch_id', '=', $this->branch_active);
                            $qr->where('acc_ledgers.ledger_accgrp_id', '!=', 23)
                                ->orWhere(function ($q) {
                                    $q->where('acc_ledgers.ledger_accgrp_id', '=', 23);
                                    $q->where('acc_ledgers.ledger_branch_id', '!=', $this->branch_active);
                                });
                        

                    },
                ];
                $qr->select('*')->where('accgrp_depth', 1)->with($rltn1);
            },
            'ledgers' => function ($qr) use ($ptype, $date1, $date2, $branchOpenDate) {
                
                    $qr->where('acc_ledgers.branch_id', '=', $this->branch_active);
                

            },

        ];
        $Accgroup = Acc_group::select('*')->with($rltn)->where($where)->paginate(10000);
        $total_debit = 0;
        $total_credit = 0;
        foreach ($Accgroup as $k => $val) {

            $Accgroup[$k]['display'] = false;
            foreach ($val['subGroups'] as $k2 => $val2) {

                $Accgroup[$k]['subGroups'][$k2]['balance'] = 0;
                $Accgroup[$k]['subGroups'][$k2]['display'] = false;

                foreach ($val2['ledgers'] as $k3 => $val3) {
                    $balnc = isset($ledgBal[$val3['ledger_id']]['balance']) ? $ledgBal[$val3['ledger_id']]['balance'] : 0;
                    $Accgroup[$k]['balance'] += $balnc;
                    $Accgroup[$k]['subGroups'][$k2]['balance'] += $balnc;
                    if ($balnc != 0) {
                        $Accgroup[$k]['display'] = true;
                        $Accgroup[$k]['subGroups'][$k2]['display'] = true;
                    }

                    // $Accgroup[$k]['subGroups'][$k2]['vch_out'] = $Accgroup[$k]['subGroups'][$k2]['vch_out'] + $balance['vch_out'];

                    $Accgroup[$k]['subGroups'][$k2]['ledgers'][$k3]['gtz'] = 0;
                    $Accgroup[$k]['subGroups'][$k2]['ledgers'][$k3]['ltz'] = 0;
                    if ($balnc > 0) {
                        $Accgroup[$k]['subGroups'][$k2]['ledgers'][$k3]['gtz'] = abs($balnc);
                    }
                    if ($balnc < 0) {
                        $Accgroup[$k]['subGroups'][$k2]['ledgers'][$k3]['ltz'] = abs($balnc);
                    }

                }
                $Accgroup[$k]['subGroups'][$k2]['gtz'] = 0;
                $Accgroup[$k]['subGroups'][$k2]['ltz'] = 0;
                if ($Accgroup[$k]['subGroups'][$k2]['balance'] > 0) {
                    $Accgroup[$k]['subGroups'][$k2]['gtz'] = abs($Accgroup[$k]['subGroups'][$k2]['balance']);
                }
                if ($Accgroup[$k]['subGroups'][$k2]['balance'] < 0) {
                    $Accgroup[$k]['subGroups'][$k2]['ltz'] = abs($Accgroup[$k]['subGroups'][$k2]['balance']);
                }
            }
            foreach ($val['ledgers'] as $k4 => $val4) {
                $balnc = isset($ledgBal[$val4['ledger_id']]['balance']) ? $ledgBal[$val4['ledger_id']]['balance'] : 0;
                $Accgroup[$k]['balance'] += $balnc;
                if ($balnc != 0) {
                    $Accgroup[$k]['display'] = true;
                }

                $Accgroup[$k]['ledgers'][$k4]['gtz'] = 0;
                $Accgroup[$k]['ledgers'][$k4]['ltz'] = 0;
                if ($balnc > 0) {
                    $Accgroup[$k]['ledgers'][$k4]['gtz'] = abs($balnc);
                }
                if ($balnc < 0) {
                    $Accgroup[$k]['ledgers'][$k4]['ltz'] = abs($balnc);
                }
            }

            if ($val['accgrp_name'] == 'Current Assets') {
                // chek is starting date is branch opening date
                $openingStock = 0;
                if ($date1 == $branchOpenDate) {
                    $rateColumn = 'bs_prate';
                    $qry = OpeningStockLog::select(DB::raw('(IFNULL(sum(opstklog_prate * opstklog_stock_quantity_add),0)) as open_stock'))
                        ->where('opstklog_type', '=', 0);
                   
                        $qry->where('branch_id', '=', $this->branch_active);
                    
                    $openingStk = $qry->first('open_stock');
                    $openingStock = $openingStk->open_stock;
                } else {

                    $rateColumn = (isset($request['disp_purch_rate']) && $request['disp_purch_rate'] == 'avg') ? 'bs_avg_prate' : 'bs_prate';
                    $uptoDateStock = StockDailyUpdates::select(DB::raw('(IFNULL(sum(' . $rateColumn . ' * sdu_stock_quantity),0)) as uptodate_stock'))
                        ->join('branch_stocks', 'branch_stocks.branch_stock_id', 'stock_daily_updates.sdu_branch_stock_id')
                        ->where(DB::raw('DATE(sdu_date)'), '>=', $branchOpenDate)
                        ->where(DB::raw('DATE(sdu_date)'), '<=', $date1)->first('uptodate_stock');
                    $openingStock += $uptoDateStock->uptodate_stock;
                }

                // if ($ptype != 'all') {

                // }

                $Accgroup[$k]['balance'] += $openingStock;
                $newRow = array('ledger_name' => "Opening Stock",
                    'balance' => $openingStock,
                    'gtz' => ($openingStock > 0) ? $openingStock : 0,
                    'ltz' => ($openingStock < 0) ? abs($openingStock) : 0);
                $Accgroup[$k]['ledgers'][] = $newRow;
            }

            $Accgroup[$k]['gtz'] = 0;
            $Accgroup[$k]['ltz'] = 0;
            if ($Accgroup[$k]['balance'] > 0) {
                $Accgroup[$k]['gtz'] = abs($Accgroup[$k]['balance']);
            }
            if ($Accgroup[$k]['balance'] < 0) {
                $Accgroup[$k]['ltz'] = abs($Accgroup[$k]['balance']);
            }
            $total_debit += $Accgroup[$k]['gtz'];
            $total_credit += $Accgroup[$k]['ltz'];
        }
        $info['opening_bal'] = $openingStock;
        if ($date1 == $branchOpenDate) {
            $branchOpenPrevDate = date('Y-m-d', strtotime('-1 day', strtotime($branchOpenDate)));
            $openingBalance = Acc_voucher::select(DB::raw('(IFNULL(sum(vch_in),0) - IFNULL(sum(vch_out),0)) as op_bal_sum'))
                ->where([
                    ['branch_id', '=', $this->branch_active],
                    ['vch_vchtype_id', '=', 14],
                    [DB::raw('DATE(vch_date)'), '=', date('Y-m-d', strtotime($branchOpenPrevDate))],
                    ['vch_flags', '=', 1],
                ])->first();
            $info['opening_bal'] = $openingBalance['op_bal_sum'] + $openingStock;
        }
        if ($info['opening_bal'] > 0) {
            $total_credit += abs($info['opening_bal']);
        }
        if ($info['opening_bal'] < 0) {
            $total_debit += abs($info['opening_bal']);
        }
        $info['total_debit'] = $total_debit;
        $info['total_credit'] = $total_credit;

        if (isset($date1) && isset($date2)) {
            $info['Date']['date1'] = date("d-F-Y", strtotime($date1));
            $info['Date']['date2'] = date("d-F-Y", strtotime($date2));
            $info['Date']['branch_open_date'] = date("d-F-Y", strtotime($branchOpenDate));
        } else {
            $info['Date'] = "";
        }

        return response()->json(['data' => $Accgroup, 'info' => $info, 'status' => 200]);

    }

    public function openingBalanceSummary($request)
    {

        $where = array();
        $where[] = ['accgrp_flags', '=', 1];
        $where[] = ['accgrp_depth', '=', 0];

        if (isset($request['group_id']) && $request['group_id'] != '') {

            if ($request['group_id']['accgrp_parent'] > 0) {
                $where[] = ['accgrp_id', '=', $request['group_id']['accgrp_parent']];
            } else {
                $where[] = ['accgrp_id', '=', $request['group_id']['accgrp_id']];
            }

        }

        $rltn = [
            'subGroups' => function ($qr) use ($request) {
                $rltn = [
                    'ledgers' => function ($qr) use ($request) {
                        $qr->select('*');

                        if ($this->branch_id > 0) {
                            $qr->where('acc_ledgers.branch_id', '=', $this->branch_id);
                        } else {
                
                            if ($this->active_branch_id == 'All') {
                
                            } else {
                                $qr->where('acc_ledgers.branch_id', '=', $this->active_branch_id);
                            }
                        }

                    },
                ];

                $qr->select('*')->where('accgrp_depth', 1)->with($rltn);
                if (isset($request['group_id']) && $request['group_id'] != ''
                    && $request['group_id']['accgrp_depth'] != 0) {

                    $qr->where('accgrp_id', '=', $request['group_id']['accgrp_id']);

                }
            },
            'ledgers' => function ($qr) use ($request) {
                $qr->select('*');
                if (isset($request['group_id']) && $request['group_id'] != '') {
                    $qr->where('ledger_accgrp_id', '=', $request['group_id']['accgrp_id']);

                }

                if ($this->branch_id > 0) {
                    $qr->where('acc_ledgers.branch_id', '=', $this->branch_id);
                } else {
        
                    if ($this->active_branch_id == 'All') {
        
                    } else {
                        $qr->where('acc_ledgers.branch_id', '=', $this->active_branch_id);
                    }
                }

            },

        ];
        $total_debit = $total_credit = 0;
        $Accgroup = Acc_group::select('*')->with($rltn)->where($where)->paginate(1000);
        $company_data = CompanySettings::first();

        $date1 = $date2 = null;
        if ($ptype = isset($request['period_type']) ? $request['period_type'] : 0) {
            list($date1, $date2) = $this->datesFromPeriods($ptype, $request);
        }
        foreach ($Accgroup as $k => $val) {

            foreach ($val['subGroups'] as $k2 => $val2) {

                $Accgroup[$k]['subGroups'][$k2]['vch_in'] = 0;
                $Accgroup[$k]['subGroups'][$k2]['vch_out'] = 0;
                foreach ($val2['ledgers'] as $k3 => $val3) {
                    $balance = $this->getOpeningBalance($val3['ledger_id']);

                    if ($balance['vch_in'] > $balance['vch_out']) {

                        $net_calculated = $balance['vch_in'] - $balance['vch_out'];

                        $Accgroup[$k]['subGroups'][$k2]['ledgers'][$k3]['vch_in'] = $net_calculated;
                        $Accgroup[$k]['subGroups'][$k2]['ledgers'][$k3]['vch_out'] = 0;

                        $Accgroup[$k]['vch_in'] += $net_calculated;
                        $Accgroup[$k]['subGroups'][$k2]['vch_in'] += $net_calculated;

                        $total_debit = $total_debit + $net_calculated;

                    } else {

                        $net_calculated = $balance['vch_out'] - $balance['vch_in'];

                        $Accgroup[$k]['subGroups'][$k2]['ledgers'][$k3]['vch_in'] = 0;
                        $Accgroup[$k]['subGroups'][$k2]['ledgers'][$k3]['vch_out'] = $net_calculated;

                        $Accgroup[$k]['vch_out'] += $net_calculated;
                        $Accgroup[$k]['subGroups'][$k2]['vch_out'] += $net_calculated;

                        $total_credit = $total_credit + $net_calculated;
                    }

                }
            }

            foreach ($val['ledgers'] as $k4 => $val4) {
                $balance = $this->getOpeningBalance($val4['ledger_id']);

                if ($balance['vch_in'] > $balance['vch_out']) {
                    $net_calculated = $balance['vch_in'] - $balance['vch_out'];
                    $Accgroup[$k]['vch_in'] += $net_calculated;
                    $Accgroup[$k]['ledgers'][$k4]['vch_in'] = $net_calculated;
                    $Accgroup[$k]['ledgers'][$k4]['vch_out'] = 0;

                    $total_debit = $total_debit + $net_calculated;
                } else {
                    $net_calculated = $balance['vch_out'] - $balance['vch_in'];
                    $Accgroup[$k]['vch_out'] += $net_calculated;
                    $Accgroup[$k]['ledgers'][$k4]['vch_out'] = $net_calculated;
                    $Accgroup[$k]['ledgers'][$k4]['vch_in'] = 0;
                    $total_credit = $total_credit + $net_calculated;
                }

            }

        }

        if (isset($date1) && isset($date2)) {
            $info['Date']['date1'] = date("d-F-Y", strtotime($date1));
            $info['Date']['date2'] = date("d-F-Y", strtotime($date2));
        } else {
            $info['Date'] = "";
        }

        $info['total_credit'] = $total_credit;
        $info['total_debit'] = $total_debit;
        return response()->json(['data' => $Accgroup, 'info' => $info, 'status' => 200]);

    }
    public function getOpeningBalance($ledger_id)
    {

        

        $result['vch_out'] = 0;
        $result['vch_in'] = 0;
        if ($this->branch_id) {
            $this->branch_active =$this->branch_id;
        } else {

            $this->branch_active =$this->active_branch_id;
           
        }

        $branchDet = Acc_branch::where('branch_id', $this->branch_active)->first('branch_open_date');


        $branchOpenPrevDate = date('Y-m-d', strtotime('-1 day', strtotime($branchDet->branch_open_date)));
        $inOut = Acc_voucher::where([
            ['branch_id', '=', $this->branch_active],
            ['vch_vchtype_id', '=', 14],
            ['vch_ledger_to', '=', $ledger_id],
            ['vch_date', '=', date('Y-m-d 00:00:00', strtotime($branchOpenPrevDate))],
            ['vch_flags', '=', 1],

        ])->first();

        if ($inOut) {
            $op_balance = $inOut->vch_out - $inOut->vch_in;
        } else {
            $op_balance = 0;
        }

        if ($op_balance > 0) {
            $result['vch_out'] = abs($op_balance);
        }

        if ($op_balance < 0) {
            $result['vch_in'] = abs($op_balance);
        }
        return $result;
    }
}
