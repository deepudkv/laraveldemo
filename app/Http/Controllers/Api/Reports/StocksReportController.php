<?php

namespace App\Http\Controllers\Api\Reports;

use App\Http\Controllers\Api\ApiParent;
use App\Models\Product\Product;
use App\Models\Stocks\BranchStocks;
use App\Models\Stocks\StockBatches;
use App\Models\Stocks\StockDailyUpdates;
use DB;
use Illuminate\Http\Request;

class StocksReportController extends ApiParent
{

    public function Report(Request $request, $type)
    {

        switch ($type) {
            case 'summary':
                $out = $this->stockSummary($request);

                $out['calc_mode'] = $request['mrp'] ? 'mrp' : 'purchase_rate';
                $remark = $request['avg_rate'] ? 'Using Average Purchase Rate ' : 'Using Last Purchase Rate';
                $out['total']['remark'] = $remark;

                return parent::displayData($out);
                break;

            case 'opening_stock_summary':
                return parent::displayData($this->openStockSummary($request));
                break;

            case 'opening_stock_log':
                return parent::displayData($this->openStockLog($request));
                break;

            case 'by_period':
                return parent::displayData($this->stockSummaryByPeriod($request));
                break;

            case 'stock_expiry_summary':
                return parent::displayData($this->stockExpirySummary($request));
                break;

            default:
                return parent::displayError('Invalid Endpoint', $this->badrequest_stat);
                break;
        }
    }

    public function stockSummary($rq)
    {

        // $company_branch = trim($rq->branch ? $rq->branch : 0);
        // $this->branch_id = trim(($this->usr_type == 2) ? $company_branch : $this->branch_id);

        $qty_filter = isset($rq['qty_filter']) ? $rq['qty_filter'] : 0;
        $qty_val = isset($rq['qty_val']) ? $rq['qty_val'] : 0;
        $qty_val2 = isset($rq['qty_val2']) ? $rq['qty_val2'] : 0;

        $rate_filter = isset($rq['rate_filter']) ? $rq['rate_filter'] : 0;
        $rate_val = isset($rq['rate_val']) ? $rq['rate_val'] : 0;
        $rate_val2 = isset($rq['rate_val2']) ? $rq['rate_val2'] : 0;

        $expiry_filter = isset($rq['expiry']) ? $rq['expiry'] : 0;
        $expiry_val = isset($rq['expiry_val']) ? $rq['expiry_val'] : 0;
        $expiry_val2 = isset($rq['expiry_val2']) ? $rq['expiry_val2'] : 0;

        $bar_filter = isset($rq['bar_filter']) ? $rq['bar_filter'] : 0;
        $bar_val = isset($rq['bar_val']) ? $rq['bar_val'] : 0;
        $bar_val2 = isset($rq['bar_val2']) ? $rq['bar_val2'] : 0;

        $ean_key = isset($rq['ean_key']) ? $rq['ean_key'] : 0;

        if (isset($rq['prd_id'])) {
            $whr['prd_id'] = $rq['prd_id'];
        }
        if (isset($rq['prd_cat_id'])) {
            $whr['prd_cat_id'] = $rq['prd_cat_id'];
        }
        if (isset($rq['prd_manufact_id'])) {
            $whr['prd_supplier'] = $rq['prd_manufact_id'];
        }

        $mrp_calc = isset($rq['mrp']) ? $rq['mrp'] : 0;
        $avg_rate_calc = isset($rq['avg_rate']) ? $rq['avg_rate'] : 0;
        if ($mrp_calc) {
            $rate = "bs_srate as rate";
            $amount = DB::raw('(bs_stock_quantity * bs_srate) as amount');
        } else {
            if ($avg_rate_calc) {
                $rate = "bs_avg_prate as rate";
                $amount = DB::raw('(bs_stock_quantity * bs_avg_prate) as amount');
            } else {
                $rate = "bs_prate as rate";
                $amount = DB::raw('(bs_stock_quantity * bs_prate) as amount');
            }
        }
        // DB::enableQueryLog();
        $slct = Product::select(
            'prd_name as name', 'prd_barcode',
            'bs_stock_quantity as stock',
            $rate,
            'cat_name',
            $amount
        );

        $qry = isset($whr) ? $slct->where($whr) : $slct;
        // quantity filtartion
        if ($qty_filter && $qty_val) {
            switch ($qty_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $qry = $qry->where('bs_stock_quantity', $qty_filter, $qty_val);
                    break;
                case 'between':
                    if ($qty_val2) {
                        $qry = $qry->whereBetween('bs_stock_quantity', [$qty_val, $qty_val2]);
                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }
        // purchase rate filtartion
        if ($rate_filter && $rate_val) {
            switch ($rate_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $qry = $qry->where('bs_prate', $rate_filter, $rate_val);
                    break;
                case 'between':
                    if ($rate_val2) {
                        $qry = $qry->whereBetween('bs_prate', [$rate_val, $rate_val2]);
                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }

        // barcode rate filtartion
        if ($bar_filter && $bar_val) {

            switch ($bar_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $qry = $qry->where('prd_barcode', $bar_filter, $bar_val);
                    break;
                case 'between':
                    if ($bar_val2) {
                        $qry = $qry->whereBetween('prd_barcode', [$bar_val, $bar_val2]);
                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }

        if ($this->branch_id > 0) {
            $qry = $qry->Join(
                'branch_stocks',
                function ($join) {
                    $join->on('products.prd_id', 'branch_stocks.bs_prd_id')
                        ->where('bs_branch_id', $this->branch_id);
                }
            );
        } else {
            $qry = $qry->Join(
                'branch_stocks',
                function ($join) {
                    $join->on('products.prd_id', 'branch_stocks.bs_prd_id')
                        ->where('bs_branch_id', $this->active_branch_id);
                }
            );
        }

        $qry = $qry->join('categories', 'categories.cat_id', '=', 'products.prd_cat_id');

        if (isset($rq['active_stock']) && $rq['active_stock']) {
            $qry = $qry->where('bs_stock_quantity', '!=', 0);
        }

        if ($expiry_filter) {
            $sb_branch_stock_id = StockBatches::select('sb_branch_stock_id')->get()->toArray();
            $where_ids = array_unique(array_column($sb_branch_stock_id, 'sb_branch_stock_id'));

            // die($expiry_filter);
            switch ($expiry_filter) {
                case 'all':
                    break;

                case 'no':
                    $qry = $qry->whereNotIn('branch_stock_id', [$where_ids]);

                    break;
                case 'has':
                    $qry = $qry->whereIn('branch_stock_id', [$where_ids]);
                    $batch_exist = true;
                    break;

                case 'expired':
                    $sb_branch_stock_id = StockBatches::select('sb_branch_stock_id')->where('sb_expiry_date', '<', date('Y-m-d'))->get()->toArray();
                    $expired = array_unique(array_column($sb_branch_stock_id, 'sb_branch_stock_id'));
                    if (empty($expired)) {
                        return [];
                    }
                    $qry = $qry->whereIn('branch_stock_id', [$expired]);
                    $batch_exist = true;
                    break;

                case 'not_expired':
                    $sb_branch_stock_id = StockBatches::select('sb_branch_stock_id')->where('sb_expiry_date', '>', date('Y-m-d'))->get()->toArray();
                    $not_expired = array_unique(array_column($sb_branch_stock_id, 'sb_branch_stock_id'));

                    if (empty($not_expired)) {
                        return [];
                    }
                    $qry = $qry->whereIn('branch_stock_id', [$not_expired]);
                    $batch_exist = true;
                    break;

                case 'custom':
                    if ($expiry_val && $expiry_val2) {
                        $sb_branch_stock_id = StockBatches::select('sb_branch_stock_id')->whereBetween('sb_expiry_date', [$expiry_val, $expiry_val2])->get()->toArray();
                        $where_ids = array_unique(array_column($sb_branch_stock_id, 'sb_branch_stock_id'));
                        if (empty($where_ids)) {
                            return [];
                        }
                        $qry = $qry->whereIn('branch_stock_id', [$where_ids]);
                    } else {
                        return parent::displayError('Invalid Dates', $this->badrequest_stat);
                    }
                    $batch_exist = true;
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    $batch_exist = true;
                    break;
            }
        }


        
        if ($this->branch_id > 0) {
            $qry = $qry->where('bs_branch_id', $this->branch_id);
        } else {

            if($this->active_branch_id=='All')
            {
                $qry = $qry->where('bs_branch_id','!=',$this->active_branch_id);
               
            }
            else
            {
                $qry = $qry->where('bs_branch_id', $this->active_branch_id);
            }

        
        }

        $count = $qry->orderBy('cat_name', 'ASC')->groupby('prd_name')->get()->toArray();
        $result = $qry->orderBy('cat_name', 'ASC')->groupby('prd_name')->paginate(100)->toArray();
        // print_r(DB::getQueryLog());
        // print_r($result );die();
        return empty($result['data']) ? [] : $this->arrayFormat($result, $count);
    }

    public function arrayFormat($result, $total)
    {
        foreach ($result['data'] as $key => $val) {
            $items[$val['cat_name']][] = $val;
        }
        $j = 0;
        foreach ($items as $cat_name => $prd) {

            unset($prd[0]['cat_name']);
            $cat['name'] = $cat_name;
            $cat['stock'] = array_sum(array_column($prd, 'stock'));
            $cat['amount'] = array_sum(array_column($prd, 'amount'));
            $cat['rate'] = ($cat['stock'] > 0) ? round($cat['amount'] / $cat['stock']) : 0;

            $response[$j]['category'][] = $cat;
            $response[$j]['products'] = $prd;
            $j++;
        }

        unset($result['data']);
        $result['data'] = $response;

        $tot['total_products'] = count($total);
        $tot['total_stock'] = array_sum(array_column($total, 'stock'));
        $tot['total_purchase_amount'] = array_sum(array_column($total, 'amount'));
        $tot['total_categories'] = count(array_unique(array_column($total, 'cat_name')));

        $tot['total_products'] = count($total);
        $result['total'] = $tot;

        return $result;
        //return [$result, $tot];
    }

    public function arrayReduce($data)
    {
        asort($data);
        $id = 0;
        $i = 0;
        foreach ($data as $key => $value) {
            // return $data[$key];
            if ($data[$key]['prd_id'] == $id) {

                $res[$i]['name'] = $data[$key]['prd_name'];
                $res[$i]['barcode'] = $data[$key]['prd_barcode'];
                $res[$i]['open_stock_qty'] = $res[$i]['open_stock_qty'] + $data[$key]['open_stock_qty'];
                $res[$i]['purch_amount'] = $res[$i]['purch_amount'] + $data[$key]['purch_amount'];

                $id = $data[$key]['prd_id'];
            } else {
                $id = $data[$key]['prd_id'];
                $i = $i + 1;
                $res[$i]['name'] = $data[$key]['prd_name'];
                $res[$i]['barcode'] = $data[$key]['prd_barcode'];
                $res[$i]['open_stock_qty'] = $data[$key]['open_stock_qty'];
                $res[$i]['purch_amount'] = $data[$key]['purch_amount'];
            }
        }

        return $res;
    }

    public function calcPurchPrice($result)
    {

        foreach ($result as $key => $res) {
            foreach ($res as $key => $val) {
                $val['purch_amount'] = $val['purch_amount'];
                $val['p_rate'] = ($val['open_stock_qty'] > 0) ? ($val['purch_amount'] / $val['open_stock_qty']) : 0;
                $out[] = $val;
            }
        }

        return $out;

    }

    public function totalPurchAmount($coutRes)
    {
//   asort($coutRes);
        //   return $coutRes;
        $order_products = array();
        foreach ($coutRes as $product) {
            $order_products[$product['prd_id']]['total_qty'] = 0;
            $order_products[$product['prd_id']]['total_purch_price'] = 0;
        }
        foreach ($coutRes as $product) {
            if (isset($order_products[$product['prd_id']])) {
                $order_products[$product['prd_id']]['total_qty'] += $product['open_stock_qty'];
                $order_products[$product['prd_id']]['total_purch_price'] += $product['purchase_price'];
            } else {

                $order_products[$product['prd_id']] = $product;
            }
        }

        foreach ($order_products as $key => $res) {

            $res['pur_amt'] = $res['total_qty'] * $res['total_purch_price'];
            $out[] = $res;

        }

        return $out;

    }

    public function openStockSummary($rq)
    {
        // DB::enableQueryLog();

        if ($this->branch_id) {
            $result = $coutRes = Product::select(
                'prd_id',
                'prd_name',
                'prd_barcode',
                DB::raw('SUM(opstklog_stock_quantity_add) as open_stock_qty'),
                'opening_stock_log.opstklog_prate as purchase_price',
                'cat_name',
                DB::raw('(SUM(opstklog_stock_quantity_add * opstklog_prate)) as purch_amount')
                )
                ->join('categories', 'categories.cat_id', '=', 'products.prd_cat_id')

                ->Join('branch_stocks', function ($join) {
                    $join->on('products.prd_id', 'branch_stocks.bs_prd_id')->where('bs_branch_id', $this->branch_id);
                })

                ->Join('opening_stock_log', function ($join) {
                    $join->on('branch_stocks.branch_stock_id', 'opening_stock_log.opstklog_branch_stock_id')->where('opening_stock_log.opstklog_type', 0);
                });

        } else {

            $result = $coutRes = Product::select(
                'prd_id',
                'prd_name',
                'prd_barcode',
                // 'opening_stock_log.opstklog_stock_quantity_add as open_stock_qty',
                DB::raw('SUM(opstklog_stock_quantity_add) as open_stock_qty'),

                'opening_stock_log.opstklog_prate as purchase_price',
                'cat_name',
                // DB::raw('(erp_opening_stock_log.opstklog_stock_quantity_add * erp_opening_stock_log.opstklog_prate) as purch_amount')
                DB::raw('SUM(opstklog_stock_quantity_add * opstklog_prate) as purch_amount')
            
                )
                ->join('categories', 'categories.cat_id', '=', 'products.prd_cat_id')

                ->Join('branch_stocks', function ($join) {
                    $join->on('products.prd_id', 'branch_stocks.bs_prd_id')->where('bs_branch_id', $this->active_branch_id);
                })

                ->Join('opening_stock_log', function ($join) {
                    $join->on('branch_stocks.branch_stock_id', 'opening_stock_log.opstklog_branch_stock_id')->where('opening_stock_log.opstklog_type', 0);
                });

        }

        if ($rq['osq_type'] != "") {

            switch ($rq['osq_type']) {
                case '<':
                    $result = $coutRes = $result->where('opening_stock_log.opstklog_stock_quantity_add', '<', $rq['osq_val1']);
                    break;
                case '<=':
                    $result = $coutRes = $result->where('opening_stock_log.opstklog_stock_quantity_add', '<=', $rq['osq_val1']);
                    break;
                case '=':
                    $result = $coutRes = $result->where('opening_stock_log.opstklog_stock_quantity_add', '=', $rq['osq_val1']);
                    break;
                case '>':
                    $result = $coutRes = $result->where('opening_stock_log.opstklog_stock_quantity_add', '>', $rq['osq_val1']);
                    break;
                case '>=':
                    $result = $coutRes = $result->where('opening_stock_log.opstklog_stock_quantity_add', '>=', $rq['osq_val1']);
                    break;
                case 'between':
                    $result = $coutRes = $result->whereBetween('opening_stock_log.opstklog_stock_quantity_add', [$rq['osq_val1'], $rq['osq_val2']]);
                    break;
                default:
                    return parent::displayError('Invalid Entry', $this->badrequest_stat);
                    break;
            }
        }

        if ($rq['opr_type'] != "") {

            switch ($rq['opr_type']) {
                case '<':
                    $result = $coutRes = $result->where('opening_stock_log.opstklog_prate', '<', $rq['opr_val1']);
                    break;
                case '<=':
                    $result = $coutRes = $result->where('opening_stock_log.opstklog_prate', '<=', $rq['opr_val1']);
                    break;
                case '=':
                    $result = $coutRes = $result->where('opening_stock_log.opstklog_prate', '=', $rq['opr_val1']);
                    break;
                case '>':
                    $result = $coutRes = $result->where('opening_stock_log.opstklog_prate', '>', $rq['opr_val1']);
                    break;
                case '>=':
                    $result = $coutRes = $result->where('opening_stock_log.opstklog_prate', '>=', $rq['opr_val1']);
                    break;
                case 'between':
                    $result = $coutRes = $result->whereBetween('opening_stock_log.opstklog_prate', [$rq['opr_val1'], $rq['opr_val2']]);
                    break;
                default:
                    return parent::displayError('Invalid Entry', $this->badrequest_stat);
                    break;
            }
        }

        if ($this->branch_id) {
            $result = $coutRes = $result->where('branch_stocks.bs_branch_id', $this->branch_id);
        } else {
            $result = $coutRes = $result->where('branch_stocks.bs_branch_id', $this->active_branch_id);

        }

        if (isset($rq['prd_id']) && $rq['prd_id'] != "") {
            $result = $coutRes = $result->where('prd_id', $rq['prd_id']);
        }

        if ($rq['gd_id'] != "") {
            if ($rq['gd_id'] != "ALL") {
                $result = $coutRes = $result->where('opening_stock_log.opstklog_gd_id', $rq['gd_id']);
            }
            if ($rq['gd_id'] == "Shop") {
                $result = $coutRes = $result->where('opening_stock_log.opstklog_gd_id', 0);
            }
        }
        $result = $coutRes = $result->where('opstklog_status', 1);


        $coutRes = $coutRes->orderby('prd_name', 'ASC')->groupby('prd_id')->get()->toArray();

        // $prd_count = count(array_unique(array_column($coutRes, 'prd_id')));
        $prd_count = count(array_unique(array_column($coutRes, 'prd_id')));

        $qty = array_sum(array_column($coutRes, 'open_stock_qty'));
        $purch_amt = array_sum(array_column($coutRes, 'purch_amount'));
        // print_r(DB::getQueryLog());

        $result = $result->orderBy('prd_name', 'ASC')->groupby('prd_id')->paginate(50)->toArray();
        
            $result['total_products'] = $prd_count;
            $result['total_quantity'] = $qty;
            $result['total_purch_amt'] = $purch_amt;

        return $result;

        // if ($result['data']) {

        //     foreach ($result['data'] as $key => $val) {
        //         $out[$val['cat_name']][] = $val;
        //     }

        //     $i = 0;
        //     foreach ($out as $key => $val) {
        //         $category['name'] = $key;

        //         $category['totl_open_qty'] = array_sum(array_column($val, 'open_stock_qty'));

        //         $category['purchase_price'] = array_sum(array_column($val, 'purch_amount'));

        //         $sort[$i]['category'][] = $category;
        //         $sort[$i]['items'] = $val;

        //         $sort[$i]['prods'][] = $this->arrayReduce($sort[$i]['items']);
        //     // return $sort[$i]['prods'];exit;
        //         $sort[$i]['products'] = $this->calcPurchPrice($sort[$i]['prods']);

        //         unset($sort[$i]['items']);
        //         unset($sort[$i]['prods']);
        //         $i++;
        //     }
        //     // return $res;
        //     unset($result['data']);

        //     $result['data'] = $sort;

        //     $result['total_products'] = $prd_count;
        //     $result['total_quantity'] = $qty;
        //     $result['total_purch_amt'] = $purch_amt;

        //     return $result;
        // } else {
        //     return null;
        // }
    }

    public function openStockLog($rq)
    {

        $add_qty_filter = isset($rq['add_qty_filter']) ? $rq['add_qty_filter'] : 0;
        $add_qty_val = isset($rq['add_qty_val']) ? $rq['add_qty_val'] : 0;
        $add_qty_val2 = isset($rq['add_qty_val2']) ? $rq['add_qty_val2'] : 0;

        $rmv_qty_filter = isset($rq['rmv_qty_filter']) ? $rq['rmv_qty_filter'] : 0;
        $rmv_qty_val = isset($rq['rmv_qty_val']) ? $rq['rmv_qty_val'] : 0;
        $rmv_qty_val2 = isset($rq['rmv_qty_val2']) ? $rq['rmv_qty_val2'] : 0;

        $pur_rate_filter = isset($rq['pur_rate_filter']) ? $rq['pur_rate_filter'] : 0;
        $pur_rt_val = isset($rq['pur_rt_val']) ? $rq['pur_rt_val'] : 0;
        $pur_rt_val2 = isset($rq['pur_rt_val2']) ? $rq['pur_rt_val2'] : 0;

        // $company_branch = trim($rq->branch ? $rq->branch : 0);
        // $this->branch_id = trim(($this->usr_type == 2) ? $company_branch : $this->branch_id);

        $result = Product::select(
            'prd_id',
            'prd_name',
            'prd_barcode',
            'opening_stock_log.opstklog_date as open_stock_date',
            'opening_stock_log.opstklog_type',
            'opening_stock_log.opstklog_stock_quantity_add as os_add_qty',
            'opening_stock_log.opstklog_stock_quantity_rem as os_rem_qty',
            'opening_stock_log.opstklog_prate as purchase_price',

            DB::raw('erp_opening_stock_log.opstklog_stock_quantity_add*erp_opening_stock_log.opstklog_prate AS add_pur_amt'),
            DB::raw('erp_opening_stock_log.opstklog_stock_quantity_rem*erp_opening_stock_log.opstklog_prate AS rem_pur_amt')

        )
            ->join('branch_stocks', 'products.prd_id', 'branch_stocks.bs_prd_id')
            ->join('opening_stock_log', 'branch_stocks.branch_stock_id', 'opening_stock_log.opstklog_branch_stock_id');

        if ($add_qty_filter != "" & $add_qty_val != "") {

            switch ($add_qty_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $result = $result->where('opening_stock_log.opstklog_stock_quantity_add', $add_qty_filter, $add_qty_val);
                    break;
                case 'between':
                    $result = $result->whereBetween('opening_stock_log.opstklog_stock_quantity_add', [$add_qty_val, $add_qty_val2]);
                    break;
                default:
                    return parent::displayError('Invalid Entry', $this->badrequest_stat);
                    break;
            }
        }

        if ($rmv_qty_filter & $add_qty_val) {
            switch ($rmv_qty_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $result = $result->where('opening_stock_log.opstklog_stock_quantity_rem', $rmv_qty_filter, $rmv_qty_val);
                    break;
                case 'between':
                    $result = $result->whereBetween('opening_stock_log.opstklog_stock_quantity_rem', [$rmv_qty_val, $rmv_qty_val2]);
                    break;
                default:
                    return parent::displayError('Invalid Entry', $this->badrequest_stat);
                    break;
            }
        }

        if ($pur_rate_filter & $pur_rt_val) {
            switch ($pur_rate_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $result = $result->where('opening_stock_log.opstklog_prate', $rmv_qty_filter, $pur_rt_val);
                    break;
                case 'between':
                    $result = $result->whereBetween('opening_stock_log.opstklog_prate', [$pur_rt_val, $pur_rt_val2]);
                    break;
                default:
                    return parent::displayError('Invalid Entry', $this->badrequest_stat);
                    break;
            }
        }

        if (isset($rq['opstklog_type']) && $rq['opstklog_type'] != "") {
            $whr['opstklog_type'] = $rq['opstklog_type'];
        }

        if (isset($rq['prd_id']) && $rq['prd_id'] != "") {
            $whr['prd_id'] = $rq['prd_id'];
        }

        if ($this->branch_id > 0) {
            $whr['branch_stocks.bs_branch_id'] = $this->branch_id;
        }

        if ($ptype = isset($rq['period_type']) ? $rq['period_type'] : 0) {
            list($date1, $date2) = $this->datesFromPeriods($ptype, $rq);

            if ($date1 && $date2) {
                $result = $result->whereBetween('opstklog_date', [$date1, $date2]);
            }
        }

        $result = isset($whr) ? $result->where($whr) : $result;

        $total_returns = $result;
        $total_add = $total_returns;
        $dataRes = $sum_qty_add = $result;

        $data = $result->get()->toArray();

        $result = $result->orderBy('prd_name')->paginate(50)->toArray();
        // return $result;
        if ($result['data']) {

            foreach ($result['data'] as $key => $val) {
                $out[$val['prd_name']][] = $val;

                unset($result[$key]);
            }

            unset($result['data']);

            $result['products'] = $out;

            $result['total_products'] = count(array_unique(array_column($data, 'prd_id')));

            $total_num = array_count_values(array_column($data, 'opstklog_type'));

            // return $total_num;
            if (isset($rq['opstklog_type']) && $rq['opstklog_type'] == 0) {
                $result['total_add'] = $total_num[0];
            }
            if (isset($rq['opstklog_type']) && $rq['opstklog_type'] == 1) {
                $result['total_return'] = $total_num[1];
            }

            $add_qty = 0;
            $rem_qty = 0;
            $add_price = 0;
            $rem_price = 0;
            foreach ($data as $key => $val) {

                if ($val['opstklog_type'] == 0) {

                    $add_qty += $val['os_add_qty'];

                    $add_price += $val['add_pur_amt'];
                } elseif ($val['opstklog_type'] == 1) {

                    $rem_qty += $val['os_rem_qty'];
                    $rem_price += $val['rem_pur_amt'];
                }
            }

            $result['sum_qty_add'] = $add_qty;
            $result['sum_qty_rem'] = $rem_qty;

            $result['sum_add_price'] = $add_price;
            $result['sum_rem_price'] = $rem_price;

            $result['rem_op_stk_qty'] = $add_qty - $rem_qty;

            $result['rem_op_stk_price'] = $add_price - $rem_price;

            return $result;

        } else {
            return null;
        }
    }

    public function stockSummaryByPeriod($rq)
    {
        $whr = $this->whrbtwn = array();
        // $company_branch = trim($rq->branch ? $rq->branch : 0);
        // $this->branch_id = trim(($this->usr_type == 2) ? $company_branch : $this->branch_id);

        if ($prd_cat_id = isset($rq['prd_cat_id']) ? $rq['prd_cat_id'] : 0) {
            $whr['prd_cat_id'] = $prd_cat_id;
        }
        if ($prd_manufact_id = isset($rq['prd_manufact_id']) ? $rq['prd_manufact_id'] : 0) {
            $whr['prd_supplier'] = $prd_manufact_id;
        }

        $bar_filter = isset($rq['bar_filter']) ? $rq['bar_filter'] : 0;
        $bar_val = isset($rq['bar_val']) ? $rq['bar_val'] : 0;
        $bar_val2 = isset($rq['bar_val2']) ? $rq['bar_val2'] : 0;

        $qry = StockDailyUpdates::groupBy('sdu_stock_id')
            ->selectRaw('sum(sdu_stock_quantity) as stock, sdu_prd_id,sdu_stock_id');

        if ($ptype = isset($rq['period_type']) ? $rq['period_type'] : 0) {
            list($date1, $date2) = $this->datesFromPeriods($ptype, $rq);
            if ($date1 && $date2) {
                $qry = $qry->whereBetween('sdu_date', [$date1, $date2]);
            }

        }

        if ($prd_id = isset($rq['prd_id']) ? $rq['prd_id'] : 0) {
            $qry = $qry->where('sdu_prd_id', $prd_id);
            $whr['prd_id'] = $prd_id;
        } else {

            if (!empty($whr)) {
                $prds = Product::select('prd_id')->where($whr)->get()->toArray();
                if (empty($prds)) {
                    return [];
                } else {
                    $prd_ids = array_column($prds, 'prd_id');
                    $qry = $qry->whereIn('sdu_prd_id', $prd_ids);
                }
            }

        }

        // barcode rate filtartion
        if ($bar_filter && $bar_val) {

            switch ($bar_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $prd_ids = Product::select('prd_id')->where('prd_barcode', $bar_filter, $bar_val)->get()->toArray();
                    $prd_ids = array_column($prd_ids, 'prd_id');
                    $qry = $qry->whereIn('sdu_prd_id', $prd_ids);
                    break;

                case 'between':

                    if ($bar_val2) {
                        $prd_ids = Product::select('prd_id')->whereBetween('prd_barcode', [$bar_val, $bar_val2])->get()->toArray();
                        $prd_ids = array_column($prd_ids, 'prd_id');
                        $qry = $qry->whereIn('sdu_prd_id', $prd_ids);
                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }

        if ($qry->get()->count()) {
            $this->avg_rate = isset($rq['avg_rate']) ? $rq['avg_rate'] : 0;
            $this->whr = $whr;
            $rltn = [
                'prdData' => function ($qr) {

                    if (!empty($this->whr)) {
                        $qr->select('prd_id', 'prd_name', 'prd_barcode', 'prd_cat_id', 'prd_supplier')->where($this->whr);
                    } else {
                        $qr->select('prd_id', 'prd_name', 'prd_barcode', 'prd_cat_id', 'prd_supplier');

                    }
                },
                'rateInfo' => function ($qr) {

                    if ($this->avg_rate) {

                        if ($this->branch_id > 0) {
                            $qr->select('bs_stock_id', 'bs_avg_prate as rate')->where('bs_branch_id', $this->branch_id);
                        } else {
                            $qr->select('bs_stock_id', 'bs_avg_prate as rate')->where('bs_branch_id', $this->active_branch_id);
                        }

                    } else {

                        if ($this->branch_id > 0) {
                            $qr->select('bs_stock_id', 'bs_prate as rate')->where('bs_branch_id', $this->branch_id);
                        } else {
                            $qr->select('bs_stock_id', 'bs_prate as rate')->where('bs_branch_id', $this->active_branch_id);
                        }

                    }
                },
            ];
            // return $this->avg_rate;
            if ($this->branch_id > 0) {
                $qry = $qry->where('branch_id', $this->branch_id);
            } else {
                $qry = $qry->where('branch_id', $this->active_branch_id);
            }

            $sum = $qry->with($rltn)->get()->toArray();

            $result = $qry->with($rltn)->paginate(10)->toArray();
            $total['total_item'] = count($sum);
            $total['total_stock'] = array_sum(array_column($sum, 'stock'));
            $total_amount = 0;
            foreach ($sum as $key => $val) {
                $total_amount = $total_amount + $val['stock'] * $val['rate_info']['rate'];
            }
            $total['total_amount'] = $total_amount;
            $total['remark'] = $rq['avg_rate'] ? 'Using Average Purchase Rate' : 'Using Last Purchase Rate';
            $out = $qry->with($rltn)->paginate(10)->toArray();

            $out['total'] = $total;

            if ($rq['period_type'] != "") {
                $out['Date']['date1'] = date("d-F-Y", strtotime($date1));
                $out['Date']['date2'] = date("d-F-Y", strtotime($date2));
            } else {
                $out['Date'] = "";
            }

            return $out;

        }
        return [];

    }

    public function datesFromPeriods($pt, $rq)
    {
        switch ($pt) {

            case 't':
                $date1 = $date2 = date('Y-m-d');
                break;

            case 'ld':
                $date1 = $date2 = date('Y-m-d', strtotime("-1 days"));
                break;

            case 'lw':
                $previous_week = strtotime("-1 week +1 day");
                $start_week = strtotime("last sunday midnight", $previous_week);
                $end_week = strtotime("next saturday", $start_week);
                $date1 = date("Y-m-d", $start_week);
                $date2 = date("Y-m-d", $end_week);
                break;

            case 'lm':
                $date1 = date("Y-m-d", strtotime("first day of previous month"));
                $date2 = date("Y-m-d", strtotime("last day of previous month"));
                break;

            case 'ly':
                $year = date('Y') - 1;
                $date1 = $year . "-01-01";
                $date2 = $year . "-12-31";
                break;

            case 'c':

                $date1 = isset($rq['date1']) ? date('Y-m-d H:i:s', strtotime($rq['date1'])) : 0;
                $date2 = isset($rq['date2']) ? date('Y-m-d H:i:s', strtotime($rq['date2'])) : 0;

                break;

            default:
                return false;
                break;
        }
        return [$date1, $date2];
    }

    public function stockExpirySummaryold($request)
    {
        $query = BranchStocks::join('products', 'products.prd_id', '=', 'branch_stocks.bs_prd_id')->leftjoin('stock_batches', 'stock_batches.sb_id', '=', 'branch_stocks.bs_batch_id');

        if (isset($request['filter']) && $request['filter'] == 1) {
            if (isset($request['prd_id']) && $request['prd_id'] != '') {
                $res = $query->where('products.prd_id', '=', $request['prd_id']);
            }

            if (isset($request['prd_barcode']) && $request['prd_barcode'] != '') {
                $res = $query->where('products.prd_barcode', '=', $request['prd_barcode']);
            }
            if (isset($request['prd_batchcode']) && $request['prd_batchcode'] != '') {
                $res = $query->where('stock_batches.sb_batch_code', '=', $request['prd_batchcode']);
            }

            if (isset($request['period_exp']) && $request['period_exp'] != '') {
                $date1 = date('Y-m-d H:i:s', strtotime($request['exp1']));
                $date2 = date('Y-m-d H:i:s', strtotime($request['exp2']));
                switch ($request['period_exp']) {
                    case '>':
                        $res = $query->where('stock_batches.sb_manufacture_date', '>', $date1);
                        break;
                    case '<':
                        $res = $query->where('stock_batches.sb_manufacture_date', '<', $date1);
                        break;
                    case '>=':
                        $res = $query->where('stock_batches.sb_manufacture_date', '>=', $date1);
                        break;
                    case '<=':
                        $res = $query->where('stock_batches.sb_manufacture_date', '<=', $date1);
                        break;
                    case '=':
                        $res = $query->where('stock_batches.sb_manufacture_date', '=', $date1);
                        break;
                    case 'between':

                        $res = $query->whereBetween('stock_batches.sb_manufacture_date', [$date1, $date2]);
                        break;
                }
            }

            if (isset($request['period_mfg']) && $request['period_mfg'] != '') {
                $date1 = date('Y-m-d H:i:s', strtotime($request['mfg1']));
                $date2 = date('Y-m-d H:i:s', strtotime($request['mfg2']));
                switch ($request['period_mfg']) {
                    case '>':
                        $res = $query->where('stock_batches.sb_expiry_date', '>', $date1);
                        break;
                    case '<':
                        $res = $query->where('stock_batches.sb_expiry_date', '<', $date1);
                        break;
                    case '>=':
                        $res = $query->where('stock_batches.sb_expiry_date', '>=', $date1);
                        break;
                    case '<=':
                        $res = $query->where('stock_batches.sb_expiry_date', '<=', $date1);
                        break;
                    case '=':
                        $res = $query->where('stock_batches.sb_expiry_date', '=', $date1);
                        break;
                    case 'between':
                        $res = $query->whereBetween('stock_batches.sb_expiry_date', [$date1, $date2]);
                        break;
                }
            }

            if (isset($request['status_id']) && $request['status_id'] != '') {
                $res = $query->where('branch_stocks.bs_expiry', '=', $request['status_id']);
            }

        }
        // end filter

        $res = $query->paginate($this->per_page, array('products.prd_name as product', 'branch_stocks.bs_expiry as expiry', 'products.prd_barcode as barcode', 'products.prd_exp_dur as validity', 'stock_batches.sb_batch_code as batchcode', 'stock_batches.sb_manufacture_date as mfg', 'stock_batches.sb_expiry_date as exp'));
        return $res;

    }

    public function stockExpirySummary($request)
    {
        $date_today = date('Y-m-d');
        $query = StockBatches::join('products', 'products.prd_id', '=', 'stock_batches.sb_prd_id');

        if (isset($request['filter']) && $request['filter'] == 1) {
            if (isset($request['prd_id']) && $request['prd_id'] != '') {
                $res = $query->where('products.prd_id', '=', $request['prd_id']);
            }

            if (isset($request['prd_barcode']) && $request['prd_barcode'] != '') {
                $res = $query->where('products.prd_barcode', '=', $request['prd_barcode']);
            }
            if (isset($request['prd_batchcode']) && $request['prd_batchcode'] != '') {
                $res = $query->where('stock_batches.sb_batch_code', '=', $request['prd_batchcode']);
            }

            if (isset($request['period_exp']) && $request['period_exp'] != '') {
                $date1 = date('Y-m-d H:i:s', strtotime($request['exp1']));
                $date2 = date('Y-m-d H:i:s', strtotime($request['exp2']));
                switch ($request['period_exp']) {
                    case '>':
                        $res = $query->where('stock_batches.sb_expiry_date', '>', $date1);
                        break;
                    case '<':
                        $res = $query->where('stock_batches.sb_expiry_date', '<', $date1);
                        break;
                    case '>=':
                        $res = $query->where('stock_batches.sb_expiry_date', '>=', $date1);
                        break;
                    case '<=':
                        $res = $query->where('stock_batches.sb_expiry_date', '<=', $date1);
                        break;
                    case '=':
                        $res = $query->where('stock_batches.sb_expiry_date', '=', $date1);
                        break;
                    case 'between':

                        $res = $query->whereBetween('stock_batches.sb_expiry_date', [$date1, $date2]);
                        break;
                }
            }

            if (isset($request['period_mfg']) && $request['period_mfg'] != '') {
                $date1 = date('Y-m-d H:i:s', strtotime($request['mfg1']));
                $date2 = date('Y-m-d H:i:s', strtotime($request['mfg2']));
                switch ($request['period_mfg']) {
                    case '>':
                        $res = $query->where('stock_batches.sb_manufacture_date', '>', $date1);
                        break;
                    case '<':
                        $res = $query->where('stock_batches.sb_manufacture_date', '<', $date1);
                        break;
                    case '>=':
                        $res = $query->where('stock_batches.sb_manufacture_date', '>=', $date1);
                        break;
                    case '<=':
                        $res = $query->where('stock_batches.sb_manufacture_date', '<=', $date1);
                        break;
                    case '=':
                        $res = $query->where('stock_batches.sb_manufacture_date', '=', $date1);
                        break;
                    case 'between':
                        $res = $query->whereBetween('stock_batches.sb_manufacture_date', [$date1, $date2]);
                        break;
                }
            }

            if (isset($request['status_id']) && $request['status_id'] != '') {
                switch ($request['status_id']) {
                    case '1':
                        $res = $query->where('stock_batches.sb_expiry_date', '<', $date_today);
                        break;
                    case '2':
                        $res = $query->where('stock_batches.sb_expiry_date', '>', $date_today);
                        break;
                }
            }

        }

        if ($this->branch_id) {
            $query = $query->where('stock_batches.branch_id', $this->branch_id);
        }
        else
        {
            $query = $query->where('stock_batches.branch_id', $this->active_branch_id);
        }

        // end filter

        $res = $query->paginate($this->per_page, array('products.prd_name as product', 'products.prd_barcode as barcode', 'products.prd_exp_dur as validity', 'stock_batches.sb_manufacture_date as mfg', 'stock_batches.sb_expiry_date as exp', 'stock_batches.sb_batch_code as batchcode', DB::raw('CASE WHEN erp_stock_batches.sb_expiry_date < "' . $date_today . '" THEN 1 ELSE 0 END as expiry'), DB::Raw('DATEDIFF(erp_stock_batches.sb_expiry_date,erp_stock_batches.sb_manufacture_date) as validity')));
        return $res;

    }
}

// $slct = Product::select('prd_id', 'prd_name', 'bs_stock_quantity', 'bs_prate', 'bs_srate', 'prd_cat_id', 'prd_supplier',
// DB::raw('(bs_stock_quantity * bs_prate) as purchase_amount'),
// DB::raw('(bs_stock_quantity * bs_avg_prate) as avg_purchase_amount'),
// DB::raw('(SELECT cat_name FROM erp_categories WHERE erp_products.prd_cat_id = erp_categories.cat_id )
//   as cat_name'), DB::raw('(bs_stock_quantity * bs_srate) as sales_amount'));
