<?php

namespace App\Http\Controllers\Api\Reports;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\ApiParent;
use App\Models\Product\Product;
use App\Models\Godown\GodownStockLog;
use App\Models\Godown\GodownMaster;

use DB;
use validator;

class GodownReportController extends ApiParent
{


    public function Report(Request $request, $type)
    {

        switch ($type) {
            case 'product':
                return parent::displayData($this->GodownByProduct($request));
                break;

            case 'transaction':
                return parent::displayData($this->GodownByTrans($request));
                break;

            case 'summary':
           
                $out = $this->GodownStockSummary($request);
               
                $out['list_type'] = isset($request['is_godown']) ? 'Godown' : 'Shop';

                $out['alldetais']['list_type_remark'] = isset($request['is_godown']) ? 'Godown Wise Details' : 'Shop Wise Details';
                $out['calc_mode'] = $request->mrp ? 'mrp' : 'purchase_rate';

                $remark = $request->avg_rate ? "Using Average Purchase Rate " : "Using Last Purchase Rate";
                $out['alldetais']['remark'] = $remark;
                return parent::displayData($out);
                break;

            default:
                return parent::displayError('Invalid Endpoint', $this->badrequest_stat);
                break;
        }
    }




    public function GodownByProduct($rq)
    {


        // $company_branch = trim($rq->branch ? $rq->branch : 0);
        // $this->branch_id = trim(($this->usr_type == 2) ? $company_branch : $this->branch_id);

        DB::enableQueryLog();

        $allres =  $result = GodownStockLog::select(
            'products.prd_id',
            'products.prd_name',
            'products.prd_barcode',
            'godown_stock_log.gsl_date as gwn_stock_date',
            'godown_stock_log.gsl_qty',
            'stock_unit_rates.branch_stock_id',
            'stock_unit_rates.sur_unit_rate as sales_rate',
            'branch_stocks.bs_prate as pur_rate',
            DB::raw('IFNULL(erp_stock_unit_rates.sur_unit_rate, 0) as sales_rate'),
            DB::raw('(SELECT cat_name FROM erp_categories WHERE erp_products.prd_cat_id = erp_categories.cat_id )
                as cat_name'),
            DB::raw('(SELECT sum(sur_unit_rate) FROM erp_stock_unit_rates WHERE erp_godown_stock_log.gsl_branch_stock_id =  erp_stock_unit_rates.branch_stock_id AND erp_godown_stock_log.gsl_prod_unit =  erp_stock_unit_rates.sur_unit_id ) as unit_rate')

        )

            ->leftJoin('stock_unit_rates', function ($join) {
                $join->on('godown_stock_log.gsl_branch_stock_id', '=', 'stock_unit_rates.branch_stock_id')->on('godown_stock_log.gsl_prod_unit', 'stock_unit_rates.sur_unit_id');
            })

            ->join('products', 'godown_stock_log.gsl_prd_id', 'products.prd_id')
            ->join('branch_stocks', 'branch_stocks.branch_stock_id', 'godown_stock_log.gsl_branch_stock_id');




        if (isset($rq['prd_id']) && $rq['prd_id'] != "") {

            $result =  $result->where('products.prd_id', $rq['prd_id']);
            $allres = $allres->where('products.prd_id', $rq['prd_id']);
        }

        if (isset($rq['cat_id']) && $rq['cat_id'] != "") {

            $result =  $result->where('products.prd_cat_id', $rq['cat_id']);
            $allres = $allres->where('products.prd_cat_id', $rq['cat_id']);
        }

        if (isset($rq['gd_id']) && $rq['gd_id'] != "") {

            $result =  $result->where('godown_stock_log.gsl_to', $rq['gd_id']);
            $allres = $allres->where('godown_stock_log.gsl_to', $rq['gd_id']);
        }

        if (isset($rq['add_by']) && $rq['add_by'] != "") {

            $result =  $result->where('godown_stock_log.gsl_added_by', $rq['add_by']);
            $allres = $allres->where('godown_stock_log.gsl_added_by', $rq['add_by']);
        }

        if ($ptype = isset($rq['period_type']) ? $rq['period_type'] : 0) {
            list($date1, $date2) = $this->datesFromPeriods($ptype, $rq);

            if ($date1 && $date2) {
                $result = $result->whereBetween('godown_stock_log.gsl_date', [$date1, $date2]);
                $allres = $allres->whereBetween('godown_stock_log.gsl_date', [$date1, $date2]);
            }
        }
       if($this->branch_id > 0){
        $result = $result->where('godown_stock_log.branch_id',$this->branch_id);
        $allres = $allres->where('godown_stock_log.branch_id',$this->branch_id);
    }

        $allresult = $allres->orderBy('prd_name')->get()->toArray();
        $result = $result->orderBy('prd_name')->paginate(10)->toArray();

        // return $this->usr_id;
        // print_r(DB::getQueryLog());exit;

        if ($allresult && $result) {

            foreach ($result['data'] as $key => $val) {
                $items[$val['cat_name']][] = $val;
            }

            foreach ($allresult as $key => $val) {
                $allitems[$val['cat_name']][] = $val;
            }




            $j = 0;

            foreach ($allitems as $cat_name => $allprd) {

                foreach ($items as $key => $prd) {

                    if ($key == $cat_name) {

                        unset($allprd[0]['cat_name']);
                        $cat['name'] = $cat_name;
                        $cat['gsl_qty'] = array_sum(array_column($allprd, 'gsl_qty'));
                        $cat['avg_sales_rate'] = array_sum(array_column($allprd, 'sales_rate')) / count(array_column($allprd, 'sales_rate'));
                        $cat['avg_pur_rate'] = array_sum(array_column($allprd, 'pur_rate')) / count(array_column($allprd, 'pur_rate'));

                        $response[$j]['category'][] = $cat;


                        $response[$j]['products'] = $this->prod_array_format($prd);

                        $j++;
                    }
                }
            }


            unset($result['data']);

            $totalRes['total_products'] = count($allresult);
            $totalRes['total_stock'] = array_sum(array_column($allresult, 'gsl_qty'));
            $totalRes['total_purchase_amount'] = array_sum(array_column($allresult, 'pur_rate'));
            $totalRes['total_categories'] = count(array_unique(array_column($allresult, 'cat_name')));
    
            if (isset($rq['gd_id']) && ($rq['gd_id'] != "" || $rq['gd_id'] != 0 )) {
    
                $totalRes['godownRep'] =  $this->getGodown($rq['gd_id']);
            }
    
    
            $result['alldetais'] = $totalRes;
            $result['data'] = $response;

            if ($rq['period_type'] != "") {
                $result['Date']['date1'] = date("d-F-Y", strtotime($date1));
                $result['Date']['date2'] = date("d-F-Y", strtotime($date2));
            }else{
                $result['Date'] = "";
            }
    

            return $result;
        } else {

            return "No Result";
        }
    }










    public function GodownByTrans($rq)
    {


        // $company_branch = trim($rq->branch ? $rq->branch : 0);
        // $this->branch_id = trim(($this->usr_type == 2) ? $company_branch : $this->branch_id);

        DB::enableQueryLog();

        $allres =  $result = GodownStockLog::select(
            'products.prd_id',
            'products.prd_name',
            'products.prd_barcode',
            'godown_stock_log.gsl_date as gwn_stock_date',
            // 'godown_stock_log.gsl_qty',
            'stock_unit_rates.branch_stock_id',
            'units.unit_base_qty',
            DB::raw('(erp_units.unit_base_qty * erp_godown_stock_log.gsl_qty) as gsl_qty'),
            
            // 'stock_unit_rates.sur_unit_rate as sales_rate',
            'branch_stocks.bs_prate as pur_rate',
            DB::raw('((erp_units.unit_base_qty * erp_godown_stock_log.gsl_qty) * erp_branch_stocks.bs_prate) as pur_amt'),
            DB::raw('IFNULL(erp_stock_unit_rates.sur_unit_rate, 0) as sales_rate'),
            DB::raw('(SELECT cat_name FROM erp_categories WHERE erp_products.prd_cat_id = erp_categories.cat_id )
                as cat_name'),

            // 'godown_stock_log.gsl_to', 'godown_stock_log.gsl_from',   
            DB::raw('(SELECT sum(sur_unit_rate) FROM erp_stock_unit_rates WHERE erp_godown_stock_log.gsl_branch_stock_id =  erp_stock_unit_rates.branch_stock_id AND erp_godown_stock_log.gsl_prod_unit =  erp_stock_unit_rates.sur_unit_id ) as unit_rate'),

            DB::raw('(SELECT gd_name FROM erp_godown_master WHERE erp_godown_stock_log.gsl_from = erp_godown_master.gd_id )
                as gd_from'),
            DB::raw('(SELECT gd_name FROM erp_godown_master WHERE erp_godown_stock_log.gsl_to = erp_godown_master.gd_id )
                as gd_to')

        )

            ->leftJoin('stock_unit_rates', function ($join) {
                $join->on('godown_stock_log.gsl_branch_stock_id', '=', 'stock_unit_rates.branch_stock_id')->on('godown_stock_log.gsl_prod_unit', 'stock_unit_rates.sur_unit_id')
                ->where('stock_unit_rates.sur_branch_id',$this->branch_id);
            })


            // ->join('godown_master', function ($join) {
            //     $join->on('godown_master.gd_id', 'godown_stock_log.gsl_from')->on('godown_master.gd_id','godown_stock_log.gsl_to');
            // })     

            ->join('products', 'godown_stock_log.gsl_prd_id', 'products.prd_id')
            ->join('branch_stocks', 'branch_stocks.branch_stock_id', 'godown_stock_log.gsl_branch_stock_id')
            ->join('units', 'units.unit_id', 'godown_stock_log.gsl_prod_unit');


        if (isset($rq['prd_id']) && $rq['prd_id'] != "") {

            $allres =   $result =  $result->where('products.prd_id', $rq['prd_id']);
        }

        if (isset($rq['prd_cat_id']) && $rq['prd_cat_id'] != "") {

            $allres =  $result =  $result->where('products.prd_cat_id', $rq['prd_cat_id']);
        }

        if (isset($rq['gd_id_from']) && is_int($rq['gd_id_from'])) {

            $allres =   $result =  $result->where('godown_stock_log.gsl_from', $rq['gd_id_from']);
        }

        if (isset($rq['gd_id_to']) && is_int($rq['gd_id_to'])) {

            $allres =   $result =  $result->where('godown_stock_log.gsl_to', $rq['gd_id_to']);
        }

        if (isset($rq['add_by']) && $rq['add_by'] != "") {

            $allres =    $result =  $result->where('godown_stock_log.gsl_added_by', $rq['add_by']);
        }

        if ($ptype = isset($rq['period_type']) ? $rq['period_type'] : 0) {
            list($date1, $date2) = $this->datesFromPeriods($ptype, $rq);

            if ($date1 && $date2) {
                $allres =    $result = $result->whereBetween('godown_stock_log.gsl_date', [$date1, $date2]);
            }
        }

        if($this->branch_id > 0){
            $allres = $result = $result->where('godown_stock_log.branch_id',$this->branch_id);
        }

        $tot = $allres->orderBy('gsl_date','desc')->get()->toArray();
        $result = $result->orderBy('gsl_date','desc')->paginate(10)->toArray();
        // print_r(DB::getQueryLog());exit;
        // return $result;
        
        
        if ($result['data']) {
            // return $result;	
            foreach ($result['data'] as $key => $val) {
                $items[$val['cat_name']][] = $val;
            }


            $j = 0;
            // return $items;
            foreach ($items as $cat_name => $prd) {

                unset($prd[0]['cat_name']);
                $cat['name'] = $cat_name;
                $cat['gsl_qty'] = array_sum(array_column($prd, 'gsl_qty'));
                $cat['avg_sales_rate'] = array_sum(array_column($prd, 'sales_rate')) / count(array_column($prd, 'sales_rate'));
                $cat['avg_pur_rate'] = array_sum(array_column($prd, 'pur_rate')) / count(array_column($prd, 'pur_rate'));
                $cat['avg_pur_amt'] = array_sum(array_column($prd, 'pur_amt'));


                // $prd[$j]['from'] = $this->getGodown($prd[0]['gsl_from']);
                // $prd[$j]['to'] = $this->getGodown($prd[0]['gsl_to']);

                $response[$j]['category'][] = $cat;
                $response[$j]['products'] = $prd;
                // $response[$j]['products']['from'] = $this->getGodown($prd[0]['gsl_from']);
                // $response[$j]['products']['to'] = $this->getGodown($prd[0]['gsl_to']);

                $j++;
            }


            unset($result['data']);

        $totalRes['total_products'] = count($tot);
        $totalRes['total_stock'] = array_sum(array_column($tot, 'gsl_qty'));
        $totalRes['total_purchase_amount'] = array_sum(array_column($tot, 'pur_amt'));
        $totalRes['total_categories'] = count(array_unique(array_column($tot, 'cat_name')));

        if (isset($rq['gd_id']) && ($rq['gd_id'] != "" || $rq['gd_id'] != 0 )) {

            $totalRes['godownRep'] =  $this->getGodown($rq['gd_id']);
        }


        $result['alldetais'] = $totalRes;
        $result['data'] = $response;

        if ($rq['period_type'] != "") {
            $result['Date']['date1'] = date("d-F-Y", strtotime($date1));
            $result['Date']['date2'] = date("d-F-Y", strtotime($date2));
        }else{
            $result['Date'] = "";
        }


            return $result;
        } else {

            return [];
        }
    }











    public function GodownStockSummary($rq)
    {

        // $company_branch = trim($rq->branch ? $rq->branch : 0);
        // $this->branch_id = trim(($this->usr_type == 2) ? $company_branch : $this->branch_id);

        $qty_filter = isset($rq['qty_filter']) ? $rq['qty_filter'] : 0;
        $qty_val = isset($rq['qty_val']) ? $rq['qty_val'] : 0;
        $qty_val2 = isset($rq['qty_val2']) ? $rq['qty_val2'] : 0;

        $rate_filter = isset($rq['rate_filter']) ? $rq['rate_filter'] : 0;
        $rate_val = isset($rq['rate_val']) ? $rq['rate_val'] : 0;
        $rate_val2 = isset($rq['rate_val2']) ? $rq['rate_val2'] : 0;

        if (isset($rq['prd_id']) && $rq['prd_id'] != '') {
            $whr['prd_id'] = $rq['prd_id'];
        }
        if (isset($rq['prd_cat_id']) && $rq['prd_cat_id'] != '') {
            $whr['prd_cat_id'] = $rq['prd_cat_id'];
        }
        if (isset($rq['prd_manufact_id']) && $rq['prd_manufact_id'] != '') {
            $whr['prd_supplier'] = $rq['prd_manufact_id'];
        }
        if (isset($rq['is_godown']) && $rq['gd_id'] !='') {
            $whr['gd_id'] = $rq['gd_id'];
        }

      

        $totlqty = DB::raw('(bs_stock_quantity_shop + bs_stock_quantity_gd + bs_stock_quantity_van) as totalqty');


        if (isset($rq['mrp']) && $rq['mrp']) {
            $rate = "bs_srate as rate";
            if (isset($rq['is_godown'])) {
                $amount = DB::raw('(gs_qty * bs_srate) as amount');
            } else {
                $amount = DB::raw('(bs_stock_quantity * bs_srate) as amount');
            }
        } else {
            if (isset($rq['avg_rate']) && $rq['avg_rate']) {
                $rate = "bs_avg_prate as rate";
                if (isset($rq['is_godown'])) {
                    $amount = DB::raw('(gs_qty * bs_avg_prate) as amount');
                } else {
                    $amount = DB::raw('(bs_stock_quantity * bs_avg_prate) as amount');
                }
            } else {
                $rate = "bs_prate as rate";
                if (isset($rq['is_godown'])) {
                    $amount = DB::raw('(gs_qty * bs_prate) as amount');
                } else {
                    $amount = DB::raw('(bs_stock_quantity * bs_prate) as amount');
                }
            }
        }


        if (isset($rq['is_godown'])) {
            $slct = Product::select(
                'prd_name as name',
                DB::raw('(SELECT cat_name FROM erp_categories WHERE erp_products.prd_cat_id = erp_categories.cat_id )
                as cat_name'),
                'godown_stocks.gs_qty as stock',
                'godown_master.gd_name',
                $amount
            );
        } else {
            $slct = Product::select(
                'prd_name as name',
                $rate,
                $rate,
                'branch_stocks.bs_stock_quantity_gd as godown_qty',
                'branch_stocks.bs_stock_quantity_shop as shop_qty',
                'branch_stocks.bs_stock_quantity_van as van_qty',
                'branch_stocks.bs_stock_quantity as total_qty',
                DB::raw('(SELECT cat_name FROM erp_categories WHERE erp_products.prd_cat_id = erp_categories.cat_id )
                as cat_name',$totlqty),
                $amount
            );
        }

        $qry = isset($whr) ? $slct->where($whr) : $slct;
        // quantity filtartion
        if ($qty_filter && $qty_val) {
            switch ($qty_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    if (isset($rq['is_godown'])) {
                        $qry = $qry->where('gs_qty', $qty_filter, $qty_val);
                    } else {
                        $qry = $qry->where('bs_stock_quantity', $qty_filter, $qty_val);
                    }
                    break;

                case 'between':
                    if ($qty_val2) {
                        if (isset($rq['is_godown'])) {
                            $qry = $qry->whereBetween('gs_qty', [$qty_val, $qty_val2]);
                        } else {
                            $qry = $qry->whereBetween('bs_stock_quantity', [$qty_val, $qty_val2]);
                        }
                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }
        // purchase rate filtartion
        if ($rate_filter && $rate_val) {
            switch ($qty_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $qry = $qry->where('bs_prate', $rate_filter, $qty_val);
                    break;
                case 'between':
                    if ($rate_val2) {
                        $qry = $qry->whereBetween('bs_prate', [$rate_val, $rate_val2]);
                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }
        

        if (isset($rq['active_stock']) && $rq['active_stock']) {
            $qry = $qry->where('bs_stock_quantity_shop', '!=', 0)
            ->orWhere('bs_stock_quantity_gd', '!=', 0)
            ->orWhere('bs_stock_quantity_van', '!=', 0);
        }

        $qry->Join(
            'branch_stocks',
            function ($join) {
                $join->on('products.prd_id', 'branch_stocks.bs_prd_id');
                if($this->branch_id > 0)
                    $join->where('bs_branch_id', $this->branch_id);
            }
        );

        if (isset($rq['is_godown'])) {

            $qry->Join(
                'godown_stocks',
                function ($join) {
                    $join->on('branch_stocks.bs_prd_id', 'godown_stocks.gs_prd_id')
                        ->on('branch_stocks.branch_stock_id', 'godown_stocks.gs_branch_stock_id');
                        if($this->branch_id > 0)
                        $join->where('godown_stocks.branch_id', $this->branch_id);
                }
            )
                ->join('godown_master', 'godown_master.gd_id', 'godown_stocks.gs_godown_id');
        }
        $totalRes = $qry->orderBy('cat_name', 'ASC')->groupby('prd_name')->get()->toArray();
        $result = $qry->orderBy('cat_name', 'ASC')->groupby('prd_name')->paginate(10)->toArray();
        // $a = $rq->mrp;
        // return $a;
        // $total['remark'] = (!isset($rq->mrp) ? 'Using Average Purchase Rate' : 'Using Last Purchase Rate');
        
        // $result['total'] = $total;


        $tot['total_products'] = count($totalRes);
        $tot['total_stock'] = array_sum(array_column($totalRes, 'stock'));
        $tot['total_purchase_amount'] = array_sum(array_column($totalRes, 'amount'));
        $tot['total_categories'] = count(array_unique(array_column($totalRes, 'cat_name')));

        $tot['total_products'] = count($totalRes);
        $result['alldetais'] = $tot;
        
        return empty($result['data']) ? [] : $this->arrayFormat($result);
    }

    public function arrayFormat($result)
    {
        foreach ($result['data'] as $key => $val) {
            $items[$val['cat_name']][] = $val;
        }
        $j = 0;
        foreach ($items as $cat_name => $prd) {

            unset($prd[0]['cat_name']);
            $cat['name'] = $cat_name;
            $cat['stock'] = array_sum(array_column($prd, 'stock'));
            $cat['amount'] = array_sum(array_column($prd, 'amount'));
            $cat['rate'] = ($cat['stock'] > 0) ? round($cat['amount'] / $cat['stock']) : 0;

            $response[$j]['category'][] = $cat;
            $response[$j]['products'] = $prd;
            $j++;
        }

        unset($result['data']);
        $result['data'] = $response;

        return $result;
    }
















    function prod_array_format($products)
    {
        foreach ($products as $key => $val) {

            $prods[$val['prd_name']][] = $val;
        }

        $i = 0;
        foreach ($prods as $prd_name => $prd) {

            $id = array_column($prd, 'prd_id');
            $barcode = array_column($prd, 'prd_barcode');

            $prod[$i]['prd_id'] = $id[0];
            $prod[$i]['prd_barcode'] = $barcode[0];
            $prod[$i]['name'] = $prd_name;
            $prod[$i]['stock_qty'] = array_sum(array_column($prd, 'gsl_qty'));
            $prod[$i]['stock_sales_rate'] = array_sum(array_column($prd, 'sales_rate'));
            $prod[$i]['stock_pur_rate'] = array_sum(array_column($prd, 'pur_rate'));

            $i++;
        }

        return $prod;
    }


    public function getGodown($gdId)
    {

        $godown = GodownMaster::select('gd_name')->find($gdId);

        return $godown['gd_name'];
    }



    public function datesFromPeriods($pt, $rq)
    {
        switch ($pt) {

            case 't':
                $date1 = $date2 = date('Y-m-d');
                break;

            case 'ld':
                $date1 = $date2 = date('Y-m-d', strtotime("-1 days"));
                break;

            case 'lw':
                $previous_week = strtotime("-1 week +1 day");
                $start_week = strtotime("last sunday midnight", $previous_week);
                $end_week = strtotime("next saturday", $start_week);
                $date1 = date("Y-m-d", $start_week);
                $date2 = date("Y-m-d", $end_week);
                break;

            case 'lm':
                $date1 = date("Y-m-d", strtotime("first day of previous month"));
                $date2 = date("Y-m-d", strtotime("last day of previous month"));
                break;

            case 'ly':
                $year = date('Y') - 1;
                $date1 = $year . "-01-01";
                $date2 = $year . "-12-31";
                break;

            case 'c':
                $date1 = isset($rq['date1']) ? date('Y-m-d',strtotime($rq['date1'])) : 0;
                $date2 = isset($rq['date2']) ? date('Y-m-d',strtotime($rq['date2'])) : 0;

                break;

            default:
                return false;
                break;
        }
        return [$date1, $date2];
    }
}
