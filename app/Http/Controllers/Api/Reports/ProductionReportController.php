<?php

namespace App\Http\Controllers\Api\Reports;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\ApiParent;
use App\Models\Production\Production;
use App\Models\Production\ProductionCommission;
use App\Models\Production\ProductionFormula;
use App\Models\Production\ProductionFormulaComm;
use App\Models\Production\ProductionFormulaProd;
use App\Models\Production\ProductionFormulaSub;
use App\Models\Production\ProductionProducts;
use App\Models\Production\ProductionSub;
use Validator;
use DB;

class ProductionReportController extends ApiParent
{
    public function Report(Request $request, $type)
    {

        switch ($type) {
            case 'summary':
            return parent::displayData($this->productionSummary($request));
            break; 
            case 'details':
            return parent::displayData($this->productionDetails($request));
            break;
            case 'incredients':
            return parent::displayData($this->productionIncredients($request));
            break;
            case 'commission':
            return parent::displayData($this->productionCommission($request));
            break;
            case 'product':
            return parent::displayData($this->productionProduct($request));
            break;

        }
    }


    public function productionSummary($request)
    {

      $query = Production::join('production_formula', 'production.prdn_prodform_id', '=', 'production_formula.prodform_id')->where('production.prdn_flag','=',1);

      if (isset($request['filter']) && $request['filter'] == 1){

       if (isset($request['period_type']) && $request['period_type'] != '') {
        $date2 = date('Y-m-d');           
        switch( $request['period_type']) {            
            case 't':            
            $res = $query->where('production.prdn_date', $date2);
            break;
            case 'ld':
            $date1 =date('Y-m-d', strtotime("-1 days"));
            $res = $query->whereBetween('production.prdn_date', [$date1, $date2]); 
            break;           
            case 'lw':
            $res = $previous_week = strtotime("-1 week +1 day");
            $start_week = strtotime("last sunday midnight", $previous_week);
            $end_week = strtotime("next saturday", $start_week);
            $date1 = date("Y-m-d", $start_week);
            $res = $query->whereBetween('production.prdn_date', [$date1, $date2]); 
            break;
            case 'lm':
            $date1 = date("Y-m-d", strtotime("first day of previous month"));
            $res = $query->whereBetween('production.prdn_date', [$date1, $date2]);          
            break;
            case 'ly': 
            $year = date('Y') - 1;
            $date1 = $year . "-01-01"; 
            $res = $query->whereBetween('production.prdn_date', [$date1, $date2]);  
            break;
            case 'c':             
            $date1 = date('Y-m-d H:i:s', strtotime($request['date1']));
            $date2 = date('Y-m-d H:i:s', strtotime($request['date2']));  
            $res = $query->whereBetween('production.prdn_date', [$date1, $date2]); 
            break;     
        }
    }


    if (isset($request['prod_id']) && $request['prod_id'] != '') {                      
        switch($request['prod_id']) {            
            case '>':            
            $res = $query->where('production.prdn_id','>',$request['pid1']);
            break; 
            case '<':            
            $res = $query->where('production.prdn_id','<',$request['pid1']);
            break;
            case '<=':            
            $res = $query->where('production.prdn_id','<=',$request['pid1']);
            break; 
            case '>=':            
            $res = $query->where('production.prdn_id','>=',$request['pid1']);
            break; 
            case '=':            
            $res = $query->where('production.prdn_id','=',$request['pid1']);
            break; 
            case 'between':          
            $res = $query->whereBetween('production.prdn_id',[$request['pid1'], $request['pid2']]);
            break;              
        }
    } 

    if (isset($request['prod_cost']) && $request['prod_cost'] != '') {                      
        switch($request['prod_cost']) {            
            case '>':            
            $res = $query->where('production.prdn_cost','>',$request['pcost1']);
            break; 
            case '<':            
            $res = $query->where('production.prdn_cost','<',$request['pcost1']);
            break;
            case '<=':            
            $res = $query->where('production.prdn_cost','<=',$request['pcost1']);
            break; 
            case '>=':            
            $res = $query->where('production.prdn_cost','>=',$request['pcost1']);
            break; 
            case '=':            
            $res = $query->where('production.prdn_cost','=',$request['pcost1']);
            break; 
            case 'between':          
            $res = $query->whereBetween('production.prdn_cost',[$request['pcost1'], $request['pcost2']]);
            break;              
        }
    } 

    if (isset($request['prod_amount']) && $request['prod_amount'] != '') {                      
        switch($request['prod_amount']) {            
            case '>':            
            $res = $query->where('production.prdn_purch_amount','>',$request['pamount1']);
            break; 
            case '<':            
            $res = $query->where('production.prdn_purch_amount','<',$request['pamount1']);
            break;
            case '<=':            
            $res = $query->where('production.prdn_purch_amount','<=',$request['pamount1']);
            break; 
            case '>=':            
            $res = $query->where('production.prdn_purch_amount','>=',$request['pamount1']);
            break; 
            case '=':            
            $res = $query->where('production.prdn_purch_amount','=',$request['pamount1']);
            break; 
            case 'between':          
            $res = $query->whereBetween('production.prdn_purch_amount',[$request['pamount1'], $request['pamount2']]);
            break;              
        }
    } 

    if (isset($request['prod_comm']) && $request['prod_comm'] != '') {                      
        switch($request['prod_comm']) {            
            case '>':            
            $res = $query->where('production.prdn_comm_amount','>',$request['pcomm1']);
            break; 
            case '<':            
            $res = $query->where('production.prdn_comm_amount','<',$request['pcomm1']);
            break;
            case '<=':            
            $res = $query->where('production.prdn_comm_amount','<=',$request['pcomm1']);
            break; 
            case '>=':            
            $res = $query->where('production.prdn_comm_amount','>=',$request['pcomm1']);
            break; 
            case '=':            
            $res = $query->where('production.prdn_comm_amount','=',$request['pcomm1']);
            break; 
            case 'between':          
            $res = $query->whereBetween('production.prdn_comm_amount',[$request['pcomm1'], $request['pcomm2']]);
            break;              
        }
    } 

    if (isset($request['prod_misc']) && $request['prod_misc'] != '') {                      
        switch($request['prod_misc']) {            
            case '>':            
            $res = $query->where('production.prdn_misc_exp','>',$request['pmisc1']);
            break; 
            case '<':            
            $res = $query->where('production.prdn_misc_exp','<',$request['pmisc1']);
            break;
            case '<=':            
            $res = $query->where('production.prdn_misc_exp','<=',$request['pmisc1']);
            break; 
            case '>=':            
            $res = $query->where('production.prdn_misc_exp','>=',$request['pmisc1']);
            break; 
            case '=':            
            $res = $query->where('production.prdn_misc_exp','=',$request['pmisc1']);
            break; 
            case 'between':          
            $res = $query->whereBetween('production.prdn_misc_exp',[$request['pmisc1'], $request['pmisc2']]);
            break;              
        }

    }

    if (isset($request['prd_formula']) && $request['prd_formula'] != '') {
       $res = $query->where('production_formula.prodform_name','=',$request['prd_formula']);
   }         

}

if($this->branch_id > 0)
$query = $query->where('production.branch_id',$this->branch_id);

$res = $query->paginate($this->per_page, array('prdn_id as id',
   'prdn_date as prd_date','prdn_cost as total_cost','prdn_purch_amount as incr_amount','prdn_comm_amount as commission','prdn_misc_exp as misc_exp','production_formula.prodform_name as formula'));

   if ($request['period_type'] != "") {
    $data['Date']['date1'] = date("d-F-Y", strtotime($date1));
    $data['Date']['date2'] = date("d-F-Y", strtotime($date2));
}else{
    $data['Date'] = "";
}

$data['data'] = $res;

return $data;
}

public function productionDetails($request)
{
    $validator = Validator::make(
        $request->all(),
        ['prdn_id' => 'required|exists:production,prdn_id'],
        ['prdn_id.required' => 'Id required',
        'prdn_id.exists' => 'Id not exists']           
    );

    if ($validator->fails()) {
        return $validator->messages();
    }


    $queryopprod = Production::join('production_formula', 'production.prdn_prodform_id', '=', 'production_formula.prodform_id')->leftjoin('production_products', 'production_products.prdnprd_prdn_id', '=', 'production.prdn_id')->join('products', 'products.prd_id', '=', 'production_products.prdnprd_prd_id')->leftjoin('godown_master', 'godown_master.gd_id', '=', 'production_products.prdnprd_gd_id')->join('units', 'products.prd_base_unit_id', '=', 'units.unit_id')->join('users', 'production.prdn_added_by', '=', 'users.usr_id')->where('production.prdn_id','=',$request['prdn_id']);  

    $resop = $queryopprod->get(array('prdn_id as id',
        'prdn_date as prd_date','prdn_cost as sum_total_purch_amount','prdn_purch_amount as total_purch_amount','prdn_comm_amount as commission','prdn_misc_exp as misc_exp','production_formula.prodform_name as formula','production_products.prdnprd_rate as rate','production_products.prdnprd_qty as opqty','production_products.prdnprd_required_qty as reqqty','products.prd_name as product',
        'units.unit_code as unit','users.usr_name as addedby','production_products.prdnprd_mfg_date as mfd',
        'production_products.prdnprd_exp_date as exp','godown_master.gd_name as godown',
        'production.prdn_flag as flag' 
    )
);

    $queryincre = Production::join('production_formula', 'production.prdn_prodform_id', '=', 'production_formula.prodform_id')->leftjoin('production_sub', 'production.prdn_id', '=', 'production_sub.prdnsub_prdn_id')->join('products', 'products.prd_id', '=', 'production_sub.prdnsub_prd_id')->join('units', 'products.prd_base_unit_id', '=', 'units.unit_id')->join('users', 'production.prdn_added_by', '=', 'users.usr_id')->where('production.prdn_id','=',$request['prdn_id']);
    $resincr = $queryincre->get(array('prdn_date as prd_date','prdn_cost as total_cost','prdn_purch_amount as incr_amount','production.prdn_comm_amount as commission','prdn_misc_exp as misc_exp','production_formula.prodform_name as formula','production_sub.prdnsub_rate as rate','production_sub.prdnsub_qty as opqty','products.prd_name as product','products.prd_barcode as barcode',
        'units.unit_code as unit','users.usr_name as addedby','production.prdn_id as id'));


    $data['increds'] = $resincr;
    $data['op'] = $resop;
    return $data;
} 


public function productionIncredients($request)
{
    $query = ProductionSub::join('products', 'products.prd_id', '=', 'production_sub.prdnsub_prd_id')
    ->join('production', 'production.prdn_id', '=', 'production_sub.prdnsub_prdn_id')
    ->join('production_formula','production_formula.prodform_id', '=', 'production_sub.prdnsub_prodform_id')->where('production.prdn_flag','=',1);

    if (isset($request['filter']) && $request['filter'] == 1){

     if (isset($request['period_type']) && $request['period_type'] != '') {
        $date2 = date('Y-m-d');           
        switch( $request['period_type']) {            
            case 't':            
            $res = $query->where('production_sub.prdnsub_date', $date2);
            break;
            case 'ld':
            $date1 =date('Y-m-d', strtotime("-1 days"));
            $res = $query->whereBetween('production_sub.prdnsub_date', [$date1, $date2]); 
            break;           
            case 'lw':
            $res = $previous_week = strtotime("-1 week +1 day");
            $start_week = strtotime("last sunday midnight", $previous_week);
            $end_week = strtotime("next saturday", $start_week);
            $date1 = date("Y-m-d", $start_week);
            $res = $query->whereBetween('production_sub.prdnsub_date', [$date1, $date2]); 
            break;
            case 'lm':
            $date1 = date("Y-m-d", strtotime("first day of previous month"));
            $res = $query->whereBetween('production_sub.prdnsub_date', [$date1, $date2]);          
            break;
            case 'ly': 
            $year = date('Y') - 1;
            $date1 = $year . "-01-01"; 
            $res = $query->whereBetween('production_sub.prdnsub_date', [$date1, $date2]);  
            break;
            case 'c':             
            $date1 = date('Y-m-d H:i:s', strtotime($request['date1']));
            $date2 = date('Y-m-d H:i:s', strtotime($request['date2']));  
            $res = $query->whereBetween('production_sub.prdnsub_date', [$date1, $date2]); 
            break;     
        }
    }

    if (isset($request['purchase_rate']) && $request['purchase_rate'] != '') {                      
        switch($request['purchase_rate']) {            
            case '>':            
            $res = $query->where('production_sub.prdnsub_rate','>',$request['prate1']);
            break; 
            case '<':            
            $res = $query->where('production_sub.prdnsub_rate','<',$request['prate1']);
            break;
            case '<=':            
            $res = $query->where('production_sub.prdnsub_rate','<=',$request['prate1']);
            break; 
            case '>=':            
            $res = $query->where('production_sub.prdnsub_rate','>=',$request['prate1']);
            break; 
            case '=':            
            $res = $query->where('production_sub.prdnsub_rate','=',$request['prate1']);
            break; 
            case 'between':          
            $res = $query->whereBetween('production_sub.prdnsub_rate',[$request['prate1'], $request['prate2']]);
            break;              
        }

    }


    if (isset($request['prod_amount']) && $request['prod_amount'] != '') {                      
        switch($request['prod_amount']) {            
            case '>':            
            $res = $query->where(DB::raw('(erp_production_sub.prdnsub_rate*erp_production_sub.prdnsub_qty)'),'>',$request['pamount1']);
            break; 
            case '<':            
            $res = $query->where(DB::raw('(erp_production_sub.prdnsub_rate*erp_production_sub.prdnsub_qty)'),'<',$request['pamount1']);
            break;
            case '<=':            
            $res = $query->where(DB::raw('(erp_production_sub.prdnsub_rate*erp_production_sub.prdnsub_qty)'),'<=',$request['pamount1']);
            break; 
            case '>=':            
            $res = $query->where(DB::raw('(erp_production_sub.prdnsub_rate*erp_production_sub.prdnsub_qty)'),'>=',$request['pamount1']);
            break; 
            case '=':            
            $res = $query->where(DB::raw('(erp_production_sub.prdnsub_rate*erp_production_sub.prdnsub_qty)'),'=',$request['pamount1']);
            break; 
            case 'between':          
            $res = $query->whereBetween(DB::raw('(erp_production_sub.prdnsub_rate*erp_production_sub.prdnsub_qty)'),[$request['pamount1'], $request['pamount1']]);
            break;              
        }

    }

    if (isset($request['prd_formula']) && $request['prd_formula'] != '') {   
        $res = $query->where('production_formula.prodform_name','=',$request['prd_formula']);
    }


    if (isset($request['incre_name']) && $request['incre_name'] != '') {   
        $res = $query->where('products.prd_name','=',$request['incre_name']);
    }

    if (isset($request['usr_id']) && $request['usr_id'] != '') {   
        $res = $query->where('production_sub.prdnsub_added_by','=',$request['usr_id']);
    }


}



if($this->branch_id > 0)
$query = $query->where('production_sub.branch_id',$this->branch_id);


$res = $query->paginate($this->per_page, array('products.prd_name as product','products.prd_barcode as barcode','production_sub.prdnsub_qty as qty','production_formula.prodform_name as formula','production_sub.prdnsub_date as pdate','production_sub.prdnsub_rate as purchrate')); 


if ($request['period_type'] != "") {
    $data['Date']['date1'] = date("d-F-Y", strtotime($date1));
    $data['Date']['date2'] = date("d-F-Y", strtotime($date2));
}else{
    $data['Date'] = "";
}

$data['data'] = $res;


return $data;

}


public function productionCommission($request)
{
    $query = ProductionCommission::leftjoin('production', 'production.prdn_id', '=', 'production_comm.prdncomm_prdn_id')
    ->leftjoin('acc_ledgers', 'production_comm.prdncomm_laccount_no', '=', 'acc_ledgers.ledger_id')->join('production_formula', 'production_formula.prodform_id', '=', 'production_comm.prdncomm_prodform_id')->where('production.prdn_flag','=',1);

    if (isset($request['filter']) && $request['filter'] == 1){

 if (isset($request['period_type']) && $request['period_type'] != '') {
        $date2 = date('Y-m-d');           
        switch( $request['period_type']) {            
            case 't':            
            $res = $query->where('production_comm.prdncomm_date', $date2);
            break;
            case 'ld':
            $date1 =date('Y-m-d', strtotime("-1 days"));
            $res = $query->whereBetween('production_comm.prdncomm_date', [$date1, $date2]); 
            break;           
            case 'lw':
            $res = $previous_week = strtotime("-1 week +1 day");
            $start_week = strtotime("last sunday midnight", $previous_week);
            $end_week = strtotime("next saturday", $start_week);
            $date1 = date("Y-m-d", $start_week);
            $res = $query->whereBetween('production_comm.prdncomm_date', [$date1, $date2]); 
            break;
            case 'lm':
            $date1 = date("Y-m-d", strtotime("first day of previous month"));
            $res = $query->whereBetween('production_comm.prdncomm_date', [$date1, $date2]);          
            break;
            case 'ly': 
            $year = date('Y') - 1;
            $date1 = $year . "-01-01"; 
            $res = $query->whereBetween('production_comm.prdncomm_date', [$date1, $date2]);  
            break;
            case 'c':             
            $date1 = date('Y-m-d H:i:s', strtotime($request['date1']));
            $date2 = date('Y-m-d H:i:s', strtotime($request['date2']));  
            $res = $query->whereBetween('production_comm.prdncomm_date', [$date1, $date2]); 
            break;     
        }
    }

    if (isset($request['comm_rate']) && $request['comm_rate'] != '') {                      
        switch($request['comm_rate']) {            
            case '>':            
            $res = $query->where('production_comm.prdncomm_in','>',$request['crate1']);
            break; 
            case '<':            
            $res = $query->where('production_comm.prdncomm_in','<',$request['crate1']);
            break;
            case '<=':            
            $res = $query->where('production_comm.prdncomm_in','<=',$request['crate1']);
            break; 
            case '>=':            
            $res = $query->where('production_comm.prdncomm_in','>=',$request['crate1']);
            break; 
            case '=':            
            $res = $query->where('production_comm.prdncomm_in','=',$request['crate1']);
            break; 
            case 'between':          
            $res = $query->whereBetween('production_comm.prdncomm_in',[$request['crate1'], $request['crate2']]);
            break;              
        }

    }

    if (isset($request['prod_id']) && $request['prod_id'] != '') {                      
        switch($request['prod_id']) {            
            case '>':            
            $res = $query->where('production.prdn_id','>',$request['pid1']);
            break; 
            case '<':            
            $res = $query->where('production.prdn_id','<',$request['pid1']);
            break;
            case '<=':            
            $res = $query->where('production.prdn_id','<=',$request['pid1']);
            break; 
            case '>=':            
            $res = $query->where('production.prdn_id','>=',$request['pid1']);
            break; 
            case '=':            
            $res = $query->where('production.prdn_id','=',$request['pid1']);
            break; 
            case 'between':          
            $res = $query->whereBetween('production.prdn_id',[$request['pid1'], $request['pid2']]);
            break;              
        }
    } 

    if (isset($request['prd_formula']) && $request['prd_formula'] != '') {   
        $res = $query->where('production_formula.prodform_name','=',$request['prd_formula']);
    }

    if (isset($request['usr_id']) && $request['usr_id'] != '') {   
        $res = $query->where('production_formula.prodform_added_by','=',$request['usr_id']);
    }

}



if($this->branch_id > 0)
$query = $query->where('production_comm.branch_id',$this->branch_id);


$res = $query->paginate($this->per_page, array('acc_ledgers.ledger_name as staff',
    'production.prdn_id as prdnid','production_comm.prdncomm_date as prdndate','production_comm.prdncomm_in as commission','production_formula.prodform_name as formula')); 




    if ($request['period_type'] != "") {
        $data['Date']['date1'] = date("d-F-Y", strtotime($date1));
        $data['Date']['date2'] = date("d-F-Y", strtotime($date2));
    }else{
        $data['Date'] = "";
    }
    
    $data['data'] = $res;


return $data;
}


public function productionProduct($request)
{

    $query = ProductionProducts::join('production', 'production_products.prdnprd_prdn_id', '=', 'production.prdn_id')->join('products', 'products.prd_id', '=', 'production_products.prdnprd_prd_id')->join('production_formula', 'production.prdn_prodform_id', '=', 'production_formula.prodform_id')->join('units', 'products.prd_base_unit_id', '=', 'units.unit_id')->where('production.prdn_flag','=',1);


if (isset($request['filter']) && $request['filter'] == 1){

       if (isset($request['period_type']) && $request['period_type'] != '') {
        $date2 = date('Y-m-d');           
        switch( $request['period_type']) {            
            case 't':            
            $res = $query->where('production.prdn_date', $date2);
            break;
            case 'ld':
            $date1 =date('Y-m-d', strtotime("-1 days"));
            $res = $query->whereBetween('production.prdn_date', [$date1, $date2]); 
            break;           
            case 'lw':
            $res = $previous_week = strtotime("-1 week +1 day");
            $start_week = strtotime("last sunday midnight", $previous_week);
            $end_week = strtotime("next saturday", $start_week);
            $date1 = date("Y-m-d", $start_week);
            $res = $query->whereBetween('production.prdn_date', [$date1, $date2]); 
            break;
            case 'lm':
            $date1 = date("Y-m-d", strtotime("first day of previous month"));
            $res = $query->whereBetween('production.prdn_date', [$date1, $date2]);          
            break;
            case 'ly': 
            $year = date('Y') - 1;
            $date1 = $year . "-01-01"; 
            $res = $query->whereBetween('production.prdn_date', [$date1, $date2]);  
            break;
            case 'c':             
            $date1 = date('Y-m-d H:i:s', strtotime($request['date1']));
            $date2 = date('Y-m-d H:i:s', strtotime($request['date2']));  
            $res = $query->whereBetween('production.prdn_date', [$date1, $date2]); 
            break;     
        }
    }
   

    if (isset($request['period_mfg']) && $request['period_mfg'] != '') {
          $date1 = date('Y-m-d H:i:s', strtotime($request['mfg1']));
          $date2 = date('Y-m-d H:i:s', strtotime($request['mfg2']));             
        switch( $request['period_mfg']) {            
            case '>':            
            $res = $query->where('production_products.prdnprd_mfg_date','>', $date1);
            break;
            case '<':            
            $res = $query->where('production_products.prdnprd_mfg_date','<', $date1);
            break;           
            case '>=':
             $res = $query->where('production_products.prdnprd_mfg_date','>=', $date1);
            break;
            case '<=':
           $res = $query->where('production_products.prdnprd_mfg_date','<=', $date1);       
            break;
            case '=': 
            $res = $query->where('production_products.prdnprd_mfg_date','=', $date1); 
            break;
            case 'between':         
           $res = $query->whereBetween('production_products.prdnprd_mfg_date', [$date1, $date2]); 
            break;     
        }
    }

      if (isset($request['period_exp']) && $request['period_exp'] != '') {
         $date1 = date('Y-m-d H:i:s', strtotime($request['exp1']));
          $date2 = date('Y-m-d H:i:s', strtotime($request['exp2']));            
        switch( $request['period_exp']) {            
            case '>':            
            $res = $query->where('production_products.prdnprd_exp_date','>', $date1);
            break;
            case '<':
              $res = $query->where('production_products.prdnprd_exp_date','<', $date1);
            break;           
            case '>=':
            $res = $query->where('production_products.prdnprd_exp_date','>=', $date1);
            break;
            case '<=':
            $res = $query->where('production_products.prdnprd_exp_date','<=', $date1);  
            break;
            case '=': 
             $res = $query->where('production_products.prdnprd_exp_date','=', $date1); 
            break;
            case 'between':             
           
            $res = $query->whereBetween('production_products.prdnprd_exp_date', [$date1, $date2]); 
            break;     
        }
    }
   


    if (isset($request['prod_id']) && $request['prod_id'] != '') {                      
        switch($request['prod_id']) {            
            case '>':            
            $res = $query->where('production.prdn_id','>',$request['pid1']);
            break; 
            case '<':            
            $res = $query->where('production.prdn_id','<',$request['pid1']);
            break;
            case '<=':            
            $res = $query->where('production.prdn_id','<=',$request['pid1']);
            break; 
            case '>=':            
            $res = $query->where('production.prdn_id','>=',$request['pid1']);
            break; 
            case '=':            
            $res = $query->where('production.prdn_id','=',$request['pid1']);
            break; 
            case 'between':          
            $res = $query->whereBetween('production.prdn_id',[$request['pid1'], $request['pid2']]);
            break;              
        }
    } 

    if (isset($request['unit_cost']) && $request['unit_cost'] != '') {                      
        switch($request['unit_cost']) {            
            case '>':            
            $res = $query->where('production_products.prdnprd_rate','>',$request['ucost1']);
            break; 
            case '<':            
            $res = $query->where('production_products.prdnprd_rate','<',$request['ucost1']);
            break;
            case '<=':            
            $res = $query->where('production_products.prdnprd_rate','<=',$request['ucost1']);
            break; 
            case '>=':            
            $res = $query->where('production_products.prdnprd_rate','>=',$request['ucost1']);
            break; 
            case '=':            
            $res = $query->where('production_products.prdnprd_rate','=',$request['ucost1']);
            break; 
            case 'between':          
            $res = $query->whereBetween('production_products.prdnprd_rate',[$request['ucost1'], $request['ucost2']]);
            break;              
        }
    } 


   if (isset($request['unit_qty']) && $request['unit_qty'] != '') {                      
        switch($request['unit_qty']) {            
            case '>':            
            $res = $query->where('production_products.prdnprd_qty','>',$request['pqty1']);
            break; 
            case '<':            
            $res = $query->where('production_products.prdnprd_qty','<',$request['pqty1']);
            break;
            case '<=':            
            $res = $query->where('production_products.prdnprd_qty','<=',$request['pqty1']);
            break; 
            case '>=':            
            $res = $query->where('production_products.prdnprd_qty','>=',$request['pqty1']);
            break; 
            case '=':            
            $res = $query->where('production_products.prdnprd_qty','=',$request['pqty1']);
            break; 
            case 'between':          
            $res = $query->whereBetween('production_products.prdnprd_qty',[$request['pqty1'], $request['pqty2']]);
            break;              
        }
    } 


     if (isset($request['unit_qty_sum']) && $request['unit_qty_sum'] != '') {                      
        switch($request['unit_qty_sum']) {            
            case '>':            
            $res = $query->where('production_products.totalqty','>',$request['pqtys1']);
            break; 
            case '<':            
            $res = $query->where('production_products.totalqty','<',$request['pqtys1']);
            break;
            case '<=':            
            $res = $query->where('production_products.prdnprd_qty','<=',$request['pqtys1']);
            break; 
            case '>=':            
            $res = $query->where('production_products.prdnprd_qty','>=',$request['pqtys1']);
            break; 
            case '=':            
            $res = $query->where('production_products.prdnprd_qty','=',$request['pqtys1']);
            break; 
            case 'between':          
            $res = $query->whereBetween('production_products.prdnprd_qty',[$request['pqtys1'], $request['pqtys2']]);
            break;              
        }
    } 


    

    if (isset($request['prd_formula']) && $request['prd_formula'] != '') {
       $res = $query->where('production_formula.prodform_name','like','%' . $request['prd_formula'] .'%' );
   } 

       if (isset($request['prd_name']) && $request['prd_name'] != '') {
       $res = $query->where('products.prd_name','like','%' . $request['prd_name'] .'%' );
   } 



    if (isset($request['inspt_id']) && $request['inspt_id'] != '') {
       $res = $query->where('production.prdn_inspection_staff','=',$request['inspt_id']);
   } 

     if (isset($request['usr_id']) && $request['usr_id'] != '') {   
        $res = $query->where('production.prdn_added_by','=',$request['usr_id']);
    }          

 } 


    if($this->branch_id > 0)
        $query = $query->where('production_products.branch_id',$this->branch_id);


 if (isset($request['tot_amount']) && $request['tot_amount'] != '') {                      
        switch($request['tot_amount']) {            
            case '>':            
            $res = $query->where(DB::raw('(erp_production_products.prdnprd_rate*erp_production_products.prdnprd_qty)'),'>',$request['ptot1']);
            break; 
            case '<':            
            $res = $query->where(DB::raw('(erp_production_products.prdnprd_rate*erp_production_products.prdnprd_qty)'),'<',$request['ptot1']);
            break;
            case '<=':            
            $res = $query->where(DB::raw('(erp_production_products.prdnprd_rate*erp_production_products.prdnprd_qty)'),'<=',$request['ptot1']);
            break; 
            case '>=':            
            $res = $query->where(DB::raw('(erp_production_products.prdnprd_rate*erp_production_products.prdnprd_qty)'),'>=',$request['ptot1']);
            break; 
            case '=':            
            $res = $query->where(DB::raw('(erp_production_products.prdnprd_rate*erp_production_products.prdnprd_qty)'),'=',$request['ptot1']);
            break; 
            case 'between':          
            $res = $query->whereBetween(DB::raw('(erp_production_products.prdnprd_rate*erp_production_products.prdnprd_qty)'),[$request['ptot1'], $request['ptot2']]);
            break;              
        }

    }
 


  if(isset($request['filter']) && $request['filter'] == 1 && $request['datewise'] == 'false')
    { 

       $res = $query->paginate($this->per_page, array('prdnprd_id as id',
       'production.prdn_id as prodn_id','products.prd_name as product','production_formula.prodform_name as formula',
       'units.unit_code as unit','production_products.prdnprd_mfg_date as mfd',
       'production_products.prdnprd_exp_date as exp','production.prdn_date as pdate','production.prdn_cost as production_cost','production.prdn_purch_amount as purchase_amount','production_products.prdnprd_qty as qty','units.unit_code as unitcode','production_products.prdnprd_rate as rate',DB::Raw('(erp_production_products.prdnprd_rate*erp_production_products.prdnprd_qty ) as totalamount'),DB::Raw('IFNULL( erp_production_products.prdnprd_btach_code , 0 ) as batch'))); 

      $result['data'] = $res;
      $result['datewise'] = 'Product Report Detailed View';  

    }else
    {     

    $res = $query->groupby('products.prd_id')->paginate($this->per_page, array('prdnprd_id as id',
       'production.prdn_id as prodn_id','products.prd_name as product','production_formula.prodform_name as formula',
       'units.unit_code as unit','production_products.prdnprd_mfg_date as mfd',
       'production_products.prdnprd_exp_date as exp','production.prdn_date as pdate','production.prdn_cost as production_cost','production.prdn_purch_amount as purchase_amount',DB::Raw('SUM(erp_production_products.prdnprd_qty ) as totalqty'),'units.unit_code as unitcode',DB::Raw('SUM(erp_production_products.prdnprd_rate*erp_production_products.prdnprd_qty ) as totalamount'),DB::Raw('SUM(erp_production_products.prdnprd_rate ) as totalunitcost'),DB::Raw('IFNULL( erp_production_products.prdnprd_btach_code , 0 ) as batch')));
     $result['data'] = $res;
      $result['datewise'] = 'Product Report Summary';  
 //   $res = ProductionProducts::with('production')->get();
}// end datewise


if ($request['period_type'] != "") {
    $result['Date']['date1'] = date("d-F-Y", strtotime($date1));
    $result['Date']['date2'] = date("d-F-Y", strtotime($date2));
}else{
    $result['Date'] = "";
}

    return $result;
}
}