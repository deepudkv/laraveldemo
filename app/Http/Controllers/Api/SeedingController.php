<?php
namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use App\Models\Company\Acc_branch;
use App\Models\Godown\GodownStock;
use App\Models\Godown\GodownStockLog;
use App\Models\Product\Product;
use App\Models\Stocks\BranchStockLogs;
use App\Models\Stocks\BranchStocks;
use App\Models\Stocks\CompanyStocks;
use App\Models\Stocks\OpeningStockLog;
use App\Models\Stocks\StockBatches;
use App\Models\Stocks\StockDailyUpdates;
use App\Models\Stocks\StockUnitRates;

use DB;
use Illuminate\Http\Request;
use Validator;

class SeedingController extends Controller
{
    public function __construct()
    {

        $this->server_sync_time = date('ymdHis') . substr(microtime(), 2, 6);

    }

    public function issuefixOpStk()
    {

        $openstock = OpeningStockLog::where('opstklog_type', 1)->get()->toArray();
        //$openstock = OpeningStockLog::where(['opstklog_type'=>1,'opstklog_branch_stock_id'=>696])->get()->toArray();
        $j=$l=0;
        $items =$darrr=[];
        foreach ($openstock as $key => $osl) {

    
            $date = date('Y-m-d', strtotime($osl['updated_at']));


            $end_date =date('Y-m-d', strtotime('-1 day', strtotime($date)));

          //  die($date."===".$end_date);

            $return_qty = $osl['opstklog_stock_quantity_rem'];
            $branch_stock_id = $osl['opstklog_branch_stock_id'];
            $opstklog_prd_id = $osl['opstklog_prd_id'];
            $branch_id = $osl['branch_id'];

            $data = StockDailyUpdates::select('*')
            //->where('sdu_branch_stock_id', $branch_stock_id)
            ->where('branch_id', $branch_id)
            ->where('sdu_prd_id', $opstklog_prd_id)
            ->where('sdu_date', $date)
            ->first();
           
            if(empty($data))
            {
                $data = StockDailyUpdates::select('*')
                ->where('branch_id', $branch_id)
                ->where('sdu_prd_id', $opstklog_prd_id)
                ->where('sdu_date', $end_date)
                ->first();
            }

           
    
            if($data)
            {
                $data['sdu_stock_quantity'] =   $data['sdu_stock_quantity']+$return_qty;
                $data->save();

                $j++;

                $row = StockDailyUpdates::select('*')
                //->where('sdu_branch_stock_id', $branch_stock_id)
                ->where('branch_id', $branch_id)
                ->where('sdu_prd_id', $opstklog_prd_id)
                ->where('sdu_date','2019-12-31')
                ->first();
               
        
                if($row)
                {
                    $row['sdu_stock_quantity'] =   $row['sdu_stock_quantity']-$return_qty;
                    $row->save();
                    $l++;

                    $darrr[] =$branch_stock_id;
                }
              
            }

            else
            {
               $items[] =$branch_stock_id;
            }

         

          
        }
        print_r($darrr);
         print_r($items);

die("==$l====-===$j====");

        //die();
        //return $res;
    }

}
