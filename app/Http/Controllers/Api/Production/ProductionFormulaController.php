<?php

namespace App\Http\Controllers\Api\Production;

use App\Http\Controllers\Api\ApiParent;
use App\Models\Production\ProductionFormula;
use App\Models\Production\ProductionFormulaComm;
use App\Models\Production\ProductionFormulaProd;
use App\Models\Production\ProductionFormulaSub;
use App\Models\Product\ProdUnit;
use App\Models\Stocks\BranchStocks;
use App\Models\Stocks\StockBatches;
use Illuminate\Http\Request;
use Validator;

class productionformulacontroller extends ApiParent
{
    public function productionFormulaOperations(Request $request, $type)
    {

        switch ($type) {
            case 'add':
                return $this->addProductionFormula($request);
                break;
            case 'list':
                return $this->listProductionFormula($request);

            case 'formula':
                return $this->getFormulaData($request);
                break;
            case 'item':
                return $this->getProductionFormula($request);
                break;
            case 'update':
                return $this->updateProductionFormula($request);
                break;

            case 'activate':
                //  return $this->activateProductionFormula($request);
                break;

            case 'search':
                // return $this->searchProductionFormula($request);
                break;

            default:
                return response()->json(['error' => 'Invalid Endpoint', 'status' => $this->badrequest_stat]);
                break;
        }
    }

    public function addProductionFormula($request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'formula_name' => 'required|unique:production_formula,prodform_name',
                'miscExp' => 'required|numeric|min:0',
                'productOut' => 'array|min:1',
                'productOut.*.product.prd_id' => 'required',
                'productOut.*.qty' => 'required|numeric|min:0|not_in:0',
                'productOut.*.percntg' => 'required|numeric|min:0|not_in:0',
                'productIngd' => 'array|min:1',
                'productIngd.*.product.prd_id' => 'required',
                'productIngd.*.qty' => 'required|numeric|min:0|not_in:0',
                'staffs.*.staff.ledger_id' => 'required',
                'staffs.*.comm' => 'required|numeric|min:0|not_in:0',
                // "prd_unit_ids.*" => "nullable|numeric|distinct",
            ],
            [
                'formula_name.required' => 'Required',
                'formula_name.unique' => 'Already Exists',

                'miscExp.required' => 'Required',
                'miscExp.numeric' => 'Invalid Misc.Exp',
                'miscExp.min' => 'Invalid Misc.Exp',

                'productOut.min' => 'Select AtLeast One Output Product',
                'productIngd.min' => 'Select AtLeast One Ingredient Product',

                'productOut.*.product.prd_id.required' => 'Select Product',

                'productOut.*.qty.required' => 'Enter Quantity',
                'productOut.*.qty.numeric' => 'Invalid Quantity',
                'productOut.*.qty.min' => 'Invalid Quantity',
                'productOut.*.qty.not_in' => 'Invalid Quantity',

                'productOut.*.percntg.required' => 'Enter Percentage',
                'productOut.*.percntg.numeric' => 'Invalid Percentage',
                'productOut.*.percntg.min' => 'Invalid Percentage',
                'productOut.*.percntg.not_in' => 'Invalid Percentage',

                'productIngd.*.product.prd_id.required' => 'Select Product',

                'productIngd.*.qty.required' => 'Enter Quantity',
                'productIngd.*.qty.numeric' => 'Invalid Quantity',
                'productIngd.*.qty.min' => 'Invalid Quantity',
                'productIngd.*.qty.not_in' => 'Invalid Quantity',

                'staffs.*.staff.ledger_id.required' => 'Select Staff',

                'staffs.*.comm.required' => 'Enter Commission',
                'staffs.*.comm.numeric' => 'Invalid Commission',
                'staffs.*.comm.min' => 'Invalid Commission',
                'staffs.*.comm.not_in' => 'Invalid Commission',

                // 'productOut.*.prd_id' => 'Select Product',
            ]
        );
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        } else {
            $perTtl = 0;
            foreach ($request->productOut as $key => $val) {
                $perTtl += $val['percntg'];
            }
            if ($perTtl != 100) {
                $validator->errors()->add("common", 'Sum Of Percentage should be 100% ');
                return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
            }

        }
        // $validator->errors()->add('field', 'Something is wrong with this field!');
        $productionFormula = $request->all();
        $info = array();
        $info['prodform_name'] = $productionFormula['formula_name'];
        $info['prodform_ingred'] = count($productionFormula['productIngd']);
        $info['prodform_prod_count'] = count($productionFormula['productOut']);
        $info['prodform_commission'] = isset($productionFormula['isProdCommsn']) ? 1 : 0;
        $info['prodform_comm_amount'] = $productionFormula['ttlComm'];
        $info['prodform_misc'] = $productionFormula['miscExp'];
        $info['prodform_insp_laccount_no'] = (isset($productionFormula['insp_staff']['ledger_id'])) ? $productionFormula['insp_staff']['ledger_id'] : 0;
        $info['prodfrom_date'] = date('Y-m-d');
        $info['prodform_added_by'] = $this->usr_id;

        $addInfo = array();
        $addInfo['branch_id'] = $this->branch_id;
        $addInfo["server_sync_time"] = $this->server_sync_time;
        $info = array_merge($info, $addInfo);
        $data = ProductionFormula::create($info);

        $formulaId = $data->prodform_id;
        if (!$formulaId) {
            return response()->json(['message' => 'Error Adding Formula', 'status' => $this->badrequest_stat]);
        }
        $info = array();
        $info['prodformprod_prodform_id'] = $formulaId;
        foreach ($productionFormula['productOut'] as $val) {
            $info["prodformprod_prod_id"] = $val['product']['prd_id'];
            $info["prodformprod_qty"] = $val['qty'];
            $info["prodformprod_perc"] = $val['percntg'];
            $info["prodformprod_unit_id"] = $val['product']['prd_base_unit_id'];

            $info = array_merge($info, $addInfo);
            $data = ProductionFormulaProd::create($info);
        }

        $info = array();
        $info['prdn_id'] = $formulaId;
        $info['prodformsub_prodform_id'] = $formulaId;
        foreach ($productionFormula['productIngd'] as $val) {
            $info["prodformsub_prod_id"] = $val['product']['prd_id'];
            $info["prodformsub_qty"] = $val['qty'];
            $info["prodformsub_unit_id"] = $val['product']['prd_base_unit_id'];

            $info = array_merge($info, $addInfo);
            $data = ProductionFormulaSub::create($info);
        }

        if ($productionFormula['isProdCommsn']) {
            $info = array();
            $info['prodformcomm_prodform_id'] = $formulaId;
            foreach ($productionFormula['staffs'] as $val) {
                $info["prodformcomm_laccount_no"] = $val['staff']['ledger_id'];
                $info["prodformcomm_amount"] = $val['comm'];
                $info = array_merge($info, $addInfo);
                $data = ProductionFormulaComm::create($info);
            }
        }
        return response()->json(['message' => 'Saved Successfully', 'status' => $this->created_stat]);
    }

    public function updateProductionFormula($request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'formula_name' => 'required|unique:production_formula,prodform_name,' . $request->formula_id . ',prodform_id',
                'miscExp' => 'required|numeric|min:0',
                'productOut' => 'array|min:1',
                'productOut.*.product.prd_id' => 'required',
                'productOut.*.qty' => 'required|numeric|min:0|not_in:0',
                'productOut.*.percntg' => 'required|numeric|min:0|not_in:0',
                'productIngd' => 'array|min:1',
                'productIngd.*.product.prd_id' => 'required',
                'productIngd.*.qty' => 'required|numeric|min:0|not_in:0',
                'staffs.*.staff.ledger_id' => 'required',
                'staffs.*.comm' => 'required|numeric|min:0|not_in:0',
                // "prd_unit_ids.*" => "nullable|numeric|distinct",
            ],
            [
                'formula_name.required' => 'Required',
                'formula_name.unique' => 'Already Exists',

                'miscExp.required' => 'Required',
                'miscExp.numeric' => 'Invalid Misc.Exp',
                'miscExp.min' => 'Invalid Misc.Exp',

                'productOut.min' => 'Select AtLeast One Output Product',
                'productIngd.min' => 'Select AtLeast One Ingredient Product',

                'productOut.*.product.prd_id.required' => 'Select Product',

                'productOut.*.qty.required' => 'Enter Quantity',
                'productOut.*.qty.numeric' => 'Invalid Quantity',
                'productOut.*.qty.min' => 'Invalid Quantity',
                'productOut.*.qty.not_in' => 'Invalid Quantity',

                'productOut.*.percntg.required' => 'Enter Percentage',
                'productOut.*.percntg.numeric' => 'Invalid Percentage',
                'productOut.*.percntg.min' => 'Invalid Percentage',
                'productOut.*.percntg.not_in' => 'Invalid Percentage',

                'productIngd.*.product.prd_id.required' => 'Select Product',

                'productIngd.*.qty.required' => 'Enter Quantity',
                'productIngd.*.qty.numeric' => 'Invalid Quantity',
                'productIngd.*.qty.min' => 'Invalid Quantity',
                'productIngd.*.qty.not_in' => 'Invalid Quantity',

                'staffs.*.staff.ledger_id.required' => 'Select Staff',

                'staffs.*.comm.required' => 'Enter Commission',
                'staffs.*.comm.numeric' => 'Invalid Commission',
                'staffs.*.comm.min' => 'Invalid Commission',
                'staffs.*.comm.not_in' => 'Invalid Commission',

                // 'productOut.*.prd_id' => 'Select Product',
            ]
        );
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        } else {
            $perTtl = 0;
            foreach ($request->productOut as $key => $val) {
                $perTtl += $val['percntg'];
            }
            if ($perTtl != 100) {
                $validator->errors()->add("common", 'Sum Of Percentage should be 100% ');
                return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
            }
            if (!$request->formula_id || $request->formula_id == '') {
                $validator->errors()->add("common", 'Somthing went wrong ..! invalid id');
                return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
            }

        }
        // $validator->errors()->add('field', 'Something is wrong with this field!');
        $productionFormula = $request->all();
        $info = ProductionFormula::find($request->formula_id);

        $info['prodform_name'] = $productionFormula['formula_name'];
        $info['prodform_ingred'] = count($productionFormula['productIngd']);
        $info['prodform_prod_count'] = count($productionFormula['productOut']);
        $info['prodform_commission'] = isset($productionFormula['isProdCommsn']) ? 1 : 0;
        $info['prodform_comm_amount'] = $productionFormula['ttlComm'];
        $info['prodform_misc'] = $productionFormula['miscExp'];
        $info['prodform_insp_laccount_no'] = (isset($productionFormula['insp_staff']['ledger_id'])) ? $productionFormula['insp_staff']['ledger_id'] : 0;
        $info['prodfrom_date'] = date('Y-m-d');
        $info['server_sync_time'] = $request->server_sync_time;
        $info->save();

        $formulaId = $request->formula_id;

        $addInfo = array();
        $addInfo['branch_id'] = $request->branch_id;
        $addInfo["server_sync_time"] = $request->server_sync_time;

        ProductionFormulaProd::where('prodformprod_prodform_id', $formulaId)->delete();
        $info = array();
        $info['prodformprod_prodform_id'] = $formulaId;
        foreach ($productionFormula['productOut'] as $val) {
            $info["prodformprod_prod_id"] = $val['product']['prd_id'];
            $info["prodformprod_qty"] = $val['qty'];
            $info["prodformprod_perc"] = $val['percntg'];
            $info["prodformprod_unit_id"] = $val['product']['prd_base_unit_id'];

            $info = array_merge($info, $addInfo);
            $data = ProductionFormulaProd::create($info);
        }

        ProductionFormulaSub::where('prodformsub_prodform_id', $formulaId)->delete();
        $info = array();
        $info['prodformsub_prodform_id'] = $formulaId;
        foreach ($productionFormula['productIngd'] as $val) {
            $info["prodformsub_prod_id"] = $val['product']['prd_id'];
            $info["prodformsub_qty"] = $val['qty'];
            $info["prodformsub_unit_id"] = $val['product']['prd_base_unit_id'];

            $info = array_merge($info, $addInfo);
            $data = ProductionFormulaSub::create($info);
        }

        ProductionFormulaComm::where('prodformcomm_prodform_id', $formulaId)->delete();
        if ($productionFormula['isProdCommsn']) {
            $info = array();
            $info['prodformcomm_prodform_id'] = $formulaId;
            foreach ($productionFormula['staffs'] as $val) {
                $info["prodformcomm_laccount_no"] = $val['staff']['ledger_id'];
                $info["prodformcomm_amount"] = $val['comm'];
                $info = array_merge($info, $addInfo);
                $data = ProductionFormulaComm::create($info);
            }
        }
        return response()->json(['message' => 'Updated Successfully', 'status' => $this->created_stat]);
    }

    public function listProductionFormula($request)
    {
        return parent::displayData(ProductionFormula::where('branch_id', $this->branch_id)->paginate(100));
    }

    public function getProductionFormula($request)
    {
        $rltn = [
            'commision' => function ($qr) {
                $qr->select('*');
            },
            'prod' => function ($qr) {
                $qr->select('*');
            },
            'sub' => function ($qr) {
                $qr->select('*');
            },
            'staff' => function ($qr) {
                $qr->select('*');
            },
        ];
        $out = ProductionFormula::with($rltn)->where('prodform_id', $request->prodform_id)
            ->where('branch_id', $this->branch_id)->first();
        foreach ($out['prod'] as $k => $v) {

            $out['prod'][$k]['units'] = ProdUnit::select('prod_units.produnit_unit_id as sur_unit_id', 'units.unit_name', 'units.unit_base_qty')->join('units', 'units.unit_id', 'prod_units.produnit_unit_id')->where('prod_units.produnit_prod_id', $v['bs_prd_id'])->get();
            $out['prod'][$k]['batches'] = StockBatches::select('*')->where('sb_branch_stock_id', $v['branch_stock_id'])->get();

        }
        foreach ($out['sub'] as $k => $v) {
            $out['sub'][$k]['units'] = ProdUnit::select('prod_units.produnit_unit_id as sur_unit_id', 'units.unit_name', 'units.unit_base_qty')->join('units', 'units.unit_id', 'prod_units.produnit_unit_id')->where('prod_units.produnit_prod_id', $v['bs_prd_id'])->get();
        }

        return parent::displayData($out);

    }

    public function getFormulaData($request)
    {
        $rltn = [
            'commision' => function ($qr) {
                $qr->select('*');
            },
            'prod' => function ($qr) {
                $qr->select('*');
            },
            'sub' => function ($qr) {
                $qr->select('*');
            },
            'staff' => function ($qr) {
                $qr->select('*');
            },
        ];
        $out = ProductionFormula::with($rltn)->where('prodform_id', $request->prodform_id)
            ->where('branch_id', $this->branch_id)->first();
         $out['active_stock'] =true;
        foreach ($out['prod'] as $k => $v) {

            $out['prod'][$k]['units'] = ProdUnit::select('prod_units.produnit_unit_id as sur_unit_id', 'units.unit_name', 'units.unit_base_qty')->join('units', 'units.unit_id', 'prod_units.produnit_unit_id')->where('prod_units.produnit_prod_id', $v['bs_prd_id'])->get();
            $stock = BranchStocks::where(['bs_prd_id' => $v['prd_id']
                , 'bs_branch_id' => $this->branch_id])->first();
            $out['prod'][$k]['branch_stock_id'] = $stock['branch_stock_id'];
            $out['prod'][$k]['bs_stock_id'] = $stock['bs_stock_id'];
            $out['prod'][$k]['bs_prd_id'] = $stock['bs_prd_id'];
            $out['prod'][$k]['bs_stock_quantity'] = $stock['bs_stock_quantity'];
            $out['prod'][$k]['bs_stock_quantity_shop'] = $stock['bs_stock_quantity_shop'];
            $out['prod'][$k]['bs_stock_quantity_gd'] = $stock['bs_stock_quantity_gd'];
            $out['prod'][$k]['bs_prate'] = $stock['bs_prate'];
            $out['prod'][$k]['bs_avg_prate'] = $stock['bs_avg_prate'];
            $out['prod'][$k]['bs_srate'] = $stock['bs_srate'];
            $out['prod'][$k]['bs_branch_id'] = $stock['bs_branch_id'];

            //  print_r($stock);
            // die($stock['branch_stock_id']."==");
            if(!$stock['branch_stock_id'])
            {
                $out['active_stock'] =  false;
            }
            $out['prod'][$k]['batches'] = StockBatches::where('sb_branch_stock_id', $stock['branch_stock_id'])->get();

        }
        foreach ($out['sub'] as $k => $v) {
            $out['sub'][$k]['units'] = ProdUnit::select('prod_units.produnit_unit_id as sur_unit_id', 'units.unit_name', 'units.unit_base_qty')->join('units', 'units.unit_id', 'prod_units.produnit_unit_id')->where('prod_units.produnit_prod_id', $v['bs_prd_id'])->get();
            $stock = BranchStocks::where(['bs_prd_id' => $v['prd_id']
                , 'bs_branch_id' => $this->branch_id])->first();
            $out['sub'][$k]['branch_stock_id'] = $stock['branch_stock_id'];
            $out['sub'][$k]['bs_stock_id'] = $stock['bs_stock_id'];
            $out['sub'][$k]['bs_prd_id'] = $stock['bs_prd_id'];
            $out['sub'][$k]['bs_stock_quantity'] = $stock['bs_stock_quantity'];
            $out['sub'][$k]['bs_stock_quantity_shop'] = $stock['bs_stock_quantity_shop'];
            $out['sub'][$k]['bs_stock_quantity_gd'] = $stock['bs_stock_quantity_gd'];
            $out['sub'][$k]['bs_prate'] = $stock['bs_prate'];
            $out['sub'][$k]['bs_avg_prate'] = $stock['bs_avg_prate'];
            $out['sub'][$k]['bs_srate'] = $stock['bs_srate'];
            $out['sub'][$k]['bs_branch_id'] = $stock['bs_branch_id'];

            if(!$stock['branch_stock_id'])
            {
                $out['active_stock'] =  false;
            }
        }
    
        return parent::displayData($out);

    }

}
