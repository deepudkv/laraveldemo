<?php

namespace App\Http\Controllers\Api\Production;

use App\Http\Controllers\Api\ApiParent;
use App\Models\Production\Production;
use App\Models\Production\ProductionCommission;
use App\Models\Production\ProductionFormula;
use App\Models\Production\ProductionFormulaComm;
use App\Models\Production\ProductionFormulaProd;
use App\Models\Production\ProductionFormulaSub;
use App\Models\Production\ProductionProducts;
use App\Models\Production\ProductionSub;
use App\Models\Stocks\StockBatches;
use Illuminate\Http\Request;
use Validator;

class ProductionController extends ApiParent
{
    public function productionOperations(Request $request, $type)
    {

        switch ($type) {
            case 'add':
                return $this->addProduction($request);
                break;

            case 'void':
                return $this->voidProduction($request);
                break;
            case 'list':
                return $this->listProductions($request);
                break;
            case 'item':
                return $this->getProduction($request);

            case 'preview':
                return $this->productionPreview($request);
                break;
            case 'update':
                return $this->updateProduction($request);
                break;

            case 'search':
                return $this->searchProduction($request);
                break;

            default:
                return response()->json(['error' => 'Invalid Endpoint', 'status' => $this->badrequest_stat]);
                break;
        }
    }

    public function geNextProdctnId()
    {
        $prod_id = Production::get()->count() + 1;
        return parent::displayData(['prod_id' => $prod_id]);
    }
    public function productionPreview($request)
    {
        if ($request->prdn_id) {

            return parent::displayData($this->getProductionPreview($request->prdn_id));

        } else {
            return parent::displayError('Invalid Production Id');
        }

    }

    public function getProductionPreview($prdn_id)
    {

    
        $rltn = [
            'commision' => function ($qr) {
                $qr->select('*');
            },
            'prod' => function ($qr) {
                $qr->select('*');
            },
            'sub' => function ($qr) {
                $qr->select('*');
            },
        ];
        $out = Production::with($rltn)->where('prdn_id', $prdn_id)->where('prdn_flag',1)
        ->where('branch_id',$this->branch_id)->first();

        return $out;
        // foreach ($out['prod'] as $k => $v) {

        //     $out['prod'][$k]['units'] = ProdUnit::select('prod_units.produnit_unit_id as sur_unit_id', 'units.unit_name','units.unit_base_qty')->join('units', 'units.unit_id', 'prod_units.produnit_unit_id')->where('prod_units.produnit_prod_id', $v['bs_prd_id'])->get();
        //     $out['prod'][$k]['batches'] = StockBatches::select('*')->where('sb_branch_stock_id', $v['branch_stock_id'])->get();

        // }
        // foreach ($out['sub'] as $k => $v) {
        //     $out['sub'][$k]['units'] = ProdUnit::select('prod_units.produnit_unit_id as sur_unit_id', 'units.unit_name','units.unit_base_qty')->join('units', 'units.unit_id', 'prod_units.produnit_unit_id')->where('prod_units.produnit_prod_id', $v['bs_prd_id'])->get();
        // }
    }

    public function validateProduction($request)
    {

        $validator = Validator::make(
            $request->all(),
            [
                'formula_name' => 'required|unique:production_formula,prodform_name',
                'miscExp' => 'required|numeric|min:0',
                'productOut' => 'array|min:1',
                'productOut.*.product.prd_id' => 'required',
                'productOut.*.qty' => 'required|numeric|min:0|not_in:0',
                'productOut.*.percntg' => 'required|numeric|min:0|not_in:0',
                'productIngd' => 'array|min:1',
                'productIngd.*.product.prd_id' => 'required',
                'productIngd.*.qty' => 'required|numeric|min:0|not_in:0',
                'staffs.*.staff.ledger_id' => 'required',
                'staffs.*.comm' => 'required|numeric|min:0|not_in:0',
                'prd_unit_ids.*" => "nullable|numeric|distinct',
            ],
            [
                'formula_name.required' => 'Required',
                'formula_name.unique' => 'Already Exists',

                'miscExp.required' => 'Required',
                'miscExp.numeric' => 'Invalid Misc.Exp',
                'miscExp.min' => 'Invalid Misc.Exp',

                'productOut.min' => 'Select AtLeast One Output Product',
                'productIngd.min' => 'Select AtLeast One Ingredient Product',

                'productOut.*.product.prd_id.required' => 'Select Product',

                'productOut.*.qty.required' => 'Enter Quantity',
                'productOut.*.qty.numeric' => 'Invalid Quantity',
                'productOut.*.qty.min' => 'Invalid Quantity',
                'productOut.*.qty.not_in' => 'Invalid Quantity',

                'productOut.*.percntg.required' => 'Enter Percentage',
                'productOut.*.percntg.numeric' => 'Invalid Percentage',
                'productOut.*.percntg.min' => 'Invalid Percentage',
                'productOut.*.percntg.not_in' => 'Invalid Percentage',

                'productIngd.*.product.prd_id.required' => 'Select Product',

                'productIngd.*.qty.required' => 'Enter Quantity',
                'productIngd.*.qty.numeric' => 'Invalid Quantity',
                'productIngd.*.qty.min' => 'Invalid Quantity',
                'productIngd.*.qty.not_in' => 'Invalid Quantity',

                'staffs.*.staff.ledger_id.required' => 'Select Staff',

                'staffs.*.comm.required' => 'Enter Commission',
                'staffs.*.comm.numeric' => 'Invalid Commission',
                'staffs.*.comm.min' => 'Invalid Commission',
                'staffs.*.comm.not_in' => 'Invalid Commission',

            ]
        );
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        } else {
            $perTtl = 0;
            foreach ($request->productOut as $key => $val) {
                $perTtl += $val['percntg'];
            }
            if ($perTtl != 100) {
                $validator->errors()->add("common", 'Sum Of Percentage should be 100% ');
                return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
            }

        }
        $validator->errors()->add('field', 'Something is wrong with this field!');
    }

    public function addProduction($request)
    {

        $production = $request->all();

        //   print_r( $production['insp_staff']);die($production['insp_staff']['ledger_id']);
        $production['prdn_date'] = date('Y-m-d', strtotime($production['prdn_date']));

        $prdctn['prdn_prodform_id'] = $production['formula_id'];
        $prdctn['prdn_purch_amount'] = $production['ttlPurchase'];
        $prdctn['prdn_misc_exp'] = $production['miscExp'];
        $prdctn['prdn_comm_amount'] = $production['ttlComm'];
        $prdctn['prdn_misc_exp'] = $production['miscExp'];
        $prdctn['prdn_date'] = $production['prdn_date'];
        $prdctn['prdn_added_by'] = $this->usr_id;
        $prdctn['prdn_inspection_staff'] = isset($production['insp_staff']['ledger_id']) ? $production['insp_staff']['ledger_id'] : 0;
        $prdctn['prdn_cost'] = $production['ttlPurchase'] + $production['miscExp'] + $production['ttlComm'];

        $prdctn['prdn_commission'] = $production['isProdCommsn'];
        $prdctn['prdn_flag'] = 1;
        $prdctn['branch_id'] = $this->branch_id;
        $prdctn['server_sync_time'] = $this->server_sync_time;
        $last_insert = Production::create($prdctn);

        $pprd = array();
        $pprd['prdnprd_prdn_id'] = $last_insert['prdn_id'];
        $pprd['prdnprd_prodform_id'] = $last_insert['prdn_prodform_id'];
        $pprd['prdnprd_date'] = $production['prdn_date'];
        $pprd['prdnprd_added_by'] = $this->usr_id;
        $pprd["prdnprd_flag"] = 1;
        $pprd['branch_id'] = $this->branch_id;
        $pprd['server_sync_time'] = $this->server_sync_time;
        foreach ($production['productOut'] as $val) {
            $pprd["prdnprd_prd_id"] = $val['product']['prd_id'];
            $pprd["prdnprd_stock_id"] = $val['product']['bs_stock_id'];
            $pprd["prdnprd_branch_stock_id"] = $val['product']['branch_stock_id'];
            $pprd["prdnprd_qty"] = $val['qty_received'];
            $pprd["prdnprd_required_qty"] = $val['qty_expected'];
            $pprd["prdnprd_unit_id"] = $val['product']['prd_base_unit_id'];
            $pprd["prdnprd_rate_expected"] = $val['rate_expected'];
            $pprd["prdnprd_rate"] = $val['net_rate'];

            $pprd["prdnprd_btach_code"] = $val['batch_code'];            
            $pprd["prdnprd_exp_date"] = date('Y-m-d',strtotime($val['exp_date']));
            $pprd["prdnprd_mfg_date"] = date('Y-m-d',strtotime($val['manf_date']));
            $pprd["prdnprd_gd_id"] = $val['gd_id'];

            $data = ProductionProducts::create($pprd);

            $pprd['gd_id'] = $val['gd_id'];
            $pprd['manufacture_date'] = $val['manf_date'];
            $pprd['expiry_date'] = $val['exp_date'];
            $pprd['batch_code'] = $val['batch_code'];
            $this->addToStock($pprd, $production['prdn_date']);
        }
       
        $prding["prdnsub_prd_id"] = $last_insert['prdn_id'];
        $prding["prdnsub_prodform_id"] = $last_insert['prdn_prodform_id'];

        $prding["prdnsub_date"] = $production['prdn_date'];
        $prding["prdnsub_added_by"] = $this->usr_id;
        $prding["branch_id"] = $this->branch_id;
        $prding["prdnsub_flag"] = 1;
        $prding['server_sync_time'] = $this->server_sync_time;
        foreach ($production['productIngd'] as $val) {

            $prding["prdnsub_prdn_id"] = $last_insert['prdn_id'];
            $prding["prdnsub_prodformsub_id"] = $this->findProductnFrmSubId($last_insert['prdn_prodform_id'], $val['product']['prd_id']);
            $prding["prdnsub_prd_id"] = $val['product']['prd_id'];
            $prding["prdnsub_stock_id"] = $val['product']['bs_stock_id'];
            $prding["prdnsub_branch_stock_id"] = $val['product']['branch_stock_id'];
            $prding["prdnsub_qty"] = $val['qty'];
            $prding["prdnsub_unit_id"] = $val['product']['prd_base_unit_id'];
            $prding["prdnsub_rate"] = $val['product']['bs_prate'];

            $data = ProductionSub::create($prding);
            $this->remooveFromStock($prding, $production['prdn_date']);
        }
        $PrdctnComm['prdncomm_prodform_id'] = $last_insert['prdn_prodform_id'];
        $PrdctnComm['prdncomm_date'] = $production['prdn_date'];
        $PrdctnComm['branch_id'] = $this->branch_id;
        $PrdctnComm['prdncomm_flag'] = 1;
        $PrdctnComm['prdncomm_prdn_id'] = $last_insert['prdn_id'];
        $PrdctnComm['server_sync_time'] = $this->server_sync_time;
        foreach ($production['staffs'] as $staff) {
            $PrdctnComm['prdncomm_in'] = $staff['comm'];
            $PrdctnComm['prdncomm_laccount_no'] = $staff['staff']['ledger_id'];
            ProductionCommission::create($PrdctnComm);

        }

        return response()->json(['preview' => $last_insert, 'message' => 'Saved Successfully', 'status' => $this->created_stat]);
    }
    public function addToStock($purch, $cal_date)
    {

       
        $stockArray = [
            'prd_id' => $purch['prdnprd_prd_id'],
            'stock_id' => $purch['prdnprd_stock_id'],
            'opening_quantity' => $purch['prdnprd_qty'],
            'gd_id' => $purch['gd_id'],
            'purchase_rate' => $purch['prdnprd_rate'],
            'purchase_rate_unit' => 1,
            'purchase_rate_qty' => 1,
            'prd_base_unit_id' => $purch['prdnprd_unit_id'],
            'if_purchase' => 1,
            'manufacture_date' => $purch['manufacture_date'],
            'expiry_date' => $purch['expiry_date'],
            'batch_code' => $purch['batch_code'],
            'branch_id' => $this->branch_id,
            'usr_id' => $this->usr_id,
            'cal_date' => $cal_date,

        ];

        return app('App\Http\Controllers\Api\Stock\OpeningStockController')->openStockStart($stockArray);
    }

    public function remooveFromStock($purch, $cal_date)
    {

        $stockArray = [
            'prd_id' => $purch['prdnsub_prd_id'],
            'stock_id' => $purch['prdnsub_stock_id'],
            'qty' => $purch['prdnsub_qty'],
            'rate' => $purch['prdnsub_rate'],
            'batch_id' => 0,
            'gd_id' => 0,
            'branch_id' => $this->branch_id,
            'usr_id' => $this->usr_id,
            'if_purchase' => 1,
            'cal_date' => $cal_date,
        ];

        return app('App\Http\Controllers\Api\Stock\OpeningStockController')->remooveFromStock($stockArray);

    }

    public function findProductnFrmSubId($prdn_prodform_id, $prd_id)
    {
        $whr = ['prodformsub_prodform_id' => $prdn_prodform_id, 'prodformsub_prod_id' => $prd_id];
        $out = ProductionFormulaSub::where($whr)->pluck('prodformsub_id')->toArray();

        return isset($out[0]) ? $out[0] : 0;
    }

    public function updateProduction($request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'formula_name' => 'required|unique:production_formula,prodform_name,' . $request->formula_id . ',prodform_id',
                'miscExp' => 'required|numeric|min:0',
                'productOut' => 'array|min:1',
                'productOut.*.product.prd_id' => 'required',
                'productOut.*.qty' => 'required|numeric|min:0|not_in:0',
                'productOut.*.percntg' => 'required|numeric|min:0|not_in:0',
                'productIngd' => 'array|min:1',
                'productIngd.*.product.prd_id' => 'required',
                'productIngd.*.qty' => 'required|numeric|min:0|not_in:0',
                'staffs.*.staff.ledger_id' => 'required',
                'staffs.*.comm' => 'required|numeric|min:0|not_in:0',
                // "prd_unit_ids.*" => "nullable|numeric|distinct",
            ],
            [
                'formula_name.required' => 'Required',
                'formula_name.unique' => 'Already Exists',

                'miscExp.required' => 'Required',
                'miscExp.numeric' => 'Invalid Misc.Exp',
                'miscExp.min' => 'Invalid Misc.Exp',

                'productOut.min' => 'Select AtLeast One Output Product',
                'productIngd.min' => 'Select AtLeast One Ingredient Product',

                'productOut.*.product.prd_id.required' => 'Select Product',

                'productOut.*.qty.required' => 'Enter Quantity',
                'productOut.*.qty.numeric' => 'Invalid Quantity',
                'productOut.*.qty.min' => 'Invalid Quantity',
                'productOut.*.qty.not_in' => 'Invalid Quantity',

                'productOut.*.percntg.required' => 'Enter Percentage',
                'productOut.*.percntg.numeric' => 'Invalid Percentage',
                'productOut.*.percntg.min' => 'Invalid Percentage',
                'productOut.*.percntg.not_in' => 'Invalid Percentage',

                'productIngd.*.product.prd_id.required' => 'Select Product',

                'productIngd.*.qty.required' => 'Enter Quantity',
                'productIngd.*.qty.numeric' => 'Invalid Quantity',
                'productIngd.*.qty.min' => 'Invalid Quantity',
                'productIngd.*.qty.not_in' => 'Invalid Quantity',

                'staffs.*.staff.ledger_id.required' => 'Select Staff',

                'staffs.*.comm.required' => 'Enter Commission',
                'staffs.*.comm.numeric' => 'Invalid Commission',
                'staffs.*.comm.min' => 'Invalid Commission',
                'staffs.*.comm.not_in' => 'Invalid Commission',

                // 'productOut.*.prd_id' => 'Select Product',
            ]
        );
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        } else {
            $perTtl = 0;
            foreach ($request->productOut as $key => $val) {
                $perTtl += $val['percntg'];
            }
            if ($perTtl != 100) {
                $validator->errors()->add("common", 'Sum Of Percentage should be 100% ');
                return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
            }
            if (!$request->formula_id || $request->formula_id == '') {
                $validator->errors()->add("common", 'Somthing went wrong ..! invalid id');
                return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
            }

        }
        // $validator->errors()->add('field', 'Something is wrong with this field!');
        $productionFormula = $request->all();
        $info = ProductionFormula::find($request->formula_id);

        $info['prodform_name'] = $productionFormula['formula_name'];
        $info['prodform_ingred'] = count($productionFormula['productIngd']);
        $info['prodform_prod_count'] = count($productionFormula['productOut']);
        $info['prodform_commission'] = isset($productionFormula['isProdCommsn']) ? 1 : 0;
        $info['prodform_comm_amount'] = $productionFormula['ttlComm'];
        $info['prodform_misc'] = $productionFormula['miscExp'];
        $info['prodform_insp_laccount_no'] = (isset($productionFormula['insp_staff']['ledger_id'])) ? $productionFormula['insp_staff']['ledger_id'] : 0;
        $info['prodfrom_date'] = date('Y-m-d');
        $info['server_sync_time'] = $this->server_sync_time;

        $info->save();

        $formulaId = $request->formula_id;

        $addInfo = array();
        $addInfo['branch_id'] = $request->branch_id;
        $addInfo["server_sync_time"] = $request->server_sync_time;
       
        ProductionFormulaProd::where('prodformprod_prodform_id', $formulaId)->delete();
        $info = array();
        $info['prodformprod_prodform_id'] = $formulaId;
        foreach ($productionFormula['productOut'] as $val) {
            $info["prodformprod_prod_id"] = $val['product']['prd_id'];
            $info["prodformprod_qty"] = $val['qty'];
            $info["prodformprod_perc"] = $val['percntg'];
            $info["prodformprod_unit_id"] = $val['product']['prd_base_unit_id'];

            $info = array_merge($info, $addInfo);
            $data = ProductionFormulaProd::create($info);
        }

        ProductionFormulaSub::where('prodformsub_prodform_id', $formulaId)->delete();
        $info = array();
        $info['prodformsub_prodform_id'] = $formulaId;
        foreach ($productionFormula['productIngd'] as $val) {
            $info["prodformsub_prod_id"] = $val['product']['prd_id'];
            $info["prodformsub_qty"] = $val['qty'];
            $info["prodformsub_unit_id"] = $val['product']['prd_base_unit_id'];
            $info = array_merge($info, $addInfo);
            $data = ProductionFormulaSub::create($info);
        }

        ProductionFormulaComm::where('prodformcomm_prodform_id', $formulaId)->delete();
        if ($productionFormula['isProdCommsn']) {
            $info = array();
            $info['prodformcomm_prodform_id'] = $formulaId;
            foreach ($productionFormula['staffs'] as $val) {
                $info["prodformcomm_laccount_no"] = $val['staff']['ledger_id'];
                $info["prodformcomm_amount"] = $val['comm'];
                if ($info["prodformcomm_laccount_no"] && $info["prodformcomm_amount"]) {
                    $info = array_merge($info, $addInfo);
                    $data = ProductionFormulaComm::create($info);
                }

            }
        }
        return response()->json(['message' => 'Updated Successfully', 'status' => $this->created_stat]);
    }

    public function listProductions($request)
    {

        $rltn = [
            'commision' => function ($qr) {
                $qr->select('*');
            },
            'prod' => function ($qr) {
                $qr->select('*');
            },
            'sub' => function ($qr) {
                $qr->select('*');
            },
            'formula' => function ($qr) {
                $qr->select('*');
            },
        ];
        $out = Production::with($rltn)->where(['prdn_flag'=>1,'branch_id'=>$this->branch_id])->orderBy('prdn_id', 'desc')->paginate($this->per_page);

        return parent::displayData($out);
    }

    public function searchProduction()
    {

        $rltn = [
            'commision' => function ($qr) {
                $qr->select('*');
            },
            'prod' => function ($qr) {
                $qr->select('*');
            },
            'sub' => function ($qr) {
                $qr->select('*');
            },
            'staff' => function ($qr) {
                $qr->select('*');
            },
        ];

        $prodform_name = isset($request['keyword']) ? $request['keyword'] : 0;
        if ($prodform_name) {
            $van = ProductionFormula::with($rltn)->where('prodform_name', 'like', $prodform_name . '%')->limit(10)->get();
            return response()->json(['data' => $van, 'status' => $this->success_stat]);
        } else {
            $data = ProductionFormula::with($rltn)->take(20)->get();
            return response()->json(['data' => $data, 'status' => $this->success_stat]);
        }
        return parent::displayData(Van::where('van_name', 'like', $request['van_name'] . '%')->limit(10)->get());
    }

    public function getProductionDeatils($request)
    {

        $rltn = [
            'commision' => function ($qr) {
                $qr->select('*');
            },
            'prod' => function ($qr) {
                $qr->select('*');
            },
            'sub' => function ($qr) {
                $qr->select('*');
            },
            'staff' => function ($qr) {
                $qr->select('*');
            },
        ];
        $out = ProductionFormula::with($rltn)->where('prodform_id', $request->prodform_id)->first();
        print_r($out);die();
        foreach ($out['prod'] as $k => $v) {
            print_r($v);die();
            $out['prod'][$k]['units'] = ProdUnit::select('prod_units.produnit_unit_id as sur_unit_id', 'units.unit_name')->join('units', 'units.unit_id', 'prod_units.produnit_unit_id')->where('prod_units.produnit_prod_id', $v['bs_prd_id'])->get();

        }
        foreach ($out['sub'] as $k => $v) {
            $out['sub'][$k]['units'] = ProdUnit::select('prod_units.produnit_unit_id as sur_unit_id', 'units.unit_name')->join('units', 'units.unit_id', 'prod_units.produnit_unit_id')->where('prod_units.produnit_prod_id', $v['bs_prd_id'])->get();
        }
        //print_r($out);die();
        return parent::displayData($request);

    }

    public function voidProduction($request)
    {
        if ($request->prdn_id) {
            $validator = Validator::make(
                $request->all(),
                ['prdn_id' => 'required|exists:production,prdn_id'],
                ['prdn_id.required' => 'Required']
            );
            if ($validator->fails()) {
                return parent::displayError($validator->messages());
            }

            $this->revertProduction($request->prdn_id);
            return parent::displayMessage('Reverted  Production Successfully');
        } else {
            return parent::displayError('Invalid Production Id');
        }
    }


    public function revertProduction($prdn_id)
    {
        Production::where('prdn_id', $prdn_id)->update(['prdn_flag' => 0]);
        ProductionSub::where('prdnsub_prdn_id', $prdn_id)->update(['prdnsub_flag' => 0]);
        ProductionProducts::where('prdnprd_prdn_id', $prdn_id)->update(['prdnprd_flag' => 0]);
        ProductionCommission::where('prdncomm_prdn_id', $prdn_id)->update(['prdncomm_flag' => 0]);


        $productionOut = ProductionProducts::where('prdnprd_prdn_id', $prdn_id)->get()->toArray();
        $productionIng = ProductionSub::where('prdnsub_prdn_id', $prdn_id)->get()->toArray();
    
       
        foreach ($productionIng as $val) {
            $this->addToStocksub($val);
        }

        foreach ($productionOut as $val) {
            $this->voidStock($val);
        }

    }

    
    public function voidStock($purch)
    {
        $stockArray = [
            'prd_id' => $purch['prdnprd_prd_id'],
            'stock_id' => $purch['prdnprd_stock_id'],
            'qty' => $purch['prdnprd_qty'],
            'rate' => $purch['prdnprd_rate'],
            'batch_id' => $this->findBatchId($purch['prdnprd_btach_code']),
            'gd_id' => $purch['prdnprd_gd_id'],
            'branch_id' => $this->branch_id,
            'usr_id' => $this->usr_id,
            'if_purchase' => 1,
            'cal_date' => $purch['prdnprd_date'],
        ];

        return app('App\Http\Controllers\Api\Stock\OpeningStockController')->remooveFromStock($stockArray);

    }


    public function findBatchId($btach_code)
    {
        $out = StockBatches::where('sb_batch_code',$btach_code)->pluck('sb_id')->toArray();
        return isset($out[0]) ? $out[0] : 0;
    }

    public function addToStocksub($purch)
    
    {


        $stockArray = [
            'prd_id' => $purch['prdnsub_prd_id'],
            'stock_id' => $purch['prdnsub_stock_id'],
            'opening_quantity' => $purch['prdnsub_qty'],
            'gd_id' => 0,
            'purchase_rate' => $purch['prdnsub_rate'],
            'purchase_rate_unit' => $purch['prdnsub_unit_id'],
            'purchase_rate_qty' => 1,
            'prd_base_unit_id' => $purch['prdnsub_unit_id'],
            'if_purchase' => 1,
            'manufacture_date' => 0,
            'expiry_date' => 0,
            'batch_code' => 0,
            'branch_id' => $this->branch_id,
            'usr_id' => $this->usr_id,
            'cal_date' => $purch['prdnsub_date'],

        ];

        return app('App\Http\Controllers\Api\Stock\OpeningStockController')->openStockStart($stockArray);
    }
}
