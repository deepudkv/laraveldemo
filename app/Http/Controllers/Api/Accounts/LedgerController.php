<?php

namespace App\Http\Controllers\Api\Accounts;

use App\Common\Corelib;
use App\Http\Controllers\Api\ApiParent;
use App\Models\Accounts\Acc_customer;
use App\Models\Accounts\Acc_group;
use App\Models\Accounts\Acc_ledger;
use App\Models\Accounts\Acc_opening_balance;
use App\Models\Accounts\Acc_voucher;
use App\Models\Company\Acc_branch;
use App\Models\Purchase\Purchase;
use App\Models\Purchase\Supplier;
use App\Models\Sales\SalesMaster;
use App\Models\Settings\TaxLedgers;
use DB;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use validator;

class LedgerController extends ApiParent
{

    /**
     * @api {post} http://127.0.0.1:8000/api/add_ledger  Add  Ledger
     * @apiName addLedger
     * @apiGroup Accounts
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {String} ledger_name Ledger Name (Required)
     * @apiParam {String} ledger_alias Ledger Alias
     * @apiParam {String} ledger_code Ledger Code
     * @apiParam {String} ledger_accgrp_id Ledger Account Group (Required)
     * @apiParam {String} ledger_branch_id Ledger Branch ID (Required)
     * @apiParam {String} ledger_address Ledger Address
     * @apiParam {String} ledger_email Ledger Email
     * @apiParam {String} ledger_mobile Ledger Mobile
     * @apiParam {String} ledger_due Ledger Due
     * @apiParam {String} ledger_tin Ledger Tin Number
     * @apiParam {String} ledger_flags Ledger Flags
     * @apiParam {String} ledger_notes Ledger Notes
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} message Success Message
     *
     * @apiError {String} message Error Message
     * @apiError {String} status Status Code
     *
     */

    public function addLedger(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            [
                'ledger_name' => 'required|unique:acc_ledgers',
                'ledger_accgrp_id' => 'required',
                'ledger_branch_id' => 'required',

            ],
            [
                'ledger_name.required' => 'Required',
                'ledger_accgrp_id.required' => 'Required',
                'ledger_branch_id.required' => 'Required',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }

        $request->request->add(['ledger_acc_type' => 100]);
        $data = $request->all();

        if ($request->op_bal_type) {
            $data['opening_balance'] = 0 - $request->op_bal;
        } else {
            $data['opening_balance'] = 0 + $request->op_bal;
        }
        unset($data['op_bal']);
        unset($data['op_bal_type']);

        if ($request->ledger_accgrp_id == 3) {
            $data['ledger_acc_type'] = 0;} elseif ($request->ledger_accgrp_id == 4) {
            $data['ledger_acc_type'] = 1;} else {
            $data['ledger_acc_type'] = 100;
        }
        $data['ledger_flags'] = 1;
        $data['branch_id'] = $data['ledger_branch_id'];
        $res = Acc_ledger::create($data);
        if ($request->op_bal != 0) {
            $where = array();
            $where[] = ['branch_id', '=', $this->branch_id];
            $branch = Acc_branch::where($where)->first();
            $info = array();
            $max = Acc_voucher::max('vch_no');
            $info['vch_no'] = (empty($max)) ? 1 : $max + 1;
            $info['vch_ledger_from'] = $res->ledger_id;
            $info['vch_ledger_to'] = $res->ledger_id;
            $info['vch_date'] = date('Y-m-d', (strtotime('-1 day', strtotime($branch['branch_open_date']))));
            $info['vch_in'] = (!$request->op_bal_type) ? abs($request->op_bal) : 0;
            $info['vch_out'] = ($request->op_bal_type) ? abs($request->op_bal) : 0;
            $info['vch_vchtype_id'] = 14;
            $info['vch_notes'] = 'Starting balance';
            $info['vch_added_by'] = $this->usr_id;
            $info['vch_id2'] = 0;
            $info['vch_from_group'] = $request->ledger_accgrp_id;
            $info['vch_flags'] = 1;
            $info['ref_no'] = 0;
            $info['branch_id'] = $this->branch_id;
            $info['is_web'] = 1;
            $info['godown_id'] = 0;
            $info['van_id'] = 0;
            $info['branch_ref_no'] = 0;
            $info['sub_ref_no'] = 0;
            $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            $voucher = Acc_voucher::updateOrCreate(
                [
                    'vch_ledger_from' => $res->ledger_id,
                    'vch_ledger_to' => $res->ledger_id,
                    'vch_vchtype_id' => 14,
                    'vch_date' => $info['vch_date'],
                    'branch_id' => $this->branch_id,
                    'van_id' => 0,
                ],
                $info
            );

            Acc_voucher::where(['vch_in' => 0, 'vch_out' => 0])->delete();
        }

        if ($res) {
            return response()->json(['message' => 'saved_successfully', 'status' => $this->created_stat]);
        } else {
            return response()->json(['message' => 'Ledger not  created', 'status' => $this->badrequest_stat]);
        }

    }

    /**
     * @api {post} http://127.0.0.1:8000/api/edit_ledger  Edit Ledger
     * @apiName editLedger
     * @apiGroup Accounts
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {String} ledger_id Ledger Name (Required)
     * @apiParam {String} ledger_name Ledger Name (Required)
     * @apiParam {String} ledger_alias Ledger Alias
     * @apiParam {String} ledger_code Ledger Code
     * @apiParam {String} ledger_accgrp_id Ledger Account Group (Required)
     * @apiParam {String} ledger_branch_id Ledger Branch ID (Required)
     * @apiParam {String} ledger_address Ledger Address
     * @apiParam {String} ledger_email Ledger Email
     * @apiParam {String} ledger_mobile Ledger Mobile
     * @apiParam {String} ledger_due Ledger Due
     * @apiParam {String} ledger_tin Ledger Tin Number
     * @apiParam {String} ledger_flags Ledger Flags
     * @apiParam {String} ledger_notes Ledger Notes
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} message Success Message
     *
     * @apiError {String} message Error Message
     * @apiError {String} status Status Code
     *
     */
    public function editLedger(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            [
                'ledger_id' => 'required|exists:acc_ledgers',
                'ledger_name' => ['required', Rule::unique('acc_ledgers')->ignore($request['ledger_id'], 'ledger_id')],
                'ledger_accgrp_id' => 'required',
                'ledger_branch_id' => 'required',

            ],
            [
                'ledger_id.required' => 'Required',
                'ledger_name.required' => 'Required',

                'ledger_accgrp_id.required' => 'Required',
                'ledger_branch_id.required' => 'Required',

            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }

        $input = $request->all();
        $input['branch_id'] = $input['ledger_branch_id'];
        if ($request->op_bal_type) {
            $input['opening_balance'] = 0 - $request->op_bal;
        } else {
            $input['opening_balance'] = 0 + $request->op_bal;
        }
        unset($input['op_bal']);
        unset($input['op_bal_type']);
        $id = $input['ledger_id'];
        $data = Acc_ledger::find($id);
        $res = $data->fill($input)->save();

        $where = array();
        $where[] = ['branch_id', '=', $this->branch_id];
        $branch = Acc_branch::where($where)->first();
        $info = array();

        // Code for remove , at initail no voucher added
        $where = [
            'vch_ledger_from' => $request->ledger_id,
            'vch_ledger_to' => $request->ledger_id,
            'vch_vchtype_id' => 14,
            'vch_date' => date('Y-m-d', (strtotime('-1 day', strtotime($branch['branch_open_date'])))),
            'branch_id' => $this->branch_id,
            'van_id' => 0,
        ];
        $voucher = Acc_voucher::where($where)->first();
        if (empty($voucher)) {
            $max = Acc_voucher::max('vch_no');
            $info['vch_no'] = (empty($max)) ? 1 : $max + 1;
        }
        // Code Remove end
        // $max = Acc_voucher::max('vch_no');
        // $info['vch_no'] = (empty($max)) ? 1 : $max+1;
        $info['vch_ledger_from'] = $data->ledger_id;
        $info['vch_ledger_to'] = $data->ledger_id;
        $info['vch_date'] = date('Y-m-d', (strtotime('-1 day', strtotime($branch['branch_open_date']))));
        $info['vch_in'] = (!$request->op_bal_type) ? abs($request->op_bal) : 0;
        $info['vch_out'] = ($request->op_bal_type) ? abs($request->op_bal) : 0;
        $info['vch_vchtype_id'] = 14;
        $info['vch_notes'] = 'Starting balance';
        $info['vch_added_by'] = $this->usr_id;
        $info['vch_id2'] = 0;
        $info['vch_from_group'] = $request->ledger_accgrp_id;
        $info['vch_flags'] = 1;
        $info['ref_no'] = 0;
        $info['branch_id'] = $this->branch_id;
        $info['is_web'] = 1;
        $info['godown_id'] = 0;
        $info['van_id'] = 0;
        $info['branch_ref_no'] = 0;
        $info['sub_ref_no'] = 0;
        $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
        $voucher = Acc_voucher::updateOrCreate(
            [
                'vch_ledger_from' => $data->ledger_id,
                'vch_ledger_to' => $data->ledger_id,
                'vch_vchtype_id' => 14,
                'vch_date' => $info['vch_date'],
                'branch_id' => $this->branch_id,
                'van_id' => 0,
            ],
            $info
        );

        Acc_voucher::where(['vch_in' => 0, 'vch_out' => 0])->delete();
        if ($res) {
            return response()->json(['message' => 'updated_successfully', 'status' => $this->created_stat]);
        } else {
            return response()->json(['message' => 'Ledger not  edited', 'status' => $this->badrequest_stat]);
        }
    }

    /**
     * @api {post} http://127.0.0.1:8000/api/addCustomer  Add  Customer
     * @apiName addCustomer
     * @apiGroup Accounts
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {varchar} ledger_name Ledger Name (Required)
     * @apiParam {varchar} ledger_alias Ledger Alias
     * @apiParam {varchar} ledger_code Ledger Code
     * @apiParam {smallint} ledger_accgrp_id Ledger Account Group
     * @apiParam {int} ledger_branch_id Ledger Branch ID
     * @apiParam {varchar} ledger_address Ledger Address
     * @apiParam {varchar} ledger_email Ledger Email
     * @apiParam {varchar} ledger_mobile Ledger Mobile
     * @apiParam {smallint} ledger_due Ledger Due
     * @apiParam {varchar} ledger_tin Ledger Tin Number
     * @apiParam {smallint} ledger_flags Ledger Flags
     * @apiParam {varchar} ledger_notes Ledger Notes
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} message Success Message
     *
     * @apiError {String} message Error Message
     * @apiError {String} status Status Code
     *
     */

    public function addCustomer(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            [
                'ledger_name' => 'required|unique:acc_ledgers',

            ],
            [
                'ledger_name.required' => 'Required',

            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }

        $res = Corelib::createCust($request);

        if ($res) {
            return response()->json(['message' => 'saved_successfully', 'status' => $this->created_stat]);
        } else {
            return response()->json(['message' => 'Customer not  created.Please contact your administrator', 'status' => $this->badrequest_stat]);
        }
    }

    /**
     * @api {post} http://127.0.0.1:8000/api/edit_customer  Edit Customer
     * @apiName editCustomer
     * @apiGroup Accounts
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {bigint} ledger_id Ledger ID (Required)
     * @apiParam {varchar} ledger_name Ledger Name (Required)
     * @apiParam {varchar} ledger_alias Ledger Alias
     * @apiParam {varchar} ledger_code Ledger Code
     * @apiParam {smallint} ledger_accgrp_id Ledger Account Group
     * @apiParam {int} ledger_branch_id Ledger Branch ID
     * @apiParam {varchar} ledger_address Ledger Address
     * @apiParam {varchar} ledger_email Ledger Email
     * @apiParam {varchar} ledger_mobile Ledger Mobile
     * @apiParam {smallint} ledger_due Ledger Due
     * @apiParam {varchar} ledger_tin Ledger Tin Number
     * @apiParam {smallint} ledger_flags Ledger Flags
     * @apiParam {varchar} ledger_notes Ledger Notes
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} message Success Message
     *
     * @apiError {String} message Error Message
     * @apiError {String} status Status Code
     *
     */

    public function editCustomer(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            [
                'ledger_id' => 'required',
                'ledger_name' => ['required', Rule::unique('acc_ledgers')->ignore($request['ledger_id'], 'ledger_id')],

            ],
            [
                'ledger_id.required' => 'Required',
                'ledger_name.required' => 'Ledger name  required',
                'ledger_accgrp_id.required' => 'Ledger group  required',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }

        $input = $request->all();
        $id = $input['ledger_id'];
        $res = Corelib::editCust($request, $id);

        if ($res) {
            return response()->json(['message' => 'updated_successfully', 'status' => $this->created_stat]);
        } else {
            return response()->json(['message' => 'Customer not  edited.Please contact your administrator', 'status' => $this->badrequest_stat]);
        }
    }

    /**
     * @api {get} http://127.0.0.1:8000/api/get_customer  Get Customer Data
     * @apiName getCustomer
     * @apiGroup Accounts
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} data Customer Data
     *
     */

    public function getCustomer()
    {
        return response()->json(['data' => Acc_customer::where('branch_id', $this->branch_id)->get(), 'status' => $this->success_stat]);
    }

    /**
     * @api {post} http://127.0.0.1:8000/api/add_supplier  Add Supplier
     * @apiName addSupplier
     * @apiGroup Accounts
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {varchar} ledger_name Ledger Name (Required)
     * @apiParam {varchar} ledger_alias Ledger Alias
     * @apiParam {varchar} ledger_code Ledger Code
     * @apiParam {smallint} ledger_accgrp_id Ledger Account Group
     * @apiParam {int} ledger_branch_id Ledger Branch ID
     * @apiParam {varchar} ledger_address Ledger Address
     * @apiParam {varchar} ledger_email Ledger Email
     * @apiParam {varchar} ledger_mobile Ledger Mobile
     * @apiParam {smallint} ledger_due Ledger Due
     * @apiParam {varchar} ledger_tin Ledger Tin Number
     * @apiParam {smallint} ledger_flags Ledger Flags
     * @apiParam {varchar} ledger_notes Ledger Notes
     * @apiParam {varchar} supp_perm_addr Suppliers Permenent Address(Saved in Supplier table)
     * @apiParam {int} branch_id Supplier Branch ID (for supplier table)
     *
     *
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} message Success Message
     *
     * @apiError {String} message Error Message
     * @apiError {String} status Status Code
     *
     */

    public function addSupplier(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            [
                'ledger_name' => 'required|unique:acc_ledgers',
                'ledger_accgrp_id' => 'required',
            ],
            [
                'ledger_name.required' => 'Ledger name  required',
                'ledger_accgrp_id.required' => 'Ledger group  required',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }

        $res = Corelib::createSup($request);

        if ($res) {
            return response()->json(['message' => 'saved_successfully', 'status' => $this->created_stat]);
        } else {
            return response()->json(['message' => 'Supplier not  created.Please contact your administrator', 'status' => $this->badrequest_stat]);
        }
    }

    /**
     * @api {post} http://127.0.0.1:8000/api/edit_supplier  Edit Supplier Details
     * @apiName editSupplier
     * @apiGroup Accounts
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {String} supp_id Supplier ID (Required)
     * @apiParam {varchar} ledger_name Ledger Name (Required)
     * @apiParam {varchar} ledger_alias Ledger Alias
     * @apiParam {varchar} ledger_code Ledger Code
     * @apiParam {smallint} ledger_accgrp_id Ledger Account Group
     * @apiParam {int} ledger_branch_id Ledger Branch ID
     * @apiParam {varchar} ledger_address Ledger Address
     * @apiParam {varchar} ledger_email Ledger Email
     * @apiParam {varchar} ledger_mobile Ledger Mobile
     * @apiParam {smallint} ledger_due Ledger Due
     * @apiParam {varchar} ledger_tin Ledger Tin Number
     * @apiParam {smallint} ledger_flags Ledger Flags
     * @apiParam {varchar} ledger_notes Ledger Notes
     * @apiParam {varchar} supp_perm_addr Suppliers Permenent Address(Saved in Supplier table)
     * @apiParam {int} branch_id Supplier Branch ID (for supplier table)
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} message Success Message
     *
     * @apiError {String} message Error Message
     * @apiError {String} status Status Code
     *
     */
    public function editSupplier(Request $request)
    {

        $id = $request['supp_id'];
        $validator = Validator::make(
            $request->all(),
            [
                'supp_id' => 'required|exists:acc_suppliers',
                'ledger_name' => 'required',
                'ledger_accgrp_id' => 'required',
            ],
            [
                'supp_id.exists' => 'The selected supp id is invalid. ',
                'supp_id.required' => 'Supplier id required',
                'ledger_name.required' => 'Ledger name  required',
                'ledger_accgrp_id.required' => 'Ledger group  required',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }

        $res = Corelib::editSup($request, $id);

        if ($res) {
            return response()->json(['message' => 'updated_successfully', 'status' => $this->created_stat]);
        } else {
            return response()->json(['message' => 'Supplier not  edited.Please contact your administrator', 'status' => $this->badrequest_stat]);
        }
    }

    /**
     * @api {get} http://127.0.0.1:8000/api/get_ledgerdetails  Get Ledger Details
     * @apiName getLedgerDetails
     * @apiGroup Accounts
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} data Customer Data
     *
     */

    public function getLedgerDetails()
    {
        return response()->json(['data' => Acc_ledger::get(), 'status' => $this->success_stat]);
    }
    /**
     * @api {post} http://127.0.0.1:8000/api/set_opening_balance  Set Opening Balance
     * @apiName setOpeningBalance
     * @apiGroup Accounts
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {bigint} opbal_ledger_id ledger ID (Required)
     * @apiParam {double} opbal Opening Balance (Required)
     * @apiParam {bigint} opbal_added_by Added By (Required)
     * @apiParam {tinyint} opbal_flags Balance Flag (Required)
     * @apiParam {varchar} balance_type Credit/Debit (credit/debit)
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} data Customer Data
     *
     */
    public function setOpeningBalance(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            [
                'opbal_ledger_id' => 'required',
                'opbal' => 'required',
                'opbal_added_by' => 'required',
                'opbal_flags' => 'required',
                'balance_type' => 'required',
            ],
            [
                'opbal_ledger_id.required' => 'Required',
                'opbal.required' => 'Required',
                'opbal_added_by.required' => 'Required',
                'opbal_flags.required' => 'Required',
                'balance_type' => 'Required',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }

        $ledger_group = Acc_opening_balance::firstOrNew(['opbal_ledger_id' => $request->opbal_ledger_id]);
        if ($request->balance_type == "credit") {
            $ledger_group->opbal_out = $request->opbal;
            $ledger_group->opbal_in = 0;
        } else {
            $ledger_group->opbal_in = $request->opbal;
            $ledger_group->opbal_out = 0;
        }
        $ledger_group->opbal_added_by = $request->opbal_added_by;
        $ledger_group->opbal_flags = $request->opbal_flags;
        $ledger_group->company_id = $request->company_id;
        $ledger_group->branch_id = $request->branch_id;
        $ledger_group->server_sync_time = $request->server_sync_time;
        $result = $ledger_group->save();

        if ($result) {
            return response()->json(['message' => 'updated_successfully', 'status' => $this->created_stat]);
        } else {
            return response()->json(['message' => 'Failed', 'status' => $this->badrequest_stat]);
        }
    }

    /**
     * @api {post} http://127.0.0.1:8000/api/set_ledger_balance_privacy  Set Ledger Balance Privacy
     * @apiName setLedgerBalancePrivacy
     * @apiGroup Accounts
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {Int} ledger_id ledger ID (Required)
     * @apiParam {String} ledger_accgrp_id Ledger Group ID (Required)
     * @apiParam {Int} ledger_balance_privacy Admin/All User (Required | 1/0)

     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} message Message
     *
     */
    public function setLedgerBalancePrivacy(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            [
                'ledger_id' => 'required',
                'ledger_accgrp_id' => 'required',
                'ledger_balance_privacy' => 'required',

            ],
            [
                'opballedger_id_ledger_id.required' => 'Required',
                'ledger_accgrp_id.required' => 'Required',
                'ledger_balance_privacy.required' => 'Required',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }

        $ledger = Acc_ledger::find($request->ledger_id);
        $ledger->ledger_balance_privacy = $request->ledger_balance_privacy;
        $result = $ledger->save();

        if ($result) {
            return response()->json(['message' => 'updated_successfully', 'status' => $this->created_stat]);
        } else {
            return response()->json(['message' => 'Failed', 'status' => $this->badrequest_stat]);
        }
    }

    /**
     * @api {Post} http://127.0.0.1:8000/api/search_ledger  Search Ledger
     * @apiName searchLedger
     * @apiGroup Accounts
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {String} ledger_name Ledger Keyword
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} data All Ledger Details
     *
     */

    public function searchLedger(Request $request)
    {
        if ($request['ledger_name'] != "") {
            $acc_ledger = Acc_ledger::where('ledger_name', 'like', '%' . $request['ledger_name'] . '%')
                ->where('branch_id', $this->branch_id)
                ->take(50)
                ->get();
            return response()->json(['data' => $acc_ledger, 'status' => $this->success_stat]);
        } else {
            $data = array();
            return response()->json(['data' => $data, 'status' => $this->success_stat]);
        }
    }

    /**
     * @api {Post} http://127.0.0.1:8000/api/staff_search_ledger  Search Ledger
     * @apiName searchLedger
     * @apiGroup Accounts
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {String} ledger_name Ledger Keyword
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} data All Ledger Details
     *
     */

    public function searchLedgerDetail(Request $request)
    {
        if ($request['ledger_name'] != "") {
            $acc_ledger = Acc_ledger::where('ledger_name', 'like', '%' . $request['ledger_name'] . '%')
                ->join('acc_groups', 'acc_groups.accgrp_id', '=', 'acc_ledgers.ledger_accgrp_id')
                ->leftJoin('staff', 'staff.staff_laccount_no', '=', 'acc_ledgers.ledger_id')
                ->take(50)
                ->get();
            return response()->json(['data' => $acc_ledger, 'status' => $this->success_stat]);
        } else {
            $data = array();
            return response()->json(['data' => $data, 'status' => $this->success_stat]);
        }
    }

    public function searchLedgerByAccGroup(Request $request)
    {
        if ($request['ledger_name'] != "") {
            $acc_ledger = Acc_ledger::where('ledger_name', 'like', '%' . $request['ledger_name'] . '%')
                ->where('ledger_accgrp_id', $request['ledger_grp'])
                ->where('branch_id', $this->branch_id)
                ->take(20)
                ->get();
            return response()->json(['data' => $acc_ledger, 'status' => $this->success_stat]);
        } else {
            // $data = array();
            $acc_ledger = Acc_ledger::where('ledger_accgrp_id', $request['ledger_grp'])
                ->where('branch_id', $this->branch_id)
            // ->take(1)
                ->get();
            return response()->json(['data' => $acc_ledger, 'status' => $this->success_stat]);
        }
    }

    // public function getCashInHandAndBanks(Request $request) {
    //         $acc_ledger = Acc_ledger::whereIn('ledger_accgrp_id', [3,4])->get();
    //         return response()->json(['data' => $acc_ledger, 'status' => $this->success_stat]);
    // }

    public function getCashInHandAndBanks(Request $request)
    {
        $acc_ledger = Acc_ledger::whereIn('ledger_accgrp_id', [3, 4])->where('branch_id', '=', $this->branch_id)->get();
        return response()->json(['data' => $acc_ledger, 'status' => $this->success_stat]);
    }

    public function getBankLedgers(Request $request)
    {
        $acc_ledger = Acc_ledger::where('ledger_accgrp_id', 4)->where('branch_id', '=', $this->branch_id)->get();
        return response()->json(['data' => $acc_ledger, 'status' => $this->success_stat]);
    }

    public function getCashInHandLedgers(Request $request)
    {
        $acc_ledger = Acc_ledger::where('ledger_accgrp_id', 3)->where('branch_id', '=', $this->branch_id)->get();
        return response()->json(['data' => $acc_ledger, 'status' => $this->success_stat]);
    }

    // public function getNonAccLedger(Request $request) {
    //         $acc_ledger = Acc_ledger::whereNotIn('ledger_accgrp_id', [3,4])->where('ledger_edit', '=', 0)->get();
    //         return response()->json(['data' => $acc_ledger, 'status' => $this->success_stat]);
    // }
    public function getCustomerLedger(Request $request)
    {
        $acc_ledger = Acc_ledger::whereIn('ledger_accgrp_id', [21])->where('branch_id', '=', $this->branch_id)->get();
        return response()->json(['data' => $acc_ledger, 'status' => $this->success_stat]);
    }

// this function for remove after frontend up
    public function getNonAccLedger(Request $request)
    {
        $acc_ledger = Acc_ledger::whereIn('ledger_accgrp_id', [21])->where('branch_id', '=', $this->branch_id)->get();
        return response()->json(['data' => $acc_ledger, 'status' => $this->success_stat]);
    }

    public function getNonAccLedgers(Request $request)
    {
        $acc_ledger = Acc_ledger::where('branch_id', '=', $this->branch_id)
        // ->whereNotIn('ledger_accgrp_id', [3,4])
            ->get();
        return response()->json(['data' => $acc_ledger, 'status' => $this->success_stat]);
    }

    public function getCurrentBalance(Request $request)
    {
        // $acc_ledger = Acc_ledger::whereNotIn('ledger_accgrp_id', [3,4])->where('ledger_edit', '=', 0)->get();
        // return response()->json(['data' => $acc_ledger, 'status' => $this->success_stat]);
        // return response()->json(['data' => $acc_ledger, 'status' => $this->success_stat]);
    }

    public function listLedger()
    {
        $data = Acc_ledger::select('acc_ledgers.*', 'acc_groups.accgrp_name')->join('acc_groups', 'acc_ledgers.ledger_accgrp_id', 'acc_groups.accgrp_id')->where('ledger_flags', 1)->orderBy('ledger_id', 'desc')
            ->where('acc_ledgers.branch_id', $this->branch_id)->paginate($this->per_page);
        return response()->json(['data' => $data, 'status' => $this->success_stat]);

    }

    public function searchLedgerList(Request $request)
    {

        $whr['ledger_flags'] = 1;
        $anotherBranchLedgrIds = [];
        if ($this->branch_id > 0) {
            $whr['acc_ledgers.branch_id'] = $this->branch_id;
            // to get another brach branch@brach_code ledgers, for inter branch transaction details
            $where = array();
            $where[] = ['taxled_taxcat_id', '=', 0];
            $where[] = ['taxled_taxcatsub_id', '=', 12];
            $anotherBranchLedgrIds = TaxLedgers::where($where)->pluck('taxled_ledger_id')->toArray();
            $LedgrIds = implode(',', $anotherBranchLedgrIds);
        }

        if ($request->search == "") {
            $data = Acc_ledger::select('acc_ledgers.*', 'acc_groups.accgrp_name')
                ->join('acc_groups', 'acc_ledgers.ledger_accgrp_id', 'acc_groups.accgrp_id')
                ->where(function ($q) use ($whr, $anotherBranchLedgrIds) {
                    $q->where($whr);
                    if ($this->branch_id > 0 && !empty($anotherBranchLedgrIds)) {
                        // $q->orWhere('acc_ledgers.ledger_id IN(' . $LedgrIds . ')');
                        $q->orWhereIn('acc_ledgers.ledger_id', $anotherBranchLedgrIds);
                    }
                })->orderBy('ledger_id', 'desc')->paginate(100);
        } else {
            $data = Acc_ledger::select('acc_ledgers.*', 'acc_groups.accgrp_name')
                ->join('acc_groups', 'acc_ledgers.ledger_accgrp_id', 'acc_groups.accgrp_id')
                ->where(function ($q) use ($whr, $anotherBranchLedgrIds) {
                    $q->where($whr);
                    if ($this->branch_id > 0 && !empty($anotherBranchLedgrIds)) {
                        // $q->orWhere('acc_ledgers.ledger_id IN(' . $LedgrIds . ')');
                        $q->orWhereIn('acc_ledgers.ledger_id', $anotherBranchLedgrIds);
                    }
                })
                ->where(function ($q) use ($request) {
                    $q->where('ledger_name', 'like', '%' . $request->search . '%')
                        ->orWhere('ledger_code', 'like', '%' . $request->search . '%')
                        ->orWhere('opening_balance', 'like', '%' . $request->search . '%');})
                ->orderBy('ledger_id', 'desc')->paginate(100);
        }
        //->where('ledger_alias', 'like', '%' .  $request->search . '%')->orWhere('ledger_code', 'like', '%' .  $request->search . '%')

        return response()->json(['data' => $data, 'status' => $this->success_stat]);

    }

    public function ledgerAll(Request $request)
    {
        $acc_ledger = Acc_ledger::where('ledger_name', 'like', '%' . $request['ledger_name'] . '%')
            ->where('branch_id', $this->branch_id)->get();
        return response()->json(['data' => $acc_ledger, 'status' => $this->success_stat]);
    }

    // with openig balance from ledger table (but an entry will come to voucher table with type 14)
    // public function getledgerBalance(Request $request) {

    //     $amnt = DB::raw('distinct(opening_balance) + (IFNULL(sum(vch_in),0) - IFNULL(sum(vch_out),0)) as bal_amt');
    //     $where = array();
    //     $where[] = ['ledger_id','=', $request->ledger_id];
    //     $qry = Acc_ledger::select($amnt)
    //     ->where($where)
    //     ->leftjoin('acc_voucher', 'acc_ledgers.ledger_id', '=', 'acc_voucher.vch_ledger_from')
    //     ->first();
    //     return parent::displayData($qry['bal_amt']);

    // }

    public function getledgerBalance(Request $request)
    {

        $amnt = DB::raw('(IFNULL(sum(vch_in),0) - IFNULL(sum(vch_out),0)) as bal_amt');
        $where = array();
        $where[] = ['vch_ledger_to', '=', $request->ledger_id];
        $where[] = ['vch_flags', '=', 1];
        $qry = Acc_voucher::select($amnt)
            ->where($where)
            ->first();
        $sign = '';
        if ($qry['bal_amt'] < 0) {
            $sign = 'CR';
        }
        if ($qry['bal_amt'] > 0) {
            $sign = 'DR';
        }
        $qry['bal_with_sign'] = abs($qry['bal_amt']) . ' ' . $sign;
        $qry['bal_without_sign'] = abs($qry['bal_amt']);
        $qry['bal_sign'] = $sign;
        return parent::displayData($qry);

    }

    public function searchAccGroup(Request $request)
    {
        if ($request['name'] != "") {
            $acc_ledger = Acc_group::where('accgrp_name', 'like', '%' . $request['name'] . '%')
                ->take(50)
                ->get();
            return response()->json(['data' => $acc_ledger, 'status' => $this->success_stat]);
        } else {
            $data = array();
            return response()->json(['data' => $data, 'status' => $this->success_stat]);
        }
    }

    public function isLedgerUsed(Request $request)
    {

        $exist_insale = $exist_inpurch = $exist_invoucher = 0;
        $is_sup = $is_cust = 0;
        if ($ledger_id = $request->ledger_id) {

            if ($customer = Acc_customer::where('ledger_id', $ledger_id)->first()) {
                $exist_insale = SalesMaster::where('sales_cust_id', $customer['cust_id'])->count();
            } else {
                $is_cust = 1;
            }

            if ($supplier = Supplier::where('supp_ledger_id', $ledger_id)->first()) {
                $exist_inpurch = Purchase::where('purch_supp_id', $supplier['supp_id'])->count();
            } else {
                $is_sup = 1;
            }

            // $whr = " (vch_ledger_from = $ledger_id) OR (vch_ledger_to = $ledger_id) ";
            $exist_invoucher = Acc_voucher::where('vch_ledger_from', $ledger_id)
                ->orWhere('vch_ledger_to', $ledger_id)->count();

            if ($exist_insale || $exist_inpurch || $exist_invoucher) {
                return response()->json(['deletable' => 0, 'status' => $this->success_stat]);
            }

            return response()->json(['deletable' => 1, 'is_sup' => $is_sup, 'is_cust' => $is_cust, 'status' => $this->success_stat]);

        }
        return response()->json(['deletable' => 1, 'is_sup' => $is_sup, 'is_cust' => $is_cust, 'status' => $this->success_stat]);

    }

    public function isCustomerUsed(Request $request)
    {

        $exist_insale = $exist_invoucher = 0;

        if ($cust_id = $request->cust_id) {

            if ($customer = Acc_customer::where('cust_id', $cust_id)->first()) {
                $exist_insale = SalesMaster::where('sales_cust_id', $cust_id)->count();
            }

            $exist_invoucher = Acc_voucher::where('vch_ledger_from', $customer['ledger_id'])
                ->orWhere('vch_ledger_to', $customer['ledger_id'])->count();

            if ($exist_insale || $exist_invoucher) {
                return response()->json(['ledger_id' => 0, 'status' => $this->success_stat]);

            } else {
                return response()->json(['ledger_id' => $customer['ledger_id'], 'status' => $this->success_stat]);
            }

        }
        return response()->json(['ledger_id' => 0, 'status' => $this->success_stat]);

    }

    public function deleteLedger(Request $request)
    {
        $exist_insale = $exist_inpurch = $exist_invoucher = 0;
        if ($ledger_id = $request->ledger_id) {

            if ($customer = Acc_customer::where('ledger_id', $ledger_id)->first()) {
                $exist_insale = SalesMaster::where('sales_cust_id', $customer['cust_id'])->count();
            }

            if ($supplier = Supplier::where('supp_ledger_id', $ledger_id)->first()) {
                $exist_inpurch = Purchase::where('purch_supp_id', $supplier['supp_id'])->count();
            }

            $exist_invoucher = Acc_voucher::where('vch_ledger_from', $ledger_id)
                ->orWhere('vch_ledger_to', $ledger_id)->count();

            if ($exist_insale || $exist_inpurch || $exist_invoucher) {
                return response()->json(['message' => 'Voucher Exist for the ledger', 'data' => $ledger_id, 'status' => $this->success_stat]);
            } else {

                if ($customer['cust_id']) {
                    $customer = Acc_customer::where('ledger_id', $ledger_id)->delete();
                }

                if ($supplier) {
                    $customer = Supplier::where('supp_ledger_id', $ledger_id)->delete();
                }

                Acc_ledger::where('ledger_id', $ledger_id)->delete();

                return response()->json(['message' => 'Ledger  Deleted', 'data' => $ledger_id, 'status' => $this->success_stat]);
            }

        }
        return response()->json(['message' => 'Ledger Not Deleted', 'data' => $ledger_id, 'status' => $this->success_stat]);

    }

    public function addLedgAsCustomer(Request $request)
    {

        if ($ledger_id = $request->ledger_id) {

            $ledg_data = Acc_ledger::where('ledger_id', $ledger_id)->first();

            return response()->json(['message' => 'Customer Created', 'status' => $this->success_stat]);
        }

        return response()->json(['message' => 'Invalid Ledger ', 'status' => $this->success_stat]);

    }

    public function addLedgAsSupplier(Request $request)
    {

        if ($customer_id = $request->customer_id) {

            $data = Acc_customer::where('cust_id', $request->customer_id)->first();

            $supplier['supp_ledger_id'] = $data['ledger_id'];
            $supplier['supp_name'] =  $data['name'];
            $supplier['supp_alias'] = $data['alias'];
            $supplier['supp_code'] =($data['cust_code']) ? $data['cust_code'] : '';
            $supplier['supp_address1'] =($data['cust_home_addr']) ? $data['cust_home_addr'] : '';
         

            $supplier['supp_zip'] =  ($data['zip']) ? $data['zip'] : '';
            $supplier['supp_city'] = ($data['city']) ? $data['city'] : '';
            $supplier['supp_state'] =($data['state']) ? $data['state'] : '';
            $supplier['supp_country'] =  ($data['country']) ? $data['country'] : '';

            $supplier['opening_balance'] =  $data['opening_balance'];
       
            $supplier['supp_mob'] = ($data['mobile']) ? $data['mobile'] : '';
            $supplier['supp_email'] = ($data['email']) ? $data['email'] : '';
            $supplier['supp_notes'] = ($data['note']) ? $data['note'] : '';
            $supplier['supp_tin'] = ($data['vat_no']) ? $data['vat_no'] : '';

            $supplier['due_days'] = ($data['due_days']) ? $data['due_days'] : '';

            $supplier['supp_branch_id'] = $this->branch_id;
            $supplier['branch_id'] = $this->branch_id;
            $supplier['server_sync_time'] = $this->server_sync_time;
            $res = Supplier::create($supplier);
           
            return response()->json(['message' => 'Supplier Created', 'status' => $this->success_stat]);
        }

        return response()->json(['message' => 'Invalid Customer ', 'status' => $this->success_stat]);

    }
   

}
