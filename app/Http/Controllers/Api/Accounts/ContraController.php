<?php

namespace App\Http\Controllers\Api\Accounts;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiParent;

use App\Http\Controllers\Controller;
use App\Models\Accounts\Acc_voucher;
use App\Models\Company\Acc_branch;
use App\Models\Accounts\Contra;


use Validator;


class ContraController extends ApiParent
{
    public function ContraOperations(Request $request, $type)
    {

        switch ($type) {
            case 'add':

                return $this->add($request);
                break;
            case 'list':
                return $this->list($request);
                break;
            case 'search_list':
                return $this->searchlist($request);
                break;    
            case 'viewContra':
                return $this->getContra($request);
                break;
            case 'next-id':
                   return $this->getNextId($request);
                   break;
            case 'void':
                    return $this->void_contra($request);

            default:
                return response()->json(['error' => 'Invalid Endpoint', 'status' => $this->badrequest_stat]);
                break;
        }
    }



    public function add($request) {
       
        $validator = Validator::make(
            $request->all(),
            [
                
                'rec_date' => 'required',
            
                'rec_type' => 'required',
                'rec_bankacc' => 'required',
                'rec_toAcc' => 'required', 
                'amount' => 'required|numeric|min:0|not_in:0',

            ],[
                'rec_date.required' => 'Required',
                'rec_bankacc.required' => 'Required',
                'rec_toAcc.required' => 'Required',
                'amount.required' => 'Required',
                'amount.numeric' => 'Enter Valid Amount',
                'amount.min' => 'Enter Valid Amount',
                'amount.not_in' => 'Enter Valid Amount',
     

            ]
        );
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }
        $request = $request->all();
// print_r(date('d-m-Y',strtotime($request['rec_date'])));
        $info = array();
        $where = array();
        $where[] = ['branch_id', '=', $this->branch_id];
        $branch = Acc_branch::where($where)->first();
        $max = Contra::max('cont_ref_no');
        $info['cont_ref_no'] = (empty($max)) ? 1 : $max + 1;
        $where[] = ['is_web', '=', 1];
        $max = Contra::where($where)->max('branch_cont_no');
        if (empty($max)) {
            $info['branch_cont_no'] = 'W#CN' . $branch['branch_code'] . '0001';
        } else {
            $no = ltrim($max, 'W#CN' . $branch['branch_code']);
            $newNo = +$no + 1;
            $info['branch_cont_no'] = 'W#CN' . $branch['branch_code'] . str_pad($newNo, 4, "0", STR_PAD_LEFT);
        }
        $branchRecNo = $info['branch_cont_no'];


        $contra['cont_ref_no'] = Contra::max('cont_ref_no') + 1;
        $contra['cont_type'] = $request['rec_type'];
        $contra['cont_date'] = $request['rec_date'];
        // print_r($contra['cont_date']);
        $contra['cont_time'] = date('H:i:s');
        $contra['cont_datetime'] = $contra['cont_date'] . ' ' . $contra['cont_time'];
        $contra['cont_timestamp'] = strtotime($request['rec_date']); 
        $contra['cont_from_ledger_id'] = $request['rec_bankacc']['ledger_id'];
        $contra['cont_to_ledger_id'] = $request['rec_toAcc']['ledger_id'];
        $contra['cont_amount'] = $request['amount'];
        $contra['cont_note'] = (isset($request['note']) ? $request['note'] : '');
        $contra['branch_cont_no'] = $branchRecNo;

        $contra['is_web'] = 1;
        $contra['sync_completed'] = 1;
        $contra['cont_flags'] = 1;
        $contra['branch_id'] = $this->branch_id;
        $contra['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);

        $res = Contra::create($contra);
// print_r($res);

if($request['rec_type'] == 0){
        $info = array();
        $max = Acc_voucher::max('vch_no');
        $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max + 1;
        $info['vch_ledger_from'] = $request['rec_bankacc']['ledger_id'];
        $info['vch_ledger_to'] = $request['rec_toAcc']['ledger_id'];
        $info['vch_date'] = $contra['cont_datetime'];
        $info['vch_in'] = 0;
        $info['vch_out'] = $request['amount'];
        $info['vch_vchtype_id'] = 2;
        $info['vch_notes'] = (isset($request['note']) ? $request['note'] : '');
        $info['vch_added_by'] = $this->usr_id;
        $info['vch_id2'] = 0;
        $info['vch_from_group'] = $request['rec_bankacc']['ledger_accgrp_id'];
        $info['vch_flags'] = 1;
        $info['ref_no'] = $res['cont_ref_no'];
        $info['branch_id'] = $this->branch_id;
        $info['is_web'] = 1;
        $info['godown_id'] = 0;
        $info['van_id'] = 0;
        $info['branch_ref_no'] = $branchRecNo;
        $info['sub_ref_no'] = 0;
        $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
        $res2 = Acc_voucher::create($info);

        $info = array();
        $info['vch_no'] = $vouchNo;
        $info['vch_ledger_from'] = $request['rec_toAcc']['ledger_id'];
        $info['vch_ledger_to'] = $request['rec_bankacc']['ledger_id'];
        $info['vch_date'] = $contra['cont_datetime'];
        $info['vch_in'] =  $request['amount'];
        $info['vch_out'] = 0;
        $info['vch_vchtype_id'] = 2;
        $info['vch_notes'] = (isset($request['note']) ? $request['note'] : '');
        $info['vch_added_by'] = $this->usr_id;
        $info['vch_id2'] = $res2->vch_id;
        $info['vch_from_group'] = $request['rec_toAcc']['ledger_accgrp_id'];
        $info['vch_flags'] = 1;
        $info['ref_no'] = $res['cont_ref_no'];
        $info['branch_id'] = $this->branch_id;
        $info['is_web'] = 1;
        $info['godown_id'] = 0;
        $info['van_id'] = 0;
        $info['branch_ref_no'] = $branchRecNo;
        $info['sub_ref_no'] = 0;
        $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
        $res2 = Acc_voucher::create($info);

    
    }else{
        
        $info = array();
        $max = Acc_voucher::max('vch_no');
        $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max + 1;
        $info['vch_ledger_from'] = $request['rec_toAcc']['ledger_id'];
        $info['vch_ledger_to'] = $request['rec_bankacc']['ledger_id'];
        $info['vch_date'] = $contra['cont_datetime'];
        $info['vch_in'] = 0;
        $info['vch_out'] = $request['amount'];
        $info['vch_vchtype_id'] = 2;
        $info['vch_notes'] = (isset($request['note']) ? $request['note'] : '');
        $info['vch_added_by'] = $this->usr_id;
        $info['vch_id2'] = 0;
        $info['vch_from_group'] = $request['rec_bankacc']['ledger_accgrp_id'];
        $info['vch_flags'] = 1;
        $info['ref_no'] = $res['cont_ref_no'];
        $info['branch_id'] = $this->branch_id;
        $info['is_web'] = 1;
        $info['godown_id'] = 0;
        $info['van_id'] = 0;
        $info['branch_ref_no'] = $branchRecNo;
        $info['sub_ref_no'] = 0;
        $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
        $res2 = Acc_voucher::create($info);

        $info = array();
        $info['vch_no'] = $vouchNo;
        $info['vch_ledger_from'] = $request['rec_bankacc']['ledger_id'];
        $info['vch_ledger_to'] = $request['rec_toAcc']['ledger_id'];
        $info['vch_date'] = $contra['cont_datetime'];
        $info['vch_in'] =  $request['amount'];
        $info['vch_out'] = 0;
        $info['vch_vchtype_id'] = 2;
        $info['vch_notes'] = (isset($request['note']) ? $request['note'] : '');
        $info['vch_added_by'] = $this->usr_id;
        $info['vch_id2'] = $res2->vch_id;
        $info['vch_from_group'] = $request['rec_toAcc']['ledger_accgrp_id'];
        $info['vch_flags'] = 1;
        $info['ref_no'] = $res['cont_ref_no'];
        $info['branch_id'] = $this->branch_id;
        $info['is_web'] = 1;
        $info['godown_id'] = 0;
        $info['van_id'] = 0;
        $info['branch_ref_no'] = $branchRecNo;
        $info['sub_ref_no'] = 0;
        $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
        $res2 = Acc_voucher::create($info);

    }

if($res && $res2){
    return parent::displayMessage('Contra Added Successfully');
    
}else{
    return response()->json(['error' => 'Error Occured on Updating', 'status' => $this->badrequest_stat]);


}



}







public function list($request) {
    $where = array();
    $where[] = ['branch_id','=', $this->branch_id];
    $where[] = ['sync_completed','=', 1];
    $where[] = ['cont_flags','=', 1];
    $result = Contra::where($where)->orderBy('cont_id','desc')->paginate($this->per_page);
    return parent::displayData($result);
}

    public function getNextId($request) {
        $max = Contra::max('cont_ref_no');
        $ret['cont_ref_no'] = (empty($max)) ? 1 : $max+1;
        return parent::displayData($ret);
    }

    public function searchlist($request) {
        $where = array();
        $where[] = ['branch_id','=', $this->branch_id];
        $where[] = ['sync_completed','=', 1];
        $where[] = ['cont_flags','=', 1];
        $orwhere = array();
        $orwhere[] = ['branch_id','=', $this->branch_id];
        $orwhere[] = ['sync_completed','=', 1];
        $orwhere[] = ['cont_flags','=', 1];

if($request['search'] != ''){
    $where[] = ['cont_ref_no','like', '%'.$request['search'].'%'];
}

if($request['search'] != ''){
    $orwhere[] = ['branch_cont_no', 'like', '%' . $request['search'] . '%'];
}

        $result = Contra::where($where)->orWhere($orwhere)->orderBy('cont_id','desc')->paginate($this->per_page);
        return parent::displayData($result);
    }



    public function getContra($request)
    {
// print_r($request);
    
        $data = Contra::select('contra.*', 'a.ledger_name as from_ledger_name', 'b.ledger_name as to_ledger_name')
        ->join('acc_ledgers as a', 'a.ledger_id', 'contra.cont_from_ledger_id')
        ->join('acc_ledgers as b', 'b.ledger_id', 'contra.cont_to_ledger_id');
        

    if ($this->branch_id > 0) {
        $data = $data->where('contra.branch_id', $this->branch_id);
    }

    $data = $data->where('contra.cont_ref_no', $request['rec_no'])->first();
    $data['total_amount_in_words'] = $this->getArabicCurrency($data['cont_amount']);

    return $data;

    }


    public function void_contra($request) {

        $data = $request->all();
       
        $flag = array(
            'cont_flags' => 0,
            'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)
        );
        $where = array();
        $where[] = ['cont_ref_no','=', $data['rec_no']];
        Contra::where($where)->update($flag);



        $flag = array(
            'vch_flags' => 0,
            'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)
        );
        $where = array();
        $where[] = ['ref_no','=', $data['rec_no']];
        $where[] = ['vch_vchtype_id','=', 2];
        $dueSub = Acc_voucher::where($where)->update($flag);
        return response()->json(['message' => 'Contra Voided Successfully', 'status' => $this->created_stat]);
    }




    public function getArabicCurrency(float $number)
    {
        $decimal = round($number - ($no = floor($number)), 2) * 100;
        $hundred = null;
        $digits_length = strlen($no);
        $i = 0;
        $str = array();
        $words = array(0 => '', 1 => 'one', 2 => 'two',
            3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
            7 => 'seven', 8 => 'eight', 9 => 'nine',
            10 => 'ten', 11 => 'eleven', 12 => 'twelve',
            13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
            16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
            19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
            40 => 'forty', 50 => 'fifty', 60 => 'sixty',
            70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
        $digits = array('', 'hundred', 'thousand', 'lakh', 'crore');
        while ($i < $digits_length) {
            $divider = ($i == 2) ? 10 : 100;
            $number = floor($no % $divider);
            $no = floor($no / $divider);
            $i += $divider == 10 ? 1 : 2;
            if ($number) {
                $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
                $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
                $str[] = ($number < 21) ? $words[$number] . ' ' . $digits[$counter] . $plural . ' ' . $hundred : $words[floor($number / 10) * 10] . ' ' . $words[$number % 10] . ' ' . $digits[$counter] . $plural . ' ' . $hundred;
            } else {
                $str[] = null;
            }

        }
        $Rupees = implode('', array_reverse($str));

        $paise = ($decimal > 0) ? "and " . ($words[floor($decimal / 10) * 10] . " " . $words[$decimal % 10]) . ' Halala' : '';
        return ($Rupees ? $Rupees . 'Riyals ' : '') . $paise . ' Only';
    }



}
