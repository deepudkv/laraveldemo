<?php

namespace App\Http\Controllers\Api\Accounts;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\ApiParent;
use App\Models\Company\Acc_branch;
use App\Models\Accounts\Acc_journal;
use App\Models\Accounts\Acc_journal_sub;
use App\Models\Accounts\Acc_voucher;


use Validator;



class JournalController extends ApiParent
{
   
    


    public function JournalOperations(Request $request, $type)
    {

        switch ($type) {
            case 'add':
                return $this->add($request);
                break;
            case 'list':
                return $this->list($request);
                break;
            case 'search_list':
                return $this->searchlist($request);
                break;    
            case 'viewJournal':
                return $this->getJournal($request);
                break;
            case 'next-id':
                   return $this->getNextId($request);
                   break;
            case 'void':
                    return $this->void_journal($request);

            default:
                return response()->json(['error' => 'Invalid Endpoint', 'status' => $this->badrequest_stat]);
                break;
        }
    }



    public function add($request) {
 
        $validator = Validator::make(
            $request->all(),
            [
                
                'rec_date' => 'required',
            
                'ledgers' => 'array|min:1',
                'ledgers.*.ledger.ledger_id.ledger_id' => 'required|numeric|min:1',
                'ledgers.*.ledger.jn_acc_ledId.ledger_id' => 'required|numeric|min:1',

                'ledgers.*.amount' => 'required|numeric|min:0|not_in:0',
            ],[
                'rec_date.required' => 'Date Required',
                'ledgers.min' => 'AtLeast One Ledger Required',

                'ledgers.*.ledger.ledger_id.ledger_id.required' => 'Ledger Required',
                'ledgers.*.ledger.ledger_id.ledger_id.min' => 'Ledger Required',
                'ledgers.*.ledger.ledger_id.ledger_id.numeric' => 'Ledger Required',

                'ledgers.*.ledger.jn_acc_ledId.ledger_id.required' => 'Ledger Required',
                'ledgers.*.ledger.jn_acc_ledId.ledger_id.min' => 'Ledger Required',
                'ledgers.*.ledger.jn_acc_ledId.ledger_id.numeric' => 'Ledger Required',

                'ledgers.*.amount.required' => 'Invalid Amount',
                'ledgers.*.amount.numeric' => 'Invalid Amount',
                'ledgers.*.amount.min' => 'Invalid Amount',
                'ledgers.*.amount.not_in' => 'Invalid Amount',

            ]
        );
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        } 

        $info = array();
        $where = array();
        $where[] = ['branch_id','=', $this->branch_id];
        $branch = Acc_branch::where($where)->first();
        $max = Acc_journal::max('jn_no');
        $info['jn_no'] = (empty($max)) ? 1 : $max+1;
        $where[] = ['is_web','=', 1];
        
        $max = Acc_journal::where($where)->max('branch_jn_no');

        if(empty($max)) {
            $info['branch_jn_no'] = 'W#JN' . $branch['branch_code'] . '000001';
        } else{
            $no = ltrim($max, 'W#JN' . $branch['branch_code']);
            $newNo = +$no + 1;
            $info['branch_jn_no'] = 'W#JN' . $branch['branch_code'] . str_pad($newNo , 6, "0", STR_PAD_LEFT);
        }  
        $branchRecNo =  $info['branch_jn_no'];   
        $data = $request->all();
        $info['jn_date'] = $data['rec_date'];
        $time = $info['jn_time'] = date('H:i:s');
        $info['jn_datetime'] = $info['jn_date'] . ' ' . $info['jn_time'];
        $info['jn_timestamp'] = strtotime($info['jn_date']);

        $info['jn_ttl_amount'] = $data['ttlAmount'];
        $info['jn_note'] = ($data['note']) ? $data['note'] : '';
        $info['branch_id'] = $this->branch_id;
        $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
        $info['is_web'] = 1;
        $info['jn_flags'] = 1;
        $info['sync_completed'] = 1;

        $res = Acc_journal::create($info);

        foreach($data['ledgers'] as $key => $val) {

            $info = array();
            $max = Acc_journal_sub::max('jnsub_no');
            $info['jnsub_no'] = (empty($max)) ? 1 : $max+1;
            $info['jnsub_ref_no'] = $res->jn_no;
            $info['jnsub_date'] = $data['rec_date'];
            $info['jnsub_time'] = $time;
            $info['jnsub_datetime'] = $info['jnsub_date'] . ' ' . $info['jnsub_time'];
            $info['jnsub_timestamp'] = strtotime($data['rec_date']);
            $info['jnsub_acc_ledger_id'] = $val['ledger']['jn_acc_ledId']['ledger_id'];
            $info['jnsub_ledger_id'] = $val['ledger']['ledger_id']['ledger_id'];
            $info['jnsub_amount'] = $val['amount'];

            $info['jnsub_narration'] = ($val['narration']) ? $val['narration'] : '';
            $info['branch_id'] = $this->branch_id;
           
            $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            $info['branch_jn_no'] =  $branchRecNo;
            // vat@5%  comes as deafualt vat category 
            $info['vat_catgeory_id'] =  1;
            $info['jn_flags'] = 1;

            $info['jnsub_ttl_amount'] = $val['amount'];
            $res1 = Acc_journal_sub::create($info);

            $info = array();
            $max = Acc_voucher::max('vch_no');
            $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max+1;
            $info['vch_ledger_from'] = $val['ledger']['ledger_id']['ledger_id'];
            $info['vch_ledger_to'] = $val['ledger']['jn_acc_ledId']['ledger_id'];
            $info['vch_date'] = $data['rec_date'];
            $info['vch_in'] = $val['amount'];
            $info['vch_out'] = 0;
            $info['vch_vchtype_id'] = 3;
            $info['vch_notes'] = ($val['narration']) ? $val['narration'] : '';
            $info['vch_added_by'] = $this->usr_id;
            $info['vch_id2'] = 0;
            $info['vch_from_group'] =  $val['ledger']['ledger_id']['ledger_accgrp_id'];
            $info['vch_flags'] = 1;
            $info['ref_no'] = $res->jn_no;
            $info['branch_id'] = $this->branch_id;
            $info['is_web'] = 1;
            $info['godown_id'] = 0;
            $info['van_id'] = 0;
            $info['branch_ref_no'] = $branchRecNo;
            $info['sub_ref_no'] = $res1->jnsub_no;
            $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            $res2 = Acc_voucher::create($info);

            $info = array();
            $info['vch_no'] = $vouchNo;
            $info['vch_ledger_from'] = $val['ledger']['jn_acc_ledId']['ledger_id'];
            $info['vch_ledger_to'] = $val['ledger']['ledger_id']['ledger_id'];
            $info['vch_date'] = $data['rec_date'];
            $info['vch_in'] = 0;
            $info['vch_out'] = $val['amount'];
            $info['vch_vchtype_id'] = 3;
            $info['vch_notes'] = ($val['narration']) ? $val['narration'] : '';
            $info['vch_added_by'] = $this->usr_id;
            $info['vch_id2'] =  $res2->vch_id;
            $info['vch_from_group'] = $val['ledger']['jn_acc_ledId']['ledger_accgrp_id'];
            $info['vch_flags'] = 1;
            $info['ref_no'] = $res->jn_no;
            $info['branch_id'] = $this->branch_id;
            $info['is_web'] = 1;
            $info['godown_id'] = 0;
            $info['van_id'] = 0;
            $info['branch_ref_no'] = $branchRecNo;
            $info['sub_ref_no'] = $res1->jnsub_no;
            $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            $res2 = Acc_voucher::create($info);
            
        }
        return response()->json(['message' => 'Journal Created Successfully', 'status' => $this->created_stat]);
    }


    public function getJournal($request)
    {

        $rltn = [
            'particulars' => function ($qr) {
  
                $qr->select('journal_sub.*','a.ledger_name as cr_led_name','a.ledger_id as cr_ledid','b.ledger_name as db_led_name','b.ledger_id as b_ledid')
                            ->join('acc_ledgers as a','a.ledger_id','journal_sub.jnsub_ledger_id')
                            ->join('acc_ledgers as b','b.ledger_id','journal_sub.jnsub_acc_ledger_id')
                            ->orderBy('jnsub_ref_no', 'asc');
            },
        ];

        $data = Acc_journal::select('*')
        // ->join('acc_ledgers','ledger_id','journal.jnsub_acc_ledger_id')
        ->with($rltn);

        if($this->branch_id > 0)
        $data = $data->where('journal.branch_id',$this->branch_id);

        $data = $data->where('journal.jn_no',$request['ref_no'])->first();
        // print_r($data);die('f');
        $data['total_amount_in_words'] = $this->getArabicCurrency($data['jn_ttl_amount']);

    return $data;

    }


    public function void_journal($request) {

        $data = $request->all();
       
        $flag = array(
            'jn_flags' => 0,
            'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)
        );
        $where = array();
        $where[] = ['jn_no','=', $data['jn_no']];
        Acc_journal::where($where)->update($flag);


        $flag = array(
            'jn_flags' => 0,
            'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)
        );
        $where = array();
        $where[] = ['jnsub_ref_no','=', $data['jn_no']];
        Acc_journal_sub::where($where)->update($flag);

        $flag = array(
            'vch_flags' => 0,
            'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)
        );
        $where = array();
        $where[] = ['ref_no','=', $data['jn_no']];
        $where[] = ['vch_vchtype_id','=', 3];
        $dueSub = Acc_voucher::where($where)->update($flag);
        return response()->json(['message' => 'Journal Void Successfully', 'status' => $this->created_stat]);
    }





    public function getNextId($request) {
        $max = Acc_journal::max('jn_no');
        $ret['ref_no'] = (empty($max)) ? 1 : $max+1;
        return parent::displayData($ret);
    }

    public function list($request) {
        $where = array();
        $where[] = ['branch_id','=', $this->branch_id];
        $where[] = ['sync_completed','=', 1];
        $where[] = ['jn_flags','=', 1];
        $result = Acc_journal::where($where)->orderBy('jn_id','desc')->paginate($this->per_page);
        return parent::displayData($result);
    }

    public function searchlist($request) {
        $where = array();
        $where[] = ['branch_id','=', $this->branch_id];
        $where[] = ['sync_completed','=', 1];
        $where[] = ['jn_flags','=', 1];
        $orwhere = array();
        $orwhere[] = ['branch_id','=', $this->branch_id];
        $orwhere[] = ['sync_completed','=', 1];
        $orwhere[] = ['jn_flags','=', 1];

if($request['search'] != ''){
    $where[] = ['jn_no','like', '%'.$request['search'].'%'];
}

if($request['search'] != ''){
    $orwhere[] = ['branch_jn_no', 'like', '%' . $request['search'] . '%'];
}

        $result = Acc_journal::where($where)->orWhere($orwhere)->orderBy('jn_id','desc')->paginate($this->per_page);
        return parent::displayData($result);
    }






    function getArabicCurrency(float $number)
    {
        $decimal = round($number - ($no = floor($number)), 2) * 100;
        $hundred = null;
        $digits_length = strlen($no);
        $i = 0;
        $str = array();
        $words = array(0 => '', 1 => 'one', 2 => 'two',
            3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
            7 => 'seven', 8 => 'eight', 9 => 'nine',
            10 => 'ten', 11 => 'eleven', 12 => 'twelve',
            13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
            16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
            19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
            40 => 'forty', 50 => 'fifty', 60 => 'sixty',
            70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
        $digits = array('', 'hundred','thousand','lakh', 'crore');
        while( $i < $digits_length ) {
            $divider = ($i == 2) ? 10 : 100;
            $number = floor($no % $divider);
            $no = floor($no / $divider);
            $i += $divider == 10 ? 1 : 2;
            if ($number) {
                $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
                $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
                $str [] = ($number < 21) ? $words[$number].' '. $digits[$counter]. $plural.' '.$hundred:$words[floor($number / 10) * 10].' '.$words[$number % 10]. ' '.$digits[$counter].$plural.' '.$hundred;
            } else $str[] = null;
        }
        $Rupees = implode('', array_reverse($str));
      
        $paise = ($decimal > 0) ? "and " . ($words[floor($decimal/10)*10] . " " . $words[$decimal % 10]) . ' Halala' : '';
        return ($Rupees ? $Rupees . 'Riyals ' : '') . $paise . ' Only';
    }






}
