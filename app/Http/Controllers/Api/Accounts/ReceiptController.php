<?php

namespace App\Http\Controllers\Api\Accounts;

use App\Http\Controllers\Api\ApiParent;
use App\Models\Accounts\AccReceipt;
use App\Models\Accounts\AccReceiptSub;
use App\Models\Accounts\Acc_ledger;
use App\Models\Accounts\Acc_voucher;
use App\Models\Company\Acc_branch;
use App\Models\Sales\SalesDueSub;
use App\Models\Settings\TaxLedgers;
use App\Models\Accounts\AccPayment;
use Illuminate\Http\Request;
use Validator;

class receiptcontroller extends ApiParent
{
    public function customerOperations(Request $request, $type)
    {

        switch ($type) {
            case 'add':
                return $this->add($request);
                break;
            case 'list':
                return $this->list($request);
                break;
            case 'void':
                return $this->void_receipt($request);
            case 'viewReceipt':
                return $this->getReceipt($request);
                break;
            case 'update':
                return $this->update($request);
                break;
            case 'due_rec_add':
                return $this->due_rec_add($request);
                break;
            case 'due_rec_list':
                return $this->due_rec_list($request);
                break;
            case 'void_due_rec':
                return $this->void_due_rec($request);
                break;
            case 'next-id':
                return $this->getNextId($request);
                break;
            case 'inter_add':
                return $this->interBranchAdd($request);
                break;
            case 'activate':
                //  return $this->activateProductionFormula($request);
                break;

            case 'searchDue':
                return $this->Duesearch($request);
                break;

            default:
                return response()->json(['error' => 'Invalid Endpoint', 'status' => $this->badrequest_stat]);
                break;
        }
    }

    public function getNextId($request)
    {
        $max = AccReceipt::max('rec_no');
        $ret['ref_no'] = (empty($max)) ? 1 : $max + 1;
        return parent::displayData($ret);
    }

    public function add($request)
    {
        // $data = $request->all();
        // echo $data['pay_acc']['ledger_id'];exit;
        // echo '<pre>';
        // var_dump($request->all());
        // exit;
        $validator = Validator::make(
            $request->all(),
            [

                'rec_date' => 'required',
                'rec_acc.ledger_id' => 'required|numeric|min:1',
                'ledgers' => 'array|min:1',
                'ledgers.*.ledger.ledger_id' => 'required|numeric|min:1',
                'ledgers.*.amount' => 'required|numeric|min:0|not_in:0',
            ], [
                'rec_date.required' => 'Date Required',
                'rec_acc.ledger_id.required' => 'Account Ledger Required',
                'rec_acc.ledger_id.numeric' => 'Account Ledger Required',
                'rec_acc.ledger_id.min' => 'Account Ledger Required',

                'ledgers.min' => 'AtLeast One Ledger Required',
                'ledgers.*.ledger.ledger_id.required' => 'Ledger Required',
                'ledgers.*.ledger.ledger_id.min' => 'Ledger Required',
                'ledgers.*.ledger.ledger_id.numeric' => 'Ledger Required',

                'ledgers.*.amount.required' => 'Invalid Amount',
                'ledgers.*.amount.numeric' => 'Invalid Amount',
                'ledgers.*.amount.min' => 'Invalid Amount',
                'ledgers.*.amount.not_in' => 'Invalid Amount',

            ]
        );
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }

        $info = array();
        $where = array();
        $where[] = ['branch_id', '=', $this->branch_id];
        $branch = Acc_branch::where($where)->first();
        $max = AccReceipt::max('rec_no');
        $info['rec_no'] = (empty($max)) ? 1 : $max + 1;
        $where[] = ['is_web', '=', 1];

        $max = AccReceipt::where($where)->max('branch_rec_no');

        if (empty($max)) {
            $info['branch_rec_no'] = 'W#RC' . $branch['branch_code'] . '000001';
        } else {
            $no = ltrim($max, 'W#RC' . $branch['branch_code']);
            $newNo = +$no + 1;
            $info['branch_rec_no'] = 'W#RC' . $branch['branch_code'] . str_pad($newNo, 6, "0", STR_PAD_LEFT);
        }
        $branchRecNo = $info['branch_rec_no'];
        $data = $request->all();
        $info['rec_date'] = $data['rec_date'];
        $time = $info['rec_time'] = date('H:i:s');
        $info['rec_datetime'] = $info['rec_date'] . ' ' . $info['rec_time'];
        $info['rec_timestamp'] = strtotime($info['rec_date']);
        $info['rec_acc_ledger_id'] = $data['rec_acc']['ledger_id'];

        $info['rec_ttl_amount'] = $data['ttlAmount'];
        $info['rec_note'] = ($data['note']) ? $data['note'] : '';
        $info['branch_id'] = $this->branch_id;
        $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
        $info['is_web'] = 1;

        $info['rec_vat_amount'] = $data['ttlVatAmount'];
        $info['rec_gttl_amount'] = $data['grandTtlAmount'];
        $info['rec_vchtype_id'] = 1;
        $info['added_by'] = $this->usr_id;

        $res = AccReceipt::create($info);

        foreach ($data['ledgers'] as $key => $val) {

            $info = array();
            $max = AccReceiptSub::max('recsub_no');
            $info['recsub_no'] = (empty($max)) ? 1 : $max + 1;
            $info['recsub_rec_no'] = $res->rec_no;
            $info['recsub_date'] = $data['rec_date'];
            $info['recsub_time'] = $time;
            $info['recsub_datetime'] = $info['recsub_date'] . ' ' . $info['recsub_time'];
            $info['recsub_timestamp'] = strtotime($data['rec_date']);
            $info['recsub_acc_ledger_id'] = $data['rec_acc']['ledger_id'];
            $info['recsub_ledger_id'] = $val['ledger']['ledger_id'];
            $info['recsub_amount'] = $val['actlAmount'];

            $info['recsub_narration'] = ($val['narration']) ? $val['narration'] : '';
            $info['recsub_vat_per'] = $val['vat_per'];
            $info['recsub_vat_amt'] = $val['vat_amount'];
            $info['branch_id'] = $this->branch_id;
            // $info['van_id'] = $data['rec_date'];
            $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            $info['branch_rec_no'] = $branchRecNo;
            // vat@5%  comes as deafualt vat category
            $info['vat_catgeory_id'] = 1;
            $info['recsub_ttl_amount'] = $val['amount'];
            $info['recsub_vat_inc'] = $val['vatIncExc'];
            $res1 = AccReceiptSub::create($info);

            $info = array();
            $max = Acc_voucher::max('vch_no');
            $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max + 1;
            $info['vch_ledger_from'] = $data['rec_acc']['ledger_id'];
            $info['vch_ledger_to'] = $val['ledger']['ledger_id'];
            $info['vch_date'] = $data['rec_date'];
            $info['vch_in'] = 0;
            $info['vch_out'] = $val['actlAmount'];
            $info['vch_vchtype_id'] = 1;
            $info['vch_notes'] = ($val['narration']) ? $val['narration'] : '';
            $info['vch_added_by'] = $this->usr_id;
            $info['vch_id2'] = 0;
            $info['vch_from_group'] = $data['rec_acc']['ledger_accgrp_id'];
            $info['vch_flags'] = 1;
            $info['ref_no'] = $res->rec_no;
            $info['branch_id'] = $this->branch_id;
            $info['is_web'] = 1;
            $info['godown_id'] = 0;
            $info['van_id'] = 0;
            $info['branch_ref_no'] = $branchRecNo;
            $info['sub_ref_no'] = $res1->recsub_no;
            $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            $res2 = Acc_voucher::create($info);

            $info = array();
            $info['vch_no'] = $vouchNo;
            $info['vch_ledger_from'] = $val['ledger']['ledger_id'];
            $info['vch_ledger_to'] = $data['rec_acc']['ledger_id'];
            $info['vch_date'] = $data['rec_date'];
            $info['vch_in'] = $val['actlAmount'];
            $info['vch_out'] = 0;
            $info['vch_vchtype_id'] = 1;
            $info['vch_notes'] = ($val['narration']) ? $val['narration'] : '';
            $info['vch_added_by'] = $this->usr_id;
            $info['vch_id2'] = $res2->vch_id;
            $info['vch_from_group'] = $val['ledger']['ledger_accgrp_id'];
            $info['vch_flags'] = 1;
            $info['ref_no'] = $res->rec_no;
            $info['branch_id'] = $this->branch_id;
            $info['is_web'] = 1;
            $info['godown_id'] = 0;
            $info['van_id'] = 0;
            $info['branch_ref_no'] = $branchRecNo;
            $info['sub_ref_no'] = $res1->recsub_no;
            $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            $res2 = Acc_voucher::create($info);

        }
        return response()->json(['message' => 'Receipt Created Successfully', 'status' => $this->created_stat]);
    }

    public function interBranchAdd($request) {

        $validator = Validator::make(
            $request->all(),
            [

                'rec_date' => 'required',
                'rec_acc.ledger_id' => 'required|numeric|min:1',
                'amount' => 'required|numeric|min:0|not_in:0',
            ], [
                'rec_date.required' => 'Date Required',
                'rec_acc.ledger_id.required' => 'Account Ledger Required',
                'rec_acc.ledger_id.numeric' => 'Account Ledger Required',
                'rec_acc.ledger_id.min' => 'Account Ledger Required',

                'amount.required' => 'Invalid Amount',
                'amount.numeric' => 'Invalid Amount',
                'amount.min' => 'Invalid Amount',
                'amount.not_in' => 'Invalid Amount',

            ]
        );
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }

        $data = $request->all();
        // payment voucher entry
        $where = array();
        $where[] = ['pay_no','=', $data['pay_no']];
        $where[] = ['pay_inter_branch_id','=', $this->branch_id];
        $payment = AccPayment::select('*')
        ->where($where)
        ->join('acc_payment_sub','acc_payment.pay_no','acc_payment_sub.paysub_pay_no')
        ->first();

        $info = array();
        $where = array();
        $where[] = ['branch_id', '=', $this->branch_id];
        $branch = Acc_branch::where($where)->first();
        $max = AccReceipt::max('rec_no');
        $info['rec_no'] = (empty($max)) ? 1 : $max + 1;
        $where[] = ['is_web', '=', 1];

        $max = AccReceipt::where($where)->max('branch_rec_no');

        if (empty($max)) {
            $info['branch_rec_no'] = 'W#RC' . $branch['branch_code'] . '000001';
        } else {
            $no = ltrim($max, 'W#RC' . $branch['branch_code']);
            $newNo = +$no + 1;
            $info['branch_rec_no'] = 'W#RC' . $branch['branch_code'] . str_pad($newNo, 6, "0", STR_PAD_LEFT);
        }
        $branchRecNo = $info['branch_rec_no'];
       
        $info['rec_date'] = $data['rec_date'];
        $time = $info['rec_time'] = date('H:i:s');
        $info['rec_datetime'] = $info['rec_date'] . ' ' . $info['rec_time'];
        $info['rec_timestamp'] = strtotime($info['rec_date']);
        $info['rec_acc_ledger_id'] = $data['rec_acc']['ledger_id'];

        $info['rec_ttl_amount'] = $data['amount'];
        $info['rec_note'] = ($data['narration']) ? $data['narration'] : '';
        $info['branch_id'] = $this->branch_id;
        $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
        $info['is_web'] = 1;

        $info['rec_vat_amount'] = 0;
        $info['rec_gttl_amount'] = $data['amount'];
        $info['rec_vchtype_id'] = 18;
        $info['added_by'] = $this->usr_id;
        $info['inter_payment_no'] = $data['pay_no'];

        $res = AccReceipt::create($info);

        //to get paid branch branch@branch_code ledger of branch
        $where = array();
        $where[] = ['taxled_taxcat_id','=', 0];
        $where[] = ['taxled_branch_id','=', $payment['branch_id']];
        $where[] = ['taxled_taxcatsub_id','=', 12];
        $branchLedger = TaxLedgers::where($where)->first();


        $info = array();
        $max = AccReceiptSub::max('recsub_no');
        $info['recsub_no'] = (empty($max)) ? 1 : $max + 1;
        $info['recsub_rec_no'] = $res->rec_no;
        $info['recsub_date'] = $data['rec_date'];
        $info['recsub_time'] = $time;
        $info['recsub_datetime'] = $info['recsub_date'] . ' ' . $info['recsub_time'];
        $info['recsub_timestamp'] = strtotime($data['rec_date']);
        $info['recsub_acc_ledger_id'] = $data['rec_acc']['ledger_id'];
        $info['recsub_ledger_id'] = $branchLedger['taxled_ledger_id'];;
        $info['recsub_amount'] = $data['amount'];

        $info['recsub_narration'] = ($data['narration']) ? $data['narration'] : '';
        $info['recsub_vat_per'] = 0;
        $info['recsub_vat_amt'] = 0;
        $info['branch_id'] = $this->branch_id;
        // $info['van_id'] = $data['rec_date'];
        $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
        $info['branch_rec_no'] = $branchRecNo;
        // vat@5%  comes as deafualt vat category
        $info['vat_catgeory_id'] = 3;
        $info['recsub_ttl_amount'] = $data['amount'];
        $info['recsub_vat_inc'] = 0;
        $res1 = AccReceiptSub::create($info);

        $info = array();
        $max = Acc_voucher::max('vch_no');
        $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max + 1;
        $info['vch_ledger_from'] = $data['rec_acc']['ledger_id'];
        $info['vch_ledger_to'] = $branchLedger['taxled_ledger_id'];
        $info['vch_date'] = $data['rec_date'];
        $info['vch_in'] = 0;
        $info['vch_out'] = $data['amount'];
        $info['vch_vchtype_id'] = 1;
        $info['vch_notes'] = ($data['narration']) ? $data['narration'] : '';
        $info['vch_added_by'] = $this->usr_id;
        $info['vch_id2'] = 0;
        $info['vch_from_group'] = $data['rec_acc']['ledger_accgrp_id'];
        $info['vch_flags'] = 1;
        $info['ref_no'] = $res->rec_no;
        $info['branch_id'] = $this->branch_id;
        $info['is_web'] = 1;
        $info['godown_id'] = 0;
        $info['van_id'] = 0;
        $info['branch_ref_no'] = $branchRecNo;
        $info['sub_ref_no'] = $res1->recsub_no;
        $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
        $res2 = Acc_voucher::create($info);

        $info = array();
        $info['vch_no'] = $vouchNo;
        $info['vch_ledger_from'] = $branchLedger['taxled_ledger_id'];
        $info['vch_ledger_to'] = $data['rec_acc']['ledger_id'];
        $info['vch_date'] = $data['rec_date'];
        $info['vch_in'] = $data['amount'];
        $info['vch_out'] = 0;
        $info['vch_vchtype_id'] = 1;
        $info['vch_notes'] = ($data['narration']) ? $data['narration'] : '';
        $info['vch_added_by'] = $this->usr_id;
        $info['vch_id2'] = $res2->vch_id;
        $info['vch_from_group'] = 23;
        $info['vch_flags'] = 1;
        $info['ref_no'] = $res->rec_no;
        $info['branch_id'] = $this->branch_id;
        $info['is_web'] = 1;
        $info['godown_id'] = 0;
        $info['van_id'] = 0;
        $info['branch_ref_no'] = $branchRecNo;
        $info['sub_ref_no'] = $res1->recsub_no;
        $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
        $res2 = Acc_voucher::create($info);

        


        // payment voucher entry for inter branch transaction

        // to get from ledger group
        $where = array();
        $where[] = ['ledger_id','=', $payment['paysub_acc_ledger_id']];
        $ledgerDetails = Acc_ledger::where($where)->first();

        $info = array();
        $max = Acc_voucher::max('vch_no');
        $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max+1;
        $info['vch_ledger_from'] = $payment['paysub_acc_ledger_id'];
        $info['vch_ledger_to'] = $payment['paysub_ledger_id'];
        $info['vch_date'] = $payment['pay_date'];
        $info['vch_in'] = $payment['pay_ttl_amount'];
        $info['vch_out'] = 0;
        $info['vch_vchtype_id'] = 0;
        $info['vch_notes'] = ($payment['pay_note']) ? $payment['pay_note'] : '';
        $info['vch_added_by'] = $payment['added_by'];
        $info['vch_id2'] = 0;
        $info['vch_from_group'] = ($ledgerDetails['ledger_accgrp_id']) ? $ledgerDetails['ledger_accgrp_id']: 0;
        $info['vch_flags'] = 1;
        $info['ref_no'] = $payment['pay_no'];
        $info['branch_id'] = $payment['branch_id'];
        $info['is_web'] = 1;
        $info['godown_id'] = 0;
        $info['van_id'] = 0;
        $info['branch_ref_no'] = $payment['branch_pay_no'];
        $info['sub_ref_no'] = $payment['paysub_no'];
        $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
        $res2 = Acc_voucher::create($info);

        $info = array();
        $info['vch_no'] = $vouchNo;
        $info['vch_ledger_from'] = $payment['paysub_ledger_id'];
        $info['vch_ledger_to'] = $payment['paysub_acc_ledger_id'];
        $info['vch_date'] = $payment['pay_date'];
        $info['vch_in'] = 0;
        $info['vch_out'] = $payment['pay_ttl_amount'];
        $info['vch_vchtype_id'] = 0;
        $info['vch_notes'] = ($payment['pay_note']) ? $payment['pay_note'] : '';
        $info['vch_added_by'] = $payment['added_by'];
        $info['vch_id2'] =  $res2->vch_id;
        $info['vch_from_group'] = 23;
        $info['vch_flags'] = 1;
        $info['ref_no'] = $payment['pay_no'];
        $info['branch_id'] = $payment['branch_id'];
        $info['is_web'] = 1;
        $info['godown_id'] = 0;
        $info['van_id'] = 0;
        $info['branch_ref_no'] = $payment['branch_pay_no'];
        $info['sub_ref_no'] = $payment['paysub_no'];
        $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
        $res2 = Acc_voucher::create($info);

        $flag = array(
            'pay_interbranch_status' => 1,
            'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)
        );
        $where = array();
        $where[] = ['pay_no','=', $data['pay_no']];
        $where[] = ['pay_inter_branch_id','=', $this->branch_id];
        AccPayment::where($where)->update($flag);

        
        return response()->json(['message' => 'Receipt Created Successfully', 'status' => $this->created_stat]);
    }

    public function void_receipt($request)
    {

        $data = $request->all();

        $flag = array(
            'rec_flags' => 0,
            'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6),
        );
        $where = array();
        $where[] = ['rec_no', '=', $data['rec_no']];
        AccReceipt::where($where)->update($flag);

        $flag = array(
            'recsub_flags' => 0,
            'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6),
        );
        $where = array();
        $where[] = ['recsub_rec_no', '=', $data['rec_no']];
        AccReceiptSub::where($where)->update($flag);

        $flag = array(
            'vch_flags' => 0,
            'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6),
        );
        $where = array();
        $where[] = ['ref_no', '=', $data['rec_no']];
        $where[] = ['vch_vchtype_id', '=', 1];
        $dueSub = Acc_voucher::where($where)->update($flag);
        return response()->json(['message' => 'Receipt Void Successfully', 'status' => $this->created_stat]);
    }

    function list($request) {
        $where = array();
        $where[] = ['branch_id', '=', $this->branch_id];
        $where[] = ['sync_completed', '=', 1];
        $where[] = ['rec_flags', '=', 1];

        $key = isset($request->search) ? $request->search : 0;

        if ($key) {
            $result = AccReceipt::where($where)
            
            ->where('rec_no', 'like', '%' . $key . '%')->orWhere('branch_rec_no', 'like', '%' . $key . '%')

            ->with('ledger')->orderBy('rec_id', 'desc')
            
            ->paginate(20, $columns = ['*'], $pageName = 'page', $page = 1);
          
        } else {
            $result = AccReceipt::where($where)->with('ledger')->orderBy('rec_id', 'desc')->paginate($this->per_page);
        }

        
        foreach($result as $k=>$val)
        {
        
            $ledgers = AccReceiptSub::where('recsub_rec_no',$val['rec_no'])->get()->toArray();
            $recsub_ledger_ids= array_column($ledgers,'recsub_ledger_id'); 
            $ledger_name= Acc_ledger::select('ledger_name')->whereIn('ledger_id',$recsub_ledger_ids)->get()->toArray();
          
            $result[$k]['ledger_names']= implode(', ', array_column($ledger_name,'ledger_name')); 
        }

        return parent::displayData($result);
    }

    public function void_due_rec($request)
    {

        $data = $request->all();
        $where = array();
        $where[] = ['rec_no', '=', $data['rec_no']];
        $dueSub = SalesDueSub::where($where)->get();

        foreach ($dueSub as $key => $val) {
            $where = array();
            $where[] = ['salesduesub_ledger_id', '=', $val->salesduesub_ledger_id];
            $where[] = ['branch_inv_no', '=', $val->branch_inv_no];
            $creditDueSub = SalesDueSub::where($where)->whereIn('salesduesub_type', [0, 3])->first();

            $balnce = $creditDueSub['salesduesub_inv_balance'] + $val->salesduesub_in;
            $balance = array(
                'salesduesub_inv_balance' => $balnce,
                'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6),
            );
            SalesDueSub::where($where)->whereIn('salesduesub_type', [0, 3])->update($balance);
        }

        $flag = array(
            'rec_flags' => 0,
            'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6),
        );
        $where = array();
        $where[] = ['rec_no', '=', $data['rec_no']];
        AccReceipt::where($where)->update($flag);

        $flag = array(
            'recsub_flags' => 0,
            'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6),
        );
        $where = array();
        $where[] = ['recsub_rec_no', '=', $data['rec_no']];
        AccReceiptSub::where($where)->update($flag);

        $flag = array(
            'salesduesub_flags' => 0,
            'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6),
        );
        $where = array();
        $where[] = ['rec_no', '=', $data['rec_no']];
        SalesDueSub::where($where)->update($flag);
        // $dueSub = SalesDueSub::where($where)->get();

        $flag = array(
            'vch_flags' => 0,
            'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6),
        );
        $where = array();
        $where[] = ['ref_no', '=', $data['rec_no']];
        $where[] = ['vch_vchtype_id', '=', 22];
        $dueSub = Acc_voucher::where($where)->update($flag);
        return response()->json(['message' => 'Receipt Void Successfully', 'status' => $this->created_stat]);
    }

    public function due_rec_list($request)
    {
        $where = array();
        $where[] = ['acc_receipt.branch_id', '=', $this->branch_id];
        $where[] = ['rec_flags', '=', 1];
        $where[] = ['acc_receipt.sync_completed', '=', 1];
        $where[] = ['rec_vchtype_id', '=', 22];


        $orwhere = array();
        $orwhere[] = ['acc_receipt.branch_id', '=', $this->branch_id];
        $orwhere[] = ['rec_flags', '=', 1];
        $orwhere[] = ['acc_receipt.sync_completed', '=', 1];
        $orwhere[] = ['rec_vchtype_id', '=', 22];

        if($request['search'] != ''){
            $where[] = ['acc_receipt.rec_no','like', '%'.$request['search'].'%'];
        }
        
        if($request['search'] != ''){
            $orwhere[] = ['acc_ledgers.ledger_name', 'like', '%' . $request['search'] . '%'];
        }


        // $rltn = [
        //     'subLedger' => function ($qr) {
  
        //         $qr->select('acc_receipt_sub.recsub_rec_no','a.ledger_name as cust_name')
        //                     ->join('acc_ledgers as a','a.ledger_id','acc_receipt_sub.recsub_ledger_id')
        //                     ->orderBy('recsub_rec_no', 'asc')->groupby('recsub_rec_no');
        //     },
        // ];
        // $result = AccReceipt::where($where)->orderBy('rec_id', 'desc')->paginate(20);

        $key = isset($request->search) ? $request->search : 0;

        if ($key) {

            $result = AccReceipt::select('acc_receipt.*','acc_ledgers.ledger_name')
                ->join('acc_receipt_sub','acc_receipt_sub.recsub_rec_no','acc_receipt.rec_no')
                ->leftjoin('acc_ledgers','acc_ledgers.ledger_id','acc_receipt_sub.recsub_ledger_id')
                ->where($where)
                ->orWhere($orwhere)
                ->orderBy('rec_id', 'desc')
                ->groupby('recsub_rec_no')
                ->paginate(20);
          
        } else {

        $result = AccReceipt::select('acc_receipt.*','acc_ledgers.ledger_name')
            ->join('acc_receipt_sub','acc_receipt_sub.recsub_rec_no','acc_receipt.rec_no')
            ->leftjoin('acc_ledgers','acc_ledgers.ledger_id','acc_receipt_sub.recsub_ledger_id')
            ->where($where)->orderBy('rec_id','desc')
            ->groupby('recsub_rec_no')->paginate(20);
        
        }

        return parent::displayData($result);
    }

    public function due_rec_add($request)
    {

        $validator = Validator::make(
            $request->all(),
            [

                'rec_date' => 'required',
                'rec_acc' => 'required|numeric|min:1',
                'rec_cust_ledger' => 'required|numeric|min:1',
                // 'rec_ttl_amount' => 'required',
                // 'ttl_input_amount' => 'required',
                'invs' => 'array|min:1',
                'invs.*.rec_amt' => 'required|numeric|min:0|not_in:0',
            ], [
                'rec_date.required' => 'Date Required',
                'rec_acc.required' => 'Account Ledger Required',
                'rec_acc.numeric' => 'Account Ledger Required',
                'rec_acc.min' => 'Account Ledger Required',
                'rec_cust_ledger.required' => 'Customer Ledger Required',
                'rec_cust_ledger.numeric' => 'Customer Ledger Required',
                'rec_cust_ledger.min' => 'Customer Ledger Required',
                // 'rec_ttl_amount.required' => 'Total Amount Required',
                // 'ttl_input_amount.required' => 'Customer Name Required',
                'invs.min' => 'AtLeast One Invoice Required',
                'invs.*.rec_amt.required' => 'Amount Required',
                'invs.*.rec_amt.min' => 'Invalid Amount',
                'invs.*.rec_amt.not_in' => 'Invalid Amount',
            ]

        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        } else {

            $data = (object) array('cust_ledger_id' => $request->rec_cust_ledger);
            $balance = app('App\Http\Controllers\Api\Accounts\CustomerController')->getBalance($data, false);
            if ($request->payType == 0) {
                if (!isset($request->ttl_input_amount) || $request->ttl_input_amount == 0 || !is_numeric($request->ttl_input_amount)) {
                    $validator->errors()->add("ttl_input_amount", 'Invalid Amount');
                    return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
                }
                if ($request->ttl_input_amount > $balance) {
                    $validator->errors()->add("ttl_input_amount", 'Invalid Amount');
                    return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
                }
            }
            $info = $request->all();
            $isErr = false;
            foreach ($info['invs'] as $key => $dueSub) {
                $dues = app('App\Http\Controllers\Api\Accounts\CustomerController')->getPendingInvoice($data, false, $dueSub['salesduesub_id']);
                if ($dueSub['rec_amt'] > $dues['salesduesub_inv_balance']) {
                    $isErr = true;
                    $validator->errors()->add("invs.{$key}.rec_amt", 'Invalid Amount');
                }
            }
            if ($isErr) {
                return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
            }

        }
        $info = array();
        $where = array();
        $where[] = ['branch_id', '=', $this->branch_id];
        $branch = Acc_branch::where($where)->first();
        $max = AccReceipt::max('rec_no');
        $info['rec_no'] = (empty($max)) ? 1 : $max + 1;
        $where[] = ['is_web', '=', 1];
        $max = AccReceipt::where($where)->max('branch_rec_no');
        if (empty($max)) {
            $info['branch_rec_no'] = 'W#RC' . $branch['branch_code'] . '000001';
        } else {
            $no = ltrim($max, 'W#RC' . $branch['branch_code']);
            $newNo = +$no + 1;
            $info['branch_rec_no'] = 'W#RC' . $branch['branch_code'] . str_pad($newNo, 6, "0", STR_PAD_LEFT);
        }
        $branchRecNo = $info['branch_rec_no'];
        $data = $request->all();
        $info['rec_date'] = $data['rec_date'];
        $time = $info['rec_time'] = date('H:i:s');
        $info['rec_datetime'] = $info['rec_date'] . ' ' . $info['rec_time'];
        $info['rec_timestamp'] = strtotime($info['rec_date']);
        $info['rec_acc_ledger_id'] = $data['rec_acc'];
        $info['rec_ttl_amount'] = $data['ttl_amount'];
        $info['rec_note'] = ($data['note']) ? $data['note'] : '';
        $info['branch_id'] = $this->branch_id;
        $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
        $info['is_web'] = 1;
        $info['rec_vchtype_id'] = 22;
        $info['added_by'] = $this->usr_id;
        $res = AccReceipt::create($info);

        $info = array();
        $max = Acc_voucher::max('vch_no');
        $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max + 1;
        $ledgDetails = Acc_ledger::find($data['rec_acc']);
        $info['vch_ledger_from'] = $data['rec_acc'];
        $info['vch_ledger_to'] = $data['invs'][0]['salesduesub_ledger_id'];
        $info['vch_date'] = $data['rec_date'];
        $info['vch_in'] = 0;
        $info['vch_out'] = $data['ttl_amount'];
        $info['vch_vchtype_id'] = 22;
        $info['vch_notes'] = 'Sales Due Receipt';
        $info['vch_added_by'] = $this->usr_id;
        $info['vch_id2'] = 0;
        $info['vch_from_group'] = $ledgDetails->ledger_accgrp_id;
        $info['vch_flags'] = 1;
        $info['ref_no'] = $res->rec_no;
        $info['branch_id'] = $this->branch_id;
        $info['is_web'] = 1;
        $info['godown_id'] = 0;
        $info['van_id'] = 0;
        $info['branch_ref_no'] = $branchRecNo;
        $info['sub_ref_no'] = 0;
        $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
        $res2 = Acc_voucher::create($info);

        $info = array();
        $info['vch_no'] = $vouchNo;
        $info['vch_ledger_from'] = $data['invs'][0]['salesduesub_ledger_id'];
        $info['vch_ledger_to'] = $data['rec_acc'];
        $info['vch_date'] = $data['rec_date'];
        $info['vch_in'] = $data['ttl_amount'];
        $info['vch_out'] = 0;
        $info['vch_vchtype_id'] = 22;
        $info['vch_notes'] = 'Sales Due Receipt';
        $info['vch_added_by'] = $this->usr_id;
        $info['vch_id2'] = $res2->vch_id;
        $info['vch_from_group'] = 21;
        $info['vch_flags'] = 1;
        $info['ref_no'] = $res->rec_no;
        $info['branch_id'] = $this->branch_id;
        $info['is_web'] = 1;
        $info['godown_id'] = 0;
        $info['van_id'] = 0;
        $info['branch_ref_no'] = $branchRecNo;
        $info['sub_ref_no'] = 0;
        $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
        $res2 = Acc_voucher::create($info);

        

        // AccReceiptSub
        foreach ($data['invs'] as $key => $val) {
            $info = array();
            $max = AccReceiptSub::max('recsub_no');
            $info['recsub_no'] = (empty($max)) ? 1 : $max + 1;
            $info['recsub_rec_no'] = $res->rec_no;
            $info['recsub_date'] = $data['rec_date'];
            $info['recsub_time'] = $time;
            $info['recsub_datetime'] = $info['recsub_date'] . ' ' . $info['recsub_time'];
            $info['recsub_timestamp'] = strtotime($data['rec_date']);
            $info['recsub_acc_ledger_id'] = $data['rec_acc'];
            $info['recsub_ledger_id'] = $val['salesduesub_ledger_id'];
            $info['recsub_amount'] = $val['rec_amt'];
            // $info['recsub_narration'] =  $data['rec_date'];
            // $info['recsub_vat_per'] =  $data['rec_date'];
            // $info['recsub_vat_amt'] =  $data['rec_date'];
            $info['branch_id'] = $this->branch_id;
            // $info['van_id'] = $data['rec_date'];
            $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            $info['branch_rec_no'] = $branchRecNo;
            $res1 = AccReceiptSub::create($info);

            $info = array();
            $info['salesduesub_salesdue_id'] = 1;
            $info['salesduesub_inv_no'] = $val['salesduesub_inv_no'];
            $info['salesduesub_ledger_id'] = $val['salesduesub_ledger_id'];
            $info['salesduesub_date'] = $data['rec_date'];
            // salesduesub_type =>  0- sales, 1 sales return , 2 payment
            $info['salesduesub_type'] = 2;
            $info['salesduesub_in'] = $val['rec_amt'];
            $info['salesduesub_out'] = 0;
            $info['salesduesub_inv_amount'] = 0;
            $info['salesduesub_inv_balance'] = 0;
            $info['salesduesub_added_by'] = $this->usr_id;
            $info['salesduesub_flags'] = 1;
            $info['salesduesub_agent_ledger_id'] = 0;
            $info['is_web'] = 1;
            $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            $info['sync_completed'] = 1;
            $info['branch_id'] = $this->branch_id;
            $info['rec_no'] = $res->rec_no;
            $info['branch_rec_no'] = $branchRecNo;
            $info['van_inv_no'] = ($val['van_inv_no']) ? $val['van_inv_no'] : '';
            $info['branch_inv_no'] = $val['branch_inv_no'];
            $info['branch_inv_no2'] = $val['branch_inv_no2'];
            $res2 = SalesDueSub::create($info);

            $where = array();
            $where[] = ['salesduesub_id', '=', $val['salesduesub_id']];
            $dueSub = SalesDueSub::where($where)->first();

            $balnce = $dueSub->salesduesub_inv_balance - $val['rec_amt'];
            $balance = array(
                'salesduesub_inv_balance' => $balnce,
                'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6),
            );
            SalesDueSub::where($where)->update($balance);

        }
        return response()->json(['message' => 'Receipt Create Successfully', 'status' => $this->created_stat]);
    }

    public function getReceipt($request)
    {

        $rltn = [
            'particulars' => function ($qr) {

                $qr->select('acc_receipt_sub.*', 'acc_ledgers.ledger_name as led_name', 'acc_ledgers.ledger_id')
                    ->join('acc_ledgers', 'ledger_id', 'acc_receipt_sub.recsub_ledger_id')
                    ->orderBy('recsub_rec_no', 'asc');
            },
        ];

        $data = AccReceipt::select('acc_receipt.*', 'acc_ledgers.ledger_name as acc_led_name')
            ->join('acc_ledgers', 'ledger_id', 'acc_receipt.rec_acc_ledger_id')
            ->with($rltn);

        if ($this->branch_id > 0) {
            $data = $data->where('acc_receipt.branch_id', $this->branch_id);
        }

        $data = $data->where('acc_receipt.rec_no', $request['rec_no'])->first();

        $data['total_amount_in_words'] = $this->getArabicCurrency($data['rec_ttl_amount']);

        return $data;

    }

    public function getArabicCurrency(float $number)
    {
        $decimal = round($number - ($no = floor($number)), 2) * 100;
        $hundred = null;
        $digits_length = strlen($no);
        $i = 0;
        $str = array();
        $words = array(0 => '', 1 => 'one', 2 => 'two',
            3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
            7 => 'seven', 8 => 'eight', 9 => 'nine',
            10 => 'ten', 11 => 'eleven', 12 => 'twelve',
            13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
            16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
            19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
            40 => 'forty', 50 => 'fifty', 60 => 'sixty',
            70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
        $digits = array('', 'hundred', 'thousand', 'lakh', 'crore');
        while ($i < $digits_length) {
            $divider = ($i == 2) ? 10 : 100;
            $number = floor($no % $divider);
            $no = floor($no / $divider);
            $i += $divider == 10 ? 1 : 2;
            if ($number) {
                $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
                $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
                $str[] = ($number < 21) ? $words[$number] . ' ' . $digits[$counter] . $plural . ' ' . $hundred : $words[floor($number / 10) * 10] . ' ' . $words[$number % 10] . ' ' . $digits[$counter] . $plural . ' ' . $hundred;
            } else {
                $str[] = null;
            }

        }
        $Rupees = implode('', array_reverse($str));

        $paise = ($decimal > 0) ? "and " . ($words[floor($decimal / 10) * 10] . " " . $words[$decimal % 10]) . ' Halala' : '';
        return ($Rupees ? $Rupees . 'Riyals ' : '') . $paise . ' Only';
    }

}
