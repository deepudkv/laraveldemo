<?php

namespace App\Http\Controllers\Api\Accounts;

use App\Http\Controllers\Api\ApiParent;
use App\Models\Accounts\price_group;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class PriceGroupController extends ApiParent
{

    // ===================== Code Added By Deepu =======================

    public function priceGroupOperations (Request $request, $type)
    {

        switch ($type) {
            case 'add':
                return $this->add($request);
                break;
            case 'list_all':
                return $this->listAll($request);
                break;
            // case 'update':
            //     return $this->updateVan($request);
            //     break;

            // case 'activate':
            //     return $this->activateVan($request);
            //     break;

                // case 'search':
                // return $this->searchVan($request);
                // break;

            default:
                return response()->json(['error' => 'Invalid Endpoint', 'status' => $this->badrequest_stat]);
                break;
        }
    }

    /**
     * @api {post} http://127.0.0.1:8000/api/godown/add  Add Godown
     * @apiName addGodown
     * @apiGroup Godown
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {String} gd_name Godown Name (Required)
     * @apiParam {String} gd_code Godown Code
     * @apiParam {String} gd_cntct_num Godown contact Number
     * @apiParam {String} gd_cntct_person Godown contact person
     * @apiParam {String} gd_address Godown address
     * @apiParam {String} gd_desc Godown Description
     *
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} message Success Message
     *
     * @apiError {String} error Error Message.
     *
     */

    public function add ($request)
    {

       
    }

 
    public function listAll()
    {
        return parent::displayData(price_group::get());
    }

}
