<?php

namespace App\Http\Controllers\Api\Accounts;

use App\Http\Controllers\Api\ApiParent;
use App\Models\Accounts\Acc_group;
use DB;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use validator;

class LedgerGroupController extends ApiParent
{

    public function addLedgroup(Request $request)
    {

        $request->request->add(['accgrp_edit' => 1, 'accgrp_flags' => 1, 'accgrp_enable_ledger' => 1]);
        if ($request->accgrp_type == '1') {
            $validator = Validator::make(
                $request->all(),
                [
                    'accgrp_name' => 'required',
                    'accgrp_type' => 'required',
                    'accgrp_nature' => 'required_if:accgrp_type,1',

                ],
                [
                    'accgrp_name.required' => 'Required',
                    'accgrp_type.required' => 'Required',
                    'accgrp_nature.required' => 'Required',
                ]
            );

            if ($validator->fails()) {
                return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
            }

            unset($request['accgrp_type']);

            $data = $request->all();

            unset($data['active_branch_id']);
            $data['accgrp_root'] = 0;
            $data['accgrp_parent'] = 0;

            $result = Acc_group::create($data);
            return response()->json(['message' => 'saved_successfully', 'status' => $this->created_stat]);
        } else {

            $validator = Validator::make(
                $request->all(),
                [
                    'accgrp_name' => 'required',
                    'accgrp_type' => 'required',
                    // 'accgrp_nature' => 'required',
                    'accgrp_parent' => 'required',

                ],
                [
                    'accgrp_name.required' => 'Required',
                    'accgrp_type.required' => 'Required',
                    // 'accgrp_nature.required' => 'Required',
                    'accgrp_parent.required' => 'Required',
                ]
            );

            if ($validator->fails()) {
                return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
            }

            unset($request['accgrp_type']);
            $data = $request->all();
            unset($data['active_branch_id']);

            $data['accgrp_root'] = $request->accgrp_parent;

            $info = Acc_group::where('accgrp_id', $data['accgrp_parent'])->first();
            $data['accgrp_nature'] = $info['accgrp_nature'];
            $data['accgrp_depth'] = $info['accgrp_depth'] + 1;

            Acc_group::create($data);
            return response()->json(['message' => 'saved_successfully', 'status' => $this->created_stat]);
        }
    }

    public function editLedgroup(Request $request)
    {

        $request->request->add(['accgrp_edit' => 1, 'accgrp_flags' => 1, 'accgrp_enable_ledger' => 1]);

        if ($request->accgrp_type == '1') {
            $validator = Validator::make(
                $request->all(),
                [

                    'accgrp_name' => ['required', Rule::unique('acc_groups')->ignore($request['accgrp_id'], 'accgrp_id')],
                    'accgrp_type' => 'required',

                ],
                [
                    'accgrp_name.required' => 'Required',
                    'accgrp_type.required' => 'Required',

                ]
            );

            if ($validator->fails()) {
                return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
            }

            unset($request['accgrp_type']);
            $input = $request->all();
            unset($input['branch_id']);
            $data = Acc_group::find($request->accgrp_id);
            $input['accgrp_root'] = 0;
            $input['accgrp_parent'] = 0;
            $data->fill($input)->save();
            return response()->json(['message' => 'updated_successfully', 'status' => $this->created_stat]);
        } else {

            $validator = Validator::make(
                $request->all(),
                [
                    'accgrp_name' => 'required',
                    'accgrp_type' => 'required',

                    'accgrp_parent' => 'required',

                ],
                [
                    'accgrp_name.required' => 'Required',
                    'accgrp_type.required' => 'Required',

                    'accgrp_parent.required' => 'Required',
                ]
            );

            if ($validator->fails()) {
                return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
            }

            unset($request['accgrp_type']);
            $input = $request->all();
            unset($input['branch_id']);
            $accgrp_nature = Acc_group::where('accgrp_id', $request->accgrp_parent)->first();
            $input['accgrp_nature'] = $accgrp_nature['accgrp_nature'];
            $input['accgrp_root'] = $request->accgrp_parent;
            $data = Acc_group::find($request->accgrp_id);
            $data->fill($input)->save();
            return response()->json(['message' => 'updated_successfully', 'status' => $this->created_stat]);
        }
    }

    /**
     * @api {get} http://127.0.0.1:8000/api/get_primary_ledgroup  Get Primary Ledger Group Details
     * @apiName getPrimaryLedgergroup
     * @apiGroup Accounts
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} data Primary Ledger Group Data
     *
     */

    public function getLedgergroup()
    {

        return response()->json(['data' => Acc_group::get(), 'status' => $this->success_stat]);
    }

    /**
     * @api {Post} http://127.0.0.1:8000/api/get_all_ledgroup  Search Ledger Group
     * @apiName getAllLedgergroup
     * @apiGroup Accounts
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {String} accgrp_name Ledger Group Keyword
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} data All Ledger Group Data
     *
     */

    public function getPrimaryLedgergroup()
    {
        $primary_ledgroups = Acc_group::where('accgrp_depth', 0)
            ->select('*')->where('accgrp_flags', 1)
            ->get();
        return response()->json(['data' => $primary_ledgroups, 'status' => $this->success_stat]);
    }

    public function getAllLedgergroup(Request $request)
    {
        if ($request['accgrp_name'] != "") {
            $acc_groups = Acc_group::where('accgrp_name', 'like', '%' . $request['accgrp_name'] . '%')
                ->select('accgrp_id', 'accgrp_name', 'accgrp_nature', 'accgrp_parent', 'accgrp_root', 'accgrp_depth', 'accgrp_edit', 'accgrp_flags', 'accgrp_enable_ledger', 'accgrp_code', 'company_id', DB::raw('(CASE WHEN accgrp_depth = 0 THEN 1 ELSE 0 END) AS primary_status'))
                ->take(50)
                ->get();
        } else {
            $acc_groups = Acc_group::select('accgrp_id', 'accgrp_name', 'accgrp_nature', 'accgrp_parent', 'accgrp_root', 'accgrp_depth', 'accgrp_edit', 'accgrp_flags', 'accgrp_enable_ledger', 'accgrp_code', 'company_id', DB::raw('(CASE WHEN accgrp_depth = 0 THEN 1 ELSE 0 END) AS primary_status'))
                ->take(50)
                ->get();
        }

        foreach ($acc_groups as $k => $v) {
            $acc_groups[$k]['nature'] = '';
      
                $arr = Acc_group::where('accgrp_parent', $v['accgrp_id'])->first();
                if (!empty($arr)) {
                    $acc_groups[$k]['nature'] = $arr->accgrp_name;
                }
         
        }

        return response()->json(['data' => $acc_groups, 'status' => $this->success_stat]);
    }

    public function getAllEditableLedgergroup(Request $request)
    {
        if ($request['accgrp_name'] != "") {
            $acc_groups = Acc_group::where('accgrp_name', 'like', '%' . $request['accgrp_name'] . '%')
                ->where('accgrp_edit', 1)
                ->select('accgrp_id', 'accgrp_name', 'accgrp_nature', 'accgrp_parent', 'accgrp_root', 'accgrp_depth', 'accgrp_edit', 'accgrp_flags', 'accgrp_enable_ledger', 'accgrp_code', 'company_id', DB::raw('(CASE WHEN accgrp_depth = 0 THEN 1 ELSE 0 END) AS primary_status'))
                ->take(50)
                ->get();
        } else {
            $acc_groups = Acc_group::select('accgrp_id', 'accgrp_name', 'accgrp_nature', 'accgrp_parent', 'accgrp_root', 'accgrp_depth', 'accgrp_edit', 'accgrp_flags', 'accgrp_enable_ledger', 'accgrp_code', 'company_id', DB::raw('(CASE WHEN accgrp_depth = 0 THEN 1 ELSE 0 END) AS primary_status'))
                ->where('accgrp_edit', 1)
                ->take(50)
                ->get();
        }

        foreach ($acc_groups as $k => $v) {
            $acc_groups[$k]['accgrp_editable'] = 0;
            if ($v['accgrp_root'] == 0) {
                $arr = Acc_group::where('accgrp_parent', $v['accgrp_id'])->first();
                if (empty($arr)) {
                    $acc_groups[$k]['accgrp_editable'] = 1;
                }
            } else {
                $acc_groups[$k]['accgrp_editable'] = 1;
            }
        }

        return response()->json(['data' => $acc_groups, 'status' => $this->success_stat]);
    }

}
