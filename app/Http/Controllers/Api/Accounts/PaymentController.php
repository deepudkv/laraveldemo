<?php

namespace App\Http\Controllers\Api\Accounts;

use App\Http\Controllers\Api\ApiParent;
use App\Models\Accounts\AccPayment;
use App\Models\Accounts\AccPaymentSub;
use App\Models\Accounts\Acc_voucher;
use App\Models\Company\Acc_branch;
use App\Models\Accounts\Acc_ledger;
use App\Models\Settings\TaxLedgers;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class paymentcontroller extends ApiParent
{
    public function customerOperations(Request $request, $type)
    {

        switch ($type) {
            case 'add':
                return $this->add($request);
                break;
            case 'list':
                return $this->list($request);
                break;
            case 'void':
                return $this->void_payment($request);
                break;
            case 'item':
                return $this->getCustomer($request);
                break;
            case 'update':
                return $this->updateCustomer($request);
                break;
            case 'next-id':
                return $this->getNextId($request);
                break;
                // inter_add
            case 'inter_add':
                return $this->interBranchAdd($request);
                break;    
            case 'inter_list':
                return $this->interList($request);
                break;
            case 'inter_rec_list':
                return $this->interReceiptList($request);
                break;
            case 'inter_reject':
                return $this->interPaymentReject($request);
                break;
            case 'inter_update':
                return $this->interPaymentUpdateOrResubmit($request);
                break;
            case 'activate':
                //  return $this->activateProductionFormula($request);
                break;

            case 'search':
                return $this->searchPayment($request);
                break;
            case 'viewPayment':
                return $this->getPayment($request);
                break;    

            default:
                return response()->json(['error' => 'Invalid Endpoint', 'status' => $this->badrequest_stat]);
                break;
        }
    }

    public function getNextId($request) {
        $max = AccPayment::max('pay_no');
        $ret['ref_no'] = (empty($max)) ? 1 : $max+1;
        return parent::displayData($ret);

    }
    public function add($request) {
        // $data = $request->all();    
        // echo $data['pay_acc']['ledger_id'];exit;
        // echo '<pre>';
        // var_dump($request->all());
        // exit;
        $validator = Validator::make(
            $request->all(),
            [
                
                'pay_date' => 'required',
                'pay_acc.ledger_id' => 'required|numeric|min:1',
                'ledgers' => 'array|min:1',
                'ledgers.*.ledger.ledger_id' => 'required|numeric|min:1',
                'ledgers.*.amount' => 'required|numeric|min:0|not_in:0',
            ],[
                'pay_date.required' => 'Date Required',
                'pay_acc.ledger_id.required' => 'Account Ledger Required',
                'pay_acc.ledger_id.numeric' => 'Account Ledger Required',
                'pay_acc.ledger_id.min' => 'Account Ledger Required',

                'ledgers.min' => 'AtLeast One Ledger Required',
                'ledgers.*.ledger.ledger_id.required' => 'Ledger Required',
                'ledgers.*.ledger.ledger_id.min' => 'Ledger Required',
                'ledgers.*.ledger.ledger_id.numeric' => 'Ledger Required',

                'ledgers.*.amount.required' => 'Invalid Amount',
                'ledgers.*.amount.numeric' => 'Invalid Amount',
                'ledgers.*.amount.min' => 'Invalid Amount',
                'ledgers.*.amount.not_in' => 'Invalid Amount',

            ]
        );
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        } 

        $info = array();
        $where = array();
        $where[] = ['branch_id','=', $this->branch_id];
        $branch = Acc_branch::where($where)->first();
        $max = AccPayment::max('pay_no');
        $info['pay_no'] = (empty($max)) ? 1 : $max+1;
        $where[] = ['is_web','=', 1];
        
        $max = AccPayment::where($where)->max('branch_pay_no');

        if(empty($max)) {
            $info['branch_pay_no'] = 'W#P' . $branch['branch_code'] . '000001';
        } else{
            $no = ltrim($max, 'W#P' . $branch['branch_code']);
            $newNo = +$no + 1;
            $info['branch_pay_no'] = 'W#P' . $branch['branch_code'] . str_pad($newNo , 6, "0", STR_PAD_LEFT);
        }  
        $branchPayNo =  $info['branch_pay_no'];   
        $data = $request->all();
        $info['pay_date'] = $data['pay_date'];
        $time = $info['pay_time'] = date('H:i:s');
        $info['pay_datetime'] = $info['pay_date'] . ' ' . $info['pay_time'];
        $info['pay_timestamp'] = strtotime($info['pay_date']);
        $info['pay_acc_ledger_id'] = $data['pay_acc']['ledger_id'];

        $info['pay_ttl_amount'] = $data['ttlAmount'];
        $info['pay_note'] = ($data['note']) ? $data['note'] : '';
        $info['branch_id'] = $this->branch_id;
        $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
        $info['is_web'] = 1;

        $info['pay_vat_amount'] = $data['ttlVatAmount'];
        $info['pay_gttl_amount'] = $data['grandTtlAmount'];
        $info['added_by'] = $this->usr_id;
     
        $res = AccPayment::create($info);

        foreach($data['ledgers'] as $key => $val) {

            $info = array();
            $max = AccPaymentSub::max('paysub_no');
            $info['paysub_no'] = (empty($max)) ? 1 : $max+1;
            $info['paysub_pay_no'] = $res->pay_no;
            $info['paysub_date'] = $data['pay_date'];
            $info['paysub_time'] = $time;
            $info['paysub_datetime'] = $info['paysub_date'] . ' ' . $info['paysub_time'];
            $info['paysub_timestamp'] = strtotime($data['pay_date']);
            $info['paysub_acc_ledger_id'] = $data['pay_acc']['ledger_id'];
            $info['paysub_ledger_id'] = $val['ledger']['ledger_id'];
            $info['paysub_amount'] = $val['actlAmount'];

            $info['paysub_narration'] = ($val['narration']) ? $val['narration'] : '';
            $info['paysub_vat_per'] = $val['vat_per'];
            $info['paysub_vat_amt'] = $val['vat_amount'];
            $info['branch_id'] = $this->branch_id;
            // $info['van_id'] = $data['pay_date'];
            $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            $info['branch_pay_no'] =  $branchPayNo;
            // vat@5%  comes as deafualt vat category 
            $info['vat_catgeory_id'] =  1;
            $info['paysub_ttl_amount'] = $val['amount'];
            $info['paysub_vat_inc'] = $val['vatIncExc'];
            $res1 = AccPaymentSub::create($info);

            $info = array();
            $max = Acc_voucher::max('vch_no');
            $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max+1;
            $info['vch_ledger_from'] = $data['pay_acc']['ledger_id'];
            $info['vch_ledger_to'] = $val['ledger']['ledger_id'];
            $info['vch_date'] = $data['pay_date'];
            $info['vch_in'] = $val['actlAmount'];
            $info['vch_out'] = 0;
            $info['vch_vchtype_id'] = 0;
            $info['vch_notes'] = ($val['narration']) ? $val['narration'] : '';
            $info['vch_added_by'] = $this->usr_id;
            $info['vch_id2'] = 0;
            $info['vch_from_group'] = $data['pay_acc']['ledger_accgrp_id'];
            $info['vch_flags'] = 1;
            $info['ref_no'] = $res->pay_no;
            $info['branch_id'] = $this->branch_id;
            $info['is_web'] = 1;
            $info['godown_id'] = 0;
            $info['van_id'] = 0;
            $info['branch_ref_no'] = $branchPayNo;
            $info['sub_ref_no'] = $res1->paysub_no;
            $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            $res2 = Acc_voucher::create($info);

            $info = array();
            $info['vch_no'] = $vouchNo;
            $info['vch_ledger_from'] = $val['ledger']['ledger_id'];
            $info['vch_ledger_to'] = $data['pay_acc']['ledger_id'];
            $info['vch_date'] = $data['pay_date'];
            $info['vch_in'] = 0;
            $info['vch_out'] = $val['actlAmount'];
            $info['vch_vchtype_id'] = 0;
            $info['vch_notes'] = ($val['narration']) ? $val['narration'] : '';
            $info['vch_added_by'] = $this->usr_id;
            $info['vch_id2'] =  $res2->vch_id;
            $info['vch_from_group'] = $val['ledger']['ledger_accgrp_id'];
            $info['vch_flags'] = 1;
            $info['ref_no'] = $res->pay_no;
            $info['branch_id'] = $this->branch_id;
            $info['is_web'] = 1;
            $info['godown_id'] = 0;
            $info['van_id'] = 0;
            $info['branch_ref_no'] = $branchPayNo;
            $info['sub_ref_no'] = $res1->paysub_no;
            $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            $res2 = Acc_voucher::create($info);
            
        }
        return response()->json(['message' => 'Inter Branch Payment Created Successfully', 'status' => $this->created_stat]);
    }

    public function interBranchAdd($request) {
        $validator = Validator::make(
            $request->all(),
            [
                
                'pay_date' => 'required',
                'pay_acc.ledger_id' => 'required|numeric|min:1',
                'pay_branch_id' => 'required|numeric|min:1',
                'amount' => 'required|numeric|min:0|not_in:0',
            ],[
                'pay_date.required' => 'Date Required',
                'pay_acc.ledger_id.required' => 'Account Ledger Required',
                'pay_acc.ledger_id.numeric' => 'Account Ledger Required',
                'pay_acc.ledger_id.min' => 'Account Ledger Required',

                'pay_branch_id.required' => 'Invalid Branch',
                'pay_branch_id.numeric' => 'Invalid Branch',
                'pay_branch_id.min' => 'Invalid Branch',

                'amount.required' => 'Invalid Amount',
                'amount.numeric' => 'Invalid Amount',
                'amount.min' => 'Invalid Amount',
                'amount.not_in' => 'Invalid Amount',

            ]
        );
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        } 

        $info = array();
        $where = array();
        $where[] = ['branch_id','=', $this->branch_id];
        $branch = Acc_branch::where($where)->first();
        $max = AccPayment::max('pay_no');
        $info['pay_no'] = (empty($max)) ? 1 : $max+1;
        $where[] = ['is_web','=', 1];
        
        $max = AccPayment::where($where)->max('branch_pay_no');

        if(empty($max)) {
            $info['branch_pay_no'] = 'W#P' . $branch['branch_code'] . '000001';
        } else{
            $no = ltrim($max, 'W#P' . $branch['branch_code']);
            $newNo = +$no + 1;
            $info['branch_pay_no'] = 'W#P' . $branch['branch_code'] . str_pad($newNo , 6, "0", STR_PAD_LEFT);
        }  
        $branchPayNo = $info['branch_pay_no'];   
        $data = $request->all();
        $info['pay_date'] = $data['pay_date'];
        $time = $info['pay_time'] = date('H:i:s');
        $info['pay_datetime'] = $info['pay_date'] . ' ' . $info['pay_time'];
        $info['pay_timestamp'] = strtotime($info['pay_date']);
        $info['pay_acc_ledger_id'] = $data['pay_acc']['ledger_id'];

        $info['pay_ttl_amount'] = $data['amount'];
        $info['pay_note'] = ($data['narration']) ? $data['narration'] : '';
        $info['branch_id'] = $this->branch_id;
        $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
        $info['is_web'] = 1;

        $info['pay_vat_amount'] = 0;
        $info['pay_gttl_amount'] = $data['amount'];
        $info['pay_vchtype_id'] = 0;
        $info['pay_interbranch_status'] = 0;
        $info['pay_inter_branch_id'] = $data['pay_branch_id'];
        $info['added_by'] = $this->usr_id;
        $info['is_inter_branch'] = 1;
     
        $res = AccPayment::create($info);

        //to get branch@branch_code ledger of selected branch
        $where = array();
        $where[] = ['taxled_taxcat_id','=', 0];
        $where[] = ['taxled_branch_id','=', $data['pay_branch_id']];
        $where[] = ['taxled_taxcatsub_id','=', 12];
        $branchLedger = TaxLedgers::where($where)->first();
        

        $info = array();
        $max = AccPaymentSub::max('paysub_no');
        $info['paysub_no'] = (empty($max)) ? 1 : $max+1;
        $info['paysub_pay_no'] = $res->pay_no;
        $info['paysub_date'] = $data['pay_date'];
        $info['paysub_time'] = $time;
        $info['paysub_datetime'] = $info['paysub_date'] . ' ' . $info['paysub_time'];
        $info['paysub_timestamp'] = strtotime($data['pay_date']);
        $info['paysub_acc_ledger_id'] = $data['pay_acc']['ledger_id'];
        $info['paysub_ledger_id'] = $branchLedger['taxled_ledger_id'];
        $info['paysub_amount'] = $data['amount'];

        $info['paysub_narration'] = ($data['narration']) ? $data['narration'] : '';
        $info['paysub_vat_per'] = 0;
        $info['paysub_vat_amt'] = 0;
        $info['branch_id'] = $this->branch_id;
        // $info['van_id'] = $data['pay_date'];
        $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
        $info['branch_pay_no'] =  $branchPayNo;
        // vat@5%  comes as deafualt vat category 
        $info['vat_catgeory_id'] =  3;
        $info['paysub_ttl_amount'] = $data['amount'];
        $info['paysub_vat_inc'] = 0;
        $res1 = AccPaymentSub::create($info);

        // $info = array();
        // $max = Acc_voucher::max('vch_no');
        // $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max+1;
        // $info['vch_ledger_from'] = $data['pay_acc']['ledger_id'];
        // $info['vch_ledger_to'] = $branchLedger['taxled_ledger_id'];
        // $info['vch_date'] = $data['pay_date'];
        // $info['vch_in'] = $data['amount'];
        // $info['vch_out'] = 0;
        // $info['vch_vchtype_id'] = 0;
        // $info['vch_notes'] = ($data['narration']) ? $data['narration'] : '';
        // $info['vch_added_by'] = $this->usr_id;
        // $info['vch_id2'] = 0;
        // $info['vch_from_group'] = $data['pay_acc']['ledger_accgrp_id'];
        // $info['vch_flags'] = 1;
        // $info['ref_no'] = $res->pay_no;
        // $info['branch_id'] = $this->branch_id;
        // $info['is_web'] = 1;
        // $info['godown_id'] = 0;
        // $info['van_id'] = 0;
        // $info['branch_ref_no'] = $branchPayNo;
        // $info['sub_ref_no'] = $res1->paysub_no;
        // $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
        // $res2 = Acc_voucher::create($info);

        // $info = array();
        // $info['vch_no'] = $vouchNo;
        // $info['vch_ledger_from'] = $branchLedger['taxled_ledger_id'];
        // $info['vch_ledger_to'] = $data['pay_acc']['ledger_id'];
        // $info['vch_date'] = $data['pay_date'];
        // $info['vch_in'] = 0;
        // $info['vch_out'] = $data['amount'];
        // $info['vch_vchtype_id'] = 0;
        // $info['vch_notes'] = ($data['narration']) ? $data['narration'] : '';
        // $info['vch_added_by'] = $this->usr_id;
        // $info['vch_id2'] =  $res2->vch_id;
        // $info['vch_from_group'] = 23;
        // $info['vch_flags'] = 1;
        // $info['ref_no'] = $res->pay_no;
        // $info['branch_id'] = $this->branch_id;
        // $info['is_web'] = 1;
        // $info['godown_id'] = 0;
        // $info['van_id'] = 0;
        // $info['branch_ref_no'] = $branchPayNo;
        // $info['sub_ref_no'] = $res1->paysub_no;
        // $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
        // $res2 = Acc_voucher::create($info);

        return response()->json(['message' => 'Payment Created Successfully', 'status' => $this->created_stat]);
    }

    public function interPaymentUpdateOrResubmit($request) {
        $validator = Validator::make(
            $request->all(),
            [
                
                'pay_date' => 'required',
                'pay_acc.ledger_id' => 'required|numeric|min:1',
                'pay_branch_id' => 'required|numeric|min:1',
                'amount' => 'required|numeric|min:0|not_in:0',
            ],[
                'pay_date.required' => 'Date Required',
                'pay_acc.ledger_id.required' => 'Account Ledger Required',
                'pay_acc.ledger_id.numeric' => 'Account Ledger Required',
                'pay_acc.ledger_id.min' => 'Account Ledger Required',

                'pay_branch_id.required' => 'Invalid Branch',
                'pay_branch_id.numeric' => 'Invalid Branch',
                'pay_branch_id.min' => 'Invalid Branch',

                'amount.required' => 'Invalid Amount',
                'amount.numeric' => 'Invalid Amount',
                'amount.min' => 'Invalid Amount',
                'amount.not_in' => 'Invalid Amount',

            ]
        );
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        } 

        $info = array();
        $data = $request->all();
        $info['pay_date'] = $data['pay_date'];
        $time = $info['pay_time'] = date('H:i:s');
        $info['pay_datetime'] = $info['pay_date'] . ' ' . $info['pay_time'];
        $info['pay_timestamp'] = strtotime($info['pay_date']);
        $info['pay_acc_ledger_id'] = $data['pay_acc']['ledger_id'];

        $info['pay_ttl_amount'] = $data['amount'];
        $info['pay_note'] = ($data['narration']) ? $data['narration'] : '';
        $info['branch_id'] = $this->branch_id;
        $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
        $info['pay_gttl_amount'] = $data['amount'];
        $info['pay_interbranch_status'] = 0;
        $info['pay_inter_branch_id'] = $data['pay_branch_id'];
        $info['added_by'] = $this->usr_id;

        $where = array();
        $where[] = ['pay_no', '=', $data['pay_no']];
        AccPayment::where($where)->update($info);


        $info = array();
        //to get branch@branch_code ledger of selected branch
        $where = array();
        $where[] = ['taxled_taxcat_id','=', 0];
        $where[] = ['taxled_branch_id','=', $data['pay_branch_id']];
        $where[] = ['taxled_taxcatsub_id','=', 12];
        $branchLedger = TaxLedgers::where($where)->first();
        

        $info['paysub_date'] = $data['pay_date'];
        $info['paysub_time'] = $time;
        $info['paysub_datetime'] = $info['paysub_date'] . ' ' . $info['paysub_time'];
        $info['paysub_timestamp'] = strtotime($data['pay_date']);
        $info['paysub_acc_ledger_id'] = $data['pay_acc']['ledger_id'];
        $info['paysub_ledger_id'] = $branchLedger['taxled_ledger_id'];
        $info['paysub_amount'] = $data['amount'];

        $info['paysub_narration'] = ($data['narration']) ? $data['narration'] : '';
        $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
        $info['paysub_ttl_amount'] = $data['amount'];

        $where = array();
        $where[] = ['paysub_pay_no', '=', $data['pay_no']];
        AccPaymentSub::where($where)->update($info);
        $res1 = AccPaymentSub::create($info);
        $retMsg = ($data['is_resubmit']) ? 'Resumbitted' : 'Updated';
        return response()->json(['message' => 'Payment ' . $retMsg .' Successfully', 'status' => $this->created_stat]);
    }

    public function interPaymentReject($request) {

        $data = $request->all();
       
        $flag = array(
            'pay_interbranch_status' => 2,
            'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)
        );
        $where = array();
        $where[] = ['pay_no','=', $data['pay_no']];
        AccPayment::where($where)->update($flag);

        return response()->json(['message' => 'Payment Rejected Successfully', 'status' => $this->created_stat]);
    }
    public function void_payment($request) {

        $data = $request->all();
       
        $flag = array(
            'pay_flags' => 0,
            'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)
        );
        $where = array();
        $where[] = ['pay_no','=', $data['pay_no']];
        AccPayment::where($where)->update($flag);


        $flag = array(
            'paysub_flags' => 0,
            'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)
        );
        $where = array();
        $where[] = ['paysub_pay_no','=', $data['pay_no']];
        AccPaymentSub::where($where)->update($flag);

        $flag = array(
            'vch_flags' => 0,
            'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)
        );
        $where = array();
        $where[] = ['ref_no','=', $data['pay_no']];
        $where[] = ['vch_vchtype_id','=', 0];
        Acc_voucher::where($where)->update($flag);
        return response()->json(['message' => 'Payment Void Successfully', 'status' => $this->created_stat]);
    }

    public function list($request) {
        $where = array();
        $where[] = ['acc_payment.branch_id','=', $this->branch_id];
        $where[] = ['pay_flags','=', 1];
        $where[] = ['pay_interbranch_status','=', 1];

        $rltn = [
            'particulars' => function ($qr) {
  
                $qr->select('acc_payment_sub.paysub_pay_no','a.ledger_name as led_name')
                            ->join('acc_ledgers as a','a.ledger_id','acc_payment_sub.paysub_ledger_id')
                            ->orderBy('paysub_pay_no', 'asc')->groupby('paysub_pay_no');
            },
        ];


        $result = AccPayment::select('*')->with($rltn)->where($where)->orderBy('pay_id','desc')->paginate($this->per_page);
        return parent::displayData($result);
    }

    public function interList($request) {
        $where = array();
        $where[] = ['acc_payment.branch_id','=', $this->branch_id];
        $where[] = ['pay_flags','=', 1];
        $where[] = ['is_inter_branch','=', 1];

        $rltn = [
            'particulars' => function ($qr) {
  
                $qr->select('acc_payment_sub.paysub_pay_no','a.ledger_name as led_name')
                            ->join('acc_ledgers as a','a.ledger_id','acc_payment_sub.paysub_ledger_id')
                            ->orderBy('paysub_pay_no', 'asc')->groupby('paysub_pay_no');
            },
        ];


        $result = AccPayment::select('*')
            ->with($rltn)
            ->where($where)
            ->join('acc_branches','acc_payment.pay_inter_branch_id','acc_branches.branch_id')
            ->orderBy('pay_id','desc')->paginate($this->per_page);
        return parent::displayData($result);
    }

    public function interReceiptList($request) {

        $where = array();
        $where[] = ['acc_payment.pay_inter_branch_id','=', $this->branch_id];
        $where[] = ['pay_flags','=', 1];
        $where[] = ['is_inter_branch','=', 1];

        $result = AccPayment::select('*')
        // ->with($rltn)
        ->where($where)
        ->join('acc_branches','acc_payment.branch_id','acc_branches.branch_id')
        ->leftJoin('acc_receipt','acc_payment.pay_no','acc_receipt.inter_payment_no')
        ->leftJoin('acc_receipt_sub','acc_receipt.rec_no','acc_receipt_sub.recsub_rec_no')
        ->leftJoin('acc_ledgers','acc_receipt_sub.recsub_acc_ledger_id','acc_ledgers.ledger_id')
        ->orderBy('pay_id','desc')->paginate($this->per_page);
        return parent::displayData($result);
    }

    public function searchPayment($request) {
        $where = array();
        $where[] = ['acc_payment.branch_id','=', $this->branch_id];
        $where[] = ['pay_flags','=', 1];
        $orwhere = array();
        $orwhere[] = ['acc_payment.branch_id','=', $this->branch_id];
        $orwhere[] = ['pay_flags','=', 1];

        $rltn = [
            'particulars' => function ($qr) {
  
                $qr->select('acc_payment_sub.paysub_pay_no','a.ledger_name as led_name')
                            ->join('acc_ledgers as a','a.ledger_id','acc_payment_sub.paysub_ledger_id')
                            ->orderBy('paysub_pay_no', 'asc')->groupby('paysub_pay_no');
            },
        ];

if($request['search'] != ''){
    $where[] = ['pay_no','like', '%'.$request['search'].'%'];
}

if($request['search'] != ''){
    $orwhere[] = ['acc_payment.branch_pay_no', 'like', '%' . $request['search'] . '%'];
}

        $result = AccPayment::select('*')->where($where)->orWhere($orwhere)->with($rltn)->orderBy('pay_id','desc')->paginate($this->per_page);
        return parent::displayData($result);
    }




    public function getPayment($request)
    {

        $rltn = [
            'particulars' => function ($qr) {

                $qr->select('acc_payment_sub.*', 'acc_ledgers.ledger_name as led_name', 'acc_ledgers.ledger_id')
                    ->join('acc_ledgers', 'ledger_id', 'acc_payment_sub.paysub_ledger_id')
                    ->orderBy('paysub_pay_no', 'asc');
            },
        ];

        $data = AccPayment::select('acc_payment.*', 'acc_ledgers.ledger_name as acc_led_name')
            ->join('acc_ledgers', 'ledger_id', 'acc_payment.pay_acc_ledger_id')
            ->with($rltn);

        if ($this->branch_id > 0) {
            $data = $data->where('acc_payment.branch_id', $this->branch_id);
        }

        $data = $data->where('acc_payment.pay_no', $request['rec_no'])->first();
// return $data['pay_ttl_amount'];exit;
        $data['total_amount_in_words'] = $this->getArabicCurrency($data['pay_ttl_amount']);

        return $data;

    }

    public function getArabicCurrency(float $number)
    {
        $decimal = round($number - ($no = floor($number)), 2) * 100;
        $hundred = null;
        $digits_length = strlen($no);
        $i = 0;
        $str = array();
        $words = array(0 => '', 1 => 'one', 2 => 'two',
            3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
            7 => 'seven', 8 => 'eight', 9 => 'nine',
            10 => 'ten', 11 => 'eleven', 12 => 'twelve',
            13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
            16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
            19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
            40 => 'forty', 50 => 'fifty', 60 => 'sixty',
            70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
        $digits = array('', 'hundred', 'thousand', 'lakh', 'crore');
        while ($i < $digits_length) {
            $divider = ($i == 2) ? 10 : 100;
            $number = floor($no % $divider);
            $no = floor($no / $divider);
            $i += $divider == 10 ? 1 : 2;
            if ($number) {
                $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
                $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
                $str[] = ($number < 21) ? $words[$number] . ' ' . $digits[$counter] . $plural . ' ' . $hundred : $words[floor($number / 10) * 10] . ' ' . $words[$number % 10] . ' ' . $digits[$counter] . $plural . ' ' . $hundred;
            } else {
                $str[] = null;
            }

        }
        $Rupees = implode('', array_reverse($str));

        $paise = ($decimal > 0) ? "and " . ($words[floor($decimal / 10) * 10] . " " . $words[$decimal % 10]) . ' Halala' : '';
        return ($Rupees ? $Rupees . 'Riyals ' : '') . $paise . ' Only';
    }


}
