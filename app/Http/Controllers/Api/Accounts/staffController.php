<?php

namespace App\Http\Controllers\Api\Accounts;

use App\Http\Controllers\Api\ApiParent;
use App\Models\Accounts\Acc_staff;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class staffController extends ApiParent
{

    // ===================== Code Added By Deepu =======================

    public function satffOperations (Request $request, $type)
    {

        switch ($type) {
            case 'add':
                return $this->addStaff($request);
                break;
            case 'list_all':
                return $this->listStaff($request);
                break;
            // case 'update':
            //     return $this->updateVan($request);
            //     break;

            // case 'activate':
            //     return $this->activateVan($request);
            //     break;

                // case 'search':
                // return $this->searchVan($request);
                // break;

            default:
                return response()->json(['error' => 'Invalid Endpoint', 'status' => $this->badrequest_stat]);
                break;
        }
    }

    /**
     * @api {post} http://127.0.0.1:8000/api/godown/add  Add Godown
     * @apiName addGodown
     * @apiGroup Godown
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {String} gd_name Godown Name (Required)
     * @apiParam {String} gd_code Godown Code
     * @apiParam {String} gd_cntct_num Godown contact Number
     * @apiParam {String} gd_cntct_person Godown contact person
     * @apiParam {String} gd_address Godown address
     * @apiParam {String} gd_desc Godown Description
     *
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} message Success Message
     *
     * @apiError {String} error Error Message.
     *
     */

    public function addStaff ($request)
    {

        $validator = Validator::make(
            $request->all(),
            ['staff_laccount_no' => 'required|unique:staff'],
            ['staff_laccount_no.required' => 'Required',
            'staff_laccount_no.unique' => 'Already Exists']
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }
        $info = $request->all();
        // $info = array();
        // $info['staff_laccount_no'] = $info['ledger']['ledger_id'];
        // $info['staff_notes'] = $info['notes'];
        // unset($info['ledger']);    
        // unset($info['notes']);    
        $info['staff_add_date'] = date('Y-m-d');
        $info['staff_addedby'] = $this->usr_id;
        
        Acc_staff::create($info);
        return parent::displayMessage('Saved Successfully');
       
    }

    /**
     * @api {post} http://127.0.0.1:8000/api/godown/edit  Edit Godown
     * @apiName editGodown
     * @apiGroup Godown
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {BigInt} gd_id Godown Id (Required)
     * @apiParam {String} gd_name Godown Name (Required)
     * @apiParam {String} gd_code Godown Code
     * @apiParam {String} gd_cntct_num Godown contact Number
     * @apiParam {String} gd_cntct_person Godown contact person
     * @apiParam {String} gd_address Godown address
     * @apiParam {String} gd_desc Godown Description
     *
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} message Success Message
     *
     * @apiError {String} error Error Message.
     *
     */
    public function listStaff()
    {
        
        // $rltn = [

        //     'ledger' => function ($qr) {
        //         $qr->select('ledger_id', 'ledger_name');
        //     },
        //     'vanlines' => function ($qr) {
        //         $qr->select('vanline_id', 'vanline_name');
        //     }

            

        // ];

        $acc_ledger = Acc_staff::leftJoin('acc_ledgers', 'staff.staff_laccount_no', '=', 'acc_ledgers.ledger_id')
        ->get();

        return parent::displayData($acc_ledger);
    }
    public function searchVan($request)
    {
        return parent::displayData(Van::where('van_name','like', $request['van_name'] . '%')->limit(10)->get());
    }
    public function updateVan($request)
    {

        $validator = Validator::make(
            $request->all(),
            [
                'van_id' => 'required',
                'van_name' => 'required',
               

            ],
            [
                'van_name.required' => 'Required',
                'van_name.unique' => 'Already Exists',
                

            ]
           
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        }

        $Van = $request->all();
        $db = Van::find($request->van_id);
        $db->fill($Van);
        $db->save();
        return response()->json(['message' => 'Updated Successfully', 'status' => $this->created_stat]);
    }
    public function activateVan(Request $request)
    {
      
        $db = Van::find($request->van_id);
        $active = $db['van_flag'];
        $db['van_flag'] = ($active)?0:1;
        $db->save();

        return parent::displayMessage(($active)?'Inactivated':'Activated');
    }

    /**
     * @api {post} http://127.0.0.1:8000/api/search_godown  Search Godown
     * @apiName searchGodown
     * @apiVersion 0.1.0
     * @apiGroup Godown
     *
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     * @apiParam {varchar} gd_name Godown Name
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} data Godowns

     */

    public function searchGodown(Request $request)
    {
        if ($request['gd_name'] != "") {
            $gdData = GodownMaster::where('gd_name', 'like', '%' . $request['gd_name'] . '%')->where('branch_id', $request->branch_id)->take(50)->get();

            return response()->json(['data' => $gdData, 'status' => $this->success_stat]);
        } else {

            $data = GodownMaster::where('branch_id', $request->branch_id)->take(50)->get();
            return response()->json(['data' => $data, 'status' => $this->success_stat]);
        }
    }

    /**
     * @api {post} http://127.0.0.1:8000/api/get_godowns Get Godowns
     * @apiName getGodownByBranch
     * @apiVersion 0.1.0
     * @apiGroup Godown
     *
     * @apiHeader {String} Authorization Authorization (Required)
     * @apiHeader {String} Accept Accept (Required)
     *
     *
     * @apiSuccess {String} status Status Code
     * @apiSuccess {String} data Godowns by branch

     */

    public function getGodownByBranch()
    {

        $user = auth()->user();
        $branch_id = ($user != null ? $user->branch_id : 0);

        $gdData = GodownMaster::where('branch_id', $branch_id)->get();

        return parent::displayData($gdData, $this->success_stat);
    }

    public function getAllGodowns(Request $rq)
    {
        $gdData = [];
        if ($gd_name = isset($rq['gd_name']) ? $rq['gd_name'] : 0) {
            $gdData = GodownMaster::select('gd_id', 'gd_name', 'gd_code')->where('gd_name', 'like', '%' . $gd_name . '%')
                ->where('branch_id', $rq->branch_id)->take(10)->get();
        } else {
            $gdData = GodownMaster::select('gd_id', 'gd_name', 'gd_code')->where('branch_id', $rq->branch_id)->take(50)->get();
        }
        $gdData[] = ['gd_id' => 0,
            'gd_name' => 'Shop',
            'gd_code' => 'SP'];
        return parent::displayData($gdData, $this->success_stat);
    }

    // ===================== End of Code Added By Rakesh =======================

}
