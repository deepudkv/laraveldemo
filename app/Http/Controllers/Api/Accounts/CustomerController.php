<?php

namespace App\Http\Controllers\Api\Accounts;

use App\Http\Controllers\Api\ApiParent;
use App\Models\Accounts\Acc_customer;
use App\Models\Accounts\Acc_ledger;
use App\Models\Sales\SalesDueSub;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Models\Company\Acc_branch;
use App\Models\Accounts\Acc_voucher;
use App\Models\Purchase\Supplier;
use DB;
use Validator;

class customercontroller extends ApiParent
{
    public function customerOperations(Request $request, $type)
    {

        switch ($type) {
            case 'add':
                return $this->addCustomer($request);
                break;
            case 'list':
                return $this->listCustomer($request);
                break;

             case 'search_cust_list':
                return $this->searchListCustomer($request);
                break;

            case 'item':
                return $this->getCustomer($request);
                break;
            case 'update':
                return $this->updateCustomer($request);
                break;

            case 'activate':
                //  return $this->activateProductionFormula($request);
                break;

            case 'search':
                return $this->searchCustomer($request);
                break;

            case 'get-balance':
                return $this->getBalance($request);
                break;
            case 'get-pending-invoice':
                return $this->getPendingInvoice($request);
                break;

            default:
                return response()->json(['error' => 'Invalid Endpoint', 'status' => $this->badrequest_stat]);
                break;
        }
    }

    public function addCustomer($request)
    {

        // echo '<pre>';
        // print_r($request->all());
        $validator = Validator::make(
            $request->all(),
            [
                'name' => 'required|unique:acc_ledgers,ledger_name',
                // 'alias' => 'required',
                // 'code' => 'required',
                // 'email' => 'required',
                // 'mobile' => 'required',
                // 'mobile' => 'required|regex:/[0-9]{10}/',
                // 'vat_no' => 'required',
                // 'due_day' => 'required',
                // 'van_line' => 'required',
                'price_group' => 'required',

                // 'add.addr' => 'required',
                // 'add.zip' => 'required',
                // 'add.city' => 'required',
                // 'add.country' => 'required',
                // 'add.state' => 'required',
                // 'add.state_code' => "required_if:add.country,==,india",
                // 'add.fax' => 'required',

                // 'del.addr' => 'required',
                // 'del.zip' => 'required',
                // 'del.city' => 'required',
                // 'del.country' => 'required',
                // 'del.state' => 'required',
                // 'del.state_code' => 'required',
                // 'del.state_code' => "required_if:del.country,==,india",
                // 'del.fax' => 'required',
            ],
            [
                'name.required' => 'Customer Name Required',
                'name.unique' => 'Customer name already exists',

                'alias.required' => 'Required',
                'email.required' => 'Required',
                'mobile.required' => 'Required',
                'mobile.regex' => 'Enter Valid Mobile Number',
                'due_day.required' => 'Required',
                'price_group.required' => 'Required',

                'add.addr.required' => 'Required',
                'add.zip.required' => 'Required',
                'add.city.required' => 'Required',
                'add.country.required' => 'Required',
                'add.state.required' => 'Required',
                'add.state_code.required_if' => "Required",

                'del.addr.required' => 'Required',
                'del.zip.required' => 'Required',
                'del.city.required' => 'Required',
                'del.country.required' => 'Required',
                'del.state.required' => 'Required',
                // 'del.state_code' => 'required',
                'del.state_code.required_if' => "Required",
            ]
        );
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
        } 

        $data = $request->all();
        $info = array();
        $info['ledger_name'] = $data['name'];
        $info['ledger_accgrp_id'] = 21;
        $info['ledger_acc_type'] = 2;
        $info['ledger_return'] = 1;
        $info['ledger_address'] = $data['add']['addr'];
        $info['ledger_notes'] = $data['note'];
        $info['ledger_edit'] = 1;
        $info['ledger_flags'] = 1;
    if(isset($data['op_bal_type'])){        
        
        if($data['op_bal_type']){
        $info['opening_balance'] = 0 - $data['op_bal'];
        }else{
            $info['opening_balance'] = 0 + $data['op_bal'];
        }
    }else{
        $info['opening_balance'] = (isset($data['op_bal']) ? $data['op_bal'] : 0);
    }
        $info['ledger_branch_id'] = $this->branch_id;
        $info['ledger_balance_privacy'] = 0;
        $info['ledger_due'] = 0;
        $info['ledger_tin'] = $data['vat_no'];
        $info['ledger_alias'] = $data['alias'];
        $info['ledger_code'] = $data['code'];
        $info['ledger_email'] = $data['email'];
        $info['ledger_mobile'] = $data['mobile'];
        $info['company_id'] = $data['company_id'];
        $addInfo = array();

        $addInfo['branch_id'] = $this->branch_id;
        $addInfo["server_sync_time"] = $this->server_sync_time;

        $info = array_merge($info, $addInfo);
        $res = Acc_ledger::create($info);

        $ledgerId = $res->ledger_id;
        if (!$ledgerId) {
            return response()->json(['message' => 'Error Adding Customer', 'status' => $this->badrequest_stat]);
            die();
        }
        if($request->op_bal != 0){
            $where = array();
            $where[] = ['branch_id','=', $this->branch_id];
            $branch = Acc_branch::where($where)->first();
            $info = array();
            $max = Acc_voucher::max('vch_no');
            $info['vch_no'] = (empty($max)) ? 1 : $max+1;
            $info['vch_ledger_from'] = $res->ledger_id;
            $info['vch_ledger_to'] = $res->ledger_id;
            $info['vch_date'] = date('Y-m-d',(strtotime ( '-1 day' , strtotime ( $branch['branch_open_date']))));
            $info['vch_in'] = (!$request->op_bal_type) ? abs($request->op_bal) : 0;
            $info['vch_out'] = ($request->op_bal_type) ? abs($request->op_bal) : 0;
            $info['vch_vchtype_id'] = 14;
            $info['vch_notes'] = 'Starting balance';
            $info['vch_added_by'] = $this->usr_id;
            $info['vch_id2'] = 0;
            $info['vch_from_group'] = 21;
            $info['vch_flags'] = 1;
            $info['ref_no'] = 0;
            $info['branch_id'] = $this->branch_id;
            $info['is_web'] = 1;
            $info['godown_id'] = 0;
            $info['van_id'] = 0;
            $info['branch_ref_no'] = 0;
            $info['sub_ref_no'] = 0;
            $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            $voucher = Acc_voucher::updateOrCreate(
                    [
                            'vch_ledger_from' => $res->ledger_id, 
                            'vch_ledger_to' => $res->ledger_id,
                            'vch_vchtype_id' => 14,
                            'vch_date' => $info['vch_date'],
                            'branch_id' => $this->branch_id, 
                            'van_id' => 0 
                    ],
                    $info
            );
            Acc_voucher::where(['vch_in'=>0,'vch_out'=>0])->delete();
        }

        $info = array();
        $info['ledger_id'] = $ledgerId;
        // echo 'ssss';
        // `cust_id` bigint(20) UNSIGNED NOT NULL,
        // `ledger_id` int(11) NOT NULL DEFAULT 0,
       $info['name'] = $data['name'];
       $info['alias'] = ($data['alias']) ? $data['alias'] : '';
       $info['cust_code'] = ($data['code']) ? $data['code'] : '';
       $info['cust_home_addr'] = ($data['add']['addr']) ? $data['add']['addr'] : '';
       $info['zip'] = ($data['add']['zip']) ? $data['add']['zip'] : '';
       $info['city'] = ($data['add']['city']) ? $data['add']['city'] : '';
       $info['state'] = ($data['add']['state']) ? $data['add']['state'] : '';
       if(trim(strtolower($data['add']['country'])) == 'india') {
            $info['state_code'] = ($data['add']['state_code']) ? $data['add']['state_code'] : '';
       }
       $info['country'] = ($data['add']['country']) ? $data['add']['country'] : '';
       $info['fax'] = ($data['add']['fax']) ? $data['add']['fax'] : '';

       $info['dflt_delvry_addr'] = ($data['del']['addr']) ? $data['del']['addr'] : '';
       $info['dflt_delvry_zip'] = ($data['del']['zip']) ? $data['del']['zip'] : '';
       $info['dflt_delvry_city'] = ($data['del']['city']) ? $data['del']['city'] : '';
       $info['dflt_delvry_state'] = ($data['del']['state']) ? $data['del']['state'] : '';
       if(trim(strtolower($data['del']['country'])) == 'india') {
            $info['dflt_delvry_state_code'] = ($data['del']['state_code']) ? $data['del']['state_code'] : '';
       }
       $info['dflt_delvry_country'] = ($data['del']['country']) ? $data['del']['country'] : '';
       $info['dflt_delvry_fax'] = ($data['del']['fax']) ? $data['del']['fax'] : '';

       $info['email'] = ($data['email']) ? $data['email'] : '';
       $info['mobile'] = ($data['mobile']) ? $data['mobile'] : '';
       $info['note'] = ($data['note']) ? $data['note'] : '';
       $info['due_days'] = ($data['due_day']) ? $data['due_day'] : '';
       $info['vat_no'] = ($data['vat_no']) ? $data['vat_no'] : '';
       $info['van_line_id'] = ($data['van_line']) ? $data['van_line'] : '';
       $info['price_group_id'] = ($data['price_group']) ? $data['price_group'] : '';
    if(isset($data['op_bal_type'])){        
      
       if($data['op_bal_type']){
        $info['opening_balance'] = 0 - $data['op_bal'];
        }else{
            $info['opening_balance'] = 0 + $data['op_bal'];
        }
    }else{
        $info['opening_balance'] = (isset($data['op_bal']) ? $data['op_bal'] : 0);
    }

        $info = array_merge($info, $addInfo);
        $data1 = Acc_customer::create($info);

        $info = array();
        $info['salesduesub_inv_no'] = 0;
        $info['salesduesub_ledger_id'] = $ledgerId;
        $info['salesduesub_date'] = date('Y-m-d');
        // salesduesub_type =>  0- sales, 1 sales return , 2 payment, 3 opening balance
        $info['salesduesub_type'] = 3;
        $info['salesduesub_in'] = (isset($data['op_bal_type']) && $data['op_bal_type']) ? $data['op_bal'] : 0;
        $info['salesduesub_out'] = (!isset($data['op_bal_type']) || !$data['op_bal_type']) ? $data['op_bal'] : 0;
        $info['salesduesub_inv_amount'] = (isset($data['op_bal_type']) && $data['op_bal_type']) ? $data['op_bal'] * -1 : $data['op_bal'];
        $info['salesduesub_inv_balance'] = (isset($data['op_bal_type']) && $data['op_bal_type']) ? $data['op_bal'] * -1 : $data['op_bal'];
        $info['salesduesub_added_by'] = $this->usr_id;
        $info['salesduesub_flags'] = 1;
        $info['salesduesub_agent_ledger_id'] = 0;
        $info['is_web'] = 1;
        $info['godown_id'] = 0;
        $info['van_inv_no'] = 'Opening Balance';
        $info['branch_inv_no'] = 'Opening Balance';
        $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
        $info['branch_id'] = $this->branch_id;
  
        $OpBalance = SalesDueSub::updateOrCreate(
            [
                'salesduesub_ledger_id' => $ledgerId,
                'salesduesub_type' => 3, 
            ],
            $info
        );


        if ($request->is_supplier) {
            
            $supplier['supp_ledger_id'] = $ledgerId;
            $supplier['supp_name'] = isset($request->name)?$request->name:'';
            $supplier['supp_alias'] = isset($request->alias)?$request->alias:'';
            $supplier['supp_code'] =($data['code']) ? $data['code'] : '';
            $supplier['supp_address1'] =($data['add']['addr']) ? $data['add']['addr'] : '';
         

            $supplier['supp_zip'] =  ($data['add']['zip']) ? $data['add']['zip'] : '';
            $supplier['supp_city'] = ($data['add']['city']) ? $data['add']['city'] : '';
            $supplier['supp_state'] =($data['add']['state']) ? $data['add']['state'] : '';
            $supplier['supp_country'] =  ($data['add']['country']) ? $data['add']['country'] : '';

            if(trim(strtolower($data['add']['country'])) == 'india') {
                $supplier['supp_state_code'] = ($data['add']['state_code']) ? $data['add']['state_code'] : '';
           }
            if(isset($data['op_bal_type'])){        
      
                if($data['op_bal_type']){
                    $supplier['opening_balance'] = 0 - $data['op_bal'];
                 }else{
                    $supplier['opening_balance'] = 0 + $data['op_bal'];
                 }
             }else{
                $supplier['opening_balance'] = (isset($data['op_bal']) ? $data['op_bal'] : 0);
             }

            $supplier['due_days'] = isset($data['due_day']) ? $data['due_day'] : '';
            $supplier['supp_mob'] = ($data['mobile']) ? $data['mobile'] : '';
            $supplier['supp_email'] = ($data['email']) ? $data['email'] : '';
            $supplier['supp_notes'] = ($data['note']) ? $data['note'] : '';
            $supplier['supp_tin'] = ($data['vat_no']) ? $data['vat_no'] : '';

            $supplier['supp_branch_id'] = $this->branch_id;
            $supplier['branch_id'] = $this->branch_id;
            $supplier['server_sync_time'] = $this->server_sync_time;
            $res = Supplier::create($supplier);
        }


        return response()->json(['message' => 'Saved Successfully', 'status' => $this->created_stat]);
    }

    public function updateCustomer($request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'name' => 'required|unique:acc_ledgers,ledger_name,' . $request->ledger_id . ',ledger_id',
                // 'alias' => 'required',
                // 'code' => 'required',
                // 'email' => 'required',
                // 'mobile' => 'required',
                // 'mobile' => 'required|regex:/[0-9]{10}/',
                // 'vat_no' => 'required',
                // 'due_day' => 'required',
                'van_line' => 'required',
                'price_group' => 'required',

                // 'add.addr' => 'required',
                // 'add.zip' => 'required',
                // 'add.city' => 'required',
                // 'add.country' => 'required',
                // 'add.state' => 'required',
                // 'add.state_code' => "required_if:add.country,==,india",
                // 'add.fax' => 'required',

                // 'del.addr' => 'required',
                // 'del.zip' => 'required',
                // 'del.city' => 'required',
                // 'del.country' => 'required',
                // 'del.state' => 'required',
                // 'del.state_code' => 'required',
                // 'del.state_code' => "required_if:del.country,==,india",
                // 'del.fax' => 'required',
            ],
            [
                'name.required' => 'Customer Name Required',
                'name.unique' => 'Customer name already exists',

                'alias.required' => 'Required',
                'email.required' => 'Required',
                'mobile.required' => 'Required',
                'mobile.regex' => 'Enter Valid Mobile Number',
                'due_day.required' => 'Required',
                'price_group.required' => 'Required',

                'add.addr.required' => 'Required',
                'add.zip.required' => 'Required',
                'add.city.required' => 'Required',
                'add.country.required' => 'Required',
                'add.state.required' => 'Required',
                'add.state_code.required_if' => "Required",

                'del.addr.required' => 'Required',
                'del.zip.required' => 'Required',
                'del.city.required' => 'Required',
                'del.country.required' => 'Required',
                'del.state.required' => 'Required',
                // 'del.state_code' => 'required',
                'del.state_code.required_if' => "Required",
            ]
        );
        
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages(), 'status' => $this->badrequest_stat]);
            die();
        } 

        $data = $request->all();
        $info = Acc_ledger::find($request->ledger_id);
        $info['ledger_name'] = $data['name'];
        $info['ledger_address'] = $data['add']['addr'];
        $info['ledger_notes'] = $data['note'];
        // $info['ledger_branch_id'] = $request->branch_id;
    if(isset($data['op_bal_type'])){
        if($data['op_bal_type']){
            $info['opening_balance'] = 0 - $data['op_bal'];
            }else{
                $info['opening_balance'] = 0 + $data['op_bal'];
            }
        }else{
            $info['opening_balance'] = (isset($data['op_bal']) ? $data['op_bal'] : 0);
        }
        $info['ledger_tin'] = $data['vat_no'];
        $info['ledger_alias'] = $data['alias'];
        $info['ledger_code'] = $data['code'];
        $info['ledger_email'] = $data['email'];
        $info['ledger_mobile'] = $data['mobile'];
        $info['company_id'] = $data['company_id'];
        $info["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);
        // print_r($info);
        $info->save();

        $where = array();
		$where[] = ['branch_id','=', $this->branch_id];
        $branch = Acc_branch::where($where)->first();
        $info = array();
        // Code for remove , at initail no voucher added  
        $where = [
            'vch_ledger_from' => $request->ledger_id, 
            'vch_ledger_to' => $request->ledger_id,
            'vch_vchtype_id' => 14,
            'vch_date' => date('Y-m-d',(strtotime ( '-1 day' , strtotime ( $branch['branch_open_date'])))),
            'branch_id' => $this->branch_id, 
            'van_id' => 0 
        ];
        $voucher = Acc_voucher::where($where)->first();
        if(empty($voucher)){
            $max = Acc_voucher::max('vch_no');
            $info['vch_no'] = (empty($max)) ? 1 : $max+1;
        }
        // Code Remove end

		$info['vch_ledger_from'] = $request->ledger_id;
		$info['vch_ledger_to'] = $request->ledger_id;
		$info['vch_date'] = date('Y-m-d',(strtotime ( '-1 day' , strtotime ( $branch['branch_open_date']))));
		$info['vch_in'] = (!$request->op_bal_type) ? abs($request->op_bal) : 0;
		$info['vch_out'] = ($request->op_bal_type) ? abs($request->op_bal) : 0;
		$info['vch_vchtype_id'] = 14;
		$info['vch_notes'] = 'Starting balance';
		$info['vch_added_by'] = $this->usr_id;
		$info['vch_id2'] = 0;
		$info['vch_from_group'] = 21;
		$info['vch_flags'] = 1;
		$info['ref_no'] = 0;
		$info['branch_id'] = $this->branch_id;
		$info['is_web'] = 1;
		$info['godown_id'] = 0;
		$info['van_id'] = 0;
		$info['branch_ref_no'] = 0;
		$info['sub_ref_no'] = 0;
		$info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
		$voucher = Acc_voucher::updateOrCreate(
				[
						'vch_ledger_from' => $request->ledger_id, 
						'vch_ledger_to' => $request->ledger_id,
                        'vch_vchtype_id' => 14,
                        'vch_date' => $info['vch_date'],
						'branch_id' => $this->branch_id, 
						'van_id' => 0 
				],
				$info
		);
        Acc_voucher::where(['vch_in'=>0,'vch_out'=>0])->delete();
        
        // $info1 = Acc_customer::where('cust_id',$request->cust_id);
        $info = Acc_customer::find($request->id);

        $info['name']  = $data['name'];
        $info['alias'] = $data['alias'];
        $info['cust_code'] = $data['code'];
        $info['cust_home_addr'] = $data['add']['addr'];
        $info['zip'] = $data['add']['zip'];
        $info['city'] = $data['add']['city'];
        $info['state'] = $data['add']['state'];
        $info['state_code'] = (trim(strtolower($data['add']['country'])) == 'india') ? $data['add']['state_code'] : '';
        $info['country'] = $data['add']['country'];
        $info['fax'] = $data['add']['fax'];

        $info['dflt_delvry_addr'] = $data['del']['addr'];
        $info['dflt_delvry_zip'] = $data['del']['zip'];
        $info['dflt_delvry_city'] = $data['del']['city'];
        $info['dflt_delvry_state'] = $data['del']['state'];
        $info['dflt_delvry_state_code'] = (trim(strtolower($data['del']['country'])) == 'india') ? $data['del']['state_code'] : '';
  
        $info['dflt_delvry_country'] = $data['del']['country'];
        $info['dflt_delvry_fax'] = $data['del']['fax'];
    if(isset($data['op_bal_type'])){        
        if($data['op_bal_type']){
            $info['opening_balance'] = 0 - $data['op_bal'];
            }else{
                $info['opening_balance'] = 0 + $data['op_bal'];
            }
        }else{
            $info['opening_balance'] = (isset($data['op_bal']) ? $data['op_bal'] : 0);
        }
        $info['email'] = $data['email'];
        $info['mobile'] = $data['mobile'];
        $info['note'] = $data['note'];
        $info['due_days'] = $data['due_day'];
        $info['vat_no'] = $data['vat_no'];
        $info['van_line_id'] = $data['van_line'];
        $info['price_group_id'] = $data['price_group'];
        $info["server_sync_time"] = $request->server_sync_time;
        
        $info->save();

        $amnt = DB::raw('( IFNULL(sum(salesduesub_in),0)) as paid_amt');
		$where = array();
        $where[] = ['salesduesub_ledger_id','=', $request->ledger_id];
        $where[] = ['salesduesub_type','=', 2];
        $where[] = ['branch_inv_no','=', 'Opening Balance'];
        $where[] = ['salesduesub_flags','=', 1];
		$paid = SalesDueSub::select($amnt)
			->where($where)
            ->first();
        $paid['paid_amt'];

        $info = array();
        $info['salesduesub_inv_no'] = 0;
        $info['salesduesub_ledger_id'] = $request->ledger_id;
        $info['salesduesub_date'] = date('Y-m-d');
        // salesduesub_type =>  0- sales, 1 sales return , 2 payment, 3 opening balance
        $info['salesduesub_type'] = 3;
        $info['salesduesub_in'] = (isset($data['op_bal_type']) && $data['op_bal_type']) ? $data['op_bal'] : 0;
        $info['salesduesub_out'] = (!isset($data['op_bal_type']) || !$data['op_bal_type']) ? $data['op_bal'] : 0;
        $info['salesduesub_inv_amount'] = (isset($data['op_bal_type']) && $data['op_bal_type']) ? $data['op_bal'] * -1 : $data['op_bal'];
        $info['salesduesub_inv_balance'] = $info['salesduesub_inv_amount'] - $paid['paid_amt'];
        $info['salesduesub_added_by'] = $this->usr_id;
        $info['salesduesub_flags'] = 1;
        $info['salesduesub_agent_ledger_id'] = 0;
        $info['is_web'] = 1;
        $info['godown_id'] = 0;
        $info['van_inv_no'] = 'Opening Balance';
        $info['branch_inv_no'] = 'Opening Balance';
        $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
        $info['branch_id'] = $this->branch_id;
  
        $OpBalance = SalesDueSub::updateOrCreate(
            [
                'salesduesub_ledger_id' => $request->ledger_id,
                'salesduesub_type' => 3, 
            ],
            $info
        );
        return response()->json(['message' => 'Updated Successfully', 'status' => $this->created_stat]);
    }

    public function listCustomer($request)
    {
        return parent::displayData(Acc_customer::select('acc_customers.*','vanline.vanline_name')->leftjoin('vanline','acc_customers.van_line_id','vanline.vanline_id')->where('acc_customers.branch_id',$this->branch_id)->paginate($this->per_page));
    }

    public function getCustomer($request)
    {
        $out = Acc_customer::where('cust_id', $request->cust_id)->first();
        $ledger_id = $out->ledger_id;
        
        if ($supplier = Supplier::where('supp_ledger_id', $ledger_id)->first()) {
            $out->is_sup = 0;
        } else {
            $out->is_sup = 1;
        }
        return parent::displayData($out);
    }

    public function searchCustomer(Request $request)
	{
		if ($request['cust_name'] != "") {
			$acc_cust = Acc_customer::where('name', 'like', '%' .  $request['cust_name'] . '%')
				->take(50)
				->get();
			return response()->json(['data' => $acc_cust, 'status' => $this->success_stat]);
		} else {
			$data = array();
			return response()->json(['data' => $data, 'status' => $this->success_stat]);
		}
	}

      public function searchListCustomer($request)
    {
        if ($request->search=="") {
            return parent::displayData(Acc_customer::select('acc_customers.*', 'vanline.vanline_name')->leftjoin('vanline', 'acc_customers.van_line_id', 'vanline.vanline_id')->where('acc_customers.branch_id', $this->branch_id)->paginate($this->per_page));
        } else {
            return parent::displayData(Acc_customer::select('acc_customers.*', 'vanline.vanline_name')->leftjoin('vanline', 'acc_customers.van_line_id', 'vanline.vanline_id')->where('acc_customers.branch_id', $this->branch_id)->where(function ($q) use ($request) {
                $q->where('name', 'like', '%' .  $request->search . '%')->orWhere('cust_code', 'like', '%' .  $request->search . '%')->orWhere('vanline_name', 'like', '%' .  $request->search . '%')->orWhere('opening_balance', 'like', '%' .  $request->search . '%');
            })->paginate($this->per_page));
        }
    }
    public function getBalance($request, $json=true){

        $amnt = DB::raw('(IFNULL(sum(salesduesub_out),0) - IFNULL(sum(salesduesub_in),0)) as bal_amt');
		$where = array();
        $where[] = ['salesduesub_ledger_id','=', $request->cust_ledger_id];
        $where[] = ['salesduesub_flags','=', 1];
		$balance = SalesDueSub::select($amnt)
			->where($where)
            ->first();
        $result = $balance['bal_amt'];
        if($json) {
            return parent::displayData($result);
        } else {
            return $result;
        }
            
        // $where = array();
        // $where[] = ['salesduesub_ledger_id','=', $request->cust_ledger_id];
        // $where[] = ['salesduesub_salesdue_id','=', 0];
        // // $where[] = ['salesduesub_type','=', 0];
        // $where[] = ['salesduesub_flags','=', 1];
        // $result = SalesDueSub::where($where)->whereIn('salesduesub_type', [0,3,4])->sum('salesduesub_inv_balance');
        // if($json){
        //     return parent::displayData($result);
        // }else{
        //     return $result;
        // }

        // echo 'ssss';exit;
        // $request->ledger_id = $request->cust_ledger_id;
        // $balance = app('App\Http\Controllers\Api\Accounts\LedgerController')->getledgerBalance($request, false);
        // var_dump($balance);
        // $result = $balance['bal_amt'];
        // $result = $balance->bal_amt;
       
    }

    public function getPendingInvoice($request, $json = true, $id = ''){
        $where = array();
        $where[] = ['salesduesub_ledger_id','=', $request->cust_ledger_id];
        $where[] = ['salesduesub_salesdue_id','=', 0];
        // $where[] = ['salesduesub_type','=', 0];
        $where[] = ['salesduesub_inv_balance','<>', 0];
        $where[] = ['salesduesub_flags','=', 1];
        if($id == ''){
            $result = SalesDueSub::select('sales_due_sub.*','sales_master.sales_due_date', 'sales_due_sub.salesduesub_inv_balance as rec_amt')
            ->leftjoin('sales_master','sales_master.sales_inv_no', 'sales_due_sub.salesduesub_inv_no')
            ->whereIn('salesduesub_type', [0,3,4])->orderby('salesduesub_type', 'desc')->orderBy('salesduesub_id', 'ASC')
            ->where($where)->get();
        } else{
            $where[] = ['salesduesub_id','=', $id];
            $result = SalesDueSub::select('sales_due_sub.*','sales_master.sales_due_date', 'sales_due_sub.salesduesub_inv_balance as rec_amt')
            ->leftjoin('sales_master','sales_master.sales_inv_no', 'sales_due_sub.salesduesub_inv_no')
            ->whereIn('salesduesub_type', [0,3,4])
            ->where($where)->first();
        }
        if($json){
            return parent::displayData($result);
        }else{
            return $result;
        }
    }
}


