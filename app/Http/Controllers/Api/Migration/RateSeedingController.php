<?php
namespace App\Http\Controllers\API\Migration;

use App\Http\Controllers\Controller;
use App\Models\Stocks\BranchStocks;
use App\Models\Stocks\CompanyStocks;
use App\Models\Stocks\StockUnitRates;
use DB;

class RateSeedingController extends Controller
{
    public function __construct()
    {
        $this->db2 = DB::connection('mysql2');
        $this->branch_id = 1;
    }

    public function getActiveEanWithUnitRates()
    {

        $array_result = $this->db2->table('tbl_stock_rates')->whereNotNull('stockrate_mrp')->whereNotNull('stockrate_ean')->get();
        $i = 0;
        foreach ($array_result as $data) {

            if ($data->stockrate_ean && $data->stockrate_unit_id && $data->stockrate_mrp) {

                $product_data = $this->createStock($data->stockrate_ean, $data->stockrate_mrp);
//              echo "UNITID====$data->stockrate_unit_id==MRP===$data->stockrate_mrp==EAN==$data->stockrate_ean<br>";
                $i++;
            }

        }

        die($i . " products rate updated ");
    }

    public function createStock($unit_ean, $mrp)
    {
        $data = $product = DB::table('prod_units')->where('produnit_ean_barcode', $unit_ean)->get()->toArray();

        $prd = isset($data['0']) ? $data['0'] :array();

        if (!empty($prd)) {

            $prd_id = $prd->produnit_prod_id;
            $company = CompanyStocks::where('cmp_prd_id', $prd_id)->first();
            $unit_id = $prd->produnit_unit_id;
            $unit_ean = $prd->produnit_ean_barcode;
            $branchInfo = BranchStocks::where('bs_stock_id', $prd_id)->where('bs_branch_id', 16)->first();

            if (empty($branchInfo)) {
                $bstocks["bs_stock_id"] = $company->cmp_stock_id;
                $bstocks["bs_prd_id"] = $prd_id;
                $bstocks["bs_prate"] = 0;
                $bstocks["bs_avg_prate"] = 0;
                $bstocks["bs_stock_quantity"] = 0;
                $bstocks["bs_stock_quantity_shop"] = 0;
                $bstocks["bs_srate"] = $mrp;
                $bstocks["bs_stk_status"] = 1;
                $bstocks["bs_branch_id"] = 16;
                $bstocks['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                $branchInfo = BranchStocks::create($bstocks);
            }

            $stock_rates["branch_stock_id"] = $branchInfo->branch_stock_id;
            $stock_rates["sur_prd_id"] = $prd_id;
            $stock_rates["sur_stock_id"] = $company->cmp_stock_id;
            $stock_rates["sur_unit_id"] = $unit_id;
            $stock_rates["sur_unit_rate"] = $mrp;
            $stock_rates["sur_branch_id"] = 16;
            $stock_rates['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            StockUnitRates::create($stock_rates);

        }

    }


    public function updatestockQuantity()
    {
die("dfxg");
    }

}
