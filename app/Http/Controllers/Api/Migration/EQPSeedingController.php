<?php
namespace App\Http\Controllers\API\Migration;

use App\Http\Controllers\Controller;
use App\Models\Product\Category;
use App\Models\Product\Product;
use App\Models\Product\Subcategory;
use App\Models\Product\Units;
use App\Models\Stocks\BranchStocks;
use App\Models\Stocks\CompanyStocks;
use App\Models\Stocks\OpeningStockLog;
use App\Models\Stocks\StockDailyUpdates;
use App\Models\Stocks\StockUnitRates;
use DB;

class EQPSeedingController extends Controller
{
    public function __construct()
    {
        $this->db2 = DB::connection('mysql2');
        $this->branch_id = 4;
    }
    public function addProductToStock($prd_id, $stock, $old_prd_id, $stock_not)
    {

        $cmpStocks['cmp_prd_id'] = $prd_id;

        $stocks = CompanyStocks::where($cmpStocks)->first();

        if ($stock_not) {
            $cmpStocks['cmp_stock_quantity'] = $stock->stock_qty;
            $cmpStocks['cmp_avg_prate'] = $stock->stock_prate;
            $cmpStocks['cmp_srate'] = $stock->stock_mrp;
            $cmpStocks['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);

            $stocks = CompanyStocks::create($cmpStocks)->first();

            $bstocks["bs_prd_id"] = $prd_id;
            $bstocks["bs_branch_id"] = 0;
            $bstocks["bs_stock_id"] = $stocks->cmp_stock_id;
            $bstocks["bs_prate"] = $stock->stock_prate;
            $bstocks["bs_avg_prate"] = $stock->stock_prate;
            $bstocks["bs_stock_quantity"] = $stock->stock_qty;
            $bstocks["bs_stock_quantity_shop"] = $stock->stock_qty;
            $bstocks["bs_srate"] = $stock->stock_mrp;
            $bstocks["bs_stk_status"] = 1;

            $bstocks['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);

            BranchStocks::create($bstocks);

        }
        else
        {
            $stocks['cmp_stock_quantity'] = $stocks['cmp_stock_quantity']+ $stock->stock_qty;
            $stocks['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            $stocks->save();


            $bstocks["bs_prd_id"] = $prd_id;
            $bstocks["bs_branch_id"] = 0;
            $db= BranchStocks::where($bstocks)->first();

            $db["bs_stock_quantity"] =  $db["bs_stock_quantity"] +$stock->stock_qty;
            $db["bs_stock_quantity_shop"] =  $db["bs_stock_quantity"] +$stock->stock_qty;
            $db['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);

            $db->save();
        }

        $bstocks["bs_stock_id"] = $stocks->cmp_stock_id;
        $bstocks["bs_prd_id"] = $prd_id;
        $bstocks["bs_prate"] = $stock->stock_prate;
        $bstocks["bs_avg_prate"] = $stock->stock_prate;
        $bstocks["bs_stock_quantity"] = $stock->stock_qty;
        $bstocks["bs_stock_quantity_shop"] = $stock->stock_qty;
        $bstocks["bs_srate"] = $stock->stock_mrp;
        $bstocks["bs_stk_status"] = 1;
        $bstocks["bs_branch_id"] = 4;
        $bstocks['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);

        $branchInfo = BranchStocks::create($bstocks);

        $this->addProductUnitsandRates($prd_id, $branchInfo['branch_stock_id'], $stock->stock_id, $stocks->cmp_stock_id, $old_prd_id);

        $this->insertInDailyStocks($branchInfo, $bstocks["bs_stock_quantity"], $gd_id = 0, $sb_id = 0, $opening_stock = 1, $cal_date = date('Y-m-d'));

        return true;

    }

    public function insertInDailyStocks($bs, $new_qty, $gd_id = 0, $batch_id = 0, $opening_stock, $cal_date = null)
    {

        $open_date = date('2019-12-31');

        $ds['sdu_branch_stock_id'] = $bs['branch_stock_id'];
        $ds['sdu_stock_id'] = $bs['bs_stock_id'];
        $ds['sdu_prd_id'] = $bs['bs_prd_id'];
        $ds['sdu_date'] = $open_date;
        $ds['sdu_stock_quantity'] = $new_qty;
        $ds['sdu_gd_id'] = $gd_id;
        $ds['sdu_batch_id'] = $batch_id;
        $ds['branch_id'] = 4;
        $ds['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
        StockDailyUpdates::create($ds);

        $opLog['opstklog_branch_stock_id'] = $bs['branch_stock_id'];
        $opLog['opstklog_stock_id'] = $bs['bs_stock_id'];
        $opLog['opstklog_prd_id'] = $bs['bs_prd_id'];
        $opLog['opstklog_date'] = $open_date;

        $opLog['opstklog_stock_quantity_add'] = $new_qty;
        $opLog['opstklog_gd_id'] = 0;
        $opLog['opstklog_batch_id'] = 0;
        $opLog['opstklog_prate'] = $bs['bs_prate'];
        $opLog['opstklog_srate'] = $bs['bs_srate'];
        $opLog['branch_id'] = 4;
        $opLog['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
        $opLog['opstklog_unit_id'] = 1;

        OpeningStockLog::create($opLog);

    }

    public function addProductUnitsandRates($prdId, $branch_stock_id, $old_stk_id, $cmp_stock_id, $old_prd_id)
    {

        $array = $this->db2->table('tbl_prod_units')->where('produnit_prod_id', $old_prd_id)->get();

        foreach ($array as $data) {

            $new_unit_id = 1;
            $ean = $this->db2->table('tbl_stock_rates')
                ->where('stockrate_prod_id', $old_prd_id)
                ->where('stockrate_stock_id', $old_stk_id)
                ->where('stockrate_unit_id', $data->produnit_unit_id)->first();

            $insert = ['produnit_prod_id' => $prdId,
                'produnit_unit_id' => $new_unit_id,
                'produnit_ean_barcode' => isset($ean->stockrate_ean) ? $ean->stockrate_ean : '',
                'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)];

            $units = DB::table('prod_units')->insert((array) $insert);

            if (isset($ean->stockrate_mrp) && $ean->stockrate_mrp) {
                $stock_rates["branch_stock_id"] = $branch_stock_id;
                $stock_rates["sur_branch_id"] = 1;
                $stock_rates["sur_prd_id"] = $prdId;
                $stock_rates["sur_stock_id"] = $cmp_stock_id;
                $stock_rates["sur_unit_id"] = $new_unit_id;
                $stock_rates["sur_unit_rate"] = $ean->stockrate_mrp;
                $stock_rates["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);
                StockUnitRates::create($stock_rates);
            }

        }
    }

    public function getUnitId($unit_id)
    {

        $unit_name = $this->db2->table('tbl_units')->where('unit_id', $unit_id)->first();

        $unit_data = Units::where('unit_name', $unit_name->unit_name)->get()->first();

        if (empty($unit_data)) {
            die("end");

            $unitdata['unit_name'] = $unitdata['unit_code'] = $unitdata['unit_display'] = $unit_name->unit_name;
            $unitdata['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            $insert = Units::create($unitdata);
            return $insert->unit_id;
        }

        return $unit_data->unit_id;

    }

    public function eqpProductMigration()
    {
        $stocks = $this->db2->table('tbl_stock')->where('stock_qty', '>', 0)->get()->toArray();

        $j = 0;
        foreach ($stocks as $k => $stock) {

            $data = $this->db2->table('tbl_productmaster')->where('prod_id', $stock->stock_prod_id)->first();

            $prd_id = Product::where('prd_name', $data->prod_name)->first();

            if (isset($prd_id->prd_id) && $prd_id->prd_id > 0) {

                $prdctid = $prd_id->prd_id;
                $stk_stat = 0;
            } else {
                $prdctid = $this->createProduct($stock->stock_prod_id);
                $stk_stat = 1;
            }

            $this->addProductToStock($prdctid, $stock, $stock->stock_prod_id, $stk_stat);
            $j++;


        }

        echo "imported products== $j";

    }

    public function getProduct($prod_id)
    {
        $data = $this->db2->table('tbl_productmaster')->where('prod_id', $prod_id)->first();

        $prd_id = Product::where('prd_name', $data->prod_name)->first();

        if (isset($prd_id->prd_id) && $prd_id->prd_id > 0) {
            //  print_r($prd_id);die();
            return $prd_id->prd_id;
        } else {
            $this->createProduct($prod_id);
        }

    }

    public function createProduct($prod_id)
    {

        $data = $this->db2->table('tbl_productmaster')->where('prod_id', $prod_id)->first();

        $cat_id = $this->getCategoryId($data->prod_category);
        $sub_catid = 0;
        if ($data->prod_subcategory > 0) {
            $sub_catid = $this->getSubCategoryId($data->prod_subcategory);
        }

        $base_unitid = 1;

        $ean = $this->getBaseUnitEan($data->prod_id, $data->prod_base_unit_id);
        $ean_code = isset($ean->stockrate_ean) ? $ean->stockrate_ean : '';
        $bar_code = $this->generatePrdBarcode();

        $insert = ['prd_name' => $data->prod_name,
            'prd_cat_id' => $cat_id,
            'prd_sub_cat_id' => $sub_catid, 'prd_supplier' => $data->prod_company,
            'prd_alias' => $data->prod_alias,
            'prd_short_name' => $data->prod_shortname, 'prd_min_stock' => $data->prod_minstock,
            'prd_max_stock' => $data->prod_maxstock, 'prd_barcode' => $bar_code,
            'prd_added_by' => $data->prod_added_by, 'prd_base_unit_id' => $base_unitid,
            'prd_default_unit_id' => $base_unitid, 'prd_tax' => $data->prod_tax,
            'prd_code' => $data->prod_code, 'prd_ean' => $ean_code,
            'prd_tax_code' => $data->prod_tax_code, 'prd_desc' => $data->prod_desc,
            'prd_exp_dur' => $data->prod_exp_dur,
            'prd_loyalty_rate' => $data->prod_loyalty_rate, 'prd_type' => 1,
            'prd_stock_status' => 1,
            'prd_stock_stat' => 1, 'prd_minstock_alert' => $data->prod_minstockalert,
            'prd_maxstock_alert' => $data->prod_maxstockalert,
            'prd_remarks' => $data->prod_desc, 'prd_flag' => 1,
            'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)];

        $product = Product::create($insert);

        return $product->prd_id;

    }

    public function getCategoryId($prod_category)
    {
        $cat = $this->db2->table('tbl_category')->where('cat_id', $prod_category)->first();

        $rowexist = Category::where('cat_name', $cat->cat_name)->get()->first();

        if (empty($rowexist)) {

            $category_data['cat_name'] = $cat->cat_name;
            $category_data['cat_description'] = $cat->cat_description;
            $category_data['cat_pos'] = 1;
            $category_data['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            $insert = Category::create($category_data);
            return $insert->cat_id;
        }

        return $rowexist->cat_id;
    }

    public function getSubCategoryId($prod_category)
    {
        $cat = $this->db2->table('tbl_subcategory')->where('subcat_id', $prod_category)->first();

        $rowexist = Subcategory::where('subcat_name', $cat->subcat_name)->get()->first();

        if (empty($rowexist)) {

            $category_data['subcat_name'] = $cat->subcat_name;
            $category_data['subcat_parent_category'] = $cat->subcat_cat_id;
            $category_data['subcat_pos'] = 1;
            $category_data['subcat_flag'] = 1;
            $category_data['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            $insert = Subcategory::create($category_data);
            return $insert->subcat_id;
        }

        return $rowexist->subcat_id;
    }

    public function getStock($prod_id)
    {
        $ldb = DB::connection('mysql2');
        return $data = $this->db2->table('tbl_stock')->where('stock_qty >', 0)->get()->toArray();
    }

    public function getBaseUnitEan($prod_id, $unit_id)
    {
        $ldb = DB::connection('mysql2');
        return $data = $this->db2->table('tbl_stock_rates')->where('stockrate_prod_id', $prod_id)
            ->where('stockrate_unit_id', $unit_id)->first();
    }

    public function generatePrdBarcode()
    {
        $last_barcode = trim(Product::max('prd_barcode'));

        return ($last_barcode > 0) ? ($last_barcode + 1) : '1000000';
    }

    public function addProductUnits($prdId)
    {
        foreach ($this->db2->table('tbl_prod_units')->where('produnit_prod_id', $prdId)->get() as $data) {

            $ean = $this->db2->table('tbl_stock_rates')->where('stockrate_prod_id', $prdId)
                ->where('stockrate_unit_id', $data->produnit_unit_id)->first();

            $insert = ['produnit_prod_id' => $data->produnit_prod_id,
                'produnit_unit_id' => $data->produnit_unit_id,
                'produnit_ean_barcode' => isset($ean->stockrate_ean) ? $ean->stockrate_ean : '',
                'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)];

            $units = DB::table('prod_units')->insert((array) $insert);

        }
    }

    public function addStockRates($prd_id, $cmp_stock_id, $branch_stock_id)
    {
        foreach ($this->db2->table('tbl_stock_rates')->where('stockrate_prod_id', $prd_id)->get() as $data) {

            $stock_rates["branch_stock_id"] = $branch_stock_id;
            $stock_rates["sur_prd_id"] = $prd_id;
            $stock_rates["sur_stock_id"] = $cmp_stock_id;
            $stock_rates["sur_unit_id"] = $data->stockrate_unit_id;
            $stock_rates["sur_unit_rate"] = $data->stockrate_mrp;
            $stock_rates["sur_branch_id"] = 1;
            $stock_rates['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            StockUnitRates::create($stock_rates);

        }
    }

}
