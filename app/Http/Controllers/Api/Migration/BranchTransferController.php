<?php
namespace App\Http\Controllers\API\Migration;

use App\Http\Controllers\Controller;
use App\Models\BranchTransfer\BranchTransfer;
use App\Models\BranchTransfer\BranchTransferSub;
use App\Models\Sales\SalesSub;
use App\Models\Stocks\BranchStocks;
use App\Models\Stocks\CompanyStocks;
use App\Models\Stocks\OpeningStockLog;
use App\Models\Stocks\StockDailyUpdates;
use DB;

class BranchTransferController extends Controller
{
    public function __construct()
    {
        $this->db2 = DB::connection('mysql2');
    }
    // die(count($oldstock)."==");
    // print_r($oldstock);die();

    public function isssue_fix_transfer()
    {

        $sub_items = BranchTransferSub::where('stocktrsub_stocktr_id', 107)

            ->where('stocktrsub_flags', 1)->get()->toArray();

        foreach ($sub_items as $sub_array) {

            $prd_id = $sub_array['stocktrsub_prd_id'];
            $to_branch = $sub_array['stocktrsub_to'];
            $rate = $sub_array['stocktrsub_rate'];
            $qty = $sub_array['stocktrsub_qty'];

            $where = ['bs_prd_id' => $prd_id, 'bs_branch_id' => $to_branch];

            $bs = BranchStocks::where($where)->first();

            if (isset($bs->bs_prd_id)) {

                $old_p_amount = $bs->bs_stock_quantity * $bs->bs_avg_prate;
                $new_p_amount = $qty * $rate;

                if ($new_p_amount > 0) {

                    if(($bs->bs_stock_quantity - $qty)>0)
                    {
                        $bs_avg_prate = (abs($old_p_amount) - abs($new_p_amount)) / (abs($bs->bs_stock_quantity) - abs($qty));
                    }
                  
                } else {
                    $bs_avg_prate = $bs->bs_avg_prate;
                }

                BranchStocks::where($where)->update([
                    'bs_stock_quantity_shop' => \DB::raw("bs_stock_quantity_shop-$qty"),
                    'bs_stock_quantity' => \DB::raw("bs_stock_quantity-$qty"),
                    'bs_avg_prate' => $bs_avg_prate,
                    'bs_prate' => $rate,
                    'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6),
                ]);
                $stock_info = BranchStocks::where($where)->first();

                $stocktrsub_branch_stock_id = $stock_info['branch_stock_id'];
                $cmp_stock_id = $stock_info['bs_stock_id'];

                $dateExist = StockDailyUpdates::select('*')->where('sdu_branch_stock_id', $stocktrsub_branch_stock_id)
                    ->where('sdu_date', '2020-02-15')->first();

                if ($dateExist) {

                    $column_name = 'sdu_stock_quantity';
                    StockDailyUpdates::where('sdu_branch_stock_id', $stocktrsub_branch_stock_id)
                        ->where('sdu_date', '2020-02-15')->update([
                        $column_name => \DB::raw("$column_name-$qty"),
                        'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6),
                    ]);

                } else {

                    $ds['sdu_branch_stock_id'] = $stocktrsub_branch_stock_id;
                    $ds['sdu_stock_id'] = $cmp_stock_id;
                    $ds['sdu_prd_id'] = $prd_id;
                    $ds['sdu_date'] = '2020-02-15';
                    $ds['sdu_stock_quantity'] = $qty*-1;
                    $ds['sdu_gd_id'] = 0;
                    $ds['sdu_batch_id'] = 0;
                    $ds['branch_id'] = $to_branch;
                    $ds['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                    StockDailyUpdates::create($ds);

                }

            } else {

                die("sss");
                $company = CompanyStocks::where('cmp_prd_id', $prd_id)->first();

                $branchStk["bs_stock_id"] = $company->cmp_stock_id;
                $branchStk["bs_prd_id"] = $prd_id;
                $branchStk["bs_prate"] = $rate;
                $branchStk["bs_avg_prate"] = $rate;
                $branchStk["bs_srate"] = 0;
                $branchStk["bs_branch_id"] = $to_branch;
                $branchStk["bs_stock_quantity"] = $qty;
                $branchStk["bs_stock_quantity_shop"] = $qty;
                $branchStk["bs_stock_quantity_gd"] = 0;
                $branchStk["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);
                $bstocks = BranchStocks::create($branchStk);

                $ds['sdu_branch_stock_id'] = $bstocks->branch_stock_id;
                $ds['sdu_stock_id'] = $company->cmp_stock_id;
                $ds['sdu_prd_id'] = $prd_id;
                $ds['sdu_date'] = '2020-02-15';
                $ds['sdu_stock_quantity'] = $qty;
                $ds['sdu_gd_id'] = 0;
                $ds['sdu_batch_id'] = 0;
                $ds['branch_id'] = $to_branch;
                $ds['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                StockDailyUpdates::create($ds);

            }

        }

       
        $update2['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
        BranchTransferSub::where('stocktrsub_stocktr_id', 107)->
        where('stocktrsub_flags', 1)->update($update2);

        return response()->json(['message' => 'Transfer updatedted', 'status' =>100]);

    }

    public function addOldStock()
    {

        $oldstock = $this->db2->table('erp_stock_daily_updates')->whereBetween('sdu_date', ['2019-12-30', '2019-12-31'])->get()->toArray();

        foreach ($oldstock as $stock) {
            $bs2 = BranchStocks::where(['bs_prd_id' => $stock->sdu_prd_id, 'bs_branch_id' => $stock->branch_id])->first();

            $bs2['bs_stock_quantity'] = $bs2['bs_stock_quantity'] + $stock->sdu_stock_quantity;
            $bs2['bs_stock_quantity_shop'] = $bs2['bs_stock_quantity_shop'] + $stock->sdu_stock_quantity;
            $bs2['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            $bs2->save();

            $insert = json_decode(json_encode($stock), true);
            StockDailyUpdates::create($insert);
        }
        echo "sucees";
    }

    public function recreateStockQuantity()
    {

        //TRUNCATE TABLE `erp_stock_daily_updates`
        //UPDATE `erp_company_stocks` SET `cmp_stock_quantity` = '0'
        //UPDATE `erp_branch_stocks` SET `bs_stock_quantity` = '0' ,`bs_stock_quantity_shop`=0,`bs_stock_quantity_gd`=0,`bs_stock_quantity_van`=0

        $opening_stock = OpeningStockLog::where('branch_id', 1)->where('opstklog_type', 0)->get()->toArray();

        foreach ($opening_stock as $key => $log) {

            $branch_stock_id = $log['opstklog_branch_stock_id'];
            $gd_id = $log['opstklog_gd_id'];
            $quantity_add = $log['opstklog_stock_quantity_add'];
            $date = '2019-12-31';
            $branch_id = $log['branch_id'];
            $prd_id = $log['opstklog_prd_id'];
            $batch_id = $log['opstklog_batch_id'];
            $stock_id = $log['opstklog_stock_id'];

            $daYStk = StockDailyUpdates::select('*')->
                where('sdu_branch_stock_id', $branch_stock_id)
                ->where('sdu_date', $date)->first();

            if ($daYStk) {
                $daYStk['sdu_stock_quantity'] = $daYStk['sdu_stock_quantity'] + $quantity_add;
                $daYStk->save();

            } else {

                $ds['sdu_branch_stock_id'] = $branch_stock_id;
                $ds['sdu_stock_id'] = $stock_id;
                $ds['sdu_prd_id'] = $prd_id;
                $ds['sdu_date'] = $date;
                $ds['sdu_stock_quantity'] = $quantity_add;
                $ds['sdu_gd_id'] = $gd_id;
                $ds['sdu_batch_id'] = $batch_id;
                $ds['branch_id'] = $branch_id;
                $ds['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                StockDailyUpdates::create($ds);

            }

            $bs = BranchStocks::where('branch_stock_id', $branch_stock_id)->first();

            $bs['bs_stock_quantity'] = $bs['bs_stock_quantity'] + $quantity_add;
            $bs['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            if ($gd_id > 0) {
                $bs['bs_stock_quantity_gd'] = $bs['bs_stock_quantity_gd'] + $quantity_add;
            } else {
                $bs['bs_stock_quantity_shop'] = $bs['bs_stock_quantity_shop'] + $quantity_add;
            }
            $bs->save();

            $cs = BranchStocks::where(['bs_prd_id' => $prd_id, 'bs_branch_id' => 0])->first();

            $cs['bs_stock_quantity'] = $cs['bs_stock_quantity'] + $quantity_add;
            $cs['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            if ($gd_id > 0) {
                $cs['bs_stock_quantity_gd'] = $cs['bs_stock_quantity_gd'] + $quantity_add;
            } else {
                $cs['bs_stock_quantity_shop'] = $cs['bs_stock_quantity_shop'] + $quantity_add;
            }
            $cs->save();

            $stock = CompanyStocks::where('cmp_prd_id', $prd_id)->first();
            $stock->cmp_stock_quantity = $stock->cmp_stock_quantity + $quantity_add;
            $stock->server_sync_time = date('ymdHis') . substr(microtime(), 2, 6);
            $stock->save();

        }

        echo "updated succesfully";
    }

    public function recreatebranchTransfer()
    {

        $sub_items = BranchTransferSub::where('branch_id', 1)->where('stocktrsub_flags', 1)->get()->toArray();

        foreach ($sub_items as $sub_array) {

            $prd_id = $sub_array['stocktrsub_prd_id'];
            $to_branch = $sub_array['stocktrsub_to'];
            $quantity = $sub_array['stocktrsub_qty'];
            $branch_id = $sub_array['branch_id'];
            $date = $sub_array['stocktrsub_date'];
            $branch_stock_id = $sub_array['stocktrsub_branch_stock_id'];

            $accepted_date = date('Y-m-d', strtotime($sub_array['updated_at']));

            $bs = BranchStocks::where('branch_stock_id', $branch_stock_id)->first();

            $bs['bs_stock_quantity'] = $bs['bs_stock_quantity'] - $quantity;
            $bs['bs_stock_quantity_shop'] = $bs['bs_stock_quantity_shop'] - $quantity;
            $bs['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            $bs->save();

            $daYStk = StockDailyUpdates::select('*')->
                where('sdu_branch_stock_id', $branch_stock_id)
                ->where('sdu_date', $date)->first();

            if ($daYStk) {
                $daYStk['sdu_stock_quantity'] = $daYStk['sdu_stock_quantity'] - $quantity;
                $daYStk->save();

            } else {

                $ds['sdu_branch_stock_id'] = $branch_stock_id;
                $ds['sdu_stock_id'] = $prd_id;
                $ds['sdu_prd_id'] = $prd_id;
                $ds['sdu_date'] = $date;
                $ds['sdu_stock_quantity'] = $quantity * -1;
                $ds['sdu_gd_id'] = 0;
                $ds['sdu_batch_id'] = 0;
                $ds['branch_id'] = $branch_id;
                $ds['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                StockDailyUpdates::create($ds);

            }

            $bs2 = BranchStocks::where(['bs_prd_id' => $prd_id, 'bs_branch_id' => $to_branch])->first();

            $branch_stock_id_2 = $bs2['branch_stock_id'];
            $bs2['bs_stock_quantity'] = $bs2['bs_stock_quantity'] + $quantity;
            $bs2['bs_stock_quantity_shop'] = $bs2['bs_stock_quantity_shop'] + $quantity;
            $bs2['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            $bs2->save();

            $daYStk2 = StockDailyUpdates::select('*')->
                where('sdu_branch_stock_id', $branch_stock_id_2)
                ->where('sdu_date', $accepted_date)->first();

            if ($daYStk2) {
                $daYStk2['sdu_stock_quantity'] = $daYStk2['sdu_stock_quantity'] + $quantity;
                $daYStk2->save();

            } else {

                $ds2['sdu_branch_stock_id'] = $branch_stock_id_2;
                $ds2['sdu_stock_id'] = $prd_id;
                $ds2['sdu_prd_id'] = $prd_id;
                $ds2['sdu_date'] = $accepted_date;
                $ds2['sdu_stock_quantity'] = $quantity;
                $ds2['sdu_gd_id'] = 0;
                $ds2['sdu_batch_id'] = 0;
                $ds2['branch_id'] = $to_branch;
                $ds2['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                StockDailyUpdates::create($ds2);

            }

        }

        return response()->json(['message' => 'Transfer updated']);

    }

// SELECT * FROM `erp_sale_sub` WHERE `salesub_qty`!=`salesub_rem_qty`
    public function recreateSale()
    {

        $sub_items = SalesSub::where('branch_id', 1)->get()->toArray();

        foreach ($sub_items as $sub_array) {

            $prd_id = $sub_array['salesub_prod_id'];
            $branch_id = $sub_array['branch_id'];
            $quantity = $sub_array['salesub_qty'];
            $date = $sub_array['salesub_date'];

            $bs = BranchStocks::where(['bs_prd_id' => $prd_id, 'bs_branch_id' => $branch_id])->first();

            $branch_stock_id = $bs['branch_stock_id'];

            $bs['bs_stock_quantity'] = $bs['bs_stock_quantity'] - $quantity;
            $bs['bs_stock_quantity_shop'] = $bs['bs_stock_quantity_shop'] - $quantity;
            $bs['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            $bs->save();

            // $bs2 = BranchStocks::where(['bs_prd_id' => $prd_id, 'bs_branch_id' => 0])->first();

            // $branch_stock_id2 = $bs2['branch_stock_id'];
            // $bs2['bs_stock_quantity'] = $bs2['bs_stock_quantity'] - $quantity;
            // $bs2['bs_stock_quantity_shop'] = $bs2['bs_stock_quantity_shop'] - $quantity;
            // $bs2['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            // $bs2->save();

            $daYStk = StockDailyUpdates::select('*')
                ->where('sdu_prd_id', $prd_id)
                ->where('branch_id', $branch_id)
                ->where('sdu_date', $date)->first();

            if ($daYStk) {
                $daYStk['sdu_stock_quantity'] = $daYStk['sdu_stock_quantity'] - $quantity;
                $daYStk->save();

            } else {

                $ds['sdu_branch_stock_id'] = $branch_stock_id;
                $ds['sdu_stock_id'] = $prd_id;
                $ds['sdu_prd_id'] = $prd_id;
                $ds['sdu_date'] = $date;
                $ds['sdu_stock_quantity'] = $quantity * -1;
                $ds['sdu_gd_id'] = 0;
                $ds['sdu_batch_id'] = 0;
                $ds['branch_id'] = $branch_id;
                $ds['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                StockDailyUpdates::create($ds);

            }

            $stock = CompanyStocks::where('cmp_prd_id', $prd_id)->first();
            $stock->cmp_stock_quantity = $stock->cmp_stock_quantity - $quantity;
            $stock->server_sync_time = date('ymdHis') . substr(microtime(), 2, 6);
            $stock->save();

        }

        return response()->json(['message' => 'sales updated']);

    }

}
