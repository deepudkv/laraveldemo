<?php
namespace App\Http\Controllers\API\Migration;

use App\Http\Controllers\Controller;
use App\Models\Product\Product;
use App\Models\Product\Units;
use App\Models\Stocks\BranchStocks;
use App\Models\Stocks\CompanyStocks;
use App\Models\Stocks\StockUnitRates;
use DB;

class ProductSeedingController extends Controller
{
    public function __construct()
    {
        $this->db2 = DB::connection('mysql2');
        $this->server_sync_time = date('ymdHis') . substr(microtime(), 2, 6);
        $this->branch_id = 10;

    }

    public function getStock($prod_id)
    {
        $ldb = DB::connection('mysql2');
        return $data = $this->db2->table('tbl_stock')->where('stock_prod_id', $prod_id)->first();
    }

    public function getBaseUnitEan($prod_id, $unit_id)
    {
        $ldb = DB::connection('mysql2');
        return $data = $this->db2->table('tbl_stock_rates')->where('stockrate_prod_id', $prod_id)
            ->where('stockrate_unit_id', $unit_id)->first();
    }

    public function generatePrdBarcode()
    {
        $last_barcode = trim(Product::max('prd_barcode'));

        return ($last_barcode > 0) ? ($last_barcode + 1) : '5000000';
    }

    public function addProductToStock($prd_id, $stock, $old_prd_id)
    {

        $cmpStocks['cmp_prd_id'] = $prd_id;
        $cmpStocks['cmp_stock_quantity'] = 0;
        $cmpStocks['cmp_avg_prate'] = $stock->stock_avg_prate;
        $cmpStocks['cmp_srate'] = $stock->stock_mrp;
        $cmpStocks['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);

        $stocks = CompanyStocks::create($cmpStocks);

        $bstocks["bs_stock_id"] = $stocks->cmp_stock_id;
        $bstocks["bs_prd_id"] = $prd_id;
        $bstocks["bs_prate"] = $stock->stock_prate;
        $bstocks["bs_avg_prate"] = $stock->stock_avg_prate;
        $bstocks["bs_stock_quantity"] = 0;
        $bstocks["bs_stock_quantity_shop"] = 0;
        $bstocks["bs_srate"] = $stock->stock_mrp;
        $bstocks["bs_stk_status"] = 1;
        $bstocks["bs_branch_id"] = 0;
        $bstocks['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);

        BranchStocks::create($bstocks);

        $bstocks["bs_stock_id"] = $stocks->cmp_stock_id;
        $bstocks["bs_prd_id"] = $prd_id;
        $bstocks["bs_prate"] = $stock->stock_prate;
        $bstocks["bs_avg_prate"] = $stock->stock_avg_prate;
        $bstocks["bs_stock_quantity"] = 0;
        $bstocks["bs_stock_quantity_shop"] = 0;
        $bstocks["bs_srate"] = $stock->stock_mrp;
        $bstocks["bs_stk_status"] = 1;
        $bstocks["bs_branch_id"] = 10;
        $bstocks['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);

        $branchInfo = BranchStocks::create($bstocks);

        //die($branchInfo['branch_stock_id']."==".$stock->stock_id);
        $this->addProductUnitsandRates($prd_id, $branchInfo['branch_stock_id'], $stock->stock_id, $stocks->cmp_stock_id, $old_prd_id);

        // $this->insertInDailyStocks($branchInfo, $bstocks["bs_stock_quantity"], $gd_id = 0, $sb_id = 0, $opening_stock = 1, $cal_date = date('Y-m-d'));
        //$this->addStockRates($prd_id, $stocks->cmp_stock_id, $branchInfo->branch_stock_id);
        return true;

    }

    public function addDefaultStock($prd_id)
    {
        $cmpStocks['cmp_prd_id'] = $prd_id;
        $cmpStocks['cmp_avg_prate'] = 0;
        $cmpStocks['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
        $stocks = CompanyStocks::create($cmpStocks);
        if ($stocks->cmp_stock_id) {
            $bstocks["bs_stock_id"] = $stocks->cmp_stock_id;
            $bstocks["bs_prd_id"] = $prd_id;
            $bstocks["bs_prate"] = 0;
            $bstocks["bs_branch_id"] = 0;
            $bstocks['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            BranchStocks::create($bstocks);
            return $stocks->cmp_stock_id;
        }
        return false;
    }

    public function addProductUnitsandRates($prdId, $branch_stock_id, $old_stk_id, $cmp_stock_id, $old_prd_id)
    {

        $array = $this->db2->table('tbl_prod_units')->where('produnit_prod_id', $old_prd_id)->get();

        foreach ($array as $data) {

            $new_unit_id = $this->getUnitId($data->produnit_unit_id);

            $ean = $this->db2->table('tbl_stock_rates')
                ->where('stockrate_prod_id', $old_prd_id)
                ->where('stockrate_stock_id', $old_stk_id)
                ->where('stockrate_unit_id', $data->produnit_unit_id)->first();


            $insert = ['produnit_prod_id' => $prdId,
                'produnit_unit_id' => $new_unit_id,
                'produnit_ean_barcode' => isset($ean->stockrate_ean) ? $ean->stockrate_ean : '',
                'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)];

         
            $units = DB::table('prod_units')->insert((array) $insert);

            if (isset($ean->stockrate_mrp) && $ean->stockrate_mrp) {
                $stock_rates["branch_stock_id"] = $branch_stock_id;
                $stock_rates["sur_branch_id"] = 10;
                $stock_rates["sur_prd_id"] = $prdId;
                $stock_rates["sur_stock_id"] = $cmp_stock_id;
                $stock_rates["sur_unit_id"] = $new_unit_id;
                $stock_rates["sur_unit_rate"] = $ean->stockrate_mrp;
                $stock_rates["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);
                StockUnitRates::create($stock_rates);
            }

            //  $this->insertRateForUnit();

        }
    }

    public function getUnitName($unit_id)
    {

        $ldb = DB::connection('mysql2');
        return $data = $this->db2->table('tbl_units')->where('unit_id', $unit_id)->first();
    }

    public function getUnitId($unit_id)
    {

        $unit_name = $this->getUnitName($unit_id);

        //  die($unit_name->unit_name);
        $unit_data = Units::where('unit_name', $unit_name->unit_name)->get()->first();

        //    print_r($  $insert );die("fd");

        if (empty($unit_data)) {
            // print_r($  $insert );die("fd");
            $unit_data['unit_name'] = $unit_data['unit_code'] = $unit_data['unit_display'] = $unit_name->unit_name;
            $unit_data['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            $insert = Units::create($unit_data);
            //print_r($insert );die("fd");
            return $insert->unit_id;
        }

        return $unit_data->unit_id;

    }

    public function unitsUpdations()
    {

        $data = Units::whereBetween('unit_id', [244, 420])->where('unit_type',1)->get()->toArray();

        foreach ($data as $k => $val) {
            $unit_id = $val['unit_id'];
            $unit_name = $val['unit_name'];
            $out = $this->getUnitsData($unit_name, $unit_id);

            $update[$unit_name][] = $out;

        }

        // die(count($update)."==");
        echo json_encode($update);
    }

    public function getUnitsData($unit_name, $unit_id)
    {

        $qr = "SELECT * FROM `tbl_units` WHERE `unit_name` LIKE '%$unit_name%'";
        $out = $this->db2->select($qr);

        if (isset($out['0']->unit_name) && $out['0']->unit_name) {

            $out['0']->BASE_UNIT_DB1 = $this->getBaseUnitNmae(trim($out['0']->unit_base_id));

            $RESPONSE = Units::where('unit_name', $out['0']->BASE_UNIT_DB1)->first();

            if ($base_id = $RESPONSE['unit_id']) {

                $unit_data['unit_type'] = $out['0']->unit_type;
                $unit_data['unit_base_id'] = $base_id;
                $unit_data['unit_base_qty'] = $out['0']->unit_base_qty;

              
                $unit_data['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);

                Units::where('unit_id', $unit_id)->update($unit_data);


                $out['0']->BASE_UNIT_NAME = $RESPONSE['unit_name'] ? $RESPONSE['unit_name'] : 'BASEROW';
                $out['0']->BASE_UNIT_ID = $RESPONSE['unit_id'] ? $RESPONSE['unit_id'] : 'dkv';
    
                $out['0']->AUTO_INC_ID = $unit_id;
                return $out['0'];

            }

         
        }

        return ["not_matches"];

    }

    public function getBaseUnitNmae($unit_base_id)
    {

        $qr = "SELECT * FROM `tbl_units` WHERE `unit_id`= $unit_base_id";
        $out = $this->db2->select($qr);

        if (isset($out['0']->unit_name) && $out['0']->unit_name) {
            return $out['0']->unit_name;
        }

        return '0';
    }

    public function migrateProductsWithCategory()
    {
        $total = 0;
        // $prod_category = 12;
        // $prd_cat_id = 17;
        $prod_category = 7;
        $prd_cat_id = 18;
        $prdctarray = $this->db2->table('tbl_productmaster')->where('prod_category', $prod_category)->get()->toArray();

        //print_r( $prdctarray );die();
        foreach ($prdctarray as $data) {

            $exist = Product::where('prd_name', $data->prod_name)->get()->first();
            if ($data->prod_base_unit_id && empty($exist)) {

                $stock = $this->getStock($data->prod_id);
                $ean = $this->getBaseUnitEan($data->prod_id, $data->prod_base_unit_id);
                $ean_code = isset($ean->stockrate_ean) ? $ean->stockrate_ean : '';
                $bar_code = $this->generatePrdBarcode();
                $base_unit = $this->getUnitId($data->prod_base_unit_id);
                //  die("45s");
                $insert = ['prd_name' => $data->prod_name,
                    'prd_cat_id' => $prd_cat_id,
                    'prd_sub_cat_id' => 0, 'prd_supplier' => $data->prod_company, 'prd_alias' => $data->prod_alias,
                    'prd_short_name' => $data->prod_shortname, 'prd_min_stock' => $data->prod_minstock,
                    'prd_max_stock' => $data->prod_maxstock, 'prd_barcode' => $bar_code,
                    'prd_added_by' => $data->prod_added_by, 'prd_base_unit_id' => $base_unit,
                    'prd_default_unit_id' => $base_unit, 'prd_tax' => $data->prod_tax,
                    'prd_code' => $data->prod_code, 'prd_ean' => $ean_code,
                    'prd_tax_code' => $data->prod_tax_code, 'prd_desc' => $data->prod_desc, 'prd_exp_dur' => $data->prod_exp_dur,
                    'prd_loyalty_rate' => $data->prod_loyalty_rate, 'prd_type' => 1, 'prd_stock_status' => 1,
                    'prd_stock_stat' => 1, 'prd_minstock_alert' => $data->prod_minstockalert, 'prd_maxstock_alert' => $data->prod_maxstockalert,
                    'prd_remarks' => $data->prod_desc, 'prd_flag' => 1,
                    'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)];

                $product = Product::create($insert);

                $prdId = $product->prd_id;

                if (isset($stock->stock_id)) {
                    $this->addProductToStock($prdId, $stock, $data->prod_id);
                } else {
                    $this->addDefaultStock($prdId);
                }
                $total = $total + 1;
            }

        }

        return response()->json(['status' => true, 'msg' => "Migration Successfull ($total-" . " products)"]);
    }

    public function eqpProductMigration()
    {

    }
}
