<?php
namespace App\Http\Controllers\API\Migration;

use App\Http\Controllers\Controller;
use App\Models\Product\Product;
use App\Models\Stocks\BranchStocks;
use App\Models\Stocks\CompanyStocks;
use App\Models\Stocks\StockUnitRates;
use DB;

class MigrationController extends Controller
{
    public function __construct()
    {
        $this->db2 = DB::connection('mysql2');
        $this->prdIds_without_stock = [];

    }
    public function install_db()
    {

        $colname = 'Tables_in_' . env('DB_DATABASE');
        $droplist = [];
        $tables = DB::select('SHOW TABLES');

        foreach ($tables as $table) {

            $droplist[] = $table->$colname;

        }

        if (!empty($droplist)) {
            $droplist = implode(',', $droplist);

            DB::beginTransaction();
            DB::statement("DROP TABLE $droplist");
            DB::commit();
        }

        // Artisan::call('migrate');
        shell_exec('php ../artisan migrate');
        shell_exec('php ../artisan passport:install');
        $stmnt = "ALTER TABLE `erp_categories` ADD PRIMARY KEY(`cat_id`)";
        DB::unprepared($stmnt);
        $stmnt = "ALTER TABLE `erp_subcategories` ADD PRIMARY KEY(`subcat_id`)";
        DB::unprepared($stmnt);
        $stmnt = "ALTER TABLE `erp_units` ADD PRIMARY KEY(`unit_id`)";
        DB::unprepared($stmnt);
        return response()->json(['status' => true, 'msg' => 'Migration Successfull (units ,category,subcategory)']);

    }

    public function generalSeeding()
    {

        foreach ($this->db2->table('tbl_category')->get() as $data) {

            $insert = ['cat_id' => $data->cat_id, 'cat_name' => $data->cat_name, 'cat_remarks' => $data->cat_description, 'cat_flag' => 1, 'cat_pos' => $data->cat_pos];
            DB::table('categories')->insert((array) $insert);
        }
        $stmnt = "ALTER TABLE `erp_categories` ADD PRIMARY KEY(`cat_id`)";
        DB::unprepared($stmnt);

        foreach ($this->db2->table('tbl_subcategory')->get() as $data) {

            $insert = ['subcat_id' => $data->subcat_id, 'subcat_parent_category' => $data->subcat_cat_id, 'subcat_name' => $data->subcat_name, 'subcat_flag' => 1, 'subcat_pos' => 1];
            DB::table('subcategories')->insert((array) $insert);
        }
        $stmnt = "ALTER TABLE `erp_subcategories` ADD PRIMARY KEY(`subcat_id`)";
        DB::unprepared($stmnt);

        foreach ($this->db2->table('tbl_units')->get() as $data) {

            $insert = ['unit_id' => $data->unit_id, 'unit_name' => $data->unit_name, 'unit_code' => $data->unit_code,
                'unit_display' => $data->unit_disp, 'unit_type' => $data->unit_type, 'unit_base_id' => $data->unit_base_id,
                'unit_base_qty' => $data->unit_base_qty, 'unit_flag' => $data->unit_flags, 'unit_remarks' => $data->unit_desc];

            DB::table('units')->insert((array) $insert);
        }
        $stmnt = "ALTER TABLE `erp_units` ADD PRIMARY KEY(`unit_id`)";
        DB::unprepared($stmnt);

    }

    public function export_old_db()
    {

        $colname = 'Tables_in_' . env('DB_DATABASE');
        $droplist = [];
        $tables = DB::select('SHOW TABLES');

        foreach ($tables as $table) {

            $droplist[] = $table->$colname;

        }

        if (!empty($droplist)) {
            $droplist = implode(',', $droplist);

            DB::beginTransaction();
            DB::statement("DROP TABLE $droplist");
            DB::commit();
        }

        shell_exec('php ../artisan migrate');
        shell_exec('php ../artisan passport:install');
        $this->generalSeeding();
        $links = $this->generateLinks();
        return response()->json(['status' => true, 'migration_links' => $links, 'msg' => 'Migration Successfull (units ,category,subcategory)']);

    }

    public function generateLinks()
    {
        $total_products = $this->db2->table('tbl_productmaster')->count();
        $batch = ceil($total_products / 500);
        $links = [];
        for ($i = 0; $i < $batch; $i++) {
            $links[$i] = "http://127.0.0.1:8000/api/product_transfer/$i";
        }
        return $links;
    }

    public function startTransfer($s)
    {
        $total = $this->migrateProductsWithStocks(500, $s * 500);

        return response()->json(['status' => true, 'msg' => "Migration Successfull $total Products"]);
    }

    public function migrateProductsWithStocks($limit, $offset)
    {
        $total = 0;
        foreach ($this->db2->table('tbl_productmaster')->skip($offset)->take($limit)->get() as $data) {

            $stock = $this->getStock($data->prod_id);
            $bar_code = isset($stock->stock_barcode) ? $stock->stock_barcode : $this->generatePrdBarcode();

            $insert = ['prd_id' => $data->prod_id, 'prd_name' => $data->prod_name,
                'prd_cat_id' => $data->prod_category,
                'prd_sub_cat_id' => $data->prod_subcategory, 'prd_supplier' => $data->prod_company, 'prd_alias' => $data->prod_alias,
                'prd_short_name' => $data->prod_shortname, 'prd_min_stock' => $data->prod_minstock,
                'prd_max_stock' => $data->prod_maxstock, 'prd_barcode' => $bar_code,
                'prd_added_by' => $data->prod_added_by, 'prd_base_unit_id' => $data->prod_base_unit_id,
                'prd_default_unit_id' => $data->prod_default_unit_id, 'prd_tax' => $data->prod_tax,
                'prd_code' => $data->prod_code,
                'prd_tax_code' => $data->prod_tax_code, 'prd_desc' => $data->prod_desc, 'prd_exp_dur' => $data->prod_exp_dur,
                'prd_loyalty_rate' => $data->prod_loyalty_rate, 'prd_type' => 1, 'prd_stock_status' => 1,
                'prd_stock_stat' => 1, 'prd_minstock_alert' => $data->prod_minstockalert, 'prd_maxstock_alert' => $data->prod_maxstockalert,
                'prd_remarks' => $data->prod_desc, 'prd_flag' => 1];

            // print_r($data);die();
            $product = DB::table('products')->insert((array) $insert);

            $prdId = $data->prod_id;

            $this->addProductUnits($prdId);

            if (isset($stock->stock_id)) {
                $branch_stock_id = $this->addProductToStock($prdId, $stock);
                $this->addStockRtaes($prdId, $branch_stock_id);
                $total = $total + 1;
            } else {
                $arr['prd_id'] = $prdId;
                $product = DB::table('product_without_stock')->insert((array) $arr);

            }

        }
        return $total;
        $last = (int) $offset + (int) $limit;
        return response()->json(['status' => true, 'msg' => "Migration Successfull ($offset-" . $last . " products)"]);
    }

    public function getStock($prod_id)
    {
        $ldb = DB::connection('mysql2');
        return $data = $this->db2->table('tbl_stock')->where('stock_prod_id', $prod_id)->first();

    }
    public function generatePrdBarcode()
    {
        $last_barcode = trim(Product::max('prd_barcode'));

        return ($last_barcode > 0) ? ($last_barcode + 1) : '5000000';

    }
    public function addProductUnits($prdId)
    {
        foreach ($this->db2->table('tbl_prod_units')->where('produnit_prod_id', $prdId)->get() as $data) {

            $ean = $this->db2->table('tbl_stock_rates')->where('stockrate_prod_id', $prdId)
                ->where('stockrate_unit_id', $data->produnit_unit_id)->first();

            $insert = ['produnit_id' => $data->produnit_id, 'produnit_prod_id' => $data->produnit_prod_id,
                'produnit_unit_id' => $data->produnit_unit_id,
                'produnit_ean_barcode' => isset($ean->stockrate_ean) ? $ean->stockrate_ean : ''];

            $units = DB::table('prod_units')->insert((array) $insert);

        }
    }

    public function addProductToStock($prd_id, $stock)
    {

        $cmpStocks['cmp_stock_id'] = $stock->stock_id;
        $cmpStocks['cmp_prd_id'] = $prd_id;
        // $cmpStocks['cmp_stock_quantity'] = $stock->stock_qty + $stock->stock_qty_gdn;
        // $cmpStocks['cmp_avg_prate'] = $stock->stock_avg_prate;

        $cmpStocks['cmp_stock_quantity'] =0;
        $cmpStocks['cmp_avg_prate'] =0;
        $cmpStocks['cmp_srate'] =0;
        // $cmpStocks['cmp_srate'] = $stock->stock_mrp;
        $stocks = CompanyStocks::create($cmpStocks);

        $bstocks["bs_stock_id"] = $stock->stock_id;
        $bstocks["bs_prd_id"] = $prd_id;
        $bstocks["bs_prate"] = $stock->stock_prate;
        $bstocks["bs_avg_prate"] = $stock->stock_avg_prate;
        $bstocks["bs_stock_quantity"] = $stock->stock_qty + $stock->stock_qty_gdn;
        $bstocks["bs_stock_quantity_shop"] = $stock->stock_qty + $stock->stock_qty_gdn;
        $bstocks["bs_srate"] = $stock->stock_mrp;
        $bstocks["bs_stk_status"] = 1;
        $bstocks["bs_branch_id"] = 0;
        $branchInfo = BranchStocks::create($bstocks);

        return $branchInfo->branch_stock_id;

    }
    public function addStockRtaes($prd_id, $branch_stock_id)
    {
        foreach ($this->db2->table('tbl_stock_rates')->where('stockrate_prod_id', $prd_id)->get() as $data) {

            $stock_rates["branch_stock_id"] = $branch_stock_id;
            $stock_rates["sur_prd_id"] = $prd_id;
            $stock_rates["sur_stock_id"] = $data->stockrate_stock_id;
            $stock_rates["sur_unit_id"] = $data->stockrate_unit_id;
            $stock_rates["sur_unit_rate"] = $data->stockrate_mrp;
            $stock_rates["sur_branch_id"] = 0;

            StockUnitRates::create($stock_rates);

        }
    }
    public function createDefaultStock()
    {  
        foreach (DB::table('product_without_stock')->where('import_flag', 1)->get() as $info) {
            $this->addDefaultStock($info->prd_id);
            DB::table('product_without_stock')->where('prd_id',$info->prd_id)->update(['import_flag'=>0]);
        }
    }
    public function addDefaultStock($prd_id)
    {
        $cmpStocks['cmp_prd_id'] = $prd_id;
        $cmpStocks['cmp_avg_prate'] = 0;
        $stocks = CompanyStocks::create($cmpStocks);
        if ($stocks->cmp_stock_id) {
            $bstocks["bs_stock_id"] = $stocks->cmp_stock_id;
            $bstocks["bs_prd_id"] = $prd_id;
            $bstocks["bs_prate"] = 0;
            $bstocks["bs_branch_id"] = 0;
            BranchStocks::create($bstocks);
            return $stocks->cmp_stock_id;
        }
        return false;
    }

}
