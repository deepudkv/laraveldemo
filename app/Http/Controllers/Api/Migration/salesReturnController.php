<?php

namespace App\Http\Controllers\Api\Migration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Sales\SalesReturnSub;
use App\Models\Stocks\StockDailyUpdates;




use DB;

class salesReturnController extends Controller
{

    public function stockdayFixDecreaseStock()
    {
        

         $saleSub = SalesReturnSub::select('*')->where('salesretsub_flags',1)->get();

        //  return $saleSub;

        foreach($saleSub as $key => $val){
            $stockDay = StockDailyUpdates::where('sdu_branch_stock_id',$val['salesretsub_branch_stock_id'])->decrement('sdu_stock_quantity',$val['salesretsub_qty']);

        }
return "Stock Day Stocks Reverted";
       
}

public function stockdayFixIncrementeStock()
    {
    
        $saleSub = SalesReturnSub::select('*')->where('salesretsub_flags',1)->get();
    
        foreach($saleSub as $key => $val){
            $stockDays = StockDailyUpdates::where('sdu_branch_stock_id',$val['salesretsub_branch_stock_id'])->where('sdu_date',$val['salesretsub_date'])->increment('sdu_stock_quantity',$val['salesretsub_qty']);

        }

        return "Stock Day Stocks Added";


    }

    
}
