<?php
namespace App\Http\Controllers\API\Migration;

use App\Http\Controllers\Controller;
use App\Models\Accounts\Acc_customer;
use App\Models\Accounts\Acc_ledger;
use App\Models\Purchase\Supplier;

use App\Models\Accounts\Acc_staff;
use DB;

class CustomerSeedingController extends Controller
{
    public function __construct()
    {
        $this->db2 = DB::connection('mysql2');
        $this->server_sync_time = date('ymdHis') . substr(microtime(), 2, 6);

    }

    public function tanoorCustomer()
    {

        $key = $_GET['key'];
        $vanid = $_GET['vanid'];
        $customer = $this->db2->table('tbl_acc_ledger')->
            where('ledger_name', 'like', '%' . $key . '%')
            ->get();

        //print_r($customer);die();
        $total=0;
        foreach ($customer as $data) {

            $info['ledger_name'] = $data->ledger_name;
            $info['ledger_accgrp_id'] = 21;
            $info['ledger_acc_type'] = 100;
            $info['ledger_return'] = $data->ledger_return;
            $info['ledger_address'] = $data->ledger_address ? $data->ledger_address : '';

            $info['ledger_edit'] = $data->ledger_edit;
            $info['ledger_balance_privacy'] = $data->ledger_balance_privacy;
            $info['ledger_due'] = $data->ledger_due;
            $info['ledger_tin'] = $data->ledger_tin ? $data->ledger_tin : '';
            $info['ledger_alias'] = $data->ledger_alias ? $data->ledger_alias : '';
            $info['ledger_code'] = $data->ledger_code ? $data->ledger_code : '';
            $info['ledger_email'] = $data->ledger_email ? $data->ledger_email : '';
            $info['ledger_mobile'] = $data->ledger_mobile ? $data->ledger_mobile : '';
            $info['ledger_flags'] = 1;
            $info['branch_id'] = 1;
            $info['ledger_branch_id'] = 1;
            $info['opening_balance'] = 0;
            $info["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);

            $res = Acc_ledger::create($info);

            if ($ledgerId = $res->ledger_id) {
                $cust = array();
                $cust['ledger_id'] = $ledgerId;

                $cust['name'] = $data->ledger_name;
                $cust['alias'] = $data->ledger_alias ? $data->ledger_alias : '';
                $cust['cust_code'] = $data->ledger_code ? $data->ledger_code : '';
                $cust['cust_home_addr'] = $data->ledger_address ? $data->ledger_address : '';

                $cust['email'] = $data->ledger_email ? $data->ledger_email : '';
                $cust['mobile'] = $data->ledger_mobile ? $data->ledger_mobile : '';
                $cust['due_days'] = $data->ledger_due ? $data->ledger_due : '';
                $cust['van_line_id'] = $vanid;
                $cust['opening_balance'] = 0;
                $cust['vat_no'] = '';
                $cust['price_group_id'] = 1;
                $cust['branch_id'] = 1;
                $cust["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);

                $data = Acc_customer::create($cust);
            }
            $total = $total + 1;
        }

        return response()->json(['status' => true, 'msg' => "Migration Successfull ($total tanoor customer)"]);
    }

    public function migrateVanLineLineNano()
    {
    }

    public function migrateCustomersNano()
    {

        $ledger_accgrp_id = ['105' => 12, '106' => 13, '107' => 14, '108' => 15];
        $total = 0;

        $customer = $this->db2->table('tbl_acc_ledger')->whereIn('ledger_accgrp_id', [105, 106, 107, 108])->get();

        foreach ($customer as $data) {

            $info['ledger_name'] = $data->ledger_name;
            $info['ledger_accgrp_id'] = 21;
            $info['ledger_acc_type'] = 100;
            $info['ledger_return'] = $data->ledger_return;
            $info['ledger_address'] = $data->ledger_address ? $data->ledger_address : '';

            $info['ledger_edit'] = $data->ledger_edit;
            $info['ledger_balance_privacy'] = $data->ledger_balance_privacy;
            $info['ledger_due'] = $data->ledger_due;
            $info['ledger_tin'] = $data->ledger_tin ? $data->ledger_tin : '';
            $info['ledger_alias'] = $data->ledger_alias ? $data->ledger_alias : '';
            $info['ledger_code'] = $data->ledger_code ? $data->ledger_code : '';
            $info['ledger_email'] = $data->ledger_email ? $data->ledger_email : '';
            $info['ledger_mobile'] = $data->ledger_mobile ? $data->ledger_mobile : '';
            $info['ledger_flags'] = 1;
            $info['branch_id'] = 2;
            $info['ledger_branch_id'] = 2;
            $info['opening_balance'] = 0;
            $info["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);

            $res = Acc_ledger::create($info);

            if ($ledgerId = $res->ledger_id) {

                $count = Acc_customer::where('name', $data->ledger_name)->get()->count();
                $cust = array();
                $cust['ledger_id'] = $ledgerId;

                $cust['name'] = ($count > 0) ? $data->ledger_name . "-MHL" : $data->ledger_name;
                $cust['alias'] = $data->ledger_alias ? $data->ledger_alias : '';
                $cust['cust_code'] = $data->ledger_code ? $data->ledger_code : '';
                $cust['cust_home_addr'] = $data->ledger_address ? $data->ledger_address : '';

                $cust['email'] = $data->ledger_email ? $data->ledger_email : '';
                $cust['mobile'] = $data->ledger_mobile ? $data->ledger_mobile : '';
                $cust['due_days'] = $data->ledger_due ? $data->ledger_due : '';
                $cust['van_line_id'] = trim($ledger_accgrp_id[$data->ledger_accgrp_id]);
                $cust['opening_balance'] = 0;
                $cust['vat_no'] = '';
                $cust['price_group_id'] = 1;
                $cust['branch_id'] = 2;
                $cust["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);

                $data = Acc_customer::create($cust);
            }
            $total = $total + 1;
        }

        return response()->json(['status' => true, 'msg' => "Migration Successfull ($total customer)"]);
    }
    public function migrateCustomers()
    {

        $total = 0;

        $customer = $this->db2->table('tbl_acc_ledger')->whereIn('ledger_accgrp_id', [5])->get();

        foreach ($customer as $data) {

            $info['ledger_name'] = $data->ledger_name;
            $info['ledger_accgrp_id'] = 21;
            $info['ledger_acc_type'] = 2;
            $info['ledger_return'] = $data->ledger_return;
            $info['ledger_address'] = $data->ledger_address ? $data->ledger_address : '';

            $info['ledger_edit'] = $data->ledger_edit;
            $info['ledger_flags'] = 1;
            $info['ledger_branch_id'] = 1;
            $info['ledger_balance_privacy'] = $data->ledger_balance_privacy;
            $info['ledger_due'] = $data->ledger_due;
            $info['ledger_tin'] = $data->ledger_tin ? $data->ledger_tin : '';
            $info['ledger_alias'] = $data->ledger_alias ? $data->ledger_alias : '';
            $info['ledger_code'] = $data->ledger_code ? $data->ledger_code : '';
            $info['ledger_email'] = $data->ledger_email ? $data->ledger_email : '';
            $info['ledger_mobile'] = $data->ledger_mobile ? $data->ledger_mobile : '';
            $info['branch_id'] = 1;
            $info['opening_balance'] = 0;
            $info["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);

            $res = Acc_ledger::create($info);

            $isexist = Acc_customer::where(['name'=>$data->ledger_name])->first();

            // if ($ledgerId = $res->ledger_id && empty($isexist)) {
            //     $cust = array();
            //     $cust['ledger_id'] = $ledgerId;

            //     $cust['name'] = $data->ledger_name;
            //     $cust['alias'] = $data->ledger_alias ? $data->ledger_alias : '';
            //     $cust['cust_code'] = $data->ledger_code ? $data->ledger_code : '';
            //     $cust['cust_home_addr'] = $data->ledger_address ? $data->ledger_address : '';

            //     $cust['email'] = $data->ledger_email ? $data->ledger_email : '';
            //     $cust['mobile'] = $data->ledger_mobile ? $data->ledger_mobile : '';
            //     $cust['due_days'] = $data->ledger_due ? $data->ledger_due : '';
            //     $cust['van_line_id'] = 0;
            //     $cust['opening_balance'] = 0;
            //     $cust['vat_no'] = '';

            //     $cust['price_group_id'] = 1;
            //     $cust['branch_id'] = 1;
            //     $cust["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);

            //     $data = Acc_customer::create($cust);
            //     $total = $total + 1;
            // }
            $total = $total + 1;
        }

        return response()->json(['status' => true, 'msg' => "Migration Successfull ($total customer)"]);
    }

    public function migrateSuppliers()
    {

        $total = 0;

        $customer = $this->db2->table('tbl_acc_ledger')->where('ledger_accgrp_id', 102)->get();

        foreach ($customer as $data) {

            $info['ledger_name'] = $data->ledger_name;
            $info['ledger_accgrp_id'] = 20;
            $info['ledger_acc_type'] = 100;
            $info['ledger_return'] = $data->ledger_return;
            $info['ledger_address'] = $data->ledger_address ? $data->ledger_address : '';

            $info['ledger_edit'] = $data->ledger_edit;
            $info['ledger_flags'] = 1;
            $info['ledger_branch_id'] = 3;
            $info['ledger_balance_privacy'] = $data->ledger_balance_privacy;
            $info['ledger_due'] = $data->ledger_due;
            $info['ledger_tin'] = $data->ledger_tin ? $data->ledger_tin : '';
            $info['ledger_alias'] = $data->ledger_alias ? $data->ledger_alias : '';
            $info['ledger_code'] = $data->ledger_code ? $data->ledger_code : '';
            $info['ledger_email'] = $data->ledger_email ? $data->ledger_email : '';
            $info['ledger_mobile'] = $data->ledger_mobile ? $data->ledger_mobile : '';
            $info['branch_id'] = 3;
            $info['opening_balance'] = 0;
            $info["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);

            $res = Acc_ledger::create($info);

            if ($ledgerId = $res->ledger_id) {
                $supplier = array();
                $supplier['supp_ledger_id'] = $ledgerId;

                $supplier['supp_name'] = $data->ledger_name;
                $supplier['supp_alias'] = $data->ledger_alias ? $data->ledger_alias : '';
                $supplier['supp_code'] = $data->ledger_code ? $data->ledger_code : '';
                $supplier['supp_address1'] = $data->ledger_address ? $data->ledger_address : '';

                $supplier['supp_email'] = $data->ledger_email ? $data->ledger_email : '';
                $supplier['supp_mob'] = $data->ledger_mobile ? $data->ledger_mobile : '';
                $supplier['supp_due'] = $data->ledger_due ? $data->ledger_due : '';
                $supplier['opening_balance'] = 0;
                $supplier['supp_tin'] = $data->ledger_tin ? $data->ledger_tin : '';
                $supplier['supp_branch_id'] = 3;
                $supplier['branch_id'] = 3;
                $supplier['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                $res = Supplier::create($supplier);
            }
            $total = $total + 1;
        }

        return response()->json(['status' => true, 'msg' => "Migration Successfull ($total supplier)"]);
    }

    public function migrateCustomersdb2()
    {

        $total = 0;

        $customer = $this->db2->table('tbl_acc_ledger')->where('ledger_accgrp_id', 13,130)->get();

        foreach ($customer as $data) {

            $info['ledger_name'] = $data->ledger_name;
            $info['ledger_accgrp_id'] = 21;
            $info['ledger_acc_type'] = 100;
            $info['ledger_return'] = $data->ledger_return;
            $info['ledger_address'] = $data->ledger_address ? $data->ledger_address : '';

            $info['ledger_edit'] = $data->ledger_edit;
            $info['ledger_flags'] = 1;
            $info['ledger_branch_id'] = 2;
            $info['ledger_balance_privacy'] = $data->ledger_balance_privacy;
            $info['ledger_due'] = $data->ledger_due;
            $info['ledger_tin'] = $data->ledger_tin ? $data->ledger_tin : '';
            $info['ledger_alias'] = $data->ledger_alias ? $data->ledger_alias : '';
            $info['ledger_code'] = $data->ledger_code ? $data->ledger_code : '';
            $info['ledger_email'] = $data->ledger_email ? $data->ledger_email : '';
            $info['ledger_mobile'] = $data->ledger_mobile ? $data->ledger_mobile : '';
            $info['branch_id'] = 2;
            $info['opening_balance'] = 0;
            $info["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);

            $res = Acc_ledger::create($info);

            if ($ledgerId = $res->ledger_id) {
                $cust = array();
                $cust['ledger_id'] = $ledgerId;

                $cust['name'] = $data->ledger_name;
                $cust['alias'] = $data->ledger_alias ? $data->ledger_alias : '';
                $cust['cust_code'] = $data->ledger_code ? $data->ledger_code : '';
                $cust['cust_home_addr'] = $data->ledger_address ? $data->ledger_address : '';

                $cust['email'] = $data->ledger_email ? $data->ledger_email : '';
                $cust['mobile'] = $data->ledger_mobile ? $data->ledger_mobile : '';
                $cust['due_days'] = $data->ledger_due ? $data->ledger_due : '';
                $cust['van_line_id'] = 0;
                $cust['opening_balance'] = 0;
                $cust['vat_no'] = '';

                $cust['price_group_id'] = 1;
                $cust['branch_id'] = 2;
                $cust["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);

                $data = Acc_customer::create($cust);
            }
            $total = $total + 1;
        }

        return response()->json(['status' => true, 'msg' => "Migration Successfull ($total customer)"]);
    }

    public function migrateSuppliersdb2()
    {

        $total = 0;

        $customer = $this->db2->table('tbl_acc_ledger')->whereIn('ledger_accgrp_id', [109])->get();

        foreach ($customer as $data) {

            $info['ledger_name'] = $data->ledger_name;
            $info['ledger_accgrp_id'] = 1001;
            $info['ledger_acc_type'] = 2;
            $info['ledger_return'] = $data->ledger_return;
            $info['ledger_address'] = $data->ledger_address ? $data->ledger_address : '';

            $info['ledger_edit'] = $data->ledger_edit;
            $info['ledger_flags'] = 1;
            $info['ledger_branch_id'] = 1;
            $info['ledger_balance_privacy'] = $data->ledger_balance_privacy;
            $info['ledger_due'] = $data->ledger_due;
            $info['ledger_tin'] = $data->ledger_tin ? $data->ledger_tin : '';
            $info['ledger_alias'] = $data->ledger_alias ? $data->ledger_alias : '';
            $info['ledger_code'] = $data->ledger_code ? $data->ledger_code : '';
            $info['ledger_email'] = $data->ledger_email ? $data->ledger_email : '';
            $info['ledger_mobile'] = $data->ledger_mobile ? $data->ledger_mobile : '';
            $info['branch_id'] = 1;
            $info['opening_balance'] = 0;
            $info["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);

            $res = Acc_ledger::create($info);


          

            if ($ledgerId = $res->ledger_id) {

               
                $supplier = array();
                $supplier['supp_ledger_id'] = $ledgerId;

                $supplier['supp_name'] = $data->ledger_name;
                $supplier['supp_alias'] = $data->ledger_alias ? $data->ledger_alias : '';
                $supplier['supp_code'] = $data->ledger_code ? $data->ledger_code : '';
                $supplier['supp_address1'] = $data->ledger_address ? $data->ledger_address : '';

                $supplier['supp_email'] = $data->ledger_email ? $data->ledger_email : '';
                $supplier['supp_mob'] = $data->ledger_mobile ? $data->ledger_mobile : '';
                $supplier['supp_due'] = $data->ledger_due ? $data->ledger_due : '';
                $supplier['opening_balance'] = 0;
                $supplier['supp_tin'] = $data->ledger_tin ? $data->ledger_tin : '';
                $supplier['supp_branch_id'] = 1;
                $supplier['branch_id'] = 1;
                $supplier['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                $res = Supplier::create($supplier);


                // print_r( $supplier);die();
                // die($ledgerId."==");
            }
            $total = $total + 1;
        }

        return response()->json(['status' => true, 'msg' => "Migration Successfull ($total supplier)"]);
    }

    public function migrateStaff()
    {

        $total = 0;

        $customer = $this->db2->table('tbl_acc_ledger')->whereIn('ledger_accgrp_id', [109])->get();

        foreach ($customer as $data) {

            $info['ledger_name'] = $data->ledger_name;
            $info['ledger_accgrp_id'] = 1001;
            $info['ledger_acc_type'] = 2;
            $info['ledger_return'] = $data->ledger_return;
            $info['ledger_address'] = $data->ledger_address ? $data->ledger_address : '';

            $info['ledger_edit'] = $data->ledger_edit;
            $info['ledger_flags'] = 1;
            $info['ledger_branch_id'] = 1;
            $info['ledger_balance_privacy'] = $data->ledger_balance_privacy;
            $info['ledger_due'] = $data->ledger_due;
            $info['ledger_tin'] = $data->ledger_tin ? $data->ledger_tin : '';
            $info['ledger_alias'] = $data->ledger_alias ? $data->ledger_alias : '';
            $info['ledger_code'] = $data->ledger_code ? $data->ledger_code : '';
            $info['ledger_email'] = $data->ledger_email ? $data->ledger_email : '';
            $info['ledger_mobile'] = $data->ledger_mobile ? $data->ledger_mobile : '';
            $info['branch_id'] = 1;
            $info['opening_balance'] = 0;
            $info["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);

            $res = Acc_ledger::create($info);


          

            if ($ledgerId = $res->ledger_id) {

               
                $staff = array();
                $staff['staff_laccount_no'] =$ledgerId; 
    $staff['staff_add_date'] = date('Y-m-d');
    $staff['staff_addedby'] = 8;
    
    Acc_staff::create($staff);
            }
            $total = $total + 1;
        }

        return response()->json(['status' => true, 'msg' => "Migration Successfull ($total staff)"]);
    }


    public function bakerycustomer()
    {

        $ledger_accgrp_id = ['106' => 1, '108' => 2, '109' => 3, '110' => 4];
        $total = 0;

        $customer = $this->db2->table('tbl_acc_ledger')->whereIn('ledger_accgrp_id',
            [106, 108, 109, 110])->get();

        foreach ($customer as $data) {

            $info['ledger_name'] = $data->ledger_name;
            $info['ledger_accgrp_id'] = 21;
            $info['ledger_acc_type'] = 100;
            $info['ledger_return'] = $data->ledger_return;
            $info['ledger_address'] = $data->ledger_address ? $data->ledger_address : '';

            $info['ledger_edit'] = $data->ledger_edit;
            $info['ledger_balance_privacy'] = $data->ledger_balance_privacy;
            $info['ledger_due'] = $data->ledger_due;
            $info['ledger_tin'] = $data->ledger_tin ? $data->ledger_tin : '';
            $info['ledger_alias'] = $data->ledger_alias ? $data->ledger_alias : '';
            $info['ledger_code'] = $data->ledger_code ? $data->ledger_code : '';
            $info['ledger_email'] = $data->ledger_email ? $data->ledger_email : '';
            $info['ledger_mobile'] = $data->ledger_mobile ? $data->ledger_mobile : '';
            $info['ledger_flags'] = 1;
            $info['branch_id'] = 12;
            $info['ledger_branch_id'] = 12;
            $info['opening_balance'] = 0;
            $info["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);

            $res = Acc_ledger::create($info);

            if ($ledgerId = $res->ledger_id) {
                $cust = array();
                $cust['ledger_id'] = $ledgerId;

                $cust['name'] = $data->ledger_name;
                $cust['alias'] = $data->ledger_alias ? $data->ledger_alias : '';
                $cust['cust_code'] = $data->ledger_code ? $data->ledger_code : '';
                $cust['cust_home_addr'] = $data->ledger_address ? $data->ledger_address : '';

                $cust['email'] = $data->ledger_email ? $data->ledger_email : '';
                $cust['mobile'] = $data->ledger_mobile ? $data->ledger_mobile : '';
                $cust['due_days'] = $data->ledger_due ? $data->ledger_due : '';
                $cust['van_line_id'] = trim($ledger_accgrp_id[$data->ledger_accgrp_id]);
                $cust['opening_balance'] = 0;
                $cust['vat_no'] = '';
                $cust['price_group_id'] = 1;
                $cust['branch_id'] = 12;
                $cust["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);

                $data = Acc_customer::create($cust);
            }
            $total = $total + 1;
        }

        return response()->json(['status' => true, 'msg' => "Migration Successfull ($total bakerycustomer)"]);
    }

    public function ledgerImport1()
    {
// nano
        $ledger_accgrp_id = ['2' => 2, '168' => 1000, '191' => 1001, '192' => 1002, '193' => 1003,
            '194' => 1004, '195' => 1005, '5' => 5, '147' => 1006,
            '167' => 1006, '172' => 1008, '177' => 1009, '190' => 1010,
            '146' => 1024, '108' => 1011, '114' => 1012, '115' => 1013,
            '116' => 1014, '143' => 1015, '148' => 1016, '149' => 1017,
            '162' => 1018, '163' => 1019, '164' => 1020, '176' => 1021, '179' => 1022, '199' => 1023];
        $total = 0;

        $exit = [2, 168, 191, 192, 193, 194, 195, 5, 147, 167, 172, 177, 190, 146, 108, 114, 115, 116, 143, 148,
            149, 162, 163, 164, 176, 179, 199];
        $customer = $this->db2->table('tbl_acc_ledger')
            ->whereIn('ledger_accgrp_id', $exit)->get();

        foreach ($customer as $data) {

            $info['ledger_name'] = $data->ledger_name;
            $info['ledger_accgrp_id'] = trim($ledger_accgrp_id[$data->ledger_accgrp_id]);
            $info['ledger_acc_type'] = 101;
            $info['ledger_return'] = $data->ledger_return;
            $info['ledger_address'] = $data->ledger_address ? $data->ledger_address : '';

            $info['ledger_edit'] = $data->ledger_edit;
            $info['ledger_balance_privacy'] = $data->ledger_balance_privacy;
            $info['ledger_due'] = $data->ledger_due;
            $info['ledger_tin'] = $data->ledger_tin ? $data->ledger_tin : '';
            $info['ledger_alias'] = $data->ledger_alias ? $data->ledger_alias : '';
            $info['ledger_code'] = $data->ledger_code ? $data->ledger_code : '';
            $info['ledger_email'] = $data->ledger_email ? $data->ledger_email : '';
            $info['ledger_mobile'] = $data->ledger_mobile ? $data->ledger_mobile : '';
            $info['ledger_flags'] = 1;
            $info['branch_id'] = 1;
            $info['ledger_branch_id'] = 1;
            $info['opening_balance'] = 0;
            $info["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);

            $res = Acc_ledger::create($info);

            $total = $total + 1;
        }

        return response()->json(['status' => true, 'msg' => "Migration Successfull ($total bakerycustomer)"]);
    }

    public function migrateCustomersBakery()
    {

        $total = 0;

        $customer = $this->db2->table('tbl_acc_ledger')->where('ledger_accgrp_id', 14)->get();

        foreach ($customer as $data) {

            $info['ledger_name'] = $data->ledger_name;
            $info['ledger_accgrp_id'] = 14;
            $info['ledger_acc_type'] = 100;
            $info['ledger_return'] = $data->ledger_return;
            $info['ledger_address'] = $data->ledger_address ? $data->ledger_address : '';

            $info['ledger_edit'] = $data->ledger_edit;
            $info['ledger_flags'] = 1;
            $info['ledger_branch_id'] = 12;
            $info['ledger_balance_privacy'] = $data->ledger_balance_privacy;
            $info['ledger_due'] = $data->ledger_due;
            $info['ledger_tin'] = $data->ledger_tin ? $data->ledger_tin : '';
            $info['ledger_alias'] = $data->ledger_alias ? $data->ledger_alias : '';
            $info['ledger_code'] = $data->ledger_code ? $data->ledger_code : '';
            $info['ledger_email'] = $data->ledger_email ? $data->ledger_email : '';
            $info['ledger_mobile'] = $data->ledger_mobile ? $data->ledger_mobile : '';
            $info['branch_id'] = 12;
            $info['opening_balance'] = 0;
            $info["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);

            $res = Acc_ledger::create($info);

            if ($ledgerId = $res->ledger_id) {
                $cust = array();
                $cust['ledger_id'] = $ledgerId;

                $cust['name'] = $data->ledger_name;
                $cust['alias'] = $data->ledger_alias ? $data->ledger_alias : '';
                $cust['cust_code'] = $data->ledger_code ? $data->ledger_code : '';
                $cust['cust_home_addr'] = $data->ledger_address ? $data->ledger_address : '';

                $cust['email'] = $data->ledger_email ? $data->ledger_email : '';
                $cust['mobile'] = $data->ledger_mobile ? $data->ledger_mobile : '';
                $cust['due_days'] = $data->ledger_due ? $data->ledger_due : '';
                $cust['van_line_id'] = 0;
                $cust['opening_balance'] = 0;
                $cust['vat_no'] = '';

                $cust['price_group_id'] = 1;
                $cust['branch_id'] = 12;
                $cust["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);

                $data = Acc_customer::create($cust);
            }
            $total = $total + 1;
        }

        return response()->json(['status' => true, 'msg' => "Migration Successfull ($total customer)"]);
    }

    public function migrateSuppliersBakery()
    {

        $total = 0;

        $keys = ['13' => '13', '107' => '1000'];
        $customer = $this->db2->table('tbl_acc_ledger')->whereIn('ledger_accgrp_id', [13, 107])->get();

        foreach ($customer as $data) {

            $info['ledger_name'] = $data->ledger_name;
            $info['ledger_accgrp_id'] = trim($keys[$data->ledger_accgrp_id]);
            $info['ledger_acc_type'] = 100;
            $info['ledger_return'] = $data->ledger_return;
            $info['ledger_address'] = $data->ledger_address ? $data->ledger_address : '';

            $info['ledger_edit'] = $data->ledger_edit;
            $info['ledger_flags'] = 1;
            $info['ledger_branch_id'] = 12;
            $info['ledger_balance_privacy'] = $data->ledger_balance_privacy;
            $info['ledger_due'] = $data->ledger_due;
            $info['ledger_tin'] = $data->ledger_tin ? $data->ledger_tin : '';
            $info['ledger_alias'] = $data->ledger_alias ? $data->ledger_alias : '';
            $info['ledger_code'] = $data->ledger_code ? $data->ledger_code : '';
            $info['ledger_email'] = $data->ledger_email ? $data->ledger_email : '';
            $info['ledger_mobile'] = $data->ledger_mobile ? $data->ledger_mobile : '';
            $info['branch_id'] = 12;
            $info['opening_balance'] = 0;
            $info["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);

            $res = Acc_ledger::create($info);

            if ($ledgerId = $res->ledger_id) {
                $supplier = array();
                $supplier['supp_ledger_id'] = $ledgerId;

                $supplier['supp_name'] = $data->ledger_name;
                $supplier['supp_alias'] = $data->ledger_alias ? $data->ledger_alias : '';
                $supplier['supp_code'] = $data->ledger_code ? $data->ledger_code : '';
                $supplier['supp_address1'] = $data->ledger_address ? $data->ledger_address : '';

                $supplier['supp_email'] = $data->ledger_email ? $data->ledger_email : '';
                $supplier['supp_mob'] = $data->ledger_mobile ? $data->ledger_mobile : '';
                $supplier['supp_due'] = $data->ledger_due ? $data->ledger_due : '';
                $supplier['opening_balance'] = 0;
                $supplier['supp_tin'] = $data->ledger_tin ? $data->ledger_tin : '';
                $supplier['supp_branch_id'] = 12;
                $supplier['branch_id'] = 12;
                $supplier['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                $res = Supplier::create($supplier);
            }
            $total = $total + 1;
        }

        return response()->json(['status' => true, 'msg' => "Migration Successfull ($total supplier)"]);
    }

    public function migrateLedgerBakery()
    {

        $keys = ['5' => '5', '100' => '10001', '101' => '10002', '102' => '10003', '103' => '10004', '104' => '10005', '105' => '10006', '111' => '10007'];
        $total = 0;
        $customer = $this->db2->table('tbl_acc_ledger')->whereIn('ledger_accgrp_id', [5, 100, 101, 102, 103, 104, 105, 111])->get();

        foreach ($customer as $data) {

            $info['ledger_name'] = $data->ledger_name;
            $info['ledger_accgrp_id'] = trim($keys[$data->ledger_accgrp_id]);
            $info['ledger_acc_type'] = 100;
            $info['ledger_return'] = $data->ledger_return;
            $info['ledger_address'] = $data->ledger_address ? $data->ledger_address : '';

            $info['ledger_edit'] = $data->ledger_edit;
            $info['ledger_flags'] = 1;
            $info['ledger_branch_id'] = 12;
            $info['ledger_balance_privacy'] = $data->ledger_balance_privacy;
            $info['ledger_due'] = $data->ledger_due;
            $info['ledger_tin'] = $data->ledger_tin ? $data->ledger_tin : '';
            $info['ledger_alias'] = $data->ledger_alias ? $data->ledger_alias : '';
            $info['ledger_code'] = $data->ledger_code ? $data->ledger_code : '';
            $info['ledger_email'] = $data->ledger_email ? $data->ledger_email : '';
            $info['ledger_mobile'] = $data->ledger_mobile ? $data->ledger_mobile : '';
            $info['branch_id'] = 12;
            $info['opening_balance'] = 0;
            $info["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);

            $res = Acc_ledger::create($info);
            $total = $total + 1;
        }

        return response()->json(['status' => true, 'msg' => "Migration Successfull ($total supplier)"]);
    }

    public function migrateCustomersMasna()
    {

        $total = 0;

        $keys = ['14' => '14', '100' => '1008', '128' => '1012', '129' => '1009', '130' => '1010', '131' => '1011'];
        $customer = $this->db2->table('tbl_acc_ledger')->whereIn('ledger_accgrp_id', [14, 100, 128, 129, 130, 131])->get();

        foreach ($customer as $data) {

            $info['ledger_name'] = $data->ledger_name;
            $info['ledger_accgrp_id'] = trim($keys[$data->ledger_accgrp_id]);
            $info['ledger_acc_type'] = 100;
            $info['ledger_return'] = $data->ledger_return;
            $info['ledger_address'] = $data->ledger_address ? $data->ledger_address : '';

            $info['ledger_edit'] = $data->ledger_edit;
            $info['ledger_flags'] = 1;
            $info['ledger_branch_id'] = 10;
            $info['ledger_balance_privacy'] = $data->ledger_balance_privacy;
            $info['ledger_due'] = $data->ledger_due;
            $info['ledger_tin'] = $data->ledger_tin ? $data->ledger_tin : '';
            $info['ledger_alias'] = $data->ledger_alias ? $data->ledger_alias : '';
            $info['ledger_code'] = $data->ledger_code ? $data->ledger_code : '';
            $info['ledger_email'] = $data->ledger_email ? $data->ledger_email : '';
            $info['ledger_mobile'] = $data->ledger_mobile ? $data->ledger_mobile : '';
            $info['branch_id'] = 10;
            $info['opening_balance'] = 0;
            $info["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);

            $res = Acc_ledger::create($info);

            if ($ledgerId = $res->ledger_id) {
                $cust = array();
                $cust['ledger_id'] = $ledgerId;

                $cust['name'] = $data->ledger_name;
                $cust['alias'] = $data->ledger_alias ? $data->ledger_alias : '';
                $cust['cust_code'] = $data->ledger_code ? $data->ledger_code : '';
                $cust['cust_home_addr'] = $data->ledger_address ? $data->ledger_address : '';

                $cust['email'] = $data->ledger_email ? $data->ledger_email : '';
                $cust['mobile'] = $data->ledger_mobile ? $data->ledger_mobile : '';
                $cust['due_days'] = $data->ledger_due ? $data->ledger_due : '';
                $cust['van_line_id'] = 0;
                $cust['opening_balance'] = 0;
                $cust['vat_no'] = '';

                $cust['price_group_id'] = 1;
                $cust['branch_id'] = 10;
                $cust["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);

                $data = Acc_customer::create($cust);
            }
            $total = $total + 1;
        }

        return response()->json(['status' => true, 'msg' => "Migration Successfull ($total customer)"]);
    }

    public function migrateSuppliersMasna()
    {

        $total = 0;

        $keys = ['13' => '13', '107' => '1013'];
        $customer = $this->db2->table('tbl_acc_ledger')->whereIn('ledger_accgrp_id', [13, 107])->get();

        foreach ($customer as $data) {

            $info['ledger_name'] = $data->ledger_name;
            $info['ledger_accgrp_id'] = trim($keys[$data->ledger_accgrp_id]);
            $info['ledger_acc_type'] = 100;
            $info['ledger_return'] = $data->ledger_return;
            $info['ledger_address'] = $data->ledger_address ? $data->ledger_address : '';

            $info['ledger_edit'] = $data->ledger_edit;
            $info['ledger_flags'] = 1;
            $info['ledger_branch_id'] = 10;
            $info['ledger_balance_privacy'] = $data->ledger_balance_privacy;
            $info['ledger_due'] = $data->ledger_due;
            $info['ledger_tin'] = $data->ledger_tin ? $data->ledger_tin : '';
            $info['ledger_alias'] = $data->ledger_alias ? $data->ledger_alias : '';
            $info['ledger_code'] = $data->ledger_code ? $data->ledger_code : '';
            $info['ledger_email'] = $data->ledger_email ? $data->ledger_email : '';
            $info['ledger_mobile'] = $data->ledger_mobile ? $data->ledger_mobile : '';
            $info['branch_id'] = 10;
            $info['opening_balance'] = 0;
            $info["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);

            $res = Acc_ledger::create($info);

            if ($ledgerId = $res->ledger_id) {
                $supplier = array();
                $supplier['supp_ledger_id'] = $ledgerId;

                $supplier['supp_name'] = $data->ledger_name;
                $supplier['supp_alias'] = $data->ledger_alias ? $data->ledger_alias : '';
                $supplier['supp_code'] = $data->ledger_code ? $data->ledger_code : '';
                $supplier['supp_address1'] = $data->ledger_address ? $data->ledger_address : '';

                $supplier['supp_email'] = $data->ledger_email ? $data->ledger_email : '';
                $supplier['supp_mob'] = $data->ledger_mobile ? $data->ledger_mobile : '';
                $supplier['supp_due'] = $data->ledger_due ? $data->ledger_due : '';
                $supplier['opening_balance'] = 0;
                $supplier['supp_tin'] = $data->ledger_tin ? $data->ledger_tin : '';
                $supplier['supp_branch_id'] = 10;
                $supplier['branch_id'] = 10;
                $supplier['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                $res = Supplier::create($supplier);
            }
            $total = $total + 1;
        }

        return response()->json(['status' => true, 'msg' => "Migration Successfull ($total supplier)"]);
    }
    public function migrateLedgerMasna()
    {
        $ids = [5, 102, 103, 104, 109, 110, 111, 113, 114, 116, 117, 118, 119, 120, 122, 123, 126, 127];
        $keys = ['5' => '5', '102' => '10014', '103' => '10015', '104' => '10016',
            '109' => '10017', '110' => '10018', '111' => '10019', '113' => '10020',
            '114' => '10021', '116' => '10022', '117' => '10023', '118' => '10024',
            '119' => '10025', '120' => '10026', '122' => '10027', '123' => '10028',
            '126' => '10029', '127' => '10030'];
        $total = 0;
        $customer = $this->db2->table('tbl_acc_ledger')->whereIn('ledger_accgrp_id', $ids)->get();

        foreach ($customer as $data) {

            $info['ledger_name'] = $data->ledger_name;
            $info['ledger_accgrp_id'] = trim($keys[$data->ledger_accgrp_id]);
            $info['ledger_acc_type'] = 100;
            $info['ledger_return'] = $data->ledger_return;
            $info['ledger_address'] = $data->ledger_address ? $data->ledger_address : '';

            $info['ledger_edit'] = $data->ledger_edit;
            $info['ledger_flags'] = 1;
            $info['ledger_branch_id'] = 10;
            $info['ledger_balance_privacy'] = $data->ledger_balance_privacy;
            $info['ledger_due'] = $data->ledger_due;
            $info['ledger_tin'] = $data->ledger_tin ? $data->ledger_tin : '';
            $info['ledger_alias'] = $data->ledger_alias ? $data->ledger_alias : '';
            $info['ledger_code'] = $data->ledger_code ? $data->ledger_code : '';
            $info['ledger_email'] = $data->ledger_email ? $data->ledger_email : '';
            $info['ledger_mobile'] = $data->ledger_mobile ? $data->ledger_mobile : '';
            $info['branch_id'] = 10;
            $info['opening_balance'] = 0;
            $info["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);

            $res = Acc_ledger::create($info);
            $total = $total + 1;
        }

        return response()->json(['status' => true, 'msg' => "Migration Successfull ($total supplier)"]);
    }

    public function migrateSupplierStore()
    {

        $total = 0;

        $customer = $this->db2->table('tbl_acc_ledger')->where('ledger_accgrp_id', 13)->get();

        foreach ($customer as $data) {

            $info['ledger_name'] = $data->ledger_name;
            $info['ledger_accgrp_id'] = 13;
            $info['ledger_acc_type'] = 100;
            $info['ledger_return'] = $data->ledger_return;
            $info['ledger_address'] = $data->ledger_address ? $data->ledger_address : '';

            $info['ledger_edit'] = $data->ledger_edit;
            $info['ledger_flags'] = 1;
            $info['ledger_branch_id'] = 11;
            $info['ledger_balance_privacy'] = $data->ledger_balance_privacy;
            $info['ledger_due'] = $data->ledger_due;
            $info['ledger_tin'] = $data->ledger_tin ? $data->ledger_tin : '';
            $info['ledger_alias'] = $data->ledger_alias ? $data->ledger_alias : '';
            $info['ledger_code'] = $data->ledger_code ? $data->ledger_code : '';
            $info['ledger_email'] = $data->ledger_email ? $data->ledger_email : '';
            $info['ledger_mobile'] = $data->ledger_mobile ? $data->ledger_mobile : '';
            $info['branch_id'] = 11;
            $info['opening_balance'] = 0;
            $info["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);

            $res = Acc_ledger::create($info);

            if ($ledgerId = $res->ledger_id) {
                $supplier = array();
                $supplier['supp_ledger_id'] = $ledgerId;

                $supplier['supp_name'] = $data->ledger_name;
                $supplier['supp_alias'] = $data->ledger_alias ? $data->ledger_alias : '';
                $supplier['supp_code'] = $data->ledger_code ? $data->ledger_code : '';
                $supplier['supp_address1'] = $data->ledger_address ? $data->ledger_address : '';

                $supplier['supp_email'] = $data->ledger_email ? $data->ledger_email : '';
                $supplier['supp_mob'] = $data->ledger_mobile ? $data->ledger_mobile : '';
                $supplier['supp_due'] = $data->ledger_due ? $data->ledger_due : '';
                $supplier['opening_balance'] = 0;
                $supplier['supp_tin'] = $data->ledger_tin ? $data->ledger_tin : '';
                $supplier['supp_branch_id'] = 11;
                $supplier['branch_id'] = 11;
                $supplier['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                $res = Supplier::create($supplier);
            }
            $total = $total + 1;
        }

        return response()->json(['status' => true, 'msg' => "Migration Successfull ($total supplier)"]);
    }

}
