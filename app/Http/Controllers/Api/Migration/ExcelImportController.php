<?php
namespace App\Http\Controllers\API\Migration;

use App\Http\Controllers\Api\ApiParent;
use App\Models\Accounts\Acc_customer;
use App\Models\Accounts\Acc_ledger;
use App\Models\Accounts\price_group;
use App\Models\Product\Category;
use App\Models\Product\Product;
use App\Models\Product\ProdUnit;
use App\Models\Product\Units;
use App\Models\Stocks\BranchStocks;
use App\Models\Stocks\CompanyStocks;
use App\Models\Stocks\StockUnitRates;


use App\Models\Stocks\OpeningStockLog;
use App\Models\Stocks\StockDailyUpdates;
use DB;
use Maatwebsite\Excel\Facades\Excel;

class ExcelImportController extends ApiParent
{
    public function __construct()
    {
        $this->db2 = DB::connection('mysql2');
        $this->usr_id = 1;
        $this->branch_id = 1;
        $this->ig=0;
    }



    public function rateUpdateStock()
    {
      
     $file2 = 'C:\wamp64\www\erp\api\fujishka_erp\public\uploads\\openingstock\Dammam2020.xlsx';
 //$file2 = 'C:\wamp64\www\erp\api\fujishka_erp\public\uploads\\openingstock\Riyad2020.xlsx';
        //$file2 = 'C:\wamp64\www\erp\api\fujishka_erp\public\uploads\\openingstock\Riyad2020.xlsx';
        Excel::load($file2, function ($reader) {
            
            foreach ($reader->toArray() as $products) {
                
                foreach ($products as $data) {



                    $barcode = isset($data['barcode'])?$data['barcode']:0;
                    $prate = isset($data['rate'])?$data['rate']:0;
                   

                    if($barcode&&$prate)
                    {

                    $prdct = Product::where(['prd_barcode'=>$barcode])->first();
                  
                    $whr['bs_prd_id'] =  $prdct['prd_id'];
                    $whr['bs_branch_id'] =  3;

                    $stock= BranchStocks::where($whr)->first();

                    if($branch_stock_id= $stock['branch_stock_id'])
                    {
                        $updt['opstklog_prate']=$prate;
                        $updt['server_sync_time']= date('ymdHis') . substr(microtime(), 2, 6);
    
                        OpeningStockLog::where('opstklog_branch_stock_id',$branch_stock_id)->update($updt);
                        $this->ig++;
                    }
                    }

                }
            }
        });

        return response()->json(['status' => true, 'msg' => "$this->ig rows updated "]);

    }


    public function productImport()
    {
        $file2 = 'C:\wamp64\www\erp\api\fujishka_erp\public\uploads\excel\ItemsExcel01-Jan-2020 01-12-42.xlsx';
        Excel::load($file2, function ($reader) {

            foreach ($reader->toArray() as $products) {
                foreach ($products as $data) {

                    $cat_id = $this->getCatId($data['itemgroups']);
                    $unit_id = 1;
                    //   $unit_id = $this->getUnitId($data['unitname']);
                
                        $insert = [
                            'prd_name' => $data['product_name'],
                            'prd_cat_id' => $cat_id,
                            'prd_sub_cat_id' => 0,
                            'prd_alias' => isset($data['alias']) ? $data['alias'] : $data['product_name'],
                            'prd_short_name' => mb_substr($data['product_name'], 0, 4),
                            'prd_barcode' => $this->generatePrdBarcode(),
                            'prd_code' => isset($data['prd_code']) ? $data['prd_code'] : '',
                            'prd_added_by' => $this->usr_id,
                            'branch_id' => 0,
                            'prd_tax' => isset($data['percentage']) ? $data['percentage'] : 0,
                            'prd_base_unit_id' => $unit_id,
                            'prd_default_unit_id' => $unit_id,
                            'prd_ean' => isset($data['eanbarcode']) ? $data['eanbarcode'] : '',
                            'prd_desc' => isset($data['description']) ? $data['description'] : '',
                            'prd_type' => ($data['itemtype'] == 'Product') ? 1 : 0,
                            'prd_stock_status' => ($data['itemtype'] == 'Product') ? 1 : 0,
                            'prd_remarks' => isset($data['description']) ? $data['description'] : '',
                            'prd_flag' => 1,
                            'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6),
                        ];

                        $product = Product::create($insert);
                        $prd_id = $product->prd_id;

                    

                    $this->addProductBaseUnit($prd_id, $unit_id, $data['eanbarcode']);
                    $this->addtoStock($prd_id, $data, $unit_id);

                }
            }
        });

        return response()->json(['status' => true, 'msg' => 'Product Import Successfull']);

    }

    public function generatePrdBarcode()
    {
        $last_barcode = trim(Product::max('prd_barcode'));

        return ($last_barcode > 0) ? ($last_barcode + 1) : '1000000';

    }
    public function addProductBaseUnit($prd_id, $unit_id, $prd_ean)
    {

    
            $prd_unit["produnit_prod_id"] = $prd_id;
            $prd_unit["branch_id"] = 1;
            $prd_unit["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);
            $prd_unit["produnit_unit_id"] = $unit_id;
            $prd_unit["produnit_ean_barcode"] = $prd_ean;

            return ProdUnit::create($prd_unit);   
    }

    public function addtoStock($prd_id, $data, $unit_id)
    {

        $stocks = CompanyStocks::where('cmp_prd_id', $prd_id)->first();

        if (empty($cstock)) {
            $cmpStocks['cmp_prd_id'] = $prd_id;
            $cmpStocks['cmp_avg_prate'] = $data['purchase_unitprice'];
            $cmpStocks['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            $stocks = CompanyStocks::create($cmpStocks);

            $bstocks["bs_stock_id"] = $stocks->cmp_stock_id;
            $bstocks["bs_prd_id"] = $prd_id;
            $bstocks["bs_stock_quantity"] = $data['stock'];
            $bstocks["bs_stock_quantity_shop"] = $data['stock'];
            $bstocks["bs_prate"] = $data['purchase_unitprice'];
            $bstocks["bs_avg_prate"] = $data['purchase_unitprice'];
            $bstocks["bs_srate"] = $data['sales_unitprice'];
            $bstocks["bs_branch_id"] = 0;
            $bstocks['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            BranchStocks::create($bstocks);
        }
        if ($stocks->cmp_stock_id) {
            $bstocks["bs_stock_id"] = $stocks->cmp_stock_id;
            $bstocks["bs_prd_id"] = $prd_id;
            $bstocks["bs_stock_quantity"] = $data['stock'];
            $bstocks["bs_stock_quantity_shop"] = $data['stock'];
            $bstocks["bs_prate"] = $data['purchase_unitprice'];
            $bstocks["bs_avg_prate"] = $data['purchase_unitprice'];
            $bstocks["bs_srate"] = $data['sales_unitprice'];
            $bstocks["bs_branch_id"] = 1;
            $bstocks['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            $bstocks = BranchStocks::create($bstocks);

            $stock_rates["branch_stock_id"] = $bstocks->branch_stock_id;
            $stock_rates["sur_branch_id"] = 1;
            $stock_rates["sur_unit_id"] = $unit_id;

            $stock_rates["sur_prd_id"] = $prd_id;
            $stock_rates["sur_stock_id"] =  $stocks->cmp_stock_id;


            $stock_rates["sur_unit_rate"] = $data['sales_unitprice'];
            $stock_rates["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);
            StockUnitRates::create($stock_rates);
            return true;
        }
        return false;
    }

    public function getProductId($prd_name)
    {

        $prd = Product::where('prd_name', $prd_name)->first();
        return trim($prd['prd_id']);

    }

    public function getCatId($cat_name)
    {
        $cat = Category::where('cat_name', $cat_name)->first();

        if (empty($cat)) {
            $category_data['cat_name'] = $cat_name;
            $category_data['cat_flag'] = $category_data['cat_pos'] = 1;
            $category_data['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            $insert = Category::create($category_data);
            return $insert->cat_id;
        }
        return $cat['cat_id'];

    }

    public function getUnitId($unit_name)
    {
        $unit_data = Units::where('unit_name', $unit_name)->first();

        if (empty($unit_data)) {
            $unit_data['unit_name'] = $unit_data['unit_code'] = $unit_data['unit_display'] = $unit_name;
            $unit_data['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            $insert = Units::create($unit_data);
            return $insert->unit_id;
        }
        return $unit_data['unit_id'];

    }

    public function customerImport()
    {
        $file2 = 'C:\wamp64\www\erp\api\fujishka_erp\public\uploads\excel\CustomersExcel25Nov.xlsx';
        Excel::load($file2, function ($reader) {

            foreach ($reader->toArray() as $customer) {
                foreach ($customer as $data) {

                    // print_r($data);die();
                    $info['ledger_name'] = $data['name'];
                    $info['ledger_accgrp_id'] = 21;
                    $info['ledger_acc_type'] = 2;
                    $info['ledger_return'] = 1;
                    $info['ledger_address'] = $data['place'] ? $data['place'] : '';

                    $info['ledger_edit'] = 1;
                    $info['ledger_flags'] = 1;
                    $info['ledger_branch_id'] = 1;
                    $info['ledger_balance_privacy'] = 0;
                    $info['ledger_due'] = 0;
                    $info['ledger_tin'] = $data['taxno'] ? $data['taxno'] : '';
                    $info['ledger_alias'] = $data['aliasname'] ? $data['aliasname'] : '';
                    $info['ledger_code'] = $data['code'] ? $data['code'] : '';
                    $info['ledger_email'] = $data['email'] ? $data['email'] : '';
                    $info['ledger_mobile'] = $data['mobile'] ? $data['mobile'] : '';
                    $info['branch_id'] = 1;
                    $info["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);

                    $res = Acc_ledger::create($info);

                    if ($ledgerId = $res->ledger_id) {
                        $cust = array();
                        $cust['ledger_id'] = $ledgerId;

                        $cust['name'] = $data['name'];
                        $cust['alias'] = $data['aliasname'] ? $data['aliasname'] : '';
                        $cust['cust_code'] = $data['code'] ? $data['code'] : '';
                        $cust['cust_home_addr'] = $data['place'] ? $data['place'] : '';

                        $cust['email'] = $data['email'] ? $data['email'] : '';
                        $cust['mobile'] = $data['mobile'] ? $data['mobile'] : '';
                        $cust['due_days'] = $data['duedays'] ? $data['duedays'] : 0;
                        $cust['opening_balance'] = 0;
                        $cust['vat_no'] = $data['taxno'] ? $data['taxno'] : '';

                        $group_id = $this->getPriceGroupId($data['customerpricingmode']);
                        $cust['price_group_id'] = $group_id;
                        $cust['branch_id'] = 1;
                        $cust["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);

                        $data = Acc_customer::create($cust);
                    }

                }

            }
        });
        return response()->json(['status' => true, 'msg' => 'Customer Import Successfull']);
    }

    public function getPriceGroupId($group_name)
    {
        $group = price_group::where('group_name', $group_name)->first();

        if (empty($group)) {
            $group_data['group_name'] = $group_data['group_descp'] = $group_name;
            $group_data['branch_id'] = 1;
            $group_data['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            $insert = price_group::create($group_data);

            return $insert->id;
        }
        return $group['prcgrp_id'];

    }

    public function stockImport()
    {
        $file2 = 'C:\wamp64\www\erp\api\fujishka_erp\public\uploads\excel\StockExcel.xlsx';
        Excel::load($file2, function ($reader) {

            foreach ($reader->toArray() as $stocks) {
                foreach ($stocks as $data) {

                    $prd_data = $this->getProductData($data['product_name']);
                    $prd_id = $prd_data['prd_id'];
                    $quantity = $prd_data['quantity'];
                    $this->updateStockQuantity($prd_id, $quantity);

                }

            }
        });
        return response()->json(['status' => true, 'msg' => 'Stock Import Successfull']);
    }

    public function getProductData($prd_name)
    {

        return $prd = Product::where('prd_name', $prd_name)->first();

    }

    public function updateStockQuantity($prd_id, $quantity)
    {

        $where["cmp_prd_id"] = $prd_id;
        $update['cmp_stock_quantity'] = $quantity;
        $update['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
        CompanyStocks::where($where)->update($update);

        $where1["bs_prd_id"] = $prd_id;
        $where1["bs_branch_id"] = 0;
        $update1['bs_stock_quantity'] = $quantity;
        $update1['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
        BranchStocks::where($where1)->update($update1);

        $where2["bs_prd_id"] = $prd_id;
        $where2["bs_branch_id"] = $this->branch_id;
        $update2['bs_stock_quantity'] = $quantity;
        $update2['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
        BranchStocks::where($where2)->update($update2);

        $bs = BranchStocks::where($where2)->first();

        $this->insertInDailyStocks($bs, $quantity, $prd_id);
        return true;
    }

    public function insertInDailyStocks($bs, $new_qty, $prd_id)
    {
       // branch open date
        $date = date('Y-m-d');

        $ds['sdu_branch_stock_id'] = $bs['branch_stock_id'];
        $ds['sdu_stock_id'] = $bs['bs_stock_id'];
        $ds['sdu_prd_id'] = $bs['bs_prd_id'];
        $ds['sdu_date'] = $date;
        $ds['sdu_stock_quantity'] = $new_qty;
        $ds['sdu_gd_id'] = 0;
        $ds['sdu_batch_id'] = 0;
        $ds['branch_id'] = $this->branch_id;
        $ds['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
        StockDailyUpdates::create($ds);

       
            $opLog['opstklog_branch_stock_id'] = $bs['branch_stock_id'];
            $opLog['opstklog_stock_id'] = $bs['bs_stock_id'];
            $opLog['opstklog_prd_id'] = $bs['bs_prd_id'];
            $opLog['opstklog_date'] = $date;

            $opLog['opstklog_stock_quantity_add'] = $new_qty;
            $opLog['opstklog_gd_id'] = 0;
            $opLog['opstklog_batch_id'] = 0;
            $opLog['opstklog_prate'] = $bs['bs_prate'];
            $opLog['opstklog_srate'] = 0;
            $opLog['branch_id'] = $this->branch_id;
            $opLog['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);

            $prdUnit = Product::select('prd_base_unit_id')
                ->where('prd_id', $bs['bs_prd_id'])
                ->first();
            $opLog['opstklog_unit_id'] = $prdUnit['prd_base_unit_id'];

            OpeningStockLog::create($opLog);
        

    }

    public function rateUpdate()
    {
        $file2 = 'C:\wamp64\www\erp\api\fujishka_erp\public\uploads\excel\itemstockrate.xlsx';
      
        Excel::load($file2, function ($reader) {
            $total = 0;
            foreach ($reader->toArray() as $products) {
                foreach ($products as $data) {

                    $prd_id = $this->getProductId($data['item_name']);
                    if ($prd_id) {
                        
                        CompanyStocks::where('cmp_prd_id', $prd_id)->update(['cmp_avg_prate' => $data['rate']]);

                        BranchStocks::where('bs_prd_id', $prd_id)->update(['bs_prate' => $data['rate'],'bs_avg_prate' => $data['rate']]);
                      
                        $total = $total+1;
                    }
                     
                   

                }
                return response()->json(['status' => true, 'msg' => 'Rate Import Successfull'.$total]);
            }
           
        });

        

    }

}
