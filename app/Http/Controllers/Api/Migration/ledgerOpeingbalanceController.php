<?php
namespace App\Http\Controllers\API\Migration;

use App\Http\Controllers\Controller;
use App\Models\Accounts\Acc_customer;
use App\Models\Accounts\Acc_ledger;
use App\Models\Company\Acc_branch;
use App\Models\Accounts\Acc_voucher;
use App\Models\Purchase\Supplier;

use App\Models\Settings\TaxCategory;
use App\Models\Settings\TaxCategorySub;
use App\Models\Settings\TaxLedgers;
use App\Models\Sales\SalesMaster;
use App\Models\Sales\SalesSub;

use App\Models\Sales\SalesReturnMaster;
use App\Models\Sales\SalesReturnSub;

use App\Models\Purchase\Purchase;
use App\Models\Purchase\PurchaseSub;
use App\Models\Purchase\PurchaseReturn;
use App\Models\Purchase\PurchaseReturnSub;

use App\Models\BranchTransfer\BranchTransfer;

use App\Models\Product\Product;

use Illuminate\Http\Request;
use App\Models\Van\Van;

use App\Models\Stocks\BranchStocks;
use DB;

class ledgerOpeingbalanceController extends Controller
{
    public function __construct()
    {

    }

    public function Operations(Request $request, $type)
    {

        switch ($type) {
            case 'seedoptovoucher':
                return $this->seedOpeningBalanceToVoucher($request);
                break;
            case 'list':
                return $this->list($request);
                break;
            case 'setvanopledgers':
                return $this->setVanOpeningBalanceLedgers($request);
                break;
            case 'settaxledger':
                return $this->setTaxLedger($request);
                break;
            case 'setproducttaxcategory':
                return $this->setProductTaxCategory($request);
                break;
            case 'setsalessubtovoucher':
                return $this->setsalessubtovoucher();
                break;
            case 'setsalesreturnsubtovoucher':
                return $this->setsalesreturnsubtovoucher();
                break;
            case 'setpurchasetovoucher':
                return $this->setpurchasetovoucher();
                break;
            case 'setpurchasereturntovoucher':
                return $this->setpurchasereturntovoucher();
                break;
            case 'revertreceiptvoucher':
                return $this->revertreceiptvoucher();
                break;
            // case 'gensalesretbranchno':
            //     return $this->genSaleRetBranchNo($request);
            //     break;
            // case 'stocktransfertovoucher':
            //     return $this->stocktransfertovoucher();
            //     break;
            default:
                return response()->json(['error' => 'Invalid Endpoint', 'status' => $this->badrequest_stat]);
                break;
        }
    }
    public function revertreceiptvoucher() {
        // $where = array();
        // $where[] = ['vch_vchtype_id','=', 1];
        // $where[] = ['vch_in','=', 0];
        // $where[] = ['vch_out','!=', 0];
        $where = array();
        $where[] = ['vch_id2','=', 0];
        $where[] = ['vch_in','!=', 0];
        $where[] = ['vch_out','=', 0];
        $where1 = array();
        $where1[] = ['vch_id2','!=', 0];
        $where1[] = ['vch_in','=', 0];
        $where1[] = ['vch_out','!=', 0];
        $recpt = Acc_voucher::whereIn('vch_vchtype_id', array(1,22))
        ->where(function($q) use ($where, $where1) {
            $q->orWhere($where);
            $q->orWhere($where1);
        })->orderBy('vch_no','asc')
            ->orderBy('vch_id2','desc')
            ->get()->toArray();
        // dd($recpt);
        $prevVochid = 0;
        foreach($recpt as $val){
            $prevVochid = $val['vch_id'];
            if($val['vch_id2'] != 0){
                $info['vch_id2'] = 0;
            }else{
                $info['vch_id2'] = $prevVochid;
            }
            $where = array();
            $where[] = ['vch_id','=', $val['vch_id']];
            Acc_voucher::where($where)->update($info);
        }
    }
    
    // SELECT * FROM `erp_acc_voucher` WHERE `vch_in` = 0 AND `vch_out` != 0 AND `vch_vchtype_id` = 1 AND `vch_id2` = 0
// for existing entry
    // public function genSaleRetBranchNo() {
    //     $salesReturn = SalesReturnMaster::get()->toArray();
    //     foreach($salesReturn as $val){

    //         if($val['salesret_van_ret_no'] == ''){
    //             $where = array();
    //             $where[] = ['branch_id', '=', $val['branch_id']];
    //             $branch = Acc_branch::where($where)->first();
    //             $where[] = ['is_web', '=', 1];
    //             $max = SalesReturnMaster::where($where)->max('salesret_branch_ret_no');

    //             $info = array();
    //             if (empty($max)) {
    //                 $info['salesret_branch_ret_no'] = 'W#RT' . $branch['branch_code'] . '0001';
    //             } else {
    //                 $no = ltrim($max, 'W#RT' . $branch['branch_code']);
    //                 $newNo = +$no + 1;
    //                 $info['salesret_branch_ret_no'] = 'W#RT' . $branch['branch_code'] . str_pad($newNo, 4, "0", STR_PAD_LEFT);
    //             }
    //         } else {
    //             $info['salesret_branch_ret_no'] = $val['salesret_van_ret_no'];
    //         }

    //         $data = array(
    //             'salesret_branch_ret_no' => $info['salesret_branch_ret_no'] ,
    //             'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)
    //         );
    //         $where = array();
    //         $where[] = ['salesret_id','=', $val['salesret_id']];
    //         SalesReturnMaster::where($where)->update($data);
    //     }
    // }
    public function stocktransfertovoucher($refNo = '') {
        $where = array();
        $where[] = ['stocktr_flags','=', 1];
        $where[] = ['stocktr_accepted','=', 1];
        if($refNo != ''){
            $where[] = ['stocktr_id','=', $refNo];
        }
        $branchTransfers = BranchTransfer::where($where)->get()->toArray();
        // dd($branchTransfers);
        // exit;
        foreach($branchTransfers as $branchMaster){
            $where = array();
            $where[] = ['taxled_taxcat_id','=', 0];
            $where[] = ['taxled_branch_id','=', $branchMaster['branch_id']];
            // sub category id 13  => Stock Transfer, refer erp_tax_category_sub
            $where[] = ['taxled_taxcatsub_id','=', 13];
            $stockTransferLedger = TaxLedgers::where($where)->first();

            // to get Branchledger, eg:- Branch@branch_code
            $where = array();
            $where[] = ['taxled_taxcat_id','=', 0];
            $where[] = ['taxled_branch_id','=', $branchMaster['stocktr_to']];
            // sub category id 12  => Branch, refer erp_tax_category_sub
            $where[] = ['taxled_taxcatsub_id','=', 12];
            $branchLedger = TaxLedgers::where($where)->first();

            // Stock transfer voucher entry 
            $info = array();
            $max = Acc_voucher::max('vch_no');
            $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max+1;
            //Stock Transfer@branchcode
            $info['vch_ledger_from'] =  $stockTransferLedger['taxled_ledger_id'];
            // branch@branchcode ledger
            $ledgerId = $branchLedger['taxled_ledger_id'];
            $info['vch_ledger_to'] = $ledgerId;
            $info['vch_date'] = $branchMaster['stocktr_date'];
            // i think want to add discount to sales_total
            $info['vch_in'] = $branchMaster['stocktr_amount'];
            $info['vch_out'] = 0;
            // voucher type 18 for Stock Transfer
            $info['vch_vchtype_id'] = 18;
            $info['vch_notes'] = $branchMaster['stocktr_notes'];
            $info['vch_added_by'] = $branchMaster['stocktr_added_by'];
            $info['vch_id2'] = 0;
            $info['vch_from_group'] = 9;
            $info['vch_flags'] = 1;
            $info['ref_no'] = $branchMaster['stocktr_id'];
            $info['branch_id'] = $branchMaster['branch_id'];
            $info['is_web'] = 1;
            $info['godown_id'] = 0;
            $info['van_id'] = 0;
            $info['branch_ref_no'] = $branchMaster['stocktr_id'];
            $info['sub_ref_no'] = 0;
            $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            $res2 = Acc_voucher::create($info);

            $info = array();
            $max = Acc_voucher::max('vch_no');
            $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max+1;
            // customer or cash@branchcode ledger 
            $info['vch_ledger_from'] = $ledgerId;
            //sales@branchcode
            $info['vch_ledger_to'] = $stockTransferLedger['taxled_ledger_id'];
            $info['vch_date'] = $branchMaster['stocktr_date'];
            $info['vch_in'] = 0;
            $info['vch_out'] = $branchMaster['stocktr_amount'];
            // voucher type 18 for Stock Transfer
            $info['vch_vchtype_id'] = 18;
            $info['vch_notes'] = $branchMaster['stocktr_notes'];
            $info['vch_added_by'] = $branchMaster['stocktr_added_by'];
            $info['vch_id2'] = $res2->vch_id;;
            $info['vch_from_group'] = 23;
            $info['vch_flags'] = 1;
            $info['ref_no'] = $branchMaster['stocktr_id'];
            $info['branch_id'] = $branchMaster['branch_id'];
            $info['is_web'] = 1;
            $info['godown_id'] = 0;
            $info['van_id'] = 0;
            $info['branch_ref_no'] = $branchMaster['stocktr_id'];
            $info['sub_ref_no'] = 0;
            $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            $res2 = Acc_voucher::create($info);


            $where = array();
            $where[] = ['taxled_taxcat_id','=', 0];
            $where[] = ['taxled_branch_id','=', $branchMaster['stocktr_to']];
            // sub category id 14  => Stock Receipt, refer erp_tax_category_sub
            $where[] = ['taxled_taxcatsub_id','=', 14];
            $stockRecLedger = TaxLedgers::where($where)->first();

            $info = array();
            $max = Acc_voucher::max('vch_no');
            $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max+1;
            // Stock Receipt@branchcode
            $info['vch_ledger_from'] = $stockRecLedger['taxled_ledger_id'];


            // to get Branchledger, eg:- Branch@branch_code
            $where = array();
            $where[] = ['taxled_taxcat_id','=', 0];
            $where[] = ['taxled_branch_id','=', $branchMaster['branch_id']];
            // sub category id 12  => Branch, refer erp_tax_category_sub
            $where[] = ['taxled_taxcatsub_id','=', 12];
            $branchLedger = TaxLedgers::where($where)->first();
        
            // Supplier or cash@branchcode ledger , purch_pay_type => 1-credit, 2-cash
            $ledgerId = $branchLedger['taxled_ledger_id'];
            $info['vch_ledger_to'] = $ledgerId;
            $info['vch_date'] = $branchMaster['stocktr_date'];
            $info['vch_in'] = 0;
            $info['vch_out'] = $branchMaster['stocktr_amount'];
            // voucher type 16 for Stock Receipt
            $info['vch_vchtype_id'] = 16;
            $info['vch_notes'] = $branchMaster['stocktr_notes'];
            $info['vch_added_by'] = $branchMaster['stocktr_added_by'];
            $info['vch_id2'] = 0;
            $info['vch_from_group'] = 10;
            $info['vch_flags'] = 1;
            $info['ref_no'] = $branchMaster['stocktr_id'];
            $info['branch_id'] = $branchMaster['stocktr_to'];
            $info['is_web'] = 1;
            $info['godown_id'] = 0;
            $info['van_id'] = 0;
            $info['branch_ref_no'] = $branchMaster['stocktr_id'];
            $info['sub_ref_no'] = 0;
            $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            $res2 = Acc_voucher::create($info);

            $info = array();
            $max = Acc_voucher::max('vch_no');
            $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max+1;
            // supplier or cash@branchcode ledger 
            $info['vch_ledger_from'] = $ledgerId;
            //purchase@branchcode
            $info['vch_ledger_to'] = $stockRecLedger['taxled_ledger_id'];
            $info['vch_date'] = $branchMaster['stocktr_date'];
            $info['vch_in'] = $branchMaster['stocktr_amount'];
            $info['vch_out'] = 0;
            // voucher type 16 for Stock Receipt
            $info['vch_vchtype_id'] = 16;
            $info['vch_notes'] = $branchMaster['stocktr_notes'];
            $info['vch_added_by'] = $branchMaster['stocktr_added_by'];
            $info['vch_id2'] = $res2->vch_id;;
            // $info['vch_from_group'] = 3;
            $info['vch_from_group'] = 23;
            $info['vch_flags'] = 1;
            $info['ref_no'] = $branchMaster['stocktr_id'];
            $info['branch_id'] = $branchMaster['stocktr_to'];
            $info['is_web'] = 1;
            $info['godown_id'] = 0;
            $info['van_id'] = 0;
            $info['branch_ref_no'] = $branchMaster['stocktr_id'];
            $info['sub_ref_no'] = 0;
            $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            $res2 = Acc_voucher::create($info);
            

        }
        // var_dump($branchTransfers);
    }

    public function setsalesreturnsubtovoucher($refNo = '') {
        $rltn = [
            'items' => function ($qr) {
                $qr->select('*');
            },
        ];
        $where = array();
        $where[] = ['salesret_flags','=', 1];
        $where[] = ['sync_completed','=', 1];
        if($refNo != ''){
            $where[] = ['salesret_id','=', $refNo];
        }
        $salesReturn = SalesReturnMaster::where($where)->with($rltn)->get()->toArray();
        // echo '<pre>';
        // print_r($salesReturn);
        // exit;
        foreach($salesReturn as $val){
            // Acc_voucher::where('branch_ref_no', $val['salesret_branch_ret_no'])->delete();

            // to get braanch sales ledger, eg:- Sales@FJK
            $where = array();
            $where[] = ['taxled_taxcat_id','=', 0];
            $where[] = ['taxled_branch_id','=', $val['branch_id']];
            // sub category id 1  => Sales, refer erp_tax_category_sub
            $where[] = ['taxled_taxcatsub_id','=', 1];
            $salesLedger = TaxLedgers::where($where)->first();

           if($val['salesret_amount'] != 0) {
               // sales voucher entry 
               $info = array();
               $max = Acc_voucher::max('vch_no');
               $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max+1;
               //sales@branchcode
               $info['vch_ledger_from'] =  $salesLedger['taxled_ledger_id'];
               // customer or cash@branchcode ledger , paytpe => 0-cash, 1-credit
    //            this.payment_types.push({ "type_id": 1, 'name': 'Cash' },
    //   { "type_id": 2, 'name': 'Credit' },
    //   // { "type_id": 3, 'name': 'Credit/Bank' }
    //   );
    // $val['salesret_cust_id'] => is ledger id
    // $val['salesret_acc_ledger_id'] => cash@fjk
               $ledgerId = ($val['salesret_pay_type'] == 1) ? $val['salesret_acc_ledger_id'] : $val['salesret_cust_ledger_id'];
               $info['vch_ledger_to'] = $ledgerId;
               $info['vch_date'] = $val['salesret_date'];
               // i think want to add discount to sales_total
               $info['vch_in'] = 0;
               $info['vch_out'] = $val['salesret_amount'];
               // voucher type 9 for Sales Return
               $info['vch_vchtype_id'] = 9;
               $info['vch_notes'] = ($val['salesret_notes']) ? $val['salesret_notes'] : '';
               $info['vch_added_by'] = (isset($val['salesret_added_by'])) ? $val['salesret_added_by'] : 0;
               $info['vch_id2'] = 0;
               $info['vch_from_group'] = 9;
               $info['vch_flags'] = 1;
               $info['ref_no'] = $val['salesret_no'];
               $info['branch_id'] = $val['branch_id'];
               $info['is_web'] = $val['is_web'];
               $info['godown_id'] = $val['godown_id'];
               $info['van_id'] = $val['van_id'];
               $info['branch_ref_no'] = $val['salesret_branch_ret_no'];
               $info['sub_ref_no'] = 0;
               $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
               $res2 = Acc_voucher::create($info);

               $info = array();
               $max = Acc_voucher::max('vch_no');
               $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max+1;
               // customer or cash@branchcode ledger 
               $info['vch_ledger_from'] = $ledgerId;
               //sales@branchcode
               $info['vch_ledger_to'] = $salesLedger['taxled_ledger_id'];
               $info['vch_date'] = $val['salesret_date'];
               $info['vch_in'] = $val['salesret_amount'];
               $info['vch_out'] = 0;
               // voucher type 9 for Sales Return
               $info['vch_vchtype_id'] = 9;
               $info['vch_notes'] = ($val['salesret_notes']) ? $val['salesret_notes'] : '';
               $info['vch_added_by'] = (isset($val['salesret_added_by'])) ? $val['salesret_added_by'] : 0;
               $info['vch_id2'] = $res2->vch_id;;
               $info['vch_from_group'] = ($val['salesret_pay_type'] == 1) ? 3 : 21;
               $info['vch_flags'] = 1;
               $info['ref_no'] = $val['salesret_no'];
               $info['branch_id'] = $val['branch_id'];
               $info['is_web'] = $val['is_web'];
               $info['godown_id'] = $val['godown_id'];
               $info['van_id'] = $val['van_id'];
               $info['branch_ref_no'] = $val['salesret_branch_ret_no'];
               $info['sub_ref_no'] = 0;
               $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
               $res2 = Acc_voucher::create($info);
           }
           

           if($val['salesret_discount'] != 0){

               $where = array();
               $where[] = ['taxled_taxcat_id','=', 0];
               $where[] = ['taxled_branch_id','=', $val['branch_id']];
               // sub category id 10  => Descount Allowed, refer erp_tax_category_sub
               $where[] = ['taxled_taxcatsub_id','=', 10];
               $discLedger = TaxLedgers::where($where)->first();

               $info = array();
               $max = Acc_voucher::max('vch_no');
               $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max+1;
               //sales@branchcode
               $info['vch_ledger_from'] = $salesLedger['taxled_ledger_id'];
               // discoutallowed@branchcode
               $info['vch_ledger_to'] = $discLedger['taxled_ledger_id'];

               $info['vch_date'] = $val['salesret_date'];
               $info['vch_in'] = 0;
               $info['vch_out'] = $val['salesret_discount'];
               // voucher type 9 for Sales return
               $info['vch_vchtype_id'] = 9;
               $info['vch_notes'] = ($val['salesret_notes']) ? $val['salesret_notes'] : '';
               $info['vch_added_by'] = (isset($val['salesret_added_by'])) ? $val['salesret_added_by'] : 0;
               $info['vch_id2'] = 0;
               $info['vch_from_group'] =9;
               $info['vch_flags'] = 1;
               $info['ref_no'] = $val['salesret_no'];
               $info['branch_id'] = $val['branch_id'];
               $info['is_web'] = $val['is_web'];
               $info['godown_id'] = $val['godown_id'];
               $info['van_id'] = $val['van_id'];
               $info['branch_ref_no'] = $val['salesret_branch_ret_no'];
               $info['sub_ref_no'] = 0;
               $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
               $res2 = Acc_voucher::create($info);

               $info = array();
               $max = Acc_voucher::max('vch_no');
               $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max+1;
       
               // discoutallowed@branchcode
               $info['vch_ledger_from'] = $discLedger['taxled_ledger_id'];
               //sales@branchcode
               $info['vch_ledger_to'] = $salesLedger['taxled_ledger_id'];

               $info['vch_date'] = $val['salesret_date'];
               $info['vch_in'] = $val['salesret_discount'];
               $info['vch_out'] = 0;
               // voucher type 9 for Sales return
               $info['vch_vchtype_id'] = 9;
               $info['vch_notes'] = ($val['salesret_notes']) ? $val['salesret_notes'] : '';
               $info['vch_added_by'] = (isset($val['salesret_added_by'])) ? $val['salesret_added_by'] : 0;
               $info['vch_id2'] = $res2->vch_id;;
               $info['vch_from_group'] = 22;
               $info['vch_flags'] = 1;
               $info['ref_no'] = $val['salesret_no'];
               $info['branch_id'] = $val['branch_id'];
               $info['is_web'] = $val['is_web'];
               $info['godown_id'] = $val['godown_id'];
               $info['van_id'] = $val['van_id'];
               $info['branch_ref_no'] = $val['salesret_branch_ret_no'];
               $info['sub_ref_no'] = 0;
               $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
               $res2 = Acc_voucher::create($info);
           }
           $salesRet = array(); 
           $vat = array(); 
        //    $service = array(); 
           foreach($val['items'] as $val1) {
               // product or service
            //    if($val1['prd_type'] == 1){
                $salesRet[$val1['salesretsub_taxcat_id']] = (isset($salesRet[$val1['salesretsub_taxcat_id']])) ? $salesRet[$val1['salesretsub_taxcat_id']] : 0;
                $salesRet[$val1['salesretsub_taxcat_id']] += ($val1['salesretsub_qty'] * $val1['salesretsub_rate']);
            //    }
               if($val1['salesretsub_tax_per'] != 0){
                   $vat[$val1['salesretsub_taxcat_id']] = (isset($vat[$val1['salesretsub_taxcat_id']])) ? $vat[$val1['salesretsub_taxcat_id']] : 0;
                   $vat[$val1['salesretsub_taxcat_id']] += ($val1['salesretsub_qty'] * $val1['salesretsub_tax_rate']);
               }
           }
           
           foreach($salesRet as $taxCatId => $saleAmt) {
               if($saleAmt != 0){
                   $saleAmt = round($saleAmt, 3);
                   $where = array();
                   $where[] = ['taxled_taxcat_id','=', $taxCatId];
                   $where[] = ['taxled_branch_id','=', $val['branch_id']];
                   // sub category id 1  => Sales, refer erp_tax_category_sub
                   $where[] = ['taxled_taxcatsub_id','=', 1];
                   $taxLedger = TaxLedgers::where($where)
                   ->join('tax_category','tax_category.taxcat_id','tax_ledgers.taxled_taxcat_id')
                   ->first();
               
                   // $info = array();
                   $max = Acc_voucher::max('vch_no');
                   $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max+1;
                   //sales@vatper5%-branchcode
                   $info['vch_ledger_from'] = $taxLedger['taxled_ledger_id'];
                   //sales@branchcode
                   $info['vch_ledger_to'] = $salesLedger['taxled_ledger_id'];
                   $info['vch_date'] = $val['salesret_date'];
                   $info['vch_in'] = 0;
                   $info['vch_out'] = $saleAmt;
                   // voucher type 9 for Sales
                   $info['vch_vchtype_id'] = 9;
                   $info['vch_notes'] = ($val['salesret_notes']) ? $val['salesret_notes'] : '';
                   $info['vch_added_by'] = (isset($val['salesret_added_by'])) ? $val['salesret_added_by'] : 0;
                   $info['vch_id2'] = 0;
                   $info['vch_from_group'] = 9;
                   $info['vch_flags'] = 1;
                   $info['ref_no'] = $val['salesret_no'];
                   $info['branch_id'] = $val['branch_id'];
                   $info['is_web'] = $val['is_web'];
                   $info['godown_id'] = $val['godown_id'];
                   $info['van_id'] = $val['van_id'];
                   $info['branch_ref_no'] = $val['salesret_branch_ret_no'];
                   $info['sub_ref_no'] = 0;
                   $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                   $res2 = Acc_voucher::create($info);

                   $info = array();
                   $max = Acc_voucher::max('vch_no');
                   $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max+1;
                   //sales@branchcode
                   $info['vch_ledger_from'] = $salesLedger['taxled_ledger_id'];
                   //sales@vatper5%-branchcode
                   $info['vch_ledger_to'] = $taxLedger['taxled_ledger_id'];
                   $info['vch_date'] = $val['salesret_date'];
                   $info['vch_in'] = $saleAmt;
                   $info['vch_out'] = 0;
                   // voucher type 9 for Sales return
                   $info['vch_vchtype_id'] = 9;
                   $info['vch_notes'] = ($val['salesret_notes']) ? $val['salesret_notes'] : '';
                   $info['vch_added_by'] = (isset($val['salesret_added_by'])) ? $val['salesret_added_by'] : 0;
                   $info['vch_id2'] = $res2->vch_id;;
                   $info['vch_from_group'] = 9;
                   $info['vch_flags'] = 1;
                   $info['ref_no'] = $val['salesret_no'];
                   $info['branch_id'] = $val['branch_id'];
                   $info['is_web'] = $val['is_web'];
                   $info['godown_id'] = $val['godown_id'];
                   $info['van_id'] = $val['van_id'];
                   $info['branch_ref_no'] = $val['salesret_branch_ret_no'];
                   $info['sub_ref_no'] = 0;
                   $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                   $res2 = Acc_voucher::create($info);
               }

           }
           foreach($vat as $taxCatId => $amt) {
               if($amt != 0) {
                   $amt = round($amt, 3);
                   $where = array();
                   $where[] = ['taxled_taxcat_id','=', $taxCatId];
                   $where[] = ['taxled_branch_id','=', $val['branch_id']];
                   // sub category id 2  => otuput tax, refer erp_tax_category_sub
                   $where[] = ['taxled_taxcatsub_id','=', 2];
                   $taxLedger = TaxLedgers::where($where)
                   ->join('tax_category','tax_category.taxcat_id','tax_ledgers.taxled_taxcat_id')
                   ->first();
               
                   // $info = array();
                   $max = Acc_voucher::max('vch_no');
                   $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max+1;
                   // output tax@5%
                   $info['vch_ledger_from'] = $taxLedger['taxled_ledger_id'];
                   // sales@branchcode
                   $info['vch_ledger_to'] = $salesLedger['taxled_ledger_id'];
                   $info['vch_date'] = $val['salesret_date'];
                   $info['vch_in'] = 0;
                   $info['vch_out'] = $amt;
                   // voucher type 24 for output tax
                   $info['vch_vchtype_id'] = 24;
                   $info['vch_notes'] = ($val['salesret_notes']) ? $val['salesret_notes'] : '';
                   $info['vch_added_by'] = (isset($val['salesret_added_by'])) ? $val['salesret_added_by'] : 0;
                   $info['vch_id2'] = 0;
                   $info['vch_from_group'] = 15;
                   $info['vch_flags'] = 1;
                   $info['ref_no'] = $val['salesret_no'];
                   $info['branch_id'] = $val['branch_id'];
                   $info['is_web'] = $val['is_web'];
                   $info['godown_id'] = $val['godown_id'];
                   $info['van_id'] = $val['van_id'];
                   $info['branch_ref_no'] = $val['salesret_branch_ret_no'];
                   $info['sub_ref_no'] = 0;
                   $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                   $res2 = Acc_voucher::create($info);

                   $info = array();
                   $max = Acc_voucher::max('vch_no');
                   $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max+1;
                   // sales@branchcode
                   $info['vch_ledger_from'] = $salesLedger['taxled_ledger_id'];
                   // output tax@5%
                   $info['vch_ledger_to'] = $taxLedger['taxled_ledger_id'];
                   $info['vch_date'] = $val['salesret_date'];
                   $info['vch_in'] = $amt;
                   $info['vch_out'] = 0;
                   // voucher type 24 for Sales
                   $info['vch_vchtype_id'] = 24;
                   $info['vch_notes'] = ($val['salesret_notes']) ? $val['salesret_notes'] : '';
                   $info['vch_added_by'] = (isset($val['salesret_added_by'])) ? $val['salesret_added_by'] : 0;
                   $info['vch_id2'] = $res2->vch_id;;
                   $info['vch_from_group'] = 9;
                   $info['vch_flags'] = 1;
                   $info['ref_no'] = $val['salesret_no'];
                   $info['branch_id'] = $val['branch_id'];
                   $info['is_web'] = $val['is_web'];
                   $info['godown_id'] = $val['godown_id'];
                   $info['van_id'] = $val['van_id'];
                   $info['branch_ref_no'] = $val['salesret_branch_ret_no'];
                   $info['sub_ref_no'] = 0;
                   $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                   $res2 = Acc_voucher::create($info);
               }

           }
           
       }
        if($refNo != ''){
            return true;
        } else {
            echo 'sales retrun vocher entry Completed';
        }
    }

    public function setpurchasereturntovoucher($refNo = '') {
        $rltn = [
            'items' => function ($qr) {
                $qr->select('*');
            },
        ];
        $where = array();
        $where[] = ['purchret_flag','=', 1];
        if($refNo != ''){
            $where[] = ['purchret_id','=', $refNo];
        }
        $purchaseReturn = PurchaseReturn::where($where)->with($rltn)->get()->toArray();
        // echo '<pre>';
        // print_r($purchaseReturn);
        // exit;
        foreach($purchaseReturn as $val) {

            // Acc_voucher::where('branch_ref_no', $val['purchret_id2'])->delete();

            $where = array();
            $where[] = ['taxled_taxcat_id','=', 0];
            $where[] = ['taxled_branch_id','=', $val['branch_id']];
            // sub category id 3  => Purchase, refer erp_tax_category_sub
            $where[] = ['taxled_taxcatsub_id','=', 3];
            $purchaseLedger = TaxLedgers::where($where)->first();

            if($val['purchret_amount'] != 0) {
                // sales voucher entry 
                $info = array();
                $max = Acc_voucher::max('vch_no');
                $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max+1;
                // purchase@branchcode
                $info['vch_ledger_from'] = $purchaseLedger['taxled_ledger_id'];
                // pay_type = [
                //     { id: 1, name: 'Credit' },
                //     { id: 2, name: 'Cash' }
                //   ];
                if ($val['purchret_pay_type'] == 2) {
                    $where = array();
                    $where[] = ['taxled_taxcat_id','=', 0];
                    $where[] = ['taxled_branch_id','=', $val['branch_id']];
                    // sub category id 9  => Branch Cash, refer erp_tax_category_sub
                    $where[] = ['taxled_taxcatsub_id','=', 9];
                    $CashLedger = TaxLedgers::where($where)->first();
                }
                if ($val['purchret_pay_type'] == 1) {
                    $val['purchret_supp_id']; 
                    $where = array();
                    $where[] = ['supp_id','=', $val['purchret_supp_id']];
                    $suppLedger = Supplier::where($where)->first();
                }

                // Supplier or cash@branchcode ledger , purch_pay_type => 1-credit, 2-cash
                $ledgerId = ($val['purchret_pay_type'] == 2) ? $CashLedger['taxled_ledger_id'] : ((isset($suppLedger['supp_ledger_id'])) ? $suppLedger['supp_ledger_id'] : 0);
                $info['vch_ledger_to'] = $ledgerId;
                $info['vch_date'] = $val['purchret_date'];
                // i think want to add discount to sales_total
                $purchAmt = $val['purchret_amount'];
                $info['vch_in'] = $purchAmt;
                $info['vch_out'] = 0;
                // voucher type 8 for Purchase return
                $info['vch_vchtype_id'] = 8;
                $info['vch_notes'] = ($val['purchret_note']) ? $val['purchret_note'] : '';
                $info['vch_added_by'] = (isset($val['purchret_added_by'])) ? $val['purchret_added_by'] : 0;
                $info['vch_id2'] = 0;
                $info['vch_from_group'] = 10;
                $info['vch_flags'] = 1;
                $info['ref_no'] = $val['purchret_id'];
                $info['branch_id'] = $val['branch_id'];
                $info['is_web'] = 1;
                $info['godown_id'] = $val['purchret_gd_id'];
                $info['van_id'] = 0;
                $info['branch_ref_no'] = $val['purchret_id2'];
                $info['sub_ref_no'] = 0;
                $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                $res2 = Acc_voucher::create($info);

                $info = array();
                $max = Acc_voucher::max('vch_no');
                $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max+1;
                // supplier or cash@branchcode ledger 
                $info['vch_ledger_from'] = $ledgerId;
                //purchase@branchcode
                $info['vch_ledger_to'] = $purchaseLedger['taxled_ledger_id'];
                $info['vch_date'] = $val['purchret_date'];
                $info['vch_in'] = 0;
                $info['vch_out'] = $purchAmt;
                // voucher type 8 for Purchase Return
                $info['vch_vchtype_id'] = 8;
                $info['vch_notes'] = ($val['purchret_note']) ? $val['purchret_note'] : '';;
                $info['vch_added_by'] = (isset($val['purchret_added_by'])) ? $val['purchret_added_by'] : 0;
                $info['vch_id2'] = $res2->vch_id;;
                // $info['vch_from_group'] = 3;
                $info['vch_from_group'] = ($val['purchret_pay_type'] == 2) ? 3 : 20;
                $info['vch_flags'] = 1;
                $info['ref_no'] = $val['purchret_id'];
                $info['branch_id'] = $val['branch_id'];
                $info['is_web'] = 1;
                $info['godown_id'] = $val['purchret_gd_id'];
                $info['van_id'] = 0;
                $info['branch_ref_no'] = $val['purchret_id2'];
                $info['sub_ref_no'] = 0;
                $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                $res2 = Acc_voucher::create($info);
            }

            if($val['purchret_discount'] != 0){

                $where = array();
                $where[] = ['taxled_taxcat_id','=', 0];
                $where[] = ['taxled_branch_id','=', $val['branch_id']];
                // sub category id 10  => Descount Allowed, refer erp_tax_category_sub
                $where[] = ['taxled_taxcatsub_id','=', 10];
                $discLedger = TaxLedgers::where($where)->first();

                $info = array();
                $max = Acc_voucher::max('vch_no');
                $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max+1;
              
                
                
                // purchase@branchcode
                $info['vch_ledger_from'] = $purchaseLedger['taxled_ledger_id'];
                // discoutallowed@branchcode
                $info['vch_ledger_to'] = $discLedger['taxled_ledger_id'];

                $info['vch_date'] = $val['purchret_date'];
                $info['vch_in'] = $val['purchret_discount'];
                $info['vch_out'] = 0;
                // voucher type 8 for Purchase return
                $info['vch_vchtype_id'] = 8;
                $info['vch_notes'] = ($val['purchret_note']) ? $val['purchret_note'] : '';;
                $info['vch_added_by'] = (isset($val['purchret_added_by'])) ? $val['purchret_added_by'] : 0;
                $info['vch_id2'] = 0;
                $info['vch_from_group'] = 10;
                $info['vch_flags'] = 1;
                $info['ref_no'] = $val['purchret_id'];
                $info['branch_id'] = $val['branch_id'];
                $info['is_web'] = 1;
                $info['godown_id'] = $val['purchret_gd_id'];
                $info['van_id'] = 0;
                $info['branch_ref_no'] = $val['purchret_id2'];
                $info['sub_ref_no'] = 0;
                $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                $res2 = Acc_voucher::create($info);

                $info = array();
                $max = Acc_voucher::max('vch_no');
                $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max+1;
        
               
                // discoutallowed@branchcode
                $info['vch_ledger_from'] = $discLedger['taxled_ledger_id'];
                
                 // purchase@branchcode
                $info['vch_ledger_to'] = $purchaseLedger['taxled_ledger_id'];

                $info['vch_date'] = $val['purchret_date'];
                $info['vch_in'] = 0;
                $info['vch_out'] = $val['purchret_discount'];
                // voucher type 5 for Purchase
                $info['vch_vchtype_id'] = 5;
                $info['vch_notes'] = ($val['purchret_note']) ? $val['purchret_note'] : '';
                $info['vch_added_by'] = (isset($val['purchret_added_by'])) ? $val['purchret_added_by'] : 0;
                $info['vch_id2'] = $res2->vch_id;;
                $info['vch_from_group'] = 22;
                $info['vch_flags'] = 1;
                $info['ref_no'] = $val['purchret_id'];
                $info['branch_id'] = $val['branch_id'];
                $info['is_web'] = 1;
                $info['godown_id'] = $val['purchret_gd_id'];
                $info['van_id'] = 0;
                $info['branch_ref_no'] = $val['purchret_id2'];
                $info['sub_ref_no'] = 0;
                $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                $res2 = Acc_voucher::create($info);
            }


            $purchaseret = array(); 
            $vat = array(); 
            foreach($val['items'] as $val1) {
                
                $purchaseret[$val1['purchretsub_taxcat_id']] = (isset($purchaseret[$val1['purchretsub_taxcat_id']])) ? $purchaseret[$val1['purchretsub_taxcat_id']] : 0;
                $purchaseret[$val1['purchretsub_taxcat_id']] += ($val1['purchretsub_qty'] * $val1['purchretsub_rate']);
                
                if($val1['purchretsub_tax_per'] != 0) {
                    $vat[$val1['purchretsub_taxcat_id']] = (isset($vat[$val1['purchretsub_taxcat_id']])) ? $vat[$val1['purchretsub_taxcat_id']] : 0;
                    $vat[$val1['purchretsub_taxcat_id']] += ($val1['purchretsub_qty'] * $val1['purchretsub_tax']);
                }
            }

            foreach($purchaseret as $taxCatId => $amt) {
                if($amt != 0){
                    $amt = round($amt, 3);
                    $where = array();
                    // $where[] = ['taxcat_tax_per','=', $taxCatId];
                    $where[] = ['taxled_taxcat_id','=', $taxCatId];
                    $where[] = ['taxled_branch_id','=', $val['branch_id']];
                    // sub category id 3  => Purchase, refer erp_tax_category_sub
                    $where[] = ['taxled_taxcatsub_id','=', 3];
                    $taxLedger = TaxLedgers::where($where)
                    ->join('tax_category','tax_category.taxcat_id','tax_ledgers.taxled_taxcat_id')
                    ->first();
                
                    // $info = array();
                    $max = Acc_voucher::max('vch_no');
                    $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max+1;
                    //purchase@vatper5%-branchcode
                    $info['vch_ledger_from'] = $taxLedger['taxled_ledger_id'];
                    //sales@branchcode
                    $info['vch_ledger_to'] = $purchaseLedger['taxled_ledger_id'];
                    $info['vch_date'] = $val['purchret_date'];
                    $info['vch_in'] = $amt;
                    $info['vch_out'] = 0;
                    // voucher type 8 for Purchase return
                    $info['vch_vchtype_id'] = 8;
                    $info['vch_notes'] = ($val['purchret_note']) ? $val['purchret_note'] : '';;
                    $info['vch_added_by'] = (isset($val['purchret_added_by'])) ? $val['purchret_added_by'] : 0;
                    $info['vch_id2'] = 0;
                    $info['vch_from_group'] = 10;
                    $info['vch_flags'] = 1;
                    $info['ref_no'] = $val['purchret_id'];
                    $info['branch_id'] = $val['branch_id'];
                    $info['is_web'] = 1;
                    $info['godown_id'] = $val['purchret_gd_id'];
                    $info['van_id'] = 0;
                    $info['branch_ref_no'] = $val['purchret_id2'];
                    $info['sub_ref_no'] = 0;
                    $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                    $res2 = Acc_voucher::create($info);

                    $info = array();
                    $max = Acc_voucher::max('vch_no');
                    $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max+1;
                    //purchase@branchcode
                    $info['vch_ledger_from'] = $purchaseLedger['taxled_ledger_id'];
                    //purchase@vatper5%-branchcode
                    $info['vch_ledger_to'] = $taxLedger['taxled_ledger_id'];
                    $info['vch_date'] = $val['purchret_date'];
                    $info['vch_in'] = 0;
                    $info['vch_out'] = $amt;
                    // voucher type 8 for purchase return
                    $info['vch_vchtype_id'] = 8;
                    $info['vch_notes'] = ($val['purchret_note']) ? $val['purchret_note'] : '';;
                    $info['vch_added_by'] = (isset($val['purchret_added_by'])) ? $val['purchret_added_by'] : 0;
                    $info['vch_id2'] = $res2->vch_id;;
                    $info['vch_from_group'] = 10;
                    $info['vch_flags'] = 1;
                    $info['ref_no'] = $val['purchret_id'];
                    $info['branch_id'] = $val['branch_id'];
                    $info['is_web'] = 1;
                    $info['godown_id'] = $val['purchret_gd_id'];
                    $info['van_id'] = 0;
                    $info['branch_ref_no'] = $val['purchret_id2'];
                    $info['sub_ref_no'] = 0;
                    $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                    $res2 = Acc_voucher::create($info);
                }

            }
            foreach($vat as $taxCatId => $amt) {
                if($amt != 0) {
                    $amt = round($amt, 3);
                    $where = array();
                    // $where[] = ['taxcat_tax_per','=', $taxCatId];
                    $where[] = ['taxled_taxcat_id','=', $taxCatId];
                    $where[] = ['taxled_branch_id','=', $val['branch_id']];
                    // sub category id 4  => input tax, refer erp_tax_category_sub
                    $where[] = ['taxled_taxcatsub_id','=', 4];
                    $taxLedger = TaxLedgers::where($where)
                    ->join('tax_category','tax_category.taxcat_id','tax_ledgers.taxled_taxcat_id')
                    ->first();
                
                    // $info = array();
                    $max = Acc_voucher::max('vch_no');
                    $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max+1;
                    //input tax@5%
                    $info['vch_ledger_from'] = $taxLedger['taxled_ledger_id'];
                    //purchase@branchcode
                    $info['vch_ledger_to'] = $purchaseLedger['taxled_ledger_id'];
                    $info['vch_date'] = $val['purchret_date'];
                    $info['vch_in'] = $amt;
                    $info['vch_out'] = 0;
                    // voucher type 23 for input tax
                    $info['vch_vchtype_id'] = 23;
                    $info['vch_notes'] = ($val['purchret_note']) ? $val['purchret_note'] : '';;
                    $info['vch_added_by'] = (isset($val['purchret_added_by'])) ? $val['purchret_added_by'] : 0;
                    $info['vch_id2'] = 0;
                    $info['vch_from_group'] = 15;
                    $info['vch_flags'] = 1;
                    $info['ref_no'] = $val['purchret_id'];
                    $info['branch_id'] = $val['branch_id'];
                    $info['is_web'] = 1;
                    $info['godown_id'] = $val['purchret_gd_id'];
                    $info['van_id'] = 0;
                    $info['branch_ref_no'] = $val['purchret_id2'];;
                    $info['sub_ref_no'] = 0;
                    $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                    $res2 = Acc_voucher::create($info);

                    $info = array();
                    $max = Acc_voucher::max('vch_no');
                    $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max+1;
                    //purchase@branchcode
                    $info['vch_ledger_from'] = $purchaseLedger['taxled_ledger_id'];
                    //input tax@5%
                    $info['vch_ledger_to'] = $taxLedger['taxled_ledger_id'];
                    $info['vch_date'] = $val['purchret_date'];
                    $info['vch_in'] = 0;
                    $info['vch_out'] = $amt;
                    // voucher type 23 for input tax
                    $info['vch_vchtype_id'] = 23;
                    $info['vch_notes'] = ($val['purchret_note']) ? $val['purchret_note'] : '';;
                    $info['vch_added_by'] = (isset($val['purchret_added_by'])) ? $val['purchret_added_by'] : 0;
                    $info['vch_id2'] = $res2->vch_id;;
                    $info['vch_from_group'] = 10;
                    $info['vch_flags'] = 1;
                    $info['ref_no'] = $val['purchret_id'];
                    $info['branch_id'] = $val['branch_id'];
                    $info['is_web'] = 1;
                    $info['godown_id'] = $val['purchret_gd_id'];
                    $info['van_id'] = 0;
                    $info['branch_ref_no'] = $val['purchret_id2'];
                    $info['sub_ref_no'] = 0;
                    $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                    $res2 = Acc_voucher::create($info);
                }

            }

        }
        if($refNo != ''){
            return true;
        } else{
            echo 'purchase retrun voucher entry Completed';
        }
    }

    
    public function setpurchasetovoucher($refNo = '') {

        $rltn = [
            'items' => function ($qr) {
                $qr->select('*');
            },
        ];
        $where = array();
        $where[] = ['purch_flag','=', 1];
        if($refNo != ''){
            $where[] = ['purch_id','=', $refNo];
        }
        $purchase = Purchase::where($where)->with($rltn)->get()->toArray();
        // echo '<pre>';
        // print_r($purchase);
        // exit;
        // $purchase = Purchase::with($rltn)->get()->toArray();

        // different purchase types
        // { id: 1, name: 'Standard Rated Domestic' },
        // { id: 2, name: 'Zero Rated Domestic' },
        // { id: 3, name: 'Exempt pruchase' },
        // { id: 4, name: 'Imports Vat Paid to Custom' },
        // { id: 5, name: 'Imports Vat - reverse change mechanism' }
        foreach($purchase as $val) {

            // Acc_voucher::where('branch_ref_no', $val['purch_id2'])->delete();

            $where = array();
            $where[] = ['taxled_taxcat_id','=', 0];
            $where[] = ['taxled_branch_id','=', $val['branch_id']];
            // sub category id 3  => Purchase, refer erp_tax_category_sub
            $where[] = ['taxled_taxcatsub_id','=', 3];
            $purchaseLedger = TaxLedgers::where($where)->first();

            if($val['purch_amount'] != 0) {
                // sales voucher entry 
                $info = array();
                $max = Acc_voucher::max('vch_no');
                $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max+1;
                // purchase@branchcode
                $info['vch_ledger_from'] = $purchaseLedger['taxled_ledger_id'];

                // purchasePayType = [
                //     { id: 1, name: 'Credit' },
                //     { id: 2, name: 'Cash' }
                   
                //   ];
                if ($val['purch_pay_type'] == 2) {
                    // chk any cash ledger selected from ui, in old app no option to choose cash ledger
                    if($val['purch_acc_ledger_id'] != 0) {
                        $CashLedger['taxled_ledger_id'] = $val['purch_acc_ledger_id'];
                    } else {
                        $where = array();
                        $where[] = ['taxled_taxcat_id','=', 0];
                        $where[] = ['taxled_branch_id','=', $val['branch_id']];
                        // sub category id 9  => Branch Cash, refer erp_tax_category_sub
                        $where[] = ['taxled_taxcatsub_id','=', 9];
                        $CashLedger = TaxLedgers::where($where)->first();
                    }
                }

                if ($val['purch_pay_type'] == 1) {
                    $where = array();
                    $where[] = ['supp_id','=', $val['purch_supp_id']];
                    $suppLedger = Supplier::where($where)->first();
                }

               
                // Supplier or cash@branchcode ledger , purch_pay_type => 1-credit, 2-cash
                $ledgerId = ($val['purch_pay_type'] == 2) ? $CashLedger['taxled_ledger_id'] : $suppLedger['supp_ledger_id'];
                $info['vch_ledger_to'] = $ledgerId;
                $info['vch_date'] = $val['purch_date'];
       
                $purchAmt = $val['purch_amount'] - $val['purch_frieght'];
                $purchAmt = ($val['purch_type'] == 4) ? $purchAmt - $val['purch_tax'] : $purchAmt;
                $info['vch_in'] = 0;
                $info['vch_out'] = $purchAmt;
                // voucher type 5 for Purchase
                $info['vch_vchtype_id'] = 5;
                $info['vch_notes'] = ($val['purch_note']) ? $val['purch_note'] : '';
                $info['vch_added_by'] = (isset($val['purch_added_by'])) ? $val['purch_added_by'] : 0;
                $info['vch_id2'] = 0;
                $info['vch_from_group'] = 10;
                $info['vch_flags'] = 1;
                $info['ref_no'] = $val['purch_id'];
                $info['branch_id'] = $val['branch_id'];
                $info['is_web'] = 1;
                $info['godown_id'] = 0;
                $info['van_id'] = 0;
                $info['branch_ref_no'] = $val['purch_id2'];
                $info['sub_ref_no'] = 0;
                $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                $res2 = Acc_voucher::create($info);

                $info = array();
                $max = Acc_voucher::max('vch_no');
                $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max+1;
                // supplier or cash@branchcode ledger 
                $info['vch_ledger_from'] = $ledgerId;
                //purchase@branchcode
                $info['vch_ledger_to'] = $purchaseLedger['taxled_ledger_id'];
                $info['vch_date'] = $val['purch_date'];
                $info['vch_in'] = $purchAmt;
                $info['vch_out'] = 0;
                // voucher type 5 for Purchase
                $info['vch_vchtype_id'] = 5;
                $info['vch_notes'] = ($val['purch_note']) ? $val['purch_note'] : '';;
                $info['vch_added_by'] = (isset($val['purch_added_by'])) ? $val['purch_added_by'] : 0;
                $info['vch_id2'] = $res2->vch_id;;
                // $info['vch_from_group'] = 3;
                $info['vch_from_group'] = ($val['purch_pay_type'] == 2) ? 3 : 20;
                $info['vch_flags'] = 1;
                $info['ref_no'] = $val['purch_id'];
                $info['branch_id'] = $val['branch_id'];
                $info['is_web'] = 1;
                $info['godown_id'] = 0;
                $info['van_id'] = 0;
                $info['branch_ref_no'] = $val['purch_id2'];
                $info['sub_ref_no'] = 0;
                $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                $res2 = Acc_voucher::create($info);
            }

            if($val['purch_discount'] != 0){

                $where = array();
                $where[] = ['taxled_taxcat_id','=', 0];
                $where[] = ['taxled_branch_id','=', $val['branch_id']];
                // sub category id 10  => Descount Allowed, refer erp_tax_category_sub
                $where[] = ['taxled_taxcatsub_id','=', 10];
                $discLedger = TaxLedgers::where($where)->first();

                $info = array();
                $max = Acc_voucher::max('vch_no');
                $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max+1;
              
                
                
                // purchase@branchcode
                $info['vch_ledger_from'] = $purchaseLedger['taxled_ledger_id'];
                // discoutallowed@branchcode
                $info['vch_ledger_to'] = $discLedger['taxled_ledger_id'];

                $info['vch_date'] = $val['purch_date'];
                $info['vch_in'] = 0;
                $info['vch_out'] = $val['purch_discount'];
                // voucher type 5 for Purchase
                $info['vch_vchtype_id'] = 5;
                $info['vch_notes'] = ($val['purch_note']) ? $val['purch_note'] : '';;
                $info['vch_added_by'] = (isset($val['purch_added_by'])) ? $val['purch_added_by'] : 0;
                $info['vch_id2'] = 0;
                $info['vch_from_group'] = 10;
                $info['vch_flags'] = 1;
                $info['ref_no'] = $val['purch_id'];
                $info['branch_id'] = $val['branch_id'];
                $info['is_web'] = 1;
                $info['godown_id'] = 0;
                $info['van_id'] = 0;
                $info['branch_ref_no'] = $val['purch_id2'];
                $info['sub_ref_no'] = 0;
                $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                $res2 = Acc_voucher::create($info);

                $info = array();
                $max = Acc_voucher::max('vch_no');
                $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max+1;
                
                // discoutallowed@branchcode
                $info['vch_ledger_from'] = $discLedger['taxled_ledger_id'];
                // purchase@branchcode
                $info['vch_ledger_to'] = $purchaseLedger['taxled_ledger_id'];

                $info['vch_date'] = $val['purch_date'];
                $info['vch_in'] = $val['purch_discount'];
                $info['vch_out'] = 0;
                // voucher type 5 for Purchase
                $info['vch_vchtype_id'] = 5;
                $info['vch_notes'] = ($val['purch_note']) ? $val['purch_note'] : '';
                $info['vch_added_by'] = (isset($val['purch_added_by'])) ? $val['purch_added_by'] : 0;
                $info['vch_id2'] = $res2->vch_id;;
                $info['vch_from_group'] = 22;
                $info['vch_flags'] = 1;
                $info['ref_no'] = $val['purch_id'];
                $info['branch_id'] = $val['branch_id'];
                $info['is_web'] = 1;
                $info['godown_id'] = 0;
                $info['van_id'] = 0;
                $info['branch_ref_no'] = $val['purch_id2'];
                $info['sub_ref_no'] = 0;
                $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                $res2 = Acc_voucher::create($info);
            }

            if($val['purch_frieght'] != 0) {

                $where = array();
                $where[] = ['taxled_taxcat_id','=', 0];
                $where[] = ['taxled_branch_id','=', $val['branch_id']];
                // sub category id 11  => purchase frieght a/c, refer erp_tax_category_sub
                $where[] = ['taxled_taxcatsub_id','=', 11];
                $puchFrieghtLedger = TaxLedgers::where($where)->first();

                $info = array();
                $max = Acc_voucher::max('vch_no');
                $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max+1;
            
                // purchase frieght A/c@branchcode
                $info['vch_ledger_from'] = $puchFrieghtLedger['taxled_ledger_id'];    
                // selected frieght ledger id
                $info['vch_ledger_to'] = $val['purch_frieght_ledger_id'];
               
                

                $info['vch_date'] = $val['purch_date'];
                $info['vch_in'] = 0;
                $info['vch_out'] = $val['purch_frieght'];
                // voucher type 5 for Purchase
                $info['vch_vchtype_id'] = 5;
                $info['vch_notes'] = 'Purch.Freight - Ref No: ' . $val['purch_id'];
                $info['vch_added_by'] = (isset($val['purch_added_by'])) ? $val['purch_added_by'] : 0;
                $info['vch_id2'] = 0;
                $info['vch_from_group'] = 10;
                $info['vch_flags'] = 1;
                $info['ref_no'] = $val['purch_id'];
                $info['branch_id'] = $val['branch_id'];
                $info['is_web'] = 1;
                $info['godown_id'] = 0;
                $info['van_id'] = 0;
                $info['branch_ref_no'] = $val['purch_id2'];
                $info['sub_ref_no'] = 0;
                $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                $res2 = Acc_voucher::create($info);

                $info = array();
                $max = Acc_voucher::max('vch_no');
                $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max+1;
                
                // selected frieght ledger id
                $info['vch_ledger_from'] = $val['purch_frieght_ledger_id'];
                // purchase frieght A/c@branchcode
                $info['vch_ledger_to'] = $puchFrieghtLedger['taxled_ledger_id'];

                // to get from ledger group
                $where = array();
                $where[] = ['ledger_id','=', $val['purch_frieght_ledger_id']];
                $ledgerDetails = Acc_ledger::where($where)->first();

                $info['vch_date'] = $val['purch_date'];
                $info['vch_in'] = $val['purch_frieght'];
                $info['vch_out'] = 0;
                // voucher type 5 for Purchase
                $info['vch_vchtype_id'] = 5;
                $info['vch_notes'] = 'Purch.Freight - Ref No: ' . $val['purch_id'];
                $info['vch_added_by'] = (isset($val['purch_added_by'])) ? $val['purch_added_by'] : 0;
                $info['vch_id2'] = $res2->vch_id;;
                $info['vch_from_group'] = ($ledgerDetails['ledger_accgrp_id']) ? $ledgerDetails['ledger_accgrp_id']: 0;
                $info['vch_flags'] = 1;
                $info['ref_no'] = $val['purch_id'];
                $info['branch_id'] = $val['branch_id'];
                $info['is_web'] = 1;
                $info['godown_id'] = 0;
                $info['van_id'] = 0;
                $info['branch_ref_no'] = $val['purch_id2'];
                $info['sub_ref_no'] = 0;
                $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                $res2 = Acc_voucher::create($info);
            }

           // $purchAmt = ($val['purch_type'] == 4) ? $val['purch_amount'] : $val['purch_amount'] + $val['purch_tax'];
           // in case tax into differnt ledger , then first move to purchase@branch code then desired ledger id 
           if($val['purch_type'] == 4) {

                $info = array();
                $max = Acc_voucher::max('vch_no');
                $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max+1;
                // purchase@branchcode
                $info['vch_ledger_from'] = $purchaseLedger['taxled_ledger_id'];
                // selected tax ledger id
                $info['vch_ledger_to'] = $val['purch_tax_ledger_id'];
                $info['vch_date'] = $val['purch_date'];
                $info['vch_in'] = 0;
                $info['vch_out'] = $val['purch_tax'];
                // voucher type 5 for Purchase
                $info['vch_vchtype_id'] = 5;
                $info['vch_notes'] = ($val['purch_note']) ? $val['purch_note'] : '';
                $info['vch_added_by'] = (isset($val['purch_added_by'])) ? $val['purch_added_by'] : 0;
                $info['vch_id2'] = 0;
                $info['vch_from_group'] = 10;
                $info['vch_flags'] = 1;
                $info['ref_no'] = $val['purch_id'];
                $info['branch_id'] = $val['branch_id'];
                $info['is_web'] = 1;
                $info['godown_id'] = 0;
                $info['van_id'] = 0;
                $info['branch_ref_no'] = $val['purch_id2'];
                $info['sub_ref_no'] = 0;
                $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                $res2 = Acc_voucher::create($info);

                $info = array();
                $max = Acc_voucher::max('vch_no');
                $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max+1;
        
                // purchase frieght A/c@branchcode
                $info['vch_ledger_from'] = $val['purch_tax_ledger_id'];

                // to get from ledger group
                $where = array();
                $where[] = ['ledger_id','=', $val['purch_tax_ledger_id']];
                $ledgerDetails = Acc_ledger::where($where)->first();
                // selected frieght ledger id
                
                $info['vch_ledger_to'] = $purchaseLedger['taxled_ledger_id'];

                $info['vch_date'] = $val['purch_date'];
                $info['vch_in'] = $val['purch_tax'];
                $info['vch_out'] = 0;
                // voucher type 5 for Purchase
                $info['vch_vchtype_id'] = 5;
                $info['vch_notes'] = 'Purch.Freight - Ref No: ' . $val['purch_id'];
                $info['vch_added_by'] = (isset($val['purch_added_by'])) ? $val['purch_added_by'] : 0;
                $info['vch_id2'] = $res2->vch_id;;
                $info['vch_from_group'] = ($ledgerDetails['ledger_accgrp_id']) ? $ledgerDetails['ledger_accgrp_id']: 0;
                $info['vch_flags'] = 1;
                $info['ref_no'] = $val['purch_id'];
                $info['branch_id'] = $val['branch_id'];
                $info['is_web'] = 1;
                $info['godown_id'] = 0;
                $info['van_id'] = 0;
                $info['branch_ref_no'] = $val['purch_id2'];
                $info['sub_ref_no'] = 0;
                $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                $res2 = Acc_voucher::create($info);
            }

            $purchase = array(); 
            $vat = array(); 
            foreach($val['items'] as $val1) {
                
                $purchase[$val1['purchsub_taxcat_id']] = (isset($purchase[$val1['purchsub_taxcat_id']])) ? $purchase[$val1['purchsub_taxcat_id']] : 0;
                $purchase[$val1['purchsub_taxcat_id']] += ($val1['purchsub_qty'] * $val1['purchsub_rate']);
                
                if($val1['purchsub_tax_per'] != 0) {
                    $vat[$val1['purchsub_taxcat_id']] = (isset($vat[$val1['purchsub_taxcat_id']])) ? $vat[$val1['purchsub_taxcat_id']] : 0;
                    $vat[$val1['purchsub_taxcat_id']] += ($val1['purchsub_qty'] * $val1['purchsub_tax']);
                }
            }

            foreach($purchase as $taxCatId => $amt) {
                if($amt != 0){
                    $amt = round($amt, 3);
                    $where = array();
                    // $where[] = ['taxcat_tax_per','=', $taxCatId];
                    $where[] = ['taxled_taxcat_id','=', $taxCatId];
                    $where[] = ['taxled_branch_id','=', $val['branch_id']];
                    // sub category id 3  => Purchase, refer erp_tax_category_sub
                    $where[] = ['taxled_taxcatsub_id','=', 3];
                    $taxLedger = TaxLedgers::where($where)
                    ->join('tax_category','tax_category.taxcat_id','tax_ledgers.taxled_taxcat_id')
                    ->first();
                
                    // $info = array();
                    $max = Acc_voucher::max('vch_no');
                    $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max+1;
                     //purchase@vatper5%-branchcode
                     $info['vch_ledger_from'] = $taxLedger['taxled_ledger_id'];
                    //sales@branchcode
                    $info['vch_ledger_to'] = $purchaseLedger['taxled_ledger_id'];
                   
                    $info['vch_date'] = $val['purch_date'];
                    $info['vch_in'] = 0;
                    $info['vch_out'] = $amt;
                    // voucher type 5 for Purchase
                    $info['vch_vchtype_id'] = 5;
                    $info['vch_notes'] = ($val['purch_note']) ? $val['purch_note'] : '';;
                    $info['vch_added_by'] = (isset($val['purch_added_by'])) ? $val['purch_added_by'] : 0;
                    $info['vch_id2'] = 0;
                    $info['vch_from_group'] = 10;
                    $info['vch_flags'] = 1;
                    $info['ref_no'] = $val['purch_id'];
                    $info['branch_id'] = $val['branch_id'];
                    $info['is_web'] = 1;
                    $info['godown_id'] = 0;
                    $info['van_id'] = 0;
                    $info['branch_ref_no'] = $val['purch_id2'];
                    $info['sub_ref_no'] = 0;
                    $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                    $res2 = Acc_voucher::create($info);

                    $info = array();
                    $max = Acc_voucher::max('vch_no');
                    $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max+1;
                    //purchase@branchcode
                    $info['vch_ledger_from'] = $purchaseLedger['taxled_ledger_id'];
                    //purchase@vatper5%-branchcode
                    $info['vch_ledger_to'] = $taxLedger['taxled_ledger_id'];
                    
                    $info['vch_date'] = $val['purch_date'];
                    $info['vch_in'] = $amt;
                    $info['vch_out'] = 0;
                    // voucher type 5 for Purchase
                    $info['vch_vchtype_id'] = 5;
                    $info['vch_notes'] = ($val['purch_note']) ? $val['purch_note'] : '';;
                    $info['vch_added_by'] = (isset($val['purch_added_by'])) ? $val['purch_added_by'] : 0;
                    $info['vch_id2'] = $res2->vch_id;;
                    $info['vch_from_group'] = 10;
                    $info['vch_flags'] = 1;
                    $info['ref_no'] = $val['purch_id'];
                    $info['branch_id'] = $val['branch_id'];
                    $info['is_web'] = 1;
                    $info['godown_id'] = 0;
                    $info['van_id'] = 0;
                    $info['branch_ref_no'] = $val['purch_id2'];
                    $info['sub_ref_no'] = 0;
                    $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                    $res2 = Acc_voucher::create($info);
                }

            }
            foreach($vat as $taxCatId => $amt) {
                if($amt != 0) {
                    $amt = round($amt, 3);
                    $where = array();
                    // $where[] = ['taxcat_tax_per','=', $taxCatId];
                    $where[] = ['taxled_taxcat_id','=', $taxCatId];
                    $where[] = ['taxled_branch_id','=', $val['branch_id']];
                    // sub category id 4  => input tax, refer erp_tax_category_sub
                    $where[] = ['taxled_taxcatsub_id','=', 4];
                    $taxLedger = TaxLedgers::where($where)
                    ->join('tax_category','tax_category.taxcat_id','tax_ledgers.taxled_taxcat_id')
                    ->first();
                
                    // $info = array();
                    $max = Acc_voucher::max('vch_no');
                    $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max+1;
                    //input tax@5%-branchcode
                    $info['vch_ledger_from'] = $taxLedger['taxled_ledger_id'];
                    //purchase@branchcode
                    $info['vch_ledger_to'] = $purchaseLedger['taxled_ledger_id'];
                    
                    $info['vch_date'] = $val['purch_date'];
                    $info['vch_in'] = 0;
                    $info['vch_out'] = $amt;
                    // voucher type 23 for input tax
                    $info['vch_vchtype_id'] = 23;
                    $info['vch_notes'] = ($val['purch_note']) ? $val['purch_note'] : '';;
                    $info['vch_added_by'] = (isset($val['purch_added_by'])) ? $val['purch_added_by'] : 0;
                    $info['vch_id2'] = 0;
                    $info['vch_from_group'] = 15;
                    $info['vch_flags'] = 1;
                    $info['ref_no'] = $val['purch_id'];
                    $info['branch_id'] = $val['branch_id'];
                    $info['is_web'] = 1;
                    $info['godown_id'] = 0;
                    $info['van_id'] = 0;
                    $info['branch_ref_no'] = $val['purch_id2'];;
                    $info['sub_ref_no'] = 0;
                    $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                    $res2 = Acc_voucher::create($info);

                    $info = array();
                    $max = Acc_voucher::max('vch_no');
                    $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max+1;
                    //purchase@branchcode
                    $info['vch_ledger_from'] = $purchaseLedger['taxled_ledger_id'];
                    //input tax@5%-branchcode
                    $info['vch_ledger_to'] = $taxLedger['taxled_ledger_id'];
                    
                    $info['vch_date'] = $val['purch_date'];
                    $info['vch_in'] = $amt;
                    $info['vch_out'] = 0;
                    // voucher type 23 for input tax
                    $info['vch_vchtype_id'] = 23;
                    $info['vch_notes'] = ($val['purch_note']) ? $val['purch_note'] : '';;
                    $info['vch_added_by'] = (isset($val['purch_added_by'])) ? $val['purch_added_by'] : 0;
                    $info['vch_id2'] = $res2->vch_id;;
                    $info['vch_from_group'] = 10;
                    $info['vch_flags'] = 1;
                    $info['ref_no'] = $val['purch_id'];
                    $info['branch_id'] = $val['branch_id'];
                    $info['is_web'] = 1;
                    $info['godown_id'] = 0;
                    $info['van_id'] = 0;
                    $info['branch_ref_no'] = $val['purch_id2'];
                    $info['sub_ref_no'] = 0;
                    $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                    $res2 = Acc_voucher::create($info);
                }

            }

        }
        if($refNo != ''){
            return true;
        }else{
            echo 'Purchase vocher entry compled successfully';
        }
    }

    public function setsalessubtovoucher($refNo = ''){
        
        $rltn = [
            'sales_sub_and_prod' => function ($qr) {
                $qr->select('*');
            },
        ];
        $where = array();
        $where[] = ['sales_flags','=', 1];
        $where[] = ['sync_completed','=', 1];
        if($refNo != '') {
            $where[] = ['sales_inv_no','=', $refNo];
        }
        $saleSub = SalesMaster::where($where)->with($rltn)->get()->toArray();
        // echo '<pre>';
        // print_r($saleSub);
        // exit;
        foreach($saleSub as $val){
            // Acc_voucher::where('branch_ref_no', $val['sales_branch_inv'])->delete();

             // to get braanch sales ledger, eg:- Sales@FJK
             $where = array();
             $where[] = ['taxled_taxcat_id','=', 0];
             $where[] = ['taxled_branch_id','=', $val['branch_id']];
             // sub category id 1  => Sales, refer erp_tax_category_sub
             $where[] = ['taxled_taxcatsub_id','=', 1];
             $salesLedger = TaxLedgers::where($where)->first();

            if($val['sales_total'] != 0) {
                // sales voucher entry 
                $info = array();
                $max = Acc_voucher::max('vch_no');
                $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max+1;
                //sales@branchcode
                $info['vch_ledger_from'] =  $salesLedger['taxled_ledger_id'];
                // customer or cash@branchcode ledger , paytpe => 0-cash, 1-credit
                if(($val['van_id'] == 0)){
                    $ledgerId = ($val['sales_pay_type'] == 1) ? $val['sales_cust_ledger_id'] : $val['sales_acc_ledger_id'];
                } else {
                    // cash@van
                    $where = array();
                    $where[] = ['van_id','=', $val['van_id']];
                    $van = Van::where($where)->first();
                    $ledgerId = ($val['sales_pay_type'] == 1) ? $val['sales_cust_ledger_id'] : $van['van_cash_ledger_id'];
                }
                // $ledgerId = ($val['sales_pay_type'] == 0) ? (($val['van_id'] == 0) ? $val['sales_acc_ledger_id'] : '') : $val['sales_cust_ledger_id'];
                $info['vch_ledger_to'] = $ledgerId;
                $info['vch_date'] = $val['sales_date'];
                // i think want to add discount to sales_total
                $info['vch_in'] = $val['sales_total'];
                $info['vch_out'] = 0;
                // voucher type 4 for Sales
                $info['vch_vchtype_id'] = 4;
                $info['vch_notes'] = $val['sales_notes'];
                $info['vch_added_by'] = $val['sales_added_by'];
                $info['vch_id2'] = 0;
                $info['vch_from_group'] = 9;
                $info['vch_flags'] = 1;
                $info['ref_no'] = $val['sales_inv_no'];
                $info['branch_id'] = $val['branch_id'];
                $info['is_web'] = $val['is_web'];
                $info['godown_id'] = $val['godown_id'];
                $info['van_id'] = $val['van_id'];
                $info['branch_ref_no'] = $val['sales_branch_inv'];
                $info['sub_ref_no'] = 0;
                $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                $res2 = Acc_voucher::create($info);

                $info = array();
                $max = Acc_voucher::max('vch_no');
                $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max+1;

                $where = array();
                $where[] = ['ledger_id','=', $ledgerId];
                $ledgerDetails = Acc_ledger::where($where)->first();
                // customer or cash@branchcode ledger 
                $info['vch_ledger_from'] = $ledgerId;
                //sales@branchcode
                $info['vch_ledger_to'] = $salesLedger['taxled_ledger_id'];
                $info['vch_date'] = $val['sales_date'];
                $info['vch_in'] = 0;
                $info['vch_out'] = $val['sales_total'];
                // voucher type 4 for Sales
                $info['vch_vchtype_id'] = 4;
                $info['vch_notes'] = $val['sales_notes'];
                $info['vch_added_by'] = $val['sales_added_by'];
                $info['vch_id2'] = $res2->vch_id;;
                $info['vch_from_group'] = ($ledgerDetails['ledger_accgrp_id']) ? $ledgerDetails['ledger_accgrp_id']: 0;
                $info['vch_flags'] = 1;
                $info['ref_no'] = $val['sales_inv_no'];
                $info['branch_id'] = $val['branch_id'];
                $info['is_web'] = $val['is_web'];
                $info['godown_id'] = $val['godown_id'];
                $info['van_id'] = $val['van_id'];
                $info['branch_ref_no'] = $val['sales_branch_inv'];
                $info['sub_ref_no'] = 0;
                $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                $res2 = Acc_voucher::create($info);
            }
            

            if($val['sales_discount'] != 0){

                $where = array();
                $where[] = ['taxled_taxcat_id','=', 0];
                $where[] = ['taxled_branch_id','=', $val['branch_id']];
                // sub category id 10  => Descount Allowed, refer erp_tax_category_sub
                $where[] = ['taxled_taxcatsub_id','=', 10];
                $discLedger = TaxLedgers::where($where)->first();

                $info = array();
                $max = Acc_voucher::max('vch_no');
                $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max+1;
                // first 
                // // discoutallowed@branchcode
                // $info['vch_ledger_from'] =  $discLedger['taxled_ledger_id'];
                // // customer or cash@branchcode ledger 
                // $info['vch_ledger_to'] = $ledgerId;

                // modified on 28-01-2020
                //sales@branchcode
                $info['vch_ledger_from'] = $salesLedger['taxled_ledger_id'];
                // discoutallowed@branchcode
                $info['vch_ledger_to'] = $discLedger['taxled_ledger_id'];

                $info['vch_date'] = $val['sales_date'];
                $info['vch_in'] = $val['sales_discount'];
                $info['vch_out'] = 0;
                // voucher type 4 for Sales
                $info['vch_vchtype_id'] = 4;
                $info['vch_notes'] = $val['sales_notes'];
                $info['vch_added_by'] = $val['sales_added_by'];
                $info['vch_id2'] = 0;
                $info['vch_from_group'] = 9;
                $info['vch_flags'] = 1;
                $info['ref_no'] = $val['sales_inv_no'];
                $info['branch_id'] = $val['branch_id'];
                $info['is_web'] = $val['is_web'];
                $info['godown_id'] = $val['godown_id'];
                $info['van_id'] = $val['van_id'];
                $info['branch_ref_no'] = $val['sales_branch_inv'];
                $info['sub_ref_no'] = 0;
                $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                $res2 = Acc_voucher::create($info);

                $info = array();
                $max = Acc_voucher::max('vch_no');
                $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max+1;
                // // customer or cash@branchcode ledger 
                // $info['vch_ledger_from'] = $ledgerId;
                // // discoutallowed@branchcode
                // $info['vch_ledger_to'] = $discLedger['taxled_ledger_id'];

                // modified on 28-01-2020
                // discoutallowed@branchcode
                $info['vch_ledger_from'] = $discLedger['taxled_ledger_id'];
                //sales@branchcode
                $info['vch_ledger_to'] = $salesLedger['taxled_ledger_id'];

                $info['vch_date'] = $val['sales_date'];
                $info['vch_in'] = 0;
                $info['vch_out'] = $val['sales_discount'];
                // voucher type 4 for Sales
                $info['vch_vchtype_id'] = 4;
                $info['vch_notes'] = $val['sales_notes'];
                $info['vch_added_by'] = $val['sales_added_by'];
                $info['vch_id2'] = $res2->vch_id;;
                $info['vch_from_group'] = 22;
                $info['vch_flags'] = 1;
                $info['ref_no'] = $val['sales_inv_no'];
                $info['branch_id'] = $val['branch_id'];
                $info['is_web'] = $val['is_web'];
                $info['godown_id'] = $val['godown_id'];
                $info['van_id'] = $val['van_id'];
                $info['branch_ref_no'] = $val['sales_branch_inv'];
                $info['sub_ref_no'] = 0;
                $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                $res2 = Acc_voucher::create($info);
            }
            $sales = array(); 
            $vat = array(); 
            $service = array(); 
            foreach($val['sales_sub_and_prod'] as $val1) {
                // product or service
                if($val1['prd_type'] == 1){
                    $sales[$val1['salesub_taxcat_id']] = (isset($sales[$val1['salesub_taxcat_id']])) ? $sales[$val1['salesub_taxcat_id']] : 0;
                    $sales[$val1['salesub_taxcat_id']] += ($val1['salesub_qty'] * $val1['salesub_rate']);
                } else {
                    $service[$val1['salesub_taxcat_id']] = (isset($service[$val1['salesub_taxcat_id']])) ? $service[$val1['salesub_taxcat_id']] : 0;
                    $service[$val1['salesub_taxcat_id']] += ($val1['salesub_qty'] * $val1['salesub_rate']);
                }
                if($val1['salesub_tax_per'] != 0){
                    $vat[$val1['salesub_taxcat_id']] = (isset($vat[$val1['salesub_taxcat_id']])) ? $vat[$val1['salesub_taxcat_id']] : 0;
                    $vat[$val1['salesub_taxcat_id']] += ($val1['salesub_qty'] * $val1['salesub_tax_rate']);
                }
            }
            foreach($service as $taxCatId => $amt) {
                if($amt != 0){
                    $amt = round($amt, 2); 
                    $where = array();
                    $where[] = ['taxled_taxcat_id','=', $taxCatId];
                    $where[] = ['taxled_branch_id','=', $val['branch_id']];
                    // sub category id 7  => Service, refer erp_tax_category_sub
                    $where[] = ['taxled_taxcatsub_id','=', 7];
                    $taxLedger = TaxLedgers::where($where)
                    ->join('tax_category','tax_category.taxcat_id','tax_ledgers.taxled_taxcat_id')
                    ->first();
                
                    // $info = array();
                    $max = Acc_voucher::max('vch_no');
                    $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max+1;
                     //sales@vatper5%-branchcode
                    $info['vch_ledger_from'] = $taxLedger['taxled_ledger_id'];
                    //sales@branchcode
                    $info['vch_ledger_to'] = $salesLedger['taxled_ledger_id'];
                    $info['vch_date'] = $val['sales_date'];
                    $info['vch_in'] = $amt;
                    $info['vch_out'] = 0;
                    // voucher type 10 for Service
                    $info['vch_vchtype_id'] = 10;
                    $info['vch_notes'] = $val['sales_notes'];
                    $info['vch_added_by'] = $val['sales_added_by'];
                    $info['vch_id2'] = 0;
                    $info['vch_from_group'] = 17;
                    $info['vch_flags'] = 1;
                    $info['ref_no'] = $val['sales_inv_no'];
                    $info['branch_id'] = $val['branch_id'];
                    $info['is_web'] = $val['is_web'];
                    $info['godown_id'] = $val['godown_id'];
                    $info['van_id'] = $val['van_id'];
                    $info['branch_ref_no'] = $val['sales_branch_inv'];
                    $info['sub_ref_no'] = 0;
                    $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                    $res2 = Acc_voucher::create($info);

                    $info = array();
                    $max = Acc_voucher::max('vch_no');
                    $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max+1;
                    //sales@branchcode
                    $info['vch_ledger_from'] = $salesLedger['taxled_ledger_id'];
                    //sales@vatper5%-branchcode
                    $info['vch_ledger_to'] = $taxLedger['taxled_ledger_id'];
                    
                    $info['vch_date'] = $val['sales_date'];
                    $info['vch_in'] = 0;
                    $info['vch_out'] = $amt;
                    // voucher type 10 for Service
                    $info['vch_vchtype_id'] = 10;
                    $info['vch_notes'] = $val['sales_notes'];
                    $info['vch_added_by'] = $val['sales_added_by'];
                    $info['vch_id2'] = $res2->vch_id;;
                    $info['vch_from_group'] = 9;
                    $info['vch_flags'] = 1;
                    $info['ref_no'] = $val['sales_inv_no'];
                    $info['branch_id'] = $val['branch_id'];
                    $info['is_web'] = $val['is_web'];
                    $info['godown_id'] = $val['godown_id'];
                    $info['van_id'] = $val['van_id'];
                    $info['branch_ref_no'] = $val['sales_branch_inv'];
                    $info['sub_ref_no'] = 0;
                    $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                    $res2 = Acc_voucher::create($info);
                }

            }
            foreach($sales as $taxCatId => $saleAmt) {
                if($saleAmt != 0){
                    $saleAmt = round($saleAmt, 2);
                    $where = array();
                    $where[] = ['taxled_taxcat_id','=', $taxCatId];
                    $where[] = ['taxled_branch_id','=', $val['branch_id']];
                    // sub category id 1  => Sales, refer erp_tax_category_sub
                    $where[] = ['taxled_taxcatsub_id','=', 1];
                    $taxLedger = TaxLedgers::where($where)
                    ->join('tax_category','tax_category.taxcat_id','tax_ledgers.taxled_taxcat_id')
                    ->first();
                
                    // $info = array();
                    $max = Acc_voucher::max('vch_no');
                    $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max+1;
                    //sales@vatper5%-branchcode
                    $info['vch_ledger_from'] = $taxLedger['taxled_ledger_id'];
                    //sales@branchcode
                    $info['vch_ledger_to'] = $salesLedger['taxled_ledger_id'];
                    
                    $info['vch_date'] = $val['sales_date'];
                    $info['vch_in'] = $saleAmt;
                    $info['vch_out'] = 0;
                    // voucher type 4 for Sales
                    $info['vch_vchtype_id'] = 4;
                    $info['vch_notes'] = $val['sales_notes'];
                    $info['vch_added_by'] = $val['sales_added_by'];
                    $info['vch_id2'] = 0;
                    $info['vch_from_group'] = 9;
                    $info['vch_flags'] = 1;
                    $info['ref_no'] = $val['sales_inv_no'];
                    $info['branch_id'] = $val['branch_id'];
                    $info['is_web'] = $val['is_web'];
                    $info['godown_id'] = $val['godown_id'];
                    $info['van_id'] = $val['van_id'];
                    $info['branch_ref_no'] = $val['sales_branch_inv'];
                    $info['sub_ref_no'] = 0;
                    $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                    $res2 = Acc_voucher::create($info);

                    $info = array();
                    $max = Acc_voucher::max('vch_no');
                    $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max+1;
                     //sales@branchcode
                     $info['vch_ledger_from'] = $salesLedger['taxled_ledger_id'];
                    //sales@vatper5%-branchcode
                    $info['vch_ledger_to'] = $taxLedger['taxled_ledger_id'];
                   
                    $info['vch_date'] = $val['sales_date'];
                    $info['vch_in'] = 0;
                    $info['vch_out'] = $saleAmt;
                    // voucher type 4 for Sales
                    $info['vch_vchtype_id'] = 4;
                    $info['vch_notes'] = $val['sales_notes'];
                    $info['vch_added_by'] = $val['sales_added_by'];
                    $info['vch_id2'] = $res2->vch_id;;
                    $info['vch_from_group'] = 9;
                    $info['vch_flags'] = 1;
                    $info['ref_no'] = $val['sales_inv_no'];
                    $info['branch_id'] = $val['branch_id'];
                    $info['is_web'] = $val['is_web'];
                    $info['godown_id'] = $val['godown_id'];
                    $info['van_id'] = $val['van_id'];
                    $info['branch_ref_no'] = $val['sales_branch_inv'];
                    $info['sub_ref_no'] = 0;
                    $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                    $res2 = Acc_voucher::create($info);
                }

            }
            foreach($vat as $taxCatId => $amt) {
                if($amt != 0) {
                    $amt = round($amt, 2);
                    $where = array();
                    $where[] = ['taxled_taxcat_id','=', $taxCatId];
                    $where[] = ['taxled_branch_id','=', $val['branch_id']];
                    // sub category id 2  => otuput tax, refer erp_tax_category_sub
                    $where[] = ['taxled_taxcatsub_id','=', 2];
                    $taxLedger = TaxLedgers::where($where)
                    ->join('tax_category','tax_category.taxcat_id','tax_ledgers.taxled_taxcat_id')
                    ->first();
                
                    // $info = array();
                    $max = Acc_voucher::max('vch_no');
                    $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max+1;
                    $info['vch_ledger_from'] = $taxLedger['taxled_ledger_id'];
                    $info['vch_ledger_to'] = $salesLedger['taxled_ledger_id'];
                    $info['vch_date'] = $val['sales_date'];
                    $info['vch_in'] = $amt;
                    $info['vch_out'] = 0;
                    // voucher type 24 for output tax
                    $info['vch_vchtype_id'] = 24;
                    $info['vch_notes'] = $val['sales_notes'];
                    $info['vch_added_by'] = $val['sales_added_by'];
                    $info['vch_id2'] = 0;
                    $info['vch_from_group'] = 15;
                    $info['vch_flags'] = 1;
                    $info['ref_no'] = $val['sales_inv_no'];
                    $info['branch_id'] = $val['branch_id'];
                    $info['is_web'] = $val['is_web'];
                    $info['godown_id'] = $val['godown_id'];
                    $info['van_id'] = $val['van_id'];
                    $info['branch_ref_no'] = $val['sales_branch_inv'];
                    $info['sub_ref_no'] = 0;
                    $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                    $res2 = Acc_voucher::create($info);

                    $info = array();
                    $max = Acc_voucher::max('vch_no');
                    $vouchNo = $info['vch_no'] = (empty($max)) ? 1 : $max+1;
                    $info['vch_ledger_from'] = $salesLedger['taxled_ledger_id'];
                    $info['vch_ledger_to'] = $taxLedger['taxled_ledger_id'];
                    $info['vch_date'] = $val['sales_date'];
                    $info['vch_in'] = 0;
                    $info['vch_out'] = $amt;
                    // voucher type 24 for Sales
                    $info['vch_vchtype_id'] = 24;
                    $info['vch_notes'] = $val['sales_notes'];
                    $info['vch_added_by'] = $val['sales_added_by'];
                    $info['vch_id2'] = $res2->vch_id;;
                    $info['vch_from_group'] = 9;
                    $info['vch_flags'] = 1;
                    $info['ref_no'] = $val['sales_inv_no'];
                    $info['branch_id'] = $val['branch_id'];
                    $info['is_web'] = $val['is_web'];
                    $info['godown_id'] = $val['godown_id'];
                    $info['van_id'] = $val['van_id'];
                    $info['branch_ref_no'] = $val['sales_branch_inv'];
                    $info['sub_ref_no'] = 0;
                    $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                    $res2 = Acc_voucher::create($info);
                }

            }
            
        }
        if($refNo != ''){
            return true;
        }else{
            echo 'Sales vocher entry compled successfully';
        }
    }


    public function setProductTaxCategory($directAccess=true){

        $products = Product::get()->toArray();
        $taxCats = TaxCategory::get()->toArray();
        $taxCat = [];
    
        foreach($taxCats as $cat){
            //skip international
            if($cat['taxcat_id'] != 4){
                $taxCat[$cat['taxcat_tax_per']] = $cat;
            }
        }

        foreach($products as $val) {
       
            if(!in_array($val['prd_tax_cat_id'], [1,2,3,4])){
                echo '<br>taxcatid missing Prod id : ' . $val['prd_id'];
                $where = array();
                $where[] = ['prd_id','=', $val['prd_id']];
                $val['prd_tax'] = ($val['prd_tax']) ? $val['prd_tax'] : 0;
                $updt = array(
                    'prd_tax' => $val['prd_tax'],
                    'prd_tax_cat_id' =>  $taxCat[$val['prd_tax']]['taxcat_id'],
                    // 'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6),
                );
                Product::where($where)->update($updt);
               
            }
        }

        $purchaseSub = PurchaseSub::get()->toArray();
        foreach($purchaseSub as $val) {
            if(!in_array($val['purchsub_taxcat_id'], array(1,2,3,4))){
                echo '<br>taxcatid missing Purchase sub id : ' . $val['purchsub_id'];
                $where = array();
                $where[] = ['purchsub_id','=', $val['purchsub_id']];
                $val['purchsub_tax_per'] = ($val['purchsub_tax_per']) ? $val['purchsub_tax_per'] : 0;
                $updt = array(
                    'purchsub_tax_per' => $val['purchsub_tax_per'],
                    'purchsub_taxcat_id' =>  $taxCat[$val['purchsub_tax_per']]['taxcat_id'],
                    // 'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6),
                );
                PurchaseSub::where($where)->update($updt);
            }
        }

        $purchaseReturnSub = PurchaseReturnSub::get()->toArray();
        foreach($purchaseReturnSub as $val) {
            if(!in_array($val['purchretsub_taxcat_id'], array(1,2,3,4))){
                echo '<br>taxcatid missing Purchase Return sub id : ' . $val['purchretsub_id'];
                $where = array();
                $where[] = ['purchretsub_id','=', $val['purchretsub_id']];
                $val['purchretsub_tax_per'] = ($val['purchretsub_tax_per']) ? $val['purchretsub_tax_per'] : 0;
                $updt = array(
                    'purchretsub_tax_per' => $val['purchretsub_tax_per'],
                    'purchretsub_taxcat_id' =>  $taxCat[$val['purchretsub_tax_per']]['taxcat_id'],
                    // 'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6),
                );
                PurchaseReturnSub::where($where)->update($updt);
            }
        }

        $salesSub = SalesSub::get()->toArray();
        foreach($salesSub as $val) {
            if(!in_array($val['salesub_taxcat_id'], array(1,2,3,4))){
                echo '<br>taxcatid missing Sales sub id : ' . $val['salesub_id'];
                $where = array();
                $where[] = ['salesub_id','=', $val['salesub_id']];
                $val['salesub_tax_per'] = ($val['salesub_tax_per']) ? $val['salesub_tax_per'] : 0;
                $updt = array(
                    'salesub_tax_per' => $val['salesub_tax_per'],
                    'salesub_taxcat_id' =>  $taxCat[$val['salesub_tax_per']]['taxcat_id'],
                    // 'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6),
                );
                SalesSub::where($where)->update($updt);
            }
        }

        $salesReturnSub = SalesReturnSub::get()->toArray();
        foreach($salesReturnSub as $val) {
            if(!in_array($val['salesretsub_taxcat_id'], array(1,2,3,4))){
                echo '<br>taxcatid missing Sales Return sub id : ' . $val['salesretsub_id'];
                $where = array();
                $where[] = ['salesretsub_id','=', $val['salesretsub_id']];
                $val['salesretsub_tax_per'] = ($val['salesretsub_tax_per']) ? $val['salesretsub_tax_per'] : 0;
                $updt = array(
                    'salesretsub_tax_per' => $val['salesretsub_tax_per'],
                    'salesretsub_taxcat_id' =>  $taxCat[$val['salesretsub_tax_per']]['taxcat_id'],
                    // 'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6),
                );
                SalesReturnSub::where($where)->update($updt);
            }
        }

        echo 'completed';
    }

    public function setTaxLedger($directAccess=true){
        $taxCatSub = TaxCategorySub::get();
        $branches = Acc_branch::get();
        $taxCat = TaxCategory::get();
        foreach($taxCatSub as $val){
            foreach($branches as $br){

                
                // ledger group duties and taxes for input and output tax
                $accGrp = $val['taxcatsub_acc_grp'];
                // $accGrp = 15;
                // // change ledger group in case of Sales or sales return
                // if($val['taxcatsub_id'] == 1 || $val['taxcatsub_id'] == 5){
                //     $accGrp = 9;
                // }
                // // change ledger group in case of Purchase or purchase return or purchase frieght
                // if($val['taxcatsub_id'] == 3 || $val['taxcatsub_id'] == 6 || $val['taxcatsub_id'] == 11){
                //     $accGrp = 10;
                // }
                // // change ledger group in case of Service or service return
                // if($val['taxcatsub_id'] == 7 || $val['taxcatsub_id'] == 8){
                //     $accGrp = 17;
                // }
                // // change ledger group in case of Cash
                // if($val['taxcatsub_id'] == 9){
                //     $accGrp = 3;
                // }
                // // change ledger group in case of Discount Allowed
                // if($val['taxcatsub_id'] == 10){
                //     $accGrp = 22;
                // }

                if($val['taxcatsub_type'] == 1 || $val['taxcatsub_type'] == 2) {
                    foreach($taxCat as $cat) {
                    
                        $where = array();
                        // $where[] = ['branch_stock_id','=', $val['salesub_branch_stock_id']];
                        $where[] = ['taxled_taxcat_id','=', $cat['taxcat_id']];
                        $where[] = ['taxled_branch_id','=', $br['branch_id']];
                        $where[] = ['taxled_taxcatsub_id','=', $val['taxcatsub_id']];
                        $taxLedgers = TaxLedgers::where($where)->first();
                        if(empty($taxLedgers)){
                        
                            $createLedger = true;
                            // Don't create 0% ledger for input and output tax
                            if($cat['taxcat_tax_per'] == 0 && ($val['taxcatsub_id'] == 2 || $val['taxcatsub_id'] == 4)){
                                $createLedger = false;
                            }
                             // don't generate Sales@ if is_sales_ledger == 0
                            if($cat['is_sales_ledger'] == 0 && $val['taxcatsub_id'] == 1) {
                                $createLedger = false;
                            }
                            // don't generate purchase@ and inputtax if is_purchase_ledger ==0
                            if($cat['is_purchase_ledger'] == 0 && ($val['taxcatsub_id'] == 3 || $val['taxcatsub_id'] == 4)) {
                                $createLedger = false;
                            }
                            // don't generate service@ if is_servcie_ledger ==0
                            if($cat['is_servcie_ledger'] == 0 && $val['taxcatsub_id'] == 7) {
                                $createLedger = false;
                            }
                            // don't generate outputtax@ if is_servcie_ledger ==0 &&  is_sales_ledger == 0
                            if($cat['is_servcie_ledger'] == 0  && $cat['is_sales_ledger'] == 0 && $val['taxcatsub_id'] == 2) {
                                $createLedger = false;
                            }

                            if($createLedger){
                                $info = array();
                                $info['ledger_name'] = $val['taxcatsub_code'] . $cat['taxcat_code'] . '-' . $br['branch_code'];
                                $info['ledger_accgrp_id'] = $accGrp;
                                $info['ledger_acc_type'] = 2;
                                $info['ledger_return'] = 1;
                                $info['ledger_edit'] = 0;
                                $info['ledger_flags'] = 1;
                                $info['opening_balance'] = 0;
                                $info['ledger_alias'] = $info['ledger_name'];
                                $info['ledger_branch_id'] = $br['branch_id'];
                                $info['ledger_balance_privacy'] = 0;
                                $info['ledger_due'] = 0;
                                $addInfo = array();
                                $addInfo['branch_id'] = $br['branch_id'];
                                $addInfo["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);

                                $info = array_merge($info, $addInfo);
                                $res = Acc_ledger::create($info);

                                $updt = array(
                                    'taxled_ledger_id' => $res->ledger_id,
                                    'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6),
                                );

                                $info = array();
                                $max = TaxLedgers::max('taxled_id');
                                $info['taxled_id'] = (empty($max)) ? 1 : $max+1;
                                $info['taxled_ledger_id'] = $res->ledger_id;
                                $info['taxled_taxcat_id'] = $cat['taxcat_id'];
                                $info['taxled_branch_id'] = $br['branch_id'];
                                $info['taxled_taxcatsub_id'] = $val['taxcatsub_id'];
                                $info['taxled_vchtype_id'] = $val['taxcatsub_vchtype_id'];
                                $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                                TaxLedgers::create($info);
                                // TaxLedgers::where($where)->update($updt);
                            }

                        }
                    
                    }
                } 

                if($val['taxcatsub_type'] == 0 || $val['taxcatsub_type'] == 2) {
                    $where = array();
                    $where[] = ['taxled_taxcat_id','=', 0];
                    $where[] = ['taxled_branch_id','=', $br['branch_id']];
                    $where[] = ['taxled_taxcatsub_id','=', $val['taxcatsub_id']];
                    $taxLedgers = TaxLedgers::where($where)->first();

                    

                    if(empty($taxLedgers)){

                        $info = array();
                        $info['ledger_name'] = $val['taxcatsub_code'] . '@' . $br['branch_code'];
                        $info['ledger_accgrp_id'] = $accGrp;
                        $info['ledger_acc_type'] = ($val['taxcatsub_id'] == 9) ? 0 :2;
                        $info['ledger_return'] = 1;
                        $info['ledger_edit'] = 0;
                        $info['ledger_flags'] = 1;
                        $info['opening_balance'] = 0;
                        $info['ledger_alias'] = $info['ledger_name'];
                        $info['ledger_branch_id'] = $br['branch_id'];
                        $info['ledger_balance_privacy'] = 0;
                        $info['ledger_due'] = 0;
                        $addInfo = array();
                        $addInfo['branch_id'] = $br['branch_id'];
                        $addInfo["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);

                        $info = array_merge($info, $addInfo);
                        $res = Acc_ledger::create($info);

                        $updt = array(
                            'taxled_ledger_id' => $res->ledger_id,
                            'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6),
                        );

                        $info = array();
                        $max = TaxLedgers::max('taxled_id');
                        $info['taxled_id'] = (empty($max)) ? 1 : $max+1;
                        $info['taxled_ledger_id'] = $res->ledger_id;
                        $info['taxled_taxcat_id'] = 0;
                        $info['taxled_branch_id'] = $br['branch_id'];
                        $info['taxled_taxcatsub_id'] = $val['taxcatsub_id'];
                        $info['taxled_vchtype_id'] = 0;
                        $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                        TaxLedgers::create($info);

                    }
                }
            }
        }
        if($directAccess){
            echo 'suceessfully created tax ledgers';
        } else{
            return true;
        }
    }
    public function setVanOpeningBalanceLedgers(){
       
        $van = Van::get();
        foreach($van as $val){
            echo '<br><br><br>Van name : ' . $val['van_name'];
            echo '<br>van code :' . $val['van_code'];
            echo '<br>van ledger id :' . $val['van_cash_ledger_id'];

            $info = array();
            $info['ledger_name'] = 'Cash@' . $val['van_code'];
            $info['ledger_accgrp_id'] = 3;
            $info['ledger_acc_type'] = 0;
            $info['ledger_return'] = 0;
            // $info['ledger_address'] = $Van['van_contact_address'];
            $info['ledger_address'] = '';
            $info['ledger_notes'] = '';
            $info['ledger_edit'] = 0;
            $info['ledger_flags'] = 1;
            $info['opening_balance'] = $val['van_op_bal'];
        
            $info['ledger_branch_id'] = $val['branch_id'];
            $info['ledger_balance_privacy'] = 0;
            $info['ledger_due'] = 0;
            $info['ledger_tin'] = '';
            $info['ledger_alias'] = 'Cash@' . $val['van_code'];
            $info['ledger_code'] = '';
            $info['ledger_email'] = '';
            $info['ledger_mobile'] = '';
            $addInfo = array();

            $addInfo['branch_id'] = $val['branch_id'];
            $addInfo["server_sync_time"] = date('ymdHis') . substr(microtime(), 2, 6);

            $info = array_merge($info, $addInfo);
            $condtion = [];
            if($val['van_cash_ledger_id'] != 0) {
                $condtion['branch_id'] = $val['branch_id'];
                $condtion['ledger_id'] = $val['van_cash_ledger_id'];
            }else{
                $condtion['ledger_name'] = 'Cash@' . $val['van_code'];
            }

            $res = Acc_ledger::updateOrCreate($condtion ,$info);
  
            $where = array();
            $where[] = ['branch_id','=', $val['branch_id']];
            $branch = Acc_branch::where($where)->first();
            $info = array();
        
            $info['vch_ledger_from'] = $res->ledger_id;
            $info['vch_ledger_to'] = $res->ledger_id;
            $info['vch_date'] = date('Y-m-d',(strtotime ( '-1 day' , strtotime ( $branch['branch_open_date']))));
            $info['vch_in'] = ($val['van_op_bal'] >= 0 ) ? abs($val['van_op_bal']) : 0;
            $info['vch_out'] = ($val['van_op_bal'] < 0) ? abs($val['van_op_bal']) : 0;
            $info['vch_vchtype_id'] = 14;
            $info['vch_notes'] = 'Starting balance';
            $info['vch_added_by'] = 0;
            $info['vch_id2'] = 0;
            $info['vch_from_group'] = 3;
            $info['vch_flags'] = 1;
            $info['ref_no'] = 0;
            $info['branch_id'] = $val['branch_id'];
            $info['is_web'] = 1;
            $info['godown_id'] = 0;
            $info['van_id'] = 0;
            $info['branch_ref_no'] = 0;
            $info['sub_ref_no'] = 0;
            $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);

            if($val['van_cash_ledger_id'] == 0) {
                $max = Acc_voucher::max('vch_no');
                $info['vch_no'] = (empty($max)) ? 1 : $max+1;

                $where = array();
                $where[] = ['van_id','=', $val['van_id']];
                Van::where($where)->update(['van_cash_ledger_id' => $res->ledger_id, 'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)]);
            }

            $voucher = Acc_voucher::updateOrCreate(
                [
                        'vch_ledger_from' => $res->ledger_id, 
                        'vch_ledger_to' => $res->ledger_id,
                        'vch_vchtype_id' => 14,
                        'branch_id' => $val['branch_id'], 
                        'van_id' => 0 
                ],
                $info
            );
            Acc_voucher::where(['vch_in'=>0,'vch_out'=>0])->delete();
        }

    }
    public function seedOpeningBalanceToVoucher(){
        $where = array();
		$where[] = ['ledger_flags','=', 1];
		$where[] = ['opening_balance','<>', 0];
        $ledgers = Acc_ledger::where($where)->get();
        foreach($ledgers as $val){
            echo '<br><br><br>ledger Id : ' . $val['ledger_id'];
            echo '<br>Branch ID :' . $val['branch_id'];
            $where = array();
            $where[] = ['branch_id','=', $val['branch_id']];
            $branch = Acc_branch::where($where)->first();
            $info = array();
            $max = Acc_voucher::max('vch_no');
            $info['vch_no'] = (empty($max)) ? 1 : $max+1;
            $info['vch_ledger_from'] = $val['ledger_id'];
            $info['vch_ledger_to'] = $val['ledger_id'];
            $info['vch_date'] = date('Y-m-d',(strtotime ( '-1 day' , strtotime ( $branch['branch_open_date']))));
            $info['vch_in'] = ($val['opening_balance'] >= 0 ) ? abs($val['opening_balance']) : 0;
            $info['vch_out'] = ($val['opening_balance'] < 0) ? abs($val['opening_balance']) : 0;
            $info['vch_vchtype_id'] = 14;
            $info['vch_notes'] = 'Starting balance';
            $info['vch_added_by'] = 0;
            $info['vch_id2'] = 0;
            $info['vch_from_group'] = 21;
            $info['vch_flags'] = 1;
            $info['ref_no'] = 0;
            $info['branch_id'] = $val['branch_id'];
            $info['is_web'] = 1;
            $info['godown_id'] = 0;
            $info['van_id'] = 0;
            $info['branch_ref_no'] = 0;
            $info['sub_ref_no'] = 0;
            $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
            $voucher = Acc_voucher::updateOrCreate(
                    [
                            'vch_ledger_from' => $val['ledger_id'], 
                            'vch_ledger_to' => $val['ledger_id'],
                            'vch_vchtype_id' => 14,
                            'branch_id' => $val['branch_id'],
                            'van_id' => 0 
                    ],
                    $info
            );
            Acc_voucher::where(['vch_in'=>0,'vch_out'=>0])->delete();
        }
        // echo '<pre>';
        // // var_dump($ledgers);
        // // print_r($ledgers);
        // exit;
    }


}
