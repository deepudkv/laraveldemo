<?php
namespace App\Http\Controllers\API\Migration;

use App\Http\Controllers\Controller;
use App\Models\BranchTransfer\BranchTransferSub;
use App\Models\Stocks\BranchStocks;
use App\Models\Stocks\OpeningStockLog;
use App\Models\Stocks\StockDailyUpdates;
use App\Models\Van\VanDailyStocks;
use App\Models\Van\VanTransferSub;

use App\Models\Sales\SalesSub;
use App\Models\Sales\SalesMaster;
use DB;

class SaleRevertController extends Controller
{
    public function __construct()
    {
        // $this->branch_id = 10;
    }

    public function revertVantranDates2()
    {

        $transub = SalesSub::where('salesub_flags',1)->get()->toArray();

  
        foreach ($transub as $k => $val) {
            $where = array();
            $where[] = ['vds_branch_stock_id','=', $val['salesub_branch_stock_id']];
            $where[] = ['vds_prd_id','=', $val['salesub_prod_id']];
            $where[] = ['vds_date','=', $val['salesub_date']];
            $where[] = ['vds_van_id','=', $val['van_id']];
            $stock = VanDailyStocks::where($where)->first();
            if(!empty($stock)){
                $fnlQty = $stock->vds_stock_quantity - $val['salesub_qty'];
                VanDailyStocks::where($where)->update([
                    'vds_stock_quantity' => $fnlQty,
                    'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)
                ]);
            } else {
                            
                $info = array();
                $info['vds_branch_stock_id'] = $val['salesub_branch_stock_id'];
                $info['vds_prd_id'] = $val['salesub_prod_id'];
                $info['vds_stock_id'] = $val['salesub_stock_id'];
                $info['vds_van_id'] =  $val['van_id'];
                $info['vds_date'] = $val['salesub_date'];
                $info['vds_stock_quantity'] = $val['salesub_qty'] * -1;
                $info['branch_id'] = $val['branch_id'];
                $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                 VanDailyStocks::create($info);

              

            }

        }

        ECHO "SUCEES";
    }

    public function revertVantranDates()
    {

        $transub = VanTransferSub::where('vantransub_flags', 1)->get()->toArray();

        foreach ($transub as $k => $val) {

            $vantransub_qty = $val['vantransub_qty'];

            $whr = ['vds_prd_id' => $val['vantransub_prod_id'],
                'vds_branch_stock_id' => $val['vantransub_branch_stock_id'],
                'vds_van_id' => $val['vantransub_van_id'],
                'vds_date' => $val['vantransub_date']];

            $stock = VanDailyStocks::where($whr)->first();
            if (!empty($stock)) {
                $fnlQty = $stock->vds_stock_quantity + $vantransub_qty;
                VanDailyStocks::where($whr)->update([
                    'vds_stock_quantity' => $fnlQty,
                    'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6),
                ]);
            //    print_r($stock);die();
            } else {

                $info = array();
                $info['vds_branch_stock_id'] = $val['vantransub_branch_stock_id'];
                $info['vds_prd_id'] = $val['vantransub_prod_id'];
                $info['vds_stock_id'] = $val['vantransub_stock_id'];
                $info['vds_van_id'] = $val['vantransub_van_id'];
                $info['vds_date'] = $val['vantransub_date'];
                $info['vds_stock_quantity'] = $vantransub_qty;
                $info['branch_id'] = $val['branch_id'];
                $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                $stock = VanDailyStocks::create($info);
               // print_r($info);die();
            }

        }
        ECHO "SUCEES";
    }
               //echo $val['salesub_date']."===actual==".  $actual_date."<br>";
    public function revertVansales()
    {
        $transub = SalesSub::where('salesub_flags',1)->get()->toArray();

  
        foreach ($transub as $k => $val) {



            $salesub_sales_inv_no = $val['salesub_sales_inv_no'];

            $data = SalesMaster::where('sales_inv_no',$salesub_sales_inv_no)->get()->toArray();

            $actual_date = $data[0]['sales_date'];
      
            $branchstock = BranchStocks::where('bs_prd_id',$val['salesub_prod_id'])->first()->toArray() ;
            $branch_stock_id = $branchstock['branch_stock_id'];
          
          

            SalesSub::where('salesub_id',$val['salesub_id'])->update([
                'salesub_date'=>  $actual_date,
                'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)
                
                ]);


            $where1 = array();
        
            $where1[] = ['sdu_prd_id','=', $val['salesub_prod_id']];
            $where1[] = ['sdu_date','=', $val['salesub_date']];
            $where1[] = ['branch_id','=',  $val['branch_id']];

            $stock = StockDailyUpdates::where($where1)->first();
            if(!empty($stock)){
                $fnlQty = $stock->sdu_stock_quantity+ $val['salesub_qty'];
                StockDailyUpdates::where($where1)->update([
                    'sdu_stock_quantity' => $fnlQty,
                    'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)
                ]);
            } else {
                            die("deepu");
                $info = array();
                $info['sdu_branch_stock_id'] =  $branch_stock_id;
                $info['sdu_prd_id'] = $val['salesub_prod_id'];
                $info['sdu_stock_id'] = $val['salesub_stock_id'];
                $info['sdu_date'] = $val['salesub_date'];
                $info['sdu_stock_quantity'] = $val['salesub_qty'];
                $info['branch_id'] = $val['branch_id'];
                $info['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                $stock = StockDailyUpdates::create($info);

            }
         
            $where2 = array();
            $where2[] = ['sdu_prd_id','=', $val['salesub_prod_id']];
            $where2[] = ['sdu_date','=',$actual_date];
            $where2[] = ['branch_id','=',  $val['branch_id']];

            $stock_data = StockDailyUpdates::where($where2)->first();
            if(!empty($stock_data)){
                $fnlQty = $stock_data->sdu_stock_quantity-$val['salesub_qty'];
                StockDailyUpdates::where($where2)->update([
                    'sdu_stock_quantity' => $fnlQty,
                    'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)
                ]);
              
            } else {
              
                $info = array();
                $info['sdu_branch_stock_id'] =  $branch_stock_id;
                $info['sdu_prd_id'] = $val['salesub_prod_id'];
                $info['sdu_stock_id'] = $val['salesub_stock_id'];
                $info['sdu_date'] = $actual_date;
                $info['sdu_stock_quantity'] = $val['salesub_qty'] * -1;
                $info['branch_id'] =$val['branch_id'];
                $info['server_sync_time'] =  date('ymdHis') . substr(microtime(), 2, 6);
                $stock = StockDailyUpdates::create($info);
               
            }
           

        }

        ECHO "sales dates updated====";
    }

    public function revertStockqunatity()
    {
        $dta = BranchStocks::where('bs_branch_id', 2)->get()->toArray();

        foreach ($dta as $k => $v) {
            //print_r($v);die();
            $bs_stock_quantity_shop = $v['bs_stock_quantity_shop'];
            $bs_stock_quantity_gd = $v['bs_stock_quantity_gd'];
            $bs_stock_quantity = $v['bs_stock_quantity'];

            $bs_stock_id = $v['bs_stock_id'];
            $bs_prd_id = $v['bs_prd_id'];

            $updt = [
                'bs_stock_quantity_shop' => \DB::raw("bs_stock_quantity_shop-$bs_stock_quantity_shop"),
                'bs_stock_quantity_gd' => \DB::raw("bs_stock_quantity_gd-$bs_stock_quantity_gd"),
                'bs_stock_quantity' => \DB::raw("bs_stock_quantity-$bs_stock_quantity"),
                'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6),
            ];
            // BranchStocks::where('bs_branch_id', 0)->where('bs_prd_id', $bs_prd_id)->update($updt);

            BranchStocks::where('bs_branch_id', 2)->where('bs_prd_id', $bs_prd_id)->update($updt);
            // $updt2 = [
            //     'cmp_stock_quantity' => \DB::raw("cmp_stock_quantity-$bs_stock_quantity"),
            //     'server_sync_time'=> date('ymdHis') . substr(microtime(), 2, 6)
            // ];
            // CompanyStocks::where('cmp_stock_id', $bs_stock_id)->update($updt2);
        }
        echo "sucees";
        //  UPDATE `erp_stock_daily_updates` SET `sdu_stock_quantity`=0 WHERE `branch_id` = 2
        // UPDATE `erp_opening_stock_log` SET`opstklog_stock_quantity_add`=0,`opstklog_stock_quantity_rem`=0  WHERE `branch_id` = 2
    }

    public function transferIssuefix()
    {

        $sub_items = BranchTransferSub::where('stocktrsub_accepted', 1)->get()->toArray();

        foreach ($sub_items as $sub_array) {

            $prd_id = $sub_array['stocktrsub_prd_id'];
            $to_branch = $sub_array['stocktrsub_to'];
            $qty = $sub_array['stocktrsub_qty'];

            $from_branch_id = $sub_array['branch_id'];
            $date_send = $sub_array['stocktrsub_date'];

            $from_branch = $sub_array['stocktrsub_branch_stock_id'];

            $where = ['bs_prd_id' => $prd_id, 'bs_branch_id' => $to_branch];
            $bs = BranchStocks::where($where)->first();

            $stocktrsub_branch_stock_id = $bs['branch_stock_id'];
            $cmp_stock_id = $bs['bs_stock_id'];

            if ($stocktrsub_branch_stock_id) {

                $dateExist = StockDailyUpdates::select('*')->where('sdu_branch_stock_id', $from_branch)
                    ->where('sdu_date', $date_send)->first();

                if ($dateExist) {

                    $column_name = 'sdu_stock_quantity';
                    StockDailyUpdates::where('sdu_branch_stock_id', $from_branch)
                        ->where('sdu_date', $date_send)->update([
                        $column_name => \DB::raw("$column_name-$qty"),
                        'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6),
                    ]);

                } else {

                    $ds['sdu_branch_stock_id'] = $from_branch;
                    $ds['sdu_stock_id'] = $cmp_stock_id;
                    $ds['sdu_prd_id'] = $prd_id;
                    $ds['sdu_date'] = $date_send;
                    $ds['sdu_stock_quantity'] = (0 - $qty);
                    $ds['sdu_gd_id'] = 0;
                    $ds['sdu_batch_id'] = 0;
                    $ds['branch_id'] = $from_branch_id;
                    $ds['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                    StockDailyUpdates::create($ds);

                }
                $date_accepted = '2020-01-22';
                $dateExist = StockDailyUpdates::select('*')->where('sdu_branch_stock_id', $stocktrsub_branch_stock_id)
                    ->where('sdu_date', $date_accepted)->first();

                if ($dateExist) {

                    $column_name = 'sdu_stock_quantity';
                    StockDailyUpdates::where('sdu_branch_stock_id', $stocktrsub_branch_stock_id)
                        ->where('sdu_date', $date_accepted)->update([
                        $column_name => \DB::raw("$column_name+$qty"),
                        'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6),
                    ]);

                } else {

                    $ds['sdu_branch_stock_id'] = $stocktrsub_branch_stock_id;
                    $ds['sdu_stock_id'] = $cmp_stock_id;
                    $ds['sdu_prd_id'] = $prd_id;
                    $ds['sdu_date'] = $date_accepted;
                    $ds['sdu_stock_quantity'] = $qty;
                    $ds['sdu_gd_id'] = 0;
                    $ds['sdu_batch_id'] = 0;
                    $ds['branch_id'] = $to_branch;
                    $ds['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);
                    StockDailyUpdates::create($ds);

                }

            }

        }

        return response()->json(['message' => 'Transfer updated']);

    }

    // public function stockRevert()
    // {

    //     $offset = 100;
    //     $limit = 50;
    //     $m = 0;
    //     $salesSub = SalesSub::where('branch_id', $this->branch_id)->skip($offset)->take($limit)->get();

    //     foreach ($salesSub as $key => $val) {

    //         $where = array();
    //         $where['cmp_prd_id'] = $val['salesub_prod_id'];

    //         $cmpStock = CompanyStocks::where($where)->first();
    //         $fnlQty = $cmpStock->cmp_stock_quantity + $val['salesub_qty'];
    //         CompanyStocks::where($where)->update([
    //             'cmp_stock_quantity' => $fnlQty,
    //             'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6),
    //         ]);

    //         $where = array();
    //         $where['bs_prd_id'] = $val['salesub_prod_id'];
    //         $where['bs_branch_id'] = 0;
    //         $stock = BranchStocks::where($where)->first();
    //         if (!empty($stock)) {

    //             $fnlQty = $stock->bs_stock_quantity + $val['salesub_qty'];

    //             $qty = array(
    //                 'bs_stock_quantity' => $fnlQty,
    //                 'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6),
    //             );

    //             $qty['bs_stock_quantity_shop'] = $stock->bs_stock_quantity_shop + $val['salesub_qty'];
    //             BranchStocks::where($where)->update($qty);

    //         }

    //         $where = array();
    //         $where['bs_prd_id'] = $val['salesub_prod_id'];
    //         $where['bs_branch_id'] = 10;
    //         $stock = BranchStocks::where($where)->first();
    //         if (!empty($stock)) {

    //             $fnlQty = $stock->bs_stock_quantity + $val['salesub_qty'];
    //             $qty = array(
    //                 'bs_stock_quantity' => $fnlQty,
    //                 'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6),
    //             );
    //             $qty['bs_stock_quantity_shop'] = $stock->bs_stock_quantity_shop + $val['salesub_qty'];
    //             BranchStocks::where($where)->update($qty);
    //         }

    //         $m++;
    //     }
    //     echo "sucess==$m";

    // }

    public function opdateRevert()
    {
        $row_exist = 0;
        $stocks = OpeningStockLog::where('opstklog_date', '>', '2019-12-31')->where('opstklog_type', 0)->get()->toArray();

        foreach ($stocks as $key => $value) {

            $qty = $value['opstklog_stock_quantity_add'];

            $whr['sdu_date'] = $value['opstklog_date'];
            $whr['sdu_branch_stock_id'] = $value['opstklog_branch_stock_id'];

            $insertExist = StockDailyUpdates::select('*')->where($whr)->first();

            if ($insertExist) {

                $row_exist = $row_exist + 1;
                StockDailyUpdates::where($whr)->decrement('sdu_stock_quantity', $qty);
                StockDailyUpdates::where($whr)->update(['server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)]);

                $whr2['sdu_date'] = '2019-12-31';
                $whr2['sdu_branch_stock_id'] = $value['opstklog_branch_stock_id'];

                $dateExist = StockDailyUpdates::select('*')->where($whr2)->first();

                if ($dateExist) {
                    StockDailyUpdates::where($whr2)->increment('sdu_stock_quantity', $qty);
                    StockDailyUpdates::where($whr2)->update(['server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6)]);

                } else {
                    $ds['sdu_branch_stock_id'] = $value['opstklog_branch_stock_id'];
                    $ds['sdu_stock_id'] = $value['opstklog_stock_id'];
                    $ds['sdu_prd_id'] = $value['opstklog_prd_id'];
                    $ds['sdu_date'] = '2019-12-31';
                    $ds['sdu_stock_quantity'] = $qty;
                    $ds['sdu_gd_id'] = $value['opstklog_gd_id'];
                    $ds['sdu_batch_id'] = $value['opstklog_batch_id'];
                    $ds['branch_id'] = $value['branch_id'];
                    $ds['server_sync_time'] = date('ymdHis') . substr(microtime(), 2, 6);

                    StockDailyUpdates::create($ds);
                }
            }

        }

        echo "total opening _stocks logs ==" . trim(count($stocks)) . "and rows reverted==" . $row_exist;
    }
}
