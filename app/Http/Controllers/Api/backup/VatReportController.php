<?php

namespace App\Http\Controllers\Api\Reports1;

use App\Http\Controllers\Api\ApiParent;
use App\Models\Purchase\PurchaseReturnSub;
use App\Models\Purchase\PurchaseSub;
use App\Models\Sales\SalesReturnSub;
use App\Models\Sales\SalesSub;
use App\Models\Settings\TaxCategory;
use App\Models\Company\Acc_branch;

use DB;
use Illuminate\Http\Request;

class VatReportController1 extends ApiParent
{

    public function Report(Request $request, $type)
    {

        switch ($type) {
            case 'input_vat_summary':
                return parent::displayData($this->inputVatSummary($request));
                break;
            case 'output_vat_summary':
                return parent::displayData($this->outVatSummary($request));
                break;

            case 'day_summary':
                return parent::displayData($this->daySummary($request));
                break;
            default:
                return parent::displayError('Invalid Endpoint', $this->badrequest_stat);
                break;
        }
    }
    public function daySummary1($request)
    {

        // $pt = $ptype = isset($request['period_type']) ? $request['period_type'] : 0;
        // if ($pt >= 0 && $pt <= 12) {
        //     if ($pt < 10) {
        //         $pt = "0" . $pt;
        //     }
        //     $date1 = date("Y-$pt-01");
        //     $date2 = date("Y-$pt-31");

        // }

        // if ($pt == 'a') {
        //     $year = date('Y');
        //     $date1 = $year . "-01-01";
        //     $date2 = $year . "-12-31";

        // }

        // if ($pt == 'c') {
        //     $date1 = isset($rq['date1']) ? date('Y-m-d', strtotime($rq['date1'])) : 0;
        //     $date2 = isset($rq['date2']) ? date('Y-m-d', strtotime($rq['date2'])) : 0;
        // }
        $date1 = date("Y-01-01");
        $date2 = date("Y-02-6");
        $dates = $this->getDatesFromRange($date1, $date2);
        $taxcats = TaxCategory::get()->toArray();
        $i = 0;
        foreach ($dates as $k => $d) {
            $amnt = DB::raw('SUM(purchsub_rate * purchsub_qty) as purch_amount');
            $rate = DB::raw('SUM(purchsub_tax) as vat');

        

            $retamnt = DB::raw('SUM(purchretsub_rate * purchretsub_qty) as purch_ret_amount');
            $retrate = DB::raw('SUM(purchretsub_tax) as purch_ret_vat');

            $retqry = PurchaseReturnSub::select($retrate, $retamnt)->where('purchretsub_date', $d)
                ->join('purchase_return', 'purchase_return.purchret_id', '=', 'purchase_return_sub.purchretsub_purch_id');

            $standQry = clone $qry;
            $pstd = $standQry->where('purchases.purch_type', 1)->where('purchsub_date', $d)->get()->toArray();

           
            $out[$i]['vat'] = $vat;
            $out[$i]['total_purchase'] = $total_purchase;


            $vat = $total_purchase = 0;
            foreach ($taxcats as $k => $tax) {
                $taxcat_name = $tax['taxcat_name'];
                $display_name = $tax['display_name'];
                $taxcat_id = $tax['taxcat_id'];
                $taxcat_tax_per = $tax['taxcat_tax_per'];
   

                $rslt = PurchaseSub::select($rate, $amnt)->where('purchsub_date', $d)
                ->join('purchases', 'purchases.purch_id', '=', 'purchase_subs.purchsub_purch_id')->
                where('purchases.purchsub_taxcat_id',$taxcat_id)->where('purchsub_date', $d)->get()->toArray();

              
                $rtrslt = PurchaseReturnSub::select($retrate, $retamnt)->where('purchretsub_date', $d)
                ->join('purchase_return', 'purchase_return.purchret_id', '=', 'purchase_return_sub.purchretsub_purch_id')->
                where('purchases.purchretsub_taxcat_id',$taxcat_id)->where('purchsub_date', $d)->get()->toArray();

                $purch_amount = isset($rslt[0]['purch_amount']) ? $rslt[0]['purch_amount'] : 0;
                $purch_ret_amount = isset($rtrslt[0]['purch_ret_amount']) ? $rtrslt[0]['purch_ret_amount'] : 0;
            

                $vat_deduct = $vat_deduct + $rslt[0]['purch_ret_vat'];
                $out[$i][$display_name] = $purch_amount - $purch_ret_amount;
                $out[$i][$k] = $purch_amount - $purch_ret_amount;


                $sale_amount_total = $sale_amount_total + $sale_amount;
                $total_purchase = $pstd[0]['purch_amount'] + $zpurch[0]['purch_amount'] + $epurch[0]['purch_amount'] + $ipurch[0]['purch_amount'];
                $out[$i]['vat'] = $vat;
                $out[$i]['total_purchase'] = $total_purchase;
                $sale_amount_total = $sale_amount_total + $sale_amount;
                $out[$i]['sale_amount'] = $sale_amount_total;
                $out[$i]['vat_deduct'] = $vat_deduct;

            }


            $count = count($taxcats)-1;
            $vat_deduct = $sale_amount_total = 0;
            foreach ($taxcats as $k => $tax) {
                $taxcat_name = $tax['taxcat_name'];
                $display_name = $tax['display_name'];
                $taxcat_id = $tax['taxcat_id'];
                $taxcat_tax_per = $tax['taxcat_tax_per'];
   
                $rslt = SalesSub::select($amnt, 'salesub_tax_rate')->where('salesub_taxcat_id', $taxcat_id)->where('salesub_date', $d)->get()->toArray();
                $rtrslt = SalesReturnSub::select('salesretsub_sales_inv_no', 'salesretsub_date', 'salesretsub_tax_per',
                    'salesretsub_tax_rate', 'salesretsub_tax_rate'
                    , $retamnt)->where('salesretsub_tax_per', $taxcat_tax_per)->where('salesretsub_date', $d)->get()->toArray();

                $sale_amount = isset($rslt[0]['sale_amount']) ? $rslt[0]['sale_amount'] : 0;
                $sale_ret_amount = isset($rtrslt[0]['sale_ret_amount']) ? $rtrslt[0]['sale_ret_amount'] : 0;
                $sale_amount = isset($rslt[0]['sale_amount']) ? $rslt[0]['sale_amount'] : 0;

                $vat_deduct = $vat_deduct + $rslt[0]['salesub_tax_rate'];
                $out[$i][$display_name] = $sale_amount - $sale_ret_amount;
                $out[$i][$count+1] = $sale_amount - $sale_ret_amount;
                $sale_amount_total = $sale_amount_total + $sale_amount;
                $out[$i]['sale_amount'] = $sale_amount_total;
                $out[$i]['vat_deduct'] = $vat_deduct;

            }


            $amnt = DB::raw('SUM(salesub_rate * salesub_qty) as sale_amount');


            $retamnt = DB::raw('SUM(salesretsub_rate * 	salesretsub_qty) as sale_ret_amount');

      
            $vat_deduct = $sale_amount_total = 0;
            foreach ($taxcats as $k => $tax) {
                $taxcat_name = $tax['taxcat_name'];
                $display_name = $tax['display_name'];
                $taxcat_id = $tax['taxcat_id'];
                $taxcat_tax_per = $tax['taxcat_tax_per'];
   
                $rslt = SalesSub::select($amnt, 'salesub_tax_rate')->where('salesub_taxcat_id', $taxcat_id)->where('salesub_date', $d)->get()->toArray();
                $rtrslt = SalesReturnSub::select('salesretsub_sales_inv_no', 'salesretsub_date', 'salesretsub_tax_per',
                    'salesretsub_tax_rate', 'salesretsub_tax_rate'
                    , $retamnt)->where('salesretsub_tax_per', $taxcat_tax_per)->where('salesretsub_date', $d)->get()->toArray();

                $sale_amount = isset($rslt[0]['sale_amount']) ? $rslt[0]['sale_amount'] : 0;
                $sale_ret_amount = isset($rtrslt[0]['sale_ret_amount']) ? $rtrslt[0]['sale_ret_amount'] : 0;
                $sale_amount = isset($rslt[0]['sale_amount']) ? $rslt[0]['sale_amount'] : 0;

                $vat_deduct = $vat_deduct + $rslt[0]['salesub_tax_rate'];
                $out[$i][$display_name] = $sale_amount - $sale_ret_amount;

                $sale_amount_total = $sale_amount_total + $sale_amount;
                $out[$i]['sale_amount'] = $sale_amount_total;
                $out[$i]['vat_deduct'] = $vat_deduct;

            }

            if ($out[$i]['vat_deduct'] == 0 && $out[$i]['vat_deduct'] == 0) {
                unset($out[$i]);
            }
            else{
                $i++;
            }
          
        }
        return ['data' => $out, 'titles_purchase' => '', 'titles_sales' => ''];

    }
    public function daySummary($request)
    {

        $pt = $ptype = isset($request['period_type']) ? $request['period_type'] : 0;
        if ($pt >= 0 && $pt <= 12) {
            if ($pt < 10) {
                $pt = "0" . $pt;
            }
            $date1 = date("Y-$pt-01");
            $date2 = date("Y-$pt-31");

        }

        if ($pt == 'a') {
            $year = date('Y');
            $date1 = $year . "-01-01";
            $date2 = $year . "-12-31";

        }

        if ($pt == 'c') {
            $date1 = isset($rq['date1']) ? date('Y-m-d', strtotime($rq['date1'])) : 0;
            $date2 = isset($rq['date2']) ? date('Y-m-d', strtotime($rq['date2'])) : 0;
        }
        $date1 = date("Y-01-01");
        $date2 = date("Y-01-31");
        $dates = $this->getDatesFromRange($date1, $date2);
        $taxcats = TaxCategory::get()->toArray();
        $i = 0;
        $vat_sum=$net_vat=0;
        foreach ($dates as $k => $d) {
            $amnt = DB::raw('SUM(purchsub_rate * purchsub_qty) as purch_amount');
            $rate = DB::raw('SUM(purchsub_tax) as vat');

            $qry = PurchaseSub::select($rate, $amnt)->where('purchsub_date', $d)
                ->join('purchases', 'purchases.purch_id', '=', 'purchase_subs.purchsub_purch_id');

            $retamnt = DB::raw('SUM(purchretsub_rate * purchretsub_qty) as purch_ret_amount');
            $retrate = DB::raw('SUM(purchretsub_tax) as vat_deduct');

            $retqry = PurchaseReturnSub::select($retrate, $retamnt)->where('purchretsub_date', $d)
                ->join('purchase_return', 'purchase_return.purchret_id', '=', 'purchase_return_sub.purchretsub_purch_id');

            $standQry = clone $qry;
            $pstd = $standQry->where('purchases.purch_type', 1)->where('purchsub_date', $d)->get()->toArray();

            //print_r( $pstd);die();
            $retstandQry = clone $retqry;
            $rstd = $retstandQry->where('purchase_return.purchret_type', 1)->where('purchretsub_date', $d)->get()->toArray();

            $zeroQry = clone $qry;
            $zpurch = $standQry->where('purchases.purch_type', 2)->where('purchsub_date', $d)->get()->toArray();
            $retzeroQry = clone $retqry;
            $rtz = $retzeroQry->where('purchase_return.purchret_type', 2)->where('purchretsub_date', $d)->get()->toArray();

            $exemQry = clone $qry;
            $epurch = $standQry->where('purchases.purch_type', 3)->where('purchsub_date', $d)->get()->toArray();
            $retexemQry = clone $retqry;
            $ertn = $retstandQry->where('purchase_return.purchret_type', 3)->where('purchretsub_date', $d)->get()->toArray();

            $importQry = clone $qry;
            $ipurch = $standQry->where('purchases.purch_type', 4)->where('purchsub_date', $d)->get()->toArray();
            $retimportQry = clone $retqry;
            $irtn = $retstandQry->where('purchase_return.purchret_type', 4)->where('purchretsub_date', $d)->get()->toArray();

            $retqry->join('purchase_return', 'purchase_return.purchret_id', '=', 'purchase_return_sub.purchretsub_purch_id');
            $out[$i]['date'] = $d;
            $out[$i]['standard'] = $pstd[0]['purch_amount'] - $rstd[0]['purch_ret_amount'];
            $out[$i]['zero_rated'] = $zpurch[0]['purch_amount'] - $rtz[0]['purch_ret_amount'];
            $out[$i]['exempt'] = $epurch[0]['purch_amount'] - $ertn[0]['purch_ret_amount'];
            $out[$i]['imports_customs'] = $ipurch[0]['purch_amount'] - $irtn[0]['purch_ret_amount'];
            $vat = $pstd[0]['vat'] + $zpurch[0]['vat'] + $epurch[0]['vat'] + $ipurch[0]['vat'];


            $vat_deduct = $rstd[0]['vat_deduct'] + $rtz[0]['vat_deduct'] + $ertn[0]['vat_deduct'] + $irtn[0]['vat_deduct'];
            $total_purchase = $pstd[0]['purch_amount'] + $zpurch[0]['purch_amount'] + $epurch[0]['purch_amount'] + $ipurch[0]['purch_amount'];
            $out[$i]['vat'] = $vat-  $vat_deduct;
            $out[$i]['total_purchase'] = $total_purchase;

            // $out[$d]['purchase']['standard'] = $pstd[0]['purch_amount'] - $rstd[0]['purch_ret_amount'];
            // $out[$d]['purchase']['zero_rated']= $zpurch[0]['purch_amount'] - $rtz[0]['purch_ret_amount'];
            // $out[$d]['purchase']['exempt']= $epurch[0]['purch_amount'] - $ertn[0]['purch_ret_amount'];
            // $out[$d]['purchase']['imports_customs']= $ipurch[0]['purch_amount'] - $irtn[0]['purch_ret_amount'];
            // $vat = $pstd[0]['vat'] +$zpurch[0]['vat'] +$epurch[0]['vat'] +$ipurch[0]['vat'];
            // $out[$d]['purchase']['vat']= $vat;

            // $pstd[0]['purch_amount'] = isset($pstd[0]['purch_amount'])? $pstd[0]['purch_amount']:0;
            // $rstd[0]['purch_ret_amount'] = isset($pstd[0]['purch_ret_amount'])? $pstd[0]['purch_ret_amount']:0;

            // $out[$d]['purchase']['standard']['purchase'] = $pstd[0]['purch_amount'];
            // $out[$d]['purchase']['standard']['return'] = $rstd[0]['purch_ret_amount'];
            // $out[$d]['purchase']['standard']['net'] = $pstd[0]['purch_amount'] - $rstd[0]['purch_ret_amount'];

            // $zpurch[0]['purch_amount'] = isset($zpurch[0]['purch_amount'])? $zpurch[0]['purch_amount']:0;
            // $rtz[0]['purch_ret_amount'] = isset($rtz[0]['purch_ret_amount'])? $rtz[0]['purch_ret_amount']:0;

            // $out[$d]['purchase']['zero_rated']['purchase'] = $zpurch[0]['purch_amount'];
            // $out[$d]['purchase']['zero_rated']['return'] =  $rtz[0]['purch_ret_amount'];
            // $out[$d]['purchase']['zero_rated']['net'] = $zpurch[0]['purch_amount'] - $rtz[0]['purch_ret_amount'];

            // $epurch[0]['purch_amount'] = isset($epurch[0]['purch_amount'])? $epurch[0]['purch_amount']:0;
            // $ertn[0]['purch_ret_amount'] = isset($ertn[0]['purch_ret_amount'])? $ertn[0]['purch_ret_amount']:0;

            // $out[$d]['purchase']['exempt']['purchase'] = $epurch[0]['purch_amount'];
            // $out[$d]['purchase']['exempt']['return'] =  $ertn[0]['purch_ret_amount'];
            // $out[$d]['purchase']['exempt']['net'] = $epurch[0]['purch_amount'] - $ertn[0]['purch_ret_amount'];

            // $ipurch[0]['purch_amount'] = isset($ipurch[0]['purch_amount'])? $ipurch[0]['purch_amount']:0;
            // $irtn[0]['purch_ret_amount'] = isset($irtn[0]['purch_ret_amount'])? $irtn[0]['purch_ret_amount']:0;

            // $out[$d]['purchase']['imports_customs']['purchase'] = $ipurch[0]['purch_amount'];
            // $out[$d]['purchase']['imports_customs']['return'] =  $irtn[0]['purch_ret_amount'];
            // $out[$d]['purchase']['imports_customs']['net'] = $ipurch[0]['purch_amount'] - $irtn[0]['purch_ret_amount'];

            // $out[$d]['purchase']['purchase_vat']['purchase'] = 1;

            $amnt = DB::raw('SUM(salesub_rate * salesub_qty) as sale_amount');

            //$qrysale = SalesSub::select('salesub_sales_inv_no', 'salesub_date', 'salesub_tax_per', 'salesub_tax_rate',$amnt);

            $retamnt = DB::raw('SUM(salesretsub_rate * 	salesretsub_qty) as sale_ret_amount');

            // $retsaleqry = SalesReturnSub::select('salesretsub_sales_inv_no', 'salesretsub_date', 'salesretsub_tax_per',
            //     'salesretsub_tax_rate'  , $retamnt);
            $vat_deduct =$vat_deduct2= $sale_amount_total = 0;
            foreach ($taxcats as $k => $tax) {
                $taxcat_name = $tax['taxcat_name'];
              
                $taxcat_auto_id = $tax['taxcat_auto_id'];

                if($taxcat_auto_id==1)
                {
                    $display_name = 'five';
                }
                
                if($taxcat_auto_id==2)
                {
                    $display_name = 'fifty';
                }
                
                if($taxcat_auto_id==3)
                {
                    $display_name = 'zero';
                }
                $taxcat_id = $tax['taxcat_id'];
                $taxcat_tax_per = $tax['taxcat_tax_per'];
                //$qrysale = clone $qrysale->newQuery();
                //$retsaleqry = clone $retsaleqry->newQuery();

                // die($taxcat_id."==");
                // ->where('salesub_taxcat_id', $taxcat_id)
                //die($d)
                $rslt = SalesSub::select($amnt, 'salesub_tax_rate')->where('salesub_taxcat_id', $taxcat_id)->where('salesub_date', $d)->get()->toArray();
                $rtrslt = SalesReturnSub::select('salesretsub_sales_inv_no', 'salesretsub_date', 'salesretsub_tax_per',
                    'salesretsub_tax_rate', 'salesretsub_tax_rate'
                    , $retamnt)->where('salesretsub_tax_per', $taxcat_tax_per)->where('salesretsub_date', $d)->get()->toArray();

                $sale_amount = isset($rslt[0]['sale_amount']) ? $rslt[0]['sale_amount'] : 0;
                $sale_ret_amount = isset($rtrslt[0]['sale_ret_amount']) ? $rtrslt[0]['sale_ret_amount'] : 0;
                $sale_amount = isset($rslt[0]['sale_amount']) ? $rslt[0]['sale_amount'] : 0;
                //    print_r( $rtrslt);die();
                // $out[$d]['sale'][$taxcat_name]['sale'] =  $sale_amount;
                // $out[$d]['sale'][$taxcat_name]['return'] =  $sale_ret_amount;
                // $out[$d]['sale'][$taxcat_name] =  $sale_amount- $sale_ret_amount;
                // $out[$d]['sale']['vat_deduct'] =  $vat_deduct;
                // $vat_deduct =$vat_deduct +$rslt[0]['salesub_tax_rate'];

                $vat_deduct = $vat_deduct + $rslt[0]['salesub_tax_rate'];

                $vat_deduct2 = $vat_deduct2 + $rtrslt[0]['salesretsub_tax_rate'];
                $out[$i][$display_name] = $sale_amount - $sale_ret_amount;
                $out[$i][$taxcat_auto_id] = $sale_amount - $sale_ret_amount;
                $sale_amount_total = $sale_amount_total + $sale_amount;
                $out[$i]['sale_amount'] = $sale_amount_total;
                $out[$i]['vat_deduct'] = $vat_deduct- $vat_deduct2;

            }

            if ($out[$i]['vat_deduct'] == 0 && $out[$i]['vat_deduct'] == 0) {
                unset($out[$i]);
            }
            else{

                $net_vat = $out[$i]['vat_deduct']-  $out[$i]['vat'] ;
                $out[$i]['net_vat'] =$net_vat;
                $vat_sum=$net_vat+$vat_sum;
                $i++;
            }
          
        }
        return ['data' => $out,'vat_sum' => $vat_sum, 'branch' => Acc_branch::where('branch_id',$this->branch_id)->first(),'titles_purchase' => '', 'titles_sales' => ''];

    }

    public function getDatesFromRange($Date1, $Date2)
    {

        $array = array();

        $Variable1 = strtotime($Date1);
        $Variable2 = strtotime($Date2);

        for ($currentDate = $Variable1; $currentDate <= $Variable2;
            $currentDate += (86400)) {

            $Store = date('Y-m-d', $currentDate);
            $array[] = $Store;
        }
        return $array;
    }
    public function inputVatSummary($request)
    {

        $inv_filter = isset($request['inv_filter']) ? $request['inv_filter'] : 0;
        $inv_val = isset($request['inv_val']) ? $request['inv_val'] : 0;
        $inv_val2 = isset($request['inv_val2']) ? $request['inv_val2'] : 0;


        // Purchase section 
        $amnt = DB::raw('SUM(purchsub_rate * purchsub_qty) as purch_amount');
        $rate = DB::raw('SUM(purchsub_tax) as tax_rate');

        $qry = PurchaseSub::select('purchase_subs.purchsub_purch_id', 'purchase_subs.purchsub_date'
            , $rate, $amnt, 'purchase_subs.branch_id', 'purchases.purch_inv_no', 'purchases.purch_frieght', 'purchases.purch_discount', 'acc_ledgers.ledger_name', 'acc_ledgers.ledger_tin', 'purchases.purch_type');

        $qry->join('purchases', 'purchases.purch_id', '=', 'purchase_subs.purchsub_purch_id');
        $qry->join('suppliers', 'suppliers.supp_id', '=', 'purchases.purch_supp_id');
        $qry->join('acc_ledgers', 'acc_ledgers.ledger_id', '=', 'suppliers.supp_ledger_id');
        
        
        // Purchase qry end 

        // Purchase Return section 

        $retamnt = DB::raw('SUM(purchretsub_rate * purchretsub_qty) as purch_ret_amount');
        $retrate = DB::raw('SUM(purchretsub_tax) as purch_ret_tax');

        $retqry = PurchaseReturnSub::select('purchase_return_sub.purchretsub_purch_id', 'purchase_return.purchret_inv_no', 'purchase_return_sub.purchretsub_date'
            , $retrate, $retamnt, 'purchase_return_sub.branch_id', 'purchase_return.purchret_discount', 'purchase_return.purchret_frieght', 'acc_ledgers.ledger_name', 'acc_ledgers.ledger_tin', 'purchase_return.purchret_type');

        $retqry->join('purchase_return', 'purchase_return.purchret_id', '=', 'purchase_return_sub.purchretsub_purch_id');
        $retqry->join('suppliers', 'suppliers.supp_id', '=', 'purchase_return.purchret_supp_id');
        $retqry->join('acc_ledgers', 'acc_ledgers.ledger_id', '=', 'suppliers.supp_ledger_id');
        // Purchase Return section end 
        
        if ($inv_filter && $inv_val) {
            switch ($inv_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $qry = $qry->where('purchase_subs.purchsub_purch_id', $inv_filter, $inv_val);

                    $retqry = $retqry->where('purchase_return_sub.purchretsub_purch_id', $inv_filter, $inv_val);

                    break;
                case 'between':
                    if ($inv_val) {
                        $qry = $qry->whereBetween('purchase_subs.purchsub_purch_id', [$inv_val, $inv_val2]);
                        $retqry = $retqry->whereBetween('purchase_return_sub.purchretsub_purch_id', [$inv_val, $inv_val2]);

                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }

        if ($ptype = isset($request['period_type']) ? $request['period_type'] : 0) {
            list($date1, $date2) = $this->datesFromPeriods($ptype, $request);

            if ($date1 && $date2) {
                $qry = $qry->whereBetween('purchase_subs.purchsub_date', [$date1, $date2]);
                $retqry = $retqry->whereBetween('purchase_return_sub.purchretsub_date', [$date1, $date2]);

            }
        }


        if($this->branch_id > 0 ){
            $qry = $qry->where('purchase_subs.branch_id',$this->branch_id);
            $retqry = $retqry->where('purchase_return_sub.branch_id',$this->branch_id);

            $branchDet = Acc_branch::select('branch_name','branch_code','branch_tin')->where('branch_id',$this->branch_id)->first();

        }else{
            if($request['branch_id'] != ''){
                $qry = $qry->whereIn('purchase_subs.branch_id', $request['branch_id']);
                $retqry = $retqry->whereIn('purchase_return_sub.branch_id', $request['branch_id']);          
                $branchDet = Acc_branch::select('branch_name','branch_code','branch_tin')->where('branch_id',$request['branch_id'][0])->first();
            
            }else{
                $branchDet = Acc_branch::select('branch_name','branch_code','branch_tin')->first();
            }
        }

        $qry = $qry->where('purchase_subs.purchsub_flag',1);
        $retqry = $retqry->where('purchase_return_sub.purchretsub_flag',1);

        $data = [];
        if ($taxcat_id = $request['taxcat_id']) {

            $taxcat = TaxCategory::where('taxcat_id', $taxcat_id)->first();
            $taxcat_name = $taxcat->taxcat_name;
            $taxcat_tax_per = $taxcat->taxcat_name;
            $taxcat_dispname = $taxcat->display_name;
            $data['branch'] = $branchDet;
            $data['category']['name'] = $taxcat_name;
            $data['category']['dispname'] =$taxcat_dispname;

            $data['items']  = $qry->where('purchase_subs.purchsub_tax_per', $taxcat_tax_per)->groupby('purchase_subs.purchsub_purch_id')->get()->toArray();
            $data['category']['amt_total'] = array_sum(array_column($data['items'], 'purch_amount'));
            $data['category']['vat_total'] = array_sum(array_column($data['items'], 'tax_rate'));
            
            $data['ret_cat']['name'] = $taxcat_name . "_return";
            $data['ret_cat']['dispname'] = $taxcat_dispname;
            $data['ret_items'] = $retqry->where('purchase_return_sub.purchretsub_tax_per', $taxcat_tax_per)->groupby('purchase_return_sub.purchretsub_purch_id')->get()->toArray();
            $data['ret_cat']['amt_total'] = array_sum(array_column($data['ret_items'], 'purch_ret_amount'));
            $data['ret_cat']['vat_total'] = array_sum(array_column($data['ret_items'], 'purch_ret_tax'));
            $data = array($data);
        } else {
            $taxcats = TaxCategory::get()->toArray();

            foreach ($taxcats as $k => $val) {
                $taxcat_name = $val['taxcat_name'];
                $taxcat_id = $val['taxcat_id'];
                $taxcat_dispname = $val['display_name'];
                $taxcat_tax_per = $val['taxcat_tax_per'];
                $sqlQry = clone $qry->newQuery();
                $retSqlQryy = clone $retqry->newQuery();
                $data[$k]['branch'] = $branchDet;

                $data[$k]['category']['name'] = $taxcat_name;
                $data[$k]['category']['dispname'] =$taxcat_dispname;
                $purch_type =2;
                
                $data[$k]['items'] = $sqlQry->where('purchase_subs.purchsub_tax_per', $taxcat_tax_per)->groupby('purchase_subs.purchsub_purch_id')->get()->toArray();
                $data[$k]['category']['amt_total'] = array_sum(array_column($data[$k]['items'], 'purch_amount'));
                $data[$k]['category']['vat_total'] = array_sum(array_column($data[$k]['items'], 'tax_rate'));

                $data[$k]['ret_cat']['name'] = $taxcat_name . "_return";
                $data[$k]['ret_cat']['dispname'] = $taxcat_dispname;
                $data[$k]['ret_items'] = $retSqlQryy->where('purchase_return_sub.purchretsub_tax_per', $taxcat_tax_per)->groupby('purchase_return_sub.purchretsub_purch_id')->get()->toArray();
                $data[$k]['ret_cat']['amt_total'] = array_sum(array_column($data[$k]['ret_items'], 'purch_ret_amount'));
                $data[$k]['ret_cat']['vat_total'] = array_sum(array_column($data[$k]['ret_items'], 'purch_ret_tax'));
            }

        }
    

        // $tot = $qry->groupby('purchase_subs.purchsub_purch_id')->get()->toArray();
        // $data = $qry->groupby('purchase_subs.purchsub_purch_id')->paginate($this->per_page)->toArray();

        return $data;

    }

    // Function For Output Vat Calculation
    public function outVatSummary($request)
    {
        

        $inv_filter = isset($request['inv_filter']) ? $request['inv_filter'] : 0;
        $inv_val = isset($request['inv_val']) ? $request['inv_val'] : 0;
        $inv_val2 = isset($request['inv_val2']) ? $request['inv_val2'] : 0;

        $amnt = DB::raw('SUM(salesub_rate * salesub_qty) as sale_amount');

        $qry = SalesSub::select('salesub_sales_inv_no', 'salesub_date', 'salesub_tax_per', 'salesub_tax_rate', $amnt);

        $retamnt = DB::raw('SUM(salesretsub_rate * 	salesretsub_qty) as sale_ret_amount');

        $retqry = SalesReturnSub::select('salesretsub_sales_inv_no', 'salesretsub_date', 'salesretsub_tax_per',
            'salesretsub_tax_rate'
            , $retamnt);

        if ($inv_filter && $inv_val) {
            switch ($inv_filter) {
                case '<':
                case '<=':
                case '=':
                case '>':
                case '>=':
                    $qry = $qry->where('sale_sub.salesub_sales_inv_no', $inv_filter, $inv_val);
                    break;
                case 'between':
                    if ($inv_val) {
                        $qry = $qry->whereBetween('sale_sub.salesub_sales_inv_no', [$inv_val, $inv_val2]);
                    }
                    break;
                default:
                    return parent::displayError('Invalid Request', $this->badrequest_stat);
                    break;
            }
        }
        $date1 = date("Y-01-01");
        $date2 = date("Y-01-31");
        if ($ptype = isset($request['period_type']) ? $request['period_type'] : 0) {
            list($date1, $date2) = $this->datesFromPeriods($ptype, $request);
           
            if ($date1 && $date2) {
                $qry = $qry->whereBetween('sale_sub.salesub_date', [$date1, $date2]);
            }
        }
        $data = [];
        if ($taxcat_id = $request['taxcat_id']) {

            $taxcat = TaxCategory::where('taxcat_id', $taxcat_id)->first();
            $taxcat_name = $taxcat->taxcat_name;
            $taxcat_tax_per = $taxcat->taxcat_name;
            $data[$taxcat_name] = $qry->where('salesub_taxcat_id', $taxcat_id)->groupby('sale_sub.salesub_date')->paginate($this->per_page)->toArray();
            $data[$taxcat_name . "_return"] = $retqry->where('salesretsub_tax_per', $taxcat_tax_per)->groupby('salesretsub_date')->paginate($this->per_page)->toArray();

        } else {

            $taxcats = TaxCategory::get()->toArray();

            foreach ($taxcats as $k => $val) {
                $taxcat_name = $val['taxcat_name'];
                $taxcat_id = $val['taxcat_id'];
                $taxcat_tax_per = $val['taxcat_tax_per'];
                $sqlQry = clone $qry->newQuery();
                $retSqlQryy = clone $retqry->newQuery();
                $data[$taxcat_name] = $sqlQry->where('sale_sub.salesub_taxcat_id', $taxcat_id)->groupby('salesub_date')->paginate($this->per_page)->toArray();
                $data[$taxcat_name . "_return"] = $retSqlQryy->where('salesretsub_tax_per', $taxcat_tax_per)->groupby('salesretsub_date')->paginate($this->per_page)->toArray();

            }

        }

        return $data;

    }

    public function datesFromPeriods($pt, $rq)
    {

        if ($pt >= 0 && $pt <= 12) {

            if ($pt < 10) {
                $pt = "0" . $pt;
            }
            $date1 = date("Y-$pt-d");
            $date2 = date("Y-$pt-d");

        }

        if ($pt = 'a') {
            $year = date('Y') - 1;
            $date1 = $year . "-01-01";
            $date2 = $year . "-12-31";
        }
        if ($pt == 'c') {
            $date1 = isset($rq['date1']) ? date('Y-m-d', strtotime($rq['date1'])) : 0;
            $date2 = isset($rq['date2']) ? date('Y-m-d', strtotime($rq['date2'])) : 0;

        }

        return [$date1, $date2];
    }

}
