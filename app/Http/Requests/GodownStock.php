<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;


class GodownStock extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      
        /* Custom Rules */

        if ($this->type == 'add') {
        $rule = [
            'gs_godown_id' => 'required',
            'gs_branch_stock_id' => 'required',
            'gs_stock_id' => 'required',
            'gs_prd_id' => 'required',
            'gs_qty' => 'required',
            
    ];
        }
        if ($this->type == 'edit') {
           
             $rule = [
            'gs_id' => 'required',
            'gs_godown_id' => 'required',
            'gs_branch_stock_id' => 'required',
            'gs_stock_id' => 'required',
            'gs_prd_id' => 'required',
            'gs_qty' => 'required',
           
            ];
        }

        return $rule;
    }

    public function messages()
    {
        /* Common Messages */
      

        /* Custom Messages */

          if ($this->type == 'add') {
        $message = [
            'gs_godown_id.required' => 'Required',
            'gs_branch_stock_id.required' => 'Required',
            'gs_stock_id.required' => 'Required',
            'gs_prd_id.required' => 'Required',
            'gs_qty.required' => 'Required'
        ];
    }
        if ($this->type == 'edit') {
           $message = [
            'gs_id.required' => 'Required',
            'gs_godown_id.required' => 'Required',
            'gs_branch_stock_id.required' => 'Required',
            'gs_stock_id.required' => 'Required',
            'gs_prd_id.required' => 'Required',
            'gs_qty.required' => 'Required'
        ];
        }
        return $message;
    }


    protected function validationData()
    {
        return array_merge($this->request->all(), [
            'type' => Route::input('type'),
        ]);  // Combines Post Request & route parameters
    }
}
