<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Http\Exceptions\HttpResponseException;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Route;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

// ===================== Code By Rakesh =======================

class AddGodown extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        /* Common Rules */
      
        /* Custom Rules */
           if ($this->type == 'add') {
        $rule = ['gd_name' => 'required|unique:godown_master'];
    }
        if ($this->type == 'edit') {
            $rule['gd_id'] = 'required';
             $rule['gd_name'] = ['required',Rule::unique('godown_master')->ignore($this->gd_id, 'gd_id')];
        }

        return $rule;
    }

    public function messages()
    {
        /* Common Messages */

        /* Custom Messages */

        if ($this->type == 'add') {
        $message = ['gd_name.required' => 'Required',
        'gd_name.unique' => 'Already Exists'];
    }

        if ($this->type == 'edit') {
            $message['gd_name.required'] = 'Required';
            $message['gd_name.unique'] = 'Already Exists';
        }
        return $message;
    }
     protected function failedValidation(Validator $validator): void
    {
        $errors = $validator->errors();

        throw new HttpResponseException(response()->json([
            'errors' => $errors
        ], Response::HTTP_UNPROCESSABLE_ENTITY));
    }

    protected function validationData()
    {
        return array_merge($this->request->all(), [
            'type' => Route::input('type'),
        ]);  // Combines Post Request & route parameters
    }
}
