<?php

namespace App\Providers;

use Laravel\Passport\Passport; 

use Illuminate\Support\ServiceProvider;
use App\Models\Product\Feature;
use Validator;
use Illuminate\Support\Facades\Input;
use App\Models\Product\ProdUnit;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
       
               Validator::extend('uniqueedit', function($attribute, $value, $parameters, $validator) {
                $feat_name = ucfirst(array_get($validator->getData(), $parameters[0], null));               
                $id =  $parameters[1];
                $data = Feature::find($id);
                $count = Feature::where('feat_name',$feat_name)->get()->count(); 
                
                if(($count ==1 && $data->feat_name==$feat_name ) || $count ==0)
                 return $value  == $feat_name;
                
        });

            Validator::extend('bar_code', function($attribute, $value, $parameters) {     
            $count = ProdUnit::where('produnit_ean_barcode',$value)->get()->count();        
            $itemName = 'test';
             if($count == 0)
              return (array(true=>true,itemname=>$itemName));
        });   
    
    }
}
