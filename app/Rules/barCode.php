<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\Product\ProdUnit;

class barCode implements Rule
{
 
    public $itemName;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if($value!=""){
          $count = ProdUnit::where('produnit_ean_barcode',$value)->join('products', 'prod_units.produnit_prod_id', '=', 'products.prd_id')->pluck('products.prd_name'); 
          } else
          {
           $count =[]; 
          } 
          if (count($count)>0)
          {
           $this->itemName = $count[0];
          }else{
        return true;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {     
      return 'The Barcode is associated with another item. Item Name :'.$this->itemName;
    }
}
