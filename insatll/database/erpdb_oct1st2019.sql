-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Oct 01, 2019 at 10:36 AM
-- Server version: 5.7.24
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `erpdb_oct1st2019`
--

-- --------------------------------------------------------

--
-- Table structure for table `erp_acc_branches`
--

DROP TABLE IF EXISTS `erp_acc_branches`;
CREATE TABLE IF NOT EXISTS `erp_acc_branches` (
  `branch_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `branch_name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `branch_code` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `branch_address` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `branch_notes` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `branch_default` smallint(6) NOT NULL DEFAULT '0',
  `branch_flags` smallint(6) NOT NULL DEFAULT '1',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`branch_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_acc_customers`
--

DROP TABLE IF EXISTS `erp_acc_customers`;
CREATE TABLE IF NOT EXISTS `erp_acc_customers` (
  `cust_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ledger_id` int(11) NOT NULL DEFAULT '0',
  `cust_home_addr` varchar(250) COLLATE utf8_unicode_ci DEFAULT 'NILL',
  `cust_image` varchar(250) COLLATE utf8_unicode_ci DEFAULT 'NILL',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`cust_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_acc_groups`
--

DROP TABLE IF EXISTS `erp_acc_groups`;
CREATE TABLE IF NOT EXISTS `erp_acc_groups` (
  `accgrp_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `accgrp_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `accgrp_nature` smallint(6) DEFAULT '0',
  `accgrp_parent` smallint(6) DEFAULT '0',
  `accgrp_root` smallint(6) DEFAULT '0',
  `accgrp_depth` smallint(6) DEFAULT '0',
  `accgrp_edit` smallint(6) DEFAULT '0',
  `accgrp_flags` tinyint(4) NOT NULL DEFAULT '0',
  `accgrp_enable_ledger` smallint(6) DEFAULT '0',
  `accgrp_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT '0',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` int(11) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`accgrp_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1000 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `erp_acc_groups`
--

INSERT INTO `erp_acc_groups` (`accgrp_id`, `accgrp_name`, `accgrp_nature`, `accgrp_parent`, `accgrp_root`, `accgrp_depth`, `accgrp_edit`, `accgrp_flags`, `accgrp_enable_ledger`, `accgrp_code`, `company_id`, `branch_id`, `server_sync_flag`, `server_sync_time`, `local_sync_time`, `created_at`, `updated_at`) VALUES
(1, 'Current Assets', 2, 1, 1, 0, 0, 1, 1, '', 0, 0, 0, NULL, NULL, NULL, NULL),
(2, 'Fixed Assets', 2, 2, 2, 0, 0, 1, 1, '', 0, 0, 0, NULL, NULL, NULL, NULL),
(3, 'Cash in Hand', 2, 1, 1, 1, 0, 1, 1, '', 0, 0, 0, NULL, NULL, NULL, NULL),
(4, 'Bank Accounts', 2, 1, 1, 1, 0, 1, 1, '', 0, 0, 0, NULL, NULL, NULL, NULL),
(5, 'Indirect Expense', 1, 5, 5, 0, 0, 1, 1, '', 0, 0, 0, NULL, NULL, NULL, NULL),
(6, 'Direct Expense', 1, 6, 6, 0, 0, 1, 1, '', 0, 0, 0, NULL, NULL, NULL, NULL),
(7, 'Indirect Income', 0, 7, 7, 0, 0, 1, 1, '', 0, 0, 0, NULL, NULL, NULL, NULL),
(8, 'Direct Income', 0, 8, 8, 0, 0, 1, 1, '', 0, 0, 0, NULL, NULL, NULL, NULL),
(9, 'Sales Accounts', 0, 9, 9, 0, 0, 1, 1, '', 0, 0, 0, NULL, NULL, NULL, NULL),
(10, 'Purchase Accounts', 1, 10, 10, 0, 0, 1, 1, '', 0, 0, 0, NULL, NULL, NULL, NULL),
(11, 'Capital Account', 3, 11, 11, 0, 0, 1, 1, '', 0, 0, 0, NULL, NULL, NULL, NULL),
(12, 'Current Liability', 3, 12, 12, 0, 0, 1, 1, '', 0, 0, 0, NULL, NULL, NULL, NULL),
(13, 'Sundry Creditors', 3, 12, 12, 1, 0, 1, 1, '', 0, 0, 0, NULL, NULL, NULL, NULL),
(14, 'Sundry Deptors', 2, 1, 1, 1, 0, 1, 1, '', 0, 0, 0, NULL, NULL, NULL, NULL),
(15, 'Duties and Taxes', 3, 12, 12, 1, 0, 1, 1, '', 0, 0, 0, NULL, NULL, NULL, NULL),
(16, 'Stock in Hand', 2, 1, 1, 1, 0, 1, 1, '', 0, 0, 0, NULL, NULL, NULL, NULL),
(17, 'Service Accounts', 0, 17, 17, 0, 0, 1, 1, '', 0, 0, 0, NULL, NULL, NULL, NULL),
(18, 'Loans (Liability)', 3, 18, 18, 0, 0, 1, 1, '', 0, 0, 0, NULL, NULL, NULL, NULL),
(19, 'Salary', 3, 19, 19, 0, 0, 1, 1, '', 0, 0, 0, NULL, NULL, NULL, NULL),
(20, 'Supplier', 3, 12, 12, 1, 0, 1, 1, '', 0, 0, 0, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `erp_acc_ledgers`
--

DROP TABLE IF EXISTS `erp_acc_ledgers`;
CREATE TABLE IF NOT EXISTS `erp_acc_ledgers` (
  `ledger_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ledger_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NILL',
  `ledger_accgrp_id` smallint(6) NOT NULL DEFAULT '0',
  `ledger_acc_type` smallint(6) DEFAULT '2' COMMENT '0-cash 1-bank 2-other',
  `ledger_return` smallint(6) DEFAULT '1' COMMENT '0-no 1-yes',
  `ledger_address` varchar(250) COLLATE utf8_unicode_ci DEFAULT 'NILL',
  `ledger_notes` varchar(250) COLLATE utf8_unicode_ci DEFAULT 'NILL',
  `ledger_edit` smallint(6) DEFAULT '1',
  `ledger_flags` smallint(6) NOT NULL DEFAULT '0',
  `ledger_branch_id` smallint(6) DEFAULT '1',
  `ledger_balance_privacy` smallint(6) DEFAULT '0',
  `ledger_due` smallint(6) DEFAULT '0',
  `ledger_tin` varchar(50) COLLATE utf8_unicode_ci DEFAULT 'NILL',
  `ledger_alias` varchar(50) COLLATE utf8_unicode_ci DEFAULT 'NILL',
  `ledger_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT 'NILL',
  `ledger_email` varchar(50) COLLATE utf8_unicode_ci DEFAULT 'NILL',
  `ledger_mobile` varchar(50) COLLATE utf8_unicode_ci DEFAULT 'NILL',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ledger_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_acc_opening_balances`
--

DROP TABLE IF EXISTS `erp_acc_opening_balances`;
CREATE TABLE IF NOT EXISTS `erp_acc_opening_balances` (
  `opbal_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `opbal_ledger_id` int(11) NOT NULL DEFAULT '0',
  `opbal_in` double(8,2) NOT NULL DEFAULT '0.00',
  `opbal_out` double(8,2) NOT NULL DEFAULT '0.00',
  `opbal_added_by` int(11) NOT NULL DEFAULT '1',
  `opbal_flags` tinyint(4) NOT NULL DEFAULT '1',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` int(11) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`opbal_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_acc_suppliers`
--

DROP TABLE IF EXISTS `erp_acc_suppliers`;
CREATE TABLE IF NOT EXISTS `erp_acc_suppliers` (
  `supp_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ledger_id` int(11) NOT NULL DEFAULT '0',
  `supp_perm_addr` varchar(250) COLLATE utf8_unicode_ci DEFAULT 'NILL',
  `supp_image` varchar(250) COLLATE utf8_unicode_ci DEFAULT 'NILL',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`supp_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_branch_stocks`
--

DROP TABLE IF EXISTS `erp_branch_stocks`;
CREATE TABLE IF NOT EXISTS `erp_branch_stocks` (
  `branch_stock_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `bs_stock_id` bigint(20) NOT NULL,
  `bs_prd_id` bigint(20) NOT NULL,
  `bs_stock_quantity` double NOT NULL DEFAULT '0',
  `bs_stock_quantity_shop` double NOT NULL DEFAULT '0',
  `bs_stock_quantity_gd` double NOT NULL DEFAULT '0',
  `bs_prate` double NOT NULL DEFAULT '0',
  `bs_avg_prate` double NOT NULL DEFAULT '0',
  `bs_srate` double NOT NULL DEFAULT '0',
  `bs_batch_id` bigint(20) NOT NULL DEFAULT '0',
  `bs_expiry` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `bs_stk_status` tinyint(4) NOT NULL DEFAULT '1',
  `bs_branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`branch_stock_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_branch_stock_logs`
--

DROP TABLE IF EXISTS `erp_branch_stock_logs`;
CREATE TABLE IF NOT EXISTS `erp_branch_stock_logs` (
  `bsl_log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `bsl_branch_stock_id` bigint(20) NOT NULL,
  `bsl_stock_id` bigint(20) DEFAULT NULL,
  `bsl_prd_id` bigint(20) DEFAULT NULL,
  `bsl_shop_quantity` double NOT NULL DEFAULT '0',
  `bsl_gd_quantity` double NOT NULL DEFAULT '0',
  `bsl_prate` double NOT NULL DEFAULT '0',
  `bsl_srate` double NOT NULL DEFAULT '0',
  `bsl_revert` tinyint(4) NOT NULL DEFAULT '0',
  `bsl_avg_prate` double NOT NULL DEFAULT '0',
  `bsl_branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`bsl_log_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_categories`
--

DROP TABLE IF EXISTS `erp_categories`;
CREATE TABLE IF NOT EXISTS `erp_categories` (
  `cat_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `cat_remarks` mediumtext COLLATE utf8_unicode_ci,
  `cat_flag` tinyint(4) NOT NULL DEFAULT '1',
  `cat_pos` tinyint(4) DEFAULT '1',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_company_stocks`
--

DROP TABLE IF EXISTS `erp_company_stocks`;
CREATE TABLE IF NOT EXISTS `erp_company_stocks` (
  `cmp_stock_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cmp_prd_id` bigint(20) NOT NULL,
  `cmp_stock_quantity` double NOT NULL DEFAULT '0',
  `cmp_avg_prate` double NOT NULL DEFAULT '0',
  `cmp_srate` double NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`cmp_stock_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_damages`
--

DROP TABLE IF EXISTS `erp_damages`;
CREATE TABLE IF NOT EXISTS `erp_damages` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `damage_stock_id` int(11) NOT NULL DEFAULT '0',
  `damage_prod_id` int(11) NOT NULL DEFAULT '0',
  `damage_qty` double DEFAULT '0',
  `damage_rem_qty` double DEFAULT '0',
  `damage_purch_rate` double DEFAULT '0',
  `damage_notes` varchar(100) COLLATE utf8_unicode_ci DEFAULT 'NILL',
  `damage_serail` varchar(100) COLLATE utf8_unicode_ci DEFAULT 'NILL',
  `damage_date` date DEFAULT '2000-01-01',
  `damage_added_by` int(11) DEFAULT '0',
  `damage_flags` tinyint(4) NOT NULL DEFAULT '1',
  `damage_unit_id` int(11) DEFAULT '1',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_features`
--

DROP TABLE IF EXISTS `erp_features`;
CREATE TABLE IF NOT EXISTS `erp_features` (
  `feat_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `feat_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `feat_description` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` int(11) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`feat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_feature_products`
--

DROP TABLE IF EXISTS `erp_feature_products`;
CREATE TABLE IF NOT EXISTS `erp_feature_products` (
  `fetprod_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `fetprod_prod_id` int(11) NOT NULL DEFAULT '0',
  `fetprod_fet_id` int(11) DEFAULT '0',
  `fetprod_fet_val` varchar(200) COLLATE utf8_unicode_ci DEFAULT 'Nill',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`fetprod_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_godown_master`
--

DROP TABLE IF EXISTS `erp_godown_master`;
CREATE TABLE IF NOT EXISTS `erp_godown_master` (
  `gd_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `gd_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gd_code` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gd_cntct_person` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gd_cntct_num` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gd_address` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gd_desc` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`gd_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_godown_stocks`
--

DROP TABLE IF EXISTS `erp_godown_stocks`;
CREATE TABLE IF NOT EXISTS `erp_godown_stocks` (
  `gs_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `gs_godown_id` bigint(20) NOT NULL,
  `gs_branch_stock_id` bigint(20) NOT NULL,
  `gs_stock_id` bigint(20) NOT NULL,
  `gs_prd_id` bigint(20) NOT NULL,
  `gs_qty` int(11) NOT NULL DEFAULT '0',
  `gs_date` date NOT NULL DEFAULT '2019-10-01',
  `gs_flag` tinyint(4) NOT NULL DEFAULT '1',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`gs_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_godown_stock_log`
--

DROP TABLE IF EXISTS `erp_godown_stock_log`;
CREATE TABLE IF NOT EXISTS `erp_godown_stock_log` (
  `gsl_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `gsl_gdwn_stock_id` bigint(20) NOT NULL,
  `gsl_branch_stock_id` bigint(20) NOT NULL,
  `gsl_stock_id` bigint(20) NOT NULL,
  `gsl_prd_id` bigint(20) NOT NULL,
  `gsl_prod_unit` int(11) NOT NULL DEFAULT '0',
  `gsl_qty` int(11) NOT NULL DEFAULT '0',
  `gsl_from` int(11) NOT NULL DEFAULT '0',
  `gsl_to` int(11) NOT NULL DEFAULT '0',
  `gsl_vchr_type` int(11) NOT NULL DEFAULT '0',
  `gsl_date` date NOT NULL DEFAULT '2019-10-01',
  `gsl_added_by` int(11) NOT NULL DEFAULT '0',
  `gsl_flag` tinyint(4) NOT NULL DEFAULT '1',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`gsl_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_manufacturer`
--

DROP TABLE IF EXISTS `erp_manufacturer`;
CREATE TABLE IF NOT EXISTS `erp_manufacturer` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `manftr_comp_name` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `manftr_comp_code` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `manftr_comp_address` mediumtext COLLATE utf8_unicode_ci,
  `manftr_comp_phone` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manftr_comp_mobile` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manftr_comp_fax` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manftr_comp_email` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manftr_comp_contact_person` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manftr_comp_website` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manftr_comp_addedby` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manftr_comp_add_date` date NOT NULL DEFAULT '2019-10-01',
  `manftr_comp_notes` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manftr_comp_flags` tinyint(4) DEFAULT '1',
  `manftr_created_at` timestamp NULL DEFAULT NULL,
  `manftr_updated_at` timestamp NULL DEFAULT NULL,
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `erp_manufacturer_manftr_comp_email_unique` (`manftr_comp_email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_migrations`
--

DROP TABLE IF EXISTS `erp_migrations`;
CREATE TABLE IF NOT EXISTS `erp_migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `erp_migrations`
--

INSERT INTO `erp_migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2019_06_11_062713_create_sessions_table', 1),
(9, '2019_06_12_055131_create_categories_table', 1),
(10, '2019_06_12_055325_create_units_table', 1),
(11, '2019_06_12_055625_create_subcategories_table', 1),
(12, '2019_06_12_093427_create_products_table', 1),
(13, '2019_06_12_102148_create_usertypes_table', 1),
(14, '2019_06_17_113814_create_features_table', 1),
(15, '2019_06_20_075225_create_acountgroups_table', 1),
(16, '2019_06_20_115143_create_ledger_table', 1),
(17, '2019_06_21_085945_create_branches_table', 1),
(18, '2019_06_21_100926_create_customer_table', 1),
(19, '2019_06_22_070511_create_supplier_table', 1),
(20, '2019_07_10_083429_manufacturer', 1),
(21, '2019_07_10_085528_damagedproducts', 1),
(22, '2019_07_16_110942_prod_units', 1),
(23, '2019_07_19_062705_feature_product', 1),
(24, '2019_08_14_115417_acc_opening_balance', 1),
(25, '2019_08_22_074218_create_company_stocks_table', 1),
(26, '2019_08_22_074232_create_branch_stocks_table', 1),
(27, '2019_08_22_074304_create_stock_unit_rates_table', 1),
(28, '2019_08_23_043349_stock_daily_updates', 1),
(29, '2019_08_23_052705_stock_batches', 1),
(30, '2019_08_23_092352_create_godown_masters_table', 1),
(31, '2019_08_24_065252_create_godown_stocks_table', 1),
(32, '2019_08_27_051528_godownstok_log', 1),
(33, '2019_08_28_085748_create_branchstocklogs_table', 1),
(34, '2019_08_30_053121_voucher_type', 1),
(35, '2019_09_16_071907_opening_stock_log', 1),
(36, '2019_09_25_063500_create_suppliers_table', 1),
(37, '2019_09_25_102258_create_purchases_table', 1),
(38, '2019_09_25_102335_create_purchase_subs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `erp_oauth_access_tokens`
--

DROP TABLE IF EXISTS `erp_oauth_access_tokens`;
CREATE TABLE IF NOT EXISTS `erp_oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `erp_oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_oauth_auth_codes`
--

DROP TABLE IF EXISTS `erp_oauth_auth_codes`;
CREATE TABLE IF NOT EXISTS `erp_oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_oauth_clients`
--

DROP TABLE IF EXISTS `erp_oauth_clients`;
CREATE TABLE IF NOT EXISTS `erp_oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `erp_oauth_clients_user_id_index` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `erp_oauth_clients`
--

INSERT INTO `erp_oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'e3c8LN7UwyvnnuKRwfVjO24X3EaF4MRtnk5tDEWr', 'http://localhost', 1, 0, 0, '2019-10-01 05:04:40', '2019-10-01 05:04:40'),
(2, NULL, 'Laravel Password Grant Client', 'B5uhi7lZ4x005ZRkoLZmkm44axBxkAJXkPV2FJxG', 'http://localhost', 0, 1, 0, '2019-10-01 05:04:40', '2019-10-01 05:04:40');

-- --------------------------------------------------------

--
-- Table structure for table `erp_oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `erp_oauth_personal_access_clients`;
CREATE TABLE IF NOT EXISTS `erp_oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `erp_oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `erp_oauth_personal_access_clients`
--

INSERT INTO `erp_oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-10-01 05:04:40', '2019-10-01 05:04:40');

-- --------------------------------------------------------

--
-- Table structure for table `erp_oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `erp_oauth_refresh_tokens`;
CREATE TABLE IF NOT EXISTS `erp_oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `erp_oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_opening_stock_log`
--

DROP TABLE IF EXISTS `erp_opening_stock_log`;
CREATE TABLE IF NOT EXISTS `erp_opening_stock_log` (
  `opstklog_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `opstklog_branch_stock_id` bigint(20) NOT NULL,
  `opstklog_prd_id` bigint(20) NOT NULL,
  `opstklog_stock_id` bigint(20) NOT NULL,
  `opstklog_date` date NOT NULL DEFAULT '2019-10-01',
  `opstklog_prate` double NOT NULL DEFAULT '0',
  `opstklog_srate` double NOT NULL DEFAULT '0',
  `opstklog_stock_quantity_add` double NOT NULL DEFAULT '0',
  `opstklog_stock_quantity_rem` double NOT NULL DEFAULT '0',
  `opstklog_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Add 0 | Remove 1',
  `opstklog_unit_id` bigint(20) NOT NULL DEFAULT '0',
  `opstklog_added_by` bigint(20) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`opstklog_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_password_resets`
--

DROP TABLE IF EXISTS `erp_password_resets`;
CREATE TABLE IF NOT EXISTS `erp_password_resets` (
  `email` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `erp_password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_products`
--

DROP TABLE IF EXISTS `erp_products`;
CREATE TABLE IF NOT EXISTS `erp_products` (
  `prd_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `prd_name` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `prd_cat_id` bigint(20) NOT NULL,
  `prd_sub_cat_id` bigint(20) DEFAULT '0',
  `prd_supplier` bigint(20) DEFAULT NULL,
  `prd_alias` mediumtext COLLATE utf8_unicode_ci,
  `prd_short_name` mediumtext COLLATE utf8_unicode_ci,
  `prd_min_stock` int(11) DEFAULT NULL,
  `prd_max_stock` int(11) DEFAULT NULL,
  `prd_barcode` text COLLATE utf8_unicode_ci,
  `prd_added_by` int(11) DEFAULT NULL,
  `prd_base_unit_id` int(11) NOT NULL,
  `prd_default_unit_id` int(11) DEFAULT NULL,
  `prd_tax` int(11) DEFAULT NULL,
  `prd_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prd_tax_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prd_desc` text COLLATE utf8_unicode_ci,
  `prd_exp_dur` int(11) DEFAULT NULL,
  `prd_loyalty_rate` double(15,8) DEFAULT NULL,
  `prd_type` tinyint(4) NOT NULL DEFAULT '1',
  `prd_stock_status` tinyint(4) NOT NULL DEFAULT '1',
  `prd_stock_stat` tinyint(4) NOT NULL DEFAULT '1',
  `prd_minstock_alert` tinyint(4) DEFAULT NULL,
  `prd_maxstock_alert` tinyint(4) DEFAULT NULL,
  `prd_remarks` mediumtext COLLATE utf8_unicode_ci,
  `prd_flag` tinyint(4) NOT NULL DEFAULT '1',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`prd_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_prod_units`
--

DROP TABLE IF EXISTS `erp_prod_units`;
CREATE TABLE IF NOT EXISTS `erp_prod_units` (
  `produnit_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `produnit_prod_id` int(11) NOT NULL DEFAULT '0',
  `produnit_unit_id` int(11) DEFAULT '0',
  `produnit_ean_barcode` varchar(255) COLLATE utf8_unicode_ci DEFAULT '0',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`produnit_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_purchases`
--

DROP TABLE IF EXISTS `erp_purchases`;
CREATE TABLE IF NOT EXISTS `erp_purchases` (
  `purch_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `purch_id2` bigint(20) NOT NULL,
  `purch_branch_id` bigint(20) NOT NULL,
  `purch_no` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `purch_inv_no` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `purch_supp_id` bigint(20) NOT NULL,
  `purch_tax_ledger_id` bigint(20) NOT NULL,
  `purch_ord_no` bigint(20) NOT NULL,
  `purch_type` tinyint(4) NOT NULL,
  `purch_pay_type` tinyint(4) NOT NULL,
  `purch_date` date NOT NULL,
  `purch_inv_date` date NOT NULL,
  `purch_amount` double NOT NULL,
  `purch_tax` double NOT NULL,
  `purch_tax2` double NOT NULL,
  `purch_tax3` double NOT NULL,
  `purch_discount` double NOT NULL,
  `purch_frieght` double NOT NULL,
  `purch_agent` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `purch_flag` tinyint(4) NOT NULL DEFAULT '1',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`purch_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_purchase_subs`
--

DROP TABLE IF EXISTS `erp_purchase_subs`;
CREATE TABLE IF NOT EXISTS `erp_purchase_subs` (
  `purchsub_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `purchsub_purch_id` bigint(20) NOT NULL,
  `purchsub_prd_id` bigint(20) NOT NULL,
  `purchsub_stock_id` bigint(20) NOT NULL,
  `purchsub_branch_id` tinyint(4) NOT NULL,
  `purchsub_batch_id` bigint(20) NOT NULL DEFAULT '0',
  `purchsub_expiry` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `purchsub_qty` bigint(20) NOT NULL,
  `purchsub_rate` double NOT NULL,
  `purchsub_frieght` double NOT NULL,
  `purchsub_tax` double NOT NULL,
  `purchsub_tax_per` double NOT NULL,
  `purchsub_tax2` double NOT NULL,
  `purchsub_tax2_per` double NOT NULL,
  `purchsub_tax3` double NOT NULL,
  `purchsub_tax3_per` double NOT NULL,
  `purchsub_unit_id` bigint(20) NOT NULL,
  `purchsub_gd_qty` bigint(20) NOT NULL,
  `purchsub_gd_id` int(11) NOT NULL,
  `purchsub_date` date NOT NULL,
  `purchsub_flag` tinyint(4) NOT NULL DEFAULT '1',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`purchsub_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_sessions`
--

DROP TABLE IF EXISTS `erp_sessions`;
CREATE TABLE IF NOT EXISTS `erp_sessions` (
  `id` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `ip_address` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'NILL',
  `user_agent` text COLLATE utf8_unicode_ci NOT NULL,
  `payload` text COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL,
  UNIQUE KEY `erp_sessions_id_unique` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_stock_batches`
--

DROP TABLE IF EXISTS `erp_stock_batches`;
CREATE TABLE IF NOT EXISTS `erp_stock_batches` (
  `sb_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sb_batch_code` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `sb_branch_stock_id` bigint(20) NOT NULL,
  `sb_prd_id` bigint(20) NOT NULL,
  `sb_stock_id` bigint(20) NOT NULL,
  `sb_quantity` bigint(20) NOT NULL,
  `sb_manufacture_date` date NOT NULL,
  `sb_expiry_date` date NOT NULL,
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`sb_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_stock_daily_updates`
--

DROP TABLE IF EXISTS `erp_stock_daily_updates`;
CREATE TABLE IF NOT EXISTS `erp_stock_daily_updates` (
  `sdu_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sdu_branch_stock_id` bigint(20) NOT NULL,
  `sdu_prd_id` bigint(20) NOT NULL,
  `sdu_stock_id` bigint(20) NOT NULL,
  `sdu_date` date NOT NULL DEFAULT '2019-10-01',
  `sdu_stock_quantity` double NOT NULL,
  `sdu_godown_id` double NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`sdu_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_stock_unit_rates`
--

DROP TABLE IF EXISTS `erp_stock_unit_rates`;
CREATE TABLE IF NOT EXISTS `erp_stock_unit_rates` (
  `sur_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `branch_stock_id` bigint(20) NOT NULL,
  `sur_prd_id` bigint(20) NOT NULL,
  `sur_stock_id` bigint(20) NOT NULL,
  `sur_unit_id` bigint(20) NOT NULL,
  `sur_unit_rate` double NOT NULL,
  `sur_branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`sur_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_subcategories`
--

DROP TABLE IF EXISTS `erp_subcategories`;
CREATE TABLE IF NOT EXISTS `erp_subcategories` (
  `subcat_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `subcat_parent_category` int(11) NOT NULL,
  `subcat_name` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subcat_remarks` mediumtext COLLATE utf8_unicode_ci,
  `subcat_flag` tinyint(4) NOT NULL DEFAULT '1',
  `subcat_pos` tinyint(4) NOT NULL DEFAULT '1',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`subcat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_suppliers`
--

DROP TABLE IF EXISTS `erp_suppliers`;
CREATE TABLE IF NOT EXISTS `erp_suppliers` (
  `supp_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `supp_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `supp_alias` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supp_code` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `supp_ledger_id` int(11) NOT NULL,
  `supp_branch_id` smallint(6) NOT NULL DEFAULT '0',
  `supp_address1` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supp_address2` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supp_zip` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supp_city` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supp_state` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supp_country` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supp_state_code` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supp_phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supp_mob` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supp_email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supp_fax` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supp_notes` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supp_flag` tinyint(4) NOT NULL DEFAULT '1',
  `supp_tin` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supp_contact` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supp_due` int(11) DEFAULT NULL,
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`supp_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_units`
--

DROP TABLE IF EXISTS `erp_units`;
CREATE TABLE IF NOT EXISTS `erp_units` (
  `unit_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `unit_name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `unit_code` char(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unit_display` char(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unit_type` int(11) NOT NULL DEFAULT '1' COMMENT '0 - derived,1 - base ',
  `unit_base_id` int(11) NOT NULL DEFAULT '0' COMMENT 'unit_id of base unit',
  `unit_base_qty` int(11) DEFAULT '1' COMMENT 'base qty/unit',
  `unit_remarks` mediumtext COLLATE utf8_unicode_ci,
  `unit_flag` tinyint(4) NOT NULL DEFAULT '1',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`unit_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_users`
--

DROP TABLE IF EXISTS `erp_users`;
CREATE TABLE IF NOT EXISTS `erp_users` (
  `usr_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `usr_type` int(11) NOT NULL,
  `usr_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `usr_username` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `usr_password` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `usr_nickname` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `usr_phone` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `usr_mobile` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `usr_email` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `usr_address` mediumtext COLLATE utf8_unicode_ci,
  `usr_active` tinyint(4) NOT NULL DEFAULT '1',
  `email_verified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`usr_id`),
  UNIQUE KEY `erp_users_usr_username_unique` (`usr_username`),
  UNIQUE KEY `erp_users_usr_email_unique` (`usr_email`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `erp_users`
--

INSERT INTO `erp_users` (`usr_id`, `usr_type`, `usr_name`, `usr_username`, `usr_password`, `usr_nickname`, `usr_phone`, `usr_mobile`, `usr_email`, `usr_address`, `usr_active`, `email_verified_at`, `remember_token`, `company_id`, `branch_id`, `server_sync_flag`, `server_sync_time`, `local_sync_time`, `created_at`, `updated_at`) VALUES
(1, 1, 'Fujishka', 'Fujishka', '$2y$10$A2wgmHzb4CYGMGvsibJOJOA8UxOJ2gj1x8ocBy3lxP8yiasJDvX5S', NULL, NULL, NULL, 'admin@fujishka.com', NULL, 1, '2019-09-18 10:49:55', NULL, 0, 0, 0, NULL, NULL, '2019-09-18 05:19:55', '2019-09-18 05:19:55');

-- --------------------------------------------------------

--
-- Table structure for table `erp_user_types`
--

DROP TABLE IF EXISTS `erp_user_types`;
CREATE TABLE IF NOT EXISTS `erp_user_types` (
  `type_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `erp_user_types`
--

INSERT INTO `erp_user_types` (`type_id`, `user_type`, `created_at`, `updated_at`) VALUES
(1, 'ADMIN', NULL, NULL),
(2, 'COMPANY', NULL, NULL),
(3, 'BRANCH', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `erp_voucher_type`
--

DROP TABLE IF EXISTS `erp_voucher_type`;
CREATE TABLE IF NOT EXISTS `erp_voucher_type` (
  `vchtype_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `vchtype_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`vchtype_id`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `erp_voucher_type`
--

INSERT INTO `erp_voucher_type` (`vchtype_id`, `vchtype_name`, `created_at`, `updated_at`) VALUES
(1, 'Payment', NULL, NULL),
(2, 'Reciept', NULL, NULL),
(3, 'Contra', NULL, NULL),
(4, 'Journal', NULL, NULL),
(5, 'Sales', NULL, NULL),
(6, 'Purchase', NULL, NULL),
(7, 'Debit note', NULL, NULL),
(8, 'Credit note', NULL, NULL),
(9, 'Purch Return', NULL, NULL),
(10, 'Sales Return', NULL, NULL),
(11, 'Service Charge', NULL, NULL),
(12, 'Void Service', NULL, NULL),
(13, 'Production', NULL, NULL),
(14, 'Void Production', NULL, NULL),
(15, 'Godown Transfer', NULL, NULL),
(16, 'Stock Receipt Return', NULL, NULL),
(17, 'Stock Receipt', NULL, NULL),
(18, 'Stock Transfer Return', NULL, NULL),
(19, 'Stock Transfer', NULL, NULL),
(20, 'Dividend Allocation', NULL, NULL),
(21, 'Starting Balance', NULL, NULL),
(22, 'Opening Stock', NULL, NULL),
(23, 'Invoice Due Receipt', NULL, NULL),
(24, 'Input Tax', NULL, NULL),
(25, 'Output Tax', NULL, NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
ALTER TABLE `erp_purchases` ADD `purch_supp_date` DATE NULL DEFAULT NULL AFTER `purch_supp_id`;
ALTER TABLE `erp_products` ADD `prd_ean` VARCHAR(250) NULL DEFAULT NULL AFTER `prd_barcode`;

ALTER TABLE `erp_opening_stock_log` ADD `opstklog_status` TINYINT NOT NULL DEFAULT '1' AFTER `opstklog_type`;

ALTER TABLE `erp_branch_stocks` ADD `bs_stock_quantity_van` DOUBLE NOT NULL DEFAULT '0' AFTER `bs_stock_quantity_gd`;

ALTER TABLE `erp_godown_stock_log` ADD `gsl_tran_vanid` INT NOT NULL DEFAULT '0' AFTER `gsl_vchr_type`;

ALTER TABLE `erp_purchase_return` ADD `cancelled_amount` DOUBLE NOT NULL DEFAULT '0' AFTER `purch_amount`;

ALTER TABLE `erp_production_products` ADD `prdnprd_rate_expected` DOUBLE NOT NULL DEFAULT '0' AFTER `prdnprd_rate`;

ALTER TABLE `erp_production_sub` CHANGE `prdnprd_prd_id` `prdnsub_prd_id` BIGINT(20) NOT NULL;