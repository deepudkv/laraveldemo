-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 09, 2019 at 05:24 AM
-- Server version: 5.6.41-84.1-log
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bquicker_fujishka`
--

-- --------------------------------------------------------

--
-- Table structure for table `erp_acc_branches`
--

CREATE TABLE `erp_acc_branches` (
  `branch_id` bigint(20) UNSIGNED NOT NULL,
  `branch_name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `branch_code` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `branch_address` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `branch_notes` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `branch_default` smallint(6) NOT NULL DEFAULT '0',
  `branch_flags` smallint(6) NOT NULL DEFAULT '1',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `erp_acc_branches`
--

INSERT INTO `erp_acc_branches` (`branch_id`, `branch_name`, `branch_code`, `branch_address`, `branch_notes`, `branch_default`, `branch_flags`, `company_id`, `server_sync_flag`, `server_sync_time`, `local_sync_time`, `created_at`, `updated_at`) VALUES
(1, 'CALICUT', 'CALICUT', 'CALICUT', NULL, 0, 1, 0, 0, 20191001110555, NULL, '2019-10-01 05:35:55', '2019-10-01 05:35:55'),
(2, 'ERNAKULAM', 'ERNAKULAM', 'ERNAKULAM', 'ERNAKULAM', 0, 1, 0, 0, 20191001110604, NULL, '2019-10-01 05:36:04', '2019-10-01 05:36:04');

-- --------------------------------------------------------

--
-- Table structure for table `erp_acc_customers`
--

CREATE TABLE `erp_acc_customers` (
  `cust_id` bigint(20) UNSIGNED NOT NULL,
  `ledger_id` int(11) NOT NULL DEFAULT '0',
  `cust_home_addr` varchar(250) COLLATE utf8_unicode_ci DEFAULT 'NILL',
  `cust_image` varchar(250) COLLATE utf8_unicode_ci DEFAULT 'NILL',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_acc_groups`
--

CREATE TABLE `erp_acc_groups` (
  `accgrp_id` bigint(20) UNSIGNED NOT NULL,
  `accgrp_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `accgrp_nature` smallint(6) DEFAULT '0',
  `accgrp_parent` smallint(6) DEFAULT '0',
  `accgrp_root` smallint(6) DEFAULT '0',
  `accgrp_depth` smallint(6) DEFAULT '0',
  `accgrp_edit` smallint(6) DEFAULT '0',
  `accgrp_flags` tinyint(4) NOT NULL DEFAULT '0',
  `accgrp_enable_ledger` smallint(6) DEFAULT '0',
  `accgrp_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT '0',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` int(11) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `erp_acc_groups`
--

INSERT INTO `erp_acc_groups` (`accgrp_id`, `accgrp_name`, `accgrp_nature`, `accgrp_parent`, `accgrp_root`, `accgrp_depth`, `accgrp_edit`, `accgrp_flags`, `accgrp_enable_ledger`, `accgrp_code`, `company_id`, `branch_id`, `server_sync_flag`, `server_sync_time`, `local_sync_time`, `created_at`, `updated_at`) VALUES
(1, 'Current Assets', 2, 1, 1, 0, 0, 1, 1, '', 0, 0, 0, NULL, NULL, NULL, NULL),
(2, 'Fixed Assets', 2, 2, 2, 0, 0, 1, 1, '', 0, 0, 0, NULL, NULL, NULL, NULL),
(3, 'Cash in Hand', 2, 1, 1, 1, 0, 1, 1, '', 0, 0, 0, NULL, NULL, NULL, NULL),
(4, 'Bank Accounts', 2, 1, 1, 1, 0, 1, 1, '', 0, 0, 0, NULL, NULL, NULL, NULL),
(5, 'Indirect Expense', 1, 5, 5, 0, 0, 1, 1, '', 0, 0, 0, NULL, NULL, NULL, NULL),
(6, 'Direct Expense', 1, 6, 6, 0, 0, 1, 1, '', 0, 0, 0, NULL, NULL, NULL, NULL),
(7, 'Indirect Income', 0, 7, 7, 0, 0, 1, 1, '', 0, 0, 0, NULL, NULL, NULL, NULL),
(8, 'Direct Income', 0, 8, 8, 0, 0, 1, 1, '', 0, 0, 0, NULL, NULL, NULL, NULL),
(9, 'Sales Accounts', 0, 9, 9, 0, 0, 1, 1, '', 0, 0, 0, NULL, NULL, NULL, NULL),
(10, 'Purchase Accounts', 1, 10, 10, 0, 0, 1, 1, '', 0, 0, 0, NULL, NULL, NULL, NULL),
(11, 'Capital Account', 3, 11, 11, 0, 0, 1, 1, '', 0, 0, 0, NULL, NULL, NULL, NULL),
(12, 'Current Liability', 3, 12, 12, 0, 0, 1, 1, '', 0, 0, 0, NULL, NULL, NULL, NULL),
(13, 'Sundry Creditors', 3, 12, 12, 1, 0, 1, 1, '', 0, 0, 0, NULL, NULL, NULL, NULL),
(14, 'Sundry Deptors', 2, 1, 1, 1, 0, 1, 1, '', 0, 0, 0, NULL, NULL, NULL, NULL),
(15, 'Duties and Taxes', 3, 12, 12, 1, 0, 1, 1, '', 0, 0, 0, NULL, NULL, NULL, NULL),
(16, 'Stock in Hand', 2, 1, 1, 1, 0, 1, 1, '', 0, 0, 0, NULL, NULL, NULL, NULL),
(17, 'Service Accounts', 0, 17, 17, 0, 0, 1, 1, '', 0, 0, 0, NULL, NULL, NULL, NULL),
(18, 'Loans (Liability)', 3, 18, 18, 0, 0, 1, 1, '', 0, 0, 0, NULL, NULL, NULL, NULL),
(19, 'Salary', 3, 19, 19, 0, 0, 1, 1, '', 0, 0, 0, NULL, NULL, NULL, NULL),
(20, 'Supplier', 3, 12, 12, 1, 0, 1, 1, '', 0, 0, 0, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `erp_acc_ledgers`
--

CREATE TABLE `erp_acc_ledgers` (
  `ledger_id` bigint(20) UNSIGNED NOT NULL,
  `ledger_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NILL',
  `ledger_accgrp_id` smallint(6) NOT NULL DEFAULT '0',
  `ledger_acc_type` smallint(6) DEFAULT '2' COMMENT '0-cash 1-bank 2-other',
  `ledger_return` smallint(6) DEFAULT '1' COMMENT '0-no 1-yes',
  `ledger_address` varchar(250) COLLATE utf8_unicode_ci DEFAULT 'NILL',
  `ledger_notes` varchar(250) COLLATE utf8_unicode_ci DEFAULT 'NILL',
  `ledger_edit` smallint(6) DEFAULT '1',
  `ledger_flags` smallint(6) NOT NULL DEFAULT '0',
  `ledger_branch_id` smallint(6) DEFAULT '1',
  `ledger_balance_privacy` smallint(6) DEFAULT '0',
  `ledger_due` smallint(6) DEFAULT '0',
  `ledger_tin` varchar(50) COLLATE utf8_unicode_ci DEFAULT 'NILL',
  `ledger_alias` varchar(50) COLLATE utf8_unicode_ci DEFAULT 'NILL',
  `ledger_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT 'NILL',
  `ledger_email` varchar(50) COLLATE utf8_unicode_ci DEFAULT 'NILL',
  `ledger_mobile` varchar(50) COLLATE utf8_unicode_ci DEFAULT 'NILL',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_acc_opening_balances`
--

CREATE TABLE `erp_acc_opening_balances` (
  `opbal_id` bigint(20) UNSIGNED NOT NULL,
  `opbal_ledger_id` int(11) NOT NULL DEFAULT '0',
  `opbal_in` double(8,2) NOT NULL DEFAULT '0.00',
  `opbal_out` double(8,2) NOT NULL DEFAULT '0.00',
  `opbal_added_by` int(11) NOT NULL DEFAULT '1',
  `opbal_flags` tinyint(4) NOT NULL DEFAULT '1',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` int(11) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_acc_suppliers`
--

CREATE TABLE `erp_acc_suppliers` (
  `supp_id` bigint(20) UNSIGNED NOT NULL,
  `ledger_id` int(11) NOT NULL DEFAULT '0',
  `supp_perm_addr` varchar(250) COLLATE utf8_unicode_ci DEFAULT 'NILL',
  `supp_image` varchar(250) COLLATE utf8_unicode_ci DEFAULT 'NILL',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_branch_stocks`
--

CREATE TABLE `erp_branch_stocks` (
  `branch_stock_id` bigint(20) UNSIGNED NOT NULL,
  `bs_stock_id` bigint(20) NOT NULL,
  `bs_prd_id` bigint(20) NOT NULL,
  `bs_stock_quantity` double NOT NULL DEFAULT '0',
  `bs_stock_quantity_shop` double NOT NULL DEFAULT '0',
  `bs_stock_quantity_gd` double NOT NULL DEFAULT '0',
  `bs_prate` double NOT NULL DEFAULT '0',
  `bs_avg_prate` double NOT NULL DEFAULT '0',
  `bs_srate` double NOT NULL DEFAULT '0',
  `bs_batch_id` bigint(20) NOT NULL DEFAULT '0',
  `bs_expiry` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `bs_stk_status` tinyint(4) NOT NULL DEFAULT '1',
  `bs_branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_branch_stock_logs`
--

CREATE TABLE `erp_branch_stock_logs` (
  `bsl_log_id` bigint(20) UNSIGNED NOT NULL,
  `bsl_branch_stock_id` bigint(20) NOT NULL,
  `bsl_stock_id` bigint(20) DEFAULT NULL,
  `bsl_prd_id` bigint(20) DEFAULT NULL,
  `bsl_shop_quantity` double NOT NULL DEFAULT '0',
  `bsl_gd_quantity` double NOT NULL DEFAULT '0',
  `bsl_prate` double NOT NULL DEFAULT '0',
  `bsl_srate` double NOT NULL DEFAULT '0',
  `bsl_revert` tinyint(4) NOT NULL DEFAULT '0',
  `bsl_avg_prate` double NOT NULL DEFAULT '0',
  `bsl_branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_categories`
--

CREATE TABLE `erp_categories` (
  `cat_id` bigint(20) UNSIGNED NOT NULL,
  `cat_name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `cat_remarks` mediumtext COLLATE utf8_unicode_ci,
  `cat_flag` tinyint(4) NOT NULL DEFAULT '1',
  `cat_pos` tinyint(4) DEFAULT '1',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_company_stocks`
--

CREATE TABLE `erp_company_stocks` (
  `cmp_stock_id` bigint(20) UNSIGNED NOT NULL,
  `cmp_prd_id` bigint(20) NOT NULL,
  `cmp_stock_quantity` double NOT NULL DEFAULT '0',
  `cmp_avg_prate` double NOT NULL DEFAULT '0',
  `cmp_srate` double NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_damages`
--

CREATE TABLE `erp_damages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `damage_stock_id` int(11) NOT NULL DEFAULT '0',
  `damage_prod_id` int(11) NOT NULL DEFAULT '0',
  `damage_qty` double DEFAULT '0',
  `damage_rem_qty` double DEFAULT '0',
  `damage_purch_rate` double DEFAULT '0',
  `damage_notes` varchar(100) COLLATE utf8_unicode_ci DEFAULT 'NILL',
  `damage_serail` varchar(100) COLLATE utf8_unicode_ci DEFAULT 'NILL',
  `damage_date` date DEFAULT '2000-01-01',
  `damage_added_by` int(11) DEFAULT '0',
  `damage_flags` tinyint(4) NOT NULL DEFAULT '1',
  `damage_unit_id` int(11) DEFAULT '1',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_features`
--

CREATE TABLE `erp_features` (
  `feat_id` bigint(20) UNSIGNED NOT NULL,
  `feat_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `feat_description` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` int(11) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_feature_products`
--

CREATE TABLE `erp_feature_products` (
  `fetprod_id` bigint(20) UNSIGNED NOT NULL,
  `fetprod_prod_id` int(11) NOT NULL DEFAULT '0',
  `fetprod_fet_id` int(11) DEFAULT '0',
  `fetprod_fet_val` varchar(200) COLLATE utf8_unicode_ci DEFAULT 'Nill',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_godown_master`
--

CREATE TABLE `erp_godown_master` (
  `gd_id` bigint(20) UNSIGNED NOT NULL,
  `gd_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gd_code` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gd_cntct_person` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gd_cntct_num` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gd_address` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gd_desc` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_godown_stocks`
--

CREATE TABLE `erp_godown_stocks` (
  `gs_id` bigint(20) UNSIGNED NOT NULL,
  `gs_godown_id` bigint(20) NOT NULL,
  `gs_branch_stock_id` bigint(20) NOT NULL,
  `gs_stock_id` bigint(20) NOT NULL,
  `gs_prd_id` bigint(20) NOT NULL,
  `gs_qty` int(11) NOT NULL DEFAULT '0',
  `gs_date` date NOT NULL DEFAULT '2019-10-01',
  `gs_flag` tinyint(4) NOT NULL DEFAULT '1',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_godown_stock_log`
--

CREATE TABLE `erp_godown_stock_log` (
  `gsl_id` bigint(20) UNSIGNED NOT NULL,
  `gsl_gdwn_stock_id` bigint(20) NOT NULL,
  `gsl_branch_stock_id` bigint(20) NOT NULL,
  `gsl_stock_id` bigint(20) NOT NULL,
  `gsl_prd_id` bigint(20) NOT NULL,
  `gsl_prod_unit` int(11) NOT NULL DEFAULT '0',
  `gsl_qty` int(11) NOT NULL DEFAULT '0',
  `gsl_from` int(11) NOT NULL DEFAULT '0',
  `gsl_to` int(11) NOT NULL DEFAULT '0',
  `gsl_vchr_type` int(11) NOT NULL DEFAULT '0',
  `gsl_date` date NOT NULL DEFAULT '2019-10-01',
  `gsl_added_by` int(11) NOT NULL DEFAULT '0',
  `gsl_flag` tinyint(4) NOT NULL DEFAULT '1',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_manufacturer`
--

CREATE TABLE `erp_manufacturer` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `manftr_comp_name` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `manftr_comp_code` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `manftr_comp_address` mediumtext COLLATE utf8_unicode_ci,
  `manftr_comp_phone` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manftr_comp_mobile` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manftr_comp_fax` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manftr_comp_email` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manftr_comp_contact_person` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manftr_comp_website` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manftr_comp_addedby` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manftr_comp_add_date` date NOT NULL DEFAULT '2019-10-01',
  `manftr_comp_notes` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manftr_comp_flags` tinyint(4) DEFAULT '1',
  `manftr_created_at` timestamp NULL DEFAULT NULL,
  `manftr_updated_at` timestamp NULL DEFAULT NULL,
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_migrations`
--

CREATE TABLE `erp_migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `erp_migrations`
--

INSERT INTO `erp_migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2019_06_11_062713_create_sessions_table', 1),
(9, '2019_06_12_055131_create_categories_table', 1),
(10, '2019_06_12_055325_create_units_table', 1),
(11, '2019_06_12_055625_create_subcategories_table', 1),
(12, '2019_06_12_093427_create_products_table', 1),
(13, '2019_06_12_102148_create_usertypes_table', 1),
(14, '2019_06_17_113814_create_features_table', 1),
(15, '2019_06_20_075225_create_acountgroups_table', 1),
(16, '2019_06_20_115143_create_ledger_table', 1),
(17, '2019_06_21_085945_create_branches_table', 1),
(18, '2019_06_21_100926_create_customer_table', 1),
(19, '2019_06_22_070511_create_supplier_table', 1),
(20, '2019_07_10_083429_manufacturer', 1),
(21, '2019_07_10_085528_damagedproducts', 1),
(22, '2019_07_16_110942_prod_units', 1),
(23, '2019_07_19_062705_feature_product', 1),
(24, '2019_08_14_115417_acc_opening_balance', 1),
(25, '2019_08_22_074218_create_company_stocks_table', 1),
(26, '2019_08_22_074232_create_branch_stocks_table', 1),
(27, '2019_08_22_074304_create_stock_unit_rates_table', 1),
(28, '2019_08_23_043349_stock_daily_updates', 1),
(29, '2019_08_23_052705_stock_batches', 1),
(30, '2019_08_23_092352_create_godown_masters_table', 1),
(31, '2019_08_24_065252_create_godown_stocks_table', 1),
(32, '2019_08_27_051528_godownstok_log', 1),
(33, '2019_08_28_085748_create_branchstocklogs_table', 1),
(34, '2019_08_30_053121_voucher_type', 1),
(35, '2019_09_16_071907_opening_stock_log', 1),
(36, '2019_09_25_063500_create_suppliers_table', 1),
(37, '2019_09_25_102258_create_purchases_table', 1),
(38, '2019_09_25_102335_create_purchase_subs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `erp_oauth_access_tokens`
--

CREATE TABLE `erp_oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `erp_oauth_access_tokens`
--

INSERT INTO `erp_oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('798d714a17c6abe94c7d35fdd26bf65cd5dc9c42f9fb973827c7390435d52a6f98a0c6b467a3219c', 1, 1, 'MyApp', '[]', 0, '2019-10-01 05:32:00', '2019-10-01 05:32:00', '2020-10-01 11:02:00'),
('67b19326001f335687008e7ad9a8318cf74369bb0b66dbb230ce28c51568a602c93a92b99ba18b8c', 1, 1, 'MyApp', '[]', 0, '2019-10-01 11:13:20', '2019-10-01 11:13:20', '2020-10-01 11:13:20'),
('da721570e14ea1da042233579599e66de807df4f49f5790ade4b46ff4ca1746dec135aa1c6f8925e', 1, 1, 'MyApp', '[]', 0, '2019-10-03 04:41:16', '2019-10-03 04:41:16', '2020-10-03 04:41:16'),
('bfc1310dfb2172edce55e0d257d22ab036187b9292c2cade5646ddb7a09ca4d77357084af3c0be09', 1, 1, 'MyApp', '[]', 0, '2019-10-03 04:42:41', '2019-10-03 04:42:41', '2020-10-03 04:42:41'),
('a381389d6d4c5230833335f6a01d7db56b9f540fc2493ea8c6149dd162364033cea36d56fe7fbad3', 1, 1, 'MyApp', '[]', 0, '2019-10-03 04:43:10', '2019-10-03 04:43:10', '2020-10-03 04:43:10'),
('0e820d9204a22f1ddf59646840e3f8c8e65b5420074cb485b1d66ffc365cf8fc135a35943bbf4dc3', 1, 1, 'MyApp', '[]', 0, '2019-10-03 09:41:25', '2019-10-03 09:41:25', '2020-10-03 09:41:25'),
('c0290915e23e526a151626c45d4ae2a93c4c669240231cc0e5dabe81ae3a7a3d795826ce174b803e', 1, 1, 'MyApp', '[]', 0, '2019-10-03 12:39:40', '2019-10-03 12:39:40', '2020-10-03 12:39:40'),
('87298e9621744f0971dc7377da6c85c76aca6489ab8b806f068bb6d3a8474c778556251306121580', 1, 1, 'MyApp', '[]', 0, '2019-10-03 16:04:49', '2019-10-03 16:04:49', '2020-10-03 16:04:49'),
('6b5bf17a8c7a909ea7b3dd3886041075631fd48776b795c15b8020c23b1ebf5c7ddbcc2cec6bb5ce', 1, 1, 'MyApp', '[]', 0, '2019-10-04 04:11:25', '2019-10-04 04:11:25', '2020-10-04 04:11:25'),
('8e6a39f73baa58e7b3f1cbe8f2004a57c586df64ff8b77174890f970bce9bb42a03bfa45d64f80f7', 1, 1, 'MyApp', '[]', 0, '2019-10-04 04:15:59', '2019-10-04 04:15:59', '2020-10-04 04:15:59'),
('e6a089646a34f4d3161f1151bf03e4f959cbe6e7c53f392c83a5d65c2c90a4ffcaa7655d124ea650', 1, 1, 'MyApp', '[]', 0, '2019-10-04 04:16:57', '2019-10-04 04:16:57', '2020-10-04 04:16:57'),
('7d3244c227e3a7962963081fd423c2cd39b2daecc79e62d93dc33a7366571a77c1e26221c0215141', 1, 1, 'MyApp', '[]', 0, '2019-10-04 12:24:53', '2019-10-04 12:24:53', '2020-10-04 12:24:53'),
('ef62497dd2b09c34faba7a3680810b61ebe1fd5011eee43dc9d5bd7959ac9233c1628492d927c9a1', 1, 1, 'MyApp', '[]', 0, '2019-10-04 12:25:13', '2019-10-04 12:25:13', '2020-10-04 12:25:13'),
('8eaf595d078c925fed1ffbb1c055a433dfd9e9406a4c30573c97466e7e8c08565fdb11f5e7b339bb', 1, 1, 'MyApp', '[]', 0, '2019-10-04 12:25:57', '2019-10-04 12:25:57', '2020-10-04 12:25:57'),
('052606a55ef388e7c52e6bd40984ad03cfa0cb15504630c043158392c2ed9b7a6c5f265d4ae70ae9', 1, 1, 'MyApp', '[]', 0, '2019-10-04 13:52:57', '2019-10-04 13:52:57', '2020-10-04 13:52:57'),
('e163bf4c3bc57a91db80d4737a57a6a061b8bd511446210710da5119696b5f4cb9230d5864539ea6', 1, 1, 'MyApp', '[]', 0, '2019-10-04 16:23:28', '2019-10-04 16:23:28', '2020-10-04 16:23:28'),
('4ffaf596851e5bc769b2486350d8ef31d4ec4c7d42556f80de1d65e867316c8a116f07d70f60c95a', 1, 1, 'MyApp', '[]', 0, '2019-10-04 16:30:52', '2019-10-04 16:30:52', '2020-10-04 16:30:52'),
('91fdb4a04f19cab0a0ebe5309cc1e868b2bad59aa27fb2dc744fcec1f862e5690d05e29e763ae70c', 1, 1, 'MyApp', '[]', 0, '2019-10-05 05:03:48', '2019-10-05 05:03:48', '2020-10-05 05:03:48'),
('3066bb5134d724ce4ceb3aca024e2b2c0b11d1b6840546c83cf02ab2c96a2c45dd299b124eb24aea', 1, 1, 'MyApp', '[]', 0, '2019-10-05 05:10:24', '2019-10-05 05:10:24', '2020-10-05 05:10:24'),
('7fea74974f0c891a491e6b64c111b25c5d88ab0aee60b532efcd82edaaeef10aa526dfadb3a598b2', 1, 1, 'MyApp', '[]', 0, '2019-10-05 05:19:28', '2019-10-05 05:19:28', '2020-10-05 05:19:28'),
('ba72131d7b57725c87ddf379c1c4819164cb9210a4f67ead4288512b59ba487ea9b6a95708b550de', 1, 1, 'MyApp', '[]', 0, '2019-10-05 06:37:14', '2019-10-05 06:37:14', '2020-10-05 06:37:14'),
('8fc18d2594f6c47c0c6e1d83ed83544e46ecece5f7b8950b870a53085dee3a8ef499a67e9650ce04', 1, 1, 'MyApp', '[]', 0, '2019-10-05 06:46:11', '2019-10-05 06:46:11', '2020-10-05 06:46:11'),
('e919fca169531494335739b4f1462c8229189be142c2cfd95fb27c5c0ac0788cb4ac27f283ed48a0', 1, 1, 'MyApp', '[]', 0, '2019-10-05 06:55:03', '2019-10-05 06:55:03', '2020-10-05 06:55:03'),
('58b2ff86f153b4fecf5bfbed1e322aa046da6dacbfc1475df8c41ca186d63ab2447e356137c4b468', 1, 1, 'MyApp', '[]', 0, '2019-10-05 07:53:19', '2019-10-05 07:53:19', '2020-10-05 07:53:19'),
('6e75f703d7cd98a03835e304550215301e15e4a57f7ee62f91feb8143d6c39804a65770340188fcb', 1, 1, 'MyApp', '[]', 0, '2019-10-05 12:12:38', '2019-10-05 12:12:38', '2020-10-05 12:12:38'),
('875d618611642d1e578296bd1146bda45a796c78bd0602eb9acfa364e01709f26f6695c9cf56cb02', 1, 1, 'MyApp', '[]', 0, '2019-10-05 12:36:13', '2019-10-05 12:36:13', '2020-10-05 12:36:13'),
('d1c62bcd60f8d0b317b24ea07a1824c819200d8efd917b0cb9ba918ad49c571220015fed29827272', 1, 1, 'MyApp', '[]', 0, '2019-10-07 13:27:39', '2019-10-07 13:27:39', '2020-10-07 13:27:39'),
('db27a178bfe5818eba71f7e7a7f1636afed5e670651069241808e9a4cfc365d2ece4f485978ca139', 1, 1, 'MyApp', '[]', 0, '2019-10-07 13:27:40', '2019-10-07 13:27:40', '2020-10-07 13:27:40'),
('d847c17025ddffe93b6aeb7c7d5ae677cdcc1434fbcc5c960bdeed9b2fe74cbd655d175e8459bc7b', 1, 1, 'MyApp', '[]', 0, '2019-10-07 13:39:52', '2019-10-07 13:39:52', '2020-10-07 13:39:52'),
('1348c168a2fa5be7d57e530d355a826d139dacb815ec6aeaac34043bf32e76dc231816a7d92f0037', 1, 1, 'MyApp', '[]', 0, '2019-10-07 14:21:24', '2019-10-07 14:21:24', '2020-10-07 14:21:24'),
('c61577a6dd6e79a886c417d3c7ccf3c8f6e963e905db7f74325d301d2d16962eec87ddf71cd28408', 2, 1, 'MyApp', '[]', 0, '2019-10-07 14:32:05', '2019-10-07 14:32:05', '2020-10-07 14:32:05'),
('2980db056610566a2c7120d52963727bf1a9fbb9f6947a7df8dcc05e0a7869ed6ab1fc20883a37a3', 3, 1, 'MyApp', '[]', 0, '2019-10-07 14:32:35', '2019-10-07 14:32:35', '2020-10-07 14:32:35'),
('f5dda8521e7b560db25996622f4ba6899df1f56d3a660472874d126dbfe83bc62b6c3b546e92414a', 1, 1, 'MyApp', '[]', 0, '2019-10-07 14:40:51', '2019-10-07 14:40:51', '2020-10-07 14:40:51'),
('dc402dd94a84c789d5bd1943d10f8e6b2d440b102d842ef0e0cd8b773dc872a217daa198007a2e5a', 1, 1, 'MyApp', '[]', 0, '2019-10-07 14:43:38', '2019-10-07 14:43:38', '2020-10-07 14:43:38'),
('ca109ba303614f34e3fa43fbe877d7ec9bc1b4a71236df077929fc1e5273fb5de6bfefec582d131a', 1, 1, 'MyApp', '[]', 0, '2019-10-08 04:11:14', '2019-10-08 04:11:14', '2020-10-08 04:11:14'),
('73ed581dbad8061208ff8562c9afcb28087913befd6e5804190987e06fc2e15de3e2a7f38bc978d0', 1, 1, 'MyApp', '[]', 0, '2019-10-08 05:46:05', '2019-10-08 05:46:05', '2020-10-08 05:46:05'),
('08fce0fcae7df6731edce43bc6d35483985513ff7f11720f2c254210fb9088caf2d874218a0fa281', 1, 1, 'MyApp', '[]', 0, '2019-10-08 05:53:21', '2019-10-08 05:53:21', '2020-10-08 05:53:21'),
('f85037e4f91c2cd727cd995641b3d2f3c1989786bc2336ffea72c4155d51e0c6cd48726e079fb022', 1, 1, 'MyApp', '[]', 0, '2019-10-08 06:32:18', '2019-10-08 06:32:18', '2020-10-08 06:32:18'),
('d87b1a93d8a43f8435203322841b996c22375392e7172c31d388ae0c28ea9bee9e99879864893ff8', 1, 1, 'MyApp', '[]', 0, '2019-10-08 06:32:19', '2019-10-08 06:32:19', '2020-10-08 06:32:19'),
('da20f5ff1a14c31b93cd6ecde8392597d333e52110599ccd805852b62921603acb90e2a5c89fa8a6', 1, 1, 'MyApp', '[]', 0, '2019-10-08 07:37:58', '2019-10-08 07:37:58', '2020-10-08 07:37:58'),
('7d61e2efaff6468757e5d8c92ac1fda1e8e2591dfdf2950ce757cd8e8d74556b5f3b3adcd95c2c17', 1, 1, 'MyApp', '[]', 0, '2019-10-08 07:49:09', '2019-10-08 07:49:09', '2020-10-08 07:49:09'),
('669d4510c23a1fee68b75dfa6c09f7848ec44c5c02d8296898c6064000b13107a66ddbd8e2a6f536', 1, 1, 'MyApp', '[]', 0, '2019-10-08 08:03:44', '2019-10-08 08:03:44', '2020-10-08 08:03:44'),
('c68c9495fc5fb92c7938e90f8f4ae614fc9beaa84766a8c80a27676601c2aa261576d98fc1f12ef0', 1, 1, 'MyApp', '[]', 0, '2019-10-08 08:43:22', '2019-10-08 08:43:22', '2020-10-08 08:43:22'),
('bc67c3b01a10279076b1674f39d72c5ff2e0692e6d8f97f7b04c0314ee9c0298044003cb305a54b9', 1, 1, 'MyApp', '[]', 0, '2019-10-08 09:12:29', '2019-10-08 09:12:29', '2020-10-08 09:12:29'),
('54f5d7df2890c7f34f4e2c6f8d466d502ab5e4f57d923d25129bdbf41a02d7405d50bcdaa13a37a1', 1, 1, 'MyApp', '[]', 0, '2019-10-08 09:34:31', '2019-10-08 09:34:31', '2020-10-08 09:34:31'),
('efeb94184e51ab9d791a5e83571ab46fdbe5fab18449fb279ea9c64193803fc5b034c6d175f6a672', 1, 1, 'MyApp', '[]', 0, '2019-10-08 11:13:27', '2019-10-08 11:13:27', '2020-10-08 11:13:27'),
('65cc4005fdea2ac3aaff05d09cda06ae57a0b46a349db604251403a051cfde483b56a245fb5fde6b', 1, 1, 'MyApp', '[]', 0, '2019-10-08 11:40:28', '2019-10-08 11:40:28', '2020-10-08 11:40:28'),
('6895b929a0e3b9d0d045d9d9fa7209d08de3c187fb66440c622f67482171d1dd6cf20e4bc6d1015a', 1, 1, 'MyApp', '[]', 0, '2019-10-08 12:51:16', '2019-10-08 12:51:16', '2020-10-08 12:51:16'),
('5c5afbdde76afd19e6a8780d092a4cae72e7856f472d166b1e20d12b8d6694b6e680f82a952d12d4', 1, 1, 'MyApp', '[]', 0, '2019-10-08 12:51:18', '2019-10-08 12:51:18', '2020-10-08 12:51:18'),
('f45f329526bf637f6dc967eaf1ca8412b078205acf98da227d643c1b7bfe79f70bcf4a37d1e00e6e', 1, 1, 'MyApp', '[]', 0, '2019-10-08 15:31:02', '2019-10-08 15:31:02', '2020-10-08 15:31:02'),
('02d293b24af9749d1c73f6b177691a68b58e0a8a4346ff2202c857b6ee8a95e592199b01a21c149e', 1, 1, 'MyApp', '[]', 0, '2019-10-08 16:28:06', '2019-10-08 16:28:06', '2020-10-08 16:28:06'),
('34e2cb06e10d2608fe301d82e9eb3d9d50d06ea298a0b6c78597be6d09ec6a8683dc63fe3903d27c', 1, 1, 'MyApp', '[]', 0, '2019-10-09 04:05:57', '2019-10-09 04:05:57', '2020-10-09 04:05:57'),
('81238083318b017049749b34bead64378994db5845f4603beb4ceac3aa2e6021c0e3b8633ce1f735', 1, 1, 'MyApp', '[]', 0, '2019-10-09 04:23:32', '2019-10-09 04:23:32', '2020-10-09 04:23:32'),
('b08c864e27235d9fd7de0f589221f6b8c5ccde2f4a5e31e442c37a4a74e2ebe92783e4ff52edad1e', 1, 1, 'MyApp', '[]', 0, '2019-10-09 05:14:26', '2019-10-09 05:14:26', '2020-10-09 05:14:26');

-- --------------------------------------------------------

--
-- Table structure for table `erp_oauth_auth_codes`
--

CREATE TABLE `erp_oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_oauth_clients`
--

CREATE TABLE `erp_oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `erp_oauth_clients`
--

INSERT INTO `erp_oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'QUX4TMAivx7yKmMILV7rIq6ZbFF4YM4ttAfq3GOw', 'http://localhost', 1, 0, 0, '2019-10-01 05:26:19', '2019-10-01 05:26:19'),
(2, NULL, 'Laravel Password Grant Client', 'ps3e62gqQCBRsIu9CWz2nybjQK7NjyIP68lAV8sA', 'http://localhost', 0, 1, 0, '2019-10-01 05:26:19', '2019-10-01 05:26:19');

-- --------------------------------------------------------

--
-- Table structure for table `erp_oauth_personal_access_clients`
--

CREATE TABLE `erp_oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `erp_oauth_personal_access_clients`
--

INSERT INTO `erp_oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-10-01 05:26:19', '2019-10-01 05:26:19');

-- --------------------------------------------------------

--
-- Table structure for table `erp_oauth_refresh_tokens`
--

CREATE TABLE `erp_oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_opening_stock_log`
--

CREATE TABLE `erp_opening_stock_log` (
  `opstklog_id` bigint(20) UNSIGNED NOT NULL,
  `opstklog_branch_stock_id` bigint(20) NOT NULL,
  `opstklog_prd_id` bigint(20) NOT NULL,
  `opstklog_stock_id` bigint(20) NOT NULL,
  `opstklog_gd_id` int(11) NOT NULL DEFAULT '0',
  `opstklog_batch_id` int(11) NOT NULL DEFAULT '0',
  `opstklog_date` date NOT NULL DEFAULT '2019-10-01',
  `opstklog_prate` double NOT NULL DEFAULT '0',
  `opstklog_srate` double NOT NULL DEFAULT '0',
  `opstklog_stock_quantity_add` double NOT NULL DEFAULT '0',
  `opstklog_stock_quantity_rem` double NOT NULL DEFAULT '0',
  `opstklog_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Add 0 | Remove 1',
  `opstklog_status` tinyint(4) NOT NULL DEFAULT '1',
  `opstklog_unit_id` bigint(20) NOT NULL DEFAULT '0',
  `opstklog_added_by` bigint(20) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_password_resets`
--

CREATE TABLE `erp_password_resets` (
  `email` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_products`
--

CREATE TABLE `erp_products` (
  `prd_id` bigint(20) UNSIGNED NOT NULL,
  `prd_name` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `prd_cat_id` bigint(20) NOT NULL,
  `prd_sub_cat_id` bigint(20) DEFAULT '0',
  `prd_supplier` bigint(20) DEFAULT NULL,
  `prd_alias` mediumtext COLLATE utf8_unicode_ci,
  `prd_short_name` mediumtext COLLATE utf8_unicode_ci,
  `prd_min_stock` int(11) DEFAULT NULL,
  `prd_max_stock` int(11) DEFAULT NULL,
  `prd_barcode` text COLLATE utf8_unicode_ci,
  `prd_ean` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prd_added_by` int(11) DEFAULT NULL,
  `prd_base_unit_id` int(11) NOT NULL,
  `prd_default_unit_id` int(11) DEFAULT NULL,
  `prd_tax` int(11) DEFAULT NULL,
  `prd_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prd_tax_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prd_desc` text COLLATE utf8_unicode_ci,
  `prd_exp_dur` int(11) DEFAULT NULL,
  `prd_loyalty_rate` double(15,8) DEFAULT NULL,
  `prd_type` tinyint(4) NOT NULL DEFAULT '1',
  `prd_stock_status` tinyint(4) NOT NULL DEFAULT '1',
  `prd_stock_stat` tinyint(4) NOT NULL DEFAULT '1',
  `prd_minstock_alert` tinyint(4) DEFAULT NULL,
  `prd_maxstock_alert` tinyint(4) DEFAULT NULL,
  `prd_remarks` mediumtext COLLATE utf8_unicode_ci,
  `prd_flag` tinyint(4) NOT NULL DEFAULT '1',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_prod_units`
--

CREATE TABLE `erp_prod_units` (
  `produnit_id` bigint(20) UNSIGNED NOT NULL,
  `produnit_prod_id` int(11) NOT NULL DEFAULT '0',
  `produnit_unit_id` int(11) DEFAULT '0',
  `produnit_ean_barcode` varchar(255) COLLATE utf8_unicode_ci DEFAULT '0',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_purchases`
--

CREATE TABLE `erp_purchases` (
  `purch_id` bigint(20) UNSIGNED NOT NULL,
  `purch_id2` bigint(20) NOT NULL,
  `purch_branch_id` bigint(20) NOT NULL,
  `purch_no` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `purch_inv_no` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `purch_supp_id` bigint(20) NOT NULL,
  `purch_supp_date` date DEFAULT NULL,
  `purch_tax_ledger_id` bigint(20) NOT NULL,
  `purch_ord_no` bigint(20) NOT NULL,
  `purch_type` tinyint(4) NOT NULL,
  `purch_pay_type` tinyint(4) NOT NULL,
  `purch_date` date NOT NULL,
  `purch_inv_date` date NOT NULL,
  `purch_amount` double NOT NULL,
  `purch_tax` double NOT NULL,
  `purch_tax2` double NOT NULL,
  `purch_tax3` double NOT NULL,
  `purch_discount` double NOT NULL,
  `purch_frieght` double NOT NULL,
  `purch_frieght_ledger_id` int(11) NOT NULL DEFAULT '0',
  `purch_agent` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `purch_flag` tinyint(4) NOT NULL DEFAULT '1',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_purchase_subs`
--

CREATE TABLE `erp_purchase_subs` (
  `purchsub_id` bigint(20) UNSIGNED NOT NULL,
  `purchsub_purch_id` bigint(20) NOT NULL,
  `purchsub_prd_id` bigint(20) NOT NULL,
  `purchsub_stock_id` bigint(20) NOT NULL,
  `purchsub_branch_id` tinyint(4) NOT NULL,
  `purchsub_batch_id` bigint(20) NOT NULL DEFAULT '0',
  `purchsub_expiry` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `purchsub_qty` bigint(20) NOT NULL,
  `purchsub_rate` double NOT NULL,
  `purchsub_frieght` double NOT NULL,
  `purchsub_tax` double NOT NULL,
  `purchsub_tax_per` double NOT NULL,
  `purchsub_tax2` double NOT NULL,
  `purchsub_tax2_per` double NOT NULL,
  `purchsub_tax3` double NOT NULL,
  `purchsub_tax3_per` double NOT NULL,
  `purchsub_unit_id` bigint(20) NOT NULL,
  `purchsub_gd_qty` bigint(20) NOT NULL,
  `purchsub_gd_id` int(11) NOT NULL,
  `purchsub_date` date NOT NULL,
  `purchsub_flag` tinyint(4) NOT NULL DEFAULT '1',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_sessions`
--

CREATE TABLE `erp_sessions` (
  `id` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `ip_address` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'NILL',
  `user_agent` text COLLATE utf8_unicode_ci NOT NULL,
  `payload` text COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_stock_batches`
--

CREATE TABLE `erp_stock_batches` (
  `sb_id` bigint(20) UNSIGNED NOT NULL,
  `sb_batch_code` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `sb_branch_stock_id` bigint(20) NOT NULL,
  `sb_prd_id` bigint(20) NOT NULL,
  `sb_stock_id` bigint(20) NOT NULL,
  `sb_quantity` bigint(20) NOT NULL,
  `sb_manufacture_date` date NOT NULL,
  `sb_expiry_date` date NOT NULL,
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_stock_daily_updates`
--

CREATE TABLE `erp_stock_daily_updates` (
  `sdu_id` bigint(20) UNSIGNED NOT NULL,
  `sdu_branch_stock_id` bigint(20) NOT NULL,
  `sdu_prd_id` bigint(20) NOT NULL,
  `sdu_stock_id` bigint(20) NOT NULL,
  `sdu_gd_id` int(11) NOT NULL DEFAULT '0',
  `sdu_batch_id` int(11) NOT NULL DEFAULT '0',
  `sdu_date` date NOT NULL DEFAULT '2019-10-01',
  `sdu_stock_quantity` double NOT NULL,
  `sdu_godown_id` double NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_stock_unit_rates`
--

CREATE TABLE `erp_stock_unit_rates` (
  `sur_id` bigint(20) UNSIGNED NOT NULL,
  `branch_stock_id` bigint(20) NOT NULL,
  `sur_prd_id` bigint(20) NOT NULL,
  `sur_stock_id` bigint(20) NOT NULL,
  `sur_unit_id` bigint(20) NOT NULL,
  `sur_unit_rate` double NOT NULL,
  `sur_branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_subcategories`
--

CREATE TABLE `erp_subcategories` (
  `subcat_id` bigint(20) UNSIGNED NOT NULL,
  `subcat_parent_category` int(11) NOT NULL,
  `subcat_name` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subcat_remarks` mediumtext COLLATE utf8_unicode_ci,
  `subcat_flag` tinyint(4) NOT NULL DEFAULT '1',
  `subcat_pos` tinyint(4) NOT NULL DEFAULT '1',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_suppliers`
--

CREATE TABLE `erp_suppliers` (
  `supp_id` bigint(20) UNSIGNED NOT NULL,
  `supp_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `supp_alias` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supp_code` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `supp_ledger_id` int(11) NOT NULL,
  `supp_branch_id` smallint(6) NOT NULL DEFAULT '0',
  `supp_address1` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supp_address2` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supp_zip` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supp_city` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supp_state` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supp_country` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supp_state_code` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supp_phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supp_mob` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supp_email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supp_fax` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supp_notes` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supp_flag` tinyint(4) NOT NULL DEFAULT '1',
  `supp_tin` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supp_contact` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supp_due` int(11) DEFAULT NULL,
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_units`
--

CREATE TABLE `erp_units` (
  `unit_id` bigint(20) UNSIGNED NOT NULL,
  `unit_name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `unit_code` char(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unit_display` char(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unit_type` int(11) NOT NULL DEFAULT '1' COMMENT '0 - derived,1 - base ',
  `unit_base_id` int(11) NOT NULL DEFAULT '0' COMMENT 'unit_id of base unit',
  `unit_base_qty` int(11) DEFAULT '1' COMMENT 'base qty/unit',
  `unit_remarks` mediumtext COLLATE utf8_unicode_ci,
  `unit_flag` tinyint(4) NOT NULL DEFAULT '1',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `erp_users`
--

CREATE TABLE `erp_users` (
  `usr_id` bigint(20) UNSIGNED NOT NULL,
  `usr_type` int(11) NOT NULL,
  `usr_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `usr_username` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `usr_password` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `usr_nickname` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `usr_phone` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `usr_mobile` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `usr_email` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `usr_address` mediumtext COLLATE utf8_unicode_ci,
  `usr_active` tinyint(4) NOT NULL DEFAULT '1',
  `email_verified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `erp_users`
--

INSERT INTO `erp_users` (`usr_id`, `usr_type`, `usr_name`, `usr_username`, `usr_password`, `usr_nickname`, `usr_phone`, `usr_mobile`, `usr_email`, `usr_address`, `usr_active`, `email_verified_at`, `remember_token`, `company_id`, `branch_id`, `server_sync_flag`, `server_sync_time`, `local_sync_time`, `created_at`, `updated_at`) VALUES
(1, 1, 'Fujishka', 'Fujishka', '$2y$10$A2wgmHzb4CYGMGvsibJOJOA8UxOJ2gj1x8ocBy3lxP8yiasJDvX5S', NULL, NULL, NULL, 'admin@fujishka.com', NULL, 1, '2019-09-18 10:49:55', NULL, 0, 0, 0, NULL, NULL, '2019-09-18 05:19:55', '2019-09-18 05:19:55');

-- --------------------------------------------------------

--
-- Table structure for table `erp_user_types`
--

CREATE TABLE `erp_user_types` (
  `type_id` bigint(20) UNSIGNED NOT NULL,
  `user_type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `erp_user_types`
--

INSERT INTO `erp_user_types` (`type_id`, `user_type`, `created_at`, `updated_at`) VALUES
(1, 'ADMIN', NULL, NULL),
(2, 'COMPANY', NULL, NULL),
(3, 'BRANCH', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `erp_voucher_type`
--

CREATE TABLE `erp_voucher_type` (
  `vchtype_id` bigint(20) UNSIGNED NOT NULL,
  `vchtype_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `erp_voucher_type`
--

INSERT INTO `erp_voucher_type` (`vchtype_id`, `vchtype_name`, `created_at`, `updated_at`) VALUES
(1, 'Payment', NULL, NULL),
(2, 'Reciept', NULL, NULL),
(3, 'Contra', NULL, NULL),
(4, 'Journal', NULL, NULL),
(5, 'Sales', NULL, NULL),
(6, 'Purchase', NULL, NULL),
(7, 'Debit note', NULL, NULL),
(8, 'Credit note', NULL, NULL),
(9, 'Purch Return', NULL, NULL),
(10, 'Sales Return', NULL, NULL),
(11, 'Service Charge', NULL, NULL),
(12, 'Void Service', NULL, NULL),
(13, 'Production', NULL, NULL),
(14, 'Void Production', NULL, NULL),
(15, 'Godown Transfer', NULL, NULL),
(16, 'Stock Receipt Return', NULL, NULL),
(17, 'Stock Receipt', NULL, NULL),
(18, 'Stock Transfer Return', NULL, NULL),
(19, 'Stock Transfer', NULL, NULL),
(20, 'Dividend Allocation', NULL, NULL),
(21, 'Starting Balance', NULL, NULL),
(22, 'Opening Stock', NULL, NULL),
(23, 'Invoice Due Receipt', NULL, NULL),
(24, 'Input Tax', NULL, NULL),
(25, 'Output Tax', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `erp_acc_branches`
--
ALTER TABLE `erp_acc_branches`
  ADD PRIMARY KEY (`branch_id`);

--
-- Indexes for table `erp_acc_customers`
--
ALTER TABLE `erp_acc_customers`
  ADD PRIMARY KEY (`cust_id`);

--
-- Indexes for table `erp_acc_groups`
--
ALTER TABLE `erp_acc_groups`
  ADD PRIMARY KEY (`accgrp_id`);

--
-- Indexes for table `erp_acc_ledgers`
--
ALTER TABLE `erp_acc_ledgers`
  ADD PRIMARY KEY (`ledger_id`);

--
-- Indexes for table `erp_acc_opening_balances`
--
ALTER TABLE `erp_acc_opening_balances`
  ADD PRIMARY KEY (`opbal_id`);

--
-- Indexes for table `erp_acc_suppliers`
--
ALTER TABLE `erp_acc_suppliers`
  ADD PRIMARY KEY (`supp_id`);

--
-- Indexes for table `erp_branch_stocks`
--
ALTER TABLE `erp_branch_stocks`
  ADD PRIMARY KEY (`branch_stock_id`);

--
-- Indexes for table `erp_branch_stock_logs`
--
ALTER TABLE `erp_branch_stock_logs`
  ADD PRIMARY KEY (`bsl_log_id`);

--
-- Indexes for table `erp_categories`
--
ALTER TABLE `erp_categories`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `erp_company_stocks`
--
ALTER TABLE `erp_company_stocks`
  ADD PRIMARY KEY (`cmp_stock_id`);

--
-- Indexes for table `erp_damages`
--
ALTER TABLE `erp_damages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `erp_features`
--
ALTER TABLE `erp_features`
  ADD PRIMARY KEY (`feat_id`);

--
-- Indexes for table `erp_feature_products`
--
ALTER TABLE `erp_feature_products`
  ADD PRIMARY KEY (`fetprod_id`);

--
-- Indexes for table `erp_godown_master`
--
ALTER TABLE `erp_godown_master`
  ADD PRIMARY KEY (`gd_id`);

--
-- Indexes for table `erp_godown_stocks`
--
ALTER TABLE `erp_godown_stocks`
  ADD PRIMARY KEY (`gs_id`);

--
-- Indexes for table `erp_godown_stock_log`
--
ALTER TABLE `erp_godown_stock_log`
  ADD PRIMARY KEY (`gsl_id`);

--
-- Indexes for table `erp_manufacturer`
--
ALTER TABLE `erp_manufacturer`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `erp_manufacturer_manftr_comp_email_unique` (`manftr_comp_email`);

--
-- Indexes for table `erp_migrations`
--
ALTER TABLE `erp_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `erp_oauth_access_tokens`
--
ALTER TABLE `erp_oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `erp_oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `erp_oauth_auth_codes`
--
ALTER TABLE `erp_oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `erp_oauth_clients`
--
ALTER TABLE `erp_oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `erp_oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `erp_oauth_personal_access_clients`
--
ALTER TABLE `erp_oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `erp_oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `erp_oauth_refresh_tokens`
--
ALTER TABLE `erp_oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `erp_oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `erp_opening_stock_log`
--
ALTER TABLE `erp_opening_stock_log`
  ADD PRIMARY KEY (`opstklog_id`);

--
-- Indexes for table `erp_password_resets`
--
ALTER TABLE `erp_password_resets`
  ADD KEY `erp_password_resets_email_index` (`email`);

--
-- Indexes for table `erp_products`
--
ALTER TABLE `erp_products`
  ADD PRIMARY KEY (`prd_id`);

--
-- Indexes for table `erp_prod_units`
--
ALTER TABLE `erp_prod_units`
  ADD PRIMARY KEY (`produnit_id`);

--
-- Indexes for table `erp_purchases`
--
ALTER TABLE `erp_purchases`
  ADD PRIMARY KEY (`purch_id`);

--
-- Indexes for table `erp_purchase_subs`
--
ALTER TABLE `erp_purchase_subs`
  ADD PRIMARY KEY (`purchsub_id`);

--
-- Indexes for table `erp_sessions`
--
ALTER TABLE `erp_sessions`
  ADD UNIQUE KEY `erp_sessions_id_unique` (`id`);

--
-- Indexes for table `erp_stock_batches`
--
ALTER TABLE `erp_stock_batches`
  ADD PRIMARY KEY (`sb_id`);

--
-- Indexes for table `erp_stock_daily_updates`
--
ALTER TABLE `erp_stock_daily_updates`
  ADD PRIMARY KEY (`sdu_id`);

--
-- Indexes for table `erp_stock_unit_rates`
--
ALTER TABLE `erp_stock_unit_rates`
  ADD PRIMARY KEY (`sur_id`);

--
-- Indexes for table `erp_subcategories`
--
ALTER TABLE `erp_subcategories`
  ADD PRIMARY KEY (`subcat_id`);

--
-- Indexes for table `erp_suppliers`
--
ALTER TABLE `erp_suppliers`
  ADD PRIMARY KEY (`supp_id`);

--
-- Indexes for table `erp_units`
--
ALTER TABLE `erp_units`
  ADD PRIMARY KEY (`unit_id`);

--
-- Indexes for table `erp_users`
--
ALTER TABLE `erp_users`
  ADD PRIMARY KEY (`usr_id`),
  ADD UNIQUE KEY `erp_users_usr_username_unique` (`usr_username`),
  ADD UNIQUE KEY `erp_users_usr_email_unique` (`usr_email`);

--
-- Indexes for table `erp_user_types`
--
ALTER TABLE `erp_user_types`
  ADD PRIMARY KEY (`type_id`);

--
-- Indexes for table `erp_voucher_type`
--
ALTER TABLE `erp_voucher_type`
  ADD PRIMARY KEY (`vchtype_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `erp_acc_branches`
--
ALTER TABLE `erp_acc_branches`
  MODIFY `branch_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `erp_acc_customers`
--
ALTER TABLE `erp_acc_customers`
  MODIFY `cust_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_acc_groups`
--
ALTER TABLE `erp_acc_groups`
  MODIFY `accgrp_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1000;

--
-- AUTO_INCREMENT for table `erp_acc_ledgers`
--
ALTER TABLE `erp_acc_ledgers`
  MODIFY `ledger_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `erp_acc_opening_balances`
--
ALTER TABLE `erp_acc_opening_balances`
  MODIFY `opbal_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_acc_suppliers`
--
ALTER TABLE `erp_acc_suppliers`
  MODIFY `supp_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_branch_stocks`
--
ALTER TABLE `erp_branch_stocks`
  MODIFY `branch_stock_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_branch_stock_logs`
--
ALTER TABLE `erp_branch_stock_logs`
  MODIFY `bsl_log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_categories`
--
ALTER TABLE `erp_categories`
  MODIFY `cat_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_company_stocks`
--
ALTER TABLE `erp_company_stocks`
  MODIFY `cmp_stock_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_damages`
--
ALTER TABLE `erp_damages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_features`
--
ALTER TABLE `erp_features`
  MODIFY `feat_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_feature_products`
--
ALTER TABLE `erp_feature_products`
  MODIFY `fetprod_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_godown_master`
--
ALTER TABLE `erp_godown_master`
  MODIFY `gd_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_godown_stocks`
--
ALTER TABLE `erp_godown_stocks`
  MODIFY `gs_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_godown_stock_log`
--
ALTER TABLE `erp_godown_stock_log`
  MODIFY `gsl_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_manufacturer`
--
ALTER TABLE `erp_manufacturer`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_migrations`
--
ALTER TABLE `erp_migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `erp_oauth_clients`
--
ALTER TABLE `erp_oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `erp_oauth_personal_access_clients`
--
ALTER TABLE `erp_oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `erp_opening_stock_log`
--
ALTER TABLE `erp_opening_stock_log`
  MODIFY `opstklog_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_products`
--
ALTER TABLE `erp_products`
  MODIFY `prd_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_prod_units`
--
ALTER TABLE `erp_prod_units`
  MODIFY `produnit_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_purchases`
--
ALTER TABLE `erp_purchases`
  MODIFY `purch_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_purchase_subs`
--
ALTER TABLE `erp_purchase_subs`
  MODIFY `purchsub_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_stock_batches`
--
ALTER TABLE `erp_stock_batches`
  MODIFY `sb_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_stock_daily_updates`
--
ALTER TABLE `erp_stock_daily_updates`
  MODIFY `sdu_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_stock_unit_rates`
--
ALTER TABLE `erp_stock_unit_rates`
  MODIFY `sur_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_subcategories`
--
ALTER TABLE `erp_subcategories`
  MODIFY `subcat_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_suppliers`
--
ALTER TABLE `erp_suppliers`
  MODIFY `supp_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_units`
--
ALTER TABLE `erp_units`
  MODIFY `unit_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `erp_users`
--
ALTER TABLE `erp_users`
  MODIFY `usr_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `erp_user_types`
--
ALTER TABLE `erp_user_types`
  MODIFY `type_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `erp_voucher_type`
--
ALTER TABLE `erp_voucher_type`
  MODIFY `vchtype_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
COMMIT;

ALTER TABLE `erp_tmp_purchases` ADD `temp_purch_json` JSON NULL DEFAULT NULL AFTER `purch_flag`;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


ALTER TABLE `erp_production` ADD `prdn_inspection_staff` INT NOT NULL DEFAULT '0' AFTER `prdn_added_by`;
