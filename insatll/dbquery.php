
// Yasir poongadan 01-01-2020

ALTER TABLE `erp_acc_receipt` CHANGE `rec_no` `rec_no` BIGINT NOT NULL;

ALTER TABLE `erp_acc_receipt` ADD `van_rec_no` VARCHAR(45) NOT NULL AFTER `updated_at`, ADD `branch_rec_no` VARCHAR(45) NOT NULL AFTER `van_rec_no`, ADD `branch_rec_no2` BIGINT NOT NULL AFTER `branch_rec_no`;

ALTER TABLE `erp_acc_receipt_sub` CHANGE `recsub_rec_no` `recsub_rec_no` BIGINT NOT NULL;

ALTER TABLE `erp_acc_receipt_sub` ADD `van_rec_no` VARCHAR(45) NOT NULL AFTER `updated_at`, ADD `branch_rec_no` VARCHAR(45) NOT NULL AFTER `van_rec_no`;

ALTER TABLE `erp_sales_master` ADD `sales_due_date` DATE NOT NULL AFTER `sales_cust_ledger_id`;

ALTER TABLE `erp_sales_due_sub` ADD `rec_no` BIGINT NOT NULL AFTER `branch_inv_no2`, ADD `van_rec_no` VARCHAR(45) NOT NULL AFTER `rec_no`, ADD `branch_rec_no` VARCHAR(45) NOT NULL AFTER `van_rec_no`;

// 10-02-2020
ALTER TABLE `erp_sales_return_master` ADD `salesret_sales_branch_inv_no` VARCHAR(50) NOT NULL AFTER `salesret_sales_van_inv_no`, ADD `salesret_cust_ledger_id` INT NOT NULL AFTER `salesret_sales_branch_inv_no`;


// Rakesh 4/1/2019

ALTER TABLE `erp_acc_branches` ADD `img_url` VARCHAR(256) NULL DEFAULT NULL AFTER `branch_default`;

// Rakesh 7/1/2019
ALTER TABLE `erp_godown_stock_log` ADD `gsl_tran_inv_id` BIGINT NOT NULL AFTER `gsl_id`;
// Rakesh 8/1/2019
ALTER TABLE `erp_godown_transfer_master` ADD `gdtrn_notes` TEXT NULL DEFAULT NULL AFTER `gdtrn_datetime`;
ALTER TABLE `erp_van` ADD `all_cat` INT NOT NULL DEFAULT '0' AFTER `van_ledger_id`;
10/1/2020 Rakesh 
ALTER TABLE `erp_rate_codes` CHANGE `rcd_desc` `rcd_desc` VARCHAR(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

// Amit 10/1/2019
ALTER TABLE `erp_units` CHANGE `unit_base_qty` `unit_base_qty` FLOAT(11) NULL DEFAULT '1' COMMENT 'base qty/unit';

// Rakesh 25/1/2020


CREATE TABLE IF NOT EXISTS `erp_journal` (
  `jn_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `jn_no` bigint(20) NOT NULL,
  `jn_date` date NOT NULL DEFAULT '2000-01-01',
  `jn_time` time NOT NULL DEFAULT '00:00:00',
  `jn_datetime` datetime NOT NULL DEFAULT '2000-01-01 00:00:00',
  `jn_timestamp` int(11) NOT NULL,
  `jn_ttl_amount` double(8,2) NOT NULL,
  `jn_note` varchar(256) NOT NULL,
  `van_jn_no` varchar(45) NOT NULL,
  `branch_jn_no` varchar(45) NOT NULL,
  `branch_rec_no2` bigint(20) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL,
  `van_id` int(11) NOT NULL DEFAULT '0',
  `is_web` int(11) NOT NULL DEFAULT '0',
  `jn_flags` tinyint(4) NOT NULL DEFAULT '0',
  `sync_code` int(11) NOT NULL DEFAULT '0',
  `sync_completed` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`jn_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;



CREATE TABLE IF NOT EXISTS `erp_journal_sub` (
  `jnsub_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `jnsub_ref_no` bigint(20) NOT NULL,
  `jnsub_date` date NOT NULL DEFAULT '2000-01-01',
  `jnsub_time` time NOT NULL DEFAULT '00:00:00',
  `jnsub_datetime` datetime NOT NULL DEFAULT '2000-01-01 00:00:00',
  `jnsub_timestamp` int(11) NOT NULL,
  `jnsub_acc_ledger_id` int(11) NOT NULL,
  `jnsub_ledger_id` int(11) NOT NULL,
  `jnsub_amount` double NOT NULL,
  `jnsub_narration` varchar(256) NOT NULL,
  `jnsub_ttl_amount` double NOT NULL,
  `jnsub_no` double NOT NULL,
  `van_ref_no` varchar(45) NOT NULL,
  `branch_jn_no` varchar(45) NOT NULL,
  `jn_flags` tinyint(4) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL,
  `van_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`jnsub_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;


