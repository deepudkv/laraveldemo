-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Dec 19, 2019 at 09:22 AM
-- Server version: 5.7.24
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test_server`
--

-- --------------------------------------------------------

--
-- Table structure for table `erp_acc_branches`
--

DROP TABLE IF EXISTS `erp_acc_branches`;
CREATE TABLE IF NOT EXISTS `erp_acc_branches` (
  `branch_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `branch_name` varchar(150) NOT NULL,
  `branch_display_name` varchar(150) NOT NULL,
  `branch_code` varchar(50) NOT NULL,
  `branch_password` varchar(250) NOT NULL,
  `branch_address` varchar(1000) DEFAULT NULL,
  `branch_address2` varchar(1000) DEFAULT NULL,
  `branch_phone` varchar(25) DEFAULT NULL,
  `branch_mob` varchar(25) DEFAULT NULL,
  `branch_fax` varchar(40) DEFAULT NULL,
  `branch_email` varchar(100) DEFAULT NULL,
  `branch_tin` varchar(20) DEFAULT NULL,
  `branch_reg_no` varchar(100) DEFAULT NULL,
  `branch_open_date` date DEFAULT NULL,
  `branch_notes` varchar(250) DEFAULT NULL,
  `branch_default` smallint(6) NOT NULL DEFAULT '0',
  `branch_flags` smallint(6) NOT NULL DEFAULT '1',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`branch_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_acc_customers`
--

DROP TABLE IF EXISTS `erp_acc_customers`;
CREATE TABLE IF NOT EXISTS `erp_acc_customers` (
  `cust_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ledger_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(250) NOT NULL,
  `cust_home_addr` varchar(250) DEFAULT NULL,
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `alias` varchar(250) DEFAULT NULL,
  `cust_code` varchar(250) DEFAULT NULL,
  `zip` varchar(250) DEFAULT NULL,
  `city` varchar(250) DEFAULT NULL,
  `state` varchar(250) DEFAULT NULL,
  `state_code` varchar(250) DEFAULT NULL,
  `country` varchar(250) DEFAULT NULL,
  `fax` varchar(250) DEFAULT NULL,
  `dflt_delvry_addr` varchar(250) DEFAULT NULL,
  `dflt_delvry_zip` varchar(250) DEFAULT NULL,
  `dflt_delvry_city` varchar(250) DEFAULT NULL,
  `dflt_delvry_state` varchar(250) DEFAULT NULL,
  `dflt_delvry_state_code` varchar(250) DEFAULT NULL,
  `dflt_delvry_country` varchar(250) DEFAULT NULL,
  `dflt_delvry_fax` varchar(250) DEFAULT NULL,
  `dflt_delvry_mobile` varchar(250) NOT NULL,
  `cust_category` int(11) NOT NULL DEFAULT '1',
  `email` varchar(250) DEFAULT NULL,
  `mobile` varchar(250) DEFAULT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `due_days` int(11) DEFAULT NULL,
  `vat_no` varchar(250) DEFAULT NULL,
  `van_line_id` int(11) NOT NULL DEFAULT '0',
  `price_group_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`cust_id`),
  UNIQUE KEY `erp_acc_customers_name_unique` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_acc_groups`
--

DROP TABLE IF EXISTS `erp_acc_groups`;
CREATE TABLE IF NOT EXISTS `erp_acc_groups` (
  `accgrp_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `accgrp_name` varchar(100) NOT NULL,
  `accgrp_nature` smallint(6) DEFAULT '0',
  `accgrp_parent` smallint(6) DEFAULT '0',
  `accgrp_root` smallint(6) DEFAULT '0',
  `accgrp_depth` smallint(6) DEFAULT '0',
  `accgrp_edit` smallint(6) DEFAULT '0',
  `accgrp_flags` tinyint(4) NOT NULL DEFAULT '0',
  `accgrp_enable_ledger` smallint(6) DEFAULT '0',
  `accgrp_code` varchar(50) DEFAULT '0',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`accgrp_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1000 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `erp_acc_groups`
--

INSERT INTO `erp_acc_groups` (`accgrp_id`, `accgrp_name`, `accgrp_nature`, `accgrp_parent`, `accgrp_root`, `accgrp_depth`, `accgrp_edit`, `accgrp_flags`, `accgrp_enable_ledger`, `accgrp_code`, `company_id`, `branch_id`, `server_sync_flag`, `server_sync_time`, `local_sync_time`, `created_at`, `updated_at`) VALUES
(1, 'Current Assets', 2, 1, 1, 0, 0, 1, 1, '', 0, 0, 0, 191219145057797684, 0, NULL, NULL),
(2, 'Fixed Assets', 2, 2, 2, 0, 0, 1, 1, '', 0, 0, 0, 191219145057797684, 0, NULL, NULL),
(3, 'Cash in Hand', 2, 1, 1, 1, 0, 1, 1, '', 0, 0, 0, 191219145057797684, 0, NULL, NULL),
(4, 'Bank Accounts', 2, 1, 1, 1, 0, 1, 1, '', 0, 0, 0, 191219145057797684, 0, NULL, NULL),
(5, 'Indirect Expense', 1, 5, 5, 0, 0, 1, 1, '', 0, 0, 0, 191219145057797684, 0, NULL, NULL),
(6, 'Direct Expense', 1, 6, 6, 0, 0, 1, 1, '', 0, 0, 0, 191219145057797684, 0, NULL, NULL),
(7, 'Indirect Income', 0, 7, 7, 0, 0, 1, 1, '', 0, 0, 0, 191219145057797684, 0, NULL, NULL),
(8, 'Direct Income', 0, 8, 8, 0, 0, 1, 1, '', 0, 0, 0, 191219145057797684, 0, NULL, NULL),
(9, 'Sales Accounts', 0, 9, 9, 0, 0, 1, 1, '', 0, 0, 0, 191219145057797684, 0, NULL, NULL),
(10, 'Purchase Accounts', 1, 10, 10, 0, 0, 1, 1, '', 0, 0, 0, 191219145057797684, 0, NULL, NULL),
(11, 'Capital Account', 3, 11, 11, 0, 0, 1, 1, '', 0, 0, 0, 191219145057797684, 0, NULL, NULL),
(12, 'Current Liability', 3, 12, 12, 0, 0, 1, 1, '', 0, 0, 0, 191219145057797684, 0, NULL, NULL),
(13, 'Sundry Creditors', 3, 12, 12, 1, 0, 1, 1, '', 0, 0, 0, 191219145057797684, 0, NULL, NULL),
(14, 'Sundry Deptors', 2, 1, 1, 1, 0, 1, 1, '', 0, 0, 0, 191219145057797684, 0, NULL, NULL),
(15, 'Duties and Taxes', 3, 12, 12, 1, 0, 1, 1, '', 0, 0, 0, 191219145057797684, 0, NULL, NULL),
(16, 'Stock in Hand', 2, 1, 1, 1, 0, 1, 1, '', 0, 0, 0, 191219145057797684, 0, NULL, NULL),
(17, 'Service Accounts', 0, 17, 17, 0, 0, 1, 1, '', 0, 0, 0, 191219145057797684, 0, NULL, NULL),
(18, 'Loans (Liability)', 3, 18, 18, 0, 0, 1, 1, '', 0, 0, 0, 191219145057797684, 0, NULL, NULL),
(19, 'Salary', 3, 19, 19, 0, 0, 1, 1, '', 0, 0, 0, 191219145057797684, 0, NULL, NULL),
(20, 'Supplier', 3, 12, 12, 1, 0, 1, 1, '', 0, 0, 0, 191219145057797684, 0, NULL, NULL),
(21, 'Customer', 2, 1, 1, 1, 0, 1, 1, '', 0, 0, 0, 191219145057797684, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `erp_acc_ledgers`
--

DROP TABLE IF EXISTS `erp_acc_ledgers`;
CREATE TABLE IF NOT EXISTS `erp_acc_ledgers` (
  `ledger_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ledger_name` varchar(250) NOT NULL DEFAULT 'NILL',
  `ledger_accgrp_id` smallint(6) NOT NULL DEFAULT '0',
  `ledger_acc_type` smallint(6) DEFAULT '2' COMMENT '0-cash 1-bank 2-other',
  `ledger_return` smallint(6) DEFAULT '1' COMMENT '0-no 1-yes',
  `ledger_address` varchar(250) DEFAULT 'NILL',
  `ledger_notes` varchar(250) DEFAULT 'NILL',
  `ledger_edit` smallint(6) DEFAULT '1',
  `ledger_flags` smallint(6) NOT NULL DEFAULT '0',
  `ledger_branch_id` smallint(6) DEFAULT '1',
  `ledger_balance_privacy` smallint(6) DEFAULT '0',
  `ledger_due` smallint(6) DEFAULT '0',
  `ledger_tin` varchar(50) DEFAULT 'NILL',
  `ledger_alias` varchar(50) DEFAULT 'NILL',
  `ledger_code` varchar(50) DEFAULT 'NILL',
  `ledger_email` varchar(50) DEFAULT 'NILL',
  `ledger_mobile` varchar(50) DEFAULT 'NILL',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ledger_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_acc_opening_balances`
--

DROP TABLE IF EXISTS `erp_acc_opening_balances`;
CREATE TABLE IF NOT EXISTS `erp_acc_opening_balances` (
  `opbal_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `opbal_ledger_id` int(11) NOT NULL DEFAULT '0',
  `opbal_in` double(8,2) NOT NULL DEFAULT '0.00',
  `opbal_out` double(8,2) NOT NULL DEFAULT '0.00',
  `opbal_added_by` int(11) NOT NULL DEFAULT '1',
  `opbal_flags` tinyint(4) NOT NULL DEFAULT '1',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`opbal_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_acc_suppliers`
--

DROP TABLE IF EXISTS `erp_acc_suppliers`;
CREATE TABLE IF NOT EXISTS `erp_acc_suppliers` (
  `supp_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ledger_id` int(11) NOT NULL DEFAULT '0',
  `supp_perm_addr` varchar(250) DEFAULT 'NILL',
  `supp_image` varchar(250) DEFAULT 'NILL',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`supp_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_acc_voucher`
--

DROP TABLE IF EXISTS `erp_acc_voucher`;
CREATE TABLE IF NOT EXISTS `erp_acc_voucher` (
  `vch_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `vch_no` int(11) NOT NULL COMMENT 'voucher number',
  `vch_ledger_from` int(11) NOT NULL COMMENT 'Ledger from',
  `vch_ledger_to` int(11) NOT NULL COMMENT 'Ledger to',
  `vch_date` datetime NOT NULL DEFAULT '2000-01-01 00:00:00',
  `vch_in` double NOT NULL COMMENT 'amount in',
  `vch_out` double NOT NULL COMMENT 'amount out',
  `vch_vchtype_id` int(11) NOT NULL COMMENT 'voucher type\\n0-pay\\n1-Reciept\\n2-contra\\n3-journal\\n4-sales\\n5-purchase\\n6-debitnote\\n7-creditnote\\n',
  `vch_notes` varchar(250) NOT NULL DEFAULT '',
  `vch_added_by` int(11) NOT NULL DEFAULT '0',
  `vch_id2` int(11) NOT NULL DEFAULT '0',
  `vch_from_group` int(11) NOT NULL DEFAULT '0',
  `vch_flags` int(11) NOT NULL,
  `sales_inv_no` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `is_web` tinyint(4) NOT NULL DEFAULT '0',
  `godown_id` int(11) NOT NULL DEFAULT '0',
  `van_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`vch_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_branch_stocks`
--

DROP TABLE IF EXISTS `erp_branch_stocks`;
CREATE TABLE IF NOT EXISTS `erp_branch_stocks` (
  `branch_stock_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `bs_stock_id` bigint(20) NOT NULL,
  `bs_prd_id` bigint(20) NOT NULL,
  `bs_stock_quantity` double NOT NULL DEFAULT '0',
  `bs_stock_quantity_shop` double NOT NULL DEFAULT '0',
  `bs_stock_quantity_gd` double NOT NULL DEFAULT '0',
  `bs_stock_quantity_van` double NOT NULL DEFAULT '0',
  `bs_prate` double NOT NULL DEFAULT '0',
  `bs_avg_prate` double NOT NULL DEFAULT '0',
  `bs_srate` double NOT NULL DEFAULT '0',
  `bs_batch_id` bigint(20) NOT NULL DEFAULT '0',
  `bs_expiry` varchar(100) NOT NULL DEFAULT '0',
  `bs_stk_status` tinyint(4) NOT NULL DEFAULT '1',
  `bs_branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`branch_stock_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_branch_stock_logs`
--

DROP TABLE IF EXISTS `erp_branch_stock_logs`;
CREATE TABLE IF NOT EXISTS `erp_branch_stock_logs` (
  `bsl_log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `bsl_branch_stock_id` bigint(20) NOT NULL,
  `bsl_stock_id` bigint(20) DEFAULT NULL,
  `bsl_prd_id` bigint(20) DEFAULT NULL,
  `bsl_shop_quantity` double NOT NULL DEFAULT '0',
  `bsl_gd_quantity` double NOT NULL DEFAULT '0',
  `bsl_prate` double NOT NULL DEFAULT '0',
  `bsl_srate` double NOT NULL DEFAULT '0',
  `bsl_revert` tinyint(4) NOT NULL DEFAULT '0',
  `bsl_avg_prate` double NOT NULL DEFAULT '0',
  `bsl_branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`bsl_log_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_categories`
--

DROP TABLE IF EXISTS `erp_categories`;
CREATE TABLE IF NOT EXISTS `erp_categories` (
  `cat_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(150) NOT NULL,
  `cat_remarks` mediumtext,
  `cat_flag` tinyint(4) NOT NULL DEFAULT '1',
  `cat_pos` tinyint(4) DEFAULT '1',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_companies`
--

DROP TABLE IF EXISTS `erp_companies`;
CREATE TABLE IF NOT EXISTS `erp_companies` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cmp_name` varchar(200) NOT NULL,
  `cmp_addr` varchar(500) NOT NULL,
  `cpm_code` varchar(100) NOT NULL,
  `cmp_sub_domain` varchar(500) NOT NULL,
  `up_limit` int(11) NOT NULL DEFAULT '100',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_company_settings`
--

DROP TABLE IF EXISTS `erp_company_settings`;
CREATE TABLE IF NOT EXISTS `erp_company_settings` (
  `cmp_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cmp_name` varchar(150) NOT NULL,
  `cmp_display_name` varchar(150) NOT NULL,
  `cmp_code` varchar(50) NOT NULL,
  `cmp_address` varchar(1000) DEFAULT NULL,
  `cmp_address2` varchar(1000) DEFAULT NULL,
  `cmp_phone` varchar(25) DEFAULT NULL,
  `cmp_mob` varchar(25) DEFAULT NULL,
  `cmp_fax` varchar(40) DEFAULT NULL,
  `cmp_email` varchar(100) DEFAULT NULL,
  `cmp_tin` varchar(20) DEFAULT NULL,
  `cmp_finyear_start` date NOT NULL,
  `cmp_finyear_end` date NOT NULL,
  `cmp_reg_no` varchar(100) DEFAULT NULL,
  `cmp_notes` varchar(250) NOT NULL,
  `cmp_default` smallint(6) NOT NULL DEFAULT '0',
  `cmp_flags` smallint(6) NOT NULL DEFAULT '1',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`cmp_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `erp_company_settings`
--

INSERT INTO `erp_company_settings` (`cmp_id`, `cmp_name`, `cmp_display_name`, `cmp_code`, `cmp_address`, `cmp_address2`, `cmp_phone`, `cmp_mob`, `cmp_fax`, `cmp_email`, `cmp_tin`, `cmp_finyear_start`, `cmp_finyear_end`, `cmp_reg_no`, `cmp_notes`, `cmp_default`, `cmp_flags`, `server_sync_flag`, `server_sync_time`, `local_sync_time`, `created_at`, `updated_at`) VALUES
(1, 'Fujishka Pvt Ltd', 'Fujishka Pvt Ltd', 'FJK', NULL, NULL, NULL, NULL, NULL, 'info@fujishka.com', NULL, '2019-04-01', '2020-03-31', NULL, '', 0, 1, 0, 191219145058638865, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `erp_company_stocks`
--

DROP TABLE IF EXISTS `erp_company_stocks`;
CREATE TABLE IF NOT EXISTS `erp_company_stocks` (
  `cmp_stock_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cmp_prd_id` bigint(20) NOT NULL,
  `cmp_stock_quantity` double NOT NULL DEFAULT '0',
  `cmp_avg_prate` double NOT NULL DEFAULT '0',
  `cmp_srate` double NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`cmp_stock_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_damages`
--

DROP TABLE IF EXISTS `erp_damages`;
CREATE TABLE IF NOT EXISTS `erp_damages` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `damage_stock_id` int(11) NOT NULL DEFAULT '0',
  `damage_prod_id` int(11) NOT NULL DEFAULT '0',
  `damage_qty` double DEFAULT '0',
  `damage_rem_qty` double DEFAULT '0',
  `damage_purch_rate` double DEFAULT '0',
  `damage_notes` varchar(100) DEFAULT 'NILL',
  `damage_serail` varchar(100) DEFAULT 'NILL',
  `damage_date` date DEFAULT '2000-01-01',
  `damage_added_by` int(11) DEFAULT '0',
  `damage_flags` tinyint(4) NOT NULL DEFAULT '1',
  `damage_unit_id` int(11) DEFAULT '1',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_features`
--

DROP TABLE IF EXISTS `erp_features`;
CREATE TABLE IF NOT EXISTS `erp_features` (
  `feat_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `feat_name` varchar(100) NOT NULL,
  `feat_description` varchar(200) DEFAULT NULL,
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`feat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_feature_products`
--

DROP TABLE IF EXISTS `erp_feature_products`;
CREATE TABLE IF NOT EXISTS `erp_feature_products` (
  `fetprod_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `fetprod_prod_id` int(11) NOT NULL DEFAULT '0',
  `fetprod_fet_id` int(11) DEFAULT '0',
  `fetprod_fet_val` varchar(200) DEFAULT 'Nill',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`fetprod_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_godown_master`
--

DROP TABLE IF EXISTS `erp_godown_master`;
CREATE TABLE IF NOT EXISTS `erp_godown_master` (
  `gd_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `gd_name` varchar(50) DEFAULT NULL,
  `gd_code` varchar(30) DEFAULT NULL,
  `gd_cntct_person` varchar(30) DEFAULT NULL,
  `gd_cntct_num` varchar(30) DEFAULT NULL,
  `gd_address` varchar(50) DEFAULT NULL,
  `gd_desc` varchar(50) DEFAULT NULL,
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`gd_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_godown_stocks`
--

DROP TABLE IF EXISTS `erp_godown_stocks`;
CREATE TABLE IF NOT EXISTS `erp_godown_stocks` (
  `gs_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `gs_godown_id` bigint(20) NOT NULL,
  `gs_branch_stock_id` bigint(20) NOT NULL,
  `gs_stock_id` bigint(20) NOT NULL,
  `gs_prd_id` bigint(20) NOT NULL,
  `gs_qty` double(15,8) NOT NULL DEFAULT '0.00000000',
  `gs_date` date NOT NULL DEFAULT '2019-12-19',
  `gs_flag` tinyint(4) NOT NULL DEFAULT '1',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`gs_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_godown_stock_log`
--

DROP TABLE IF EXISTS `erp_godown_stock_log`;
CREATE TABLE IF NOT EXISTS `erp_godown_stock_log` (
  `gsl_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `gsl_gdwn_stock_id` bigint(20) NOT NULL,
  `gsl_branch_stock_id` bigint(20) NOT NULL,
  `gsl_stock_id` bigint(20) NOT NULL,
  `gsl_prd_id` bigint(20) NOT NULL,
  `gsl_prod_unit` int(11) NOT NULL DEFAULT '0',
  `gsl_qty` double(15,8) NOT NULL DEFAULT '0.00000000',
  `gsl_from` int(11) NOT NULL DEFAULT '0',
  `gsl_to` int(11) NOT NULL DEFAULT '0',
  `gsl_vchr_type` int(11) NOT NULL DEFAULT '0',
  `gsl_date` date NOT NULL DEFAULT '2019-12-19',
  `gsl_added_by` int(11) NOT NULL DEFAULT '0',
  `gsl_flag` tinyint(4) NOT NULL DEFAULT '1',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`gsl_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_manufacturer`
--

DROP TABLE IF EXISTS `erp_manufacturer`;
CREATE TABLE IF NOT EXISTS `erp_manufacturer` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `manftr_comp_name` varchar(80) NOT NULL,
  `manftr_comp_code` varchar(20) NOT NULL,
  `manftr_comp_address` mediumtext,
  `manftr_comp_phone` varchar(25) DEFAULT NULL,
  `manftr_comp_mobile` varchar(25) DEFAULT NULL,
  `manftr_comp_fax` varchar(25) DEFAULT NULL,
  `manftr_comp_email` varchar(60) DEFAULT NULL,
  `manftr_comp_contact_person` varchar(45) DEFAULT NULL,
  `manftr_comp_website` varchar(60) DEFAULT NULL,
  `manftr_comp_addedby` varchar(45) DEFAULT NULL,
  `manftr_comp_add_date` date NOT NULL DEFAULT '2019-12-19',
  `manftr_comp_notes` varchar(200) DEFAULT NULL,
  `manftr_comp_flags` tinyint(4) DEFAULT '1',
  `manftr_created_at` timestamp NULL DEFAULT NULL,
  `manftr_updated_at` timestamp NULL DEFAULT NULL,
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `erp_manufacturer_manftr_comp_email_unique` (`manftr_comp_email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_migrations`
--

DROP TABLE IF EXISTS `erp_migrations`;
CREATE TABLE IF NOT EXISTS `erp_migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=105 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `erp_migrations`
--

INSERT INTO `erp_migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2019_06_11_062713_create_sessions_table', 1),
(9, '2019_06_12_055131_create_categories_table', 1),
(10, '2019_06_12_055325_create_units_table', 1),
(11, '2019_06_12_055625_create_subcategories_table', 1),
(12, '2019_06_12_093427_create_products_table', 1),
(13, '2019_06_12_102148_create_usertypes_table', 1),
(14, '2019_06_17_113814_create_features_table', 1),
(15, '2019_06_20_075225_create_acountgroups_table', 1),
(16, '2019_06_20_115143_create_ledger_table', 1),
(17, '2019_06_21_085945_create_branches_table', 1),
(18, '2019_06_21_100926_create_customer_table', 1),
(19, '2019_06_22_070511_create_supplier_table', 1),
(20, '2019_07_10_083429_manufacturer', 1),
(21, '2019_07_10_085528_damagedproducts', 1),
(22, '2019_07_16_110942_prod_units', 1),
(23, '2019_07_19_062705_feature_product', 1),
(24, '2019_08_14_115417_acc_opening_balance', 1),
(25, '2019_08_22_074218_create_company_stocks_table', 1),
(26, '2019_08_22_074232_create_branch_stocks_table', 1),
(27, '2019_08_22_074304_create_stock_unit_rates_table', 1),
(28, '2019_08_23_043349_stock_daily_updates', 1),
(29, '2019_08_23_052705_stock_batches', 1),
(30, '2019_08_23_092352_create_godown_masters_table', 1),
(31, '2019_08_24_065252_create_godown_stocks_table', 1),
(32, '2019_08_27_051528_godownstok_log', 1),
(33, '2019_08_28_085748_create_branchstocklogs_table', 1),
(34, '2019_08_30_053121_voucher_type', 1),
(35, '2019_09_16_071907_opening_stock_log', 1),
(36, '2019_09_25_063500_create_suppliers_table', 1),
(37, '2019_09_25_102258_create_purchases_table', 1),
(38, '2019_09_25_102258_create_temp_purchases_table', 1),
(39, '2019_09_25_10233534_create_purchase_return_sub_table', 1),
(40, '2019_09_25_102335454_create_purchase_return_table', 1),
(41, '2019_09_25_102335_create_purchase_subs_table', 1),
(42, '2019_09_25_102335_create_temp_purchase_sub_table', 1),
(43, '2019_10_10_111731_van', 1),
(44, '2019_10_10_111731_van_lines', 1),
(45, '2019_10_15_112841_van_transfer_table', 1),
(46, '2019_10_15_112906_van_stock_table', 1),
(47, '2019_10_15_112920_van_transfer_sub_table', 1),
(48, '2019_10_21_072513_create_van_daily_stocks', 1),
(49, '2019_10_22_090836_van_transfer_return', 1),
(50, '2019_10_22_090849_van_transfer_return_sub', 1),
(51, '2019_10_23_042403_create_production_formula_table', 1),
(52, '2019_10_23_050943_create_production_formula_sub_table', 1),
(53, '2019_10_23_051549_create_production_formula_product_table', 1),
(54, '2019_10_25_083002_create_staff_table', 1),
(55, '2019_10_30_060746_create_production_formula_comm_table', 1),
(56, '2019_11_04_093843_add_perc_to_production_formula_prod', 1),
(57, '2019_11_06_121747_production', 1),
(58, '2019_11_06_121806_production_sub', 1),
(59, '2019_11_06_121826_production_products', 1),
(60, '2019_11_06_121835_production_commission', 1),
(61, '2019_11_11_113003_create_salesmaster_table', 1),
(62, '2019_11_11_121458_create_salesub_table', 1),
(63, '2019_11_13_085440_create_salesdue_table', 1),
(64, '2019_11_13_090050_create_salesdue_sub_table', 1),
(65, '2019_11_13_090951_create_salesreturn_master_table', 1),
(66, '2019_11_13_101601_create_salesreturn_sub_table', 1),
(67, '2019_11_14_043816_create_acc_voucher_table', 1),
(68, '2019_11_15_092524_create_sales_agent_table', 1),
(69, '2019_11_15_092934_create_sales_commission_table', 1),
(70, '2019_11_19_050631_sales_master_add_webcolum', 1),
(71, '2019_11_19_053357_create_sync_salesmaster_table', 1),
(72, '2019_11_19_053417_create_sync_salessub_table', 1),
(73, '2019_11_19_053458_create_sync_salesdue_table', 1),
(74, '2019_11_19_053520_create_sync_salesdue_sub_table', 1),
(75, '2019_11_19_053558_create_sync_salesreturn_master_table', 1),
(76, '2019_11_19_060426_create_sync_salesreturn_sub_table', 1),
(77, '2019_11_19_103730_update_acc_customer', 1),
(78, '2019_11_20_072022_create_price_group_table', 1),
(79, '2019_11_21_050312_update_acc_customer_code', 1),
(80, '2019_11_21_163713_create_company_table', 1),
(81, '2019_11_25_143536_add_phot_colum_to_van_table', 1),
(82, '2019_11_25_163801_add_photo_colum_to_user_table', 1),
(83, '2019_11_27_171812_create_van_users', 1),
(84, '2019_11_27_173046_add_md5_column_users_table', 1),
(85, '2019_11_29_114316_add-password_to_branch', 1),
(86, '2019_12_02_152550_sales_master_add_inv_no', 1),
(87, '2019_12_02_155928_companay_upload_limit_column_add', 1),
(88, '2019_12_03_103202_add_inv_no_to_slaes_sync_master', 1),
(89, '2019_12_04_105907_is_sync_flag_add_to_sales_master_table', 1),
(90, '2019_12_04_110914_is_sync_flag_add_to_sales_sub_table', 1),
(91, '2019_12_04_112643_is_sync_flag_add_to_sales_due_table', 1),
(92, '2019_12_04_112656_is_sync_flag_add_to_sales_due_sub_table', 1),
(93, '2019_12_04_112723_is_sync_flag_add_to_sales_return_master_table', 1),
(94, '2019_12_04_112738_is_sync_flag_add_to_sales_return_sub_table', 1),
(95, '2019_12_06_101427_prdctwithoutstock', 1),
(96, '2019_12_11_171026_company_settings', 1),
(97, '2019_12_12_114039_sync_code_add_to_sales_master_table', 1),
(98, '2019_12_12_114114_sync_code_add_to_sales_sub_table', 1),
(99, '2019_12_12_114150_sync_code_add_to_sales_due_table', 1),
(100, '2019_12_12_114545_sync_code_add_to_sales_due_sub_table', 1),
(101, '2019_12_12_114702_sync_code_add_to_sales_return_master_table', 1),
(102, '2019_12_12_114719_sync_code_add_to_sales_return_sub_table', 1),
(103, '2019_12_13_120616_data_seeding', 1),
(104, '2019_12_16_132553_cutomer_table_add_phone_coloumn', 1);

-- --------------------------------------------------------

--
-- Table structure for table `erp_oauth_access_tokens`
--

DROP TABLE IF EXISTS `erp_oauth_access_tokens`;
CREATE TABLE IF NOT EXISTS `erp_oauth_access_tokens` (
  `id` varchar(100) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `scopes` text,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `erp_oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_oauth_auth_codes`
--

DROP TABLE IF EXISTS `erp_oauth_auth_codes`;
CREATE TABLE IF NOT EXISTS `erp_oauth_auth_codes` (
  `id` varchar(100) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_oauth_clients`
--

DROP TABLE IF EXISTS `erp_oauth_clients`;
CREATE TABLE IF NOT EXISTS `erp_oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `secret` varchar(100) NOT NULL,
  `redirect` text NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `erp_oauth_clients_user_id_index` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `erp_oauth_clients`
--

INSERT INTO `erp_oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'R0LEpHPEo7D1859QysKMZMlOyJcrVLlz4LUTDhTr', 'http://localhost', 1, 0, 0, '2019-12-19 09:22:07', '2019-12-19 09:22:07'),
(2, NULL, 'Laravel Password Grant Client', 'TUSVRVvrWdsRTxbyLcNaGeIQaAy1tOdJBnjCJzNK', 'http://localhost', 0, 1, 0, '2019-12-19 09:22:07', '2019-12-19 09:22:07');

-- --------------------------------------------------------

--
-- Table structure for table `erp_oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `erp_oauth_personal_access_clients`;
CREATE TABLE IF NOT EXISTS `erp_oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `erp_oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `erp_oauth_personal_access_clients`
--

INSERT INTO `erp_oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-12-19 09:22:07', '2019-12-19 09:22:07');

-- --------------------------------------------------------

--
-- Table structure for table `erp_oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `erp_oauth_refresh_tokens`;
CREATE TABLE IF NOT EXISTS `erp_oauth_refresh_tokens` (
  `id` varchar(100) NOT NULL,
  `access_token_id` varchar(100) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `erp_oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_opening_stock_log`
--

DROP TABLE IF EXISTS `erp_opening_stock_log`;
CREATE TABLE IF NOT EXISTS `erp_opening_stock_log` (
  `opstklog_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `opstklog_branch_stock_id` bigint(20) NOT NULL,
  `opstklog_prd_id` bigint(20) NOT NULL,
  `opstklog_stock_id` bigint(20) NOT NULL,
  `opstklog_date` date NOT NULL DEFAULT '2019-12-19',
  `opstklog_prate` double NOT NULL DEFAULT '0',
  `opstklog_srate` double NOT NULL DEFAULT '0',
  `opstklog_stock_quantity_add` double NOT NULL DEFAULT '0',
  `opstklog_stock_quantity_rem` double NOT NULL DEFAULT '0',
  `opstklog_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Add 0 | Remove 1',
  `opstklog_status` tinyint(4) NOT NULL DEFAULT '1',
  `opstklog_unit_id` bigint(20) NOT NULL DEFAULT '0',
  `opstklog_added_by` bigint(20) NOT NULL DEFAULT '0',
  `opstklog_gd_id` bigint(20) NOT NULL DEFAULT '0',
  `opstklog_batch_id` bigint(20) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`opstklog_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_password_resets`
--

DROP TABLE IF EXISTS `erp_password_resets`;
CREATE TABLE IF NOT EXISTS `erp_password_resets` (
  `email` varchar(250) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `erp_password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_price_group`
--

DROP TABLE IF EXISTS `erp_price_group`;
CREATE TABLE IF NOT EXISTS `erp_price_group` (
  `prcgrp_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `group_name` varchar(200) NOT NULL,
  `group_descp` varchar(1000) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `godown_id` int(11) NOT NULL DEFAULT '0',
  `van_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`prcgrp_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_production`
--

DROP TABLE IF EXISTS `erp_production`;
CREATE TABLE IF NOT EXISTS `erp_production` (
  `prdn_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `prdn_prodform_id` int(11) NOT NULL,
  `prdn_purch_amount` double NOT NULL,
  `prdn_misc_exp` double NOT NULL,
  `prdn_comm_amount` double NOT NULL,
  `prdn_cost` double NOT NULL,
  `prdn_date` timestamp NOT NULL,
  `prdn_added_by` bigint(20) NOT NULL,
  `prdn_inspection_staff` bigint(20) NOT NULL,
  `prdn_commission` tinyint(4) NOT NULL COMMENT 'commission 0-no 1-yes',
  `prdn_flag` tinyint(4) NOT NULL DEFAULT '1',
  `prdn_note` varchar(500) DEFAULT NULL,
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`prdn_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_production_comm`
--

DROP TABLE IF EXISTS `erp_production_comm`;
CREATE TABLE IF NOT EXISTS `erp_production_comm` (
  `prdncomm_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `prdncomm_prdn_id` int(11) NOT NULL,
  `prdncomm_prodform_id` int(11) NOT NULL DEFAULT '0',
  `prdncomm_laccount_no` int(11) NOT NULL DEFAULT '0' COMMENT 'staff ledger account no',
  `prdncomm_in` double NOT NULL,
  `prdncomm_out` double NOT NULL,
  `prdncomm_date` timestamp NOT NULL,
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `prdncomm_flag` tinyint(4) NOT NULL DEFAULT '1',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) DEFAULT NULL,
  `local_sync_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`prdncomm_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_production_formula`
--

DROP TABLE IF EXISTS `erp_production_formula`;
CREATE TABLE IF NOT EXISTS `erp_production_formula` (
  `prodform_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `prodform_name` varchar(200) NOT NULL,
  `prodform_ingred` bigint(20) NOT NULL DEFAULT '1',
  `prodform_prod_count` bigint(20) NOT NULL DEFAULT '1' COMMENT 'No of output product',
  `prodform_commission` bigint(20) NOT NULL COMMENT 'commission 0-no 1-yes',
  `prodform_comm_amount` double NOT NULL COMMENT 'commssion amount',
  `prodform_misc` double NOT NULL COMMENT 'misc expense',
  `prodform_insp_laccount_no` double NOT NULL COMMENT 'inspection staff id',
  `prodfrom_date` date NOT NULL DEFAULT '2000-01-01',
  `prodform_added_by` bigint(20) NOT NULL DEFAULT '1',
  `prodform_flag` tinyint(4) NOT NULL DEFAULT '1',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`prodform_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_production_formula_comm`
--

DROP TABLE IF EXISTS `erp_production_formula_comm`;
CREATE TABLE IF NOT EXISTS `erp_production_formula_comm` (
  `prodformcomm_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `prodformcomm_prodform_id` int(11) NOT NULL DEFAULT '0',
  `prodformcomm_laccount_no` int(11) NOT NULL DEFAULT '0' COMMENT 'staff ledger account no',
  `prodformcomm_amount` double NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`prodformcomm_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_production_formula_product`
--

DROP TABLE IF EXISTS `erp_production_formula_product`;
CREATE TABLE IF NOT EXISTS `erp_production_formula_product` (
  `prodformprod_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `prodformprod_prodform_id` bigint(20) NOT NULL COMMENT 'formula id',
  `prodformprod_prod_id` bigint(20) NOT NULL COMMENT 'output product id',
  `prodformprod_qty` double NOT NULL DEFAULT '0',
  `prodformprod_unit_id` double NOT NULL DEFAULT '1',
  `prodformprod_flag` double NOT NULL DEFAULT '1',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `prodformprod_perc` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`prodformprod_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_production_formula_sub`
--

DROP TABLE IF EXISTS `erp_production_formula_sub`;
CREATE TABLE IF NOT EXISTS `erp_production_formula_sub` (
  `prodformsub_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `prodformsub_prodform_id` bigint(20) NOT NULL COMMENT 'formula id',
  `prodformsub_prod_id` bigint(20) NOT NULL COMMENT 'ingredient product id	',
  `prodformsub_qty` double NOT NULL DEFAULT '0',
  `prodformsub_unit_id` double NOT NULL DEFAULT '1',
  `prodformsub_flag` tinyint(4) NOT NULL DEFAULT '1',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`prodformsub_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_production_products`
--

DROP TABLE IF EXISTS `erp_production_products`;
CREATE TABLE IF NOT EXISTS `erp_production_products` (
  `prdnprd_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `prdnprd_prdn_id` bigint(20) NOT NULL,
  `prdnprd_prodform_id` int(11) NOT NULL,
  `prdnprd_prd_id` bigint(20) NOT NULL,
  `prdnprd_stock_id` bigint(20) NOT NULL,
  `prdnprd_branch_stock_id` bigint(20) NOT NULL,
  `prdnprd_qty` double NOT NULL,
  `prdnprd_required_qty` double NOT NULL,
  `prdnprd_unit_id` int(11) NOT NULL,
  `prdnprd_rate` double NOT NULL,
  `prdnprd_rate_expected` double NOT NULL,
  `prdnprd_date` timestamp NOT NULL,
  `prdnprd_btach_code` varchar(100) DEFAULT NULL,
  `prdnprd_exp_date` timestamp NULL DEFAULT NULL,
  `prdnprd_mfg_date` timestamp NULL DEFAULT NULL,
  `prdnprd_gd_id` bigint(20) DEFAULT NULL,
  `prdnprd_added_by` int(11) NOT NULL,
  `prdnprd_flag` tinyint(4) NOT NULL DEFAULT '1',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`prdnprd_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_production_sub`
--

DROP TABLE IF EXISTS `erp_production_sub`;
CREATE TABLE IF NOT EXISTS `erp_production_sub` (
  `prdnsub_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `prdnsub_prdn_id` bigint(20) NOT NULL,
  `prdnsub_prodform_id` int(11) NOT NULL,
  `prdnsub_prodformsub_id` int(11) NOT NULL,
  `prdnsub_prd_id` bigint(20) NOT NULL,
  `prdnsub_stock_id` bigint(20) NOT NULL,
  `prdnsub_branch_stock_id` bigint(20) NOT NULL,
  `prdnsub_qty` double NOT NULL,
  `prdnsub_unit_id` int(11) NOT NULL,
  `prdnsub_rate` double NOT NULL,
  `prdnsub_date` timestamp NOT NULL,
  `prdnsub_added_by` int(11) NOT NULL,
  `prdnsub_flag` tinyint(4) NOT NULL DEFAULT '1',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`prdnsub_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_products`
--

DROP TABLE IF EXISTS `erp_products`;
CREATE TABLE IF NOT EXISTS `erp_products` (
  `prd_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `prd_name` varchar(500) NOT NULL,
  `prd_cat_id` bigint(20) NOT NULL,
  `prd_sub_cat_id` bigint(20) DEFAULT '0',
  `prd_supplier` bigint(20) DEFAULT NULL COMMENT 'manufacture_id',
  `prd_alias` varchar(500) DEFAULT NULL,
  `prd_short_name` varchar(15) DEFAULT NULL,
  `prd_min_stock` int(11) DEFAULT NULL,
  `prd_max_stock` int(11) DEFAULT NULL,
  `prd_barcode` bigint(20) DEFAULT NULL,
  `prd_ean` varchar(250) DEFAULT NULL,
  `prd_added_by` int(11) DEFAULT NULL,
  `prd_base_unit_id` int(11) NOT NULL,
  `prd_default_unit_id` int(11) DEFAULT NULL,
  `prd_tax` double DEFAULT NULL,
  `prd_code` varchar(10) DEFAULT NULL,
  `prd_tax_code` varchar(50) DEFAULT NULL,
  `prd_desc` text,
  `prd_exp_dur` int(11) DEFAULT NULL,
  `prd_loyalty_rate` double(15,8) DEFAULT NULL,
  `prd_type` tinyint(4) NOT NULL DEFAULT '1',
  `prd_stock_status` tinyint(4) NOT NULL DEFAULT '1',
  `prd_stock_stat` tinyint(4) NOT NULL DEFAULT '1',
  `prd_minstock_alert` tinyint(4) DEFAULT NULL,
  `prd_maxstock_alert` tinyint(4) DEFAULT NULL,
  `prd_remarks` mediumtext,
  `prd_flag` tinyint(4) NOT NULL DEFAULT '1',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`prd_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_product_without_stock`
--

DROP TABLE IF EXISTS `erp_product_without_stock`;
CREATE TABLE IF NOT EXISTS `erp_product_without_stock` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `prd_id` bigint(20) NOT NULL,
  `import_flag` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_prod_units`
--

DROP TABLE IF EXISTS `erp_prod_units`;
CREATE TABLE IF NOT EXISTS `erp_prod_units` (
  `produnit_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `produnit_prod_id` int(11) NOT NULL DEFAULT '0',
  `produnit_unit_id` int(11) DEFAULT '0',
  `produnit_ean_barcode` varchar(255) DEFAULT '0',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`produnit_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_purchases`
--

DROP TABLE IF EXISTS `erp_purchases`;
CREATE TABLE IF NOT EXISTS `erp_purchases` (
  `purch_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `purch_id2` varchar(100) NOT NULL,
  `purch_branch_id` bigint(20) NOT NULL,
  `purch_no` varchar(50) NOT NULL,
  `purch_inv_no` varchar(50) NOT NULL,
  `purch_supp_id` bigint(20) NOT NULL,
  `purch_ord_no` bigint(20) NOT NULL,
  `purch_type` tinyint(4) NOT NULL,
  `purch_pay_type` tinyint(4) NOT NULL,
  `purch_date` date NOT NULL,
  `purch_inv_date` date NOT NULL,
  `purch_amount` double NOT NULL,
  `purch_tax` double NOT NULL,
  `purch_tax_ledger_id` bigint(20) NOT NULL,
  `purch_discount` double NOT NULL,
  `purch_frieght` double NOT NULL,
  `purch_note` varchar(150) DEFAULT NULL,
  `purch_frieght_ledger_id` bigint(20) NOT NULL,
  `purch_tax2` double NOT NULL,
  `purch_tax3` double NOT NULL,
  `purch_agent` varchar(100) NOT NULL,
  `purch_flag` tinyint(4) NOT NULL DEFAULT '1',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`purch_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_purchase_return`
--

DROP TABLE IF EXISTS `erp_purchase_return`;
CREATE TABLE IF NOT EXISTS `erp_purchase_return` (
  `purchret_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `purchret_id2` varchar(100) NOT NULL,
  `purchret_branch_stock_id` bigint(20) NOT NULL,
  `purchret_no` varchar(50) DEFAULT NULL,
  `purchret_inv_no` varchar(50) DEFAULT NULL,
  `purchret_refno` bigint(20) DEFAULT NULL,
  `purchret_supp_id` bigint(20) NOT NULL,
  `purchret_supp_retno` bigint(20) DEFAULT NULL,
  `purchret_supp_inv_no` bigint(20) DEFAULT NULL,
  `purchret_type` tinyint(4) DEFAULT NULL,
  `purchret_pay_type` tinyint(4) DEFAULT NULL,
  `purchret_gd_id` bigint(20) NOT NULL,
  `purchret_date` date NOT NULL,
  `purchret_inv_date` date DEFAULT NULL,
  `purchret_amount` double NOT NULL,
  `purchret_cancelled_amount` double NOT NULL,
  `purchret_tax` double NOT NULL,
  `purchret_tax_ledger_id` bigint(20) NOT NULL,
  `purchret_discount` double DEFAULT NULL,
  `purchret_frieght` double DEFAULT NULL,
  `purchret_note` varchar(150) DEFAULT NULL,
  `purchret_frieght_ledger_id` bigint(20) NOT NULL,
  `purchret_tax2` double NOT NULL,
  `purchret_tax3` double NOT NULL,
  `purchret_agent` varchar(100) NOT NULL,
  `purchret_flag` tinyint(4) NOT NULL DEFAULT '1',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`purchret_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_purchase_return_sub`
--

DROP TABLE IF EXISTS `erp_purchase_return_sub`;
CREATE TABLE IF NOT EXISTS `erp_purchase_return_sub` (
  `purchretsub_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `purchretsub_purch_id` bigint(20) NOT NULL,
  `purchretsub_prd_id` bigint(20) NOT NULL,
  `purchretsub_stock_id` bigint(20) NOT NULL,
  `purchretsub_branch_stock_id` tinyint(4) NOT NULL,
  `purchretsub_batch_id` bigint(20) NOT NULL DEFAULT '0',
  `purchretsub_expiry` varchar(100) NOT NULL DEFAULT '0',
  `purchretsub_qty` double NOT NULL,
  `purchretsub_rate` double NOT NULL,
  `purchretsub_frieght` double DEFAULT NULL,
  `purchretsub_tax` double DEFAULT NULL,
  `purchretsub_tax_per` double DEFAULT NULL,
  `purchretsub_tax2` double NOT NULL,
  `purchretsub_tax2_per` double NOT NULL,
  `purchretsub_tax3` double NOT NULL,
  `purchretsub_tax3_per` double NOT NULL,
  `purchretsub_unit_id` bigint(20) NOT NULL,
  `purchretsub_gd_id` int(11) NOT NULL,
  `purchretsub_date` date NOT NULL,
  `purchretsub_flag` tinyint(4) NOT NULL DEFAULT '1',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`purchretsub_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_purchase_subs`
--

DROP TABLE IF EXISTS `erp_purchase_subs`;
CREATE TABLE IF NOT EXISTS `erp_purchase_subs` (
  `purchsub_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `purchsub_purch_id` bigint(20) NOT NULL,
  `purchsub_prd_id` bigint(20) NOT NULL,
  `purchsub_stock_id` bigint(20) NOT NULL,
  `purchsub_branch_stock_id` bigint(20) NOT NULL,
  `purchsub_batch_id` bigint(20) NOT NULL DEFAULT '0',
  `purchsub_expiry` varchar(100) NOT NULL DEFAULT '0',
  `purchsub_qty` double DEFAULT NULL,
  `purchsub_rate` double NOT NULL DEFAULT '0',
  `purchsub_frieght` double NOT NULL DEFAULT '0',
  `purchsub_tax` double NOT NULL DEFAULT '0',
  `purchsub_tax_per` double NOT NULL DEFAULT '0',
  `purchsub_tax2` double NOT NULL DEFAULT '0',
  `purchsub_tax2_per` double NOT NULL DEFAULT '0',
  `purchsub_tax3` double NOT NULL DEFAULT '0',
  `purchsub_tax3_per` double NOT NULL DEFAULT '0',
  `purchsub_unit_id` bigint(20) DEFAULT NULL,
  `purchsub_gd_qty` double NOT NULL DEFAULT '0',
  `purchsub_gd_id` int(11) DEFAULT NULL,
  `purchsub_date` date DEFAULT NULL,
  `purchsub_flag` tinyint(4) NOT NULL DEFAULT '1',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`purchsub_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_sales_agent`
--

DROP TABLE IF EXISTS `erp_sales_agent`;
CREATE TABLE IF NOT EXISTS `erp_sales_agent` (
  `sagent_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sagent_ledger_id` int(11) NOT NULL DEFAULT '0',
  `sagent_notes` varchar(200) NOT NULL DEFAULT '',
  `sagent_added_by` int(11) NOT NULL DEFAULT '1',
  `sagent_flags` int(11) NOT NULL DEFAULT '1',
  `branch_id` int(11) NOT NULL,
  `is_web` tinyint(4) NOT NULL DEFAULT '0',
  `godown_id` int(11) NOT NULL DEFAULT '0',
  `van_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`sagent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_sales_commission`
--

DROP TABLE IF EXISTS `erp_sales_commission`;
CREATE TABLE IF NOT EXISTS `erp_sales_commission` (
  `salescomm_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `salescomm_sales_inv_no` int(11) NOT NULL DEFAULT '0',
  `salescomm_ledger_id` int(11) NOT NULL DEFAULT '0',
  `salescomm_date` date NOT NULL DEFAULT '2000-01-01',
  `salescomm_amount` double NOT NULL DEFAULT '0',
  `salescomm_sales_amount` double NOT NULL DEFAULT '0',
  `salescomm_added_by` int(11) NOT NULL DEFAULT '1',
  `salescomm_flags` int(11) NOT NULL DEFAULT '1',
  `branch_id` int(11) NOT NULL,
  `is_web` tinyint(4) NOT NULL DEFAULT '0',
  `godown_id` int(11) NOT NULL DEFAULT '0',
  `van_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`salescomm_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_sales_due`
--

DROP TABLE IF EXISTS `erp_sales_due`;
CREATE TABLE IF NOT EXISTS `erp_sales_due` (
  `sales_pay_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sales_recp_no` varchar(30) NOT NULL,
  `salesdue_vch_no` int(11) NOT NULL DEFAULT '0',
  `salesdue_date` date NOT NULL DEFAULT '2000-01-01',
  `salesdue_timestamp` int(11) NOT NULL,
  `salesdue_ledger_id` int(11) NOT NULL,
  `salesdue_amount` double NOT NULL,
  `salesdue_multi` int(11) NOT NULL,
  `salesdue_added_by` int(11) NOT NULL,
  `salesdue_flags` int(11) NOT NULL,
  `salesdue_agent_ledger_id` int(11) NOT NULL,
  `sales_pay_type` bigint(20) NOT NULL,
  `sales_pay_txn_id` varchar(30) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `is_web` tinyint(4) NOT NULL DEFAULT '0',
  `godown_id` int(11) NOT NULL DEFAULT '0',
  `van_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sync_completed` tinyint(4) NOT NULL DEFAULT '1',
  `sync_code` varchar(256) NOT NULL,
  PRIMARY KEY (`sales_pay_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_sales_due_sub`
--

DROP TABLE IF EXISTS `erp_sales_due_sub`;
CREATE TABLE IF NOT EXISTS `erp_sales_due_sub` (
  `salesduesub_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `salesduesub_salesdue_id` bigint(20) NOT NULL,
  `salesduesub_inv_no` bigint(20) NOT NULL,
  `salesduesub_ledger_id` int(11) NOT NULL,
  `salesduesub_date` date NOT NULL DEFAULT '2000-01-01',
  `salesduesub_type` int(11) NOT NULL,
  `salesduesub_in` double NOT NULL,
  `salesduesub_out` double NOT NULL,
  `salesduesub_inv_amount` double NOT NULL,
  `salesduesub_inv_balance` double NOT NULL,
  `salesduesub_added_by` int(11) NOT NULL,
  `salesduesub_flags` int(11) NOT NULL,
  `salesduesub_agent_ledger_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `is_web` tinyint(4) NOT NULL DEFAULT '0',
  `godown_id` int(11) NOT NULL DEFAULT '0',
  `van_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sync_completed` tinyint(4) NOT NULL DEFAULT '1',
  `sync_code` varchar(256) NOT NULL,
  PRIMARY KEY (`salesduesub_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_sales_master`
--

DROP TABLE IF EXISTS `erp_sales_master`;
CREATE TABLE IF NOT EXISTS `erp_sales_master` (
  `sales_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sales_inv_no` bigint(20) NOT NULL,
  `sales_cust_type` tinyint(4) NOT NULL COMMENT '0 - Non registered\\n1 - Registered',
  `sales_cust_id` int(11) NOT NULL,
  `sales_cust_name` varchar(200) NOT NULL,
  `sales_cust_ph` varchar(45) NOT NULL,
  `sales_cust_address` varchar(500) NOT NULL,
  `sales_cust_tin` varchar(25) NOT NULL,
  `sales_timestamp` int(11) NOT NULL DEFAULT '0',
  `sales_date` date NOT NULL DEFAULT '2000-01-01',
  `sales_time` time NOT NULL DEFAULT '00:00:00',
  `sales_datetime` datetime NOT NULL DEFAULT '2000-01-01 00:00:00',
  `sales_total` double NOT NULL,
  `sales_discount` double NOT NULL,
  `sales_disc_promo` double NOT NULL,
  `sales_disc_loyalty` double NOT NULL,
  `sales_paid` double NOT NULL,
  `sales_balance` double NOT NULL,
  `sales_profit` double NOT NULL,
  `sales_rec_amount` double NOT NULL,
  `sales_tax` double NOT NULL,
  `sales_added_by` int(11) NOT NULL DEFAULT '1',
  `sales_flags` tinyint(4) NOT NULL DEFAULT '1',
  `sales_notes` varchar(250) NOT NULL,
  `sales_pay_type` int(11) NOT NULL DEFAULT '0' COMMENT '0 - Cash\\n1 - Credit',
  `sales_branch_inv` varchar(45) NOT NULL,
  `sales_branch_inv2` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `is_web` tinyint(4) NOT NULL DEFAULT '0',
  `godown_id` int(11) NOT NULL DEFAULT '0',
  `van_id` int(11) NOT NULL DEFAULT '0',
  `sales_agent_ledger_id` int(11) NOT NULL DEFAULT '0',
  `sales_acc_ledger_id` int(11) NOT NULL DEFAULT '0',
  `sales_order_no` int(11) NOT NULL DEFAULT '0',
  `sales_order_type` int(11) NOT NULL DEFAULT '0',
  `sales_godownsale_id` bigint(20) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sync_completed` tinyint(4) NOT NULL DEFAULT '1',
  `sync_code` varchar(256) NOT NULL,
  PRIMARY KEY (`sales_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_sales_return_master`
--

DROP TABLE IF EXISTS `erp_sales_return_master`;
CREATE TABLE IF NOT EXISTS `erp_sales_return_master` (
  `salesret_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `salesret_sales_inv_no` int(11) NOT NULL DEFAULT '0',
  `salesret_date` date NOT NULL DEFAULT '2000-01-01',
  `salesret_time` time NOT NULL DEFAULT '00:00:00',
  `salesdue_timestamp` int(11) NOT NULL,
  `salesdue_ledger_id` int(11) NOT NULL,
  `salesdue_amount` double NOT NULL,
  `salesret_cust_type` int(11) NOT NULL COMMENT '0 - New\\n1 - Registered',
  `salesret_cust_id` int(11) NOT NULL,
  `salesret_cust_name` varchar(200) NOT NULL,
  `salesret_cust_address` varchar(500) NOT NULL,
  `salesret_cust_ph` varchar(45) NOT NULL,
  `salesret_amount` double NOT NULL COMMENT 'total amount - cancelled discount',
  `salesret_discount` double NOT NULL COMMENT 'cancelled discount',
  `salesret_pay_type` int(11) NOT NULL COMMENT '0 - cash\\n1 - Credit',
  `salesret_added_by` int(11) NOT NULL DEFAULT '0',
  `salesret_flags` int(11) NOT NULL DEFAULT '1',
  `salesret_vch_no` int(11) NOT NULL DEFAULT '0',
  `salesret_notes` varchar(250) NOT NULL DEFAULT '',
  `salesret_sales_amount` double NOT NULL DEFAULT '0',
  `salesret_sales_discount` double NOT NULL DEFAULT '0',
  `salesret_service_amount` double NOT NULL DEFAULT '0',
  `salesret_service_discount` double NOT NULL DEFAULT '0',
  `salesret_servoid_id` int(11) NOT NULL DEFAULT '0',
  `salesret_misc_amount` double NOT NULL DEFAULT '0',
  `salesret_cust_tin` varchar(45) NOT NULL DEFAULT '',
  `salesret_tax` double NOT NULL DEFAULT '0',
  `salesret_service_tax` double NOT NULL DEFAULT '0',
  `salesret_tax_vch_no` varchar(45) NOT NULL DEFAULT '0',
  `salesret_godown_id` int(11) NOT NULL DEFAULT '0',
  `salesret_agent_ledger_id` int(11) NOT NULL DEFAULT '0',
  `salesret_pay_type2` int(11) NOT NULL DEFAULT '0',
  `salesret_acc_ledger_id` int(11) NOT NULL DEFAULT '0',
  `salesret_datetime` datetime NOT NULL DEFAULT '2000-01-01 00:00:00',
  `salesret_godown_ret` varchar(45) NOT NULL DEFAULT '',
  `branch_id` int(11) NOT NULL,
  `is_web` tinyint(4) NOT NULL DEFAULT '0',
  `godown_id` int(11) NOT NULL DEFAULT '0',
  `van_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sync_completed` tinyint(4) NOT NULL DEFAULT '1',
  `sync_code` varchar(256) NOT NULL,
  PRIMARY KEY (`salesret_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_sales_return_sub`
--

DROP TABLE IF EXISTS `erp_sales_return_sub`;
CREATE TABLE IF NOT EXISTS `erp_sales_return_sub` (
  `salesretsub_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `salesretsub_salesret_id` int(11) NOT NULL DEFAULT '0',
  `salesretsub_sales_inv_no` int(11) NOT NULL DEFAULT '0',
  `salesretsub_salesub_id` int(11) NOT NULL DEFAULT '0',
  `salesretsub_stock_id` int(11) NOT NULL DEFAULT '0',
  `salesretsub_prod_id` int(11) NOT NULL DEFAULT '0',
  `salesretsub_rate` double NOT NULL DEFAULT '0',
  `salesretsub_qty` double NOT NULL DEFAULT '0',
  `salesretsub_date` date NOT NULL DEFAULT '2000-01-01',
  `salesretsub_flags` int(11) NOT NULL DEFAULT '1',
  `salesretsub_unit_id` int(11) NOT NULL DEFAULT '1',
  `salesretsub_tax_per` double NOT NULL DEFAULT '0',
  `salesretsub_tax_rate` double NOT NULL DEFAULT '0',
  `salesretsub_godown_id` int(11) NOT NULL DEFAULT '0',
  `salesretsub_server_sync` int(11) NOT NULL DEFAULT '0',
  `salesretsub_sync_timestamp` bigint(20) NOT NULL DEFAULT '0',
  `salesretsub_server_timestamp` bigint(20) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `is_web` tinyint(4) NOT NULL DEFAULT '0',
  `godown_id` int(11) NOT NULL DEFAULT '0',
  `van_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sync_completed` tinyint(4) NOT NULL DEFAULT '1',
  `sync_code` varchar(256) NOT NULL,
  PRIMARY KEY (`salesretsub_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_sale_sub`
--

DROP TABLE IF EXISTS `erp_sale_sub`;
CREATE TABLE IF NOT EXISTS `erp_sale_sub` (
  `salesub_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `salesub_sales_inv_no` bigint(20) NOT NULL,
  `salesub_stock_id` int(11) NOT NULL,
  `salesub_branch_stock_id` int(11) NOT NULL,
  `salesub_prod_id` int(11) NOT NULL,
  `salesub_rate` double NOT NULL,
  `salesub_qty` double NOT NULL,
  `salesub_rem_qty` double NOT NULL COMMENT 'Remaining Qty after returning',
  `salesub_discount` double NOT NULL,
  `salesub_timestamp` int(11) NOT NULL,
  `salesub_date` date NOT NULL DEFAULT '2000-01-01',
  `salesub_serial` varchar(150) NOT NULL,
  `salesub_flags` int(11) NOT NULL,
  `salesub_profit` double NOT NULL,
  `salesub_unit_id` int(11) NOT NULL,
  `salesub_prate` double NOT NULL,
  `salesub_promo_id` int(11) NOT NULL,
  `salesub_promo_per` double NOT NULL,
  `salesub_promo_rate` double NOT NULL,
  `salesub_tax_per` double NOT NULL,
  `salesub_tax_rate` double NOT NULL,
  `branch_id` int(11) NOT NULL,
  `is_web` tinyint(4) NOT NULL DEFAULT '0',
  `godown_id` int(11) NOT NULL,
  `van_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sync_completed` tinyint(4) NOT NULL DEFAULT '1',
  `sync_code` varchar(256) NOT NULL,
  PRIMARY KEY (`salesub_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_sessions`
--

DROP TABLE IF EXISTS `erp_sessions`;
CREATE TABLE IF NOT EXISTS `erp_sessions` (
  `id` varchar(11) NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `ip_address` varchar(45) DEFAULT 'NILL',
  `user_agent` text NOT NULL,
  `payload` text NOT NULL,
  `last_activity` int(11) NOT NULL,
  UNIQUE KEY `erp_sessions_id_unique` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_staff`
--

DROP TABLE IF EXISTS `erp_staff`;
CREATE TABLE IF NOT EXISTS `erp_staff` (
  `staff_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `staff_laccount_no` bigint(20) NOT NULL COMMENT 'Ledger id',
  `staff_notes` varchar(250) NOT NULL,
  `staff_addedby` bigint(20) NOT NULL DEFAULT '1',
  `staff_add_date` date NOT NULL DEFAULT '1900-01-01',
  `staff_flags` tinyint(4) NOT NULL DEFAULT '1',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`staff_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_stock_batches`
--

DROP TABLE IF EXISTS `erp_stock_batches`;
CREATE TABLE IF NOT EXISTS `erp_stock_batches` (
  `sb_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sb_batch_code` varchar(50) NOT NULL,
  `sb_branch_stock_id` bigint(20) NOT NULL,
  `sb_prd_id` bigint(20) NOT NULL,
  `sb_stock_id` bigint(20) NOT NULL,
  `sb_quantity` bigint(20) NOT NULL,
  `sb_manufacture_date` date NOT NULL,
  `sb_expiry_date` date NOT NULL,
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`sb_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_stock_daily_updates`
--

DROP TABLE IF EXISTS `erp_stock_daily_updates`;
CREATE TABLE IF NOT EXISTS `erp_stock_daily_updates` (
  `sdu_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sdu_branch_stock_id` bigint(20) NOT NULL,
  `sdu_prd_id` bigint(20) NOT NULL,
  `sdu_stock_id` bigint(20) NOT NULL,
  `sdu_date` date NOT NULL DEFAULT '2019-12-19',
  `sdu_stock_quantity` double NOT NULL,
  `sdu_godown_id` double NOT NULL DEFAULT '0',
  `sdu_gd_id` bigint(20) NOT NULL DEFAULT '0',
  `sdu_batch_id` bigint(20) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`sdu_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_stock_unit_rates`
--

DROP TABLE IF EXISTS `erp_stock_unit_rates`;
CREATE TABLE IF NOT EXISTS `erp_stock_unit_rates` (
  `sur_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `branch_stock_id` bigint(20) NOT NULL,
  `sur_prd_id` bigint(20) NOT NULL,
  `sur_stock_id` bigint(20) NOT NULL,
  `sur_unit_id` bigint(20) NOT NULL,
  `sur_unit_rate` double NOT NULL,
  `sur_branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`sur_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_subcategories`
--

DROP TABLE IF EXISTS `erp_subcategories`;
CREATE TABLE IF NOT EXISTS `erp_subcategories` (
  `subcat_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `subcat_parent_category` int(11) NOT NULL,
  `subcat_name` varchar(150) DEFAULT NULL,
  `subcat_remarks` mediumtext,
  `subcat_flag` tinyint(4) NOT NULL DEFAULT '1',
  `subcat_pos` tinyint(4) NOT NULL DEFAULT '1',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`subcat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_suppliers`
--

DROP TABLE IF EXISTS `erp_suppliers`;
CREATE TABLE IF NOT EXISTS `erp_suppliers` (
  `supp_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `supp_name` varchar(250) NOT NULL,
  `supp_alias` varchar(100) DEFAULT NULL,
  `supp_code` varchar(50) NOT NULL,
  `supp_ledger_id` int(11) NOT NULL,
  `supp_branch_id` smallint(6) NOT NULL DEFAULT '0',
  `supp_address1` varchar(200) DEFAULT NULL,
  `supp_address2` varchar(200) DEFAULT NULL,
  `supp_zip` varchar(20) DEFAULT NULL,
  `supp_city` varchar(100) DEFAULT NULL,
  `supp_state` varchar(100) DEFAULT NULL,
  `supp_country` varchar(100) DEFAULT NULL,
  `supp_state_code` varchar(10) DEFAULT NULL,
  `supp_phone` varchar(20) DEFAULT NULL,
  `supp_mob` varchar(20) DEFAULT NULL,
  `supp_email` varchar(100) DEFAULT NULL,
  `supp_fax` varchar(50) DEFAULT NULL,
  `supp_notes` varchar(1000) DEFAULT NULL,
  `supp_flag` tinyint(4) NOT NULL DEFAULT '1',
  `supp_tin` varchar(25) DEFAULT NULL,
  `supp_contact` varchar(100) DEFAULT NULL,
  `supp_due` int(11) DEFAULT NULL,
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`supp_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_sync_sales_due`
--

DROP TABLE IF EXISTS `erp_sync_sales_due`;
CREATE TABLE IF NOT EXISTS `erp_sync_sales_due` (
  `sales_pay_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sales_recp_no` varchar(30) NOT NULL,
  `salesdue_vch_no` int(11) NOT NULL DEFAULT '0',
  `salesdue_date` date NOT NULL DEFAULT '2000-01-01',
  `salesdue_timestamp` int(11) NOT NULL,
  `salesdue_ledger_id` int(11) NOT NULL,
  `salesdue_amount` double NOT NULL,
  `salesdue_multi` int(11) NOT NULL,
  `salesdue_added_by` int(11) NOT NULL,
  `salesdue_flags` int(11) NOT NULL,
  `salesdue_agent_ledger_id` int(11) NOT NULL,
  `sales_pay_type` bigint(20) NOT NULL,
  `sales_pay_txn_id` varchar(30) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `godown_id` int(11) NOT NULL DEFAULT '0',
  `van_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`sales_pay_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_sync_sales_due_sub`
--

DROP TABLE IF EXISTS `erp_sync_sales_due_sub`;
CREATE TABLE IF NOT EXISTS `erp_sync_sales_due_sub` (
  `salesduesub_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `salesduesub_salesdue_id` bigint(20) NOT NULL,
  `salesduesub_inv_no` bigint(20) NOT NULL,
  `salesduesub_ledger_id` int(11) NOT NULL,
  `salesduesub_date` date NOT NULL DEFAULT '2000-01-01',
  `salesduesub_type` int(11) NOT NULL,
  `salesduesub_in` double NOT NULL,
  `salesduesub_out` double NOT NULL,
  `salesduesub_inv_amount` double NOT NULL,
  `salesduesub_inv_balance` double NOT NULL,
  `salesduesub_added_by` int(11) NOT NULL,
  `salesduesub_flags` int(11) NOT NULL,
  `salesduesub_agent_ledger_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `godown_id` int(11) NOT NULL DEFAULT '0',
  `van_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`salesduesub_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_sync_sales_master`
--

DROP TABLE IF EXISTS `erp_sync_sales_master`;
CREATE TABLE IF NOT EXISTS `erp_sync_sales_master` (
  `sales_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sales_inv_no` bigint(20) NOT NULL,
  `sales_cust_type` tinyint(4) NOT NULL COMMENT '0 - Non registered\\n1 - Registered',
  `sales_cust_id` int(11) NOT NULL,
  `sales_cust_name` varchar(200) NOT NULL,
  `sales_cust_ph` varchar(45) NOT NULL,
  `sales_cust_address` varchar(500) NOT NULL,
  `sales_cust_tin` varchar(25) NOT NULL,
  `sales_timestamp` int(11) NOT NULL DEFAULT '0',
  `sales_date` date NOT NULL DEFAULT '2000-01-01',
  `sales_time` time NOT NULL DEFAULT '00:00:00',
  `sales_datetime` datetime NOT NULL DEFAULT '2000-01-01 00:00:00',
  `sales_total` double NOT NULL,
  `sales_discount` double NOT NULL,
  `sales_disc_promo` double NOT NULL,
  `sales_disc_loyalty` double NOT NULL,
  `sales_paid` double NOT NULL,
  `sales_balance` double NOT NULL,
  `sales_profit` double NOT NULL,
  `sales_rec_amount` double NOT NULL,
  `sales_tax` double NOT NULL,
  `sales_added_by` int(11) NOT NULL DEFAULT '1',
  `sales_flags` tinyint(4) NOT NULL DEFAULT '1',
  `sales_notes` varchar(250) NOT NULL,
  `sales_pay_type` int(11) NOT NULL DEFAULT '0' COMMENT '0 - Cash\\n1 - Credit',
  `sales_branch_inv` varchar(45) NOT NULL,
  `sales_branch_inv2` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `godown_id` int(11) NOT NULL DEFAULT '0',
  `van_id` int(11) NOT NULL DEFAULT '0',
  `sales_agent_ledger_id` int(11) NOT NULL DEFAULT '0',
  `sales_acc_ledger_id` int(11) NOT NULL DEFAULT '0',
  `sales_order_no` int(11) NOT NULL DEFAULT '0',
  `sales_order_type` int(11) NOT NULL DEFAULT '0',
  `sales_godownsale_id` bigint(20) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`sales_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_sync_sales_return_master`
--

DROP TABLE IF EXISTS `erp_sync_sales_return_master`;
CREATE TABLE IF NOT EXISTS `erp_sync_sales_return_master` (
  `salesret_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `salesret_sales_inv_no` int(11) NOT NULL DEFAULT '0',
  `salesret_date` date NOT NULL DEFAULT '2000-01-01',
  `salesret_time` time NOT NULL DEFAULT '00:00:00',
  `salesdue_timestamp` int(11) NOT NULL,
  `salesdue_ledger_id` int(11) NOT NULL,
  `salesdue_amount` double NOT NULL,
  `salesret_cust_type` int(11) NOT NULL COMMENT '0 - New\\n1 - Registered',
  `salesret_cust_id` int(11) NOT NULL,
  `salesret_cust_name` varchar(200) NOT NULL,
  `salesret_cust_address` varchar(500) NOT NULL,
  `salesret_cust_ph` varchar(45) NOT NULL,
  `salesret_amount` double NOT NULL COMMENT 'total amount - cancelled discount',
  `salesret_discount` double NOT NULL COMMENT 'cancelled discount',
  `salesret_pay_type` int(11) NOT NULL COMMENT '0 - cash\\n1 - Credit',
  `salesret_added_by` int(11) NOT NULL DEFAULT '0',
  `salesret_flags` int(11) NOT NULL DEFAULT '1',
  `salesret_vch_no` int(11) NOT NULL DEFAULT '0',
  `salesret_notes` varchar(250) NOT NULL DEFAULT '',
  `salesret_sales_amount` double NOT NULL DEFAULT '0',
  `salesret_sales_discount` double NOT NULL DEFAULT '0',
  `salesret_service_amount` double NOT NULL DEFAULT '0',
  `salesret_service_discount` double NOT NULL DEFAULT '0',
  `salesret_servoid_id` int(11) NOT NULL DEFAULT '0',
  `salesret_misc_amount` double NOT NULL DEFAULT '0',
  `salesret_cust_tin` varchar(45) NOT NULL DEFAULT '',
  `salesret_tax` double NOT NULL DEFAULT '0',
  `salesret_service_tax` double NOT NULL DEFAULT '0',
  `salesret_tax_vch_no` varchar(45) NOT NULL DEFAULT '0',
  `salesret_godown_id` int(11) NOT NULL DEFAULT '0',
  `salesret_agent_ledger_id` int(11) NOT NULL DEFAULT '0',
  `salesret_pay_type2` int(11) NOT NULL DEFAULT '0',
  `salesret_acc_ledger_id` int(11) NOT NULL DEFAULT '0',
  `salesret_datetime` datetime NOT NULL DEFAULT '2000-01-01 00:00:00',
  `salesret_godown_ret` varchar(45) NOT NULL DEFAULT '',
  `branch_id` int(11) NOT NULL,
  `godown_id` int(11) NOT NULL DEFAULT '0',
  `van_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`salesret_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_sync_sales_return_sub`
--

DROP TABLE IF EXISTS `erp_sync_sales_return_sub`;
CREATE TABLE IF NOT EXISTS `erp_sync_sales_return_sub` (
  `salesretsub_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `salesretsub_salesret_id` int(11) NOT NULL DEFAULT '0',
  `salesretsub_sales_inv_no` int(11) NOT NULL DEFAULT '0',
  `salesretsub_salesub_id` int(11) NOT NULL DEFAULT '0',
  `salesretsub_stock_id` int(11) NOT NULL DEFAULT '0',
  `salesretsub_prod_id` int(11) NOT NULL DEFAULT '0',
  `salesretsub_rate` double NOT NULL DEFAULT '0',
  `salesretsub_qty` double NOT NULL DEFAULT '0',
  `salesretsub_date` date NOT NULL DEFAULT '2000-01-01',
  `salesretsub_flags` int(11) NOT NULL DEFAULT '1',
  `salesretsub_unit_id` int(11) NOT NULL DEFAULT '1',
  `salesretsub_tax_per` double NOT NULL DEFAULT '0',
  `salesretsub_tax_rate` double NOT NULL DEFAULT '0',
  `salesretsub_godown_id` int(11) NOT NULL DEFAULT '0',
  `salesretsub_server_sync` int(11) NOT NULL DEFAULT '0',
  `salesretsub_sync_timestamp` bigint(20) NOT NULL DEFAULT '0',
  `salesretsub_server_timestamp` bigint(20) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `godown_id` int(11) NOT NULL DEFAULT '0',
  `van_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`salesretsub_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_sync_sales_sub`
--

DROP TABLE IF EXISTS `erp_sync_sales_sub`;
CREATE TABLE IF NOT EXISTS `erp_sync_sales_sub` (
  `salesub_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `salesub_sales_inv_no` bigint(20) NOT NULL,
  `salesub_stock_id` int(11) NOT NULL,
  `salesub_branch_stock_id` int(11) NOT NULL,
  `salesub_prod_id` int(11) NOT NULL,
  `salesub_rate` double NOT NULL,
  `salesub_qty` double NOT NULL,
  `salesub_rem_qty` double NOT NULL COMMENT 'Remaining Qty after returning',
  `salesub_discount` double NOT NULL,
  `salesub_timestamp` int(11) NOT NULL,
  `salesub_date` date NOT NULL DEFAULT '2000-01-01',
  `salesub_serial` varchar(150) NOT NULL,
  `salesub_flags` int(11) NOT NULL,
  `salesub_profit` double NOT NULL,
  `salesub_unit_id` int(11) NOT NULL,
  `salesub_prate` double NOT NULL,
  `salesub_promo_id` int(11) NOT NULL,
  `salesub_promo_per` double NOT NULL,
  `salesub_promo_rate` double NOT NULL,
  `salesub_tax_per` double NOT NULL,
  `salesub_tax_rate` double NOT NULL,
  `branch_id` int(11) NOT NULL,
  `godown_id` int(11) NOT NULL,
  `van_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`salesub_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_tmp_purchases`
--

DROP TABLE IF EXISTS `erp_tmp_purchases`;
CREATE TABLE IF NOT EXISTS `erp_tmp_purchases` (
  `purch_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `purch_id2` varchar(100) NOT NULL,
  `purch_branch_id` bigint(20) NOT NULL,
  `purch_no` varchar(50) DEFAULT NULL,
  `purch_inv_no` varchar(50) DEFAULT NULL,
  `purch_supp_id` bigint(20) DEFAULT NULL,
  `purch_ord_no` bigint(20) DEFAULT NULL,
  `purch_type` tinyint(4) DEFAULT NULL,
  `purch_pay_type` tinyint(4) DEFAULT NULL,
  `purch_date` date DEFAULT NULL,
  `purch_inv_date` date DEFAULT NULL,
  `purch_note` varchar(150) DEFAULT NULL,
  `purch_amount` double DEFAULT NULL,
  `purch_tax` double DEFAULT NULL,
  `purch_tax_ledger_id` bigint(20) DEFAULT NULL,
  `purch_discount` double DEFAULT NULL,
  `purch_frieght` double DEFAULT NULL,
  `purch_frieght_ledger_id` bigint(20) DEFAULT NULL,
  `purch_tax2` double DEFAULT NULL,
  `purch_tax3` double DEFAULT NULL,
  `purch_agent` varchar(100) DEFAULT NULL,
  `temp_purch_json` varchar(100) DEFAULT NULL,
  `purch_flag` tinyint(4) NOT NULL DEFAULT '1',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`purch_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_tmp_purchase_subs`
--

DROP TABLE IF EXISTS `erp_tmp_purchase_subs`;
CREATE TABLE IF NOT EXISTS `erp_tmp_purchase_subs` (
  `purchsub_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `purchsub_purch_id` bigint(20) NOT NULL,
  `purchsub_prd_id` bigint(20) NOT NULL,
  `purchsub_stock_id` bigint(20) NOT NULL,
  `purchsub_branch_id` tinyint(4) NOT NULL,
  `purchsub_batch_id` bigint(20) NOT NULL DEFAULT '0',
  `purchsub_expiry` varchar(100) NOT NULL DEFAULT '0',
  `purchsub_qty` double NOT NULL,
  `purchsub_rate` double NOT NULL,
  `purchsub_frieght` double NOT NULL,
  `purchsub_tax` double NOT NULL,
  `purchsub_tax_per` double NOT NULL,
  `purchsub_tax2` double NOT NULL,
  `purchsub_tax2_per` double NOT NULL,
  `purchsub_tax3` double NOT NULL,
  `purchsub_tax3_per` double NOT NULL,
  `purchsub_unit_id` bigint(20) NOT NULL,
  `purchsub_gd_qty` double NOT NULL,
  `purchsub_gd_id` int(11) NOT NULL,
  `purchsub_date` date NOT NULL,
  `purchsub_flag` tinyint(4) NOT NULL DEFAULT '1',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`purchsub_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_units`
--

DROP TABLE IF EXISTS `erp_units`;
CREATE TABLE IF NOT EXISTS `erp_units` (
  `unit_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `unit_name` varchar(40) NOT NULL,
  `unit_code` char(15) DEFAULT NULL,
  `unit_display` char(25) DEFAULT NULL,
  `unit_type` int(11) NOT NULL DEFAULT '1' COMMENT '0 - derived,1 - base ',
  `unit_base_id` int(11) NOT NULL DEFAULT '0' COMMENT 'unit_id of base unit',
  `unit_base_qty` int(11) DEFAULT '1' COMMENT 'base qty/unit',
  `unit_remarks` mediumtext,
  `unit_flag` tinyint(4) NOT NULL DEFAULT '1',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`unit_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_users`
--

DROP TABLE IF EXISTS `erp_users`;
CREATE TABLE IF NOT EXISTS `erp_users` (
  `usr_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `usr_type` int(11) NOT NULL,
  `usr_name` varchar(250) NOT NULL,
  `usr_username` varchar(250) NOT NULL,
  `pass_md5` varchar(250) NOT NULL,
  `usr_password` mediumtext NOT NULL,
  `usr_nickname` varchar(50) DEFAULT NULL,
  `usr_phone` varchar(30) DEFAULT NULL,
  `usr_mobile` varchar(30) DEFAULT NULL,
  `usr_email` varchar(80) DEFAULT NULL,
  `usr_address` mediumtext,
  `photo` varchar(250) NOT NULL DEFAULT 'default.png',
  `usr_active` tinyint(4) NOT NULL DEFAULT '1',
  `email_verified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `remember_token` varchar(100) DEFAULT NULL,
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`usr_id`),
  UNIQUE KEY `erp_users_usr_username_unique` (`usr_username`),
  UNIQUE KEY `erp_users_usr_email_unique` (`usr_email`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `erp_users`
--

INSERT INTO `erp_users` (`usr_id`, `usr_type`, `usr_name`, `usr_username`, `pass_md5`, `usr_password`, `usr_nickname`, `usr_phone`, `usr_mobile`, `usr_email`, `usr_address`, `photo`, `usr_active`, `email_verified_at`, `remember_token`, `company_id`, `branch_id`, `server_sync_flag`, `server_sync_time`, `local_sync_time`, `created_at`, `updated_at`) VALUES
(1, 1, 'Fujishka', 'Fujishka', 'e10adc3949ba59abbe56e057f20f883e', '$2y$10$A2wgmHzb4CYGMGvsibJOJOA8UxOJ2gj1x8ocBy3lxP8yiasJDvX5S', NULL, NULL, NULL, 'admin@fujishka.com', NULL, 'default.png', 1, '2019-12-19 09:20:58', NULL, 0, 0, 0, 0, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `erp_user_types`
--

DROP TABLE IF EXISTS `erp_user_types`;
CREATE TABLE IF NOT EXISTS `erp_user_types` (
  `type_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_type` varchar(50) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `erp_user_types`
--

INSERT INTO `erp_user_types` (`type_id`, `user_type`, `created_at`, `updated_at`) VALUES
(1, 'ADMIN', NULL, NULL),
(2, 'BRANCH ADMIN', NULL, NULL),
(3, 'BRANCH USER', NULL, NULL),
(4, 'VAN USER', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `erp_van`
--

DROP TABLE IF EXISTS `erp_van`;
CREATE TABLE IF NOT EXISTS `erp_van` (
  `van_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `van_vanline_id` bigint(20) NOT NULL,
  `van_name` varchar(100) NOT NULL,
  `van_code` varchar(10) NOT NULL,
  `van_reg_no` varchar(10) NOT NULL,
  `van_contact_person` varchar(100) NOT NULL,
  `van_contact_phone` varchar(25) DEFAULT NULL,
  `van_contact_mob` varchar(25) NOT NULL,
  `van_contact_email` varchar(30) NOT NULL,
  `van_password` varchar(150) NOT NULL,
  `van_contact_address` varchar(1000) DEFAULT NULL,
  `van_desc` varchar(1000) DEFAULT NULL,
  `van_ledger_id` bigint(20) NOT NULL,
  `photo` varchar(250) NOT NULL DEFAULT 'default.png',
  `van_added_by` bigint(20) NOT NULL,
  `van_type` tinyint(4) NOT NULL DEFAULT '1',
  `van_flag` tinyint(4) NOT NULL DEFAULT '1',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`van_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_vanline`
--

DROP TABLE IF EXISTS `erp_vanline`;
CREATE TABLE IF NOT EXISTS `erp_vanline` (
  `vanline_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `vanline_name` varchar(250) NOT NULL DEFAULT '0',
  `vanline_desc` varchar(100) NOT NULL DEFAULT '0',
  `vanline_flag` tinyint(4) NOT NULL DEFAULT '1',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`vanline_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_van_daily_stocks`
--

DROP TABLE IF EXISTS `erp_van_daily_stocks`;
CREATE TABLE IF NOT EXISTS `erp_van_daily_stocks` (
  `vds_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `vds_branch_stock_id` bigint(20) NOT NULL,
  `vds_prd_id` bigint(20) NOT NULL,
  `vds_stock_id` bigint(20) NOT NULL,
  `vds_van_id` bigint(20) NOT NULL,
  `vds_date` date NOT NULL DEFAULT '2019-12-19',
  `vds_stock_quantity` double NOT NULL,
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`vds_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_van_stock`
--

DROP TABLE IF EXISTS `erp_van_stock`;
CREATE TABLE IF NOT EXISTS `erp_van_stock` (
  `vanstock_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `vanstock_prod_id` bigint(20) NOT NULL,
  `vanstock_stock_id` bigint(20) NOT NULL,
  `vanstock_branch_stock_id` bigint(20) NOT NULL,
  `vanstock_van_id` bigint(20) NOT NULL,
  `vanstock_qty` double NOT NULL DEFAULT '0',
  `vanstock_last_tran_rate` double NOT NULL DEFAULT '0',
  `vanstock_avg_tran_rate` double NOT NULL DEFAULT '0',
  `vanstock_flags` tinyint(4) NOT NULL DEFAULT '1',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`vanstock_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_van_transfer`
--

DROP TABLE IF EXISTS `erp_van_transfer`;
CREATE TABLE IF NOT EXISTS `erp_van_transfer` (
  `vantran_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `vantran_van_id` bigint(20) NOT NULL,
  `vantran_godown_id` bigint(20) NOT NULL DEFAULT '0',
  `vantran_price` double NOT NULL,
  `vantran_purch_price` double NOT NULL,
  `vantran_date` date NOT NULL,
  `vantran_notes` varchar(1000) NOT NULL,
  `vantran_van_ledger_id` bigint(20) NOT NULL,
  `vantran_added_by` bigint(20) NOT NULL,
  `vantran_type` tinyint(4) NOT NULL DEFAULT '0',
  `vantran_flags` tinyint(4) NOT NULL DEFAULT '1',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`vantran_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_van_transfer_return`
--

DROP TABLE IF EXISTS `erp_van_transfer_return`;
CREATE TABLE IF NOT EXISTS `erp_van_transfer_return` (
  `vantranret_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `vantranret_vantran_id` bigint(20) NOT NULL DEFAULT '0',
  `vantranret_van_id` bigint(20) NOT NULL,
  `vantranret_godown_id` bigint(20) NOT NULL DEFAULT '0',
  `vantranret_price` double NOT NULL,
  `vantranret_purch_price` double NOT NULL,
  `vantranret_date` date NOT NULL,
  `vantranret_notes` varchar(1000) NOT NULL,
  `vantranret_van_ledger_id` bigint(20) NOT NULL,
  `vantranret_added_by` bigint(20) NOT NULL,
  `vantranret_type` tinyint(4) NOT NULL DEFAULT '0',
  `vantranret_flag` tinyint(4) NOT NULL DEFAULT '1',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`vantranret_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_van_transfer_return_sub`
--

DROP TABLE IF EXISTS `erp_van_transfer_return_sub`;
CREATE TABLE IF NOT EXISTS `erp_van_transfer_return_sub` (
  `vantranretsub_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `vantransub_vantran_id` bigint(20) NOT NULL,
  `vantranretsub_vantran_id` bigint(20) NOT NULL,
  `vantranretsub_vantranret_id` bigint(20) NOT NULL,
  `vantranretsub_van_id` bigint(20) NOT NULL,
  `vantranretsub_godown_id` bigint(20) NOT NULL DEFAULT '0',
  `vantranretsub_rate` double NOT NULL,
  `vantranretsub_purch_rate` double NOT NULL,
  `vantranretsub_qty` double NOT NULL,
  `vantranretsub_prod_id` bigint(20) NOT NULL,
  `vantranretsub_stock_id` bigint(20) NOT NULL,
  `vantransub_branch_stock_id` bigint(20) NOT NULL,
  `vantranretsub_unit_id` bigint(20) NOT NULL,
  `vantranretsub_date` date NOT NULL,
  `vantranretsub_added_by` bigint(20) NOT NULL,
  `vantranretsub_flags` tinyint(4) NOT NULL DEFAULT '1',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`vantranretsub_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_van_transfer_sub`
--

DROP TABLE IF EXISTS `erp_van_transfer_sub`;
CREATE TABLE IF NOT EXISTS `erp_van_transfer_sub` (
  `vantransub_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `vantransub_vantran_id` bigint(20) NOT NULL,
  `vantransub_van_id` bigint(20) NOT NULL,
  `vantransub_godown_id` bigint(20) NOT NULL DEFAULT '0',
  `vantransub_rate` double NOT NULL,
  `vantransub_purch_rate` double NOT NULL,
  `vantransub_qty` double NOT NULL,
  `vantransub_prod_id` bigint(20) NOT NULL,
  `vantransub_stock_id` bigint(20) NOT NULL,
  `vantransub_branch_stock_id` bigint(20) NOT NULL,
  `vantransub_unit_id` bigint(20) NOT NULL,
  `vantransub_date` date NOT NULL,
  `vantransub_added_by` bigint(20) NOT NULL,
  `vantransub_flags` tinyint(4) NOT NULL DEFAULT '1',
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`vantransub_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_van_users`
--

DROP TABLE IF EXISTS `erp_van_users`;
CREATE TABLE IF NOT EXISTS `erp_van_users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `van_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `van_user_flag` tinyint(4) NOT NULL DEFAULT '1',
  `server_sync_flag` tinyint(4) NOT NULL DEFAULT '0',
  `server_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `local_sync_time` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `erp_voucher_type`
--

DROP TABLE IF EXISTS `erp_voucher_type`;
CREATE TABLE IF NOT EXISTS `erp_voucher_type` (
  `vchtype_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `vchtype_name` varchar(50) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`vchtype_id`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `erp_voucher_type`
--

INSERT INTO `erp_voucher_type` (`vchtype_id`, `vchtype_name`, `created_at`, `updated_at`) VALUES
(1, 'Payment', NULL, NULL),
(2, 'Reciept', NULL, NULL),
(3, 'Contra', NULL, NULL),
(4, 'Journal', NULL, NULL),
(5, 'Sales', NULL, NULL),
(6, 'Purchase', NULL, NULL),
(7, 'Debit note', NULL, NULL),
(8, 'Credit note', NULL, NULL),
(9, 'Purch Return', NULL, NULL),
(10, 'Sales Return', NULL, NULL),
(11, 'Service Charge', NULL, NULL),
(12, 'Void Service', NULL, NULL),
(13, 'Production', NULL, NULL),
(14, 'Void Production', NULL, NULL),
(15, 'Godown Transfer', NULL, NULL),
(16, 'Stock Receipt Return', NULL, NULL),
(17, 'Stock Receipt', NULL, NULL),
(18, 'Stock Transfer Return', NULL, NULL),
(19, 'Stock Transfer', NULL, NULL),
(20, 'Dividend Allocation', NULL, NULL),
(21, 'Starting Balance', NULL, NULL),
(22, 'Opening Stock', NULL, NULL),
(23, 'Invoice Due Receipt', NULL, NULL),
(24, 'Input Tax', NULL, NULL),
(25, 'Output Tax', NULL, NULL),
(26, 'Van transfer(GV)', NULL, NULL),
(27, 'Van transfer Return(VG)', NULL, NULL),
(28, 'Void  transfer(VG)', NULL, NULL),
(29, 'Void transfer Return(GV)', NULL, NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
