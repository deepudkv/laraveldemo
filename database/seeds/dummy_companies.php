<?php

use Illuminate\Database\Seeder;

class dummy_companies extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('companies')->insert([
            'cmp_name' => 'Fujishka',
            'cmp_addr' => 'Calicut Mall, 3rd Floor',
            'cpm_code' => 'fjk',
            'cmp_sub_domain' => ''
        ]);
    }
}
