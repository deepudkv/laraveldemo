<?php

use Illuminate\Database\Seeder;

class PriceGroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('price_group')->insert([
            'group_name' => 'Default',
            'group_descp' => 'Default',
            'group_descp' => 'Default',
            'branch_id'=>0,
            'server_sync_time'=> substr(microtime(), 2, 6)
        ]);
    }
}
