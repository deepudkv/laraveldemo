<?php
use Illuminate\Database\Seeder;
class StagingSeeder extends Seeder {
  public function run() {
      
     // Connect to production database
     $live_database = DB::connection('mysql');
     // Get table data from production
     foreach($live_database->table('tbl_category')->get() as $data){
        // Save data to staging database - default db connection

        $insert= ['cat_name'=>$data['cat_name'],'cat_remarks'=>$data['cat_remarks'],'cat_flag'=>1,'cat_pos'=>$data['cat_pos']] ;

        DB::table('erp_categories')->insert((array) $data);
     }
     // Get table_2 data from production
    //  foreach($live_database->table('table_2_name')->get() as $data){
     
    //     DB::table('table_2_name')->insert((array) $data);
    //  }
  }
}