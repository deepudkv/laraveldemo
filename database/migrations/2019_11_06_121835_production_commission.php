<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProductionCommission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('production_comm', function (Blueprint $table) {
            $table->bigIncrements('prdncomm_id');
            $table->integer('prdncomm_prdn_id');
            $table->integer('prdncomm_prodform_id')->default(0);
            $table->integer('prdncomm_laccount_no')->default(0)->comment = 'staff ledger account no';;
            $table->double('prdncomm_in');
            $table->double('prdncomm_out');
            $table->timestamp('prdncomm_date');
            $table->integer('branch_id')->default(0);
            $table->tinyInteger('prdncomm_flag')->default(1);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->nullable();
            $table->bigInteger('local_sync_time')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
