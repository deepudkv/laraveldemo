<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OpeningStockLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { Schema::create('opening_stock_log', function (Blueprint $table) {
        $table->bigIncrements('opstklog_id');
        $table->bigInteger('opstklog_branch_stock_id');
        $table->bigInteger('opstklog_prd_id');
        $table->bigInteger('opstklog_stock_id');
        $table->date('opstklog_date')->default(date("Y-m-d"));
        $table->double('opstklog_prate')->default(0);
        $table->double('opstklog_srate')->default(0);
        $table->double('opstklog_stock_quantity_add')->default(0);
        $table->double('opstklog_stock_quantity_rem')->default(0);
        $table->tinyInteger('opstklog_type')->default(0)->comment('Add 0 | Remove 1');
        $table->tinyInteger('opstklog_status')->default(1);
        $table->bigInteger('opstklog_unit_id')->default(0);
        $table->bigInteger('opstklog_added_by')->default(0);
        

        $table->bigInteger('opstklog_gd_id')->default(0);
        $table->bigInteger('opstklog_batch_id')->default(0);

        $table->integer('branch_id')->default(0);
        $table->tinyInteger('server_sync_flag')->default(0);
        $table->bigInteger('server_sync_time')->default(0);
        $table->bigInteger('local_sync_time')->default(0);
        $table->timestamps();
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('opening_stock_log');
    }
}
