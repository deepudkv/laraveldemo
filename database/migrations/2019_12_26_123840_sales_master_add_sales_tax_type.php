<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SalesMasterAddSalesTaxType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sales_master', function (Blueprint $table) {
            $table->integer('sales_tax_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales_master', function (Blueprint $table) {
            $table->dropColumn([
                'sales_tax_type',
            ]);
        });
    }
}
