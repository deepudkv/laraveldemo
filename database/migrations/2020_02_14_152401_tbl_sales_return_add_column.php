<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblSalesReturnAddColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sales_return_master', function (Blueprint $table) {
            $table->string('salesret_branch_ret_no',50);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales_return_master', function (Blueprint $table) {
            $table->dropColumn([
                'salesret_branch_ret_no',
            ]);
        });
    }
}
