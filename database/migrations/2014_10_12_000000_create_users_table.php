<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        

        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('usr_id');
            $table->integer('usr_type');
            $table->string('usr_name',250);
            $table->string('usr_username',250)->unique();
            $table->mediumText('usr_password');
            $table->string('usr_nickname',50)->nullable();
            $table->string('usr_phone',30)->nullable();
            $table->string('usr_mobile',30)->nullable();
            $table->string('usr_email',80)->unique()->nullable();
            $table->mediumText('usr_address',250)->nullable();
            
            $table->tinyInteger('usr_active')->default(1);
            $table->timestamp('email_verified_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->rememberToken();

            $table->integer('company_id')->default(0);
            $table->integer('branch_id')->default(0);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });

    }

   
   
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
