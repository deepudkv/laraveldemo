<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockTransferSubTable  extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_transfer_sub', function (Blueprint $table) {

            $table->bigIncrements('stocktrsub_num');
            $table->bigInteger('stocktrsub_id');
            $table->bigInteger('stocktrsub_stocktr_id');
            $table->bigInteger('stocktrsub_prd_id');
            $table->bigInteger('stocktrsub_gd_id');
            $table->bigInteger('stocktrsub_branch_stock_id');
            $table->double('stocktrsub_qty');
            $table->double('stocktrsub_rate');
            $table->double('stocktrsub_prate');
            $table->date('stocktrsub_date');


            $table->bigInteger('stocktrsub_unit_id');
            $table->integer('branch_id');
            $table->integer('stocktrsub_to')->comment ='to branch id';

            $table->tinyInteger('stocktrsub_accepted')->default(0)->comment ='0-pending,1-accepted,2-rejected';
            $table->integer('stocktrsub_added_by');
            $table->tinyInteger('stocktrsub_flags')->default(1);

            
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
