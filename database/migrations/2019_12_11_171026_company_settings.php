<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CompanySettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_settings', function (Blueprint $table) {
            $table->bigIncrements('cmp_id');
            $table->string('cmp_name', 150);
            $table->string('cmp_display_name', 150);
            $table->string('cmp_code', 50);
            $table->string('cmp_address', 1000)->nullable();
            $table->string('cmp_address2', 1000)->nullable();
            $table->string('cmp_phone', 25)->nullable();
            $table->string('cmp_mob', 25)->nullable();
            $table->string('cmp_fax', 40)->nullable();
            $table->string('cmp_email', 100)->nullable();
            $table->string('cmp_tin', 20)->nullable();
            $table->date('cmp_finyear_start', 20);
            $table->date('cmp_finyear_end', 20);
            $table->string('cmp_reg_no', 100)->nullable();

            $table->string('cmp_notes', 250)->nullable(0);
            $table->smallInteger('cmp_default')->default(0);
            $table->smallInteger('cmp_flags')->default(1);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });

        $next_year = date('Y') + 1;
        $insert = array(
            'cmp_name' => 'Fujishka',
            'cmp_display_name' => 'Fujishka Pvt Ltd',
            'cmp_code' => 'fjk',
            'cmp_email' => 'info@fujishka.com',
            'cmp_finyear_start' => date('Y') . "-04-01",
            'cmp_finyear_end' => $next_year . "-03-31",
            'server_sync_time' => date('ymdHis') . substr(microtime(), 2, 6),
        );

        DB::table('company_settings')->insert($insert);

        // DB::table('companies')->insert([
        //     'cmp_name' => 'Fujishka',
        //     'cmp_addr' => 'Calicut Mall, 3rd Floor',
        //     'cpm_code' => 'fjk',
        //     'cmp_sub_domain' => ''
        // ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
