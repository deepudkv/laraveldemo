<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReceiptSubAddSubNo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_receipt_sub', function (Blueprint $table) {
            $table->bigInteger('vat_catgeory_id')->default(0);
            $table->double('recsub_ttl_amount');
            $table->bigInteger('recsub_no')->default(0);
            $table->tinyInteger('recsub_vat_inc')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acc_receipt_sub', function (Blueprint $table) {
            $table->dropColumn([
                'vat_catgeory_id',
                'recsub_ttl_amount',
                'recsub_no',
                'recsub_vat_inc'
            ]);
        });
    }
}
