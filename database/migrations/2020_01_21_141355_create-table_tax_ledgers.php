<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTaxLedgers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tax_ledgers', function (Blueprint $table) {

            $table->bigIncrements('taxled_auto_id');
            $table->integer('taxled_id');
            $table->integer('taxled_taxcat_id');
            $table->integer('taxled_branch_id');
            $table->integer('taxled_ledger_id');
            $table->integer('taxled_taxcatsub_id');

            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tax_ledgers');
    }
}
