<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    // 2019_06_11_062713_create_sessions_table
    public function up()
    {
        Schema::create('sessions', function (Blueprint $table) {
            $table->string('id',11)->unique();
            $table->unsignedBigInteger('user_id')->default(0);
            $table->string('ip_address', 45)->default('NILL')->nullable();
            $table->text('user_agent');
            $table->text('payload');
            $table->integer('last_activity');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sessions');
    }
}
