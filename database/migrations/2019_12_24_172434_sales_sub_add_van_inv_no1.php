<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SalesSubAddVanInvNo1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sale_sub', function (Blueprint $table) {
            $table->string('van_inv_no', 256);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sale_sub', function (Blueprint $table) {
            $table->dropColumn([
                'van_inv_no',
            ]);
        });
    }
}
