<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddId2ColumnSalesSub extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sale_sub', function (Blueprint $table) {
            $table->bigInteger('salesub_id2');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sale_sub', function (Blueprint $table) {
            $table->dropColumn([
                'salesub_id2',
            ]);
        });
    }
}
