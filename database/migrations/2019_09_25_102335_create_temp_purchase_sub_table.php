<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempPurchaseSubTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmp_purchase_subs', function (Blueprint $table) {
            $table->bigIncrements('purchsub_id');
            $table->biginteger('purchsub_purch_id');
            $table->biginteger('purchsub_prd_id');
            $table->biginteger('purchsub_stock_id');
            $table->tinyinteger('purchsub_branch_id');
            $table->biginteger('purchsub_batch_id')->default(0);
            $table->string('purchsub_expiry',100)->default(0);
            $table->double('purchsub_qty');
            $table->double('purchsub_rate');
            $table->double('purchsub_frieght');
            $table->double('purchsub_tax');
            $table->double('purchsub_tax_per');
            $table->double('purchsub_tax2');
            $table->double('purchsub_tax2_per');
            $table->double('purchsub_tax3');
            $table->double('purchsub_tax3_per');
            $table->biginteger('purchsub_unit_id');
            $table->double('purchsub_gd_qty');
            $table->integer('purchsub_gd_id');
            $table->date('purchsub_date');
            $table->tinyInteger('purchsub_flag')->default(1);
            $table->integer('company_id')->default(0);
            $table->integer('branch_id')->default(0);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tmp_purchase_subs');
    }
}


// ALTER TABLE `erp_purchases` CHANGE `purch_id2` `purch_id2` VARCHAR(100) NOT NULL;