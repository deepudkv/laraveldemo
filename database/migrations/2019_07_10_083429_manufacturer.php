<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Manufacturer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('manufacturer', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('manftr_comp_name',80);
            $table->string('manftr_comp_code',20);
            $table->mediumText('manftr_comp_address')->nullable();
            $table->string('manftr_comp_phone',25)->nullable();
            $table->string('manftr_comp_mobile',25)->nullable();
            $table->string('manftr_comp_fax',25)->nullable();
            $table->string('manftr_comp_email',60)->nullable()->unique();
            $table->string('manftr_comp_contact_person',45)->nullable();
            $table->string('manftr_comp_website',60)->nullable();
            $table->string('manftr_comp_addedby',45)->nullable();
            $table->date('manftr_comp_add_date')->default(date("Y-m-d H:i:s"));
            $table->string('manftr_comp_notes',200)->nullable();
            $table->tinyInteger('manftr_comp_flags')->nullable()->default(1);
            $table->timestamp('manftr_created_at')->nullable();
            $table->timestamp('manftr_updated_at')->nullable();
            
            
             $table->integer('company_id')->default(0);
             $table->integer('branch_id')->default(0);
             $table->tinyInteger('server_sync_flag')->default(0);
             $table->bigInteger('server_sync_time')->default(0);
             $table->bigInteger('local_sync_time')->default(0);
             $table->timestamps();
            
            
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manufacturer');
    }
}
