<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_master', function (Blueprint $table) {
            $table->bigIncrements('menu_id');
            $table->string('menu_name');         
            $table->integer('menu_grp_id')->comment('1 - Master | 2 - Transaction | 3 - Production | 4 - Settings | 5 - Reports | 6 - Help');
            $table->string('menu_use_id');
            $table->bigInteger('menu_prnt_id');
            $table->integer('menu_depth')->default(0);
            $table->tinyInteger('menu_flag')->default(1);

            $table->integer('branch_id')->default(0);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_masters');
    }
}
