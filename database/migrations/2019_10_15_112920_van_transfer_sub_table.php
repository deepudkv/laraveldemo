<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VanTransferSubTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('van_transfer_sub', function (Blueprint $table) {

            $table->bigIncrements('vantransub_id');
            $table->bigInteger('vantransub_vantran_id');
            $table->bigInteger('vantransub_van_id');
            $table->bigInteger('vantransub_godown_id')->default(0);
            $table->double('vantransub_rate');
            $table->double('vantransub_purch_rate');
            $table->double('vantransub_qty');
            $table->bigInteger('vantransub_prod_id');
            $table->bigInteger('vantransub_stock_id');
            $table->bigInteger('vantransub_branch_stock_id');
            $table->bigInteger('vantransub_unit_id');
            $table->date('vantransub_date');
            $table->bigInteger('vantransub_added_by');
            $table->tinyInteger('vantransub_flags')->default(1);

            $table->integer('branch_id')->default(0);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
