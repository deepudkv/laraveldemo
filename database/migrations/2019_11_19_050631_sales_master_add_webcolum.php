<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SalesMasterAddWebcolum extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sales_master', function (Blueprint $table) {
            $table->tinyInteger('is_web')->after('branch_id')->default(0);
        });
        Schema::table('sale_sub', function (Blueprint $table) {
            $table->tinyInteger('is_web')->after('branch_id')->default(0);
        });
        Schema::table('sales_due', function (Blueprint $table) {
            $table->tinyInteger('is_web')->after('branch_id')->default(0);
        });
        Schema::table('sales_due_sub', function (Blueprint $table) {
            $table->tinyInteger('is_web')->after('branch_id')->default(0);
        });
        Schema::table('sales_return_master', function (Blueprint $table) {
            $table->tinyInteger('is_web')->after('branch_id')->default(0);
        });
        Schema::table('sales_return_sub', function (Blueprint $table) {
            $table->tinyInteger('is_web')->after('branch_id')->default(0);
        });
        Schema::table('acc_voucher', function (Blueprint $table) {
            $table->tinyInteger('is_web')->after('branch_id')->default(0);
        });
        Schema::table('sales_agent', function (Blueprint $table) {
            $table->tinyInteger('is_web')->after('branch_id')->default(0);
        });
        Schema::table('sales_commission', function (Blueprint $table) {
            $table->tinyInteger('is_web')->after('branch_id')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales_master', function (Blueprint $table) {
            $table->dropColumn(['is_web']);
        });
        Schema::table('sale_sub', function (Blueprint $table) {
            $table->dropColumn(['is_web']);
        });
        Schema::table('sales_due', function (Blueprint $table) {
            $table->tinyInteger('is_web')->after('branch_id')->default(0);
        });
        Schema::table('sales_due_sub', function (Blueprint $table) {
            $table->tinyInteger('is_web')->after('branch_id')->default(0);
        });
        Schema::table('sales_return_master', function (Blueprint $table) {
            $table->tinyInteger('is_web')->after('branch_id')->default(0);
        });
        Schema::table('sales_return_sub', function (Blueprint $table) {
            $table->tinyInteger('is_web')->after('branch_id')->default(0);
        });
        Schema::table('acc_voucher', function (Blueprint $table) {
            $table->tinyInteger('is_web')->after('branch_id')->default(0);
        });
        Schema::table('sales_agent', function (Blueprint $table) {
            $table->tinyInteger('is_web')->after('branch_id')->default(0);
        });
        Schema::table('sales_commission', function (Blueprint $table) {
            $table->tinyInteger('is_web')->after('branch_id')->default(0);
        });

    }
}
