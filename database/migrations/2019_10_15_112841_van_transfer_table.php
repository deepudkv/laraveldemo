<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VanTransferTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('van_transfer', function (Blueprint $table) {
            $table->bigIncrements('vantran_id');
            $table->bigInteger('vantran_van_id');
            $table->bigInteger('vantran_godown_id')->default(0);
            $table->double('vantran_price');
            $table->double('vantran_purch_price');
            $table->date('vantran_date');
            $table->string('vantran_notes',1000);
            $table->bigInteger('vantran_van_ledger_id');
            $table->bigInteger('vantran_added_by');
            $table->tinyInteger('vantran_type')->default(0);
            $table->tinyInteger('vantran_flags')->default(1);

            $table->integer('branch_id')->default(0);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
