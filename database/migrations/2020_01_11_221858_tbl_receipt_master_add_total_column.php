<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblReceiptMasterAddTotalColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('acc_receipt', function (Blueprint $table) {
            $table->double('rec_vat_amount')->default(0);
            $table->double('rec_gttl_amount')->default(0);
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acc_receipt', function (Blueprint $table) {
            $table->dropColumn([
                'rec_vat_amount',
                'rec_gttl_amount'
            ]);
        });
    }
}
