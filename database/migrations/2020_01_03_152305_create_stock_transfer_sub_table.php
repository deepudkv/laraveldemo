<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockTransfer extends Migration
{
    /**2020_01_03_152305_create_branch_transfer
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_transfer', function (Blueprint $table) {
            $table->bigIncrements('stocktr_num');
            $table->bigInteger('stocktr_id');
            $table->integer('branch_id');
            $table->integer('stocktr_to')->comment ='to branch id';
            $table->date('stocktr_date');
            $table->bigInteger('stocktr_ledger_id')->default(0)->comment ='to branch ledger id';
            $table->double('stocktr_amount');
            $table->string('stocktr_notes',500);


            $table->tinyInteger('stocktr_accepted')->default(0)->comment ='0-pending,1-accepted,2-rejected';
            $table->integer('stocktr_accepted_by');
            $table->integer('stocktr_added_by');
            $table->tinyInteger('stocktr_flags')->default(1);


            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
