<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GodownstokLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('godown_stock_log', function (Blueprint $table) {
            $table->bigIncrements('gsl_id');
            $table->bigInteger('gsl_gdwn_stock_id');
            $table->bigInteger('gsl_branch_stock_id');
            $table->bigInteger('gsl_stock_id');
            $table->bigInteger('gsl_prd_id');
            $table->integer('gsl_prod_unit')->default(0);
            $table->double('gsl_qty',15,8)->default(0);
            $table->integer('gsl_from')->default(0);
            $table->integer('gsl_to')->default(0);
            $table->integer('gsl_vchr_type')->default(0);
            $table->integer('gsl_tran_vanid')->default(0);           
            
            $table->date('gsl_date')->default(date("Y-m-d"));
            $table->integer('gsl_added_by')->default(0);
            $table->tinyInteger('gsl_flag')->default(1);
            $table->integer('branch_id')->default(0);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('godown_stock_log');
    }
}
