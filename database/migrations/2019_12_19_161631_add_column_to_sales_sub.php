<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToSalesSub extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sale_sub', function (Blueprint $table) {
            $table->string('salesub_notes', 256);
            $table->string('branch_inv_no', 256);
            $table->bigInteger('branch_inv_no2');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sale_sub', function (Blueprint $table) {
            $table->dropColumn([
                'salesub_notes',
                'branch_inv_no',
                'branch_inv_no2'
            ]);
        });
    }
}
