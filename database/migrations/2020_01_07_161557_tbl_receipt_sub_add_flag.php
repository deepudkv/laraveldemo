<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblReceiptSubAddFlag extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_receipt_sub', function (Blueprint $table) {
            $table->tinyInteger('recsub_flags')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acc_receipt_sub', function (Blueprint $table) {
            $table->dropColumn([
                'recsub_flags',
            ]);
        });
    }
}
