<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblPaymentMasterAddPayNoColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_payment', function (Blueprint $table) {
            $table->tinyInteger('is_web')->default(0);
            $table->string('van_pay_no', 45);
            $table->string('branch_pay_no', 45);
            $table->bigInteger('branch_pay_no2')->default(0);
            $table->double('pay_vat_amount')->default(0);
            $table->double('pay_gttl_amount')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acc_payment', function (Blueprint $table) {
            $table->dropColumn([
                'van_pay_no',
                'branch_pay_no',
                'branch_pay_no2',
                'is_web',
                'pay_vat_amount',
                'pay_gttl_amount'
            ]);
        });
    }
}
