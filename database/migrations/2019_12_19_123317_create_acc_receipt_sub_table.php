<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccReceiptSubTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acc_receipt_sub', function (Blueprint $table) {
            $table->bigIncrements('recsub_id');
            $table->bigInteger('recsub_rec_no');
            $table->date('recsub_date')->default('2000-01-01');
            $table->time('recsub_time')->default('00:00:00');
            $table->dateTime('recsub_datetime')->default('2000-01-01 00:00:00');
            $table->integer('recsub_timestamp');
            $table->integer('recsub_acc_ledger_id');
            $table->integer('recsub_ledger_id');
            $table->double('recsub_amount');
            $table->string('recsub_narration',256);
            $table->double('recsub_vat_per');
            $table->double('recsub_vat_amt');
            $table->integer('branch_id');
            $table->integer('van_id')->default(0);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acc_receipt_sub');
    }
}
