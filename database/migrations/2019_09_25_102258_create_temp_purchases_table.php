<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempPurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmp_purchases', function (Blueprint $table) {
            $table->bigIncrements('purch_id');
            $table->string('purch_id2',100);
            $table->biginteger('purch_branch_id');
            $table->string('purch_no',50)->nullable();
            $table->string('purch_inv_no',50)->nullable();
            $table->biginteger('purch_supp_id')->nullable();
            $table->biginteger('purch_ord_no')->nullable();
            $table->tinyInteger('purch_type')->nullable();
            $table->tinyInteger('purch_pay_type')->nullable();
            $table->date('purch_date')->nullable();
            $table->date('purch_inv_date')->nullable();
            $table->string('purch_note',150)->nullable();
            $table->double('purch_amount')->nullable();
            $table->double('purch_tax')->nullable();
            $table->biginteger('purch_tax_ledger_id')->nullable();
            $table->double('purch_discount')->nullable();
            $table->double('purch_frieght')->nullable();
            $table->biginteger('purch_frieght_ledger_id')->nullable();
            $table->double('purch_tax2')->nullable();
            $table->double('purch_tax3')->nullable();
            $table->string('purch_agent',100)->nullable();
            $table->text('temp_purch_json')->nullable();
            $table->tinyInteger('purch_flag')->default(1);
            $table->integer('company_id')->default(0);
            $table->integer('branch_id')->default(0);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tmp_purchases');
    }
}
