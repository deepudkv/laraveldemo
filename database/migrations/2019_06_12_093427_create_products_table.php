<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('prd_id');
            $table->string('prd_name',500);
            $table->biginteger('prd_cat_id');
            $table->biginteger('prd_sub_cat_id')->default(0)->nullable();

            $table->biginteger('prd_supplier')->nullable()->comment('manufacture_id');
            $table->string('prd_alias',500)->nullable();
            $table->string('prd_short_name',15)->nullable();

            $table->integer('prd_min_stock')->nullable();
            $table->integer('prd_max_stock')->nullable();
            $table->biginteger('prd_barcode')->nullable();
            $table->string('prd_ean',250)->nullable();
            $table->integer('prd_added_by')->nullable();
            $table->integer('prd_base_unit_id');
            $table->integer('prd_default_unit_id')->nullable();
            $table->double('prd_tax')->nullable();
            $table->string('prd_code',10)->nullable();
            $table->string('prd_tax_code',50)->nullable();
            $table->text('prd_desc')->nullable();

            $table->integer('prd_exp_dur')->nullable();
            $table->double('prd_loyalty_rate',15, 8)->nullable();

            $table->tinyInteger('prd_type')->default(1);
            $table->tinyInteger('prd_stock_status')->default(1);

            $table->tinyInteger('prd_stock_stat')->default(1);

            $table->tinyInteger('prd_minstock_alert')->nullable();
            $table->tinyInteger('prd_maxstock_alert')->nullable();
            $table->mediumText('prd_remarks')->nullable();
            $table->tinyInteger('prd_flag')->default(1);

            $table->integer('company_id')->default(0);
            $table->integer('branch_id')->default(0);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }

    // ALTER TABLE `erp_products` ADD `prd_type` TINYINT NOT NULL DEFAULT '1' AFTER `prd_loyalty_rate`, ADD `prd_stock_stat` TINYINT NOT NULL DEFAULT '1' AFTER `prd_type`;
}
