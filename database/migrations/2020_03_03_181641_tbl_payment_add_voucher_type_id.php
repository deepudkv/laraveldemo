<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblPaymentAddVoucherTypeId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_payment', function (Blueprint $table) {
            $table->integer('pay_vchtype_id')->default(0);
            $table->tinyInteger('pay_interbranch_status')->default(1)->comment = '0-Pending, 1- Accepted, 2 - Rejected';
            $table->integer('pay_inter_branch_id')->default(0);
            // 
        });
    }    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acc_payment', function (Blueprint $table) {
            $table->dropColumn([
                'pay_vchtype_id',
                'pay_interbranch_status',
                'pay_inter_branch_id'
            ]);
        });
    }
}
