<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProductionSub extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('production_sub', function (Blueprint $table) {
            $table->bigIncrements('prdnsub_id');
            $table->bigInteger('prdnsub_prdn_id');
            $table->integer('prdnsub_prodform_id');
            $table->integer('prdnsub_prodformsub_id');
            $table->bigInteger('prdnsub_prd_id');
            $table->bigInteger('prdnsub_stock_id');
            $table->bigInteger('prdnsub_branch_stock_id');
            $table->double('prdnsub_qty');
            $table->Integer('prdnsub_unit_id');
            $table->double('prdnsub_rate');
            $table->timestamp('prdnsub_date');
            $table->Integer('prdnsub_added_by');
            $table->tinyInteger('prdnsub_flag')->default(1);
            $table->integer('branch_id')->default(0);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
