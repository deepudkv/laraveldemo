<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Production extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('production', function (Blueprint $table) {
            $table->bigIncrements('prdn_id');
            $table->integer('prdn_prodform_id');
            $table->double('prdn_purch_amount');
            $table->double('prdn_misc_exp');
            $table->double('prdn_comm_amount');
            $table->double('prdn_cost');
            $table->timestamp('prdn_date');
            $table->bigInteger('prdn_added_by');
            $table->bigInteger('prdn_inspection_staff');
            $table->tinyInteger('prdn_commission')->comment = 'commission 0-no 1-yes';
            $table->tinyInteger('prdn_flag')->default(1);
            $table->string('prdn_note',500)->nullable();
            $table->integer('branch_id')->default(0);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
