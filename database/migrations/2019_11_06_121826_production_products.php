<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProductionProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('production_products', function (Blueprint $table) {
            $table->bigIncrements('prdnprd_id');
            $table->bigInteger('prdnprd_prdn_id');
            $table->integer('prdnprd_prodform_id');
            $table->bigInteger('prdnprd_prd_id');
            $table->bigInteger('prdnprd_stock_id');
            $table->bigInteger('prdnprd_branch_stock_id');
            $table->double('prdnprd_qty');
            $table->double('prdnprd_required_qty');
            $table->Integer('prdnprd_unit_id');
            $table->double('prdnprd_rate');
            $table->double('prdnprd_rate_expected');
            $table->timestamp('prdnprd_date');

            $table->string('prdnprd_btach_code',100)->nullable();
            $table->timestamp('prdnprd_exp_date')->nullable();
            $table->timestamp('prdnprd_mfg_date')->nullable();
            $table->bigInteger('prdnprd_gd_id')->nullable();
            
            $table->Integer('prdnprd_added_by');
            $table->tinyInteger('prdnprd_flag')->default(1);
            $table->integer('branch_id')->default(0);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
