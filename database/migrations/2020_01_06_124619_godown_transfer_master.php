<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GodownTransferMaster extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('godown_transfer_master', function (Blueprint $table) {

            $table->bigIncrements('gdtrn_id');
            $table->bigInteger('gdtrn_inv_id');

            $table->bigInteger('gdtrn_from');
            $table->bigInteger('gdtrn_to');
            $table->string('gdtrn_notes',256)->nullable();

            
            $table->date('gdtrn_date')->default('2000-01-01');
            $table->time('gdtrn_time')->default('00:00:00');
            $table->dateTime('gdtrn_datetime')->default('2000-01-01 00:00:00');
            
            
            $table->bigInteger('gdtrn_added_by');
            $table->tinyInteger('gdtrn_flags')->default(1);
            
            $table->integer('company_id')->default(0);
            $table->integer('branch_id')->default(0);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('godown_transfer_master');

    }
}
