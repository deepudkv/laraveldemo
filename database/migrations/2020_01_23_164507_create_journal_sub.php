<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJournalSub extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('journal_sub', function (Blueprint $table) {
            $table->bigIncrements('jnsub_id');
            $table->bigInteger('jnsub_ref_no');
            $table->date('jnsub_date')->default('2000-01-01');
            $table->time('jnsub_time')->default('00:00:00');
            $table->dateTime('jnsub_datetime')->default('2000-01-01 00:00:00');
            $table->integer('jnsub_timestamp');
            $table->integer('jnsub_acc_ledger_id');
            $table->integer('jnsub_ledger_id');
            $table->double('jnsub_amount');
            $table->string('jnsub_narration',256);
            $table->double('jnsub_ttl_amount');
            $table->double('jnsub_no');
            $table->string('van_ref_no',45);
            $table->string('branch_jn_no',45);
       
            $table->tinyInteger('jn_flags')->default(0);

            $table->integer('branch_id');
            $table->integer('van_id')->default(0);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('journal_sub');
    }
}
