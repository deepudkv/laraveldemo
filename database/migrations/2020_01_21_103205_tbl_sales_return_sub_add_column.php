<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblSalesReturnSubAddColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sales_return_sub', function (Blueprint $table) {
            $table->string('salesretsub_van_ret_no', 50);
            $table->dropColumn([
                'salesretsub_salesret_id',
            ]);
            $table->bigInteger('salesretsub_salesret_no');
            // $table->renameColumn('salesretsub_salesret_id', 'salesretsub_salesret_no');
            // $table->renameColumn('salesretsub_salesret_id', 'salesretsub_salesret_no');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales_return_sub', function (Blueprint $table) {
            $table->dropColumn([
                'salesretsub_van_ret_no',
                'salesretsub_salesret_no'
            ]);
            $table->integer('salesretsub_salesret_id');
            // $table->renameColumn('salesretsub_salesret_no', 'salesretsub_salesret_id');
            // $table->renameColumn('author_ID', 'user_id');
        });
    }
}
