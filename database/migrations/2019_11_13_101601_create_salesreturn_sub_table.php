<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesreturnSubTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_return_sub', function (Blueprint $table) {
            $table->bigIncrements('salesretsub_id');
            $table->integer("salesretsub_salesret_id")->default(0);
            $table->integer("salesretsub_sales_inv_no")->default(0);
            $table->integer("salesretsub_salesub_id")->default(0);
            $table->integer("salesretsub_stock_id")->default(0);
            $table->integer("salesretsub_prod_id")->default(0);
            $table->double("salesretsub_rate")->default(0);
            $table->double("salesretsub_qty")->default(0);
            $table->date("salesretsub_date")->default('2000-01-01');
            $table->integer("salesretsub_flags")->default(1);
            $table->integer("salesretsub_unit_id")->default(1);
            $table->double("salesretsub_tax_per")->default(0);
            $table->double("salesretsub_tax_rate")->default(0);
            $table->integer("salesretsub_godown_id")->default(0);
            $table->integer("salesretsub_server_sync")->default(0);
            $table->bigInteger("salesretsub_sync_timestamp")->default(0);
            $table->bigInteger("salesretsub_server_timestamp");

            $table->integer('branch_id');
            $table->integer('godown_id')->default(0);
            $table->integer('van_id')->default(0);

            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salesreturn_sub');
    }
}
