<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VanTransferReturnSub extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('van_transfer_return_sub', function (Blueprint $table) {

            $table->bigIncrements('vantranretsub_id');
            $table->bigInteger('vantransub_vantran_id');
            $table->bigInteger('vantranretsub_vantran_id');
            $table->bigInteger('vantranretsub_vantranret_id');
            $table->bigInteger('vantranretsub_van_id');
            $table->bigInteger('vantranretsub_godown_id')->default(0);
            $table->double('vantranretsub_rate');
            $table->double('vantranretsub_purch_rate');
            $table->double('vantranretsub_qty');
            $table->bigInteger('vantranretsub_prod_id');
            $table->bigInteger('vantranretsub_stock_id');
            $table->bigInteger('vantransub_branch_stock_id');
            $table->bigInteger('vantranretsub_unit_id');
            $table->date('vantranretsub_date');
            $table->bigInteger('vantranretsub_added_by');
            $table->tinyInteger('vantranretsub_flags')->default(1);

            $table->integer('branch_id')->default(0);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
