<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CutomerTableAddPhoneColoumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_customers', function(Blueprint $table) {
            $table->string('dflt_delvry_mobile',250)->after('dflt_delvry_fax');
            $table->integer('cust_category')->after('dflt_delvry_mobile')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acc_customers', function(Blueprint $table) {
            $table->dropColumn([
                'dflt_delvry_mobile',
                'cust_category',
            ]);
        });
    }
}
