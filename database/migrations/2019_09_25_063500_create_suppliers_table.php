<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suppliers', function (Blueprint $table) {
            $table->bigIncrements('supp_id');
            $table->string('supp_name',250);
            $table->string('supp_alias',100)->nullable();
            $table->string('supp_code',50);
            
            $table->integer('supp_ledger_id');
            $table->smallInteger('supp_branch_id')->default(0);

            $table->string('supp_address1',200)->nullable();
            $table->string('supp_address2',200)->nullable();
            $table->string('supp_zip',20)->nullable();
            $table->string('supp_city',100)->nullable();
            $table->string('supp_state',100)->nullable();
            $table->string('supp_country',100)->nullable();
            $table->string('supp_state_code',10)->nullable();
            $table->string('supp_phone',20)->nullable();
            $table->string('supp_mob',20)->nullable();
            $table->string('supp_email',100)->nullable();
            $table->string('supp_fax',50)->nullable();
            $table->string('supp_notes',1000)->nullable();

            $table->tinyInteger('supp_flag')->default(1);
            $table->string('supp_tin',25)->nullable();
            $table->string('supp_contact',100)->nullable();
            $table->integer('supp_due')->nullable();
            $table->double('opening_balance', 8, 2)->default(0);

            $table->integer('company_id')->default(0);
            $table->integer('branch_id')->default(0);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suppliers');
    }
}
