<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branch_stocks', function (Blueprint $table) {
            $table->bigIncrements('branch_stock_id');
            $table->bigInteger('bs_stock_id');
            $table->bigInteger('bs_prd_id');

            $table->double('bs_stock_quantity')->default(0);
            $table->double('bs_stock_quantity_shop')->default(0);
            $table->double('bs_stock_quantity_gd')->default(0);
            $table->double('bs_stock_quantity_van')->default(0);
            $table->double('bs_prate')->default(0);
            $table->double('bs_avg_prate')->default(0);
            $table->double('bs_srate')->default(0);
            $table->bigInteger('bs_batch_id')->default(0);
            $table->string('bs_expiry',100)->default(0);
            
            $table->tinyInteger('bs_stk_status')->default(1);

            $table->integer('bs_branch_id')->default(0);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branch_stocks');
    }
}
