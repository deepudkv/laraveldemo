<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VanLines extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    
    public function up()
    {
        Schema::create('vanline', function (Blueprint $table) {
            $table->bigIncrements('vanline_id');
            $table->string('vanline_name',250)->default(0);
            $table->string('vanline_desc',100)->default(0);
            $table->tinyInteger('vanline_flag')->default(1);
            $table->integer('company_id')->default(0);
            $table->integer('branch_id')->default(0);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
