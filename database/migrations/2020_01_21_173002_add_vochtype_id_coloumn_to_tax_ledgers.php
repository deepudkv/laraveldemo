<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVochtypeIdColoumnToTaxLedgers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tax_ledgers', function (Blueprint $table) {
            $table->integer('taxled_vchtype_id')->after('taxled_taxcatsub_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tax_ledgers', function (Blueprint $table) {
            $table->dropColumn([
                'taxled_vchtype_id',
            ]);
        });
    }
}
