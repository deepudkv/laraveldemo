<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductionFormulaCommTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('production_formula_comm', function (Blueprint $table) {
            $table->bigIncrements('prodformcomm_id');
            $table->integer('prodformcomm_prodform_id')->default(0);
            $table->integer('prodformcomm_laccount_no')->default(0)->comment = 'staff ledger account no';;
            $table->double('prodformcomm_amount')->default(0);
            $table->integer('branch_id')->default(0);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('production_formula_comm');
    }
}
