<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblReceiptPaymentAddFlagColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_receipt', function (Blueprint $table) {
            $table->tinyInteger('rec_flags')->default(1);
        });
       
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acc_receipt', function (Blueprint $table) {
            $table->dropColumn([
                'rec_flags',
            ]);
        });
       
    }    
}
