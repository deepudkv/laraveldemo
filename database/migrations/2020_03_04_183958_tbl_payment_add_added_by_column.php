<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblPaymentAddAddedByColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_payment', function (Blueprint $table) {
            $table->integer('is_inter_branch')->default(0);
            $table->integer('added_by')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acc_payment', function (Blueprint $table) {
            $table->dropColumn([
                'is_inter_branch',
                'added_by',
            ]);
        });
    }
}
