<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acc_branches', function (Blueprint $table) {
            $table->bigIncrements('branch_id');
            $table->string('branch_name',150);
            $table->string('branch_display_name',150);
            $table->string('branch_code',50);
            $table->string('branch_address',1000)->nullable();
            $table->string('branch_address2',1000)->nullable();
            $table->string('branch_phone',25)->nullable();
            $table->string('branch_mob',25)->nullable();
            $table->string('branch_fax',40)->nullable();
            $table->string('branch_email',100)->nullable();
            $table->string('branch_tin',20)->nullable();
            $table->string('branch_reg_no',100)->nullable();
            $table->date('branch_open_date')->nullable();
            $table->string('branch_notes',250)->nullable();
            $table->smallInteger('branch_default')->default(0); 
            $table->smallInteger('branch_flags')->default(1); 
            $table->integer('company_id')->default(0);     
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });


        // $insert = array(
        //     'branch_id' =>'0',
        //     'branch_name' =>'COMPANY',
        //     'branch_display_name' =>'BRANCHDEFAULT',
        //     'branch_code' =>'CMP001' 

        //    );

        //  DB::table('acc_branches')->insert($insert);
        //  $stmnt = "ALTER TABLE `erp_acc_branches` ADD PRIMARY KEY(`branch_id`)";
        //  DB::unprepared($stmnt);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acc_branches');
    }
}
