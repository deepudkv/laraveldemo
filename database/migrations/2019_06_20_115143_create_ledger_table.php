<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLedgerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acc_ledgers', function (Blueprint $table) {
            $table->bigIncrements('ledger_id');
            $table->string('ledger_name',250)->default('NILL');
            $table->smallInteger('ledger_accgrp_id')->default(0);
            $table->smallInteger('ledger_acc_type')->nullable()->default(2)->comment = '0-cash 1-bank 2-other';
            $table->smallInteger('ledger_return')->nullable()->default(1)->comment = '0-no 1-yes';
            $table->string('ledger_address',250)->nullable()->default('NILL');
            $table->string('ledger_notes',250)->nullable()->default('NILL');
            $table->smallInteger('ledger_edit')->nullable()->default(1);
            $table->smallInteger('ledger_flags')->default(1);
            $table->smallInteger('ledger_branch_id')->nullable()->default(1);           
            $table->smallInteger('ledger_balance_privacy')->nullable()->default(0);
            $table->smallInteger('ledger_due')->nullable()->default(0);
            $table->string('ledger_tin',50)->nullable()->default('NILL');
            $table->string('ledger_alias',50)->nullable()->default('NILL');
            $table->string('ledger_code',50)->nullable()->default('NILL');
            $table->string('ledger_email',50)->nullable()->default('NILL');
            $table->string('ledger_mobile',50)->nullable()->default('NILL');
            $table->double('opening_balance', 8, 2)->default(0);

            
            $table->integer('company_id')->default(0);
            $table->integer('branch_id')->default(0);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acc_ledgers');
    }
}
