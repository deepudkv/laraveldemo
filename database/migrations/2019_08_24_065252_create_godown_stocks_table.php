<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGodownStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('godown_stocks', function (Blueprint $table) {
            $table->bigIncrements('gs_id');
            $table->bigInteger('gs_godown_id');
            $table->bigInteger('gs_branch_stock_id');
            $table->bigInteger('gs_stock_id');
            $table->bigInteger('gs_prd_id');
            $table->double('gs_qty',15,8)->default(0);
            $table->date('gs_date')->default(date("Y-m-d"));
            $table->tinyInteger('gs_flag')->default(1);
            $table->integer('branch_id')->default(0);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('godown_stocks');
    }
}
