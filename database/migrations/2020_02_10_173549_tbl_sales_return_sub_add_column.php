<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblSalesReturnSubAddColumn2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sales_return_sub', function (Blueprint $table) {
            $table->integer('salesretsub_id2');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales_return_sub', function (Blueprint $table) {
            $table->dropColumn([
                'salesretsub_id2',
            ]);
        });
    }
}
