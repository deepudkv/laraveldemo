<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('features', function (Blueprint $table) {
             $table->bigIncrements('feat_id');
             $table->string('feat_name',100);
             $table->string('feat_description',200)->nullable();
             
             $table->integer('company_id')->default(0);
             $table->integer('branch_id')->default(0);
             $table->tinyInteger('server_sync_flag')->default(0);
             $table->bigInteger('server_sync_time')->default(0);
             $table->bigInteger('local_sync_time')->default(0);
             $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('features');
    }
}
