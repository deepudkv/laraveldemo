<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SalesdueSubAddInvNoColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sales_due_sub', function (Blueprint $table) {
            $table->string('van_inv_no', 256);
            $table->bigInteger('salesduesub_id2');
            $table->string('branch_inv_no', 256);
            $table->bigInteger('branch_inv_no2');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales_due_sub', function (Blueprint $table) {
            $table->dropColumn([
                'van_inv_no',
                'salesduesub_id2',
                'branch_inv_no',
                'branch_inv_no2'
            ]);
        });
    }
}
