<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StockDailyUpdates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { Schema::create('stock_daily_updates', function (Blueprint $table) {
        $table->bigIncrements('sdu_id');
        $table->bigInteger('sdu_branch_stock_id');
        $table->bigInteger('sdu_prd_id');
        $table->bigInteger('sdu_stock_id');
        $table->date('sdu_date')->default(date("Y-m-d"));
        $table->double('sdu_stock_quantity');
        $table->double('sdu_godown_id')->default(0);
        
        $table->bigInteger('sdu_gd_id')->default(0);
        $table->bigInteger('sdu_batch_id')->default(0);

        $table->integer('branch_id')->default(0);
        $table->tinyInteger('server_sync_flag')->default(0);
        $table->bigInteger('server_sync_time')->default(0);
        $table->bigInteger('local_sync_time')->default(0);
        $table->timestamps();
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('stock_daily_updates');
    }
}
