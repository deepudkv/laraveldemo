<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Van extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    
    public function up()
    {
        Schema::create('van', function (Blueprint $table) {
            $table->bigIncrements('van_id');
            $table->bigInteger('van_vanline_id');
            $table->string('van_name',100);
            $table->string('van_code',10);
            $table->string('van_reg_no',10);
            $table->string('van_contact_person',100);
            $table->string('van_contact_phone',25)->nullable();
            $table->string('van_contact_mob',25);
            $table->string('van_contact_email',30);
            $table->string('van_password',150);
            $table->string('van_contact_address',1000)->nullable();
            $table->string('van_desc',1000)->nullable();
            $table->bigInteger('van_ledger_id');
            $table->bigInteger('van_added_by');
            $table->tinyInteger('van_type')->default(1);
            $table->tinyInteger('van_flag')->default(1);

            $table->integer('branch_id')->default(0);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
