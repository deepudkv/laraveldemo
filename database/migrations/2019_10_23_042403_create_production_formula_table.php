<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductionFormulaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('production_formula', function (Blueprint $table) {
            $table->bigIncrements('prodform_id');
            $table->string('prodform_name',200);
            $table->bigInteger('prodform_ingred')->default(1);
            $table->bigInteger('prodform_prod_count')->default(1)->comment = 'No of output product';
            $table->bigInteger('prodform_commission')->comment = 'commission 0-no 1-yes';
            $table->double('prodform_comm_amount')->comment = 'commssion amount';
            $table->double('prodform_misc')->comment = 'misc expense';
            $table->double('prodform_insp_laccount_no')->comment = 'inspection staff id';
            $table->date('prodfrom_date')->default('2000-01-01');
            $table->bigInteger('prodform_added_by')->default(1);
            $table->tinyInteger('prodform_flag')->default(1);

            $table->integer('branch_id')->default(0);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('production_formula');
    }
}
