<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIsTaxable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tax_category_sub', function (Blueprint $table) {
            $table->tinyInteger('taxcatsub_is_tax')->after('taxcatsub_vchtype_id')->default(1);
        });
    }    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tax_category_sub', function (Blueprint $table) {
            $table->dropColumn([
                'taxcatsub_is_tax',
            ]);
        });
    }
}
