<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblAccVoucherAddRefNoColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_voucher', function (Blueprint $table) {
            $table->string('branch_ref_no',45);
            $table->bigInteger('sub_ref_no');
            // $table->renameColumn('sales_inv_no', 'ref_no');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acc_voucher', function (Blueprint $table) {
            $table->dropColumn([
                'branch_ref_no',
                'sub_ref_no'
            ]);
            // $table->renameColumn('ref_no', 'sales_inv_no');
        });
    }
}
