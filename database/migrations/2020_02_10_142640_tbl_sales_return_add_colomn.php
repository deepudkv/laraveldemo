<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblSalesReturnAddColomn1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sales_return_master', function (Blueprint $table) {
            $table->string('salesret_sales_branch_inv_no', 60);
            $table->integer('salesret_cust_ledger_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales_return_master', function (Blueprint $table) {
            $table->dropColumn([
                'salesret_sales_branch_inv_no',
                'salesret_cust_ledger_id',
            ]);
        });
    }
}
