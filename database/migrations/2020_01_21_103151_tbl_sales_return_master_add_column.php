<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblSalesReturnMasterAddColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sales_return_master', function (Blueprint $table) {
            $table->string('salesret_van_ret_no', 50);
            $table->string('salesret_sales_van_inv_no', 50);
            $table->bigInteger('salesret_no')->after('salesret_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales_return_master', function (Blueprint $table) {
            $table->dropColumn([
                'salesret_van_ret_no',
                'salesret_sales_van_inv_no',
                'salesret_no'
            ]);
        });
    }
}
