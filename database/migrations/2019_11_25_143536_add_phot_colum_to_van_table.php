<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPhotColumToVanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('van', function (Blueprint $table) {
            $table->string('photo',250)->after('van_ledger_id')->default('default.png');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('van', function (Blueprint $table) {
            $table->dropColumn([
                'photo'
            ]);
        });
    }
}
