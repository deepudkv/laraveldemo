<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblPaymentSubAddPayNoColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_payment_sub', function (Blueprint $table) {
            $table->string('van_pay_no',45);
            $table->string('branch_pay_no',45);
            $table->integer('vat_catgeory_id')->default(0);
            $table->double('paysub_ttl_amount')->default(0);
            $table->tinyInteger('paysub_vat_inc')->default(0)->comment = '0 - Vat Exclude 1 - Vat Include';
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acc_payment_sub', function (Blueprint $table) {
            $table->dropColumn([
                'van_pay_no',
                'branch_pay_no',
                'vat_catgeory_id',
                'paysub_ttl_amount',
                'paysub_vat_inc'
            ]);
        });
    }
}
