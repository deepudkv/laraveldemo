<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblRecpMaterAddSyncCodeAndSyncCompleted extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_receipt', function (Blueprint $table) {
            $table->string('sync_code', 256);
            $table->tinyInteger('sync_completed')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acc_receipt', function (Blueprint $table) {
            $table->dropColumn([
                'sync_code',
                'sync_completed'
            ]);
        });
    }
}
