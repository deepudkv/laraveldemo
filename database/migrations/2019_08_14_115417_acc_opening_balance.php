<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AccOpeningBalance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       
        Schema::create('acc_opening_balances', function (Blueprint $table) {
            $table->bigIncrements('opbal_id');
            $table->integer('opbal_ledger_id')->default(0);
            $table->double('opbal_in', 8, 2)->default(0);;
            $table->double('opbal_out', 8, 2)->default(0);;
            $table->integer('opbal_added_by')->default(1);
            $table->tinyInteger('opbal_flags')->default(1);

            $table->integer('company_id')->default(0);
            $table->integer('branch_id')->default(0);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acc_opening_balances');
    }
}

