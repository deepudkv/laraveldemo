<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColoumnToSalesDueSub extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::table('sales_due_sub', function (Blueprint $table) {
        //     $table->bigInteger('rec_no')->default(0);
        //     $table->string('van_rec_no', 256);
        //     $table->string('branch_rec_no', 256);
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales_due_sub', function (Blueprint $table) {
            $table->dropColumn([
                'rec_no',
                'van_rec_no',
                'branch_rec_no',
            ]);
        });
    }
}
