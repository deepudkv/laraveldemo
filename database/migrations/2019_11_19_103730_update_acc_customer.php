<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAccCustomer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_customers', function(Blueprint $table) {
            $table->dropColumn('cust_image');
            $table->dropColumn('company_id');

            $table->string('name',250)->unique()->after('ledger_id');
            $table->string('alias',250)->nullable();
            $table->string('zip',250)->nullable();
            $table->string('city',250)->nullable();
            $table->string('state',250)->nullable();
            $table->string('state_code',250)->nullable();
            $table->string('country',250)->nullable();
            $table->string('fax',250)->nullable();

            $table->string('dflt_delvry_addr',250)->nullable();
            $table->string('dflt_delvry_zip',250)->nullable();
            $table->string('dflt_delvry_city',250)->nullable();
            $table->string('dflt_delvry_state',250)->nullable();
            $table->string('dflt_delvry_state_code',250)->nullable();
            $table->string('dflt_delvry_country',250)->nullable();
            $table->string('dflt_delvry_fax',250)->nullable();

            $table->string('email',250)->nullable();
            $table->string('mobile',250)->nullable();
            $table->string('note',1000)->nullable();
            $table->integer('due_days')->nullable();
            $table->string('vat_no',250)->nullable();
            $table->double('opening_balance', 8, 2)->default(0);



            $table->integer('van_line_id')->default(0)->before('branch_id');
            $table->integer('price_group_id')->default(0)->before('branch_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acc_customers', function(Blueprint $table) {
            $table->string('cust_image',250)->nullable()->default('NILL');
            $table->integer('company_id')->default(0);
            $table->dropColumn([
                'name','alias','zip','state', 'state_code','country', 'fax',
                'dflt_delvry_addr','dflt_delvry_zip','dflt_delvry_state', 'dflt_delvry_state_code',
                'dflt_delvry_country', 'dflt_delvry_fax', 'email', 'mobile', 'note','due_days',
                 'vat_no', 'van_line_id', 'price_group_id'
            ]);
        });
    }
}
