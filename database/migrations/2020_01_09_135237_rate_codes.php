<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RateCodes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rate_codes', function (Blueprint $table) {
            $table->bigInteger('rcd_id');
            $table->string('rcd_code',256);
            $table->string('rcd_desc',256)->nullable(); 
            $table->bigInteger('rcd_added_by')->nullable();  
            
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });


        $insert = array(
            array('rcd_id'=>'11','rcd_code'=>';;;;;;;;;;')
        );

    DB::table('rate_codes')->insert($insert);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rate_codes');

    }
}
