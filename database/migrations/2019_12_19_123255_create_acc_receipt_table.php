<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccReceiptTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acc_receipt', function (Blueprint $table) {
            $table->bigIncrements('rec_id');
            $table->bigInteger('rec_no');
            $table->date('rec_date')->default('2000-01-01');
            $table->time('rec_time')->default('00:00:00');
            $table->dateTime('rec_datetime')->default('2000-01-01 00:00:00');
            $table->integer('rec_timestamp');
            $table->integer('rec_acc_ledger_id');
            $table->double('rec_ttl_amount');
            $table->string('rec_note', 256);
            $table->integer('branch_id');
            $table->integer('van_id')->default(0);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acc_receipt');
    }
}
