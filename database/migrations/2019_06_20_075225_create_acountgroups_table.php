<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAcountgroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acc_groups', function (Blueprint $table) {
            $table->bigIncrements('accgrp_id');
            $table->string('accgrp_name',100); 
            $table->smallInteger('accgrp_nature')->nullable()->default(0);
            $table->smallInteger('accgrp_parent')->nullable()->default(0);
            $table->smallInteger('accgrp_root')->nullable()->default(0);
            $table->smallInteger('accgrp_depth')->nullable()->default(0);
            $table->smallInteger('accgrp_edit')->nullable()->default(0);
            $table->tinyInteger('accgrp_flags')->default(0);
            $table->smallInteger('accgrp_enable_ledger')->nullable()->default(0);
            $table->string('accgrp_code',50)->nullable()->default(0);
            
            $table->integer('company_id')->default(0);
            $table->integer('branch_id')->default(0);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    
    $inserts = array(
        array('accgrp_id' => '1','accgrp_name' => 'Current Assets','accgrp_nature' => '2','accgrp_parent' => '1','accgrp_root' => '1','accgrp_depth' => '0','accgrp_edit' => '0','accgrp_flags' => '1','accgrp_enable_ledger' => '1','accgrp_code' => '' ),
        array('accgrp_id' => '2','accgrp_name' => 'Fixed Assets','accgrp_nature' => '2','accgrp_parent' => '2','accgrp_root' => '2','accgrp_depth' => '0','accgrp_edit' => '0','accgrp_flags' => '1','accgrp_enable_ledger' => '1','accgrp_code' => '' ),
        array('accgrp_id' => '3','accgrp_name' => 'Cash in Hand','accgrp_nature' => '2','accgrp_parent' => '1','accgrp_root' => '1','accgrp_depth' => '1','accgrp_edit' => '0','accgrp_flags' => '1','accgrp_enable_ledger' => '1','accgrp_code' => '' ),
        array('accgrp_id' => '4','accgrp_name' => 'Bank Accounts','accgrp_nature' => '2','accgrp_parent' => '1','accgrp_root' => '1','accgrp_depth' => '1','accgrp_edit' => '0','accgrp_flags' => '1','accgrp_enable_ledger' => '1','accgrp_code' => '' ),
        array('accgrp_id' => '5','accgrp_name' => 'Indirect Expense','accgrp_nature' => '1','accgrp_parent' => '5','accgrp_root' => '5','accgrp_depth' => '0','accgrp_edit' => '0','accgrp_flags' => '1','accgrp_enable_ledger' => '1','accgrp_code' => '' ),
        array('accgrp_id' => '6','accgrp_name' => 'Direct Expense','accgrp_nature' => '1','accgrp_parent' => '6','accgrp_root' => '6','accgrp_depth' => '0','accgrp_edit' => '0','accgrp_flags' => '1','accgrp_enable_ledger' => '1','accgrp_code' => '' ),
        array('accgrp_id' => '7','accgrp_name' => 'Indirect Income','accgrp_nature' => '0','accgrp_parent' => '7','accgrp_root' => '7','accgrp_depth' => '0','accgrp_edit' => '0','accgrp_flags' => '1','accgrp_enable_ledger' => '1','accgrp_code' => '' ),
        array('accgrp_id' => '8','accgrp_name' => 'Direct Income','accgrp_nature' => '0','accgrp_parent' => '8','accgrp_root' => '8','accgrp_depth' => '0','accgrp_edit' => '0','accgrp_flags' => '1','accgrp_enable_ledger' => '1','accgrp_code' => '' ),
        array('accgrp_id' => '9','accgrp_name' => 'Sales Accounts','accgrp_nature' => '0','accgrp_parent' => '9','accgrp_root' => '9','accgrp_depth' => '0','accgrp_edit' => '0','accgrp_flags' => '1','accgrp_enable_ledger' => '1','accgrp_code' => '' ),
        array('accgrp_id' => '10','accgrp_name' => 'Purchase Accounts','accgrp_nature' => '1','accgrp_parent' => '10','accgrp_root' => '10','accgrp_depth' => '0','accgrp_edit' => '0','accgrp_flags' => '1','accgrp_enable_ledger' => '1','accgrp_code' => '' ),
        array('accgrp_id' => '11','accgrp_name' => 'Capital Account','accgrp_nature' => '3','accgrp_parent' => '11','accgrp_root' => '11','accgrp_depth' => '0','accgrp_edit' => '0','accgrp_flags' => '1','accgrp_enable_ledger' => '1','accgrp_code' => '' ),
        array('accgrp_id' => '12','accgrp_name' => 'Current Liability','accgrp_nature' => '3','accgrp_parent' => '12','accgrp_root' => '12','accgrp_depth' => '0','accgrp_edit' => '0','accgrp_flags' => '1','accgrp_enable_ledger' => '1','accgrp_code' => '' ),
        array('accgrp_id' => '13','accgrp_name' => 'Sundry Creditors','accgrp_nature' => '3','accgrp_parent' => '12','accgrp_root' => '12','accgrp_depth' => '1','accgrp_edit' => '0','accgrp_flags' => '1','accgrp_enable_ledger' => '1','accgrp_code' => '' ),
        array('accgrp_id' => '14','accgrp_name' => 'Sundry Deptors','accgrp_nature' => '2','accgrp_parent' => '1','accgrp_root' => '1','accgrp_depth' => '1','accgrp_edit' => '0','accgrp_flags' => '1','accgrp_enable_ledger' => '1','accgrp_code' => '' ),
        array('accgrp_id' => '15','accgrp_name' => 'Duties and Taxes','accgrp_nature' => '3','accgrp_parent' => '12','accgrp_root' => '12','accgrp_depth' => '1','accgrp_edit' => '0','accgrp_flags' => '1','accgrp_enable_ledger' => '1','accgrp_code' => '' ),
        array('accgrp_id' => '16','accgrp_name' => 'Stock in Hand','accgrp_nature' => '2','accgrp_parent' => '1','accgrp_root' => '1','accgrp_depth' => '1','accgrp_edit' => '0','accgrp_flags' => '1','accgrp_enable_ledger' => '1','accgrp_code' => '' ),
        array('accgrp_id' => '17','accgrp_name' => 'Service Accounts','accgrp_nature' => '0','accgrp_parent' => '17','accgrp_root' => '17','accgrp_depth' => '0','accgrp_edit' => '0','accgrp_flags' => '1','accgrp_enable_ledger' => '1','accgrp_code' => '' ),
        array('accgrp_id' => '18','accgrp_name' => 'Loans (Liability)','accgrp_nature' => '3','accgrp_parent' => '18','accgrp_root' => '18','accgrp_depth' => '0','accgrp_edit' => '0','accgrp_flags' => '1','accgrp_enable_ledger' => '1','accgrp_code' => '' ),
        array('accgrp_id' => '19','accgrp_name' => 'Salary','accgrp_nature' => '3','accgrp_parent' => '19','accgrp_root' => '19','accgrp_depth' => '0','accgrp_edit' => '0','accgrp_flags' => '1','accgrp_enable_ledger' => '1','accgrp_code' => '' ),
        array('accgrp_id' => '20','accgrp_name' => 'Supplier','accgrp_nature' => '3','accgrp_parent' => '12','accgrp_root' => '12','accgrp_depth' => '1','accgrp_edit' => '0','accgrp_flags' => '1','accgrp_enable_ledger' => '1','accgrp_code' => '' ),
        array('accgrp_id' => '21','accgrp_name' => 'Customer','accgrp_nature' => '2','accgrp_parent' => '1','accgrp_root' => '1','accgrp_depth' => '1','accgrp_edit' => '0','accgrp_flags' => '1','accgrp_enable_ledger' => '1','accgrp_code' => '' ),
      );

      foreach( $inserts as $insert)
      {
        DB::table('acc_groups')->insert($insert);
      }

      DB::statement("ALTER TABLE erp_acc_groups AUTO_INCREMENT = 1000");
      $sync_time = date('ymdHis') . substr(microtime(), 2, 6);
      DB::statement("UPDATE erp_acc_groups SET server_sync_time=$sync_time");
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acc_groups');
    }
}
