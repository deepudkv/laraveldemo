<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseReturnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_return', function (Blueprint $table) {
            $table->bigIncrements('purchret_id');
            $table->string('purchret_id2',100);
            $table->biginteger('purchret_branch_stock_id');
            $table->string('purchret_no',50)->nullable();
            $table->string('purchret_inv_no',50)->nullable();
            $table->biginteger('purchret_refno')->nullable();
            $table->biginteger('purchret_supp_id');
            $table->biginteger('purchret_supp_retno')->nullable();
            $table->biginteger('purchret_supp_inv_no')->nullable();
            $table->tinyInteger('purchret_type')->nullable();
            $table->tinyInteger('purchret_pay_type')->nullable();
            $table->biginteger('purchret_gd_id');
            $table->date('purchret_date');
            $table->date('purchret_inv_date')->nullable();
            $table->double('purchret_amount');
            $table->double('purchret_cancelled_amount');
            $table->double('purchret_tax');
            $table->biginteger('purchret_tax_ledger_id');
            $table->double('purchret_discount')->nullable();
            $table->double('purchret_frieght')->nullable();
            $table->string('purchret_note',150)->nullable();
            $table->biginteger('purchret_frieght_ledger_id');
            $table->double('purchret_tax2');
            $table->double('purchret_tax3');
            $table->string('purchret_agent',100);
            $table->tinyInteger('purchret_flag')->default(1);
            $table->integer('company_id')->default(0);
            $table->integer('branch_id')->default(0);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_return');
    }
}
