<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesCommissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_commission', function (Blueprint $table) {
            $table->bigIncrements('salescomm_id');
            $table->integer("salescomm_sales_inv_no")->default(0);
            $table->integer("salescomm_ledger_id")->default(0);
            $table->date("salescomm_date")->default('2000-01-01');
            $table->double("salescomm_amount")->default(0);
            $table->double("salescomm_sales_amount")->default(0);
            $table->integer("salescomm_added_by")->default(1);
            $table->integer("salescomm_flags")->default(1);

            $table->integer('branch_id');
            $table->integer('godown_id')->default(0);
            $table->integer('van_id')->default(0);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_commission');
    }
}
