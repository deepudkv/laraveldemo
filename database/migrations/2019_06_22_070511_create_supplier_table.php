<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupplierTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acc_suppliers', function (Blueprint $table) {
            $table->bigIncrements('supp_id');
            $table->integer('ledger_id')->default(0);
            $table->string('supp_perm_addr',250)->nullable()->default('NILL');
            $table->string('supp_image',250)->nullable()->default('NILL');
            $table->double('opening_balance', 8, 2)->default(0);


            
            $table->integer('company_id')->default(0);
            $table->integer('branch_id')->default(0);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acc_suppliers');
    }
}
