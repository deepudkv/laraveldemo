 <?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesmasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_master', function (Blueprint $table) { 
            $table->bigIncrements('sales_id');
            $table->bigInteger('sales_inv_no');
            
            $table->tinyInteger('sales_cust_type')->comment = '0 - Non registered\n1 - Registered';
            $table->integer('sales_cust_id');
            $table->string('sales_cust_name',200);
            $table->string('sales_cust_ph',45);
            $table->string('sales_cust_address',500);
            $table->string('sales_cust_tin',25);

            $table->integer('sales_timestamp')->default(0);
            $table->date('sales_date')->default('2000-01-01');
            $table->time('sales_time')->default('00:00:00');
            $table->dateTime('sales_datetime')->default('2000-01-01 00:00:00');

            $table->double('sales_total');
            $table->double('sales_discount');
            $table->double('sales_disc_promo');
            $table->double('sales_disc_loyalty');
            $table->double('sales_paid');
            $table->double('sales_balance');

            $table->double('sales_profit');
            $table->double('sales_rec_amount');

            // $table->integer('sales_vch_no')->default(0);
            $table->double('sales_tax');
            // $table->bigInteger('sales_tax_vch_no');

            // $table->double('sales_service')->comment = 'Sales Service Charge';
            // $table->double('sales_service_discount');
            // $table->double('sales_misc');
            // $table->integer('sales_servicebill_id')->default(0);

            $table->integer('sales_added_by')->default(1);
            $table->tinyInteger('sales_flags')->default(1);
        
            
            $table->string('sales_notes',250);
            $table->integer('sales_pay_type')->default(0)->comment = '0 - Cash\n1 - Credit';//0 for full payment may be mixed payment, 1 for partial or credit
            
            // $table->integer('sales_pay_type2')->default(0);
            $table->integer('branch_id');
            $table->integer('godown_id')->default(0);
            $table->integer('van_id')->default(0);

            $table->integer('sales_agent_ledger_id')->default(0); // temp
            $table->integer('sales_acc_ledger_id')->default(0);
           
            // $table->double('sales_item_disc');
            $table->integer('sales_order_no')->default(0);
            $table->integer('sales_order_type')->default(0);
            // $table->string('sales_godown_inv',45);

            // $table->integer('sales_server_sync')->default(0);
            // $table->bigInteger('sales_sync_timestamp');
            // $table->bigInteger('sales_server_timestamp');
            // $table->integer('sales_godownsale_add')->default(0);
            $table->bigInteger('sales_godownsale_id')->default(0);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salesmaster');
    }
}
