<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class IsSyncFlagAddToSalesReturnSubTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sales_return_sub', function (Blueprint $table) {
            $table->tinyInteger('sync_completed')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales_return_sub', function (Blueprint $table) {
            $table->dropColumn([
                'sync_completed',
            ]);
        });
    }
}
