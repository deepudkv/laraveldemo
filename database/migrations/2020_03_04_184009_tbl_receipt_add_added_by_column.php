<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblReceiptAddAddedByColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_receipt', function (Blueprint $table) {
            $table->integer('inter_payment_no')->default(0);
            $table->integer('added_by')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acc_receipt', function (Blueprint $table) {
            $table->dropColumn([
                'inter_payment_no',
                'added_by',
            ]);
        });
    }
}
