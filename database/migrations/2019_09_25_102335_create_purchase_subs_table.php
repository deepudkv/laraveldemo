<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseSubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_subs', function (Blueprint $table) {
            $table->bigIncrements('purchsub_id');
            $table->biginteger('purchsub_purch_id');
            $table->biginteger('purchsub_prd_id');
            $table->biginteger('purchsub_stock_id');
            $table->biginteger('purchsub_branch_stock_id');
            $table->biginteger('purchsub_batch_id')->default(0);
            $table->string('purchsub_expiry',100)->default(0);
            $table->double('purchsub_qty')->nullable();
            $table->double('purchsub_rate')->default(0);
            $table->double('purchsub_frieght')->default(0);
            $table->double('purchsub_tax')->default(0);
            $table->double('purchsub_tax_per')->default(0);
            $table->double('purchsub_tax2')->default(0);
            $table->double('purchsub_tax2_per')->default(0);
            $table->double('purchsub_tax3')->default(0);
            $table->double('purchsub_tax3_per')->default(0);
            $table->biginteger('purchsub_unit_id')->nullable();
            $table->double('purchsub_gd_qty')->default(0);
            $table->integer('purchsub_gd_id')->nullable();
            $table->date('purchsub_date')->nullable();
            $table->tinyInteger('purchsub_flag')->default(1);
            $table->integer('company_id')->default(0);
            $table->integer('branch_id')->default(0);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_subs');
    }
}


// ALTER TABLE `erp_purchases` CHANGE `purch_id2` `purch_id2` VARCHAR(100) NOT NULL;