<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblSalesSubAddTaxCatIdColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sale_sub', function (Blueprint $table) {
            $table->integer('salesub_taxcat_id')->after('salesub_tax_per');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sale_sub', function (Blueprint $table) {
            $table->dropColumn([
                'salesub_taxcat_id',
            ]);
        });
    }
}
