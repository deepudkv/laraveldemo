<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesdueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_due', function (Blueprint $table) {
            
            $table->bigIncrements('sales_pay_id');
            $table->string('sales_recp_no', 30);
            $table->integer('salesdue_vch_no')->default(0);
            $table->date('salesdue_date')->default('2000-01-01');
            $table->integer('salesdue_timestamp');

            $table->integer('salesdue_ledger_id');
            $table->double('salesdue_amount');
            $table->integer('salesdue_multi');
            $table->integer('salesdue_added_by');
            $table->integer('salesdue_flags');
            $table->integer('salesdue_agent_ledger_id');
            $table->bigInteger('sales_pay_type'); //'0 - Cash\n1 - Bank\n2 - Card\n3 - Cheque\n4 - Wallet';
            $table->string('sales_pay_txn_id',30); 

            $table->integer('branch_id');
            $table->integer('godown_id')->default(0);
            $table->integer('van_id')->default(0);

            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salesdue');
    }
}
