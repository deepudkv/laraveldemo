<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contra', function (Blueprint $table) {
            $table->bigIncrements('cont_id');
            $table->bigInteger('cont_ref_no');
            $table->tinyInteger('cont_type')->comment = '0 - Deposit | 1 - Withdraw';
            $table->date('cont_date')->default('2000-01-01');;
            $table->time('cont_time')->default('00:00:00');
            $table->dateTime('cont_datetime')->default('2000-01-01 00:00:00');
            $table->integer('cont_timestamp');  
            $table->integer('cont_from_ledger_id');
            $table->bigInteger('cont_to_ledger_id');
            $table->double('cont_amount', 8, 2);
            $table->string('cont_note')->nullable();         
            $table->string('branch_cont_no',45);
            $table->string('branch_cont_no2',45);
            $table->tinyInteger('is_web');
            $table->tinyInteger('cont_flags');
            $table->bigInteger('sync_code')->default(0);
            $table->tinyInteger('sync_completed')->default(0);
            

            $table->integer('branch_id')->default(0);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contra');
    }
}
