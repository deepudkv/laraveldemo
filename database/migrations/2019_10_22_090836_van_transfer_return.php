<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VanTransferReturn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('van_transfer_return', function (Blueprint $table) {
            $table->bigIncrements('vantranret_id');
            $table->bigInteger('vantranret_vantran_id')->default(0);
            $table->bigInteger('vantranret_van_id');
            $table->bigInteger('vantranret_godown_id')->default(0);
            $table->double('vantranret_price');
            $table->double('vantranret_purch_price');
            $table->date('vantranret_date');
            $table->string('vantranret_notes',1000);
            $table->bigInteger('vantranret_van_ledger_id');
            $table->bigInteger('vantranret_added_by');
            $table->tinyInteger('vantranret_type')->default(0);
            $table->tinyInteger('vantranret_flag')->default(1);

            $table->integer('branch_id')->default(0);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
