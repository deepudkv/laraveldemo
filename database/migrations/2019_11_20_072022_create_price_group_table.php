<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePriceGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('price_group', function (Blueprint $table) {
            $table->bigIncrements('prcgrp_id');
            $table->string('group_name', 200);
            $table->string('group_descp', 1000);

            $table->integer('branch_id');
            $table->integer('godown_id')->default(0);
            $table->integer('van_id')->default(0);

            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('price_group');
    }
}
