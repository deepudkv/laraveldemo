<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchases', function (Blueprint $table) {
            $table->bigIncrements('purch_id');
            $table->string('purch_id2',100);
            $table->biginteger('purch_branch_id');
            $table->string('purch_no',50);
            $table->string('purch_inv_no',50);
            $table->biginteger('purch_supp_id');
            $table->biginteger('purch_ord_no');
            $table->tinyInteger('purch_type');
            $table->tinyInteger('purch_pay_type');
            $table->date('purch_date');
            $table->date('purch_inv_date')->nullable();
            $table->double('purch_amount');
            $table->double('purch_tax');
            $table->biginteger('purch_tax_ledger_id');
            $table->double('purch_discount');
            $table->double('purch_frieght');
            $table->string('purch_note',150)->nullable();
            $table->biginteger('purch_frieght_ledger_id');
            $table->double('purch_tax2');
            $table->double('purch_tax3');
            $table->string('purch_agent',100);
            $table->tinyInteger('purch_flag')->default(1);
            $table->integer('company_id')->default(0);
            $table->integer('branch_id')->default(0);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchases');
    }
}
