<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VoucherType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('voucher_type', function (Blueprint $table) {
            $table->bigIncrements('vchtype_id');
            $table->string('vchtype_name',50);
            $table->timestamps();
        });

       
            $insert = array(
                array('vchtype_name'=>'Payment'),
                array('vchtype_name'=>'Reciept'),
                array('vchtype_name'=>'Contra'),
                array('vchtype_name'=>'Journal'),
                array('vchtype_name'=>'Sales'),
                array('vchtype_name'=>'Purchase'),
                array('vchtype_name'=>'Debit note'),
                array('vchtype_name'=>'Credit note'),
                array('vchtype_name'=>'Purch Return'),
                array('vchtype_name'=>'Sales Return'),
                array('vchtype_name'=>'Service Charge'),
                array('vchtype_name'=>'Void Service'),
                array('vchtype_name'=>'Production'),
                array('vchtype_name'=>'Void Production'),
                array('vchtype_name'=>'Godown Transfer'),
                array('vchtype_name'=>'Stock Receipt Return'),
                array('vchtype_name'=>'Stock Receipt'),
                array('vchtype_name'=>'Stock Transfer Return'),
                array('vchtype_name'=>'Stock Transfer'),
                array('vchtype_name'=>'Dividend Allocation'),
                array('vchtype_name'=>'Starting Balance'),
                array('vchtype_name'=>'Opening Stock'),
                array('vchtype_name'=>'Invoice Due Receipt'),
                array('vchtype_name'=>'Input Tax'),
                array('vchtype_name'=>'Output Tax'),
// added by deepu 

                array('vchtype_name'=>'Van transfer(GV)'),
                array('vchtype_name'=>'Van transfer Return(VG)'),
                array('vchtype_name'=>'Void  transfer(VG)'),
                array('vchtype_name'=>'Void transfer Return(GV)'),
            );


        DB::table('voucher_type')->insert($insert);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('voucher_type');
    }
}
