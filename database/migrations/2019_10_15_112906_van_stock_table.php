<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VanStockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('van_stock', function (Blueprint $table) {
            $table->bigIncrements('vanstock_id');
            $table->bigInteger('vanstock_prod_id');
            $table->bigInteger('vanstock_stock_id');
            $table->bigInteger('vanstock_branch_stock_id');
            $table->bigInteger('vanstock_van_id');
            $table->double('vanstock_qty')->default(0);
            $table->double('vanstock_last_tran_rate')->default(0);
            $table->double('vanstock_avg_tran_rate')->default(0);
            $table->tinyInteger('vanstock_flags')->default(1);
            
            $table->integer('branch_id')->default(0);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
