<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductionFormulaProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('production_formula_product', function (Blueprint $table) {
            $table->bigIncrements('prodformprod_id');
            $table->bigInteger('prodformprod_prodform_id')->comment = 'formula id';
            $table->bigInteger('prodformprod_prod_id')->comment = 'output product id';
            $table->double('prodformprod_qty')->default(0);
            $table->double('prodformprod_unit_id')->default(1);
            $table->double('prodformprod_flag')->default(1);
            $table->integer('branch_id')->default(0);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('production_formula_product');
    }
}
