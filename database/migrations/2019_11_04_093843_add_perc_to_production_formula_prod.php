<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPercToProductionFormulaProd extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('production_formula_product', function (Blueprint $table) {
            $table->double('prodformprod_perc')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('production_formula_product', function (Blueprint $table) {
            $table->dropColumn(['prodformprod_perc']);
        });
    }
}
