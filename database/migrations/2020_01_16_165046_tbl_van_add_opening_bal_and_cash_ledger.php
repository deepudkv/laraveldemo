<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblVanAddOpeningBalAndCashLedger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('van', function (Blueprint $table) {
            $table->integer('van_cash_ledger_id')->after('branch_id')->default(0);
            $table->double('van_op_bal')->after('van_cash_ledger_id')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('van', function (Blueprint $table) {
            $table->dropColumn([
                'van_cash_ledger_id',
                'van_op_bal'
            ]);
        });
    }
}
