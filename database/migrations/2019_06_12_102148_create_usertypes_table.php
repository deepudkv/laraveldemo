<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsertypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_types', function (Blueprint $table) {
            $table->bigIncrements('type_id');
            $table->string('user_type',50);
            $table->timestamps();
        });

            $insert = array(
                array('user_type'=>'ADMIN'),
                array('user_type'=>'BRANCH ADMIN'),
                array('user_type'=>'BRANCH USER'),
                array('user_type'=>'VAN USER'),
            );

        DB::table('user_types')->insert($insert);

       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_types');
    }
}
