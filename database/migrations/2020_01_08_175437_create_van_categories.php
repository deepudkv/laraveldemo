<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVanCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('van_categories', function (Blueprint $table) {
            $table->bigIncrements('vc_id');

            $table->bigInteger('vc_van_id');
            $table->bigInteger('vc_cat_id');
            $table->bigInteger('vc_cat_flag')->default(1);  
            $table->bigInteger('vc_added_by');  
            $table->integer('branch_id');

            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('van_categories');
    }
}
