<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJournal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('journal', function (Blueprint $table) {
            
            $table->bigIncrements('jn_id');
            $table->bigInteger('jn_no');
            $table->date('jn_date')->default('2000-01-01');
            $table->time('jn_time')->default('00:00:00');
            $table->dateTime('jn_datetime')->default('2000-01-01 00:00:00');
            $table->integer('jn_timestamp');
            $table->double('jn_ttl_amount', 8, 2);
            $table->string('jn_note', 256);
            $table->string('van_jn_no',45);
            $table->string('branch_jn_no',45);
            $table->bigInteger('branch_rec_no2')->default(0);
            $table->integer('branch_id');
            $table->integer('van_id')->default(0);
            $table->integer('is_web')->default(0);
            $table->tinyInteger('jn_flags')->default(0);
        
            $table->integer('sync_code')->default(0);
            $table->tinyInteger('sync_completed')->default(0);

            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('journal');
    }
}
