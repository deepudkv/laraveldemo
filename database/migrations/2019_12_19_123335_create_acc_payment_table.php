<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccPaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acc_payment', function (Blueprint $table) {
            $table->bigIncrements('pay_id');
            $table->string('pay_no', 256);
            $table->date('pay_date')->default('2000-01-01');
            $table->time('pay_time')->default('00:00:00');
            $table->dateTime('pay_datetime')->default('2000-01-01 00:00:00');
            $table->integer('pay_timestamp');
            $table->integer('pay_acc_ledger_id');
            $table->double('pay_ttl_amount');
            $table->string('pay_note', 256);
            $table->integer('branch_id');
            $table->integer('van_id')->default(0);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acc_payment');
    }
}
