<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesreturnMasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_return_master', function (Blueprint $table) {
            $table->bigIncrements('salesret_id');
            $table->integer('salesret_sales_inv_no')->default(0);

            $table->date('salesret_date')->default('2000-01-01');
            $table->time('salesret_time')->default('00:00:00');
            $table->integer('salesdue_timestamp');

            $table->integer('salesdue_ledger_id');
            $table->double('salesdue_amount');
            $table->integer('salesret_cust_type')->comment = '0 - New\n1 - Registered';
            $table->integer('salesret_cust_id')->default(0);
            $table->string('salesret_cust_name',200);
            $table->string('salesret_cust_address',500);
            $table->string('salesret_cust_ph',45);
            $table->double('salesret_amount')->comment = 'total amount - cancelled discount';
            $table->double('salesret_discount')->comment = 'cancelled discount';
            $table->integer("salesret_pay_type")->comment ='0 - cash\n1 - Credit';
            $table->integer("salesret_added_by")->default(0);
            $table->integer("salesret_flags")->default(1);
            $table->integer("salesret_vch_no")->default(0);
            $table->string("salesret_notes",250)->default('');
            $table->double("salesret_sales_amount")->default(0);
            $table->double("salesret_sales_discount")->default(0);
            $table->double("salesret_service_amount")->default(0);
            $table->double("salesret_service_discount")->default(0);
            $table->integer("salesret_servoid_id")->default(0);
            $table->double("salesret_misc_amount")->default(0);
            $table->string("salesret_cust_tin",45)->default('');
            $table->double("salesret_tax")->default(0);
            $table->double("salesret_service_tax")->default(0);
            $table->string("salesret_tax_vch_no",45)->default('0');
            $table->integer("salesret_godown_id")->default(0);
            $table->integer("salesret_agent_ledger_id")->default(0);
            $table->integer("salesret_pay_type2")->default(0);
            $table->integer("salesret_acc_ledger_id")->default(0);
            $table->dateTime("salesret_datetime")->default('2000-01-01 00:00:00');
            $table->string("salesret_godown_ret",45)->default('');
            // $table->integer("salesret_server_sync",11)->default(0);
            // $table->bigInteger("salesret_sync_timestamp",20)->default(0);
            // $table->bigInteger("salesret_server_timestamp",20);

            $table->integer('branch_id');
            $table->integer('godown_id')->default(0);
            $table->integer('van_id')->default(0);

            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salesreturn_master');
    }
}
