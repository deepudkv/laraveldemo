<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesdueSubTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_due_sub', function (Blueprint $table) {
            
            $table->bigIncrements('salesduesub_id');
            $table->bigInteger('salesduesub_salesdue_id');
            $table->bigInteger('salesduesub_inv_no');
            $table->integer('salesduesub_ledger_id');
            $table->date('salesduesub_date')->default('2000-01-01');
            $table->integer('salesduesub_type');
            $table->double('salesduesub_in');
            $table->double('salesduesub_out');
            $table->double('salesduesub_inv_amount');
            $table->double('salesduesub_inv_balance');
            $table->integer('salesduesub_added_by');
            $table->integer('salesduesub_flags');
            $table->integer('salesduesub_agent_ledger_id');

            $table->integer('branch_id');
            $table->integer('godown_id')->default(0);
            $table->integer('van_id')->default(0);

            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salesdue_sub');
    }
}
