<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGodownMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('godown_master', function (Blueprint $table) {
            $table->bigIncrements('gd_id');
            $table->string('gd_name',50)->nullable();
            $table->string('gd_code',30)->nullable();
            $table->string('gd_cntct_person',30)->nullable();
            $table->string('gd_cntct_num',30)->nullable();            
            $table->string('gd_address',50)->nullable();
            $table->string('gd_desc',50)->nullable();
            
            $table->integer('branch_id')->default(0);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
          
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('godown_master');
    }
}
