<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblTaxCategoryAddColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tax_category', function (Blueprint $table) {
            $table->string('display_name', 500);
            $table->integer('is_sales_ledger');
            $table->integer('is_purchase_ledger');
            $table->integer('is_servcie_ledger');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tax_category', function (Blueprint $table) {
            $table->dropColumn([
                'display_name',
                'is_sales_ledger',
                'is_purchase_ledger',
                'is_servcie_ledger'
            ]);
        });
    }
}
