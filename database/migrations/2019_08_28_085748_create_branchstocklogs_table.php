<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchstocklogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branch_stock_logs', function (Blueprint $table) {
            $table->bigIncrements('bsl_log_id');
            $table->bigInteger('bsl_branch_stock_id');
            $table->bigInteger('bsl_stock_id')->nullable();
            $table->bigInteger('bsl_prd_id')->nullable();
            $table->double('bsl_shop_quantity')->default(0);
            $table->double('bsl_gd_quantity')->default(0);
            $table->double('bsl_prate')->default(0);
            $table->double('bsl_srate')->default(0);
            $table->tinyInteger('bsl_revert')->default(0);
            $table->double('bsl_avg_prate')->default(0);
            $table->integer('bsl_branch_id')->default(0);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branch_stock_logs');
    }
}
