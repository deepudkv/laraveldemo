<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblSalesReturnSubAddColumn3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sales_return_sub', function (Blueprint $table) {
            $table->string('salesretsub_sales_van_inv_no',50);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales_return_sub', function (Blueprint $table) {
            $table->dropColumn([
                'salesretsub_sales_van_inv_no',
            ]);
        });
    }
}
