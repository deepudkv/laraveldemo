<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVanDailyStocks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('van_daily_stocks', function (Blueprint $table) {
            $table->bigIncrements('vds_id');
            $table->bigInteger('vds_branch_stock_id');
            $table->bigInteger('vds_prd_id');
            $table->bigInteger('vds_stock_id');
            $table->bigInteger('vds_van_id');
            $table->date('vds_date')->default(date("Y-m-d"));
            $table->double('vds_stock_quantity');
         
            $table->integer('branch_id')->default(0);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('van_daily_stocks');
    }
}
