<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTaxCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tax_category', function (Blueprint $table) {

            $table->bigIncrements('taxcat_auto_id');
            $table->integer('taxcat_id');
            $table->string('taxcat_name', 256);
            $table->string('taxcat_code', 256);
            $table->double('taxcat_tax_per');

            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });

        $insert = array(
            array('taxcat_id' => 1, 'taxcat_name'=>'5%', 'taxcat_code' => '@5%', 'taxcat_tax_per' => 5),
            array('taxcat_id' => 2, 'taxcat_name'=>'50%', 'taxcat_code' => '@50%', 'taxcat_tax_per' => 50),
            array('taxcat_id' => 3, 'taxcat_name'=>'0%', 'taxcat_code' => '@0%', 'taxcat_tax_per' => 0),
        );

        DB::table('tax_category')->insert($insert);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tax_category');
    }
}
