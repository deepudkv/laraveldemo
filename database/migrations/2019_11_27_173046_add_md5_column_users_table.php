<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMd5ColumnUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('pass_md5',250)->after('usr_username');
        });

        $insert =  ['usr_type'=>'1','usr_name'=>'Fujishka',
        'usr_username'=>'Fujishka',
        'pass_md5'=>'e10adc3949ba59abbe56e057f20f883e',
        'usr_password'=>'$2y$10$A2wgmHzb4CYGMGvsibJOJOA8UxOJ2gj1x8ocBy3lxP8yiasJDvX5S',
        'usr_email'=>'admin@fujishka.com',
        'server_sync_time'=>$sync_time = date('ymdHis') . substr(microtime(), 2, 6)];
    
          DB::table('users')->insert($insert);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn([
                'pass_md5'
            ]);
        });
    }
}
