<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccPaymentSubTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acc_payment_sub', function (Blueprint $table) {
            $table->bigIncrements('paysub_id');
            $table->string('paysub_pay_no',256);
            $table->date('paysub_date')->default('2000-01-01');
            $table->time('paysub_time')->default('00:00:00');
            $table->dateTime('paysub_datetime')->default('2000-01-01 00:00:00');
            $table->integer('paysub_timestamp');
            $table->integer('paysub_acc_ledger_id');
            $table->integer('paysub_ledger_id');
            $table->double('paysub_amount');
            $table->string('paysub_narration',256);
            $table->double('paysub_vat_per');
            $table->double('paysub_vat_amt');
            $table->integer('branch_id');
            $table->integer('van_id')->default(0);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acc_payment_sub');
    }
}
