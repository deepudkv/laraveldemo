<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColoumnToAccReceipt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_receipt', function (Blueprint $table) {
            $table->string('van_rec_no',45);
            $table->string('branch_rec_no',45);
            $table->bigInteger('branch_rec_no2')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acc_receipt', function (Blueprint $table) {
            $table->dropColumn([
                'van_rec_no',
                'branch_rec_no',
                'branch_rec_no2',
            ]);
        });
    }
}
