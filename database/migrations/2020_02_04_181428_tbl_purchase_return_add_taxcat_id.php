<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblPurchaseReturnAddTaxcatId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchase_return_sub', function (Blueprint $table) {
            $table->integer('purchretsub_taxcat_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_return_sub', function (Blueprint $table) {
            $table->dropColumn([
                'purchretsub_taxcat_id',
            ]);
        });
    }
}
