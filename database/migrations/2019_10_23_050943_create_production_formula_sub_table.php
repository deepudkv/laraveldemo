<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductionFormulaSubTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('production_formula_sub', function (Blueprint $table) {
            $table->bigIncrements('prodformsub_id');
            $table->bigInteger('prodformsub_prodform_id')->comment = 'formula id';
            $table->bigInteger('prodformsub_prod_id')->comment = 'ingredient product id	';
            $table->double('prodformsub_qty')->default(0);
            $table->double('prodformsub_unit_id')->default(1);
            $table->tinyInteger('prodformsub_flag')->default(1);

            $table->integer('branch_id')->default(0);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('production_formula_sub');
    }
}
