<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockUnitRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_unit_rates', function (Blueprint $table) {
            $table->bigIncrements('sur_id');
            $table->bigInteger('branch_stock_id');
            $table->bigInteger('sur_prd_id');
            $table->bigInteger('sur_stock_id');
            $table->bigInteger('sur_unit_id');
            $table->double('sur_unit_rate');
         
            $table->integer('sur_branch_id')->default(0);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_unit_rates');
    }
}
