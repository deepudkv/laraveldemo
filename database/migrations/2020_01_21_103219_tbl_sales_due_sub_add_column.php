<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblSalesDueSubAddColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sales_due_sub', function (Blueprint $table) {
            $table->bigInteger('salesduesub_ret_no')->default(0);
            $table->string('salesduesub_van_ret_no', 50);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales_due_sub', function (Blueprint $table) {
            $table->dropColumn([
                'salesduesub_ret_no',
                'salesduesub_van_ret_no'
            ]);
        });
    }
}
