<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPrivilagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_privilages', function (Blueprint $table) {
            $table->bigIncrements('up_id');
            $table->integer('up_menu_id');
            $table->bigInteger('up_usr_id');
            $table->tinyInteger('up_show_menu')->default(1);
            $table->tinyInteger('up_add')->default(1);
            $table->tinyInteger('up_edit')->default(1);
            $table->tinyInteger('up_void')->default(1);

            $table->tinyInteger('up_flags')->default(1);

            $table->integer('branch_id');
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_privilages');
    }
}
