<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblPurchaseSubAddTaxcatId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('purchase_subs', function (Blueprint $table) {
            $table->integer('purchsub_taxcat_id');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('purchase_subs', function (Blueprint $table) {
            $table->dropColumn([
                'purchsub_taxcat_id',
            ]);
        });
    }
}
