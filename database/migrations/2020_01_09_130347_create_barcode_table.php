<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarcodeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings_barcode_print', function (Blueprint $table) {
            $table->bigIncrements('barset_id');
            $table->string('barset_profile',50)->nullable();
            $table->string('barset_barcode',200)->nullable();
            $table->string('barset_product',200)->nullable();
            $table->string('barset_currency',200)->nullable();
            $table->string('barset_price',200)->nullable();
            $table->string('barset_currency_short',50)->nullable();
            $table->string('barset_printer',500)->nullable();
            $table->double('barset_label_width')->nullable()->default(0);
            $table->double('barset_label_height')->nullable()->default(0);
            $table->double('barset_label_gap')->nullable()->default(0);
            $table->integer('barset_active')->nullable()->default(0);
            $table->integer('barset_dark')->nullable()->default(10);
            $table->integer('barset_price_enable')->nullable()->default(1); 
            $table->integer('barset_price_encrypt')->nullable()->default(0);
            $table->string('barset_prate_epl',200)->nullable()->default('340,95,0,3,1,1,N'); 
            $table->integer('barset_prate_enable')->nullable()->default(0);
            $table->integer('barset_prate_encrypt')->nullable()->default(0);          
            $table->integer('barset_mfg_enable')->nullable()->default(0);  
            $table->string('barset_mfg_epl',200)->nullable()->default('340,110,0,3,1,1,N'); 
            $table->integer('barset_exp_enable')->nullable()->default(0);  
            $table->string('barset_exp_epl',200)->nullable()->default('340,130,0,3,1,1,N'); 
            $table->integer('barset_shop_enable')->nullable()->default(0);  
            $table->string('barset_shop_epl',200)->nullable()->default('20,2,0,3,1,1,N'); 
            $table->string('barset_shop_name',200)->nullable();
            $table->integer('barset_pdet_enable')->nullable()->default(1); 
            $table->string('barset_pdet_text',200)->nullable()->default('Incl. VAT'); 
            $table->string('barset_pdet_epl',200)->nullable()->default('340,95,0,3,1,1,N'); 
            $table->integer('barset_pdet_type')->nullable()->default(1); 
            $table->integer('company_id')->default(0);
            $table->integer('branch_id')->default(0);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings_barcode_print');
    }
}
