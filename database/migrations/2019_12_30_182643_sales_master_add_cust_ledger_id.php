<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SalesMasterAddCustLedgerId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::table('sales_master', function (Blueprint $table) {
        //     $table->bigInteger('sales_cust_ledger_id');
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales_master', function (Blueprint $table) {
            $table->dropColumn([
                'sales_cust_ledger_id',
            ]);
        });
    }
}
