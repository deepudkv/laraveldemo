<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StockBatches extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_batches', function (Blueprint $table) {
            $table->bigIncrements('sb_id');
            $table->string('sb_batch_code',50);
            $table->bigInteger('sb_branch_stock_id');
            $table->bigInteger('sb_prd_id');
            $table->bigInteger('sb_stock_id');
            $table->bigInteger('sb_quantity');
            $table->date('sb_manufacture_date');
            $table->date('sb_expiry_date');
                                 
            $table->integer('branch_id')->default(0);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_batches');
    }
}
