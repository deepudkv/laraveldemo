<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseReturnSubTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_return_sub', function (Blueprint $table) {
            $table->bigIncrements('purchretsub_id');
            $table->biginteger('purchretsub_purch_id');
            $table->biginteger('purchretsub_prd_id');
            $table->biginteger('purchretsub_stock_id');
            $table->tinyinteger('purchretsub_branch_stock_id');
            $table->biginteger('purchretsub_batch_id')->default(0);
            $table->string('purchretsub_expiry',100)->default(0);
            $table->double('purchretsub_qty');
            $table->double('purchretsub_rate');
            $table->double('purchretsub_frieght')->nullable();
            $table->double('purchretsub_tax')->nullable();
            $table->double('purchretsub_tax_per')->nullable();
            $table->double('purchretsub_tax2');
            $table->double('purchretsub_tax2_per');
            $table->double('purchretsub_tax3');
            $table->double('purchretsub_tax3_per');
            $table->biginteger('purchretsub_unit_id');
            $table->integer('purchretsub_gd_id');
            $table->date('purchretsub_date');
            $table->tinyInteger('purchretsub_flag')->default(1);
            $table->integer('company_id')->default(0);
            $table->integer('branch_id')->default(0);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_return_sub');
    }
}


// ALTER TABLE `erp_purchases` CHANGE `purch_id2` `purch_id2` VARCHAR(100) NOT NULL;