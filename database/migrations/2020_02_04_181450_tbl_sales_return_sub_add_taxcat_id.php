<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblSalesReturnSubAddTaxcatId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sales_return_sub', function (Blueprint $table) {
            $table->integer('salesretsub_taxcat_id');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales_return_sub', function (Blueprint $table) {
            $table->dropColumn([
                'salesretsub_taxcat_id',
            ]);
        });
    }
}
