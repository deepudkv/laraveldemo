<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SalesMasterAddInvNo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sales_master', function (Blueprint $table) {
            $table->string('sales_branch_inv',45)->after('sales_pay_type');
            $table->integer('sales_branch_inv2')->after('sales_branch_inv');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales_master', function (Blueprint $table) {
            $table->dropColumn([
                'sales_branch_inv',
                'sales_branch_inv2'
            ]);
        });
    }
}
