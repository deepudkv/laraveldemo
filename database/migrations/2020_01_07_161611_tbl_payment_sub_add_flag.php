<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblPaymentSubAddFlag extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_payment_sub', function (Blueprint $table) {
            $table->tinyInteger('paysub_flags')->default(1);
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acc_payment_sub', function (Blueprint $table) {
            $table->dropColumn([
                'paysub_flags',
            ]);
        });
    }
}
