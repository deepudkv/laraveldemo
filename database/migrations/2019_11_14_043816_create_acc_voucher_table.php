<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccVoucherTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acc_voucher', function (Blueprint $table) {
            $table->bigIncrements('vch_id');
            // $table->integer("vch_id")->default(NULL);
            $table->integer("vch_no")->comment ='voucher number';
            $table->integer("vch_ledger_from")->comment ='Ledger from';
            $table->integer("vch_ledger_to")->comment ='Ledger to';
            $table->dateTime("vch_date")->default('2000-01-01 00:00:00');
            $table->double("vch_in")->comment ='amount in';
            $table->double("vch_out")->comment ='amount out';
            $table->integer("vch_vchtype_id")->comment ='voucher type\n0-pay\n1-Reciept\n2-contra\n3-journal\n4-sales\n5-purchase\n6-debitnote\n7-creditnote\n';
            $table->string("vch_notes",250)->default('');
            $table->integer("vch_added_by")->default(0);
            $table->integer("vch_id2")->default(0);
            $table->integer("vch_from_group")->default(0);
            $table->integer("vch_flags");
            $table->integer("sales_inv_no");

            $table->integer('branch_id');
            $table->integer('godown_id')->default(0);
            $table->integer('van_id')->default(0);

            $table->tinyInteger("server_sync_flag")->default(0);
            $table->bigInteger("server_sync_time")->nullable();
            $table->bigInteger("local_sync_time")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acc_voucher');
    }
}
