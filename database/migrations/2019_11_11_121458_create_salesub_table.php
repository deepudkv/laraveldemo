<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesubTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sale_sub', function (Blueprint $table) {
            $table->bigIncrements('salesub_id');
            
            $table->bigInteger('salesub_sales_inv_no');
            $table->integer('salesub_stock_id');
            $table->integer('salesub_branch_stock_id');
            $table->integer('salesub_prod_id');
            $table->double('salesub_rate');
            $table->double('salesub_qty');
            $table->double('salesub_rem_qty')->comment = 'Remaining Qty after returning';
            $table->double('salesub_discount');
            $table->integer('salesub_timestamp');
            $table->date('salesub_date')->default('2000-01-01');
            $table->string('salesub_serial', 150);
            $table->integer('salesub_flags');
            $table->double('salesub_profit');
            $table->integer('salesub_unit_id');
            $table->double('salesub_prate');
            $table->integer('salesub_promo_id');
            $table->double('salesub_promo_per');
            $table->double('salesub_promo_rate');
            $table->double('salesub_tax_per');
            $table->double('salesub_tax_rate');
            // $table->integer('branch_id');
            $table->integer('branch_id');
            $table->integer('godown_id');
            $table->integer('van_id')->default(0);
            // $table->integer('salesub_server_sync');
            // $table->bigInteger('salesub_sync_timestamp');
            // $table->bigInteger('salesub_server_timestamp');
            // $table->integer('salesub_godownsalesub_id');
            
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salesub');
    }
}
