<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubcategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subcategories', function (Blueprint $table) {
            //$table->bigInteger('subcat_id');
            $table->bigIncrements('subcat_id');
            $table->integer('subcat_parent_category');
            $table->string('subcat_name',150)->nullable();
            $table->mediumText('subcat_remarks')->nullable();
            $table->tinyInteger('subcat_flag')->default(1);
            $table->tinyInteger('subcat_pos')->default(1);

            $table->integer('company_id')->default(0);
            $table->integer('branch_id')->default(0);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subcategories');
    }
}
