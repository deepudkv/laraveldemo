<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTaxCategorySub extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tax_category_sub', function (Blueprint $table) {

            $table->bigIncrements('taxcatsub_auto_id');
            $table->integer('taxcatsub_id');
            $table->string('taxcatsub_name', 256);
            $table->string('taxcatsub_code', 256);
            $table->integer('taxcatsub_vchtype_id');

            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });

        $insert = array(
            array(
                'taxcatsub_id' => 1,
                'taxcatsub_name'=>'Sales', 
                'taxcatsub_code' => 'Sales', 
                'taxcatsub_vchtype_id' => 4
            ),
            array(
                'taxcatsub_id' => 2,
                'taxcatsub_name'=>'Output Tax', 
                'taxcatsub_code' => 'Output Tax', 
                'taxcatsub_vchtype_id' => 24
            ),
            array(
                'taxcatsub_id' => 3,
                'taxcatsub_name'=>'Purchase', 
                'taxcatsub_code' => 'Purchase', 
                'taxcatsub_vchtype_id' => 5
            ),
            array(
                'taxcatsub_id' => 4,
                'taxcatsub_name'=>'Input Tax', 
                'taxcatsub_code' => 'Input Tax', 
                'taxcatsub_vchtype_id' => 23
            ),
            array(
                'taxcatsub_id' => 5,
                'taxcatsub_name'=>'Sales Return', 
                'taxcatsub_code' => 'Sales Return', 
                'taxcatsub_vchtype_id' => 9
            ),
            array(
                'taxcatsub_id' => 6,
                'taxcatsub_name'=>'Purchase Return', 
                'taxcatsub_code' => 'Purchase Return', 
                'taxcatsub_vchtype_id' => 8
            ),
            array(
                'taxcatsub_id' => 7,
                'taxcatsub_name'=>'Service', 
                'taxcatsub_code' => 'Service', 
                'taxcatsub_vchtype_id' => 10
            ),
            array(
                'taxcatsub_id' => 8,
                'taxcatsub_name'=>'Service Return', 
                'taxcatsub_code' => 'Service Return', 
                'taxcatsub_vchtype_id' => 11
            ),
        );

        DB::table('tax_category_sub')->insert($insert);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tax_category_sub');
    }
}
