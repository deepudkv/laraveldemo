<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Damagedproducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('damages', function (Blueprint $table) {            
            $table->integer('damage_id')->default(0);
            $table->integer('damage_stock_id')->default(0);
            $table->integer('damage_prod_id')->default(0);
            $table->double('damage_qty')->nullable()->default(0);
            $table->double('damage_rem_qty')->nullable()->default(0);
            $table->double('damage_purch_rate')->nullable()->default(0);
            $table->string('damage_notes',100)->nullable()->default('');
            $table->string('damage_serial',100)->nullable()->default('');
            $table->date('damage_date')->nullable()->default('2000-01-01');
            $table->integer('damage_added_by')->nullable()->default(0);
            $table->tinyInteger('damage_flags')->default(1);
            $table->integer('damage_unit_id')->nullable()->default(1);             
            $table->integer('branch_id')->default(0);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
    });
}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('damages');
    }
}
