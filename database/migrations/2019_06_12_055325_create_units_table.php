<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('units', function (Blueprint $table) {
            //$table->bigInteger('unit_id');
            $table->bigIncrements('unit_id');
            $table->string('unit_name',40);
            $table->char('unit_code',15)->nullable();
            $table->char('unit_display',25)->nullable();
            $table->integer('unit_type')->default(1)->comment('0 - derived,1 - base ');
            $table->integer('unit_base_id')->default(0)->comment('unit_id of base unit');
            $table->integer('unit_base_qty')->nullable()->default(1)->comment('base qty/unit');
            $table->mediumText('unit_remarks')->nullable();
            $table->tinyInteger('unit_flag')->default(1);
            
            $table->integer('company_id')->default(0);
            $table->integer('branch_id')->default(0);
            $table->tinyInteger('server_sync_flag')->default(0);
            $table->bigInteger('server_sync_time')->default(0);
            $table->bigInteger('local_sync_time')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('units');
    }
}
